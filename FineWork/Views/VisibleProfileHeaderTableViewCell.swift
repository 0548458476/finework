//
//  VisibleProfileHeaderTableViewCell.swift
//  FineWork
//
//  Created by User on 27.2.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import GooglePlaces

class VisibleProfileHeaderTableViewCell: UITableViewCell, UITextFieldDelegate, SSRadioButtonControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK: -- Views
    @IBOutlet var fullNameTxt: UITextField!
    @IBOutlet var nickNameTxt: UITextField!
    @IBOutlet var phoneTxt: UITextField!
    @IBOutlet var mobileTxt: UITextField!
    @IBOutlet var ageTxt: UITextField!
    @IBOutlet var addressTxt: UITextField!
    @IBOutlet weak var placesTbl: UITableView!
    @IBOutlet var familyStatusTbl: UITableView!
    
    @IBOutlet var familyStatusLbl: UILabel!
    @IBOutlet var arrowFamilyStatusLbl: UILabel!
    
    // -- first language cell
    @IBOutlet var arrowLbl: UILabel!
    @IBOutlet var languageNameLbl: UILabel!

    @IBOutlet var motherTongueLevelBtn: SSRadioButton!
    @IBOutlet var goodLevelBtn: SSRadioButton!
    @IBOutlet var mediumLevelBtn: SSRadioButton!
    @IBOutlet var lowLevelBtn: SSRadioButton!
    
    @IBOutlet var languagesTbl: UITableView!
    
    //MARK: -- Variables
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    var cellOriginY : CGFloat = 0.0
    var familyStatusSelectedId : Int = -1
    
    // -- first language cell
    var radioButtonController: SSRadioButtonsController?
    var languagesArray : [SysTable] = []
    var firstLanguageSelectedId : Int = -1
    
    //places
    var placesArr = Array<GMSAutocompletePrediction>()
    var placesClient : GMSPlacesClient! = nil
    var placeSelected : GMSPlace? = nil
    var isChangePlace = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        radioButtonController = SSRadioButtonsController(buttons: motherTongueLevelBtn, goodLevelBtn, mediumLevelBtn, lowLevelBtn)
        radioButtonController?.delegate = self
        radioButtonController?.shouldLetDeSelect = true
        arrowLbl.text = "\u{f107}"
        arrowFamilyStatusLbl.text = "\u{f107}"
        
        //set click to language label
        let tap = UITapGestureRecognizer(target: self, action: #selector(setLanguagesTblState))
        languageNameLbl.addGestureRecognizer(tap)
        
        languagesTbl.delegate = self
        languagesTbl.dataSource = self
        
        //set click to family state text field
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(setFamilyStatusTblState))
        familyStatusLbl.addGestureRecognizer(tap2)
        
        familyStatusTbl.delegate = self
        familyStatusTbl.dataSource = self
        
        //set click to address text field
//        let tap2 = UITapGestureRecognizer(target: self, action: #selector(setPlacesTblState))
//        addressTxt.addGestureRecognizer(tap2)
        // google places
        placesClient = GMSPlacesClient.shared()
        addressTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        placesTbl.delegate = self
        placesTbl.dataSource = self

        
        fullNameTxt.delegate = self
        nickNameTxt.delegate = self
        phoneTxt.delegate = self
        mobileTxt.delegate = self
        //familyStatusLbl.delegate = self
        ageTxt.delegate = self
        addressTxt.delegate = self
        
        // הוספת תגית לזיהוי האם זה טקסט רגיל או האם הוא מונח על אוביקט אחר - לצורך העלאת המקלדת למקום המתאים.
        fullNameTxt.tag = 1
        nickNameTxt.tag = 1
        phoneTxt.tag = 1
        mobileTxt.tag = 1
        //familyStatusLbl.delegate = self
        ageTxt.tag = 1
        addressTxt.tag = 2
        
        fullNameTxt.keyboardType = .default
        nickNameTxt.keyboardType = .default
        phoneTxt.keyboardType = .phonePad
        mobileTxt.keyboardType = .phonePad
//        //familyStatusLbl.delegate = self
        ageTxt.keyboardType = .decimalPad
        addressTxt.keyboardType = .default
        
        mobileTxt.isUserInteractionEnabled = false
        
//        isChangePlace = false
//        placeSelected = nil

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(/*_cellOriginY : CGFloat, */languages : Array<SysTable>, workerLanguage : WorkerLanguage?) {
        //cellOriginY = _cellOriginY
        
        // initial fields
        let worker = Global.sharedInstance.worker
        
        fullNameTxt.text = worker.nvUserName
        nickNameTxt.text = worker.nvNickName
        phoneTxt.text = worker.nvPhone
        mobileTxt.text = worker.nvMobile
        // set family status
        familyStatusLbl.text = Global.sharedInstance.familyStatusArr[getIndexOfFamilyStatusId(familyStatusId: worker.iFamilyStatusType)]
        if worker.fAge != nil {
            ageTxt.text = String(describing: worker.fAge!).description
        }
        addressTxt.text = worker.nvAddress
        
        languagesArray = languages
        languagesTbl.reloadData()
        
        // -- first language cell
//        if radioButtonController?.selectedButton() == nil {
            if workerLanguage != nil {
                //print("שונה מנאל")
                setSelectedByLanguageLevelType(languageLevelType: (workerLanguage?.iLanguageLevelType)!)
                setLanguageById(id: (workerLanguage?.iLanguageType)!)
            } else {
                //print("לא שונה מנאל")
                if !motherTongueLevelBtn.isSelected && !goodLevelBtn.isSelected && !mediumLevelBtn.isSelected && !lowLevelBtn.isSelected {
//                    motherTongueLevelBtn.isSelected = true
                    languageNameLbl.text = "בחר שפה"
                }
                if firstLanguageSelectedId == -1 {
                    languageNameLbl.text = "בחר שפה"
                }
            }
//        }
        
    }
    
    func setCellOriginY(_cellOriginY : CGFloat) {
        cellOriginY = _cellOriginY
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        getTextFieldDelegate.getTextField(textField: textField, originY: cellOriginY)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case fullNameTxt:
            nickNameTxt.becomeFirstResponder()
            break
        case nickNameTxt:
            phoneTxt.becomeFirstResponder()
            break
        case phoneTxt:
            mobileTxt.becomeFirstResponder()
            break
        case mobileTxt:
            ageTxt.becomeFirstResponder()
            break
        case ageTxt:
            addressTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case fullNameTxt:
            limitLength = 40
            break
        case nickNameTxt:
            limitLength = 20
            break
        case ageTxt:
            limitLength = 4
            break
        case addressTxt:
            limitLength = 150
            break
        case phoneTxt:
            limitLength = 10
            break
        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
 
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField.text != "" {
            placeAutocomplete(stringToSearch: textField.text!)
            
        } else {
            placesTbl.isHidden = true
        }
        isChangePlace = true
    }
    
    //MARK: -- table for first language cell / family state
    func setLanguagesTblState() {
        languagesTbl.isHidden = !languagesTbl.isHidden
    }
    
    func setFamilyStatusTblState() {
        familyStatusTbl.isHidden = !familyStatusTbl.isHidden
    }
    
    func setPlacesTblState() {
        placesTbl.isHidden = !placesTbl.isHidden
    }
   
    // language table view table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == languagesTbl {
            return languagesArray.count
        } else if tableView == familyStatusTbl {
            return Global.sharedInstance.familyStatusArr.count
        } else {
            return placesArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == languagesTbl {
            languageNameLbl.text = languagesArray[indexPath.row].nvName
            firstLanguageSelectedId = languagesArray[indexPath.row].iId
            setLanguagesTblState()
        } else if tableView == familyStatusTbl {
            familyStatusLbl.text = Global.sharedInstance.familyStatusArr[indexPath.row]
            familyStatusSelectedId = getFamilyStatusId(rowNumber: indexPath.row)
            setFamilyStatusTblState()
        } else {
            addressTxt.text = placesArr[indexPath.row].attributedFullText.string
            getPlaceByID(placeId: placesArr[indexPath.row].placeID!)
            setPlacesTblState()
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath) as! FamilyStatusTableViewCell
        if tableView == languagesTbl {
            cell.setDisplayData(txtStatus: languagesArray[indexPath.row].nvName)
        } else if tableView == familyStatusTbl {
            cell.setDisplayData(txtStatus: Global.sharedInstance.familyStatusArr[indexPath.row])
        } else {
            cell.setDisplayData(txtStatus: placesArr[indexPath.row].attributedFullText.string)
        }
        
        return cell
    }
    
    func getFamilyStatusId(rowNumber : Int) -> Int {
        switch rowNumber {
        case 0:
            return FamilyStateKey.Single.rawValue
        case 1:
            return FamilyStateKey.Married.rawValue
        case 2:
            return FamilyStateKey.Divorcee.rawValue
        case 3:
            return  FamilyStateKey.Widow.rawValue
        case 4:
            return FamilyStateKey.Separated.rawValue
        default:
            return -1
        }
    }
    
    func getIndexOfFamilyStatusId(familyStatusId : Int) -> Int {
        switch familyStatusId {
        case FamilyStateKey.Single.rawValue:
            return 0
        case FamilyStateKey.Married.rawValue:
            return 1
        case FamilyStateKey.Divorcee.rawValue:
            return 2
        case FamilyStateKey.Widow.rawValue:
            return 3
        case FamilyStateKey.Separated.rawValue:
            return 4
        default:
            return 0
        }
    }
    
    func getLanguageLevelType() -> Int {
        if motherTongueLevelBtn.isSelected {
            return LanguageLevelType.MotherTongue.rawValue
        }
        if goodLevelBtn.isSelected {
            return LanguageLevelType.Good.rawValue
        }
        if mediumLevelBtn.isSelected {
            return LanguageLevelType.Medium.rawValue
        }
        if lowLevelBtn.isSelected {
            return LanguageLevelType.Low.rawValue
        }
        return -1
    }
    
    func setSelectedByLanguageLevelType(languageLevelType : Int) {
        firstLanguageSelectedId = languageLevelType
        switch languageLevelType {
        case LanguageLevelType.MotherTongue.rawValue:
            motherTongueLevelBtn.isSelected = true
            goodLevelBtn.isSelected = false
            mediumLevelBtn.isSelected = false
            lowLevelBtn.isSelected = false
            break
        case LanguageLevelType.Good.rawValue:
            motherTongueLevelBtn.isSelected = false
            goodLevelBtn.isSelected = true
            mediumLevelBtn.isSelected = false
            lowLevelBtn.isSelected = false
            break
        case LanguageLevelType.Medium.rawValue:
            motherTongueLevelBtn.isSelected = false
            goodLevelBtn.isSelected = false
            mediumLevelBtn.isSelected = true
            lowLevelBtn.isSelected = false
            break
        case LanguageLevelType.Low.rawValue:
            motherTongueLevelBtn.isSelected = false
            goodLevelBtn.isSelected = false
            mediumLevelBtn.isSelected = false
            lowLevelBtn.isSelected = true
            break
        default:
            break
        }
    }
    
    func setLanguageById(id : Int) {
        for language in languagesArray {
            if language.iId == id {
                languageNameLbl.text = language.nvName
                firstLanguageSelectedId = language.iId
                break
            }
        }
    }
    
    func setErrorToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.red.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setValidToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.clear.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    //MARK: - Google Places
    func placeAutocomplete(stringToSearch : String) {
        
//        let filter = GMSAutocompleteFilter()
//        filter.type = .address
        
        placesClient.autocompleteQuery(/*"Sydney Oper"*/stringToSearch, bounds: nil, filter: /*filter*/nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.placesArr = results
                self.placesTbl.isHidden = false
                self.placesTbl.reloadData()
                for result in results {
                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
            }
        })
    }
    
    func getPlaceByID(placeId : String) {
        //let placeID = "ChIJV4k8_9UodTERU5KXbkYpSYs"
        
        placesClient.lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeId)")
                return
            }
            
            //print("Place name \(place.name)")
            //print("Place address \(place.formattedAddress)")
            //print("Place placeID \(place.placeID)")
            //print("Place attributions \(place.attributions)")
            self.placeSelected = place
//            self.addMarkerAndCircle(position: place.coordinate, radius: 10 * 1000)
            
        })
        
    }

}
