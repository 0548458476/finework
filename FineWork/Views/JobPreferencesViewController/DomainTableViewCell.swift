//
//  JobPreferenceTableViewCell.swift
//  FineWork
//
//  Created by Lior Ronen on 21/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class DomainTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, BasePopUpActionsDelegate {

    //MARK: - Views
    @IBOutlet var constantJobBtn: CheckBox!
    @IBOutlet var partialJobBtn: CheckBox!
    @IBOutlet var domainLbl: UILabel!
    @IBOutlet var roleLbl: UILabel!
    @IBOutlet var experienceYearsTxt: UITextField!
    @IBOutlet var hourlySalaryTxt: UITextField!
    @IBOutlet var lineView: UIView!
    @IBOutlet var removeView: UIView!
    @IBOutlet var removeBtn: UIButton!
    @IBOutlet var viewHeightFromTop: NSLayoutConstraint!
    @IBOutlet var domainsOrRolesTbl: UITableView!
    @IBOutlet weak var domainArrowLbl: UILabel!
    @IBOutlet weak var roleArrowLbl: UILabel!
    
    //MARK: - Actions
    @IBAction func constantJobBtnAction() {
        constantJobBtn.isChecked = !constantJobBtn.isChecked
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func partialJobBtnAction() {
        partialJobBtn.isChecked = !partialJobBtn.isChecked
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func removeBtnAction() {
        if !domainsOrRolesTbl.isHidden {
            setTblState()
        }
        openBasePopUp()
//        removeDomainDelegate.removeCell!(cellNumber: cellNumber)
    }
    
    //MARK: - Variables
    var removeDomainDelegate : CellOptionsDelegate! = nil
    var cellNumber : Int = 0
    
    var rolesFilterArray : Array<EmploymentDomainRole> = Array<EmploymentDomainRole>()
    //var rolesArray : Array<String> = Array<String>()
    var isDomain : Bool = false
    var workerRoleSysTables = WorkerRoleSysTables()
    var domainSelectedId : Int = 0
    var workerDomain : WorkerDomain = WorkerDomain()
    var workerRolelStrings = Array<EmploymentDomainRole>()
    var changeScrollViewHeightDelegate : ChangeScrollViewHeightDelegate! = nil
    
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        removeBtn.layer.borderWidth = 1
        removeBtn.layer.borderColor = removeBtn.currentTitleColor.cgColor
        removeBtn.layer.cornerRadius = removeBtn.frame.height / 2
        
        domainsOrRolesTbl.isHidden = true
        
        experienceYearsTxt.delegate = self
        hourlySalaryTxt.delegate = self
        
        experienceYearsTxt.keyboardType = .decimalPad
        hourlySalaryTxt.keyboardType = .decimalPad
        
        experienceYearsTxt.tag = 1
        hourlySalaryTxt.tag = 1
        
        domainArrowLbl.text = FontAwesome.sharedInstance.arrow
        roleArrowLbl.text = FontAwesome.sharedInstance.arrow
        
        domainsOrRolesTbl.layer.borderWidth = 1
        domainsOrRolesTbl.layer.borderColor = ControlsDesign.sharedInstance.colorTurquoise.cgColor
        
        roleLbl.text = ""
        domainLbl.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDesignData(isFirst : Bool, _cellNumber : Int) {
        
        lineView.isHidden = isFirst
        removeView.isHidden = isFirst
        viewHeightFromTop.constant = isFirst ? 0 : 70
        cellNumber = _cellNumber
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(setTblToRoles))
        roleLbl.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(setTblToDomains))
        domainLbl.addGestureRecognizer(tapGestureRecognizer2)
        
    }
    
    func setDisplayData(_workerRoleSysTables : WorkerRoleSysTables, _workerDomain : WorkerDomain?) {
        roleLbl.text = ""
        domainLbl.text = ""
        workerRoleSysTables = _workerRoleSysTables
        if _workerDomain != nil {
            workerDomain = _workerDomain!
            
            domainSelectedId = workerDomain.iEmploymentDomainType
            filterRolesByRole()
            
            for domainIndex in stride(from: 0, to: workerRoleSysTables.lEmploymentDomainType.count, by: 1) {
                if workerRoleSysTables.lEmploymentDomainType[domainIndex].iId == workerDomain.iEmploymentDomainType {
                    domainLbl.text = workerRoleSysTables.lEmploymentDomainType[domainIndex].nvName
                    break
                }
            }
            
            for roleIndex in stride(from: 0, to: workerDomain.lWorkerRoles.count, by: 1) {
                let domainRoleIndex = getIndexOfRoleById(role: workerDomain.lWorkerRoles[roleIndex].iEmploymentRoleType)
                if domainRoleIndex != -1 {
                    if roleLbl.text == "" {
                        roleLbl.text = rolesFilterArray[domainRoleIndex].nvEmploymentRoleTypeName
                    } else {
                        roleLbl.text = roleLbl.text! + ", " + rolesFilterArray[domainRoleIndex].nvEmploymentRoleTypeName
                    }
                }
            }
            
            if workerDomain.nYearsOfExperience != nil {
                experienceYearsTxt.text = String(describing: workerDomain.nYearsOfExperience!).description
            }
            
            if workerDomain.mHourlyWage != nil {
                hourlySalaryTxt.text = String(describing: workerDomain.mHourlyWage!).description
            }
            
            constantJobBtn.isChecked = workerDomain.bIsForFullTimeJob
            partialJobBtn.isChecked = workerDomain.bIsForPartTimeJob
            
        }
    }
    
    func setTblToRoles() {
        isDomain  = false
        domainsOrRolesTbl.reloadData()
        setTblState()
    }
    
    func setTblToDomains() {
        isDomain = true
        domainsOrRolesTbl.reloadData()
        setTblState()
    }
    
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfRowsInSection = 0
          
        if section == 1/* || isDomain*/ {
            
            numOfRowsInSection = isDomain ? workerRoleSysTables.lEmploymentDomainType.count : rolesFilterArray.count//  domainsArray.count : rolesArray.count
        } else {
           numOfRowsInSection = 1
        }
        
        return numOfRowsInSection
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
//        if isDomain {
//            return 1
//        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        if indexPath.section == 1/* || isDomain*/ {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath)
            
            (cell as! FamilyStatusTableViewCell).setDisplayData(txtStatus: isDomain ? workerRoleSysTables.lEmploymentDomainType[indexPath.row].nvName : rolesFilterArray[indexPath.row].nvEmploymentRoleTypeName)  //domainsArray[indexPath.row] :
            
            if (isDomain && workerRoleSysTables.lEmploymentDomainType[indexPath.row].isSelected) || (!isDomain && rolesFilterArray[indexPath.row].isSelected) {
                (cell as! FamilyStatusTableViewCell).contentView.backgroundColor = ControlsDesign.sharedInstance.colorTurquoise
            } else {
                (cell as! FamilyStatusTableViewCell).contentView.backgroundColor = UIColor.clear
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "closeTblCell", for: indexPath as IndexPath)
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
//        cell.setSelected(true, animated: true)
//        if !isDomain {
//            tableView.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition(rawValue: indexPath.row)!)
//            setRowSelected(indexPath: indexPath)
//        }
//        tableView(tableView, didSelectRowAt: indexPath)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1/* || isDomain*/ {
            setRowSelected(indexPath: indexPath, isInit: false)
            
            Global.sharedInstance.bWereThereAnyChanges = true
        } else {
            setTblState()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        (domainsOrRolesTbl.cellForRow(at: indexPath) as! FamilyStatusTableViewCell).contentView.backgroundColor = UIColor.clear //setBackgroundCellView(color: ControlsDesign.sharedInstance.colorWhiteGray)
        rolesFilterArray[indexPath.row].isSelected = false
        let index = getIndexOfDomainRoleById(role: rolesFilterArray[indexPath.row].iEmploymentDomainRoleId)
        if index > -1 && index < workerDomain.lWorkerRoles.count {
            workerDomain.lWorkerRoles.remove(at: index)
            if index < workerRolelStrings.count {
                workerRolelStrings.remove(at: index)
            }
        }
        setRolesText(row: indexPath.row)
    
    }
    
    func setRowSelected(indexPath : IndexPath, isInit : Bool) {
        if isDomain {
            
            if domainLbl.text != workerRoleSysTables.lEmploymentDomainType[indexPath.row].nvName {
                for role in rolesFilterArray {
                    role.isSelected = false
                }
                roleLbl.text = ""
                workerRolelStrings.removeAll()
                workerDomain.lWorkerRoles.removeAll()
            }
            domainLbl.text = workerRoleSysTables.lEmploymentDomainType[indexPath.row].nvName//domainsArray[indexPath.row]
            domainSelectedId = workerRoleSysTables.lEmploymentDomainType[indexPath.row].iId
            workerDomain.iEmploymentDomainType = domainSelectedId
            filterRolesByRole()
            setTblState()
        } else {
            
            if let cell = domainsOrRolesTbl.cellForRow(at: indexPath) {
                (cell as! FamilyStatusTableViewCell).contentView.backgroundColor = ControlsDesign.sharedInstance.colorTurquoise //setBackgroundCellView(color: ControlsDesign.sharedInstance.colorTurquoise)
            }
                rolesFilterArray[indexPath.row].isSelected = true
                if !isInit {
                    let workerRole = WorkerRole()
                    workerRole.iEmploymentDomainRoleId = rolesFilterArray[indexPath.row].iEmploymentDomainRoleId
                    workerDomain.lWorkerRoles.insert(workerRole, at: workerDomain.lWorkerRoles.count)
                }
                workerRolelStrings.insert(rolesFilterArray[indexPath.row], at: workerRolelStrings.count)
                setRolesText(row: indexPath.row)
//            }
            
            
        }
    }
    
    func setTblState() {
        domainsOrRolesTbl.isHidden = !domainsOrRolesTbl.isHidden
        
        if let tbl = self.superview?.superview as? UITableView {
//            tbl.contentSize.height += domainsOrRolesTbl.isHidden ? -100 : 100
            // change table height and scroll
            changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: domainsOrRolesTbl.isHidden ? -100 : 100)
            tbl.beginUpdates()
            tbl.endUpdates()
        }
        
        if !domainsOrRolesTbl.isHidden && !isDomain {
            workerRolelStrings.removeAll()
            for role in workerDomain.lWorkerRoles {
                let indexRow = getIndexInFilteredArray(role: role.iEmploymentDomainRoleId)
                if indexRow != -1 {
                let indexPath = IndexPath(row: indexRow, section: 1)
                    
                    domainsOrRolesTbl.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
                    setRowSelected(indexPath: indexPath, isInit: true)
                }
            }
        }
    }
    
    func filterRolesByRole() {
        rolesFilterArray.removeAll()
        for domainRole in workerRoleSysTables.lEmploymentDomainRole {
            if domainRole.iEmploymentDomainType == domainSelectedId {
                rolesFilterArray.append(domainRole)
            }
        }
    }
    
    func isRoleExistInArray(role : Int) -> Bool{
        for domainRole in workerDomain.lWorkerRoles {
            if domainRole.iEmploymentDomainRoleId == role {
                return true
            }
        }
        
        return false
    }
    
    func getIndexOfRoleById(role : Int) -> Int{
        for domainRoleIndex in stride(from: 0, to: /*workerDomain.lWorkerRoles.count*/rolesFilterArray.count, by: 1) {
            if /*workerDomain.lWorkerRoles[domainRoleIndex].iEmploymentDomainRoleId*/rolesFilterArray[domainRoleIndex].iEmploymentRoleType == role {
                return domainRoleIndex
            }
        }
        
        return -1
    }

    func getIndexOfDomainRoleById(role : Int) -> Int{
        for domainRoleIndex in stride(from: 0, to: /*workerDomain.lWorkerRoles.count*/workerDomain.lWorkerRoles.count, by: 1) {
            if /*workerDomain.lWorkerRoles[domainRoleIndex].iEmploymentDomainRoleId*//*rolesFilterArray[domainRoleIndex].iEmploymentDomainRoleId*/workerDomain.lWorkerRoles[domainRoleIndex].iEmploymentDomainRoleId == role {
                return domainRoleIndex
            }
        }
        
        return -1
    }
    
    func getIndexInFilteredArray(role : Int) -> Int{
        for domainRoleIndex in stride(from: 0, to: /*workerDomain.lWorkerRoles.count*/rolesFilterArray.count, by: 1) {
            if /*workerDomain.lWorkerRoles[domainRoleIndex].iEmploymentDomainRoleId*/rolesFilterArray[domainRoleIndex].iEmploymentDomainRoleId == role {
                return domainRoleIndex
            }
        }
        
        return -1
    }
    
    func setRolesText(row : Int) {
        roleLbl.text = ""
        for i in stride(from: 0, to: workerRolelStrings.count, by: 1) {
            if i == 0 {
                roleLbl.text = workerRolelStrings[i].nvEmploymentRoleTypeName
            } else {
                roleLbl.text  = roleLbl.text! + ", " + workerRolelStrings[/*row*/i].nvEmploymentRoleTypeName
            }
        }
    }
    
    func getWorkerDomain() -> WorkerDomain {
        workerDomain.bIsForFullTimeJob = constantJobBtn.isChecked
        workerDomain.bIsForPartTimeJob = partialJobBtn.isChecked

        workerDomain.nYearsOfExperience = Float(experienceYearsTxt.text!)
        workerDomain.mHourlyWage = Double(hourlySalaryTxt.text!)
        
        return workerDomain
    }
    
    //MARK: - Text Field Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        getTextFieldDelegate.getTextField(textField: textField, originY: 0)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case experienceYearsTxt:
            hourlySalaryTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case experienceYearsTxt:
            let count = text.characters.count
            if count == 0 && string == "." {
                limitLength = 0
            } else if count == 1 && string == "." {
                limitLength = 2
            } else if count > 1 && text[1] == "." {
                if string == "." {
                    limitLength = 0
                } else {
                    limitLength = 4
                }
            } else if count > 2 && text[2] == "."{
                if string == "." {
                    limitLength = 0
                } else {
                    limitLength = /*5*/4
                }
            } else if count == 2 && string == "." && text != "99" {
                limitLength = 3
            } else {
                limitLength = 2
            }
            
            break
        case hourlySalaryTxt:
            limitLength = 7
            
            break
        
        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
        
    }
    
    //MARK: - BasePopUpActionsDelegate
    func okAction(num: Int) {
        removeDomainDelegate.removeCell!(cellNumber: cellNumber)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך להסיר את התחום?"
        
        openAnotherPageDelegate.openViewController!(VC: basePopUp)
    }

    

}
