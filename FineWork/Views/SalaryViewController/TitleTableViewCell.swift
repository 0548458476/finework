//
//  TitleTableViewCell.swift
//  FineWork
//
//  Created by Tamy wexelbom on 16.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class TitleTableViewCell: UITableViewCell ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var btnPostalBank: SSRadioButton!
    
    @IBOutlet weak var lblArrow: UILabel!
    
    @IBOutlet weak var btnBankTransfer: SSRadioButton!
    @IBAction func btnPostalBankAction(_ sender: SSRadioButton) {
        btnPostalBank.isSelected = true
        btnBankTransfer.isSelected = false
        
        conHeightPaymentPostalBank.constant = 80
        conHeightPaymentBank.constant = 0
        
        viewPaymentPostalBank.isHidden = false
        viewPaymentBank.isHidden = true
        
        (self.superview?.superview as! UITableView).tag = 1
        (self.superview?.superview as! UITableView).beginUpdates()
        (self.superview?.superview as! UITableView).endUpdates()
        
         Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnBankTransferAction(_ sender: SSRadioButton) {
        btnPostalBank.isSelected = false
        btnBankTransfer.isSelected = true
        
        conHeightPaymentPostalBank.constant = 0
        conHeightPaymentBank.constant = 130
        
        viewPaymentPostalBank.isHidden = true
        viewPaymentBank.isHidden = false
        
        (self.superview?.superview as! UITableView).tag = 2
        (self.superview?.superview as! UITableView).beginUpdates()
        (self.superview?.superview as! UITableView).endUpdates()
        
        for bank in banksArr{
            if bank.iId == Global.sharedInstance.worker.workerBankAccountDetails.iBankId{
                lblBank.text = bank.nvName
                iBankSelected = bank.iId
                break
            }
        }
        if let iBranch = Global.sharedInstance.worker.workerBankAccountDetails.iBranchNumber{
            txtBranch.text = String(iBranch)
        }
        
        if let iAccountNumber = Global.sharedInstance.worker.workerBankAccountDetails.iAccountNumber{
            txtNumber.text = String(iAccountNumber)
        }
        if Global.sharedInstance.worker.workerBankAccountDetails.nvAccountName == ""{
            Global.sharedInstance.worker.workerBankAccountDetails.nvAccountName = Global.sharedInstance.worker.nvUserName
        }
        txtName.text = Global.sharedInstance.worker.workerBankAccountDetails.nvAccountName
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }

    @IBOutlet weak var txtBranch: UITextField!
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtNumber: UITextField!
    
    @IBOutlet weak var lblBank: UILabel!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBAction func btnSaveAction(_ sender: UIButton) {
        if btnBankTransfer.isSelected{
            txtName.layer.borderWidth = 0
            txtNumber.layer.borderWidth = 0
            txtBranch.layer.borderWidth = 0
            lblBank.layer.borderWidth = 0
            var okValidation = true
            if txtName.text == "" || !Validation.sharedInstance.nameValidation(string: txtName.text!) {
                self.txtName.layer.borderColor = UIColor.red.cgColor
                txtName.layer.borderWidth = 1
                txtName.becomeFirstResponder()
                okValidation = false
            }
            if txtNumber.text! == "" || !Validation.sharedInstance.isStringContainsOnlyNumbers(string: txtNumber.text!) || (txtNumber.text?.characters.count)! > 9 {
                txtNumber.layer.borderColor = UIColor.red.cgColor
                txtNumber.layer.borderWidth = 1
                txtNumber.becomeFirstResponder()
                okValidation = false
            }
            if txtBranch.text! == "" || !Validation.sharedInstance.isStringContainsOnlyNumbers(string: txtBranch.text!) || (txtBranch.text?.characters.count)! > 5 {
                txtBranch.layer.borderColor = UIColor.red.cgColor
                txtBranch.layer.borderWidth = 1
                txtBranch.becomeFirstResponder()
                okValidation = false
            }
            if iBankSelected == -1{
                lblBank.layer.borderColor = UIColor.red.cgColor
                lblBank.layer.borderWidth = 1
                okValidation = false
            }
            if okValidation{
            Global.sharedInstance.worker.iPaymentMethodType = PaymentMethodType.BankTransfer.rawValue
            if iBankSelected != -1{
                Global.sharedInstance.worker.workerBankAccountDetails.iBankId = iBankSelected
            }
                Global.sharedInstance.worker.workerBankAccountDetails.iBranchNumber = Int(txtBranch.text!)
                Global.sharedInstance.worker.workerBankAccountDetails.iAccountNumber = Int(txtNumber.text!)
            Global.sharedInstance.worker.workerBankAccountDetails.nvAccountName = txtName.text!
            saveDelegate.saveWorkerFinal()
            }else{
                Alert.sharedInstance.showAlert(mess: "יש למלא את כל השדות בנתונים תקינים!")
            }
        }
        else if btnPostalBank.isSelected{
            Global.sharedInstance.worker.iPaymentMethodType = PaymentMethodType.CashWithdrawal.rawValue
            saveDelegate.saveWorkerFinal()
        }
        
    }
    
    @IBOutlet weak var conHeightPaymentPostalBank: NSLayoutConstraint!
    @IBOutlet weak var conHeightPaymentBank: NSLayoutConstraint!
    
    @IBOutlet weak var viewPaymentPostalBank: UIView!
    
    @IBOutlet weak var viewPaymentBank: UIView!
    
    @IBOutlet weak var tblBanks: UITableView!
    
    @IBOutlet weak var conHeightTblBanks: NSLayoutConstraint!
    
    var banksArr : Array<SysTable> = []
    var saveDelegate:SaveWorkerDelegate! = nil
    var iBankSelected = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnSave.layer.borderColor = UIColor.orange2.cgColor
        btnSave.layer.borderWidth = 2
        btnSave.layer.cornerRadius = btnSave.frame.height/2
        
        conHeightPaymentPostalBank.constant = 0
        conHeightPaymentBank.constant = 0
        
        viewPaymentBank.isHidden = true
        viewPaymentPostalBank.isHidden = true
        
        tblBanks.delegate = self
        tblBanks.dataSource = self
        tblBanks.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openBanksTable))
        lblBank.isUserInteractionEnabled = true
        lblBank.addGestureRecognizer(tap)
        
        txtName.layer.borderWidth = 0
        txtNumber.layer.borderWidth = 0
        txtBranch.layer.borderWidth = 0
        lblBank.layer.borderWidth = 0
        
//        for item in Global.sharedInstance.workerCodeTables {
//            if item.Key == Global.sharedInstance.bankCodeTable {
//                self.banksArr = item.Value
//            }
//        }
        
        txtName.keyboardType = .default
        txtBranch.returnKeyType = .next
        txtNumber.returnKeyType = .next
        txtName.returnKeyType = .done
        
        lblArrow.text = FontAwesome.sharedInstance.arrow

        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDisplayData(){
        if Global.sharedInstance.worker.iPaymentMethodType == PaymentMethodType.BankTransfer.rawValue{
            btnBankTransfer.isSelected = true
            btnPostalBank.isSelected = false
            conHeightPaymentPostalBank.constant = 0
            conHeightPaymentBank.constant = 130
            
            viewPaymentPostalBank.isHidden = true
            viewPaymentBank.isHidden = false
            
            if let tbl = self.superview?.superview as? UITableView {
                tbl.tag = 2
                tbl.beginUpdates()
                tbl.endUpdates()
            }

        for bank in banksArr{
            if bank.iId == Global.sharedInstance.worker.workerBankAccountDetails.iBankId{
                lblBank.text = bank.nvName
                iBankSelected = bank.iId
                break
            }
        }
        if let iBranch = Global.sharedInstance.worker.workerBankAccountDetails.iBranchNumber{
            txtBranch.text = String(iBranch)
        }
        
        if let iAccountNumber = Global.sharedInstance.worker.workerBankAccountDetails.iAccountNumber{
            txtNumber.text = String(iAccountNumber)
        }
        if Global.sharedInstance.worker.workerBankAccountDetails.nvAccountName == ""{
            Global.sharedInstance.worker.workerBankAccountDetails.nvAccountName = Global.sharedInstance.worker.nvUserName
            }
        txtName.text = Global.sharedInstance.worker.workerBankAccountDetails.nvAccountName
        }else if Global.sharedInstance.worker.iPaymentMethodType == PaymentMethodType.CashWithdrawal.rawValue{
            conHeightPaymentPostalBank.constant = 80
            conHeightPaymentBank.constant = 0
            
            viewPaymentPostalBank.isHidden = false
            viewPaymentBank.isHidden = true
            
            if let tbl = self.superview?.superview as? UITableView {
                tbl.tag = 1
                tbl.beginUpdates()
                tbl.endUpdates()
            }
            btnPostalBank.isSelected = true
            btnBankTransfer.isSelected = false
        }
    }
    
    func openBanksTable(sender:UITapGestureRecognizer) {
        tblBanks.isHidden = !tblBanks.isHidden
    }
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return banksArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell") as! FamilyStatusTableViewCell
        cell.setTitle(text: banksArr[indexPath.row].nvName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lblBank.text = banksArr[indexPath.row].nvName
        iBankSelected = banksArr[indexPath.row].iId
        tblBanks.isHidden = true
    }
    
}
