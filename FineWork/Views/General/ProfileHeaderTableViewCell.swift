//
//  ProfileHeaderTableViewCell.swift
//  FineWork
//
//  Created by User on 27.2.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

protocol ChangeSubViewProfileDelegate {
    func changeSubViewProfile(tagNewView:Int)
    func tapBack()
}


class ProfileHeaderTableViewCell: UITableViewCell, UploadFilesDelegate {

    //MARK:- Outlet
    
    //MARK:-- Buttons
    
    //MARK:--- Menu Buttons
    
    @IBOutlet var btnEmployeeDetaisCard: UIButton!
    
    @IBOutlet var btnEmployeeSalaryDetails: UIButton!
    @IBOutlet var btnEmployeeVisibleProfile: UIButton!
    
    @IBOutlet var btnEmployeeContract: UIButton!
    @IBOutlet var btnEmployeeRegisterDetails: UIButton!
    @IBOutlet var btnUserProphilImgChange: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    //MARK:-- Images
    
    @IBOutlet var imgViewUserProphilImage: UIImageView!
    
    //MARK:-- TextFields
    //MARK:-- Labels
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblUserDetails: UILabel!
    
    //MARK:--Views
    
    @IBOutlet var viewDivider: UIView!
    @IBOutlet var viewMenu: UIView!
    @IBOutlet var viewUnderIMGViewProphilUserName: UIView!
    
    //MARK:-- Actions
    
    @IBAction func btnUserProphilImgChangeAction(_ sender: Any) {
        ChangePicture()
    }
    
    @IBAction func btnChangeViewAction(_ sender: UIButton) {
      changeSubViewProfileDelegate.changeSubViewProfile(tagNewView: sender.tag)
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        changeSubViewProfileDelegate.tapBack()
    }
    
    //MARK:- Variabels
//    var strImage = ""
    var flagChangePictiure = false
    var imagePicker = UIImagePickerController()
     var changeSubViewProfileDelegate:ChangeSubViewProfileDelegate! = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // 1
        let layer = CALayer()
        layer.frame = imgViewUserProphilImage.bounds
        
        // 2
        layer.contents = imgViewUserProphilImage.image
        layer.contentsGravity = kCAGravityCenter
        
        // 3
        layer.magnificationFilter = kCAFilterLinear
        layer.isGeometryFlipped = false
        
        // 4
        layer.backgroundColor = UIColor.clear.cgColor
        layer.opacity = 1.0
        layer.isHidden = false
        layer.masksToBounds = false
        
        // 5
        layer.cornerRadius = imgViewUserProphilImage.frame.height/2
        layer.borderWidth = 7.0
        layer.borderColor = UIColor.gray.cgColor
        
        // 6
        layer.shadowOpacity = 0.75
        layer.shadowOffset = CGSize(width: 3, height: 2)
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize.zero
        imgViewUserProphilImage.layer.addSublayer(layer)
        
        imgViewUserProphilImage.layer.borderColor = UIColor.gray.cgColor
        imgViewUserProphilImage.layer.borderWidth = 7
        
        
        //
        imgViewUserProphilImage.layer.cornerRadius = imgViewUserProphilImage.frame.height/2
        imgViewUserProphilImage.layer.masksToBounds = true
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(self.imageTapped(img:)))
        imgViewUserProphilImage.isUserInteractionEnabled = true
        imgViewUserProphilImage.addGestureRecognizer(tapGestureRecognizer1)
        
        let layer2 = layer
        layer2.frame = viewUnderIMGViewProphilUserName.bounds
        layer2.contents = UIColor.clear
        layer2.cornerRadius = viewUnderIMGViewProphilUserName.frame.height/2
        layer2.backgroundColor = UIColor.clear.cgColor
        layer2.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 0.25
        viewUnderIMGViewProphilUserName.layer.addSublayer(layer2)
        viewUnderIMGViewProphilUserName.layer.cornerRadius = viewUnderIMGViewProphilUserName.frame.height/2
        viewUnderIMGViewProphilUserName.backgroundColor = UIColor.clear
        
        lblUserName.text = Global.sharedInstance.user.nvUserName
        getUserImage()
        
        setDesign()

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
    func getUserImage(){

        print("A-log getUserImage()")
        if let image : UIImage = Global.sharedInstance.user.profileImage {
            imgViewUserProphilImage.image = image
        }
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    self.imgViewUserProphilImage.contentMode = .scaleToFill
                    self.imgViewUserProphilImage.image = image
                    Global.sharedInstance.user.profileImage = image
                }
                
                Global.sharedInstance.user.picture.nvFile = Global.sharedInstance.convertImageToBase64(image: image)
            }
        }
        
    }
    
    //MARK: ----camera func
    func ChangePicture()
    {
//        openAlert()
        
    }
    func openAlert()
    {
//        Alert.sharedInstance.AlertopenPictures(controller: self, tag: 1)
    }
    
    func openAction(img: UIImageView)
    {
//        Alert.sharedInstance.AlertopenPictures(controller: self,tag: 1)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        imgViewUserProphilImage.image = image
//        self.dismiss(animated: true, completion: nil);
    }
    
    
    func imageTapped(img: AnyObject)
    {
        
        uploadFilesDelegate.uploadFile!(fileUrl: "")
        //        var stringURL: String = "ibooks://"
        //        var url = URL(string: stringURL)
        //        UIApplication.shared.openURL(url!)
        
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
//            print("Button capture")
//            
//            
////            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
//            imagePicker.allowsEditing = false
//            
////            self.present(imagePicker, animated: true, completion: nil)
//        }
        
    }
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL{
            if let imageName = imageUrl.lastPathComponent{
                if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        imgViewUserProphilImage.contentMode = .scaleToFill
                        imgViewUserProphilImage.image = image
                        setImageProfileInServer(imageName: imageName, image: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                    
                    //                    image = Global.sharedInstance.resizeImage(image: image, newWidth: 100)
                    //                    imgViewUserProphilImage.contentMode = .scaleToFill
                    //                    imgViewUserProphilImage.image = image
                    //                    setImageProfileInServer(imageName: imageName, image: image)
                }
            }
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    imgViewUserProphilImage.contentMode = .scaleToFill
                    imgViewUserProphilImage.image = image
                    setImageProfileInServer(imageName: imageName, image: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func setImageProfileInServer(imageName:String,image:String){
        let imageObj = FileObj()
        imageObj.nvFile = image
        imageObj.nvFileName = imageName
        var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicToServer["image"] = imageObj.getImageDic() as AnyObject?
        dicToServer["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dicToServer["iUserType"] = Global.sharedInstance.user.iUserType.rawValue as AnyObject?
        api.sharedInstance.UpdateImage(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
            }else{
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })
        
    }
    
    //MARK:-- SETTERS Functions
    func configureButton(btn:UIButton)
    {
        let f:CGFloat = btn.frame.height / 2
        btn.layer.cornerRadius = f
        //        btn.layer.cornerRadius = 0.5 * btn.bounds.size.width
        //        btn.layer.borderColor = UIColor.white.cgColor
        //        btn.layer.borderWidth = 2.0
        //btn.clipsToBounds = true
    }
    func setDesign()    {
        setBackgroundcolor()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        setImages()
    }
    func setBackgroundcolor()
    {
      //  self.viewMenu.backgroundColor = ControlsDesign.sharedInstance.colorDarkPurple
        setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
     //   setBackgroundcolorToBtn(btn: btnBack, color: ControlsDesign.sharedInstance.colorDarkPurple)
    }
    func setBackgroundcolorToBtn(btn:UIButton, color:UIColor){
        btn.backgroundColor = color
    }
    
    func setTextcolorToBtn(btn:UIButton, color:UIColor)
    {
        btn.titleLabel?.textColor = color
        btn.tintColor = color
    }
    
    func setBorderToBtn(btn:UIButton, color:UIColor , width:CGFloat){
        btn.layer.borderColor = color.cgColor
        btn.layer.borderWidth = width
    }
    
    func setFontToButton(btn:UIButton, name:String, size:CGFloat){
        btn.titleLabel!.font =  UIFont(name: name, size: size)
    }
    
    func setImages(){
        let backgroundButtonImage = UIImage(named: "money")
        btnEmployeeSalaryDetails.setImage(backgroundButtonImage, for: .normal)
    }
    
    func setBorders(){
        setBorderToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.white , width: 1.0)
        setBorderToBtn(btn: btnEmployeeDetaisCard, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeContract, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.white, width: 1.0)
    }
    func setTextcolor(){
        lblUserName.textColor = UIColor.white
        lblUserDetails.textColor = UIColor.white
        setTextcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeContract, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.white)
    }
    
    func setCornerRadius(){
        self.configureButton(btn: btnEmployeeDetaisCard)
        self.configureButton(btn: btnEmployeeVisibleProfile)
        self.configureButton(btn: btnEmployeeContract)
        self.configureButton(btn: btnEmployeeSalaryDetails)
        self.configureButton(btn: btnEmployeeRegisterDetails)
    }
    
    func setIconFont(){
        btnEmployeeDetaisCard.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_DetaisCard, for: .normal)//facebook
        btnEmployeeVisibleProfile.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)//-profile
        btnEmployeeContract.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_Contract, for: .normal)//people-2
        btnEmployeeRegisterDetails.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_RegisterDetails, for: .normal)//interface-5
    }
    
    func setLocalizableString(){
    }
    
    //MARK:--  Other Delegate
    func remove101FormFromSuperView(tagNewView:Int){
//        viewCon101.view.removeFromSuperview()
        switch tagNewView {
        case 1:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            break
        case 2:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
            break
        case 3:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
            break
        case 4:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
            break
        default:
            break
        }
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func tapBack(){
//        let _ = self.navigationController?.popViewController(animated: true)
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func setImageProfile(image:UIImage){
        imgViewUserProphilImage.image = image
    }
    
    
    func showAlert(sendMessage: String)
    {
        let alert : UIAlertController = UIAlertController(title: "", message: sendMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction:UIAlertAction = UIAlertAction(title: NSLocalizedString((LocalizableStringStatic.sharedInstance.OK_ALERT), comment: ""), style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            //self.dismiss()
        })
        
        alert.addAction(okAction)
        
//        self.present(alert, animated: true, completion: nil)
    }
    
    
    func reloadData(tagMenu: Int) {
        getUserImage()
        
        switch tagMenu {
        case 1:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            break
        case 2:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
            break
        case 3:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
            break
        case 4:
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
            break
        default:
            break
        }
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        if displayImg != nil {
            imgViewUserProphilImage.contentMode = .scaleToFill
            imgViewUserProphilImage.image = displayImg
            Global.sharedInstance.user.profileImage = displayImg
            setImageProfileInServer(imageName: imgName, image: img)
        }
    }


}
