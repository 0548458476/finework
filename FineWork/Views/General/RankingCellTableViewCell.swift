//
//  RankingCellTableViewCell.swift
//  FineWork
//
//  Created by User on 14.6.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class RankingCellTableViewCell: UITableViewCell {

    //MARK: Views
    
    @IBOutlet weak var imgRank: UIImageView!
    @IBOutlet weak var textRang: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
       imgRank.layer.cornerRadius = imgRank.frame.height / 2
       imgRank.layer.masksToBounds = true
        imgRank.contentMode = UIViewContentMode.scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
