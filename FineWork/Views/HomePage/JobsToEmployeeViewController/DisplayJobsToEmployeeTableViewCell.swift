//
//  DisplayJobsToEmployeeTableViewCell.swift
//  FineWork
//
//  Created by User on 25.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class DisplayJobsToEmployeeTableViewCell: UITableViewCell {

    //MARK: - Views
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var jobNameLbl: UILabel!
    @IBOutlet weak var jobDescriptionLbl: UILabel!
    @IBOutlet weak var applyForCandidateBtn: UIButton!
    
    //MARK: - Constrains
    @IBOutlet weak var applyForCandidateHeightCon: NSLayoutConstraint!
    
    
    
    
    //MARK: - Actions
    
    @IBAction func btnStarAction(_ sender: Any) {
        if btnStar.titleLabel?.textColor == UIColor.black
        {
            btnStar.setTitle(FlatIcons.sharedInstance.fullStar ,for: .normal)
            btnStar.setTitleColor(UIColor.orange2, for: .normal)
        }
        else{
            btnStar.setTitle(FlatIcons.sharedInstance.emptyStar, for: .normal)
            btnStar.setTitleColor(UIColor.black,for: .normal)
        }
    }
    
    @IBAction func applyForCandidateBtnAction(_ sender: Any) {
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let viewCon = story.instantiateViewController(withIdentifier: "PersonalInformationModelViewController") as?PersonalInformationModelViewController
        openAnotherPageDelegate.openViewController!(VC: viewCon!)
        
    }
    //MARK: - Variables
    // delegates
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    var isDownloadImg = false
    var indexPath : IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnStar.setTitle(FlatIcons.sharedInstance.emptyStar, for: .normal)
        
        jobNameLbl.text = ""
        jobDescriptionLbl.text = ""
        
//        Global.sharedInstance.setButtonCircle(sender: applyForCandidateBtn, isBorder: true)
        
//        if isBorder {
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        Global.sharedInstance.setButtonCircle(sender: applyForCandidateBtn, isBorder: true)
        logoImg.layer.cornerRadius = logoImg.frame.height / 2
        logoImg.layer.masksToBounds = true

    }
    
    func setDisplayData(workerHomePageJob : WorkerHomePageJob?) {
        if workerHomePageJob != nil/* && jobNameLbl.text! != workerHomePageJob?.nvEmployerName && jobDescriptionLbl.text! != workerHomePageJob?.nvAddress*/ {
//                                isDownloadImg = true
            //            var stringPath = api.sharedInstance.buldUrlFile(fileName: (workerHomePageJob?.nvLogoImageFilePath)!)
            //            stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
            //            if let url = URL(string: stringPath) {
            //                downloadImage(url: url)
            //            }
            logoImg.image = workerHomePageJob?.logoImage
//            if workerHomePageJob!.nvLogoImageFilePath == "" {
//                
//                
//            } else {
//                var stringPath = api.sharedInstance.buldUrlFile(fileName: (workerHomePageJob!.nvLogoImageFilePath))
//                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
//                
//                            if let url = URL(string: stringPath) {
//                                downloadImage(url: url)
//                            }
////                logoImg.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
//            }
//            if workerHomePageJob?.logoImage != nil {
//                logoImg = workerHomePageJob?.logoImage
//                logoImg.contentMode = .scaleToFill
//            }
            jobNameLbl.text = workerHomePageJob?.nvEmployerName
            jobDescriptionLbl.text = workerHomePageJob?.nvAddress
        }
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    if let tbl = self.superview?.superview as? UITableView {
                        if self.indexPath != nil && tbl.cellForRow(at: self.indexPath!) != nil {
                            self.logoImg.contentMode = .scaleToFill
                            self.logoImg.image = image
                        }}
                }
            }
        }
    }
    
//    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
//        applyForCandidateBtn.layer.borderColor = applyForCandidateBtn.titleLabel?.textColor.cgColor
//        applyForCandidateBtn.layer.borderWidth = 1
//        //        }
//        applyForCandidateBtn.layer.cornerRadius = applyForCandidateHeightCon.constant / 2
//        
//        return nil
//    }

}
