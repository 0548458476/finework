//
//  JobDayCalanderCollectionViewCell.swift
//  FineWork
//
//  Created by User on 07/09/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class JobDayCalanderCollectionViewCell: JTAppleCell {
    
    //MARK:- Outlet
    @IBOutlet weak var dateNumber: UILabel!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewButtom: UIView!
    @IBOutlet weak var checkBoxBtn: CheckBoxCalander!
    @IBOutlet weak var fromLbl: UILabel!
    @IBOutlet weak var toLbl: UILabel!
    
    //MARK:- Vareble
    var position : Int = 0
    var status : Int = OfferJobStatusType.new.rawValue
    var timeAvailablityDel : TimeAvailablityDelegates!
    var timeAvailablity = WorkerOfferedJobTimeAvailablity()
    //MARK:- Action
    @IBAction func checkBtnAction(_ sender: Any) {
        print("qqq \(checkBoxBtn.isChecked )")
        timeAvailablity.bWorkerOfferedJobTimeAvailablity = !checkBoxBtn.isChecked
        timeAvailablityDel.ChangeStatusDel(position: position, checked: !checkBoxBtn.isChecked )
        //checkBoxBtn.isChecked = !checkBoxBtn.isChecked    }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    
    
    func setDisplayData(cellState : CellState , status : Int ){
        self.status = status
        
      //  print("pppp2 \(self.status)")
        
        toLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
        fromLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
        checkBoxBtn.transform = CGAffineTransform(scaleX: -1, y: 1)
        dateNumber.transform = CGAffineTransform(scaleX: -1, y: 1)
        dateNumber.text = cellState.text
        self.layer.borderColor = (UIColor.black).cgColor
        self.layer.borderWidth = 0.5
        self.backgroundColor = UIColor.white
    }
    
    func handleCellSelection (cellState : CellState , timeAvailablity : WorkerOfferedJobTimeAvailablity , statusDateInCalander : Int , statusJob : Int , position : Int){
        self.position = position
        self.timeAvailablity = timeAvailablity
       // print("pppp3 \(statusJob)")
        
        dateNumber.textColor = UIColor.gray
        viewButtom.isHidden = false
        
        
        fromLbl.text =  "מ-" + timeAvailablity.tFromHoure
        toLbl.text = "עד " + timeAvailablity.tToHoure
        
        if statusJob == OfferJobStatusType.interesting.rawValue {
            print("dd intersting ")
            
            checkBoxBtn.isHidden = false
           
            if statusDateInCalander == StatusDateInCalander.noSelected.rawValue {
                 checkBoxBtn.isChecked = false
            }else {
                timeAvailablity.bWorkerOfferedJobTimeAvailablity = true
                 checkBoxBtn.isChecked = true
            }

           
        }else  {
            checkBoxBtn.isHidden = true
            print("dd no intersting ")
        }
        
        
    }
    func handleCellSelection (cellState : CellState  , statusDateInCalander : Int ){
        
        viewButtom.isHidden = true
        checkBoxBtn.isHidden = true
        
        if statusDateInCalander != StatusDateInCalander.regularNoInMonth.rawValue {
            dateNumber.textColor = UIColor.black
            // print("date  \(cellState.date)   regularNoInMonth"  )
            
        }else if statusDateInCalander != StatusDateInCalander.regularInMonth.rawValue {
            dateNumber.textColor = UIColor.gray
            // print("date  \(cellState.date)   regularInMonth"  )
            
        }}
}
