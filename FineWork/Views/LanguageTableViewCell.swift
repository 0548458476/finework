//
//  LanguageTableViewCell.swift
//  FineWork
//
//  Created by Tamy wexelbom on 1.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//
@objc protocol CellOptionsDelegate {
    @objc optional func removeCell(cellNumber : Int)
    @objc optional func editCell(cellNumber : Int)
}

import UIKit

class LanguageTableViewCell: UITableViewCell ,SSRadioButtonControllerDelegate, UITableViewDelegate, UITableViewDataSource, BasePopUpActionsDelegate
{

    //MARK: -- Views
    
    @IBOutlet weak var languageViewLbl: UILabel!
    @IBOutlet weak var arrowLbl: UILabel!
    @IBOutlet weak var languageNameLbl: UILabel!
    
    @IBOutlet weak var motherToungeLevelBtn: SSRadioButton!
    @IBOutlet weak var goodLevelBtn: SSRadioButton!
    @IBOutlet weak var mediumLevelBtn: SSRadioButton!
    @IBOutlet weak var lowLevelBtn: SSRadioButton!
    
    @IBOutlet var lineView: UIView!
    @IBOutlet var removeBtn: UIButton!
    //@IBOutlet var removeLbl: UILabel!
    @IBOutlet var removeView: UIView!
    @IBOutlet var languageViewHeightFromTop: NSLayoutConstraint!
    
    @IBOutlet var languagesTbl: UITableView!
    
    //MARK: -- Actions
    
    @IBAction func removeLanguage() {
//        Alert.sharedInstance.showAskAlertWithCellDelegate(mess: "האם ברצונך למחוק את השפה?", delegate: self)
        openBasePopUp()
        
    }
    
    
    
    //MARK: -- Variables
    
    var radioButtonController: SSRadioButtonsController?
    var distanceFromTop = 60.0
    
    var cellNumber = 0
    var removeLanguageDelegate : CellOptionsDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    var languagesArray : [SysTable] = []
    var languageSelectedId : Int = -1
    var isSetData = true
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        isSetData = true
        print("awakeFromNib()")
        
        radioButtonController = SSRadioButtonsController(buttons: motherToungeLevelBtn, goodLevelBtn, mediumLevelBtn, lowLevelBtn)
        radioButtonController?.delegate = self
        radioButtonController?.shouldLetDeSelect = true
        
        removeBtn.layer.borderWidth = 1
        removeBtn.layer.borderColor = ControlsDesign.sharedInstance.colorTurquoise.cgColor
        removeBtn.layer.cornerRadius = removeBtn.frame.height / 2
        
        arrowLbl.text = "\u{f107}"
        
        //set click to language label
        let tap = UITapGestureRecognizer(target: self, action: #selector(LanguageTableViewCell.setLanguagesTblState))
        //let tap = UITapGestureRecognizer(target: self, action: /*Selector(("openLanguagesTbl"))*/#selector(LanguageTableViewCell.openLanguagesTbl(/*sender:*/)))
        /*languageViewLbl*/languageNameLbl.addGestureRecognizer(tap)
        
        languagesTbl.delegate = self
        languagesTbl.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(/*isFirst : Bool, cellNumber : Int, */languages : Array<SysTable>, workerLanguage : WorkerLanguage?) {
        
        //lineView.isHidden = isFirst
        //removeView.isHidden = isFirst
        
        //languageViewHeightFromTop.constant = CGFloat(isFirst ? 10.0 : distanceFromTop)
        
        //self.cellNumber = cellNumber
        
        print("setDisplayData()")
        
        languagesArray = languages
        languagesTbl.reloadData()
        
        // fill fields
//        if radioButtonController?.selectedButton() == nil {
        if isSetData {
            isSetData = false
            if workerLanguage != nil {
                setSelectedByLanguageLevelType(languageLevelType: (workerLanguage?.iLanguageLevelType)!)
                setLanguageById(id: (workerLanguage?.iLanguageType)!)
            } else {
                languageNameLbl.text = "בחר שפה"
                languageSelectedId = -1
                
                motherToungeLevelBtn.isSelected = false
                goodLevelBtn.isSelected = false
                mediumLevelBtn.isSelected = false
                lowLevelBtn.isSelected = false
                
//                if !motherToungeLevelBtn.isSelected && !goodLevelBtn.isSelected && !mediumLevelBtn.isSelected && !lowLevelBtn.isSelected {
////                    motherToungeLevelBtn.isSelected = true
//                    languageNameLbl.text = "בחר שפה"
//                }
//                
//                if languageSelectedId == -1 {
//                    languageNameLbl.text = "בחר שפה"
//                }
            }
        }
//        }
        
        
    }
    
    func setDesign(cellNumber : Int, languages : Array<SysTable>) {
    }
    
    func setLanguagesTblState(/*sender:UITapGestureRecognizer*/) {
        languagesTbl.isHidden = !languagesTbl.isHidden
    }
    
    // language table view table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languagesArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        languageNameLbl.text = languagesArray[indexPath.row].nvName
        languageSelectedId = languagesArray[indexPath.row].iId
        setLanguagesTblState()
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath)
        (cell as! FamilyStatusTableViewCell).setDisplayData(txtStatus: languagesArray[indexPath.row].nvName)
        
        return cell
    }
    
    func getLanguageLevelType() -> Int {
        if motherToungeLevelBtn.isSelected {
            return LanguageLevelType.MotherTongue.rawValue
        }
        if goodLevelBtn.isSelected {
            return LanguageLevelType.Good.rawValue
        }
        if mediumLevelBtn.isSelected {
            return LanguageLevelType.Medium.rawValue
        }
        if lowLevelBtn.isSelected {
            return LanguageLevelType.Low.rawValue
        }
        return -1
    }
    
    func setSelectedByLanguageLevelType(languageLevelType : Int) {
        languageSelectedId = languageLevelType
        switch languageLevelType {
        case LanguageLevelType.MotherTongue.rawValue:
            motherToungeLevelBtn.isSelected = true
            goodLevelBtn.isSelected = false
            mediumLevelBtn.isSelected = false
            lowLevelBtn.isSelected = false
            break
        case LanguageLevelType.Good.rawValue:
            motherToungeLevelBtn.isSelected = false
            goodLevelBtn.isSelected = true
            mediumLevelBtn.isSelected = false
            lowLevelBtn.isSelected = false
            break
        case LanguageLevelType.Medium.rawValue:
            motherToungeLevelBtn.isSelected = false
            goodLevelBtn.isSelected = false
            mediumLevelBtn.isSelected = true
            lowLevelBtn.isSelected = false
            break
        case LanguageLevelType.Low.rawValue:
            motherToungeLevelBtn.isSelected = false
            goodLevelBtn.isSelected = false
            mediumLevelBtn.isSelected = false
            lowLevelBtn.isSelected = true
            break
        default:
            break
        }
    }
    
    func setLanguageById(id : Int) {
        for language in languagesArray {
            if language.iId == id {
                languageNameLbl.text = language.nvName
                languageSelectedId = language.iId
                break
            }
        }
    }
    
    //MARK: - BasePopUpActionsDelegates
    func okAction(num: Int) {
        //remove language cell
        removeLanguageDelegate.removeCell!(cellNumber: cellNumber)
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם ברצונך להסיר את השפה?"
        
        openAnotherPageDelegate.openViewController!(VC: basePopUp)
    }
    

}
