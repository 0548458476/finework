//
//  MoreSalaryTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 6.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

protocol saveMoreSalaryDelegate {
    func saveMoreSalary()
}

//הכנסות נוספות לטופס 101
class MoreSalaryTableViewCell: UITableViewCell,saveMoreSalaryDelegate,UITextFieldDelegate/*, SSRadioButtonControllerDelegate*/ {
    
    
    //MARK: - Outlet
    //MARK: -- Views
    
    @IBOutlet weak var viewBottomSeperator: UIView!
    
    @IBOutlet weak var viewBottomTxtOther: UIView!
    @IBOutlet weak var btnMonthSalary: CheckBox!
    
    @IBOutlet weak var btnPartialSalary: CheckBox!
    
    @IBOutlet weak var btnAllowance: CheckBox!
    
    @IBOutlet weak var btnMoreSalary: CheckBox!
    
    @IBOutlet weak var btnWorkerSalary: CheckBox!
    
    @IBOutlet weak var btnScholarship: CheckBox!
    
    @IBOutlet weak var btnOther: CheckBox!
    
    @IBOutlet weak var txtOther: UITextField!
    //MARK: - Actions
    
    @IBAction func MonthSalaryAction(_ sender: CheckBox) {
        btnMonthSalary.isChecked = !btnMonthSalary.isChecked
        /*if sender.isChecked == true{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[0] = WorkerOtherIncome(_iIncomeType: 9, _nvOtherIncomeSource: "")
        }
        else{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[0] = WorkerOtherIncome()
        }*/
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
    }
    
    @IBAction func PartialSalaryAction(_ sender: CheckBox) {
        btnPartialSalary.isChecked = !btnPartialSalary.isChecked
        /*if sender.isChecked == true{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[1] = WorkerOtherIncome(_iIncomeType: 11, _nvOtherIncomeSource: "")
        }
        else{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[1] = WorkerOtherIncome()
        }*/
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
    }
    
    @IBAction func allowanceAction(_ sender: CheckBox) {
        btnAllowance.isChecked = !btnAllowance.isChecked
        /*if sender.isChecked == true{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[2] = WorkerOtherIncome(_iIncomeType: 13, _nvOtherIncomeSource: "")
        }
        else{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[2] = WorkerOtherIncome()
        }*/
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func MoreSalaryAction(_ sender: CheckBox) {
        btnMoreSalary.isChecked = !btnMoreSalary.isChecked
        /*if sender.isChecked == true{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[3] = WorkerOtherIncome(_iIncomeType: 10, _nvOtherIncomeSource: "")
        }
        else{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[3] = WorkerOtherIncome()
        }*/
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
    }
    @IBAction func workerSalaryAction(_ sender: CheckBox) {
        btnWorkerSalary.isChecked = !btnWorkerSalary.isChecked
        /*if sender.isChecked == true{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[4] = WorkerOtherIncome(_iIncomeType: 12, _nvOtherIncomeSource: "")
        }
        else{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[4] = WorkerOtherIncome()
        }*/
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func scholarshipAction(_ sender: CheckBox) {
        btnScholarship.isChecked = !btnScholarship.isChecked
        /*if sender.isChecked == true{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[5] = WorkerOtherIncome(_iIncomeType: 14, _nvOtherIncomeSource: "")
        }
        else{
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes[5] = WorkerOtherIncome()
        }*/
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    @IBAction func btnOtherAction(_ sender: CheckBox) {
        
        btnOther.isChecked = !btnOther.isChecked
        txtOther.isHidden = !btnOther.isChecked
        viewBottomTxtOther.isHidden = txtOther.isHidden
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
    }
    //MARK: - Variables
    //    var radioButtonController: SSRadioButtonsController?
    var delegateTextField:getTextFieldDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    
    
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        GlobalEmployeeDetails.sharedInstance.moreSalaryTableViewCell = self
        txtOther.delegate = self
        setDesign()
        
        //        radioButtonController = SSRadioButtonsController(buttons: btnMonthSalary, btnPartialSalary, btnAllowance, btnMoreSalary, btnWorkerSalary, btnScholarship)
        //        radioButtonController!.delegate = self
        //        radioButtonController!.shouldLetDeSelect = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDisplayData()
    {
        txtOther.isHidden = true
        viewBottomTxtOther.isHidden = true
        for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes
        {
            if item.iIncomeType != 0
            {
                switch item.iIncomeType {
                case 9:
                    btnMonthSalary.isChecked = true
                    break
                case 10:
                    btnMoreSalary.isChecked = true
                    break
                case 11:
                    btnPartialSalary.isChecked = true
                    break
                case 12:
                    btnWorkerSalary.isChecked = true
                    break
                case 13:
                    btnAllowance.isChecked = true
                    break
                case 14:
                    btnScholarship.isChecked = true
                    break
                case 15:
                    btnOther.isChecked = true
                    txtOther.text = item.nvOtherIncomeSource
                    txtOther.isHidden = false
                    viewBottomTxtOther.isHidden = false
                    break
                default:
                    break
                }
            }
        
        }
        
        //btnMonthSalary.isChecked = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
        setFont()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
    }
    func setImages()
    {
        
    }
    func setBorders()
    {
    }
    func setTextcolor()
    {
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
    }
    
    func setIconFont()
    {
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }
    
    //MARK: - Save Data
    
    func saveMoreSalary()
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes = []
        if btnMonthSalary.isChecked == true
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes.append(WorkerOtherIncome(_iIncomeType: 9, _nvOtherIncomeSource: ""))
        }
        if btnPartialSalary.isChecked == true
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes.append(WorkerOtherIncome(_iIncomeType: 11, _nvOtherIncomeSource: ""))
        }
        if btnAllowance.isChecked == true
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes.append(WorkerOtherIncome(_iIncomeType: 13, _nvOtherIncomeSource: ""))
        }
        if btnMoreSalary.isChecked == true
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes.append(WorkerOtherIncome(_iIncomeType: 10, _nvOtherIncomeSource: ""))
        }
        if btnWorkerSalary.isChecked == true
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes.append(WorkerOtherIncome(_iIncomeType: 12, _nvOtherIncomeSource: ""))
        }
        if btnScholarship.isChecked == true
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes.append(WorkerOtherIncome(_iIncomeType: 14, _nvOtherIncomeSource: ""))
        }
        if btnOther.isChecked == true //&& txtOther.text != ""//GlobalEmployeeDetails.sharedInstance.otherIncome != ""
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes.append(WorkerOtherIncome(_iIncomeType: 15, _nvOtherIncomeSource: /*GlobalEmployeeDetails.sharedInstance.otherIncome*/txtOther.text!))
        }
    }
    //MARK: - textFields
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == txtOther
//        {
//            //GlobalEmployeeDetails.sharedInstance.otherIncome = txtOther.text!
//        }
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
//    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let text = textField.text else { return true }
//        let newLength = text.characters.count + string.characters.count - range.length
//        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
//        return newLength <= limitLength // Bool
        return true
    }
    

}
