//
//  MyAccountEmployeeTableViewCell.swift
//  FineWork
//
//  Created by User on 28.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class MyAccountEmployeeTableViewCell: UITableViewCell {

    @IBAction func btnOpenAction(_ sender: Any) {
    }
    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var lblText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setDisplayData(text:String)
    {
        btnOpen.setTitle("\u{f141}",for: .normal)
       lblText.text = text
    }

}
