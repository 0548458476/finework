//
//  JobsToEmployeeTableViewCell.swift
//  FineWork
//
//  Created by User on 28.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class ActiveJobsToEmployeeTableViewCell: UITableViewCell {
    
   

  
    //MARK:- Outlet

    //MARK:-- Buttons
    
    
    @IBOutlet weak var btnStarLFavorite: UIButton!
    
    //    @IBOutlet weak var btnApplyForCandidate: UIButton!
    
    //MARK:-- Actions
    
    @IBAction func detailsBussiness(_ sender: Any) {
        jobsToEmployeeDelegates.openEmployerDatailsDel(index: indexPath!)
    }
    
    @IBAction func detailsJob(_ sender: Any) {
        jobsToEmployeeDelegates.openJobDatailsDel(index: indexPath!)
    }
    @IBAction func satrFavorite(_ sender: Any) {
        jobsToEmployeeDelegates.startFavoraiteDel(index: indexPath!)
    }
    
    //    @IBAction func btnApplyForCandidateAction(_ sender: Any) {
    //    }
    
    
    //    @IBAction func btnStarLFavoriteAction(_ sender: Any) {
    //        if btnStarLFavorite.titleLabel?.textColor == UIColor.black
    //        {
    //            btnStarLFavorite.setTitle("\u{f005}",for: .normal)
    //            btnStarLFavorite.setTitleColor(ControlsDesign.sharedInstance.colorTurquoise, for: .normal)
    //        }
    //        else{
    //            btnStarLFavorite.setTitle("\u{f006}",for: .normal)
    //            btnStarLFavorite.setTitleColor(UIColor.black,for: .normal)
    //        }
    //
    //    }
    //MARK:-- Images
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    //MARK:-- Labels
    
    @IBOutlet weak var lblJobName: UILabel!
    
    @IBOutlet weak var lblJobAddress: UILabel!
    
    @IBOutlet weak var lblJobStatus: UILabel!
    
    @IBOutlet weak var lblNum: UILabel!
    
    @IBOutlet weak var timeJob: UILabel!
    @IBOutlet weak var dateJob: UILabel!
    @IBOutlet weak var viewNumberNotification: UIView!
    @IBOutlet weak var timeLeft: UILabel!
    //MARK: - Variables
     var  jobsToEmployeeDelegates : JobsToEmployeeDelegates!
    var isDownloadImg = false
    var indexPath : IndexPath? = nil
    var seconds = 1
    var secondTimer : Timer?
    var workerOfferedJob = WorkerOfferedJob()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        seconds = 1
    }
    
    func imageTapped1(){
        print("ff")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        imgLogo.layer.cornerRadius = imgLogo.frame.height / 2
        imgLogo.layer.masksToBounds = true
    }
    
    func setDisplayData(workerOfferJob : WorkerOfferedJob?)
    {
        // btnStarLFavorite.setTitle("\u{f006}",for: .normal)
        
        if workerOfferJob != nil/* && lblJobName.text! != workerOfferJob!.nvEmployerName && lblJobAddress.text! != workerOfferJob!.nvAddress*/ {
            //            isDownloadImg = true
            self.workerOfferedJob = workerOfferJob!
            
            imgLogo.image = workerOfferJob?.logoImage
            //            if workerOfferJob!.nvLogoImageFilePath == "" {
            //
            //
            //            } else {
            //                var stringPath = api.sharedInstance.buldUrlFile(fileName: (workerOfferJob!.nvLogoImageFilePath))
            //                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
            //
            //                //            imgLogo.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
            //                if let url = URL(string: stringPath) {
            //                    downloadImage(url: url)
            //                }
            //            }
            
            //            if workerOfferJob?.logoImage != nil {
            //                imgLogo = workerOfferJob?.logoImage
            //                imgLogo.contentMode = .scaleToFill
            //            }
            
            
            if workerOfferJob!.bFavorite
            {
                print("rrr Favorite")
                btnStarLFavorite.setTitle("\u{f005}",for: .normal)
                btnStarLFavorite.setTitleColor(UIColor.orange2, for: .normal)
            }
            else{
                print("rrr No Favorite")
                
                btnStarLFavorite.setTitle("\u{f006}",for: .normal)
                btnStarLFavorite.setTitleColor(UIColor.black,for: .normal)
            }
            
            lblJobName.text = workerOfferJob!.nvEmployerName
            lblJobAddress.text = workerOfferJob!.nvMatchDescription
            lblJobStatus.text = workerOfferJob!.nvOfferJobStatusName
            
            if workerOfferJob!.iBaloonOccur > 0 {
                viewNumberNotification.isHidden = false
                lblNum.isHidden = false
                lblNum.text = String(workerOfferJob!.iBaloonOccur)
            } else {
                lblNum.isHidden = true
                viewNumberNotification.isHidden = true

            }
        }
        
        
        lblNum.layer.cornerRadius = lblNum.frame.width/2
        lblNum.layer.masksToBounds = true
        timeLeft.text = workerOfferJob!.nvRole
        ///
        if workerOfferJob!.lWorkerOfferedJobTimeAvailablity.count > 0 {
        let dateFormat = Global.sharedInstance.getStringFromDateString(dateString: workerOfferJob!.lWorkerOfferedJobTimeAvailablity[0].dFromDate)
        let dateSplit = dateFormat.components(separatedBy: "/")
        dateJob.text = dateSplit[0] + "/" + dateSplit[1]
        timeJob.text = workerOfferJob!.lWorkerOfferedJobTimeAvailablity[0].tFromHoure + "-" + workerOfferJob!.lWorkerOfferedJobTimeAvailablity[0].tToHoure
        }
        ///
//        if  workerOfferJob!.lWorkerOfferedJobTimeAvailablity.count > 0 && workerOfferedJob.dtWorkerCandidacyExpiration != ""{
//            setTimer()
//            //timeLeft.text = String(getHourDifferencesToday(serverDate: workerOfferedJob.dtWorkerCandidacyExpiration))
//        }else{
//            timeLeft.text = String("")
//
//        }
    }
  
    
    func setTimer() {
        seconds = 1
        if secondTimer != nil{
            secondTimer?.invalidate()
            //secondTimer = nil
        }
        
        // secondTimer?.invalidate()
        secondTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
    }
    
    //method to update your UI
    func updateUI() {
        //update your UI here
        
        //also update your seconds variable value by 1
        seconds += 1
        
      //  let s = Global.sharedInstance.getStringDiffBetweenCurrentDate(date: workerOfferedJob.dtWorkerCandidacyExpiration, time: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].tFromHoure)
       // timeLeft.text = String(s)
    
        if workerOfferedJob.dtWorkerCandidacyExpiration != "" {
        timeLeft.text = String(Global.sharedInstance.getHourDifferencesToday(serverDate: workerOfferedJob.dtWorkerCandidacyExpiration))
        }
        //print(s)
    }
    func downloadImage(url: URL) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    if let tbl = self.superview?.superview as? UITableView {
                        if self.indexPath != nil && tbl.cellForRow(at: self.indexPath!) != nil {
                            self.imgLogo.contentMode = .scaleToFill
                            self.imgLogo.image = image
                        }
                    }
                }
            }
        }
    }
    func imageTapped(sender:UIView){
        //openDel.openEmployerDatailsDel(index: "1")
    }
}
