//
//  Reason101ResidentPlaceTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 20.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
//נפתח מסל: אני תושב קבוע
class Reason101ResidentPlaceTableViewCell: UITableViewCell,UITextFieldDelegate,SetDateTextFieldTextDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate, UploadFilesDelegate {

  //MARK: - Outlet
    //MARK: -- Labels
    
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblNamePlace: UILabel!
      @IBOutlet weak var lblAddFileTitle: UILabel!
    
    //MARK: -- TextField
    
    @IBOutlet weak var txtNamePlace: UITextField!
    @IBOutlet weak var txtFromDate: UITextField!
    
    //MARK: - views
    @IBOutlet weak var viewFromDate: UIView!
    
    
    //MARK: -- Button
    
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var addOkPermission: UIButton!
    @IBOutlet weak var btnCancelAddFile: UIButton!
    
    //MARK: -- Action
    
    @IBAction func btnFromDateAction(_ sender: Any) {
    }
    @IBAction func addOkPermissionAction(_ sender: Any) {
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileURL)
//        
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileURL != ""//מהשרת ויש קובץ
//        {
////            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileURL) as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            if let url = NSURL(string: urlStr as String) {
////                if let data = NSData(contentsOf: url as URL) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            //                    controller?.img = UIImage(data: data as Data)
//            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileURL
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                }
////            }
//        }
//        else
//        {
//            showCameraActionSheet()
//        }
    }
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
           Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
        
           }
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var openDatePickerDelegate:OpenDatePickerDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    var indexInArr = 0
    let dateFormatter = DateFormatter()
    let reasonId = 3
    
    var citiesName = [String]()
    var citiesFilter = [String]()
    var keyboardHight:CGFloat = 0.0
    
    let screenHeight = UIScreen.main.bounds.height
    
  //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        self.delegateTextField = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        
        self.openDatePickerDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        
        ///----
//        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        ///----
        
        txtFromDate.delegate = self
        txtNamePlace.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextField(sender:)))
        viewFromDate.addGestureRecognizer(tap)
        
        setDesign()
        
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.delegate = self
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.dataSource = self
        
         txtNamePlace.addTarget(self, action: #selector(Reason101ResidentPlaceTableViewCell.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setDisplayData()
    {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = true
        
        txtFromDate.text =
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent1
        txtNamePlace.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent2
        
        //במקרה רגיל
        addOkPermission.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
         addOkPermission.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        btnCancelAddFile.isHidden = true
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileURL != ""//מהשרת ויש קובץ
        {
            addOkPermission.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            addOkPermission.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileName, for: .normal)
            btnCancelAddFile.isHidden = false
            addOkPermission.setTitleColor(UIColor.lightGray, for: .normal)
        }
//        else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף אישור רשות״
//        {
//            addOkPermission.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_FOR_RESIDENT_PLACE, comment: ""), for: .normal)
//            btnCancelAddFile.isHidden = true
//        }
        
        
        //בדיקה האם קוד הסיבה קיימת במערך
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 3
            {
                indexInArr = i
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileURL != ""//מהשרת ויש קובץ
                {
                    addOkPermission.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
                {
                    //הצגת שם הקובץ
                    addOkPermission.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName, for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף אישור רשות״
                {
                    addOkPermission.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_FOR_RESIDENT_PLACE, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = true
                }
                break
            }
        }*/
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setFont()
        setTextcolor()
        setCornerRadius()
        setBorders()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
        txtNamePlace.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        txtFromDate.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
        
    }
    func setBorders()
    {
        //set border to button
        addOkPermission.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        addOkPermission.layer.borderWidth = 0.5
        
        
        //set border to textFields
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtNamePlace)
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtFromDate)
    }
    func setTextcolor()
    {
        addOkPermission.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
    }
    
    func setCornerRadius()
    {
        addOkPermission.layer.cornerRadius = addOkPermission.frame.height/2
    }
    
    func setIconFont()
    {
        btnFromDate.setTitle(FlatIcons.sharedInstance.CALANDER, for: .normal)
        
        if Global.sharedInstance.rtl == true
        {
            txtFromDate.textAlignment = .right
        }
        else
        {
            txtFromDate.textAlignment = .left
        }
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_FOR_RESIDENT_PLACE, comment: "")
        addOkPermission.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
    }
    
    //MARK: - textField
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
//        return true
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let originY = GlobalEmployeeDetails.sharedInstance.arrReasonsPossision[reasonId]{
            delegateTextField.getTextField(textField: textField, originY: originY)
            
            cellOriginY = originY
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = true
            print("Hidden\n")
            if textField == txtNamePlace{
                registerKeyboardNotifications()
                //                extTbl_contentOffset_y = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.contentOffset.y
                //                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: 300, width: 150, height: 240)
                
                //                 GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: cellOriginY+txtNamePlace.frame.origin.y-255-extTbl_contentOffset_y!, width: txtNamePlace.frame.size.width, height: 240)
                
                citiesName = Array(Global.sharedInstance.citiesArr.keys) as [String]
                citiesName.sort{$0.compare($1) == .orderedAscending }
                citiesFilter = citiesName
                
                //                 GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = false
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.reloadData()
                
                
            }
        }
//        if GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: "")] != nil
//        {
//            delegateTextField.getTextField(textField: textField, originY: GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: "")]!)
//        }
//        else
//        {
//            delegateTextField.getTextField(textField: textField, originY: 1420)
//        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtNamePlace{
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = true
            print("Hidden\n")
            unregisterKeyboardNotifications()
            let cityName = textField.text
            var cityId = -1
            if cityName != ""{
                for (key, value) in Global.sharedInstance.citiesArr
                {
                    if key == cityName{
                        cityId = value
                    }
                }
            }
            if cityId == -1{
                textField.text = ""
            }else{
                saveData(textField: textField)
            }
        }else{

        saveData(textField: textField)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
    func saveData(textField:UITextField)
    {
        if textField == txtNamePlace
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent2 = txtNamePlace.text!
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        searching(searchText: textField.text!)
    }
    
    func searching(searchText: String) {
        //let s:CGFloat = view.frame.size.height - tableView.frame.origin.x
        
        citiesFilter = citiesName.filter{ term in
            return term.hasPrefix(searchText)
        }
        //        if searchText == ""
        //        {
        //            //            filtered =  pointsArr
        //            //print("count of list in searchin \(vertexsArrayDescription.count)")
        //
        //            citiesFilter =  citiesName
        //        }
        //        else{
        //            citiesFilter = citiesFilter.filter{ term in
        //                return term.hasPrefix(searchText)
        //            }
        //        }
        if(citiesFilter.count == 0){
            //            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = true
            citiesName.sort{$0.compare($1) == .orderedAscending }
            //            citiesName.sort{$0.compare($1) == .orderedDescending }
        }
        else{
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = false
            print("Not Hidden\n")
        }
        //        tblCities.reloadData()
        //        tabelPoints.reloadData()
        uploadTable()
        
    }
    
    func uploadTable(){
        //        let size:CGSize = CGSize(width: sized, height:(CGFloat)(filtered.count * 50))
        //        self.tableView.frame = CGRectMake(orx, ory, size.width, size.height)
        //tableView.center.x = basicView.frame.size.width  / 2
        
        if citiesFilter.count < 7
        {
            //             GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: cellOriginY+txtNamePlace.frame.origin.y-255-extTbl_contentOffset_y!, width: txtNamePlace.frame.size.width, height: 240)
            
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: screenHeight - keyboardHight - txtNamePlace.frame.size.height - (CGFloat)(40 * citiesFilter.count), width: txtNamePlace.frame.size.width, height: (CGFloat)(40 * citiesFilter.count))
            //            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: cellOriginY+txtNamePlace.frame.origin.y-(CGFloat)(40 * citiesFilter.count)-extTbl_contentOffset_y!-15, width: txtNamePlace.frame.size.width, height: (CGFloat)(40 * citiesFilter.count))
            //          GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: cellOriginY+txtNamePlace.frame.origin.y-(CGFloat)(40 * citiesFilter.count), width: 150, height: (CGFloat)(40 * citiesFilter.count))
        }
            
        else {
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: screenHeight - keyboardHight - txtNamePlace.frame.size.height - 240, width: txtNamePlace.frame.size.width, height: 240)
            //             GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: cellOriginY+txtNamePlace.frame.origin.y-255-extTbl_contentOffset_y!, width: txtNamePlace.frame.size.width, height: 240)
            //             GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: cellOriginY+txtNamePlace.frame.origin.y-240, width: 150, height: 240)
        }
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.reloadData()
    }
    
    

    
    
    func tapDatesTextField(sender:UIView)//fromDate - recidect place
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 7, dateToShow: txtFromDate.text!)
    }
    
    
    func SetDateTextFieldText(tagOfTextField:Int)
    {
        if tagOfTextField == 7//fromDate - recidect place
        {
             txtFromDate.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent1
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //            return Global.sharedInstance.citiesArr.count
        return citiesFilter.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath)
        //            (cell as! FamilyStatusTableViewCell).setTitleText(text: Array(Global.sharedInstance.citiesArr.keys)[indexPath.row])
        (cell as! FamilyStatusTableViewCell).setTitle(text: citiesFilter[indexPath.row])
        //                tblCities.setContentOffset(CGPoint(x:0, y:CGFloat(FLT_MAX)), animated: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtNamePlace.text = citiesFilter[indexPath.row]
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = true
        print("Hidden\n")
        saveData(textField: txtNamePlace)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }


    //MARK: - camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
                //do whatever with your file name
                //                self.changeBtnDesign(imgName: imageName!,img: tempImage)
//                picker.dismiss(animated: true, completion: nil);
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }

    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFile = img
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileName = imgName
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 3
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFile = Global.sharedInstance.setImageToString(image: img)
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName = imgName
                break
            }
        }*/
        addOkPermission.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
        addOkPermission.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            addOkPermission.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            addOkPermission.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
            btnCancelAddFile.isHidden = true
            
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFile = ""
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileName = ""
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].fileObj.iFileId = -1
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true
        }
    }
    
    
    //MARK: - keyBoard notifications
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        //        if didFirstDelegate{
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (userInfo.object(forKey: UIKeyboardFrameBeginUserInfoKey)! as AnyObject).cgRectValue.size
        keyboardHight = keyboardSize.height
        
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.frame = CGRect(x: txtNamePlace.frame.origin.x, y: screenHeight - keyboardHight - txtNamePlace.frame.size.height - 240, width: txtNamePlace.frame.size.width, height: 240)
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = false
        print("Not Hidden\n")
    }
    
    func keyboardWillHide(notification: NSNotification) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.tblCity.isHidden = true
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        self.changeBtnDesign(imgName: imgName, img: img)
    }

}
