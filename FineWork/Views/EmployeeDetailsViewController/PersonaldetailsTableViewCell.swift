//
//  PersonaldetailsTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 5.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
import Darwin

enum FamilyStatusType: String {
    
    case Single = "רווק/ה"
    case Married = "נשוי/אה"
    case Divorcee = "גרוש/ה"
    case Widow = "אלמן/ה"
    case Separated = "פרוד/ה"
}

@objc protocol SetDateTextFieldTextDelegate {
    @objc optional func SetDateTextFieldText(tagOfTextField:Int)
    @objc optional func SetDateFieldText(dateAsString : String, tagOfField : Int)

}



class PersonaldetailsTableViewCell: UITableViewCell,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,SetDateTextFieldTextDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate, UploadFilesDelegate {

//MARK: - Outlet
    //MARK: -- Buttons
    
    @IBOutlet weak var btnIsraelResident: CheckBox!
    
    @IBOutlet weak var btnOkAssessor: UIButton!
    
    @IBOutlet weak var btnCancelAddFile: UIButton!
    
    //MARK: -- TextFields
    
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var txtFirstName: UITextField!
    
    @IBOutlet weak var txtFamilyStatus: UITextField!
    
    @IBOutlet weak var txtTzPassport: UITextField!
    
    @IBOutlet weak var txtDateImmigration: UITextField!
    
    @IBOutlet weak var txtDateBorn: UITextField!
    
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var txtNum: UITextField!
    
    @IBOutlet var txtNumChildernUnder18: UITextField!
    
    @IBOutlet weak var txtPhone: PhonTextField!
    
    //MARK: -- Labels
    
    @IBOutlet weak var lblIconOpenFamilyStatus: UILabel!
    
    @IBOutlet weak var lblIconFontDateImmigration: UILabel!
    
    @IBOutlet weak var lblIconFontDateBorn: UILabel!
    
    @IBOutlet weak var lblTitlePage: UILabel!
    
    @IBOutlet weak var lblExplain: UILabel!
    
    @IBOutlet weak var lblLastName: UILabel!
    
    @IBOutlet weak var lblFirstName: UILabel!
    
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var lblSatusFamily: UILabel!
    
    @IBOutlet weak var lblTzPassport: UILabel!
    
    @IBOutlet weak var lblIsraeliResident: UILabel!
    
    @IBOutlet weak var lblDateImmigration: UILabel!
    
    @IBOutlet weak var lblDateBorn: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var lblNumChildUnder18: UILabel!
    
    @IBOutlet weak var lblAddFileTitle: UILabel!

    //MARK: -- TableView
    
    @IBOutlet weak var tblFamilyStatus: UITableView!
    
    @IBOutlet weak var tblCities: UITableView!
    //MARK: -- SegmentControl
    
    @IBOutlet weak var segGender: UISegmentedControl!
    
    //MARK: -- Views
    
    @IBOutlet weak var viewFamilyStatus: UIView!

    @IBOutlet weak var viewBottomSeperator: UIView!
    
    @IBOutlet weak var viewTapTxtDateBorn: UIView!
    @IBOutlet weak var viewTapTxtImmegrantDate: UIView!
    
    //MARK: -- Action

    @IBAction func segGenderClick(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0//זכר
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.iGenderType = 5
        }
        else//נקבה
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.iGenderType = 6
        }
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        GlobalEmployeeDetails.sharedInstance.ReloadReasons()
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    @IBAction func btnIsraelResidentAction(_ sender: Any) {
        btnIsraelResident.isChecked = !btnIsraelResident.isChecked
        GlobalEmployeeDetails.sharedInstance.worker101Form.bResident = btnIsraelResident.isChecked
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnOkAssessorAction(_ sender: Any) {
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileURL)
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileURL != ""//מהשרת ויש קובץ
//        {
////            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileURL) as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            if let url = NSURL(string: urlStr as String) {
////                if let data = NSData(contentsOf: url as URL) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            //                    controller?.img = UIImage(data: data as Data)
//            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileURL
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                }
////            }
//        }
//        else
//        {
//            showCameraActionSheet()
//        }
    }
    
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
        Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
    }
    
    //MARK: - constrain
    
    @IBOutlet weak var conHeightTblCities: NSLayoutConstraint!
    
    //MARK: - Variables
    
    //    var arrStatus = ["רווק/ה","נשוי/אה","גרוש/ה","אלמן/ה","פרוד/ה"]
    var arrStatus = [FamilyStatusType.Single,FamilyStatusType.Married,FamilyStatusType.Divorcee,FamilyStatusType.Widow,FamilyStatusType.Separated]
    var enumFamilyStausValues = [1,2,3,4,21]
    var indexTblStatusFamily = 0
    var openDatePickerDelegate:OpenDatePickerDelegate!=nil
    var delegateTextField:getTextFieldDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    let dateFormatter = DateFormatter()

    var citiesName = [String]()
    var citiesFilter = [String]()
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        var numRows = tableView(tblCities, numberOfRowsInSection: 0)
//        var contentInsetTop = tblCities.bounds.size.height
//        for i in 0 ..< numRows {
//            contentInsetTop -= 40
//            if contentInsetTop <= 0 {
//                contentInsetTop = 0
//            }
//        }
//        tblCities.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0)
        
//        tblCities.transform = CGAffineTransform.init(rotationAngle: (-(CGFloat)(M_PI)))
//        tblCities.transform = CGAffineTransform.init(translationX: -view.frame.with, y: view.frame.hight)
        let tapFamilyStatus: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openFamilyStatus))
        self.viewFamilyStatus.addGestureRecognizer(tapFamilyStatus)
        tapFamilyStatus.cancelsTouchesInView = false
        
        txtCity.addTarget(self, action: #selector(PersonaldetailsTableViewCell.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)

        
        //call to design the controls
        setDesign()
        
        self.contentView.bringSubview(toFront: tblFamilyStatus)
        self.contentView.bringSubview(toFront: tblCities)
        
        if GlobalEmployeeDetails.sharedInstance.familyStatus == FamilyStatusType.Separated.rawValue
        {
            btnOkAssessor.isHidden = false
        }
        else
        {
            btnOkAssessor.isHidden = true
        }
        lblAddFileTitle.isHidden = btnOkAssessor.isHidden
        
        tblFamilyStatus.isHidden = true
        tblFamilyStatus.delegate = self
        tblFamilyStatus.dataSource = self
        
        tblCities.isHidden = true
        tblCities.delegate = self
        tblCities.dataSource = self
        
        //initial delegate for textFields
        txtLastName.delegate = self
        txtFirstName.delegate = self
        txtFamilyStatus.delegate = self
        txtTzPassport.delegate = self
        txtDateImmigration.delegate = self
        txtDateBorn.delegate = self
        txtCity.delegate = self
        txtAddress.delegate = self
        txtNum.delegate = self
        //txtNumChildernUnder18.delegate = self
        txtPhone.delegate = self
        
        txtLastName.returnKeyType = UIReturnKeyType.next
        txtFirstName.returnKeyType = UIReturnKeyType.next
        txtTzPassport.returnKeyType = UIReturnKeyType.next
        txtCity.returnKeyType = UIReturnKeyType.next
        txtAddress.returnKeyType = UIReturnKeyType.next
        txtNum.returnKeyType = UIReturnKeyType.next
        txtPhone.returnKeyType = UIReturnKeyType.next
        
        txtDateBorn.isUserInteractionEnabled = false
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextField(sender:)))
        viewTapTxtDateBorn.addGestureRecognizer(tap)
        
        let tapImmegrantDate: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextFieldImmegrantDate(sender:)))
        viewTapTxtImmegrantDate.addGestureRecognizer(tapImmegrantDate)
        
        //hide keyBoard when touch background
        self.hideKeyboardWhenTappedAround()
        
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        //set default for family status(רווקה)

        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 0
        {
            indexTblStatusFamily = 0
            txtFamilyStatus.text = arrStatus[0].rawValue
            GlobalEmployeeDetails.sharedInstance.familyStatus = arrStatus[0].rawValue
            GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType = enumFamilyStausValues[indexTblStatusFamily]
        }
      
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_CONFIRMATION_OF_SEPERATE, comment: "")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setDisplayData()
    {
        txtFirstName.text = GlobalEmployeeDetails.sharedInstance.worker101Form.nvFirstName
        txtLastName.text = GlobalEmployeeDetails.sharedInstance.worker101Form.nvLastName
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iGenderType == 5//זכר
        {
            segGender.selectedSegmentIndex = 0
        }
        else//6=נקבה
        {
            segGender.selectedSegmentIndex = 1
        }
        btnIsraelResident.isChecked = GlobalEmployeeDetails.sharedInstance.worker101Form.bResident
        txtTzPassport.text = GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber
        
        var cityName = ""
        for (key, value) in Global.sharedInstance.citiesArr
        {
            if value == GlobalEmployeeDetails.sharedInstance.worker101Form.iCityId{
                cityName = key
            }
        }

        txtCity.text = cityName
        
        txtAddress.text = GlobalEmployeeDetails.sharedInstance.worker101Form.nvStreet
        txtNum.text = String(GlobalEmployeeDetails.sharedInstance.worker101Form.nvHouseNumber)
        txtPhone.text = GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone
        txtPhone.editText()
        
        for var i in 0 ..< enumFamilyStausValues.count
        {
            if enumFamilyStausValues[i] == GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType
            {
                txtFamilyStatus.text = arrStatus[i].rawValue
                GlobalEmployeeDetails.sharedInstance.familyStatus = arrStatus[i].rawValue
                break
            }
        }
        
        if GlobalEmployeeDetails.sharedInstance.familyStatus == FamilyStatusType.Separated.rawValue
        {
            btnOkAssessor.isHidden = false
        }
        else
        {
            btnOkAssessor.isHidden = true
        }
        lblAddFileTitle.isHidden = btnOkAssessor.isHidden
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate != nil
        {
            txtDateBorn.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate as! Date)
        }
        else
        {
            txtDateBorn.text = ""
        }
        
        if (GlobalEmployeeDetails.sharedInstance.worker101Form.dImmigrationDate != nil)
        {
            
            txtDateImmigration.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.dImmigrationDate as! Date)
        }
        else
        {
            txtDateImmigration.text = ""
        }
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnOkAssessor.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
            btnOkAssessor.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnOkAssessor.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileName, for: .normal)
            btnCancelAddFile.isHidden = false
            btnOkAssessor.setTitleColor(UIColor.lightGray, for: .normal)
        }
        else// אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף אישור פקיד שומה״
        {
//            btnOkAssessor.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_CONFIRMATION_OF_SEPERATE, comment: ""), for: .normal)
            btnOkAssessor.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = true
            btnOkAssessor.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        }
    }
    
    //MARK: - TextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.endEditing(true)
        if textField != txtCity{
        if textField == txtNum{//בדיקה שלא יהיה ערך ריק בשדה מס׳ שאז הוא קורס כי הוא לא יכול להמיר
            if textField.text != ""{
                saveData(textField: textField)
            }else{
                GlobalEmployeeDetails.sharedInstance.worker101Form.nvHouseNumber = ""
            }
        }
        else{
            saveData(textField: textField)
        }
        }else{
            self.tblCities.isHidden = true
            let cityName = textField.text
            var cityId = -1
            if cityName != ""{
            for (key, value) in Global.sharedInstance.citiesArr
            {
                if key == cityName{
                    cityId = value
                }
            }
            }
            GlobalEmployeeDetails.sharedInstance.worker101Form.iCityId = cityId
            if cityId == -1{
               textField.text = ""
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        delegateTextField.getTextField(textField: textField, originY: cellOriginY)
        
        //הצגת מקלדת מתאימה לתעודת זהות בהתאם למה שמסומן בכפתור ״תושב ישראל״
        if textField == txtTzPassport
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.bResident == true//תושב ישראל
            {
                textField.keyboardType = .numberPad
            }
            else
            {
                textField.keyboardType = .numbersAndPunctuation
                
            }
        }
        
        if textField == txtFamilyStatus
        {
            dismissKeyboard()
            tblFamilyStatus.isHidden = false
            self.tblCities.isHidden = true
        }
        else
        {
            if textField == txtCity{
//                dismissKeyboard()
                tblCities.isHidden = false
            }
            else{
                tblCities.isHidden = true
            }
            tblFamilyStatus.isHidden = true
        }
        if textField == txtDateBorn
        {
            dismissKeyboard()
            openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 1, dateToShow: txtDateBorn.text!)
        }
        
        if textField == txtDateImmigration
        {
            dismissKeyboard()
            openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 2, dateToShow: txtDateImmigration.text!)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtFirstName:
            txtLastName.becomeFirstResponder()
        case txtLastName:
            txtTzPassport.becomeFirstResponder()
        case txtTzPassport:
            txtCity.becomeFirstResponder()
        case txtCity:
            txtAddress.becomeFirstResponder()
        case txtAddress:
            txtNum.becomeFirstResponder()
        case txtNum:
            txtPhone.becomeFirstResponder()
        case txtPhone:
            txtAddress.becomeFirstResponder()
        default:
            txtFirstName.resignFirstResponder()
        }
        return false
//        textField.resignFirstResponder()
//        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        searching(searchText: textField.text!)
    }
    
    func searching(searchText: String) {
        //let s:CGFloat = view.frame.size.height - tableView.frame.origin.x
        
        citiesFilter = citiesName.filter{ term in
            return term.hasPrefix(searchText)
        }
//        if searchText == ""
//        {
//            //            filtered =  pointsArr
//            //print("count of list in searchin \(vertexsArrayDescription.count)")
//            
//            citiesFilter =  citiesName
//        }
//        else{
//            citiesFilter = citiesFilter.filter{ term in
//                return term.hasPrefix(searchText)
//            }
//        }
        if(citiesFilter.count == 0){
            self.tblCities.isHidden = true
            citiesName.sort{$0.compare($1) == .orderedAscending }
//            citiesName.sort{$0.compare($1) == .orderedDescending }
        }
        else{
            self.tblCities.isHidden = false
        }
//        tblCities.reloadData()
        //        tabelPoints.reloadData()
        uploadTable()
        
    }
    
    func uploadTable(){
        //        let size:CGSize = CGSize(width: sized, height:(CGFloat)(filtered.count * 50))
        //        self.tableView.frame = CGRectMake(orx, ory, size.width, size.height)
        //tableView.center.x = basicView.frame.size.width  / 2
        
        if citiesFilter.count < 7
        {
            conHeightTblCities.constant = (CGFloat)(40 * citiesFilter.count)
        }
            
        else {
            conHeightTblCities.constant = 240
        }
        self.tblCities.reloadData()
    }
    
    // MARK: - KeyBoard
    
    func dismissKeyboard() {
        self.contentView.endEditing(true)
//        tblFamilyStatus.isHidden = true
        txtDateBorn.resignFirstResponder()
        txtNum.resignFirstResponder()
        txtCity.resignFirstResponder()
        txtPhone.resignFirstResponder()
        txtAddress.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtFirstName.resignFirstResponder()
        txtTzPassport.resignFirstResponder()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        contentView.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblFamilyStatus{
        return arrStatus.count
        }else{
//            return Global.sharedInstance.citiesArr.count
            return citiesFilter.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath)
        if tableView == tblFamilyStatus{
            (cell as! FamilyStatusTableViewCell).setDisplayData(txtStatus: arrStatus[indexPath.row].rawValue)
        }else{
//            (cell as! FamilyStatusTableViewCell).setTitleText(text: Array(Global.sharedInstance.citiesArr.keys)[indexPath.row])
            (cell as! FamilyStatusTableViewCell).setTitleText(text: citiesFilter[indexPath.row])
//                tblCities.setContentOffset(CGPoint(x:0, y:CGFloat(FLT_MAX)), animated: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Global.sharedInstance.bWereThereAnyChanges = true
        
        if tableView == tblFamilyStatus{
        GlobalEmployeeDetails.sharedInstance.isFromChangeFamiliStatus = true

        if arrStatus[indexPath.row].rawValue == FamilyStatusType.Married.rawValue//choose married
        {
            //check if already open
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != /*2*/FamilyStateKey.Married.rawValue
            {
                if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 21
                {
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (85/750))//אם הסטטוס היה פרוד/ה מורידים את הגובה של הכפתור - ׳צירוף אישור׳.
                }
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] += 1
                
                //להוסיף לגובה.partner..
                if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true
                {
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (480/750))
                }
                else
                {
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (320/750))
                }
                
                //בדיקה האם להפחית גובה של הכפתור ׳צרף אישור זכאות׳
                var showWithButton = false
                
                for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
                {
                    if item.bWithMe == true
                    {
                        showWithButton = true
                        break
                    }
                }
                
                if showWithButton == true
                {
                    //שינוי גובה הסל של הוסף ילדים
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (60/750)
                }
            }
        }
        else//choose not married
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2//אם לפני כן הסטטוס היה נשוי/אה
            {
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] -= 1
                
                //אם יש הכנסה נוספת לבן זוג
                if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true
                {
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (480/750))
                }
                else
                {
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (320/750))
                }
            }
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 21//אם הסטטוס הקודם היה פרוד/ה
            {
                if arrStatus[indexPath.row].rawValue != FamilyStatusType.Separated.rawValue//אם נבחר סטטוס שונה מפוד/ה
                {
                    
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (85/750))// מפחיתים את הגובה של הכפתור - ׳צירוף אישור׳.
                }
            }
            else if arrStatus[indexPath.row].rawValue == FamilyStatusType.Separated.rawValue//אם הסטטוס הקודם שונה מפרוד ועכשיו נבחר פרוד
            {
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (85/750))// מוסיפים את הגובה של הכפתור - ׳צירוף אישור׳.
            }
        }

//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        
        txtFamilyStatus.text = arrStatus[indexPath.row].rawValue
        GlobalEmployeeDetails.sharedInstance.familyStatus = arrStatus[indexPath.row].rawValue
        tblFamilyStatus.isHidden = true
        
        indexTblStatusFamily = indexPath.row
        saveData(textField: txtFamilyStatus)
    
//            DispatchQueue.main.async {
//               GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
//            }

            
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2{
            for child in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
            {
                child.bWithMe = false
                child.bGetChildAllowance = false
            }
            }else{
                for child in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
                {
                    child.bWithMe = true
                    child.bGetChildAllowance = true
                }
            }
            DispatchQueue.main.async {
        GlobalEmployeeDetails.sharedInstance.ReloadReasons()
            }
        }else{
//            txtCity.text = Array(Global.sharedInstance.citiesArr.keys)[indexPath.row]
            txtCity.text = citiesFilter[indexPath.row]
            tblCities.isHidden = true
            saveData(textField: txtCity)
        }
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
        setFont()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
       // txtLastName.backgroundColor =
        //segGender.backgroundColor = ControlsDesign.sharedInstance.colorDarkPurple
        //set background of textFields to ControlsDesign.sharedInstance ControlsDesign.sharedInstance.colorWhiteGray
        
    }
    func setFont()
    {
        //set all textFields and kabeks font to RUBIK-REGULAR
        
        //set textField design
        txtNum.attributedPlaceholder = NSAttributedString(string:"מספר", attributes:[NSFontAttributeName :UIFont(name: "RUBIK-REGULAR", size: 16)!])
        
        //        let attr = NSAttributedString(string:"זכר", attributes:[NSFontAttributeName :UIFont(name: "RUBIK-REGULAR", size: 16)!])
        //        UISegmentedControl.appearance().setTitleTextAttributes(attr, for: .normal)
        
        if Global.sharedInstance.rtl == true
        {
            txtDateBorn.textAlignment = .right
            txtDateImmigration.textAlignment = .right
        }
        else
        {
            txtDateBorn.textAlignment = .left
            txtDateImmigration.textAlignment = .left
        }
    }
    func setImages()
    {
        
    }
    func setTextcolor()
    {
        //set to labels
        lblIconFontDateBorn.textColor = ControlsDesign.sharedInstance.colorGray
        lblIconFontDateImmigration.textColor = ControlsDesign.sharedInstance.colorGray
        //set to textFields
    }
    
    func setCornerRadius()
    {
        btnIsraelResident.layer.cornerRadius = 5
        btnOkAssessor.layer.cornerRadius = btnOkAssessor.frame.height/2
        btnOkAssessor.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnOkAssessor.layer.borderWidth = 1
    }
    
    func setIconFont()
    {
         lblIconFontDateBorn.text = FlatIcons.sharedInstance.CALANDER
         lblIconFontDateImmigration.text = FlatIcons.sharedInstance.CALANDER
        lblIconOpenFamilyStatus.text = "\u{f107}"
    }
    
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
//        btnOkAssessor.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_CONFIRMATION_OF_SEPERATE, comment: ""), for: .normal)
        btnOkAssessor.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
    }
    
    //MARK: - SaveData
    
    func saveData(textField:UITextField)
    {
        switch textField {
        case txtLastName:
            GlobalEmployeeDetails.sharedInstance.worker101Form.nvLastName = txtLastName.text!
            break
        case txtFirstName:
            GlobalEmployeeDetails.sharedInstance.worker101Form.nvFirstName = txtFirstName.text!
            break
        case txtFamilyStatus:
            GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType = enumFamilyStausValues[indexTblStatusFamily]
            break
        case txtTzPassport:
            GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber = txtTzPassport.text!
            break
        case txtDateImmigration:
            GlobalEmployeeDetails.sharedInstance.worker101Form.dImmigrationDate = dateFormatter.date(from: txtDateImmigration.text!)! as NSDate
            break
//        case txtDateBorn:
//            GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate = dateFormatter.date(from: txtDateBorn.text!)! as NSDate
//            GlobalEmployeeDetails.sharedInstance.ReloadReasons()
//            break
        case txtCity:
            GlobalEmployeeDetails.sharedInstance.worker101Form.iCityId = Global.sharedInstance.citiesArr[txtCity.text!]!
            break
        case txtAddress:
            GlobalEmployeeDetails.sharedInstance.worker101Form.nvStreet = txtAddress.text!
            break
        case txtNum:
                GlobalEmployeeDetails.sharedInstance.worker101Form.nvHouseNumber = txtNum.text!
            
            break
        case txtPhone:
            GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone = txtPhone.getOriginalText()!
            break
        default:
            break
        }
    }
    
    func SetDateTextFieldText(tagOfTextField:Int)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if tagOfTextField == 1//txtDateBorn
        {
            txtDateBorn.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate as! Date)
            GlobalEmployeeDetails.sharedInstance.ReloadReasons()
        }
        else//txtDateImmigration
        {
            txtDateImmigration.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.dImmigrationDate as! Date)
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func tapDatesTextField(sender:UIView)
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 1, dateToShow: txtDateBorn.text!)
    }
    
    func tapDatesTextFieldImmegrantDate(sender:UIView)
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 2, dateToShow: txtDateImmigration.text!)
    }
    
    //MARK: - camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        
                        //do whatever with your file name
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                } else {
                    picker.dismiss(animated: true, completion: nil)
                }
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }

    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileName = imgName//מחיקת הקובץ מהאוביקט
        GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFile = img
        btnOkAssessor.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
        btnOkAssessor.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func openFamilyStatus()
    {
        dismissKeyboard()
        tblFamilyStatus.isHidden = false
        delegateTextField.getTextField(textField: txtFamilyStatus, originY: cellOriginY)
    }
    
    
    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
//        btnOkAssessor.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_CONFIRMATION_OF_SEPERATE, comment: ""), for: .normal)
            btnOkAssessor.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnOkAssessor.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
            
            btnCancelAddFile.isHidden = true
            GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileName = ""
            GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFile = ""
            if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfSeparate.iFileId = -1
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true
        }
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        self.changeBtnDesign(imgName: imgName, img: img)
    }
}
