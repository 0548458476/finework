//
//  ExemptionTaxCreditTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 7.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class ExemptionTaxCreditTableViewCell: UITableViewCell {
   
//MARK: - Outlet
    
    //MARK: -- Labels
    @IBOutlet weak var lblCellTitle: UILabel!
    @IBOutlet weak var lblUnderTitle: UILabel!
    @IBOutlet weak var lblErrorReason: UILabel!
    //MARK: -- Buttons
    @IBOutlet weak var btnExemptionTaxCredit: CheckBox!
    
    //MARK: -- Actions
    
    @IBAction func btnExemptionTaxCreditAction(_ sender: Any) {
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        GlobalEmployeeDetails.sharedInstance.reloadFromSelectReason = false
        
        if btnExemptionTaxCredit.isChecked == false
        {
//            setBtnUnChecked()
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2//נשוי
            {
                GlobalEmployeeDetails.sharedInstance.CheckForMarried()
            }
            else//לא נשוי
            {
                GlobalEmployeeDetails.sharedInstance.CheckForNotMarried()
            }
            GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected = true
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true
            GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit = true
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
        else
        {
            GlobalEmployeeDetails.sharedInstance.howManyToScroll = 0
            GlobalEmployeeDetails.sharedInstance.arrReasonsBool = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 1
            
            GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected = false
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = false
            GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
    }
    
    var cellOriginY:CGFloat = 0.0
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDesign()
        lblErrorReason.text = "\u{f129}"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
        setFont()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
        
//        if !btnExemptionTaxCredit.isChecked {
//            setBtnUnChecked()
//        }
        
    }
    
    func setBackgroundcolor()
    {
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
    }
    func setImages()
    {
        
    }
    func setBorders()
    {
    }
    func setTextcolor()
    {
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
    }
    
    func setIconFont()
    {
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }
}
