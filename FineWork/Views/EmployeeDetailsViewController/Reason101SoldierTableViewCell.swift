//
//  Reason101SoldierTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 21.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class Reason101SoldierTableViewCell: UITableViewCell,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,SetDateTextFieldTextDelegate,UIAlertViewDelegate, UploadFilesDelegate {

    
//MARK: - Outlet
    //MARK: -- Labels
    
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
      @IBOutlet weak var lblAddFileTitle: UILabel!
    
    //MARK: -- TextField
    
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!
    
    //MARK: - views
    
    @IBOutlet weak var viewFromDate: UIView!
    
    @IBOutlet weak var viewToDate: UIView!
    //MARK: -- Buttons
    
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    @IBOutlet weak var btnAddFile: UIButton!
    @IBOutlet weak var btnCancelAddFile: UIButton!
    
    //MARK: -- Action
    
    @IBAction func btnFromDateAction(_ sender: Any) {
    }
    @IBAction func btnToDateAction(_ sender: Any) {
    }
    @IBAction func btnAddFileAction(_ sender: Any) {
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileURL)
        
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileURL != ""//מהשרת ויש קובץ
//        {
//            
////            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileURL) as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            if let url = NSURL(string: urlStr as String) {
////                if let data = NSData(contentsOf: url as URL) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            //                    controller?.img = UIImage(data: data as Data)
//            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileURL
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                }
////            }
//        }
//        else
//        {
//            showCameraActionSheet()
//        }
    }
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
        Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
            }
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var openDatePickerDelegate:OpenDatePickerDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    var indexInArr = 0
    var reasonId = 13
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.delegateTextField = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        self.openDatePickerDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        
        ///----
//        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        ///----
        
        txtFromDate.delegate = self
        txtToDate.delegate = self
        setDesign()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextField(sender:)))
        viewFromDate.addGestureRecognizer(tap)
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextFieldToDate(sender:)))
        viewToDate.addGestureRecognizer(tap1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setDisplayData()
    {
        
        txtFromDate.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent1
        
        txtToDate.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent2
        
        //במקרה רגיל
        btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
         btnAddFile.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        btnCancelAddFile.isHidden = true
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnAddFile.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileName, for: .normal)
            btnCancelAddFile.isHidden = false
             btnAddFile.setTitleColor(UIColor.lightGray, for: .normal)
        }
//        else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף תעודת שחרור / סיום השירות״
//        {
//            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_SOLDIER_FILE, comment: ""), for: .normal)
//            btnCancelAddFile.isHidden = false
//        }
        
        //בדיקה האם קוד הסיבה קיימת במערך
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 13
            {
                indexInArr = i
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileURL != ""//מהשרת ויש קובץ
                {
                    btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
                {
                    //הצגת שם הקובץ
                    btnAddFile.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName, for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף תעודת שחרור / סיום השירות״
                {
                    btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_SOLDIER_FILE, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                break
            }
        }*/
    }
    
    func setDesign()
    {
        setBorders()
        setIconText()
        setLocalizableString()

    }
    
    func setBorders()
    {
        btnAddFile.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnAddFile.layer.borderWidth = 0.5
        btnAddFile.layer.cornerRadius = btnAddFile.frame.height/2
    }
    

    func setIconText()
    {
        btnFromDate.setTitle(FlatIcons.sharedInstance.CALANDER, for: .normal)
        btnToDate.setTitle(FlatIcons.sharedInstance.CALANDER, for: .normal)
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_SOLDIER_FILE, comment: "")
        btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
    }
    
    //MARK: - textField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let originY = GlobalEmployeeDetails.sharedInstance.arrReasonsPossision[reasonId]{
            delegateTextField.getTextField(textField: textField, originY: originY)
        }
        
        
//        if GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: "")] != nil
//        {
//            delegateTextField.getTextField(textField: textField, originY: GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: "")]!)
//        }
//        else
//        {
//            delegateTextField.getTextField(textField: textField, originY: 1420)
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
//        return true
//    }
    
    
    func tapDatesTextField(sender:UIView)//soldier from date
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 8, dateToShow: txtFromDate.text!)
    }
    
    func tapDatesTextFieldToDate(sender:UIView)//soldier to date immegrant
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 9, dateToShow: txtToDate.text!)
    }
    
    
    func SetDateTextFieldText(tagOfTextField:Int)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if tagOfTextField == 8//soldier from date
        {
            txtFromDate.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent1
        }
        else//soldier to date
        {
            txtToDate.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent2
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }

    //MARK: - camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
                //do whatever with your file name
//                self.changeBtnDesign(imgName: imageName!,img: tempImage)
//                picker.dismiss(animated: true, completion: nil);
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }

    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFile = img
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileName = imgName
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 13
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFile = Global.sharedInstance.setImageToString(image: img)
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName = imgName
                break
            }
        }*/
        btnAddFile.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
        btnAddFile.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnAddFile.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
            
            btnCancelAddFile.isHidden = false
            
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFile = ""
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileName = ""
            
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].fileObj.iFileId = -1
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true

        }
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        self.changeBtnDesign(imgName: imgName, img: img)
    }

}
