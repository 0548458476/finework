//
//  AddChildTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 22.1.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class AddChildTableViewCell: UITableViewCell,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate, UploadFilesDelegate {

    //MARK: - Outlet
    //MARK: -- View
    @IBOutlet weak var viewBottom: UIView!
    
    //MARK: -- Labels
    
    @IBOutlet weak var lblChildUnder18: UILabel!
    @IBOutlet weak var lblErrorChild: UILabel!
    @IBOutlet weak var lblAddFileTitle: UILabel!
    
    //MARK: -- Button
    
    @IBOutlet weak var btnAddChild: UIButton!
    @IBOutlet weak var btnAddAlowOk: UIButton!
    @IBOutlet weak var btnCanceladdFile: UIButton!

    //MARK: -- Action
    
    @IBAction func btnAddChildAction(_ sender: Any) {
        
        //add child to the list
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.append(WorkerChild(iFamilyStatusType: GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType))
        //        GlobalEmployeeDetails.sharedInstance.isButtonsChanged.append(Bool())
        
        GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true
        GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] += 1
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2//האם ההורה לא נשוי
//        {
//            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (405/750))
//        }
//        else
//        {
            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (285/750))
//        }
        
        GlobalEmployeeDetails.sharedInstance.ReloadReasons()
        if GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected == false//this condition is to prevent reload twice, because if it true the reload is in the "ReloadReasons()" func
        {
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
        
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnAddAlowOkAction(_ sender: Any) {
        
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL)
            
//            if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL != ""//מהשרת ויש קובץ
//            {
////                var url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL) as NSString
////                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////                if let url = NSURL(string: urlStr as String) {
////                    if let data = NSData(contentsOf: url as URL) {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//                
//                controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//                //                        controller?.img = UIImage(data: data as Data)
//                controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL
//                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                    }
////                }
//            }
//            else
//            {
//                showCameraActionSheet()
//            }
        
        }

    @IBAction func btnCanceladdFileAction(_ sender: Any) {
        
        Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
        
        }
    
    var cellOriginY:CGFloat = 0.0

    
    //MARK: - Initail
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        btnAddAlowOk.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.CONFIRMATION_OfCHILD_ALLOWANCE, comment: ""), for: .normal)
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.CONFIRMATION_OfCHILD_ALLOWANCE, comment: "")
        
        setDesign()
        lblErrorChild.text = "\u{f129}"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    //MARK: - Set Design
    
    func setDesign()
    {
        setCornerRadius()
        setBorders()
    }
    
    func setBorders()
    {
        btnAddChild.layer.borderColor = UIColor.orange2.cgColor
        btnAddChild.layer.borderWidth = 1
        
        btnAddAlowOk.layer.borderColor = UIColor.orange2.cgColor
        btnAddAlowOk.layer.borderWidth = 1
        
    }
    
    func setCornerRadius()
    {
        btnAddChild.layer.cornerRadius = btnAddChild.frame.height/2
        btnAddAlowOk.layer.cornerRadius = btnAddAlowOk.frame.height/2
    }
    
    func setDisplayData()
    {
        var showWithButton = false
        
        //אם לא נשוי/אה בדיקה אם להוסיף / להפחית גובה עבור הכפתור ׳צרף אישור קובץ׳ אם יש מישהו בחזקתו
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
        {
            for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds {
                if item.bWithMe == true
                {
                    showWithButton = true
                    break
                }
            }
            if showWithButton == true
            {
                btnAddAlowOk.isHidden = false
            }
            else
            {
                btnAddAlowOk.isHidden = true
            }
        }
        else
        {
            btnAddAlowOk.isHidden = true
        }
        lblAddFileTitle.isHidden = btnAddAlowOk.isHidden

        if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnAddAlowOk.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCanceladdFile.isHidden = false
            btnAddAlowOk.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnAddAlowOk.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileName, for: .normal)
            btnCanceladdFile.isHidden = false
            btnAddAlowOk.setTitleColor(UIColor.lightGray, for: .normal)
        }
        else// אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף אישור זכאות״
        {
            btnAddAlowOk.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnCanceladdFile.isHidden = true
            btnAddAlowOk.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        }

    }
    
    
    //MARK: ----camera func
    func showCameraActionSheet()
    {
        
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    //do whatever with your file name
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil);
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    } } else {
                    picker.dismiss(animated: true, completion: nil)
                }
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileName = imgName//מחיקת הקובץ מהאוביקט
        GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFile = img
        btnAddAlowOk.setTitle(imgName, for: .normal)
        btnCanceladdFile.isHidden = false
        btnAddAlowOk.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            btnAddAlowOk.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnAddAlowOk.setTitleColor(UIColor.orange2, for: .normal)
            btnCanceladdFile.isHidden = true
            GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileName = ""//מחיקת הקובץ מהאוביקט
            GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFile = ""
            if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.iFileId = -1
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true
        }
    }

    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        self.changeBtnDesign(imgName: imgName, img: img)
    }
    
   
}
