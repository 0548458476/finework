//
//  ReasonTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 11.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
//סל של סיבה ל:״אני מבקש פטור..״
class ReasonTableViewCell: UITableViewCell {

//MARK: - Outlet
    //MARK: -- Labels
    @IBOutlet weak var lblReason: UILabel!
    
    @IBOutlet weak var lblError: UILabel!
    //MARK -- Actions
    @IBAction func btnSelectReasonAction(_ sender: Any) {
        GlobalEmployeeDetails.sharedInstance.reloadFromSelectReason = true
        selectReason()
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: -- Buttons
    @IBOutlet weak var btnSelectReason: CheckBox!
    
    //MARK: - Variables
    var section = 0
    var tagParent = 0
    var cellOriginY:CGFloat = 0.0
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setDisplayData(txtReason:String)
    {
        lblReason.text = txtReason
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
        setFont()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
    }
    func setImages()
    {
        
    }
    func setBorders()
    {
    }
    func setTextcolor()
    {
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
    }
    
    func setIconFont()
    {
    }
    func setLocalizableString()
    {
        lblError.text = "\u{f129}"
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }
    
    func selectReason()
    {
        //סיבה 7 תהא אב שפותח רק אם סיבה 6 מסומנת        
        if GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_Kods[self.tagParent] == 6{
            GlobalEmployeeDetails.sharedInstance.setReason7father(isFather: !btnSelectReason.isChecked,tag: tag,tagParent:tagParent,section:section)
        }
    
        if btnSelectReason.isChecked == false
        {
            if GlobalEmployeeDetails.sharedInstance.dicReasonsCell[tagParent] != nil//if this cell open other cell
            {
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[section] += 1
                GlobalEmployeeDetails.sharedInstance.arrReasonsBool.insert(2, at: tag + 1)
                GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit.insert("", at: tag + 1)
                GlobalEmployeeDetails.sharedInstance.howManyToScroll += CGFloat(GlobalEmployeeDetails.sharedInstance.dicHeightForRowOpened[tagParent]!)
            }
            
            GlobalEmployeeDetails.sharedInstance.arrReasonsBool[self.tag] = 1
            GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.append(GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_Kods[self.tagParent])
            //אם סימן וי ב:״אני מבקש פטור״ וזה הסל האחרון(בדיקה זו היא כדי שלא יהיה מצב של reload בתוך relod) או לוחץ על סימון וי או איקס בסלים של האבות
            if GlobalEmployeeDetails.sharedInstance.reloadFromSelectReason == true
            {
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
            }
        }
        else
        {
            GlobalEmployeeDetails.sharedInstance.arrReasonsBool[self.tag] = 0
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[self.tagParent].iTaxRelifeReasonId = 0
            if GlobalEmployeeDetails.sharedInstance.dicReasonsCell[tagParent] != nil//if this cell open other cell
            {
                if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[tag + 1] == 2//his child opened
                {
                    GlobalEmployeeDetails.sharedInstance.numRowsForSection[section] -= 1
                    GlobalEmployeeDetails.sharedInstance.arrReasonsBool.remove(at: tag + 1)
                    GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit.remove(at: tag + 1)
                }
                GlobalEmployeeDetails.sharedInstance.howManyToScroll -= CGFloat(GlobalEmployeeDetails.sharedInstance.dicHeightForRowOpened[tagParent]!)
            }
            //remove from array
            GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected = GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.filter() {$0 != GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_Kods[self.tagParent]
            }
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
    }
}
