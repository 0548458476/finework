//
//  Reason101SingleParentTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 21.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class Reason101SingleParentTableViewCell: UITableViewCell,UITextFieldDelegate {

   //MARK: - Outlet
    //MARK: -- Labels
    
    @IBOutlet weak var lblNumChildInTaxYear: UILabel!
    @IBOutlet weak var lblNumChildWillBe3: UILabel!
    @IBOutlet weak var lblOtherChild: UILabel!
    
    //MARK: -- TextField
    
    @IBOutlet weak var txtNumChildInTaxYear: UITextField!
    @IBOutlet weak var txtNumChildWillBe3: UITextField!
    @IBOutlet weak var txtOtherChild: UITextField!
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.delegateTextField = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        txtOtherChild.delegate = self
        txtNumChildWillBe3.delegate = self
        txtNumChildInTaxYear.delegate = self

        setDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData()
    {
        var childAge = 0
        var childrenLessYear = 0
        var children3 = 0
        var children1_2 = 0
        for child in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds{
//            if child.bWithMe == true && child.bGetChildAllowance == true{
                if let birthDate = child.dBirthDate{
                    childAge = Global.sharedInstance.AgeOFCurrentYear(date1: birthDate as Date, date2: NSDate() as Date)
                        if childAge < 1{
                            childrenLessYear += 1
                        }else if childAge < 3{
                            children1_2 += 1
                        }else if childAge == 3{
                            children3 += 1
                        }
                }
                
//            }
        }
        
        txtNumChildInTaxYear.text = String(childrenLessYear)
        
        txtNumChildWillBe3.text = String(children3)
        
        txtOtherChild.text = String(children1_2)
        
        
//        txtNumChildInTaxYear.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[8].nvValueContent1
//        
//        txtNumChildWillBe3.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[8].nvValueContent2
//            
//        txtOtherChild.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[8].nvValueContent3
    }

    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setFont()
        setTextcolor()
        setCornerRadius()
        setBorders()
        self.setLocalizableString()
        
    }
    
    func setFont()
    {
        //set all textFields , labels and button font to RUBIK-REGULAR
        
    }
    func setBorders()
    {
        txtNumChildInTaxYear.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        txtOtherChild.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        txtNumChildWillBe3.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        
        
        //set border to textField
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtNumChildInTaxYear)
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtNumChildWillBe3)
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtOtherChild)
    }
    func setTextcolor()
    {
        //set text to labels
        
        //set text to textField
    }
    
    func setCornerRadius()
    {
//        txtOtherChild.layer.cornerRadius = 5
//        txtNumChildWillBe3.layer.cornerRadius = 5
//        txtNumChildInTaxYear.layer.cornerRadius = 5
    }
    
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }
    
    //MARK: - textField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: "")] != nil
        {
            delegateTextField.getTextField(textField: textField, originY: GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: "")]!)
        }
        else
        {
            delegateTextField.getTextField(textField: textField, originY: 1420)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        saveData(textField: textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
    func saveData(textField:UITextField)
    {
        if textField == txtNumChildInTaxYear
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[8].nvValueContent1 = txtNumChildInTaxYear.text!
        }
        else if textField == txtNumChildWillBe3
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[8].nvValueContent2 = txtNumChildWillBe3.text!
        }
        else if textField == txtOtherChild
        {
            
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[8].nvValueContent3 = txtOtherChild.text!
        }
    }
}
