//
//  ReasonForTaxCoordinationTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 8.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class ReasonForTaxCoordinationTableViewCell: UITableViewCell,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate, UploadFilesDelegate {

 //MARK: - Outlet
    //MARK: -- labels
    
    @IBOutlet weak var lblIconOpenTbl: UILabel!//
    @IBOutlet weak var lblEmployeeName: UILabel!//שם מעסיק
    @IBOutlet weak var lblAddress: UILabel!//כתובת
    @IBOutlet weak var lblDeductionsPortfolio: UILabel!//תיק ניכויים
    @IBOutlet weak var lblTypeOfIncome: UILabel!//סןג הכנסה
    @IBOutlet weak var lblIncome: UILabel!//הכנסה
    @IBOutlet weak var lblTaxDeducted: UILabel!//המס שנוכה
      @IBOutlet weak var lblAddFileTitle: UILabel!
    
    @IBOutlet weak var lblRemove: UILabel!
    //MARK: -- TextFields
    
    @IBOutlet weak var txtEmployeeName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtDeductionsPortfolio: UITextField!
    @IBOutlet weak var txtTypeOfIncome: UITextField!
    @IBOutlet var txtIncome: UITextField!
    @IBOutlet var txtTaxDeducted: UITextField!
    
    //view
    @IBOutlet weak var viewTypeOfIncome: UIView!
    
    //MARK: -- Buttons
    
    @IBOutlet weak var btnAddSlip: UIButton!//צרף תלוש
    
    @IBOutlet weak var btnCancelAddFile: UIButton!
    @IBOutlet weak var btnMoreEmployee: UIButton!//מעסיק נוסף
    
    @IBOutlet weak var btnDelEmployee: UIButton!
    //MARK: - tableView
    
    @IBOutlet weak var tblWorkType: UITableView!
    
    //MARK: -- Action
    
    @IBAction func btnAddSlipAction(_ sender: Any) {
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileURL)
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileURL != ""//מהשרת ויש קובץ
//        {
////            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileURL) as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            if let url = NSURL(string: urlStr as String) {
////                if let data = NSData(contentsOf: url as URL) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            //                    controller?.img = UIImage(data: data as Data)
//            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileURL
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                }
////            }
//        }
//        else
//        {
//            showCameraActionSheet()
//        }
    }
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
        Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
        }
    
    @IBAction func btnMoreEmployeeAction(_ sender: Any) {
        
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.append(WorkerTaxCoordination())
        GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] += 1
        GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true

            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750))
       
        
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
    }
    
    @IBAction func btnDelEmployeeAction(_ sender: Any) {
        Alert.sharedInstance.showAskAlertWithCellDelegateTag(mess: "האם ברצונך למחוק מעסיק זה?", delegate: self, tag: 2)
    }
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
//    var delegateScrollView:scrollToBottomDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    var tagRow = 0
    var arrIncomeType = ["משכורת חודש", "משכורת נוספת","משכורת חלקית","שכר עבודה","קצבה","מילגה"]
    var arrIncomeTypeKods = [9,10,11,12,13,14]
    
 //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tblWorkType.delegate = self
        tblWorkType.dataSource = self
        tblWorkType.isHidden = true
        
        txtIncome.delegate = self
        txtAddress.delegate = self
        txtTaxDeducted.delegate = self
        txtTypeOfIncome.delegate = self
        txtDeductionsPortfolio.delegate = self
        txtEmployeeName.delegate = self
        
        txtEmployeeName.returnKeyType = UIReturnKeyType.next
        txtAddress.returnKeyType = UIReturnKeyType.next

        
        setDesign()
        
        let tapTypeOfIncome: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openTypeOfIncome))
        tapTypeOfIncome.cancelsTouchesInView = false
        self.viewTypeOfIncome.addGestureRecognizer(tapTypeOfIncome)
        
        self.tblWorkType.allowsSelection = true
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLAOD_EMPLOYER_FILE, comment: "")
//        tapTypeOfIncome.cancelsTouchesInView = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setDisplayData()
    {
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnAddSlip.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
            btnAddSlip.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnAddSlip.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileName, for: .normal)
            btnCancelAddFile.isHidden = false
            btnAddSlip.setTitleColor(UIColor.lightGray, for: .normal)
        }
        else// אין קובץ-הצגת הטקסט הדיפולטיבי:״צרף תלוש״
        {
            btnAddSlip.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = true
            btnAddSlip.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        }
        
        //להציג את הכפתור מעסיק נוסף רק במעסיק האחרון
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count == tagRow+1{
            btnMoreEmployee.isHidden = false
        }else{
            btnMoreEmployee.isHidden = true
        }
     
        
        txtEmployeeName.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].nvIncomeSourceName
        
        txtAddress.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].nvAddress
        
        txtDeductionsPortfolio.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].nvTikNikuim
        
        txtTypeOfIncome.text = convertCodeIncomeToString(code: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].iIncomeType)
        
        var str = String(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].mMonthlyIncomeAmount)
        if str != "0.0"
        {
            if String(str.characters.suffix(2)) == ".0"
            {
                txtIncome.text = String(str.characters.dropLast(2))
            }
            else
            {
                txtIncome.text = str
            }
        }
        else
        {
            txtIncome.text = ""
        }

        str = String(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].mTaxDeducted)
        if str != "0.0"
        {
            if String(str.characters.suffix(2)) == ".0"
            {
                txtTaxDeducted.text = String(str.characters.dropLast(2))
            }
            else
            {
                txtTaxDeducted.text = str
            }
        }
        else
        {
            txtTaxDeducted.text = ""
        }
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
        setFont()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
        
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
    }
    func setImages()
    {
        
    }
    func setBorders()
    {
        btnAddSlip.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnAddSlip.layer.borderWidth = 0.5
        btnAddSlip.layer.cornerRadius = btnAddSlip.frame.height/2
        
        btnMoreEmployee.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnMoreEmployee.layer.borderWidth = 0.5
        btnMoreEmployee.layer.cornerRadius = btnMoreEmployee.frame.height/2
        
    }
    func setTextcolor()
    {
        btnAddSlip.titleLabel?.textColor = ControlsDesign.sharedInstance.colorFucsia
        btnMoreEmployee.titleLabel?.textColor = ControlsDesign.sharedInstance.colorFucsia
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
        btnDelEmployee.layer.cornerRadius = btnDelEmployee.frame.height/2
        btnDelEmployee.layer.borderColor = UIColor.orange2.cgColor
        btnDelEmployee.layer.borderWidth = 1
    }
    
    func setIconFont()
    {
        lblIconOpenTbl.text = "\u{f107}"
        lblIconOpenTbl.textColor = ControlsDesign.sharedInstance.colorGray
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        lblRemove.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.REMOVE, comment: "")
        btnAddSlip.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
    }
    
    //MARK: - textField
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtTypeOfIncome
        {
            tblWorkType.isHidden = false
            self.contentView.bringSubview(toFront: tblWorkType)
            self.contentView.endEditing(true)
            
        }
//        delegateTextField.getTextField(textField: textField,originY: cellOriginY)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        saveData(textField: textField)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtEmployeeName:
            txtAddress.becomeFirstResponder()
        case txtAddress:
            txtDeductionsPortfolio.becomeFirstResponder()
        case txtDeductionsPortfolio:
            txtIncome.becomeFirstResponder()
        case txtIncome:
            txtTaxDeducted.becomeFirstResponder()
        default:
            txtEmployeeName.resignFirstResponder()
            return true
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
    //MARK: - camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם תמונה","העלה תמונה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
                //do whatever with your file name
//                self.changeBtnDesign(imgName: imageName!,img: tempImage)
//                picker.dismiss(animated: true, completion: nil);
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }

    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileName = imgName//מחיקת הקובץ מהאוביקט
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFile = img
        btnAddSlip.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
         btnAddSlip.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrIncomeType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath)
        (cell as! FamilyStatusTableViewCell).setDisplayData(txtStatus: arrIncomeType[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblWorkType.isHidden = true
        txtTypeOfIncome.text = arrIncomeType[indexPath.row]
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].iIncomeType = arrIncomeTypeKods[indexPath.row]
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func openTypeOfIncome()
    {
        self.contentView.endEditing(true)
        tblWorkType.isHidden = false
        delegateTextField.getTextField(textField: txtTypeOfIncome,originY: cellOriginY)
    }
    
    //MARK: - SaveData
    
    func saveData(textField:UITextField)
    {
        switch textField {
        case txtIncome:
            if txtIncome.text != "" && Float(txtIncome.text!) != nil
            {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].mMonthlyIncomeAmount = Float(txtIncome.text!)!
            }
            else
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].mMonthlyIncomeAmount = 0.0
            }
            break
            
        case txtAddress:
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].nvAddress = txtAddress.text!
            break
        case txtTaxDeducted:
            if txtTaxDeducted.text != "" && Float(txtTaxDeducted.text!) != nil
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].mTaxDeducted = Float(txtTaxDeducted.text!)!
            }
            else
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].mTaxDeducted = 0.0
            }
            break
        case txtEmployeeName:
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].nvIncomeSourceName = txtEmployeeName.text!
            break
        case txtDeductionsPortfolio:
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[self.tagRow].nvTikNikuim = txtDeductionsPortfolio.text!
            break
        default:
            break
        }
    }
    
    func convertCodeIncomeToString(code:Int) -> String
    {
        for var i in 0 ..< arrIncomeTypeKods.count {
            if code == arrIncomeTypeKods[i]
            {
                return arrIncomeType[i]
            }
        }
        return ""
    }
    
    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if alertView.tag == 2{
            if buttonIndex == 0{
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.remove(at:self.tagRow)
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] -= 1
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750))
                
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
                
                //        delegateScrollView.scrollToBottom()
            }
        }else{
            if buttonIndex == 0{
                btnAddSlip.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
                btnAddSlip.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
                btnCancelAddFile.isHidden = true
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFile = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileName = ""
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileURL != ""//מהשרת ויש קובץ
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.nvFileURL = ""
                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[tagRow].fileObj.iFileId = -1
                }
            }
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
//    var uploadFilesDelegateForCellReason : UploadFilesDelegate! = nil
    
//    func uploadFile(fileUrl: String) {
//        uploadFilesDelegate.uploadFile!(fileUrl: fileUrl)
//    }
    
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
//        uploadFilesDelegateForCellReason.getFile!(imgName: imgName, img: img, displayImg: displayImg)
        self.changeBtnDesign(imgName: imgName, img: img)
    }

}
