//
//  End101TableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 20.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
import Photos
#if os(iOS)
    import PhotosUI
#endif


class End101TableViewCell: UITableViewCell,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate ,UIAlertViewDelegate, UploadFilesDelegate {

 //MARK: - Outlet
    //MARK: -- TextField
    
    @IBOutlet weak var txtCalculateField: UITextField!
    
    //MARK: -- Labels
    
    @IBOutlet weak var lblAgree: UILabel!
    @IBOutlet weak var lblAddFileTitle: UILabel!
    
    //MARK: -- Buttons
    
    @IBOutlet weak var btnCalculate: UIButton!
    
    @IBOutlet weak var btnAddTz: UIButton!
    
    @IBOutlet weak var btnCancelAddFile: UIButton!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnAgreeOk: UIButton!
    
    //MARK: -- Action
    
    @IBAction func btnCalculateAction(_ sender: Any) {
    }
    
    @IBAction func btnAddTzAction(_ sender: Any) {
        
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileURL)
        //open iBooks
        /*let stringURL = "ibooks://"
        let url = URL(string: stringURL)!
        UIApplication.shared.openURL(url)*/
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileURL != ""//מהשרת ויש קובץ
//        {
////            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileURL) as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            if let url = NSURL(string: urlStr as String) {
////                if let data = NSData(contentsOf: url as URL) {
////                    //2do - להציג את התמונה בפופאפ
////                    //btnAddTz.setBackgroundImage(UIImage(data: data as Data), for: .normal)
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            //                    controller?.img = UIImage(data: data as Data)
//            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileURL
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                }
////            }
//        }
//        else
//        {
//            showCameraActionSheet()
//        }
    }
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
         Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
        
    }
    
    //שמירה זמנית- שמור והמשך בפעם אחרת
    @IBAction func btnSaveAction(_ sender: Any) {
        
        GlobalEmployeeDetails.sharedInstance.show101ViewConGeneric()
        var dicToserver:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicToserver["nvIdentityNumber"] = GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber as AnyObject?
        dicToserver["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        var result:Bool = Bool()
        //אם הכניס תעודת זהות
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber != ""
        {
            myGroup.enter()
            //הפעלת פונקציה בשרת לבדיקה האם תעודת זהות זו כבר קיימת למשתמש אחר
            api.sharedInstance.CheckExistsIdentityNumber(params: dicToserver, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
                -> Void in
                
                var dicWorker101 = responseObject as! [String:AnyObject]
                if dicWorker101["Error"]?["iErrorCode"] as! Int ==  0
                {
                    result = (responseObject as! [String:AnyObject])["Result"]! as! Bool
                }
                self.myGroup.leave()
            } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
                self.myGroup.leave()
            })
            
            myGroup.notify(queue: DispatchQueue.main) {
                if result == true//תעודת זהות קיימת
                {
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: self.errorMssArr, isError: false)
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setReasonTxtFRedBorder(errorReason: self.errorReasonMssArr, isError: false)
                    
                    self.errorMssArr = ["nvIdentityNumber"]
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: self.errorMssArr, isError: true)
                    GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.EXIST_TZ, comment: ""))
                }
                else//תעודת זהות אינה קיימת
                {
                    self.isSaveTemporaryBtnClicked = true
                    self.Save(_isSaveTemporaryBtnClicked: true)
                    //self.InsertWorker101Form()
                }
            }
        }
        else//בכל מקרה אם לא הכניס תעודת זהות:שמירת הנתונים והפעלת הפונקציות של שמירה סופית(ההודעת שגיאה ע״כ שלא הכניס ת.ז תוצג לו אח״כ לפי מה שחוזר מהשרת)
        {

        
        
        isSaveTemporaryBtnClicked = true
        Save(_isSaveTemporaryBtnClicked: true)
        }
        /*if GlobalEmployeeDetails.sharedInstance.moreSalaryTableViewCell != nil
        {
            delegateSaveMoreSalary = GlobalEmployeeDetails.sharedInstance.moreSalaryTableViewCell
            delegateSaveMoreSalary.saveMoreSalary()
        }
        if checkValidation() == true
        {
            let helpLWorkerTaxRelifeReasons = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons = []
            
            //שמירת הקודים של ״אני מבקש פטור או זכוי ממס״ באוביקט
            for kod in 0..<GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.count
            {
                helpLWorkerTaxRelifeReasons[GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected[kod]].iTaxRelifeReasonId = GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected[kod]
                
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.append(helpLWorkerTaxRelifeReasons[GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected[kod]])
            }
            isSaveTemporaryBtnClicked = true
            InsertWorker101Form()
        }*/
    }
    
    @IBAction func btnAgreeOkAction(_ sender: Any) {
        GlobalEmployeeDetails.sharedInstance.show101ViewConGeneric()
        var dicToserver:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicToserver["nvIdentityNumber"] = GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber as AnyObject?
        dicToserver["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        var result:Bool = Bool()
        //אם הכניס תעודת זהות
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber != ""
        {
            myGroup.enter()
            //הפעלת פונקציה בשרת לבדיקה האם תעודת זהות זו כבר קיימת למשתמש אחר
            api.sharedInstance.CheckExistsIdentityNumber(params: dicToserver, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
                -> Void in
                
                var dicWorker101 = responseObject as! [String:AnyObject]
                if dicWorker101["Error"]?["iErrorCode"] as! Int ==  0
                {
                    result = (responseObject as! [String:AnyObject])["Result"]! as! Bool
                }
                self.myGroup.leave()
            } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
                self.myGroup.leave()
            })
            
            myGroup.notify(queue: DispatchQueue.main) {
                if result == true//תעודת זהות קיימת
                {
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: self.errorMssArr, isError: false)
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setReasonTxtFRedBorder(errorReason: self.errorReasonMssArr, isError: false)
                    
                    self.errorMssArr = ["nvIdentityNumber"]
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: self.errorMssArr, isError: true)

                    GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.EXIST_TZ, comment: ""))
                }
                else//תעודת זהות אינה קיימת
                {
                    self.isSaveTemporaryBtnClicked = false
                    self.Save(_isSaveTemporaryBtnClicked: false)
                    //self.InsertWorker101Form()
                }
            }
        }
        else//בכל מקרה אם לא הכניס תעודת זהות:שמירת הנתונים והפעלת הפונקציות של שמירה סופית(ההודעת שגיאה ע״כ שלא הכניס ת.ז תוצג לו אח״כ לפי מה שחוזר מהשרת)
        {
            isSaveTemporaryBtnClicked = false
            Save(_isSaveTemporaryBtnClicked: false)
            //InsertWorker101Form()
        }
        
        /*myGroup.notify(queue: DispatchQueue.main, execute: {
            print("All Done"); //completion(result: resultt)
        })*/
        
        /*api.sharedInstance.CheckExistsIdentityNumber(params: dicToserver, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            
            var dicWorker101 = responseObject as! [String:AnyObject]
            if dicWorker101["Error"]?["iErrorCode"] as! Int ==  0
            {
                let result:Bool = (responseObject as! [String:AnyObject])["Result"]! as! Bool
                
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            
        })*/
    }
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    var delegateSaveMoreSalary:saveMoreSalaryDelegate!=nil
    var myGroup = DispatchGroup()//כדי לטפל בכמה פקודות כפקודה אחת ,שפקודה תתבצע אחרי שפעולה א-סינכרונית מסתיימת
    var errorMssArr = NSArray()//מערך ששומר את השדות הלא תקינים מהשמירה הקודמת כדי להחזיר אותם לצבע הרגיל לפני השמירה החדשה
    var errorReasonMssArr = NSArray()//מערך ששומר את השדות הלא תקינים (של הסיבות לתאום מס) מהשמירה הקודמת כדי להחזיר אותם לצבע הרגיל לפני השמירה החדשה
    var isSaveTemporaryBtnClicked = false
    var errorChildrenArray = Dictionary<Int,Dictionary<String,Bool>>()
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtCalculateField.delegate = self
        txtCalculateField.isEnabled = false
        setDesign()
        
        
//        lblAgree.lineBreakMode = NSLineBreakMode.byWordWrapping
//        lblAgree.numberOfLines = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData()
    {
        if GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnAddTz.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
            btnAddTz.setTitleColor(UIColor.orange2, for: .normal)
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnAddTz.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileName, for: .normal)
            btnCancelAddFile.isHidden = false
            btnAddTz.setTitleColor(UIColor.lightGray, for: .normal)
        }
        else// אין קובץ-הצגת הטקסט הדיפולטיבי:״העלאת תעודת זהות״
        {
            btnAddTz.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = true
            btnAddTz.setTitleColor(UIColor.orange2, for: .normal)
        }
    }

    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setFont()
        setTextcolor()
        setCornerRadius()
        setBorders()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
        btnAgreeOk.backgroundColor =  UIColor.orange2
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
    }
    func setBorders()
    {
        btnSave.layer.borderColor = UIColor.orange2.cgColor
        btnSave.layer.borderWidth = 0.5
        
        btnAddTz.layer.borderColor = UIColor.orange2.cgColor
        btnAddTz.layer.borderWidth = 0.5
        
        btnAgreeOk.layer.borderColor = UIColor.orange2.cgColor
        btnAgreeOk.layer.borderWidth = 0.5
        
        btnCalculate.layer.borderColor = UIColor.orange2.cgColor
        btnCalculate.layer.borderWidth = 0.5


    }
    func setTextcolor()
    {
        //set to buttons
        btnAgreeOk.setTitleColor(UIColor.white, for: .normal)
        btnCalculate.setTitleColor(UIColor.orange2, for: .normal)
        btnAddTz.setTitleColor(UIColor.orange2, for: .normal)
        btnSave.setTitleColor(UIColor.orange2, for: .normal)
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
        btnSave.layer.cornerRadius = btnSave.frame.height/2
        btnAddTz.layer.cornerRadius = btnAddTz.frame.height/2
        btnAgreeOk.layer.cornerRadius =  btnAgreeOk.frame.height/2
        btnCalculate.layer.cornerRadius = btnCalculate.frame.height/2
    }
    
    func setIconFont()
    {
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        btnAddTz.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_TZ, comment: "")
    }
    
    //MARK: - Validation
    
    //check validation without check if not empty
    func checkValidation()->Bool
    {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: errorMssArr, isError: false)
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setReasonTxtFRedBorder(errorReason: errorReasonMssArr, isError: false)
        var errorArr = [Any]()
        errorChildrenArray = Dictionary<Int,Dictionary<String,Bool>>()
        var isOk = true
        if GlobalEmployeeDetails.sharedInstance.worker101Form.bResident == true//תושב ישראל
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber != ""
            {
                if Validation.sharedInstance.isTzValid(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber) == false
                {
                    isOk = false
                    errorArr.append("nvIdentityNumber")
//                    Alert.sharedInstance.showAlert(mess: "תעודת זהות אינה תקינה")
//                    return false
                }
            }
        }else{
            if GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber != ""
            {
                if Validation.sharedInstance.isPassportValid(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "הדרכון אינו תקין")
//                    return false
                    isOk = false
                    errorArr.append("nvIdentityNumber")
                }
            }
        }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvFirstName != ""
        {
            if Validation.sharedInstance.nameValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvFirstName) == false || Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvFirstName) == false
            {
//                Alert.sharedInstance.showAlert(mess: "שם פרטי אינו תקין")
//                return false
                isOk = false
                errorArr.append("nvFirstName")
            }
        }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvLastName != ""
        {
            if Validation.sharedInstance.nameValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvLastName) == false || Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvLastName) == false
            {
//                Alert.sharedInstance.showAlert(mess: "שם משפחה אינו תקין")
//                return false
                isOk = false
                errorArr.append("nvLastName")
            }
        }
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.iCityId != -1
//        {
//            if Validation.sharedInstance.nameValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.iCityId) == false || Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvCityName) == false
//            {
//                Alert.sharedInstance.showAlert(mess: "עיר אינה תקינה")
//                return false
//            }
//        }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvStreet != ""
        {
            if Validation.sharedInstance.nameValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvStreet) == false || Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvStreet) == false
            {
//                Alert.sharedInstance.showAlert(mess: "כתובת אינה תקינה")
//                return false
                isOk = false
                errorArr.append("nvStreet")
            }
        }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvHouseNumber != ""
        {
            if Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvStreet) == false || GlobalEmployeeDetails.sharedInstance.worker101Form.nvHouseNumber.characters.count > 10
            {
//                Alert.sharedInstance.showAlert(mess: "מספר בית אינו תקין")
//                return false
                isOk = false
                errorArr.append("nvHouseNumber")
            }
        }
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone != ""
        {
            if Validation.sharedInstance.phoneValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone) == false && Validation.sharedInstance.cellPhoneValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone) == false
            {
//                Alert.sharedInstance.showAlert(mess: "טלפון אינו תקין")
//                return false
                isOk = false
                errorArr.append("nvPhone")
            }
        }
        
        //children details
        var okChildren = true
        for child in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds {
            var dicError = Dictionary<String,Bool>()
            if child.nvName != ""
            {
                if Validation.sharedInstance.nameValidation(string: child.nvName) == false || Validation.sharedInstance.isContainsEmojy(string: child.nvName) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "שם ילד אינו תקין")
//                    return false
//                    isOk = false
//                    errorArr.adding("nvIdentityNumber")
                    dicError["nvName"] = true
                    okChildren = false
                }
            }
            if child.nvIdentityNumber != ""
            {
                if Validation.sharedInstance.isTzValid(string: child.nvIdentityNumber) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "תעודת זהות ילד אינו תקין")
//                    return false
//                    isOk = false
//                    errorArr.adding("nvIdentityNumber")
                    dicError["nvIdentityNumber"] = true
                    okChildren = false
                }
            }
            let indexOfA = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.index(of: child)
            errorChildrenArray[indexOfA!] = dicError
        }
        if okChildren == false{
            isOk = false
            errorArr.append("lWorkerChilds")
        }
        //תושב קבוע - שם הישוב
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent2 != ""
        {
            if Validation.sharedInstance.nameValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent2) == false || Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent2) == false
            {
//                Alert.sharedInstance.showAlert(mess: "שם הישוב אינו תקין")
//                return false
                isOk = false
                errorArr.append("reason")
            }
        }
        
        //WorkerTaxCoordinations details
        var okWorkerTaxCoordinations = true
        for worker in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations {
            if worker.nvIncomeSourceName != ""
            {
                if Validation.sharedInstance.nameValidation(string: worker.nvIncomeSourceName) == false || Validation.sharedInstance.isContainsEmojy(string: worker.nvIncomeSourceName) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "שם מעסיק אינו תקין")
//                    return false
//                    isOk = false
//                    errorArr.adding("nvIdentityNumber")
                    okWorkerTaxCoordinations = false
                }
            }
            if worker.nvAddress != ""
            {
                if Validation.sharedInstance.nameValidation(string: worker.nvAddress) == false || Validation.sharedInstance.isContainsEmojy(string: worker.nvAddress) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "כתובת מעסיק אינה תקינה")
//                    return false
//                    isOk = false
//                    errorArr.adding("nvIdentityNumber")
                    okWorkerTaxCoordinations = false
                }
            }
            if worker.fileObj.nvFileName != ""{
                if worker.nvTikNikuim == ""{
//                    Alert.sharedInstance.showAlert(mess: "חובה להזין תיק ניכויים")
//                    return false
//                    isOk = false
//                    errorArr.adding("nvIdentityNumber")
                    okWorkerTaxCoordinations = false
                }
            }
        }
        
        if checkUniqTikNicuiimValidation() == false{
            okWorkerTaxCoordinations = false
        }
        
        if okWorkerTaxCoordinations == false{
            isOk = false
            errorArr.append("lWorkerTaxCoordinations")
        }
        
        //partner details
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2//נשוי
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseFirstName != ""{
                if Validation.sharedInstance.nameValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseFirstName) == false || Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseFirstName) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "שם פרטי בן/ת זוג אינו תקין")
//                    return false
                    isOk = false
                    errorArr.append("nvSpouseFirstName")
                }

            }
            if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseLastName != ""{
                if Validation.sharedInstance.nameValidation(string: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseLastName) == false || Validation.sharedInstance.isContainsEmojy(string: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseLastName) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "שם משפחה בן/ת זוג אינו תקין")
//                    return false
                    isOk = false
                    errorArr.append("nvSpouseLastName")
                }
                
            }
            if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseIdentityNumber != ""
            {
                if Validation.sharedInstance.isPassportValid/*isTzValid*/(string: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseIdentityNumber) == false
                {
//                    Alert.sharedInstance.showAlert(mess: "תעודת זהות בן/ת זוג אינו תקין")
//                    return false
                    isOk = false
                    errorArr.append("nvSpouseIdentityNumber")
                }
            }
            
//            for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
//            {
//                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 5
//                {
//                    if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.isRequiredFieldsNull() == true//partner details not filled
//                    {
//                        Alert.sharedInstance.showAlert(mess: "חובה למלא פרטי בן/בת זוג")
//                        return false
//                        
//                    }
//                }
//            }
            
            //אם יש לבן/בת זוג הכנסה נוספת ללא בחירת סוג הכנסה
            if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true{
                if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType == 0{
                    GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                    Alert.sharedInstance.showAlert(mess: "חובה לבחור סוג הכנסה של בן/בת זוג")
                    return false
                }
            }
        }
        if checkUniqTzValidation() == false
        {
            GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
            Alert.sharedInstance.showAlert(mess: "תעודת זהות חוזרת על עצמה")
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.errorChildrenArray = errorChildrenArray
            return false
//            isOk = false
        }
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.errorChildrenArray = errorChildrenArray
        if isOk == false{
            self.errorMssArr = errorArr as NSArray
            
            
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: self.errorMssArr, isError: true)
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: errorMssArr, isError: true)
            GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.ERRORFILES, comment: ""))
            return false
        }else{
            return true
        }
    }
    
    //בדיקה אם תעודת זהות זו מופיעה רק פעם אחת בטופס זה
    func checkUniqTzValidation()->Bool
    {
        var child = WorkerChild()
        var childTz = WorkerChild()
        
        for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count
        {
            child = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[i]
            for j in i + 1..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count
            {
                childTz = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[j]
                if child.nvIdentityNumber == childTz.nvIdentityNumber && child.nvIdentityNumber != ""
                {
                    if errorChildrenArray[i] != nil{
                        errorChildrenArray[i]?["nvIdentityNumber"] = true
                    }else{
                        errorChildrenArray[i] = ["nvIdentityNumber":true]
                    }
                    return false
                }
                if child.nvIdentityNumber == GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber && GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber != ""
                {
                    if errorChildrenArray[i] != nil{
                        errorChildrenArray[i]?["nvIdentityNumber"] = true
                    }else{
                        errorChildrenArray[i] = ["nvIdentityNumber":true]
                    }
                    return false
                }
                if child.nvIdentityNumber == GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseIdentityNumber && child.nvIdentityNumber != ""
                {
                    if errorChildrenArray[i] != nil{
                        errorChildrenArray[i]?["nvIdentityNumber"] = true
                    }else{
                        errorChildrenArray[i] = ["nvIdentityNumber":true]
                    }
                    return false
                }
            }
        }
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber == GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseIdentityNumber && GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber != ""
        {
            return false
        }
        
        return true
    }
    
    //בדיקה אם תיק ניקויים זה מופיעה רק פעם אחת בטופס זה
    func checkUniqTikNicuiimValidation()->Bool
    {
        var taxCoordination = WorkerTaxCoordination()
        var taxCoordinationTz = WorkerTaxCoordination()
        
        for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count
        {
            taxCoordination = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[i]
            for j in i + 1..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count
            {
                taxCoordinationTz = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations[j]
                if taxCoordination.nvTikNikuim == taxCoordinationTz.nvTikNikuim && taxCoordination.nvTikNikuim != ""
                {
                    return false
                }
//                if taxCoordination.nvTikNikuim == GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber && GlobalEmployeeDetails.sharedInstance.worker101Form.nvTikNikuim != ""
//                {
//                    return false
//                }
//                if taxCoordination.nvTikNikuim == GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvTikNikuim && taxCoordination.nvTikNikuim != ""
//                {
//                    return false
//                }
            }
        }
        
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber == GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseIdentityNumber && GlobalEmployeeDetails.sharedInstance.worker101Form.nvIdentityNumber != ""
//        {
//            return false
//        }
        
        return true
    }
    
    //MARK: - textField
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//         delegateTextField.getTextField(textField: textField, originY:cellOriginY)
//    }

    //MARK: - save 101Form
    
    func InsertWorker101Form()
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.iWorkerUserId = Global.sharedInstance.user.iUserId
        
        //בדיקות האם השתנו נתונים בטופס שלא עודכנו באוביקט
        if GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome == false//לא מסומן יש לי הכנסות נוספות
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes = []
        }
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit == false//לא מסומן ״אני מבקש פטור״
//        {
//            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons = []
//        }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == false//לא מסומן ״אני מבקש תיאום מס מכיוון שיש לי הכנסות נוספות״
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations = []
        }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2//לא נשוי
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse = WorkerSpouse()
        }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodinationByAssessor == false//לא מסומן ״אני מבקש תיאום מס לפי אישור פקיד״
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.iFileId == -1{
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet = WorkerTaxCoordinationFileDet()
                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.iFileId = -1
            }else{
                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet = WorkerTaxCoordinationFileDet()
            }
        }
        
        api.sharedInstance.InsertWorker101Form(params: GlobalEmployeeDetails.sharedInstance.worker101Form.getDic(),success:{(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print((responseObject as! NSDictionary))
            //success
            if ((responseObject as! NSDictionary)["Error"] as! NSDictionary)["iErrorCode"]! as! Int == 0
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.iWorker101FormId = (responseObject as! NSDictionary)["Result"] as! Int
                //if bAskTaxCredit = false dont send details of  lTaxRelifeReason
//                if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit == true
//                {
                    self.InsertWorkerTaxRelifeReason()
//                }
//                else
//                {
//                     self.InsertWorkerTaxCoordination()
//                }
                
                //למנוע קריסה שנגרמה מריקות מערך הסיבות לאחר שמירה
                GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
            }
            else//error
            {
                GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: ""))
            }
            
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
        })
    }
    
    func InsertWorkerTaxRelifeReason()
    {
        //if GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.count
         api.sharedInstance.InsertWorkerTaxRelifeReason(params: GlobalEmployeeDetails.sharedInstance.worker101Form.getDicForTaxRelifeReasons(),success:{(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print((responseObject as! NSDictionary))
            if ((responseObject as! NSDictionary)["Error"] as! NSDictionary)["iErrorCode"]! as! Int == 0
            {
                self.InsertWorkerTaxCoordination()
            }
            else//error
            {
                GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: ""))
            }
            
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
        })
    }
    
    func InsertWorkerTaxCoordination()
    {
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: errorMssArr, isError: false)
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setReasonTxtFRedBorder(errorFields: errorReasonMssArr, isError: false)
        api.sharedInstance.InsertWorkerTaxCoordination(params: GlobalEmployeeDetails.sharedInstance.worker101Form.getDicForTaxCoordinations(),success:{(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print((responseObject as! NSDictionary))
            if ((responseObject as! NSDictionary)["Error"] as! NSDictionary)["iErrorCode"]! as! Int == 0
            {
                if self.isSaveTemporaryBtnClicked == true
                {
                    //try
//                    GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
//                    self.get101Form()
                    self.nextPage()
                    GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                    //מעבר עמוד
                   GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                    Alert.sharedInstance.showAlertWithTitle(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.TEMPORAY_SAVING_SMALL_TEXT, comment: ""), title: NSLocalizedString(LocalizableStringStatic.sharedInstance.TEMPORAY_SAVING, comment: ""))
                    
                }
                else//שמירה לחתימה דיגיטלית
                {
                    //בדיקת תקינות לטופס 101
                    var dicToserver:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    dicToserver["iWorker101FormId"] = GlobalEmployeeDetails.sharedInstance.worker101Form.iWorker101FormId as AnyObject?
                    api.sharedInstance.CheckWorker101FormValidation(params: dicToserver, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
                        -> Void in
                        
                        var dicWorker101 = responseObject as! [String:AnyObject]
                        if dicWorker101["Error"]?["iErrorCode"] as! Int ==  0//יש שגיאות
                        {
                            //שרשור הסטרינגים 
                            let errToShow:String = self.ErrorsToSringWithEnter(dic: (dicWorker101["Result"]?["errorMessages"] as! NSArray))
                            GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
//                            Alert.sharedInstance.showAlert(mess: "\(NSLocalizedString(LocalizableStringStatic.sharedInstance.SAVESUCCESS_FILLREQUIRED, comment: "")):\n\n\(errToShow)")
                            
                            Alert.sharedInstance.showAlertWithTitle(mess: errToShow, title: "\(NSLocalizedString(LocalizableStringStatic.sharedInstance.SAVESUCCESS_FILLREQUIRED, comment: "")):")
                  
                            //וכן לשים הודעות אדומות
                            self.errorMssArr = dicWorker101["Result"]?["errorFields"] as! NSArray
                            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setTxtFRedBorder(errorFields: self.errorMssArr, isError: true)
                            self.errorReasonMssArr = dicWorker101["Result"]?["taxRelifeReasonValidation"] as! NSArray
                            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setReasonTxtFRedBorder(errorReason: self.errorReasonMssArr, isError: true)
                            GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                            //try
                            self.get101Form()
                            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
//                            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                        }
                        else if dicWorker101["Error"]?["iErrorCode"] as! Int == -1//תקין
                        {
                            //try
//                            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                            //מעבר לחתימה דיגיטלית
                          
                            
                            self.UpdateWorker101FormStatus()
//                           self.saveForm101Successfully()
                        }
                        else//שגיאה
                        {
                            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                            GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: ""))
                        }
                    } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
                        
                        GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                    })
                }
            }
            else//error
            {
                GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: ""))
            }
            
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
        })
    }
    
    func UpdateWorker101FormStatus()
    {
        //if GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.count
        var dic = Dictionary<String, AnyObject>()
        dic["iWorker101FormId"] = GlobalEmployeeDetails.sharedInstance.worker101Form.iWorker101FormId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject
        
        api.sharedInstance.goServer(params: dic,success:{(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print((responseObject as! NSDictionary))
            if ((responseObject as! NSDictionary)["Error"] as! NSDictionary)["iErrorCode"]! as! Int == 0
            {
                self.saveForm101Successfully()
            }
            else//error
            {
//                GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: ""))
            }
            
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
        }, funcName: "UpdateWorker101FormStatus")
    }
    
    
    func saveForm101Successfully() {
        GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SAVE_SUCCESSFULLY, comment: ""))
//        self.get101Form()
        self.nextPage()
        GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
    }
    
    //MARK: ----camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.cameraCaptureMode = .photo
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL{
            if let imageName = imageUrl.lastPathComponent{
                if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName,img: imageString)
                        picker.dismiss(animated: true, completion: nil)

                    }else{
                        GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }else{
                if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                    let imageName = "img.JPEG"
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                        
                    }else{
                        GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
        }
    }
    
//    func fetchLastImage(completion: (_ localIdentifier: String?) -> Void)
//    {
//        let fetchOptions = PHFetchOptions()
//        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
//        fetchOptions.fetchLimit = 1
//        
//        let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
//        if (fetchResult.firstObject != nil)
//        {
//            let lastImageAsset: PHAsset = fetchResult.firstObject as! PHAsset
//            completion(localIdentifier: lastImageAsset.localIdentifier)
//        }
//        else
//        {
//            completion(localIdentifier: nil)
//        }
//    }
    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        if let image = info[UIImagePickerControllerOriginalImage]as? UIImage{
//            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//            
//
//        }
//        var mediaType: AnyObject? = info[UIImagePickerControllerMediaType]
//        let image = info[UIImagePickerControllerOriginalImage]as! UIImage
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//        imgOriginalView.image = image as UIImage
//
//    }
    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
//        
//        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
//            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
//                let imageName = asset?.defaultRepresentation().filename()
//                if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
//                //do whatever with your file name
//                    let imageString = Global.sharedInstance.setImageToString(image: tempImage)
//                    if imageString != ""{
//                       self.changeBtnDesign(imgName: imageName!,img: imageString)
//                        picker.dismiss(animated: true, completion: nil)
//                    }else{
//                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
//                        picker.dismiss(animated: true, completion: nil)
//                    }
//                }
////                picker.dismiss(animated: true, completion: nil);
//            }, failureBlock: nil)
//        }else{
//            if let image = info[UIImagePickerControllerOriginalImage]as? UIImage{
////                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//                
//                
//            }
//        }
//    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileName = imgName//מחיקת הקובץ מהאוביקט
        GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFile = img
        btnAddTz.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
        btnAddTz.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func ErrorsToSringWithEnter(dic:NSArray)->String
    {
        var stringAllerrorsToShow:String = ""
        for err in dic {
            stringAllerrorsToShow += "\(err)\n"
        }
        return stringAllerrorsToShow
    }
    
    //מקבל האם לחצו על שמירה זמנית = true,אם לחצו על ״הריני מאשר״ לחתימה דיגיטלית = false
    func Save(_isSaveTemporaryBtnClicked:Bool)
    {
        Global.sharedInstance.bWereThereAnyChanges = false
        
        if GlobalEmployeeDetails.sharedInstance.moreSalaryTableViewCell != nil
        {
            delegateSaveMoreSalary = GlobalEmployeeDetails.sharedInstance.moreSalaryTableViewCell
            delegateSaveMoreSalary.saveMoreSalary()
        }
        if checkValidation() == true
        {
            let helpLWorkerTaxRelifeReasons = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons
            
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons = []
            
            //שמירת הקודים של ״אני מבקש פטור או זכוי ממס״ באוביקט
            for kod in 0..<GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.count
            {
                helpLWorkerTaxRelifeReasons[GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected[kod]].iTaxRelifeReasonId = GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected[kod]
                
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.append(helpLWorkerTaxRelifeReasons[GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected[kod]])
            }
            isSaveTemporaryBtnClicked = _isSaveTemporaryBtnClicked
            InsertWorker101Form()
        }
    }
    
    var changeSubViewProfileDelegate : ChangeSubViewProfileDelegate! = nil
    
    func get101Form(){
        
        GlobalEmployeeDetails.sharedInstance.reload101Form()
        var dicToServer = Dictionary<String, AnyObject>()
        dicToServer["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.GetWorker101Form(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            
            var dicWorker101 = responseObject as! [String:AnyObject]
            if dicWorker101["Error"]?["iErrorCode"] as! Int ==  0
            {
                
                //print(responseObject as! [String:AnyObject])
                //print(responseObject as Any)
                print((responseObject as! [String:AnyObject])["Result"]!)
                
                let worker101:Worker101Form = Worker101Form()
                
                if !((responseObject as! [String:AnyObject])["Result"]! is NSNull)
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form = worker101.dicToWorker101Form(dic: (responseObject as! [String:AnyObject])["Result"]! as! Dictionary<String, AnyObject>)
                    
                    
                    //לאחר קבלת טופס 101 מהשרת
                    //שינוי מספר הסלים וגובה הגלילה בהתאם לטופס.
//                    GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame = Float((self.viewCon101?.view.frame.size.height)!)
                    GlobalEmployeeDetails.sharedInstance.set_dicHeightOfCellsOpened()
                    
                    //הוספת סלים לילדים
                    
                    GlobalEmployeeDetails.sharedInstance.numRowsForSection[0] += GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count
                    
                    //הוספת סלים של ילדים
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (285/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count))
                    
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2//האם ההורה לא נשוי
                    {
                        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 21
                        {
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (85/750))//אם הסטטוס הוא פרוד/ה מוסיפים את הגובה של הכפתור - ׳צירוף אישור׳.
                        }
                        
                        //הוספת סל הוסף ילדים
                        //בדיקה האם להוסיף גובה עבור הכפתור ׳צרף אישור זכאות׳
                        var showWithButton = false
                        
                        for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
                        {
                            if item.bWithMe == true
                            {
                                showWithButton = true
                                break
                            }
                        }
                        
                        if showWithButton == true
                        {
                            //הוספת גובה לכפתור צרף זכאות
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (60/750))
                        }
                    }
                    else//נשוי
                    {
                        //הוספת סל בן זוג אם נשוי
                        GlobalEmployeeDetails.sharedInstance.numRowsForSection[0] += 1
                        
                        if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType == 19 || GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType == 20
                        {
                            GlobalEmployeeDetails.sharedInstance.incomePartnerSelected = true
                            
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (480/750))
                        }
                        else
                        {
                            GlobalEmployeeDetails.sharedInstance.incomePartnerSelected = false
                            
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (320/750))
                        }
                    }
                    
                    //הוספת סלים אם יש לי הכנסות נוספות
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome == true
                    {
                        GlobalEmployeeDetails.sharedInstance.numRowsForSection[1] = 2
                        GlobalEmployeeDetails.sharedInstance.numSectionsInTbl = 6
                        
                        GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2)
                    }
                    //הוספת סליםGlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count אם בחר- אני מבקש תיאום מס...
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == true
                    {
                        GlobalEmployeeDetails.sharedInstance.numRowsForSection[3] += GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count
                        
                        GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count))
                    }
                    
                    for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons
                    {
                        GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.append(item.iTaxRelifeReasonId
                            
                        )
                    }
                    
                    //המערך של הסיבות בנוי מאובייקטים ריקים
                    //מהשרת חוזר רק הסיבות שנבחרו, לכן מוסיפים את הסיבות שלא נבחרו כאובייקטים ריקים בהתאמה.
                    GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                    
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit == true
                    {
                        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2
                        {
                            GlobalEmployeeDetails.sharedInstance.CheckForMarried()
                        }
                        else
                        {
                            GlobalEmployeeDetails.sharedInstance.CheckForNotMarried()
                        }
                    }
                    
                    GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected  = GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit
                    //GlobalEmployeeDetails.sharedInstance.isFromLogin = true
                    
                }
                //                                self.navigationController?.pushViewController(self.viewCon101!, animated: true)
                //                                self.navigationController?.pushViewController(self.viewCon!, animated: true)
                
                //MARK: כאן להוסיף את ה טופס!!!!
//                self.viewCon101.extTbl.reloadData()
//                self.view.addSubview(self.viewCon101.view)
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
//                var parent = self.superview as! UITableViewWrapperView
//                parent.reloadData()
//                UITableView *parentTable = (UITableView *)self.superview;
//                [parentTable reloadData];
                //                self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
                
                
            }
            else if dicWorker101["Error"]?["iErrorCode"] as! Int ==  2
            {
//                self.showAlert(sendMessage: "לא מולא טופס 101")
            }
            else
            {
//                self.showAlert(sendMessage: "error")
                GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
                Alert.sharedInstance.showAlert(mess: "error")
            }
            
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            
//            self.showAlert(sendMessage: "error")
            GlobalEmployeeDetails.sharedInstance.hide101ViewConGeneric()
            Alert.sharedInstance.showAlert(mess: "error")
            // self.navigationController?.pushViewController(self.viewCon101!, animated: true)
        })
    }
    
    func nextPage() {
        changeSubViewProfileDelegate.changeSubViewProfile(tagNewView: 1)
    }
    
//    func showAlert(sendMessage: String)
//    {
//        let alert : UIAlertController = UIAlertController(title: "", message: sendMessage, preferredStyle: UIAlertControllerStyle.alert)
//
//        let okAction:UIAlertAction = UIAlertAction(title: NSLocalizedString((LocalizableStringStatic.sharedInstance.OK_ALERT), comment: ""), style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
//            //self.dismiss()
//        })
//        
//        alert.addAction(okAction)
//        
//        self.present(alert, animated: true, completion: nil)
//    }
    
    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            btnAddTz.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnAddTz.setTitleColor(UIColor.orange2, for: .normal)
            btnCancelAddFile.isHidden = true
            GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileName = ""//מחיקת הקובץ מהאוביקט
            GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFile = ""
            if GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.photoID.iFileId = -1
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true

        }
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        self.changeBtnDesign(imgName: imgName, img: img)
    }

}
