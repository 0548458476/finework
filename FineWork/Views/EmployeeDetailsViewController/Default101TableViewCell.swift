//
//  Default101TableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 7.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class Default101TableViewCell: UITableViewCell {
    
  //MARK: - Outlet
    //MARK: -- Views
    
    @IBOutlet weak var viewTopSeperator: UIView!
    
    //MARK: -- Labels
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblError: UILabel!
    
    //MARK: -- Buttons
    @IBOutlet weak var btnReason: CheckBox!
    
    //MARK: -- Actions
    @IBAction func btnReasonAction(_ sender: Any) {
        Global.sharedInstance.bWereThereAnyChanges = true
        
        if self.tag == 3//אני מבקש תיאום מס מכיוון שיש לי הכנסות נוספות ממשכורת/קיצבה/מילגה
        {
            let indexPath = NSIndexPath(row: 0, section: 4)
            if btnReason.isChecked == false
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination = true
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count == 0
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.append(WorkerTaxCoordination())
                }
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count + 1
                
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true
                
                //תיאום מס
//                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count == 0//if there is no employers - open one
//                {
//                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750))
//                }
//                else//if there are employers - open (scroll) all of them
//                {
//                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count))
//                }

                //הכפתור השני של תיאום מס נהפך ללא מאופשר

            (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.isEnabled = false
            (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.alpha = 0.5//make the button as unVisible
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: self.tag) as IndexSet, with: .none)
            }
            else
            {
                if GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) != nil{//למנוע קריסה שארע בביטול תאום מס הראשון אחרי כניסה חוזרת לטופס
                GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination = false
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 1
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = false

                    (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.isEnabled = true
                (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.alpha = 1//make the button as Visible
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: self.tag) as IndexSet, with: .none)
                }
                else{
                
                    GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination = false
                    GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 1
                    GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = false
                        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
                    
                    (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.isEnabled = true
                    (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.alpha = 1//make the button as Visible
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: self.tag) as IndexSet, with: .none)
                    
//                    btnReason.isChecked = true
                }
            }
        }
        else
        {
            let indexPath = NSIndexPath(row: 0, section: 3)
            //להציג פופאפ
            if btnReason.isChecked == false
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodinationByAssessor = true
                
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.unregisterKeyboardNotifications()
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "TaxCoordinationDetailsViewController") as? TaxCoordinationDetailsViewController

                controller?.modalPresentationStyle = UIModalPresentationStyle.custom
                
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
                
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true
                
                //הכפתור השני של תיאום מס נהפך ללא מאופשר
                (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.isEnabled = false
                    (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.alpha = 0.5//make the button as unVisible
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: self.tag) as IndexSet, with: .none)
            }
            else
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodinationByAssessor = false
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = false
                (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.isEnabled = true
                (GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.cellForRow(at: indexPath as IndexPath) as! Default101TableViewCell).btnReason.alpha = 1//make the button as Visible
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: self.tag) as IndexSet, with: .none)
            }
        }
    }
    
    var cellOriginY:CGFloat = 0.0
    
    //MARK: - Initial
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDesign()
        lblError.text = "\u{f129}"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDisplayData()
    {
        //בדיקה אם אחד מהתאומי מס דלוק - כדי לא לאפשר את השני.
        if tag == 3
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodinationByAssessor == true
            {
                btnReason.isEnabled = false
                btnReason.alpha = 0.5//make the button as unVisible
            }
            else
            {
                btnReason.isEnabled = true
                btnReason.alpha = 1//make the button as unVisible
            }
        }
        else
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == true
            {
                btnReason.isEnabled = false
                btnReason.alpha = 0.5//make the button as unVisible
            }
            else
            {
                btnReason.isEnabled = true
                btnReason.alpha = 1//make the button as unVisible
            }
        }
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
//        setFont()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
    }
    func setFont()
    {
        lblReason.font = UIFont(name: "RUBIK-MEDIUM", size: 17.0)!
        //set all textFields and labels font to RUBIK-REGULAR
    }
    func setImages()
    {
        
    }
    func setBorders()
    {
    }
    func setTextcolor()
    {
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
    }
    
    func setIconFont()
    {
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }
}
