//
//  Reason101NewImmegrantTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 21.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class Reason101NewImmegrantTableViewCell: UITableViewCell,UITextFieldDelegate,SetDateTextFieldTextDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate, UploadFilesDelegate {

    //MARK: - Outlet
    //MARK: -- Labels
    
    @IBOutlet weak var lblNewImmegrant: UILabel!
    @IBOutlet weak var lblReturningResident: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
      @IBOutlet weak var lblAddFileTitle: UILabel!
    
    //MARK: -- TextField
    
    @IBOutlet weak var txtFromDate: UITextField!
    
    //MARK: - views
    
    @IBOutlet weak var viewFromDate: UIView!
    
    //MARK: -- Buttons
    
    @IBOutlet weak var btnNewImmegrant: SSRadioButton!
    @IBOutlet weak var btnReturningResident: SSRadioButton!
    @IBOutlet weak var btnAddImmigrantCertificate: UIButton!
    @IBOutlet weak var btnCancelAddFile: UIButton!
    @IBOutlet weak var btnFromDate: UIButton!
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var openDatePickerDelegate:OpenDatePickerDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    
    //MARK: -- Action
    
    @IBAction func btnNewImmegrantAction(_ sender: Any) {
        
        btnNewImmegrant.isSelected = true
        btnReturningResident.isSelected = false
        
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent1 = "true"
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    @IBAction func btnReturningResidentAction(_ sender: Any) {
        
        btnNewImmegrant.isSelected = false
        btnReturningResident.isSelected = true
        
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent1 = "false"
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    @IBAction func btnAddImmigrantCertificateAction(_ sender: Any) {
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileURL)
        
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileURL != ""//מהשרת ויש קובץ
//        {
////            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileURL) as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            if let url = NSURL(string: urlStr as String) {
////                if let data = NSData(contentsOf: url as URL) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            //                    controller?.img = UIImage(data: data as Data)
//            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileURL
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                }
////            }
//        }
//        else
//        {
//            showCameraActionSheet()
//        }
    }
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
          Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
        
    }
    @IBAction func btnFromDateAction(_ sender: Any) {
        
    }
    
    var reasonId = 4
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.openDatePickerDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        
        self.delegateTextField = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        
        ///----
//        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        ///----

        txtFromDate.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextField(sender:)))
        viewFromDate.addGestureRecognizer(tap)

        
        setDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setDisplayData()
    {
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent1 == "false"
        {
            btnNewImmegrant.isSelected = false
            btnReturningResident.isSelected = true
        }
        else
            //דיפולטיבי עולה חדש
        {
            btnNewImmegrant.isSelected = true
            btnReturningResident.isSelected = false
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent1 = "true"

        }
        txtFromDate.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent2
        
        //במקרה רגיל
        btnAddImmigrantCertificate.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
         btnAddImmigrantCertificate.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        btnCancelAddFile.isHidden = true
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnAddImmigrantCertificate.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnAddImmigrantCertificate.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileName, for: .normal)
             btnAddImmigrantCertificate.setTitleColor(UIColor.lightGray, for: .normal)
            btnCancelAddFile.isHidden = false
        }
//        else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף תעודת עולה״
//        {
//            btnAddImmigrantCertificate.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_IMMEGRANT_FILE, comment: ""), for: .normal)
//            btnCancelAddFile.isHidden = true
//        }
        
        //בדיקה האם קוד הסיבה קיימת במערך
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 4
            {
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileURL != ""//מהשרת ויש קובץ
                {
                    btnAddImmigrantCertificate.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
                {
                    //הצגת שם הקובץ
                    btnAddImmigrantCertificate.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName, for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף תעודת עולה״
                {
                    btnAddImmigrantCertificate.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_IMMEGRANT_FILE, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = true
                }
                break
            }
        }*/
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setBackgroundcolor()
        setFont()
        setTextcolor()
        setCornerRadius()
        setBorders()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
        //set textField backGroundColor
        txtFromDate.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
    }
    func setFont()
    {
        //set all textFields , labels and button font to RUBIK-REGULAR
        
    }
    func setBorders()
    {
        //set border to button
        btnAddImmigrantCertificate.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnAddImmigrantCertificate.layer.borderWidth = 0.5

//        //set border to textField
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtFromDate)
    }
    func setTextcolor()
    {
        //set text to button
        btnAddImmigrantCertificate.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        //set text to labels
        
        //set text to textField
    }
    
    func setCornerRadius()
    {
        btnAddImmigrantCertificate.layer.cornerRadius = btnAddImmigrantCertificate.frame.height/2
    }
    
    func setIconFont()
    {
        btnFromDate.setTitle(FlatIcons.sharedInstance.CALANDER, for: .normal)
        
        if Global.sharedInstance.rtl == true
        {
            txtFromDate.textAlignment = .right
        }
        else
        {
            txtFromDate.textAlignment = .left
        }
    }
    
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_IMMEGRANT_FILE, comment: "")
        btnAddImmigrantCertificate.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
    }

    //MARK: - textField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let originY = GlobalEmployeeDetails.sharedInstance.arrReasonsPossision[reasonId]{
            delegateTextField.getTextField(textField: textField, originY: originY)
        }
//        if GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("NEW_IMMIGRANT", comment: "")] != nil
//        {
//            delegateTextField.getTextField(textField: textField, originY: GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("NEW_IMMIGRANT", comment: "")]!)
//        }
//        else
//        {
//            delegateTextField.getTextField(textField: textField, originY: 1420)
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
//    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
//        return true
//    }
    
    func tapDatesTextField(sender:UIView)//new immiegrant - fromDate
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 6, dateToShow: txtFromDate.text!)
    }

    
    func SetDateTextFieldText(tagOfTextField:Int)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        
        
        
        
        
        if tagOfTextField == 6//dateFrom - new immegrant
        {
            txtFromDate.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent2
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
                //do whatever with your file name
                //                self.changeBtnDesign(imgName: imageName!,img: tempImage)
                //                picker.dismiss(animated: true, completion: nil);
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }

    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFile = img
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileName = imgName
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 4
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFile = Global.sharedInstance.setImageToString(image: img)
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName = imgName
                break
            }
        }*/
        btnAddImmigrantCertificate.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
         btnAddImmigrantCertificate.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            btnAddImmigrantCertificate.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
             btnAddImmigrantCertificate.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
            
            btnCancelAddFile.isHidden = true
            
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFile = ""
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileName = ""
            
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].fileObj.iFileId = -1
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true
        }
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        self.changeBtnDesign(imgName: imgName, img: img)
    }

}
