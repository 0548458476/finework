//
//  FamilyStatusTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 13.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class FamilyStatusTableViewCell: UITableViewCell {

//MARK: - Outlet
    //MARK: -- Labels
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCellTitle: UILabel!
//MARK: - Initial
    @IBOutlet weak var lblTitle: UILabel!
    
    // for draw bckground when select cell
    @IBOutlet var backgroundCellView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(txtStatus:String)
    {
        lblStatus.text = txtStatus
    }
    
    func setTitleText(text:String)
    {
        lblCellTitle.text = text
    }
    
    func setTitle(text:String)
    {
        lblTitle.text = text
    }
    
    func setBackgroundCellView(color : UIColor) {
        backgroundCellView.backgroundColor = color
    }

}
