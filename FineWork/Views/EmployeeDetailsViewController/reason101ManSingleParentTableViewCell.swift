//
//  reason101ManSingleParentTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 21.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class reason101ManSingleParentTableViewCell: UITableViewCell,UITextFieldDelegate {

  //MARK: - Outlet
    //MARK: -- Labels
    
    @IBOutlet weak var lblNumChildBeforeTaxYear: UILabel!
    
    @IBOutlet weak var lblNumChildWillBe18: UILabel!
    
    
    @IBOutlet weak var lblNumChildWillBeUnder5: UILabel!
    
    @IBOutlet weak var lblNumOtherChildUnder19: UILabel!
    
    //MARK: -- TextField
    
    @IBOutlet weak var txtNumChildBeforeTaxYear: UITextField!
    
    @IBOutlet weak var txtNumChildWillBe18: UITextField!
    
    @IBOutlet weak var txtNumChildWillBeUnder5: UITextField!
    
    @IBOutlet weak var txtNumOtherChildUnder19: UITextField!
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.delegateTextField = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        
        txtNumOtherChildUnder19.delegate = self
        txtNumChildWillBe18.delegate = self
        txtNumChildWillBeUnder5.delegate = self
        txtNumChildBeforeTaxYear.delegate = self
        setDesign()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(){
        var childAge = 0
        var childrenLessYear = 0
        var children18 = 0
        var children1_5 = 0
        var childrenLess19 = 0
//        var sumChildren = 0
        for child in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds{
//            if child.bWithMe == true && child.bGetChildAllowance == true{
                if let birthDate = child.dBirthDate{
                   childAge = Global.sharedInstance.AgeOFCurrentYear(date1: birthDate as Date, date2: NSDate() as Date)
                    if childAge < 19{
                        if childAge < 1{
                           childrenLessYear += 1
//                           sumChildren  += 1
                        }else if childAge <= 5{
                            children1_5 += 1
//                            sumChildren  += 1
                        }else if childAge == 18{
                            children18 += 1
//                            sumChildren  += 1
                        }else{
                            childrenLess19 += 1
                        }
                    }
                }
                
//            }
        }
//        txtNumChildBeforeTaxYear.text = String(childrenLessYear)
//        txtNumOtherChildUnder19.text = String(childrenLess19)
//        txtNumChildWillBe18.text = String(children18)
//        txtNumChildWillBeUnder5.text = String(children1_5)
        
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent1 = String(childrenLessYear)
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent4 = String(childrenLess19)
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent2 = String(children18)
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent3 = String(children1_5)
        
        txtNumChildBeforeTaxYear.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent1
        txtNumOtherChildUnder19.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent4
        txtNumChildWillBe18.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent2
        txtNumChildWillBeUnder5.text = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent3
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setFont()
        setTextcolor()
        setCornerRadius()
        setBorders()
        self.setLocalizableString()
    }
    
    func setFont()
    {
        //set all textFields , labels and button font to RUBIK-REGULAR
        
    }
    func setBorders()
    {
        txtNumChildWillBe18.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        txtNumChildWillBeUnder5.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        txtNumOtherChildUnder19.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        txtNumChildBeforeTaxYear.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray

        //set border to textField
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtNumChildWillBe18)
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtNumChildWillBeUnder5)
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtNumOtherChildUnder19)
//        ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 0.5, any: txtNumChildBeforeTaxYear)
    }
    func setTextcolor()
    {
        //set textColor to labels
        
        //set textColor to textField
    }
    
    func setCornerRadius()
    {
        /*txtNumChildBeforeTaxYear.layer.cornerRadius = 5
        txtNumOtherChildUnder19.layer.cornerRadius = 5
        txtNumChildWillBeUnder5.layer.cornerRadius = 5
        txtNumChildWillBe18.layer.cornerRadius = 5*/
    }
    
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }
    
    //MARK: - textField
    func textFieldDidBeginEditing(_ textField: UITextField) {
            
         if GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: "")] != nil
        {
            delegateTextField.getTextField(textField: textField, originY: GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: "")]!)
        }
        else
        {
            delegateTextField.getTextField(textField: textField, originY: 1420)
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        saveData(textField: textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
    func saveData(textField:UITextField)
    {
        if textField == txtNumChildBeforeTaxYear
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent1 = txtNumChildBeforeTaxYear.text!
        }
        else if textField == txtNumChildWillBeUnder5
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent3 = txtNumChildWillBeUnder5.text!
        }
        else if textField == txtNumChildWillBe18
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent2 = txtNumChildWillBe18.text!
        }
        else if textField == txtNumOtherChildUnder19
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[7].nvValueContent4 = txtNumOtherChildUnder19.text!
        }
    }

}
