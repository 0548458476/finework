//
//  Reason101EndLearnTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 21.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
//נפתח מסל: בגין סיום לימודים לתואר אקדמאי, סיום התמחות או סיום לימודי מקצוע
class Reason101EndLearnTableViewCell: UITableViewCell,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate, UploadFilesDelegate {
    
    //MARK: - Outlet
    //MARK: -- Button
    
    @IBOutlet weak var btnAddFile: UIButton!
    
    @IBOutlet weak var btnCancelAddFile: UIButton!
    
    //MARK: -- Action
    
    @IBAction func btnAddFileAction(_ sender: Any) {
        //---
        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        //---
        
        uploadFilesDelegate.uploadFile!(fileUrl: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileURL)
        
//        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileURL != ""//מהשרת ויש קובץ
//        {
////            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileURL) as NSString
////            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////            if let url = NSURL(string: urlStr as String) {
////                if let data = NSData(contentsOf: url as URL) {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            //                    controller?.img = UIImage(data: data as Data)
//            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileURL
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////                }
////            }
//        }
//        else
//        {
//            showCameraActionSheet()
//        }
    }
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
        Alert.sharedInstance.showAskAlertWithCellDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
            }
    
    //MARK: --Lable
      @IBOutlet weak var lblAddFileTitle: UILabel!
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ///----
//        self.uploadFilesDelegate = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.uploadFilesDelegate2 = self
        ///----
        
        setDesign()
    }
    
    func setDisplayData()
    {
        //במקרה רגיל
        btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
         btnAddFile.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        btnCancelAddFile.isHidden = true
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnAddFile.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileName, for: .normal)
            btnAddFile.setTitleColor(UIColor.lightGray, for: .normal)
            btnCancelAddFile.isHidden = false
        }
//        else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צרף הצהרה בטופס 119״
//        {
//            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_119FORM, comment: ""), for: .normal)
//            btnCancelAddFile.isHidden = true
//        }
        
        //בדיקה האם קוד הסיבה קיימת במערך
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 14
            {
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileURL != ""//מהשרת ויש קובץ
                {
                    btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
                {
                    //הצגת שם הקובץ
                    btnAddFile.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName, for: .normal)
                    btnCancelAddFile.isHidden = false
                }
                else//אין קובץ-הצגת הטקסט הדיפולטיבי:״צרף הצהרה בטופס 119״
                {
                    btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_119FORM, comment: ""), for: .normal)
                    btnCancelAddFile.isHidden = true
                }
                break
            }
        }*/
    }
    
    func setDesign()
    {
        setBorders()
        setCornerRadius()
        setLocalizableString()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setBorders()
    {
        btnAddFile.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnAddFile.layer.borderWidth = 0.5
    }
    
    func setCornerRadius()
    {
        btnAddFile.layer.cornerRadius = btnAddFile.frame.height/2
    }
    
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        lblAddFileTitle.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.UPLOAD_119FORM, comment: "")
        btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
    }
    
    //MARK: - camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: contentView)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }

                //do whatever with your file name
//                self.changeBtnDesign(imgName: imageName!,img: tempImage)
//                picker.dismiss(animated: true, completion: nil);
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }

    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFile = img
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileName = imgName
        /*for i in 0..<GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons.count
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].iTaxRelifeReasonId == 14
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFile = Global.sharedInstance.setImageToString(image: img)
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[i].fileObj.nvFileName = imgName
                break
            }
        }*/
        btnAddFile.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
        btnAddFile.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    

    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnAddFile.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
            btnCancelAddFile.isHidden = true
            
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFile = ""
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileName = ""
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[14].fileObj.iFileId = -1
            }
            Global.sharedInstance.bWereThereAnyChanges = true
        }
    }


    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        self.changeBtnDesign(imgName: imgName, img: img)
    }
}
