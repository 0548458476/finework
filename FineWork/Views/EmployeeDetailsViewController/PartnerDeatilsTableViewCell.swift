//
//  PartnerDeatilsTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 13.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit


class PartnerDeatilsTableViewCell: UITableViewCell,SSRadioButtonControllerDelegate,UITextFieldDelegate,SetDateTextFieldTextDelegate{


    //MARK: - Outlet
    //MARK: -- Labels
    
    @IBOutlet weak var lblFNamePartner: UILabel!
    
    @IBOutlet weak var lblLNamePartner: UILabel!
    
    @IBOutlet weak var lblDateBornPartner: UILabel!
    
    @IBOutlet weak var lblIconDateBorn: UILabel!
    @IBOutlet weak var lblDateImmigrationPartner: UILabel!
    
    @IBOutlet weak var lblIconDateImmigrantion: UILabel!
    @IBOutlet weak var lblTzPartner: UILabel!
    
    @IBOutlet weak var lblIncomePartner: UILabel!
    
    @IBOutlet weak var lblIncomeWorker: UILabel!
    
    @IBOutlet weak var lblOther: UILabel!
  
    //MARK: -- TextFields
    
    @IBOutlet weak var txtFNamePartner: UITextField!
    
    @IBOutlet weak var txtLNamePartner: UITextField!
    
    @IBOutlet weak var txtDateBornPartner: UITextField!
    
    @IBOutlet weak var txtDateImmigrationPartner: UITextField!
    
    @IBOutlet weak var txtTzPartner: UITextField!
    
    //MARK: - Views
    
    @IBOutlet weak var viewBottomSeperator: UIView!
    
    @IBOutlet weak var viewDateBornPartner: UIView!
    
    @IBOutlet weak var viewDateImmigrationPartner: UIView!
    //MARK: -- Button
    
    @IBOutlet weak var btnIncome: CheckBox!
    
    @IBOutlet weak var btnIncomeWork: SSRadioButton!
    
    @IBOutlet weak var btnOther: SSRadioButton!
    
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var openDatePickerDelegate:OpenDatePickerDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    let dateFormatter = DateFormatter()
    
    //MARK: - TextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        dismissKeyboard()
        saveData(textField: textField)
            }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//         delegateTextField.getTextField(textField: textField, originY:cellOriginY)
//    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtFNamePartner:
            txtLNamePartner.becomeFirstResponder()
        case txtLNamePartner:
            txtTzPartner.becomeFirstResponder()
        default:
            txtFNamePartner.resignFirstResponder()
        }
        return false
//        textField.resignFirstResponder()
//        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }

    
    // MARK: - KeyBoard
    
    func dismissKeyboard() {
        self.contentView.endEditing(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        contentView.addGestureRecognizer(tap)
    }
    

    
    //MARK: -- Action
    
    @IBAction func btnIncomeAction(_ sender: Any) {
       btnIncome.isChecked = !btnIncome.isChecked
        if btnIncome.isChecked == true
        {
            GlobalEmployeeDetails.sharedInstance.incomePartnerSelected = true
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: self.tag) as IndexSet, with: .none)
            
            //add height----
            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (140/750))
  
        }
        else
        {
            GlobalEmployeeDetails.sharedInstance.incomePartnerSelected = false
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType = 0
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: self.tag) as IndexSet, with: .none)
            //remove height----
            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (140/750))
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnIncomeWorkAction(_ sender: Any) {
        
        if btnIncomeWork.isSelected == false
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType = 19
            
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnOtherAction(_ sender: Any) {
        if btnOther.isSelected == false
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType = 20
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - Variables
    var radioButtonController: SSRadioButtonsController?
//    var delegate:disableScrollTblDelegate!=nil
    
    
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setDesign()
        
        if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true
        {
            btnOther.isHidden = false
            btnIncomeWork.isHidden = false
            lblOther.isHidden = false
            lblIncomeWorker.isHidden = false
        }
        else
        {
            btnOther.isHidden = true
            btnIncomeWork.isHidden = true
            lblOther.isHidden = true
            lblIncomeWorker.isHidden = true
        }
        radioButtonController = SSRadioButtonsController(buttons: btnOther, btnIncomeWork)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true

        txtTzPartner.delegate = self
        txtFNamePartner.delegate = self
        txtLNamePartner.delegate = self
        txtDateBornPartner.delegate = self
        txtDateImmigrationPartner.delegate = self
        
        txtFNamePartner.returnKeyType = UIReturnKeyType.next
        txtLNamePartner.returnKeyType = UIReturnKeyType.next
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextField(sender:)))
        viewDateBornPartner.addGestureRecognizer(tap)
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextFieldImmegrantDate(sender:)))
        viewDateImmigrationPartner.addGestureRecognizer(tap1)
        
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
        setFont()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
        if Global.sharedInstance.rtl == true
        {
            txtDateBornPartner.textAlignment = .right
            txtDateImmigrationPartner.textAlignment = .right
        }
        else
        {
            txtDateBornPartner.textAlignment = .left
            txtDateImmigrationPartner.textAlignment = .left
        }

    }
    func setImages()
    {
        
    }
    func setBorders()
    {
    }
    func setTextcolor()
    {
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
        txtTzPartner.layer.cornerRadius = 5
        txtFNamePartner.layer.cornerRadius = 5
        txtLNamePartner.layer.cornerRadius = 5
        txtDateBornPartner.layer.cornerRadius = 5
        txtDateImmigrationPartner.layer.cornerRadius = 5
        btnIncome.layer.cornerRadius = 5
    }
    
    func setIconFont()
    {
        lblIconDateBorn.text = FlatIcons.sharedInstance.CALANDER
        lblIconDateImmigrantion.text = FlatIcons.sharedInstance.CALANDER
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }


    
    //MARK: - SaveData
    
    func saveData(textField:UITextField)
    {
        switch textField {
        case txtFNamePartner:
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseFirstName = txtFNamePartner.text!
            break
        case txtLNamePartner:
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseLastName = txtLNamePartner.text!
            break
        case txtTzPartner:
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseIdentityNumber = txtTzPartner.text!
            break
        default:
            break
        }
    }
    
    func setDisplayData()
    {
        txtFNamePartner.text = GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseFirstName
        txtLNamePartner.text = GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseLastName
        txtTzPartner.text = GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.nvSpouseIdentityNumber
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseBirthDate != nil
        {
            txtDateBornPartner.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseBirthDate as! Date)
        }
        else
        {
            txtDateBornPartner.text = ""
        }
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseImmigrationDate != nil
        {
            txtDateImmigrationPartner.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseImmigrationDate as! Date)
        }
        else
        {
            txtDateImmigrationPartner.text = ""
        }
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType == 19
        {
            btnIncomeWork.isSelected = true
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType == 20
        {
            btnOther.isSelected = true
        }
    }
    
    func tapDatesTextField(sender:UIView)//partner date born
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 3, dateToShow: txtDateBornPartner.text!)
    }
    
    func tapDatesTextFieldImmegrantDate(sender:UIView)//partner date immegrant
    {
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 4, dateToShow: txtDateImmigrationPartner.text!)
    }
    
    
    func SetDateTextFieldText(tagOfTextField:Int)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if tagOfTextField == 3//txtDateBorn - partner
        {
            
            txtDateBornPartner.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseBirthDate as! Date)
            
        }
        else//txtDateImmigration - partner
        {
            txtDateImmigrationPartner.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseImmigrationDate as! Date)
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
}
