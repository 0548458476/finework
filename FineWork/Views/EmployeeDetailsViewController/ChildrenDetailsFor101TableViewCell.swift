//
//  ChildrenDetailsFor101TableViewCell.swift
//  FineWork
//
//  Created by Lior Ronen on 08/12/2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class ChildrenDetailsFor101TableViewCell: UITableViewCell,UITextFieldDelegate,SetDateTextFieldTextDelegate,UIAlertViewDelegate {
    
    //MARK: - Outlet
    //MARK: - views
    
    @IBOutlet weak var viewBottomSeperator: UIView!
    
    @IBOutlet weak var viewChildDateBorn: UIView!
    
    
    //MARK: -- Labels
    
    @IBOutlet weak var lblChildName: UILabel!
    
    @IBOutlet weak var lblChuldDateBorn: UILabel!
    
    @IBOutlet weak var lblTz: UILabel!
    
    @IBOutlet weak var lblGetChildAllowance: UILabel!
    
    @IBOutlet weak var lblChildHold: UILabel!
    
    @IBOutlet weak var lblRemove: UILabel!
    @IBOutlet weak var lblErrorChildren: UILabel!
    //MARK: -- TextField
    
    @IBOutlet weak var txtChildName: UITextField!
    @IBOutlet weak var txtChildDateBorn: UITextField!
    @IBOutlet weak var txtTz: UITextField!
    
    //MARK: -- Buttons
    
    @IBOutlet weak var btnDelChild: UIButton!
    
    @IBOutlet weak var btnIconDateBorn: UIButton!
    
    @IBOutlet weak var btnGetChildAllowance: CheckBox!
    
    @IBOutlet weak var btnChildHold: CheckBox!
    
    @IBOutlet weak var btnAddAlowOk: UIButton!
    
    @IBOutlet weak var btnCanceladdFile: UIButton!
    
    @IBOutlet weak var btnOk: UIButton!
    
    //MARK: - Action
    
    @IBAction func btnGetChildAllowanceaction(_ sender: Any) {
        btnGetChildAllowance.isChecked = !btnGetChildAllowance.isChecked
        //save data
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].bGetChildAllowance = btnGetChildAllowance.isChecked
        //        GlobalEmployeeDetails.sharedInstance.isButtonsChanged[tag] = true
        GlobalEmployeeDetails.sharedInstance.ReloadReasons()
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnChildHoldAction(_ sender: Any) {
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        btnChildHold.isChecked = !btnChildHold.isChecked
        
        var showWithButton = false
        
        //אם לא נשוי/אה בדיקה אם להוסיף / להפחית גובה עבור הכפתור ׳צרף אישור קובץ׳ אם יש מישהו בחזקתו
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
        {
            for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds {
                if item.bWithMe == true
                {
                    showWithButton = true
                    break
                }
            }
        }
        if btnChildHold.isChecked == false
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].bWithMe = btnChildHold.isChecked
            
            if showWithButton == true && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
            {
                //שינוי גובה הסל של הוסף ילדים
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (60/750)
                
            }
            
            btnGetChildAllowance.isChecked = false
            btnGetChildAllowance.isEnabled = false
            btnGetChildAllowance.alpha = 0.5//make the button as unVisible
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].bGetChildAllowance = false
        }
        else
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].bWithMe = btnChildHold.isChecked
            
            if showWithButton == false && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
            {
                //שינוי גובה הסל של הוסף ילדים
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (60/750)
            }
            
            btnGetChildAllowance.isEnabled = true
            btnGetChildAllowance.alpha = 1//make the button as Visible
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2//parrent not married
            {
                btnGetChildAllowance.isChecked = true
                btnGetChildAllowance.isEnabled = true
                btnGetChildAllowance.alpha = 1//make the button as Visible
                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].bGetChildAllowance = true
            }
        }
        //        GlobalEmployeeDetails.sharedInstance.isButtonsChanged[tag] = true
        if GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected == true
        {
            GlobalEmployeeDetails.sharedInstance.ReloadReasons()
        }
        else
        {
            let indexPath:NSIndexPath = NSIndexPath(row: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1, section: tagSection)
            
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadRows(at: [indexPath as IndexPath], with: UITableViewRowAnimation.fade)
            
            
            
            //isFtomChangeCheckBox = true
            //GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
    }
    
    @IBAction func btnAddAlowOkAction(_ sender: Any) {}
    
    @IBAction func btnCanceladdFileAction(_ sender: Any) {
        
        btnCanceladdFile.isHidden = true
        GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileName = ""//מחיקת הקובץ מהאוביקט
        GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFile = ""
        if GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL != ""//מהשרת ויש קובץ
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.nvFileURL = ""
            GlobalEmployeeDetails.sharedInstance.worker101Form.confirmationOfChildAllowance.iFileId = -1
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnOkAction(_ sender: Any) {
    }
    
    
    @IBAction func btnDelChildAction(_ sender: Any) {
        Alert.sharedInstance.showAskAlertWithCellDelegate(mess: "האם ברצונך למחוק את הילד?", delegate: self)
    }
    
    //MARK: - variables
    var delegateTextField:getTextFieldDelegate!=nil
    var openDatePickerDelegate:OpenDatePickerDelegate!=nil
    var cellOriginY:CGFloat = 0.0
    let dateFormatter = DateFormatter()
    var tagSection = 0
    var isFtomChangeCheckBox = false
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //call to design the controls
        setDesign()
        //initial delegate for textFields
        txtTz.delegate = self
        txtChildName.delegate = self
        txtChildDateBorn.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDatesTextField(sender:)))
        viewChildDateBorn.addGestureRecognizer(tap)
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        txtChildName.returnKeyType = UIReturnKeyType.next
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setDisplayData(workerChild:WorkerChild)
    {
        txtChildName.text = workerChild.nvName
        if workerChild.dBirthDate != nil
        {
            txtChildDateBorn.text = dateFormatter.string(from: workerChild.dBirthDate as! Date)
        }
        else
        {
            txtChildDateBorn.text = ""
        }
        txtTz.text = workerChild.nvIdentityNumber
        
        //מתייחס לפי הדיפולט - אם בחרו נשואה משתנה בהתאם...
        //העתקתי מsetDisplayDataNull
        
        if GlobalEmployeeDetails.sharedInstance.isFromChangeFamiliStatus == true
        {
            //            if isFtomChangeCheckBox == true
            //            {
            //                isFtomChangeCheckBox = false
            //            }
            //            else
            //            {
            btnGetChildAllowance.isChecked = false
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2//parrent not married
            {
                btnGetChildAllowance.isEnabled = false
                btnGetChildAllowance.alpha = 0.5//make the button as unVisible
                btnChildHold.isChecked = false
            }
            else//default if parent married
            {
                //תמיד כשנשוי (גם גבר)- נמצא בחזקתי וגם "מקבל קצבת ילדים" בברירת מחדל צריך להיות מסומן וי
                
                //                    if GlobalEmployeeDetails.sharedInstance.worker101Form.iGenderType == 6//if woman
                //                    {
                btnGetChildAllowance.isEnabled = true
                btnGetChildAllowance.alpha = 1//make the button as Visible
                btnGetChildAllowance.isChecked = true
                btnChildHold.isChecked = true
                //                    }
            }
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].bGetChildAllowance = btnGetChildAllowance.isChecked
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].bWithMe = btnChildHold.isChecked
            //            }
        }
        else//לפי האובייקט
        {
            btnGetChildAllowance.isChecked = workerChild.bGetChildAllowance
            btnChildHold.isChecked = workerChild.bWithMe
            
            if btnChildHold.isChecked == true
            {
                btnGetChildAllowance.isEnabled = true
                btnGetChildAllowance.alpha = 1//make the button as Visible
            }
            else
            {
                btnGetChildAllowance.isEnabled = false
                btnGetChildAllowance.alpha = 0.5//make the button as unVisible
            }
        }
        
        if tag == GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count - 1//אם גמרו להציג את כל הילדים ,מעכשיו יש להציג את הנתונים של הילדים לפי המצב המשפחתי
        {
            GlobalEmployeeDetails.sharedInstance.isFromChangeFamiliStatus = false
        }
        //מתייחס לפי מצב קודם
        //        btnGetChildAllowance.isChecked = workerChild.bGetChildAllowance
        //        btnChildHold.isChecked = workerChild.bWithMe
        //
        //        if btnChildHold.isChecked == true
        //        {
        //            btnGetChildAllowance.isEnabled = true
        //            btnGetChildAllowance.alpha = 1//make the button as Visible
        //        }
        //        else
        //        {
        //            btnGetChildAllowance.isEnabled = false
        //            btnGetChildAllowance.alpha = 0.5//make the button as unVisible
        //        }
        
        
    }
    
    func setDisplayDataNull()
    {
        txtChildName.text = ""
        txtChildDateBorn.text = ""
        txtTz.text = ""
        btnGetChildAllowance.isChecked = false
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2//parrent not married
        {
            btnGetChildAllowance.isEnabled = false
            btnGetChildAllowance.alpha = 0.5//make the button as unVisible
            btnChildHold.isChecked = false
        }
        else//default if parent married
        {
            //תמיד כשנשוי (גם גבר)- נמצא בחזקתי וגם "מקבל קצבת ילדים" בברירת מחדל צריך להיות מסומן וי
            //            if GlobalEmployeeDetails.sharedInstance.worker101Form.iGenderType == 6//if woman
            //            {
            btnGetChildAllowance.isEnabled = true
            btnGetChildAllowance.alpha = 1//make the button as Visible
            btnGetChildAllowance.isChecked = true
            btnChildHold.isChecked = true
            //            }
        }
        
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag] = WorkerChild(_dBirthDate: NSDate(), _nvName: txtChildName.text!, _nvIdentityNumber: txtTz.text!, _bWithMe: btnChildHold.isChecked, _bGetChildAllowance: btnGetChildAllowance.isChecked)
        
    }
    
    //MARK: - TextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        saveData(textField: textField)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        delegateTextField.getTextField(textField: textField, originY:cellOriginY)
        //הצגת מקלדת בהתאם למה שמסומן אצל ההורה בכפתור ״אני תושב ישראל״
        if textField == txtTz
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.bResident == true//תושב ישראל
            {
                textField.keyboardType = .numberPad
            }
            else
            {
                textField.keyboardType = .numbersAndPunctuation
            }
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtChildName:
            txtTz.becomeFirstResponder()
        default:
            txtChildName.resignFirstResponder()
        }
        return false
        //        textField.resignFirstResponder()
        //        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let text = textField.text else { return true }
        //        let newLength = text.characters.count + string.characters.count - range.length
        //        let limitLength : Int = newLength
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //        return newLength <= limitLength // Bool
        return true
    }
    
    // MARK: - KeyBoard
    
    func dismissKeyboard() {
        self.contentView.endEditing(true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        contentView.addGestureRecognizer(tap)
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    func setDesign()
    {
        setFont()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
        //        btnDelChild.layer.cornerRadius = btnDelChild.frame.width / 2
        //        btnDelChild.layer.borderColor = UIColor.black.cgColor
        //        btnDelChild.layer.borderWidth = 1
    }
    
    
    func setFont()
    {
        if Global.sharedInstance.rtl == true
        {
            txtChildDateBorn.textAlignment = .right
        }
        else
        {
            txtChildDateBorn.textAlignment = .left
        }
        
        
        //set all textFields and labels font to RUBIK-REGULAR
    }
    
    func setTextcolor()
    {
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
        btnChildHold.layer.cornerRadius = 5
        btnGetChildAllowance.layer.cornerRadius = 5
        txtChildDateBorn.layer.cornerRadius = 5
        txtChildName.layer.cornerRadius = 5
        txtTz.layer.cornerRadius = 5
        btnDelChild.layer.cornerRadius = btnDelChild.frame.height/2
        btnDelChild.layer.borderColor = UIColor.orange2.cgColor
        btnDelChild.layer.borderWidth = 1
    }
    
    func setIconFont()
    {
        btnIconDateBorn.setTitle(FlatIcons.sharedInstance.CALANDER, for: .normal)
    }
    
    func setLocalizableString()
    {
        lblRemove.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.REMOVE, comment: "")
        //SET TEXT TO BUTTONS
        
    }
    
    //MARK: - SaveData
    
    func saveData(textField:UITextField)
    {
        switch textField {
        case txtChildName:
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].nvName = txtChildName.text!
            break
        case txtChildDateBorn:
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].dBirthDate = dateFormatter.date(from: txtChildDateBorn.text!)! as NSDate
            break
        case txtTz:
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].nvIdentityNumber = txtTz.text!
            break
        default:
            break
        }
    }
    
    func tapDatesTextField(sender:UIView)
    {//dateBorn - child
        openDatePickerDelegate.OpenDatePicker(viewCon: self, tag: 5, dateToShow:txtChildDateBorn.text!)
    }
    
    func SetDateTextFieldText(tagOfTextField:Int)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if tagOfTextField == 5//txtDateBorn - child
        {
            txtChildDateBorn.text = dateFormatter.string(from: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[tag].dBirthDate as! Date)
            GlobalEmployeeDetails.sharedInstance.ReloadReasons()
            //            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .none)
            //            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: --AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count == 1
            {
                //שלא תוצג שגיאה של ילדים אם יוסיפו אח״כ ילד
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.errorArray["lWorkerChilds"] = false
                
                //בדיקה האם להפחית גובה של הכפתור ׳צרף אישור זכאות׳
                var showWithButton = false
                
                for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
                {
                    if item.bWithMe == true
                    {
                        showWithButton = true
                        break
                    }
                }
                
                if showWithButton == true
                {
                    //שינוי גובה הסל של הוסף ילדים
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (60/750)
                }

            }

            //עדכון מילון שגיאות הילדים כדי שהשגיאה הקודמת תשאר על הילד הרצוי
            for i in tag+1  ..< GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count  {
               GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.errorChildrenArray[i-1] = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.errorChildrenArray[i]
            }
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.errorChildrenArray[GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count-1] = Dictionary<String,Bool>()
            
            //remove child from the list
            GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.remove(at:self.tag)
            //        GlobalEmployeeDetails.sharedInstance.isButtonsChanged.remove(at:self.tag)
            
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tagSection] = true
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tagSection] -= 1
            
            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (285/750))
            
            GlobalEmployeeDetails.sharedInstance.ReloadReasons()
            if GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected == false//this condition is to prevent reload twice, because if it true the reload is in the "ReloadReasons()" func
            {
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true
        }
    }
    
}
