//
//  EarningsDetailsTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 5.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
//פרטים על הכנסותי ממעביד זה
class EarningsDetailsTableViewCell: UITableViewCell,SSRadioButtonControllerDelegate {

  //MARK: - Outlet
    //MARK: -- Views
    
    @IBOutlet weak var viewBottomSeperator: UIView!
    
    //MARK: -- Buttons
    
    @IBOutlet weak var btnMonthSalary: SSRadioButton!
    
    @IBOutlet weak var btnPartialSalary: SSRadioButton!
    
    @IBOutlet weak var btnAllowance: SSRadioButton!
    
    @IBOutlet weak var btnMoreSalary: SSRadioButton!
    
    @IBOutlet weak var btnWorkSalary: SSRadioButton!
    
    @IBOutlet weak var btnScholarship: SSRadioButton!
    
    @IBOutlet weak var btnNoEarn: SSRadioButton!
    
    @IBOutlet weak var btnHasMoreEarn: SSRadioButton!
    
    //MARK: - Action
    
    @IBAction func btnNoMoreSalaryAction(_ sender: Any) {
        
        if btnNoEarn.isSelected == false
        {
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 1
            GlobalEmployeeDetails.sharedInstance.numSectionsInTbl = 4
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = false
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[3] = false
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[3] = 1
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[4] = false
            //להוריד גובה - MoreSalaryTableViewCell
            if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == true
            {
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= (CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2) + CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count)))
            }
            else
            {
                 GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= (CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2))
            }
            
            
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
            
//            if btnNoEarn.isSelected == false{
                GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome = false
//            }
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //יש לי הכנסות נוספות
    @IBAction func btnHasMoreSalaryAction(_ sender: Any) {
        
        if btnHasMoreEarn.isSelected == false
        {
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 2
            GlobalEmployeeDetails.sharedInstance.numSectionsInTbl = 6
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true
            
         
            
    //להוסיף גובה - MoreSalaryTableViewCell
            
            if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == true
            {
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += (CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2) + CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count)))
                
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[3] += GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count
            }
            else
            {
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += (CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2))
            }

            
            GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome = true
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
        /*else
        {
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 1
            GlobalEmployeeDetails.sharedInstance.numSectionsInTbl = 4
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = false
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[3] = false
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[3] = 1
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[4] = false
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
           
                GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome = true
        }*/
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnMoreSalaryAction(_ sender: Any) {
        if btnMoreSalary.isSelected == false
        {
            
            //הוספת גובה לגלילה
            if btnHasMoreEarn.isSelected == false
            {
                if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == true
                {
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += (CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2) + CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count)))
                    
                    GlobalEmployeeDetails.sharedInstance.numRowsForSection[3] += GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count
                }
                else
                {
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += (CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2))
                }
            }

            
            GlobalEmployeeDetails.sharedInstance.worker101Form.iIncomeType = 10
            
            btnHasMoreEarn.isSelected = true
            btnNoEarn.isEnabled = false
            btnNoEarn.alpha = 0.5//make the button as unVisible
            btnNoEarn.isSelected = false
            
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 2
            GlobalEmployeeDetails.sharedInstance.numSectionsInTbl = 6
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = true
            
            GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome = true

            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func btnWorkSalaryAction(_ sender: Any) {
        if btnWorkSalary.isSelected == false
        {
            GlobalEmployeeDetails.sharedInstance.worker101Form.iIncomeType = 12
            btnNoEarn.isEnabled = true
            btnNoEarn.alpha = 1//make the button as Visible

            if btnHasMoreEarn.isSelected == false
            {
                btnNoEarn.isSelected = true
                btnHasMoreEarn.isSelected = false
                
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[self.tag] = 1
                GlobalEmployeeDetails.sharedInstance.numSectionsInTbl = 4
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[self.tag] = false
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[3] = false
                GlobalEmployeeDetails.sharedInstance.numRowsForSection[3] = 1
                GlobalEmployeeDetails.sharedInstance.selectRowsForSection[4] = false
                
                //להוריד גובה - MoreSalaryTableViewCell
                
                GlobalEmployeeDetails.sharedInstance.howManyToScroll2 -= (CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2))
                
                
                GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome = false
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
            }
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //MARK: - Labels
    @IBOutlet weak var lblErrorMoreSaleries: UILabel!
    
    //MARK: - Variables
    var radioButtonController: SSRadioButtonsController?
    var radioButtonMoreSalary: SSRadioButtonsController?
    var cellOriginY:CGFloat = 0.0
    
    //MARK: - Initial
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        setDesign()
        
        radioButtonController = SSRadioButtonsController(buttons: btnMonthSalary, btnPartialSalary, btnAllowance, btnMoreSalary, btnWorkSalary, btnScholarship)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        btnWorkSalary.isSelected = true
        btnMonthSalary.isEnabled = false
        btnMonthSalary.alpha = 0.5//make the button as unVisible
        btnPartialSalary.isEnabled = false
        btnPartialSalary.alpha = 0.5//make the button as unVisible
        btnAllowance.isEnabled = false
        btnAllowance.alpha = 0.5//make the button as unVisible
        btnMoreSalary.isEnabled = true
        btnMoreSalary.alpha = 1//make the button as Visible
        btnWorkSalary.isEnabled = true
        btnWorkSalary.alpha = 1//make the button as Visible
        btnScholarship.isEnabled = false
        btnScholarship.alpha = 0.5//make the button as unVisible
        btnNoEarn.isSelected = true
        
        radioButtonMoreSalary = SSRadioButtonsController(buttons: btnNoEarn, btnHasMoreEarn)
        radioButtonMoreSalary!.delegate = self
        radioButtonMoreSalary!.shouldLetDeSelect = true
        
        lblErrorMoreSaleries.text = "\u{f129}"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData()
    {
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iIncomeType == 10
        {
            btnMoreSalary.isSelected = true
            btnWorkSalary.isSelected = false
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.iIncomeType == 12
            {
                btnWorkSalary.isSelected = true
                btnMoreSalary.isSelected = false
            }
        if GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome == true
        {
            btnHasMoreEarn.isSelected = true
            btnNoEarn.isSelected = false
        }
        else
        {
            btnNoEarn.isSelected = true
            btnHasMoreEarn.isSelected = false
        }
    }
    
    //MARK:-- local Functions
    //MARK:-- SETTERS Functions
    
    
    func setDesign()
    {
        setBackgroundcolor()
        setImages()
        setFont()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        
    }
    
    func setBackgroundcolor()
    {
    }
    func setFont()
    {
        //set all textFields and labels font to RUBIK-REGULAR
    }
    func setImages()
    {
        
    }
    func setBorders()
    {
    }
    func setTextcolor()
    {
        //set to labels
        
        //set to textFields
    }
    
    func setCornerRadius()
    {
    }
    
    func setIconFont()
    {
    }
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        //        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
    }

}
