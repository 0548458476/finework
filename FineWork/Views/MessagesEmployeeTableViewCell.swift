//
//  MessagesEmployeeTableViewCell.swift
//  FineWork
//
//  Created by User on 28.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class MessagesEmployeeTableViewCell: UITableViewCell {

    //MARK:- Outlet
    
    
    //MARK:-- Buttons
    
    //MARK:-- Actions
    
    //MARK:-- Images
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    //MARK:-- Labels
    
    @IBOutlet weak var lblJob: UILabel!
    
    @IBOutlet weak var lblText: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!

    @IBOutlet weak var lblHour: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(img:String,job:String,text:String,date:NSDate)
    {
        if img != ""
        {
            imgLogo.image = UIImage(named: img)
        }
        lblJob.text = job
        lblText.text = text
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd:MM:yy"
        var dateString = dateFormatter.string(from: date as Date)
        
        lblDate.text = dateString
        
        dateFormatter.dateFormat = "hh:mm"
        dateString = dateFormatter.string(from: date as Date)
        lblHour.text = dateString
    }

}
