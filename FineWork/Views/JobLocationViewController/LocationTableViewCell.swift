//
//  LocationTableViewCell.swift
//  FineWork
//
//  Created by Lior Ronen on 23/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell, BasePopUpActionsDelegate {

    //MARK: - Views
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var radiusLbl: UILabel!
    @IBOutlet var removeBtn: UIButton!
   
    //MARK: - Actions
    @IBAction func removeBtnAction(_ sender : UIButton) {
        openBasePopUp()
    }
    
    //MARK: - Variables
    var remomeCellDelegate : CellOptionsDelegate! = nil
    var cellNumber : Int = 0
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDisplayData(workerLocation : WorkerAskingLocation?) {
        if workerLocation != nil {
            addressLbl.text = workerLocation?.nvAddress
            if workerLocation?.nRadius != nil {
                radiusLbl.text = (Int((workerLocation?.nRadius!)!) /*/ 1000*/).description + " ק״מ"
            }
        }
    }
    
    //MARK: - BasePopUpActionsDelegate
    func okAction(num: Int) {
        remomeCellDelegate.removeCell!(cellNumber: cellNumber)

    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך להסיר את המיקום?"
        
        openAnotherPageDelegate.openViewController!(VC: basePopUp)
    }

}
