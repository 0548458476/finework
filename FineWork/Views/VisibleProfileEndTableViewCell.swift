//
//  VisibleProfileEndTableViewCell.swift
//  FineWork
//
//  Created by Lior Ronen on 02/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//
protocol AddLanguageDelegate {
    func addLanguage(isScrollToMiddle : Bool)
}

import UIKit

class VisibleProfileEndTableViewCell: UITableViewCell, UITextFieldDelegate, UploadFilesDelegate, BasePopUpActionsDelegate {

    //MARK: -- Views
    @IBOutlet var addLanguageBtn: UIButton!
    @IBOutlet var addLanguageHeight: NSLayoutConstraint!
    @IBOutlet var militaryServiceBtn: CheckBox!
    @IBOutlet var characteristicsTxt: UITextField!
    @IBOutlet var studiesTxt: UITextField!
    @IBOutlet var jobExperienceTxt: UITextField!
    @IBOutlet var matchingJobsTxt: UITextField!
    @IBOutlet var myYoutubeVideoTxt: UITextField!
    @IBOutlet var uploadResumeBtn: UIButton!
    @IBOutlet var cancelFileBtn: UIButton!
    @IBOutlet var saveBtn: UIButton!
    
    @IBOutlet weak var rankBtn: UIButton!
    @IBOutlet var militaryServiceView: UIView!
    @IBOutlet var militaryServiceYearsTxt: UITextField!
    @IBOutlet var militaryServiceRoleTxt: UITextField!
    @IBOutlet var heightFromTopOfCharacteristics:NSLayoutConstraint!
    
    //MARK: -- Action
    @IBAction func militaryServiceBtnAction(_ sender: CheckBox) {
        militaryServiceBtn.isChecked = !militaryServiceBtn.isChecked
        militaryServiceView.isHidden = !militaryServiceBtn.isChecked
        heightFromTopOfCharacteristics.constant = militaryServiceView.isHidden ? 10 : 80
        
        Global.sharedInstance.worker.bMilitaryService = militaryServiceBtn.isChecked
        
        if let tbl = self.superview?.superview as? UITableView {
            tbl.reloadData()
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        //militaryServiceView.isHidden ? 500 : 570
    }
    
    @IBAction func addLanguageBtnClicked() {
        // add cell of language with option to delete is.
        addLanguageDelegate.addLanguage(isScrollToMiddle: true)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func rankBtnCloced(_ sender: Any) {
        let story =  UIStoryboard(name: "StoryboardGeneral", bundle: nil)
        let viewCon = story.instantiateViewController(withIdentifier: "RankViewController") as! RankViewController
              openAnotherPageDelegate.openViewController!(VC: viewCon)

        
       // self.navigationController?.pushViewController(viewCon, animated: true)

    }
    @IBAction func saveBtnClicked() {
        // save details of this cell.
        Global.sharedInstance.worker.bMilitaryService = militaryServiceBtn.isChecked
        Global.sharedInstance.worker.workerResumeDetails.nvResumeCharacteristics = characteristicsTxt.text!
        Global.sharedInstance.worker.workerResumeDetails.nvResumeStudies = studiesTxt.text!
        Global.sharedInstance.worker.workerResumeDetails.nvResumeWorkExperience = jobExperienceTxt.text!
        Global.sharedInstance.worker.workerResumeDetails.nvResumeSuitableWork = matchingJobsTxt.text!
        
        Global.sharedInstance.worker.nvVideoLink = myYoutubeVideoTxt.text!
        
        saveWorkerDelegate.saveWorkerFinal()
    }
    
    @IBAction func uploadResumeBtnClicked() {
        // upload resume
        uploadFileDelegate.uploadFile!(fileUrl: Global.sharedInstance.worker.workerResumeDetails.resume.nvFileURL)
    }
 
    @IBAction func cancelFileBtnClicked() {
        // upload resume
        //uploadFileDelegate.uploadFile!()
        openBasePopUp()
//        cancelFileBtn.isHidden = true
//        uploadResumeBtn.setTitle("צרף קובץ", for: .normal)
//        uploadResumeBtn.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
//        Global.sharedInstance.worker.workerResumeDetails.resume.nvFileURL = ""
    }

    //MARK: -- Variables
    var addLanguageDelegate : AddLanguageDelegate! = nil
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    var saveWorkerDelegate : SaveWorkerDelegate! = nil
    var uploadFileDelegate : UploadFilesDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    var cellOriginY : CGFloat = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        addLanguageBtn.layer.borderWidth = 1
        addLanguageBtn.layer.borderColor = UIColor.orange2.cgColor
        addLanguageBtn.layer.cornerRadius = addLanguageBtn.frame.height / 2
        //addLanguageHeight.constant / 2
        
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.borderColor = UIColor.orange2.cgColor
        saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
        
        rankBtn.layer.borderWidth = 1
        rankBtn.layer.borderColor = UIColor.orange2.cgColor
        rankBtn.layer.cornerRadius = rankBtn.frame.height / 2

        uploadResumeBtn.layer.borderWidth = 1
        uploadResumeBtn.layer.borderColor = UIColor.orange2.cgColor
        uploadResumeBtn.layer.cornerRadius = uploadResumeBtn.frame.height / 2
        
        //hideKeyboardWhenTappedAround()
        
        militaryServiceYearsTxt.delegate = self
        militaryServiceRoleTxt.delegate = self
        characteristicsTxt.delegate = self
        studiesTxt.delegate = self
        jobExperienceTxt.delegate = self
        matchingJobsTxt.delegate = self
        myYoutubeVideoTxt.delegate = self
        
        militaryServiceYearsTxt.keyboardType = .numberPad
        militaryServiceRoleTxt.keyboardType = .default
        characteristicsTxt.keyboardType = .default
        studiesTxt.keyboardType = .default
        jobExperienceTxt.keyboardType = .default
        matchingJobsTxt.keyboardType = .default
        myYoutubeVideoTxt.keyboardType = .asciiCapable
        
        //הוספת תגית לצורך העלאת המקלדת למקום המתאים ( כי זה טקסט על ויו)
        militaryServiceYearsTxt.tag = 1
        militaryServiceRoleTxt.tag = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(/*_cellOriginY : CGFloat*/) {
        //cellOriginY = _cellOriginY
        
        // initial fields
        let worker = Global.sharedInstance.worker
        
        militaryServiceBtn.isChecked = worker.bMilitaryService
        if militaryServiceBtn.isChecked {
            if worker.iMilitaryServiceYears != nil {
                militaryServiceYearsTxt.text = String(describing: worker.iMilitaryServiceYears!).description
            }
            militaryServiceRoleTxt.text = worker.nvMilitaryServiceRole
            militaryServiceView.isHidden = false
            heightFromTopOfCharacteristics.constant = 80
        }
        characteristicsTxt.text = worker.workerResumeDetails.nvResumeCharacteristics
        studiesTxt.text = worker.workerResumeDetails.nvResumeStudies
        jobExperienceTxt.text = worker.workerResumeDetails.nvResumeWorkExperience
        matchingJobsTxt.text = worker.workerResumeDetails.nvResumeSuitableWork
        
        myYoutubeVideoTxt.text = worker.nvVideoLink
        
        if Global.sharedInstance.worker.workerResumeDetails.resume.nvFileURL != "" {
            uploadResumeBtn.setTitle("צפיה בקובץ", for: .normal)
            cancelFileBtn.isHidden = false
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        getTextFieldDelegate.getTextField(textField: textField, originY: cellOriginY)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case militaryServiceYearsTxt:
            militaryServiceRoleTxt.becomeFirstResponder()
            break
        case militaryServiceRoleTxt:
            characteristicsTxt.becomeFirstResponder()
            break
        case characteristicsTxt:
            studiesTxt.becomeFirstResponder()
            break
        case studiesTxt:
            jobExperienceTxt.becomeFirstResponder()
            break
        case jobExperienceTxt:
            matchingJobsTxt.becomeFirstResponder()
            break
        case matchingJobsTxt:
            myYoutubeVideoTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case militaryServiceYearsTxt:
            limitLength = 2
            break
        case militaryServiceRoleTxt:
            limitLength = 100
            break
        case characteristicsTxt:
            limitLength = 150
            break
        case studiesTxt:
            limitLength = 150
            break
        case jobExperienceTxt:
            limitLength = 150
        case matchingJobsTxt:
            limitLength = 150
            break
        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
        
    }
    
    //upload file delegate
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        //uploadResumeBtn.setTitle("עלה בהצלחה", for: .normal)
        Global.sharedInstance.worker.workerResumeDetails.resume.nvFileName = imgName
        Global.sharedInstance.worker.workerResumeDetails.resume.nvFile = img
        
        uploadResumeBtn.setTitle(imgName, for: .normal)
        cancelFileBtn.isHidden = false
        uploadResumeBtn.setTitleColor(UIColor.lightGray, for: .normal)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    //base pop-up actions delegate
    func okAction(num: Int) {
        cancelFileBtn.isHidden = true
        uploadResumeBtn.setTitle("צרף קובץ", for: .normal)
        uploadResumeBtn.setTitleColor(ControlsDesign.sharedInstance.colorFucsia, for: .normal)
        Global.sharedInstance.worker.workerResumeDetails.resume.nvFileURL = ""
        Global.sharedInstance.worker.workerResumeDetails.resume.iFileId = -1
        
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
    
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם ברצונך למחוק את הקורות חיים?"
        
        openAnotherPageDelegate.openViewController!(VC: basePopUp)
    }
    
}
