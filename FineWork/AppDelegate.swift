//
//  AppDelegate.swift
//  FineWork
//
//  Created by Racheli Kroiz on 4.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
import Google
import GooglePlaces
import GoogleMaps
//import SwiftRangeSlider
import TTRangeSlider
//import JTAppleCalendar
import UserNotifications
import UserNotificationsUI


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate/*,GIDSignInDelegate*/ , UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    var googleApiKey = "AIzaSyD4gZA3NxGdexpQYM7jQa7c8-O0LLrslGo"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        if AppDelegate.isDeviceLanguageRTL()
        {
            Global.sharedInstance.rtl = true
        }
        else
        {
            Global.sharedInstance.rtl = false
        }
        
//        UIApplication.shared.userInterfaceLayoutDirection = UIUserInterfaceLayoutDirection.rightToLeft
//        
//        
//        UserDefaults().set
//        
        //initial google services
        GMSPlacesClient.provideAPIKey(googleApiKey)
        GMSServices.provideAPIKey(googleApiKey)
        
        //אתחול נוטיפקציות
      registerForPushNotifications()
//    //רק בינתיים בשביל שיריץ מהמעסיק
//        var mainView: UIStoryboard!
//        mainView = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
//        let viewcontroller : UIViewController = mainView.instantiateViewController(withIdentifier: "ChooseBankViewController") as! ChooseBankViewController
//         let navigationController = mainView.instantiateViewController(withIdentifier: "navEmployer")as!UINavigationController
//             self.window!.rootViewController = navigationController
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")

         return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
       
//        return true
    }
    //MARK: - Notifications
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else {
                    //self.checkLogin()
                    return
                }
                self.getNotificationSettings()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        } else {
            // Fallback on earlier versions
        }
    }
    //MARK: - Notification
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        Global.sharedInstance.token = token
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("receive notification")
        print(userInfo as! [String:AnyObject])
        Global.sharedInstance.receiveRemoteNotificationDelegate?.onReceiveRemoteNorificarion(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
            print("receive notification")
        print(userInfo as! [String:AnyObject])
      ///////////ReportPresenceViewController התחלתי לטפל בנוט' בעמוד
        Global.sharedInstance.receiveRemoteNotificationDelegate?.onReceiveRemoteNorificarion(userInfo: userInfo)
    }

    
    
    
//    func application(application: UIApplication,
//                     openURL url: NSURL, options: [String: AnyObject]) -> Bool {
//        return GIDSignIn.sharedInstance().handleURL(url,
//                                                    sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String,
//                                                    annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
//    }
//
    
//    func application(_ application: UIApplication,
//                     open url: URL,
//                     sourceApplication: String?,
//                     annotation: Any) -> Bool {
//        return FBSDKApplicationDelegate.sharedInstance().application(
//            application,
//            open: url as URL!,
//            sourceApplication: sourceApplication,
//            annotation: annotation)
//    }
    
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        if(url.scheme!.isEqual("fb739483156215140")) {
//            return FBSDKApplicationDelegate.sharedInstance().application(
//                application,
//                open: url as URL!,
//                sourceApplication: sourceApplication,
//                annotation: annotation)
//            
//        } else {
//            return GIDSignIn.sharedInstance().handle(url as URL!,
//            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!,
//            annotation: options[UIApplicationOpenURLOptionsKey.annotation])
//        }
//    }
    
    
//    func application(_ app: UIApplication,
//                     open url: URL,
//                     options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//        
//        if(url.scheme!.isEqual("fb739483156215140")) {
//            return FBSDKApplicationDelegate.sharedInstance().application(
//                application,
//                open: url as URL!,
//                sourceApplication: sourceApplication,
//                annotation: annotation)
//            
//        } else {
//            return GIDSignIn.sharedInstance().application(
//                application,
//                open: url as URL!,
//                sourceApplication: sourceApplication,
//                annotation: annotation)//.handle(url as URL!,
//                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!,
//                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
//        }
//    }
      
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        if (url.scheme!.isEqual("fb739483156215140")){
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        }
            return GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    class func isDeviceLanguageRTL() -> Bool {
        //  NSLocale.characterDirectionForLanguage(<#T##isoLangCode: String##String#>)
        return (NSLocale.characterDirection(forLanguage: NSLocale.preferredLanguages[0]) == .rightToLeft)
    }
    
//    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
//                withError error: NSError!) {
//        if (error == nil) {
//            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
//            // ...
//        } else {
//            print("\(error.localizedDescription)")
//        }
//    }
//    
//    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
//        if (error == nil) {
//            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
//            // ...
//        } else {
//            print("\(error.localizedDescription)")
//        }
//
//    }
//    
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//        if (error == nil) {
//            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
//            // ...
//        } else {
//            print("\(error.localizedDescription)")
//        }
//
//    }
    
    func setLoginRoot() {
        
        // switch back to view controller 1
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        
//        self.window?.rootViewController = loginViewController
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let nav1 = UINavigationController()
//        let mainView = ViewController(nibName: nil, bundle: nil) //ViewController = Name of your controller
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController

        nav1.viewControllers = [loginViewController]
        self.window!.rootViewController = nav1
        self.window?.makeKeyAndVisible()
    }
    
}

