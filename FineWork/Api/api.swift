//
//  api.swift
//  FineWork
//
//  Created by Racheli Kroiz on 4.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class api: NSObject {
    
    //DEV
//  var url: String = "http://qa.webit-track.com/FineWorkWSDev/"
    
    //QA
   var url: String  = "http://qa.webit-track.com/FineWorkBeta/"
    
//    var url: String = "http://qa.webit-track.com/FineWorkWSDev/Service.svc/"
    //var url: String = "http://qa.webit-track.com/FineWorkWS/Service.svc/"

    let manager = AFHTTPRequestOperationManager()
    
    typealias successBlock =  (AFHTTPRequestOperation?, Any?) -> Void
    
    typealias failureBlock = (AFHTTPRequestOperation? , Error?) ->Void

    static let sharedInstance : api = {
        let instance = api()
        return instance
        }()
    
    override init()
    {
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        
        manager.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
        
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as Set<NSObject>
    }
    func buldUrlFile(fileName:String)->String{
        var pathUrl = url + "Files/" + fileName
        pathUrl = pathUrl.replacingOccurrences(of: "\\", with: "/")
        return pathUrl
    }
    
    func buildUrl(path:String)->String
    {
        let pathUrl = url + "Service.svc/" + path
//        let pathUrl:String = (url as NSString).appending(path)
        
        return pathUrl
    }
    func post(_ URLString: String, parameters: Any, success: ((_ operation: AFHTTPRequestOperation, _ responseObject: AnyObject) -> Void)! , failure: ((_ operation: AFHTTPRequestOperation, _ error: Error) -> Void)!) -> AFHTTPRequestOperation {
        
        return AFHTTPRequestOperation()
        
    }
//MARK: - register user in server
    func Registration(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "Registration"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
                     failure: failure
        )
    }
    //MARK: - Check if exist Mail And Mobile
    func CheckValidMailAndMobile(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
        {
        let path = "CheckValidMailAndMobile"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
        failure: failure
        
        )
    }

    //MARK: - login user in server
    func Login(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "Login"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
                     failure: failure
    
        )
    }
    
    func UpdateImage(params:Dictionary<String,AnyObject>,success: (successBlock)!,failure:(failureBlock)!){
        let path = "UpdateImage"
        manager.post(self.buildUrl(path: path), parameters: params, success: success, failure: failure)
    }
    
    func GetImage(params:Dictionary<String,AnyObject>,success:(successBlock)!,failure:(failureBlock)!){
        let path = "GetImage"
        manager.post(self.buildUrl(path: path), parameters: params, success: success, failure: failure)
    }
    
    func GetWorker101FormCodeTables(params:Dictionary<String,AnyObject>,success:(successBlock)!,failure:(failureBlock)!){
        let path = "GetWorker101FormCodeTables"
        manager.post(self.buildUrl(path: path), parameters: params, success: success, failure: failure)
    }
    
    func VerificationCodeSMS(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "VerificationCodeSMS"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
                     failure: failure
            
        )
    }
    
    func checkSaveFile(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "checkSaveFile"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
                     failure: failure
        )
    }

    func InsertWorker101Form(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "InsertWorker101Form"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
                     failure: failure
            
        )
    }
    
    func InsertWorkerTaxRelifeReason(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "InsertWorkerTaxRelifeReason"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
                     failure: failure
            
        )
    }
    
    func InsertWorkerTaxCoordination(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "InsertWorkerTaxCoordination"
        
        manager.post(self.buildUrl(path: path), parameters: params, success: success,
                     failure: failure
            
        )
    }
    
    func GetWorker101Form(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "GetWorker101Form"
        manager.post(self.buildUrl(path: path), parameters: params, success: success,failure: failure)
    }
    
    //true = קיים ,false = לא קיים 
    func CheckExistsIdentityNumber(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "CheckExistsIdentityNumber"
        manager.post(self.buildUrl(path: path), parameters: params, success: success,failure: failure)
    }
    
    //if ErrorCode=0 יש שגיאות,ErrorCode=-1 תקין
    func CheckWorker101FormValidation(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!)
    {
        let path = "CheckWorker101FormValidation"
        manager.post(self.buildUrl(path: path), parameters: params, success: success,failure: failure)
    }
    
    func ForgotPassword(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!){
        let path = "ForgotPassword"
        manager.post(self.buildUrl(path: path), parameters: params, success: success,failure: failure)
    }
    
    //MARK: - General
    func goServer(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!, funcName : String) {
        //let path = "GetWorker"
        manager.post(self.buildUrl(path: funcName), parameters: params, success: success,failure: failure)
    }
    
    //MARK: - Functions Names
    var GetWorkerRoleSysTables = "GetWorkerRoleSysTables"
    var GetBankAccountGuide = "GetBankAccountGuide"
    var GetWorkerDomains = "GetWorkerDomains"
    var UpdateWorkerDomain = "UpdateWorkerDomain"
    var GetWorkerAskingLocations = "GetWorkerAskingLocations"
    var UpdateWorkerAskingLocation = "UpdateWorkerAskingLocation"
    var GetEmployerCount = "GetEmployerCount"
    var GetWorkerAvailabilty = "GetWorkerAvailabilty"
    var UpdateWorkerAvailabilty = "UpdateWorkerAvailabilty"
    
    // employer
    var GetEmployerCodeTables = "GetEmployerCodeTables"
    var GetEmployer = "GetEmployer"
    var UpdateEmployer = "UpdateEmployer"
    var GetAuthorizedEmploy = "GetAuthorizedEmploy"
    var UpdateAuthorizedEmploy = "UpdateAuthorizedEmploy"
    
    // worker home page
    var GetWorkerHomePage = "GetWorkerHomePage"
    var GetWorkerOfferedJobs = "GetWorkerOfferedJobs"
    var UpdateWorkerOfferedJob = "UpdateWorkerOfferedJob"
    var UpdateWorkerOfferedJobStatus = "UpdateWorkerOfferedJobStatus"
    var UpdateWorkerOfferedJobBaloonOccur = "updateWorkerOfferedJobBaloonOccur"
    var UpdateEmployerJobBaloonOccur = "updateEmployerJobBaloonOccur"
    
    // search worker
    var GetEmployerJob = "GetEmployerJob"
    var UpdateEmployerJob = "UpdateEmployerJob"
    var GetWorkerByEmploymentDomainRole = "GetWorkerByEmploymentDomainRole"
    var GetGlobalParameters = "GetGlobalParameters"
}
