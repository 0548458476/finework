//
//  SearchInViewTableViewCell.swift
//  FineWork
//
//  Created by Racheli Kroiz on 30/07/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SearchInViewTableViewCell: UITableViewCell  {
    
    
    //MARK:- Outlet
    
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var domain: UILabel!
    @IBOutlet weak var dateStart: UILabel!
    @IBOutlet weak var iconArrow: UILabel!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var hourlyWageTitle: UILabel!
    @IBOutlet weak var hourlyWage: UILabel!
    @IBOutlet weak var sumWageTitle: UILabel!
    @IBOutlet weak var sumWage: UILabel!
    @IBOutlet weak var dateEndTitle: UILabel!
    @IBOutlet weak var dateEnd: UILabel!
    @IBOutlet weak var populationTitle: UILabel!
    @IBOutlet weak var population: UILabel!
    @IBOutlet weak var scheduleTypeTitle: UILabel!
    @IBOutlet weak var scheduleType: UILabel!
    @IBOutlet weak var bonusTitle: UILabel!
    @IBOutlet weak var bonus: UILabel!
    @IBOutlet weak var addressTitle: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var desciption: UILabel!
    @IBOutlet weak var riskTitle: UILabel!
    @IBOutlet weak var risk: UILabel!
    @IBOutlet weak var safetyRequirementTitle: UILabel!
    @IBOutlet weak var safetyRequirement: UILabel!
    @IBOutlet weak var physicalDifficultyRankTitle: UILabel!
    @IBOutlet weak var physicalDifficultyRank: UILabel!
    @IBOutlet weak var workSpaceTitle: UILabel!
    @IBOutlet weak var workSpace: UILabel!
    @IBOutlet weak var jobNewWorkerTitle: UILabel!
    @IBOutlet weak var jobNewWorker: UILabel!
    @IBOutlet weak var lastWorkerView: UIView!
    @IBOutlet weak var lastWorkerBtn: UIView!
    @IBOutlet weak var lastWorkerTableView: UITableView!
    @IBOutlet weak var searchWorkerKnowBtn: UIButton!
    @IBOutlet weak var searchWorkerKnowTxt: UILabel!
    @IBOutlet weak var searchWorkerNewBtn: UIButton!
    @IBOutlet weak var searchWorkerNewTxt: UILabel!
    
    @IBOutlet weak var addressConsrain: NSLayoutConstraint!
    
    @IBOutlet weak var searchKnowWorkerConstain: NSLayoutConstraint!
    
    @IBOutlet weak var searchNewWorkerConstain: NSLayoutConstraint!
    //MARK:- Action
    
    @IBAction func serchKnowWorke(_ sender: UIButton) {
        
        searchWorker(isNewWorker: false , jobId: savedOrExistsEmployerJob.iEmployerJobId)
    }
    
    @IBAction func serchNewWorker(_ sender: Any) {
        searchWorker(isNewWorker: true , jobId: savedOrExistsEmployerJob.iEmployerJobId)
    }
    
    
    @IBAction func iconArrow(_ sender: Any) {
        //        if detailsView.isHidden == true{
        //            detailsView.isHidden = false
        //            searchWorkerDel?.changeHeight(position: position ,  isUpdate : true)
        //        }
        //        else{
        //            detailsView.isHidden = true
        //            searchWorkerDel?.changeHeight(position: position ,  isUpdate : true)
        //        }
        print("ghgh")
        
    }
    //MARK:- Variable
    var searchWorkerDel :    SearchNewOrKnowWorkerDelegate?

    var position : Int = 0
    var savedOrExistsEmployerJob = SavedOrExistsEmployerJob()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconArrow.text = FontAwesome.sharedInstance.arrow
        
        searchWorkerNewBtn.layer.cornerRadius = searchWorkerNewBtn.frame.height / 2
        searchWorkerNewBtn.layer.masksToBounds = true
        
        searchWorkerKnowBtn.layer.cornerRadius = searchWorkerKnowBtn.frame.height / 2
        searchWorkerKnowBtn.layer.masksToBounds = true
        
        
               lastWorkerBtn.isUserInteractionEnabled = true
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(openLastWorker))
        lastWorkerBtn.addGestureRecognizer(tapGestureRecognizer1)
        

    }
    
    
    
    func setDisplayData(savedOrExistsEmployerJob : SavedOrExistsEmployerJob? , isSave : Bool ,position : Int){
        print ("mmm setDisplayData")
        if(savedOrExistsEmployerJob != nil){
            
            
            self.position = position
            self.savedOrExistsEmployerJob = savedOrExistsEmployerJob!
            role.text = savedOrExistsEmployerJob?.nvDomain
            domain.text = savedOrExistsEmployerJob?.nvRole
            dateStart.text = Global.sharedInstance.getStringFromDateString(dateString: savedOrExistsEmployerJob!.dtJobStartDate)
            print ("indexPath aaa \(savedOrExistsEmployerJob!.isShowDetails)")
            
            hourlyWage.text =  String(describing: savedOrExistsEmployerJob!.mHourlyWage)
            sumWage.text =  String(describing: savedOrExistsEmployerJob!.mSumWage)
            dateEnd.text = Global.sharedInstance.getStringFromDateString(dateString: savedOrExistsEmployerJob!.dtJobEndDate)
            population.text = savedOrExistsEmployerJob?.nvEmployerJobPopulation
            scheduleType.text = savedOrExistsEmployerJob?.nvEmployerJobPeriodType
            if (savedOrExistsEmployerJob?.bBonusOption)! {
                bonusTitle.isHidden = false
            }else{
                bonusTitle.isHidden = true
              //  addressConsrain.constant = 10
                //              //  searchWorkerDel?.changeHeight(position: -1, isUpdate: false)
                //
            }
            address.text = savedOrExistsEmployerJob?.nvAddress
            desciption.text = savedOrExistsEmployerJob?.nvJobDescription
            if(savedOrExistsEmployerJob?.bRisk)!{
                risk.text = "יש"
            }else{
                risk.text = "אין"
            }
            if(savedOrExistsEmployerJob?.bSafetyRequirement)!{
                safetyRequirement.text = "יש"
            }else{
                safetyRequirement.text = "אין"
            }
            physicalDifficultyRank.text = String (describing: savedOrExistsEmployerJob!.iPhysicalDifficultyRank)
            workSpace.text = savedOrExistsEmployerJob?.nvWorkSpaceDetails
            
            var s: String = ""
            if savedOrExistsEmployerJob?.employerJobNewWorker.nvExperienceDetails != "" {
                s = "נסיון: \(savedOrExistsEmployerJob!.employerJobNewWorker.nvExperienceDetails) "
                
            }
            if savedOrExistsEmployerJob?.employerJobNewWorker.nvLanguageDetails != "" {
                s += "שפה: \(savedOrExistsEmployerJob!.employerJobNewWorker.nvLanguageDetails) "
                
            }
            if (savedOrExistsEmployerJob?.employerJobNewWorker.bDiploma)!  {
                s += " דיפלומה "
                
            }
            if savedOrExistsEmployerJob?.employerJobNewWorker.nvTimeDescription != ""{
                s += "וותק: \(savedOrExistsEmployerJob!.employerJobNewWorker.nvTimeDescription) "
                
            }
            if savedOrExistsEmployerJob?.employerJobNewWorker.nvProfessionalTimeDetails != ""{
                s += "וותק מקצועי: \(savedOrExistsEmployerJob!.employerJobNewWorker.nvProfessionalTimeDetails) "
                
            }
            if (savedOrExistsEmployerJob?.employerJobNewWorker.bPhoneInterview)!  {
                s += " נדרש ראיון טלפוני"
                
            }
            if (savedOrExistsEmployerJob?.employerJobNewWorker.bWorkedWorkerPreference)!  {
                s += " מעדיף עובדים שכבר עבדו אצלי בעבר"
                
            }
            if s != ""
            {
                jobNewWorker.text = s
            }else{
                jobNewWorker.isHidden = true
                jobNewWorkerTitle.isHidden = true
            }
            
            if !isSave {
                //
                lastWorkerView.isHidden = true
                searchNewWorkerConstain.constant = 50
                searchKnowWorkerConstain.constant = 50
                // //searchWorkerDel?.changeHeight(position: position ,  isUpdate : false)
            }   else{
                lastWorkerTableView.isHidden = true
               initInnerTable()
            }
        }
    }
    func setColor(){
        
        searchWorkerNewBtn.backgroundColor = UIColor.myFucsia
        searchWorkerKnowBtn.backgroundColor = UIColor.myFucsia
        
    }
    
    func searchWorker(isNewWorker : Bool , jobId : Int){
        searchWorkerDel?.searchNewOrKnow(isNewWorker: isNewWorker, idJob: jobId)
    }
    func initInnerTable(){
        lastWorkerTableView.dataSource = self
        lastWorkerTableView.delegate = self
        lastWorkerTableView.reloadData()
        
    }
    //MARK:- Tap
    func openLastWorker(){
        if lastWorkerTableView.isHidden == true {
            lastWorkerTableView.isHidden = false
        }else {
            lastWorkerTableView.isHidden = true
           let i = IndexPath(item: 0, section: 0)
let currentCell1 = lastWorkerTableView.cellForRow(at: i) as! EmployerJobWorkerTableViewCell
            currentCell1 .uplodeImg()
            lastWorkerTableView.beginUpdates()
            lastWorkerTableView.endUpdates()

        }

    }
}

    extension SearchInViewTableViewCell : UITableViewDelegate, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return savedOrExistsEmployerJob.lSavedOrExistsEmployerJobWorker.count
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : UITableViewCell
            
            
       cell =  tableView.dequeueReusableCell(withIdentifier: "EmployerJobWorkerTableViewCell", for: indexPath as IndexPath) as! EmployerJobWorkerTableViewCell

          (cell as! EmployerJobWorkerTableViewCell).setDisplayData(savedOrExistsEmployerJobWorker: savedOrExistsEmployerJob.lSavedOrExistsEmployerJobWorker[indexPath.row], position: indexPath.row)
            (cell as! EmployerJobWorkerTableViewCell).searchWorkerInnerDel = self

            return cell
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
          
                    }
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return 0.5
        }
        

}

extension SearchInViewTableViewCell : SearchNewOrKnowWorkerDelegate {
    func searchNewOrKnow(isNewWorker:Bool,idJob:Int){
          }
    func searchNewOrKnow(isNewWorker:Bool,idJob:Int,number:String){
        
        searchWorkerDel?.searchNewOrKnow(isNewWorker: false, idJob: savedOrExistsEmployerJob.iEmployerJobId, number: number)
    }
    func changeHeight(position : Int ,  isUpdate : Bool){
           }

}
