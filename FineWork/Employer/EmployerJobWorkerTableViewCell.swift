//
//  EmployerJobWorkerTableViewCell.swift
//  FineWork
//
//  Created by User on 28/08/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmployerJobWorkerTableViewCell: UITableViewCell {
    
    //MARK:- Outlet

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var invate: UILabel!
    @IBOutlet weak var dateTxt: UILabel!
    
    //MARK:- Action

    @IBAction func invateBtn(_ sender: Any) {
        searchWorkerInnerDel?.searchNewOrKnow(isNewWorker: true, idJob: 0, number: savedOrExistsEmployerJobWorker.nvMobile)

    }
    //MARK:- Varable
    var searchWorkerInnerDel : SearchNewOrKnowWorkerDelegate?
    var savedOrExistsEmployerJobWorker = SavedOrExistsEmployerJobWorker()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print ("aaa awakeFromNib")
        if savedOrExistsEmployerJobWorker.image == nil {
//            DispatchQueue.main.async { () -> Void in
//                
//                if self.savedOrExistsEmployerJobWorker.nvImageFilePath != "" {
//                    var stringPath = api.sharedInstance.buldUrlFile(fileName: (self.savedOrExistsEmployerJobWorker.nvImageFilePath))
//                    stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
//                    let url = URL(string: stringPath)
//                    let data = try? Data(contentsOf: url!)
//                    self.savedOrExistsEmployerJobWorker.image = UIImage(data: data!)
//                    self.img.image =  self.savedOrExistsEmployerJobWorker.image
//                }
//            }
        }else {
            self.img.image =  savedOrExistsEmployerJobWorker.image
            
        }
    

    }
    override func layoutSubviews() {
        print ("aaa layoutSubviews")

        img.layer.cornerRadius = img.frame.height / 2
        img.layer.masksToBounds = true
        
           }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func  setDisplayData(savedOrExistsEmployerJobWorker : SavedOrExistsEmployerJobWorker , position : Int ){
        print ("aaa setDisplayData")
        self.savedOrExistsEmployerJobWorker = savedOrExistsEmployerJobWorker
                  name.text = savedOrExistsEmployerJobWorker.nvUserName
            dateTxt.text = Global.sharedInstance.getStringFromDateString(dateString: savedOrExistsEmployerJobWorker.dtDate)
       
    }
    
    func uplodeImg(){
        if savedOrExistsEmployerJobWorker.image == nil {
                        DispatchQueue.main.async { () -> Void in
            
                            if self.savedOrExistsEmployerJobWorker.nvImageFilePath != "" {
                                var stringPath = api.sharedInstance.buldUrlFile(fileName: (self.savedOrExistsEmployerJobWorker.nvImageFilePath))
                                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                                let url = URL(string: stringPath)
                                let data = try? Data(contentsOf: url!)
                                self.savedOrExistsEmployerJobWorker.image = UIImage(data: data!)
                                self.img.image =  self.savedOrExistsEmployerJobWorker.image
                            }
                        }
        }else {
            self.img.image =  savedOrExistsEmployerJobWorker.image
            
        }
 
    }
}
