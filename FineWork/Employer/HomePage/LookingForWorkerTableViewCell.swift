//
//  WorkerOfferedJobTableViewCell.swift
//  FineWork
//
//  Created by User on 02/11/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class LookingForWorkersTableViewCell: UITableViewCell {

    
    //MARK: - Views
    @IBOutlet weak var domainRoleTxt: UILabel!
    @IBOutlet weak var nameWorkerTxt: UILabel!
    @IBOutlet weak var statusTxt: UILabel!
    @IBOutlet weak var imgJob: UIImageView!
    @IBOutlet weak var chengeFavorite: UIButton!
    @IBOutlet weak var dateJob: UILabel!
    @IBOutlet weak var timeJob: UILabel!
    @IBOutlet weak var notificationNumber: UILabel!
    @IBOutlet weak var notificationView: UIView!
    
        //MARK: - Action
    @IBAction func openJobDetailsAction(_ sender: Any) {
        jobsToEmployeeDelegates.openJobDatailsDel(index: indexPath!)
    }
   
    @IBAction func chengeFavoriteAction(_ sender: Any) {
        jobsToEmployeeDelegates.startFavoraiteDel(index: indexPath!)

    }
    @IBAction func openWorkerDetailsAction(_ sender: Any) {
        jobsToEmployeeDelegates.openEmployerDatailsDel(index: indexPath!)

    }
    // @IBOutlet weak var openWorkerDetailsAction: UIButton!
    var indexPath : IndexPath? = nil
    var  jobsToEmployeeDelegates : JobsToEmployeeDelegates!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        imgJob.layer.cornerRadius = imgJob.frame.height / 2
        imgJob.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool ) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDisplayData(LookingForYouWorker : LookingForYouWorkers ,  IndexPath: IndexPath)
    {
        indexPath = IndexPath
        domainRoleTxt.text = LookingForYouWorker.nvRole
        statusTxt.text = LookingForYouWorker.nvEmployerAskedJobStatusType
        imgJob.image = LookingForYouWorker.logoImage
      let dateFormat = Global.sharedInstance.getStringFromDateString(dateString: LookingForYouWorker.dtJobStartDate!)
        let dateSplit = dateFormat.components(separatedBy: "/")
        dateJob.text = dateSplit[0] + "/" + dateSplit[1]
        timeJob.text = LookingForYouWorker.tFromHoure + "-" + LookingForYouWorker.tToHoure
        if LookingForYouWorker.iBaloonOccur > 0 {
            notificationNumber.text = String(LookingForYouWorker.iBaloonOccur)
            notificationView.isHidden = false
        }else{
            notificationView.isHidden = true
        }
        if  LookingForYouWorker.nvWorkerName.count > 0 {
            nameWorkerTxt.text = LookingForYouWorker.nvWorkerName
            nameWorkerTxt.isHidden = false
        }else {
            nameWorkerTxt.isHidden = true
        }
    }
}
