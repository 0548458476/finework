//
//  MoreCandidateTableViewCell.swift
//  FineWork
//
//  Created by User on 19/11/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class MoreCandidateTableViewCell: UITableViewCell {
    
    //MARK: - Views
    
    @IBOutlet weak var nameCandidate: UILabel!
    @IBOutlet weak var amountToHouers: UILabel!
    @IBOutlet weak var matchPrecent: UILabel!
    
    //MARK: - Variable
    var indexPath : IndexPath? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDisplayData(candidateFound : CandidateFound ,  IndexPath: IndexPath)
    {
        indexPath = IndexPath
      nameCandidate.text = candidateFound.nvWorkerName
        amountToHouers.text = String( "\(candidateFound.mAskingHourlyWage!)שח לשעה")
        matchPrecent.text = String( "\(candidateFound.iMatchPercentage)% התאמה")
    }
}
