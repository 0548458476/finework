//
//  EmploymentAuthorizedTableViewCell.swift
//  FineWork
//
//  Created by User on 23.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmploymentAuthorizedTableViewCell: UITableViewCell, BasePopUpActionsDelegate {

    //MARK: - Views
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var balanceLbl: UILabel!
    @IBOutlet weak var amountAllocatedLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    //MARK: - Actions
    @IBAction func removeBtnAction(_ sender: Any) {
        openBasePopUp()
        
    }
    @IBAction func editBtnAction(_ sender: Any) {
        cellOptionsDelegate.editCell!(cellNumber: cellNumber)
    }
    
    //MARK: - Variables
    var cellNumber : Int = -1
    var cellOptionsDelegate : CellOptionsDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        editBtn.setImage(UIImage(named: "edit-pen"), for: UIControlState.normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(authorizedEmploy : AuthorizedEmploy, _cellNumber : Int) {
        cellNumber = _cellNumber
        
        userNameLbl.text = authorizedEmploy.user.nvUserName
        if authorizedEmploy.mAmount != nil {
            amountAllocatedLbl.text = String(Int(authorizedEmploy.mAmount!))
        }
    }
    
    //MARK: - Base Pop-Up Actions Delegate
    func okAction(num: Int) {
        cellOptionsDelegate.removeCell!(cellNumber: cellNumber)
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך להסיר את המורשה עסקה ?"
        
        openAnotherPageDelegate.openViewController!(VC: basePopUp)
    }


}
