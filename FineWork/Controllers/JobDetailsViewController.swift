//
//  JobDetailsViewController.swift
//  FineWork
//
//  Created by User on 20.6.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
protocol JobDetailsDelegate{
    func viewAllDetailsDel( checked :Bool)
    
}
class JobDetailsViewController: UIViewController , UIScrollViewDelegate {
    
    //MARK: view
    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var baseViewHeughtConstrain: NSLayoutConstraint!
    @IBOutlet weak var viewImgAndTitle: UIView!
    @IBOutlet weak var imgBussines: UIImageView!
    @IBOutlet weak var numJob: UILabel!
    @IBOutlet weak var desciption: UILabel!
    @IBOutlet weak var showInDetails: UILabel!
    @IBOutlet weak var statusAboveDetails: UILabel!
    @IBOutlet weak var statusTitleView: UIView!
    @IBOutlet weak var arrow_icon: UILabel!
    @IBOutlet weak var viewAllDetails: UIView!
    @IBOutlet weak var viewAllDetailsTopConstrain: NSLayoutConstraint!
    @IBOutlet weak var domainTitle: UILabel!
    @IBOutlet weak var domainContent: UILabel!
    @IBOutlet weak var roleTitle: UILabel!
    @IBOutlet weak var roleContent: UILabel!
    @IBOutlet weak var desciptionTitle: UILabel!
    @IBOutlet weak var desciptionContent: UILabel!
    @IBOutlet weak var periodTitle: UILabel!
    @IBOutlet weak var periodType: UILabel!
    @IBOutlet weak var populationsToTitle: UILabel!
    @IBOutlet weak var populationsTo: UILabel!
    @IBOutlet weak var addressTitle: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var addressIcon: UIButton!
    @IBOutlet weak var dateBeginTitle: UILabel!
    @IBOutlet weak var dateBegin: UILabel!
    @IBOutlet weak var safetyRequirementTitle: UILabel!
    @IBOutlet weak var safetyRequirement: UILabel!
    @IBOutlet weak var riksTitle: UILabel!
    @IBOutlet weak var riks: UILabel!
    @IBOutlet weak var physicalDifficultyRankTitle: UILabel!
    @IBOutlet weak var physicalDifficultyRank: UILabel!
    @IBOutlet weak var workSpsceTitle: UILabel!
    @IBOutlet weak var workSpsce: UILabel!
    @IBOutlet weak var showDetails: UILabel!
    
    
    @IBOutlet weak var viewWithCalander: UIView!
    @IBOutlet weak var viewWithCalanserHeightConstrin: NSLayoutConstraint!
    @IBOutlet weak var arrowNext: UILabel!
    @IBOutlet weak var arrowPrev: UILabel!
    @IBOutlet weak var cuurentMonthName: UILabel!
    @IBOutlet weak var viewTitleDay: UIView!
    @IBOutlet weak var collectionCalander: JTAppleCalendarView!
    
    @IBOutlet weak var viewWitoutCalander: UIView!
    @IBOutlet weak var viewWithoutCalanserHeightConstrin: NSLayoutConstraint!

    @IBOutlet weak var fromeDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var fromeHoures: UITextField!
    @IBOutlet weak var toHoures: UITextField!
    
    //MARK: Action
    
    
    @IBAction func addressShow(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressMapViewController") as! AddressMapViewController
        vc.setDisplayData(_lat: workerOfferedJob.nvLat , _lng: workerOfferedJob.nvLng , title: "\(workerOfferedJob.nvDomain) - \(workerOfferedJob.nvRole)")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func showDetailAction(_ sender: Any) {
        print("showDetails")
        let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
        let vc = workerStory.instantiateViewController(withIdentifier: "EmployerDetailsPopUpViewController") as! EmployerDetailsPopUpViewController
        vc.setDisplayData(index: index)
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func showAllDetails(_ sender: Any) {
        if viewAllDetails.isHidden {
            arrow_icon.text = FontAwesome.sharedInstance.arrow_up
        }else{
            arrow_icon.text = FontAwesome.sharedInstance.arrow_bottom
        }
        viewAllDetails.isHidden = !viewAllDetails.isHidden
        statusAboveDetails.isHidden = !statusAboveDetails.isHidden
        

        jobDetailsDel.viewAllDetailsDel(checked: true)
    }
    
    //MARK: Object
    var workerOfferedJob : WorkerOfferedJob!
    var index : Int = 0
    var jobDetailsDel  : JobDetailsDelegate!
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var numberOfRows = 6
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    let firstDayOfWeek: DaysOfWeek = .sunday
    var hasStrictBoundaries = true
    var monthSize: MonthSize? = nil
    var myDate: Date? = Date()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCalander()
        setDisplayData()
        arrow_icon.text = FontAwesome.sharedInstance.arrow_bottom

    }
    
    override func viewDidLayoutSubviews() {
        imgBussines.layer.cornerRadius = imgBussines.frame.height / 2
        imgBussines.layer.masksToBounds = true
         imgBussines.contentMode = UIViewContentMode.scaleAspectFill
        
        Global.sharedInstance.setButtonCircle(sender: addressIcon, isBorder: true)
        addressIcon.setTitle(FlatIcons.sharedInstance.location, for: .normal)
        scroll.isScrollEnabled = true
       

      
    }
    func setDisplayData(index: Int){
        workerOfferedJob =  Global.sharedInstance.lWorkerOfferedJobs[index]
        print(workerOfferedJob.nvEmployerName)
    }
    func setDisplayData(){
        numJob.text = "מספר משרה: \(workerOfferedJob.iEmployerJobId)/ \(workerOfferedJob.iWorkerOfferedJobId)"
        imgBussines.downloadedFrom(link: api.sharedInstance.buldUrlFile(fileName: workerOfferedJob.nvLogoImageFilePath) )
        desciption.text = workerOfferedJob.nvJobDescription
        statusAboveDetails.text = workerOfferedJob.nvOfferJobStatusName
        domainContent.text = workerOfferedJob.nvDomain
        roleContent.text = workerOfferedJob.nvRole
        desciptionContent.text = workerOfferedJob.nvJobDescription
        periodType.text = workerOfferedJob.nvEmployerJobPeriodType
        populationsTo.text = workerOfferedJob.nvEmployerJobPopulations
        address.text = workerOfferedJob.nvAddress
        dateBegin.text = "\(Global.sharedInstance.getStringFromDateString(dateString: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].dFromDate)) \(workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].tFromHoure)"
        
//        print("date111 \(Global.sharedInstance.getStringFromDateString(dateString: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].dFromDate)) \(workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].tFromHoure)")
//
        if workerOfferedJob.bSafetyRequirement{
            safetyRequirement.text = workerOfferedJob.nvSafetyRequirementDetails
        }
            
        else{
            safetyRequirement.text = "אין"
        }
        
        if workerOfferedJob.bRisk == true {
            riks.text = workerOfferedJob.nvRiskDetails
        }else {
            riks.text = "אין"
        }
        physicalDifficultyRank.text = "\(workerOfferedJob.iPhysicalDifficultyRank) מתוך 10"
        
        workSpsce.text = workerOfferedJob.nvWorkSpaceDetails
        
        showDetails.text = "לצפיה בפרופיל של " + workerOfferedJob.nvEmployerName
        
        if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.new.rawValue || workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.interesting.rawValue {
            statusTitleView.isHidden = true
            viewWitoutCalander.isHidden = true
            viewWithCalander.isHidden = true
            viewWithCalanserHeightConstrin.constant = 0
           
        }   else{
            viewAllDetails.isHidden = true
        //set date in calander or without by iEmployerJobScheduleType
        if workerOfferedJob.iEmployerJobScheduleType != EmployerJobScheduleType.ConstantHoursJob.rawValue {
            baseViewHeughtConstrain.constant = baseView.frame.height + viewWithCalander.frame.height

            if workerOfferedJob.iEmployerJobScheduleType == EmployerJobScheduleType.OneTimeJob.rawValue {
            }
            
            // initCalander()
            
            
        }else{
            baseViewHeughtConstrain.constant = baseView.frame.height + viewWitoutCalander.frame.height

            
            viewWitoutCalander.isHidden = false
            viewWithCalander.isHidden = true
            viewWithCalanserHeightConstrin.constant = 0
            fromeDate.text = Global.sharedInstance.getStringFromDateString(dateString: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].dFromDate)
            toDate.text = Global.sharedInstance.getStringFromDateString(dateString: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].dToDate!)
            fromeHoures.text = workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].tFromHoure
            toHoures.text = workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].tToHoure
            
        }
        }
        
        //add Tap
        
        //  let showDetailsTap = UITapGestureRecognizer(target:self, action:#selector(self.showDetailsTapFunc))
        //  showDetails.addGestureRecognizer(showDetailsTap)
        
        let arrowNextTap = UITapGestureRecognizer(target:self, action:#selector(self.chengeNext))
        arrowNext.addGestureRecognizer(arrowNextTap)
        
        let arrowPrevTap = UITapGestureRecognizer(target:self, action:#selector(self.chengePrev))
        arrowPrev.addGestureRecognizer(arrowPrevTap)
    }
    
 
    
    //MARK: - Tap
    func chengeNext(){
        collectionCalander.scrollToSegment(SegmentDestination.next)
    }
    func chengePrev(){
        collectionCalander.scrollToSegment(SegmentDestination.previous)
    }
    
    func showDetailsTapFunc(){
        //        print("showDetails")
        //        let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
        //        let vc = workerStory.instantiateViewController(withIdentifier: "EmployerDetailsPopUpViewController") as! EmployerDetailsPopUpViewController
        //        vc.setDisplayData(index: index)
        //        self.navigationController?.pushViewController(vc, animated: true)
        //
    }
    
    func getDisplayData(index: Int){
        workerOfferedJob =  Global.sharedInstance.lWorkerOfferedJobs[index]
        self.index = index
        print(workerOfferedJob.nvEmployerName)
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        //    barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "\(workerOfferedJob.nvDomain) - \(workerOfferedJob.nvRole)"
        
    }
    
    
    
    func initCalander(){
        // Do any additional setup after loading the view.
        collectionCalander.transform = CGAffineTransform(scaleX: -1, y: 1)
        //        getDetailsFromDicMonth(BShowFullSchedule: btnRadioButton.isSelected)
        //        getHolidays()
        collectionCalander.calendarDataSource = self as? JTAppleCalendarViewDataSource
        collectionCalander.calendarDelegate = self as? JTAppleCalendarViewDelegate
        
        collectionCalander.minimumLineSpacing = 0
        
        collectionCalander.scrollToMonthOfDate(Date())
        
        collectionCalander.register(UINib(nibName: "PinkSectionHeaderView", bundle: Bundle.main),
                                    forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                    withReuseIdentifier: "PinkSectionHeaderView")
        
        self.collectionCalander.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            //   self.setupViewsOfCalendar(from: visibleDates, isEndScroll: <#Bool#>)
            self.collectionCalander.layer.borderWidth = 0.3
            
        }
    }
    
    //MARK: - Calendar Functions
    var prevSection = 0
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo, isEndScroll: Bool) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
        print("month : \(monthName)")
        cuurentMonthName.text = "\(monthName) \(year)"
        print("\(year)")
        if isEndScroll {
            collectionCalander.scrollToSection()
            // changeScrollViewDelegate.changeScrollViewHeight(heightToadd: 0)
        }
    }
    
    
    var prePostVisibility: ((CellState,JobDayCalanderCollectionViewCell?)->())?
    func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        prePostVisibility?(cellState, cell as? JobDayCalanderCollectionViewCell)
        //calendarView.reloadData()
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? JobDayCalanderCollectionViewCell  else {
            return
        }
        if testCalendar.isDateInToday(cellState.date){
            myCustomCell.backgroundColor = UIColor.lightGray
        }
    }
    
    var flagH = false
    func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? JobDayCalanderCollectionViewCell else {return }
        
        var currentTimeAvailablity : WorkerOfferedJobTimeAvailablity? = nil
        
        if workerOfferedJob.lWorkerOfferedJobTimeAvailablity.count > 0{
            flagH = false
            
            for time in workerOfferedJob.lWorkerOfferedJobTimeAvailablity {
                let date1 = Global.sharedInstance.getStringFromDateString(dateString: time.dFromDate)
                
                if date1 == cellState.date.getStringFromDate(today: cellState.date) {
                    self.flagH = true
                    currentTimeAvailablity = time
                    time.date = cellState.date
                    break
                }
            }
            
        }
        
        if currentTimeAvailablity != nil {
            
            
            if cellState.isSelected && cellState.dateBelongsTo == .thisMonth {
                
                myCustomCell.handleCellSelection(cellState: cellState, timeAvailablity: currentTimeAvailablity!, statusDateInCalander: StatusDateInCalander.yesSelected.rawValue , statusJob: workerOfferedJob.iOfferJobStatusType ,position :0 )
                myDate = cellState.date
                
            }else{
                myCustomCell.handleCellSelection(cellState: cellState, timeAvailablity: currentTimeAvailablity!, statusDateInCalander: StatusDateInCalander.noSelected.rawValue , statusJob: workerOfferedJob.iOfferJobStatusType , position :0)
            }
        } else {
            
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.handleCellSelection(cellState: cellState,  statusDateInCalander: StatusDateInCalander.regularInMonth.rawValue)
            } else {
                myCustomCell.handleCellSelection(cellState: cellState, statusDateInCalander: StatusDateInCalander.regularNoInMonth.rawValue)
                
            }
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
    }
    
}

// MARK : JTAppleCalendarDelegate
extension JobDetailsViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource{
    
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.timeZone = testCalendar.timeZone
        formatter.locale = testCalendar.locale
        
        
        
        let date = Date()
        var components = DateComponents()
        components.setValue(-10, for: Calendar.Component.year)
        let startDate = Calendar.current.date(byAdding: components, to: date)
        components.setValue(10, for: Calendar.Component.year)
        let endDate = Calendar.current.date(byAdding: components, to: date)
        
        
        //        let endDate = /*formatter.date(from: "2018 02 01")!*/Date(timeIntervalSinceNow: 30)
        
        let parameters = ConfigurationParameters(startDate: startDate!,
                                                 endDate: endDate!,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: firstDayOfWeek,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "JobDayCalanderCollectionViewCell", for: indexPath) as! JobDayCalanderCollectionViewCell
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
        
       // print("ppppp \(workerOfferedJob.iOfferJobStatusType)")
        ( myCustomCell as JobDayCalanderCollectionViewCell).setDisplayData(cellState: cellState, status: workerOfferedJob.iOfferJobStatusType/*, timeAvailablity: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[indexPath.row]*/)
        
        myCustomCell.frame.size = CGSize(width: self.collectionCalander.frame.width / 7 + 15 , height: 50)

       // myCustomCell.frame.size = CGSize(width: 65 , height: 65)
        //cell.frame.size = CGSize(width: 180 , height: self.collectionView.frame.height / 4 + 8)
        
//        if indexPath.row % 2 == 0{
           // myCustomCell.frame.origin.x = -5
//        }
//        else{
//            myCustomCell.frame.origin.x = myCustomCell.frame.width + 5
//            //self.view.frame.width / 2
//        }

        
        return myCustomCell
    }
    //    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        return CGSize(width: CGFloat((collectionView.frame.size.width / 3) - 20), height: CGFloat(100))
    //    }
    
    
    
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
        print (date)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates , isEndScroll: false)
        print("didScrollToDateSegmentWith")
    }
    
    func scrollDidEndDecelerating(for calendar: JTAppleCalendarView) {
        
        let visibleDates = collectionCalander.visibleDates()
        self.setupViewsOfCalendar(from: visibleDates, isEndScroll: true)
        print("scrollDidEndDecelerating")
    }
    
    func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
        let stride = collectionCalander.frame.width * CGFloat(indexPath.section)
        return CGRect(x: stride + 5, y: 5, width: collectionCalander.frame.width - 10, height: collectionCalander.frame.height - 10)
    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return monthSize
        
    }
    
}
