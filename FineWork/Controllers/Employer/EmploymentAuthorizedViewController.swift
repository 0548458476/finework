//
//  EmploymentAuthorizedViewController.swift
//  FineWork
//
//  Created by User on 23.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmploymentAuthorizedViewController: UIViewController, CellOptionsDelegate, UITableViewDelegate, UITableViewDataSource, SaveEmployerDelegate, OpenAnotherPageDelegate {
    
    //MARK: - Views
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var addEmploymentAuthorizedBtn: UIButton!
    @IBOutlet weak var employmentAuthorizedTbl: UITableView!
    @IBOutlet weak var manamentPossibleLbl: UILabel!
    @IBOutlet weak var createDirectDebitLbl: UILabel!
    // constrains
    @IBOutlet weak var tblHeightCon: NSLayoutConstraint!
    
    //MARK: - Actions
    @IBAction func addEmploymentAuthorizedBtnAction(_ sender: Any) {
        openDialog(cellNumber: -1)
    }
    
    //MARK: - Variables
    var authorizedEmploymentArr : Array<AuthorizedEmploy> = []
    var rowHeight : CGFloat = 150
    var spaceBetweenRows : CGFloat = 10
    // delegates
    var changeScrollViewDelegate : ChangeScrollViewHeightDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        Global.sharedInstance.setButtonCircle(sender: addEmploymentAuthorizedBtn, isBorder: true)
        
        let directDebitStr = "הוראת קבע"
        let range: Range<String.Index> = createDirectDebitLbl.text!.range(of: directDebitStr)!
        let index: Int = createDirectDebitLbl.text!.distance(from: createDirectDebitLbl.text!.startIndex, to: range.lowerBound)
        
        let text = NSMutableAttributedString(attributedString: createDirectDebitLbl.attributedText!)
        text.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange(location: index, length: directDebitStr.characters.count))
        createDirectDebitLbl.attributedText = text
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.createDirectDebitLbl.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
        self.getEmployerCodeTables()
        //        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numOfRowsInSection = authorizedEmploymentArr.count
        
        return numOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = employmentAuthorizedTbl.dequeueReusableCell(withIdentifier: "EmploymentAuthorizedTableViewCell") as! EmploymentAuthorizedTableViewCell
        
        cell.cellOptionsDelegate = self
        cell.openAnotherPageDelegate = self
        cell.setDisplayData(authorizedEmploy: authorizedEmploymentArr[indexPath.section], _cellNumber: indexPath.section)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return spaceBetweenRows
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        if tableView == autoCompletePlacesTbl {
    //            placeTxt.text = placesArr[indexPath.section/*row*/].attributedFullText.string
    //            autoCompletePlacesTbl.isHidden = true
    //            getPlaceByID(placeId: placesArr[indexPath.section/*row*/].placeID!)
    //        } else {
    //            rowToUpdate = indexPath.section/*row*/
    //            placeTxt.text = workerLocationsArr[rowToUpdate].nvAddress
    //            //            radiusSlider.setValue((workerLocationsArr[rowToUpdate].nRadius!) / 1000, animated: true)
    //            radiusSlider.selectedMaximum = (workerLocationsArr[rowToUpdate].nRadius!) / 1000
    //
    //            addMarkerAndCircle(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(workerLocationsArr[rowToUpdate].nvLat)!, longitude: CLLocationDegrees(workerLocationsArr[rowToUpdate].nvLng)!), radius: CLLocationDistance((workerLocationsArr[rowToUpdate].nRadius!)))
    //
    //
    //        }
    //    }
    
    
    //MARK: - Server Function
    func getEmployerCodeTables() {
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerId"] = Global.sharedInstance.employer.iEmployerId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : { // 8
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            if let _ = dic["Error"]?["iErrorCode"] { // 7
                
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 { // 5
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> { // 3
                        
                        
                        self.authorizedEmploymentArr.removeAll()
                        let authorizedEmploy = AuthorizedEmploy()
                        
                        //                            var employerCodeTables : Array<SysTableDictionary> = []
                        //                            let sysTable = SysTableDictionary()
                        self.tblHeightCon.constant = 0
                        for authorizedemployDic in dicResult { // 0
                            self.authorizedEmploymentArr.append(authorizedEmploy.getAuthorizedEmployFromDic(dic: authorizedemployDic))//getSysTableDictionaryFromDic(dic: authorizedemployDic))
                            self.tblHeightCon.constant += (self.rowHeight + self.spaceBetweenRows)
                            self.changeScrollViewDelegate.changeScrollViewHeight(heightToadd: self.rowHeight + self.spaceBetweenRows)
                            
                            
                        } // 0
                        
                        //                            if self.authorizedEmploymentArr.count > 3 {
                        
                        //                            }
                        
                        if self.authorizedEmploymentArr.count == 0 {
                            //                                self.tblHeightCon.constant += (self.rowHeight + self.spaceBetweenRows)
                            self.changeScrollViewDelegate.changeScrollViewHeight(heightToadd: self.rowHeight + self.spaceBetweenRows)
                            self.changeScrollViewDelegate.setScrollViewEnable!(isEnable: false)
                        }
                        self.employmentAuthorizedTbl.reloadData()
                        //                            Global.sharedInstance.employerCodeTables = employerCodeTables
                        //                            for item in employerCodeTables { // 1
                        //                                if item.Key == Global.sharedInstance.bankCodeTable {
                        ////                                    self.cell.banksArr = item.Value
                        //                                }
                        //                            }
                        //                            self.cell.tblBanks.reloadData()
                        //                            self.cell.setDisplayData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }  else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetAuthorizedEmploy) // 9
        
    }
    
    
    //    func updateAuthorizedEmploy(authorizedEmploy : AuthorizedEmploy, ) {
    //        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
    //        var dic = Dictionary<String, AnyObject>()
    //        dic["authorizedEmploy"] = authorizedEmploy.getDictionaryFromAuthorizedEmploy() as AnyObject?
    //        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
    //        //            print("---ForgotPassword:\n\(dic)")
    //        api.sharedInstance.goServer(params : dic,success : {
    //            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
    //
    //            print(responseObject as! [String:AnyObject])
    //            //            print(responseObject as Any)
    //            var dic  =  responseObject as! [String:AnyObject]
    //            if let _ = dic["Error"]?["iErrorCode"] {
    //                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
    //
    //                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
    //                    if let dicResult = dic["Result"] as? Bool {
    //                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
    //                        if dicResult {
    //                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
    //                        } else {
    //                            Alert.sharedInstance.showAlert(mess: "ארעה בעיה בשמירת הפרטים!")
    //                        }
    //                        //                        self.openFirstPage(isEmployer: true)
    //                    } else {
    //                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
    //                    }
    //                } else {
    //                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
    //                }
    //
    //                //if dic["Error"]?["iErrorCode"] as! Int ==  2
    //                //{
    //                //    Alert.sharedInstance.showAlert(mess: "מייל לא קיים במערכת")
    //                //}
    //                //else
    //                //{
    //                //    Alert.sharedInstance.showAlert(mess: "נשלח אליך מייל עם הסיסמה")
    //                //}
    //            }
    //        } ,failure : {
    //            (AFHTTPRequestOperation, Error) -> Void in
    //            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
    //            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
    //        }, funcName : api.sharedInstance.UpdateAuthorizedEmploy)
    //    }
    
    func updateAuthorizedEmploy(authorizedEmploy : AuthorizedEmploy, indexInArray : Int) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        //WorkerAskingLocation workerAskingLocation, int iUserId
        dic["authorizedEmploy"] = authorizedEmploy.getDictionaryFromAuthorizedEmploy() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String, AnyObject> {
                        var authorized = AuthorizedEmploy()
                        authorized = authorized.getAuthorizedEmployFromDic(dic: dicResult)
                        
                        if authorizedEmploy.bDeleted { // delete row
                            self.authorizedEmploymentArr.remove(at: indexInArray)
                            Alert.sharedInstance.showAlert(mess: "נמחק בהצלחה!")
                            self.tblHeightCon.constant -= (self.rowHeight + self.spaceBetweenRows)
                            self.changeScrollViewDelegate.changeScrollViewHeight(heightToadd: -self.rowHeight + -self.spaceBetweenRows)
                        } else if authorizedEmploy.user.iUserId > 0 {  // update row
                            self.authorizedEmploymentArr.remove(at: indexInArray)
                            self.authorizedEmploymentArr.insert(authorized, at: indexInArray)
                            Alert.sharedInstance.showAlert(mess: "התעדכן בהצלחה!")
                        } else { // add row
                            self.authorizedEmploymentArr.insert(authorized, at: self.authorizedEmploymentArr.count)
                            Alert.sharedInstance.showAlert(mess: "נוסף בהצלחה!")
                            //                            if self.authorizedEmploymentArr.count > 3 {
                            self.changeScrollViewDelegate.changeScrollViewHeight(heightToadd: self.rowHeight + self.spaceBetweenRows)
                            //                            }
                        }
                        
                        //                        self.resetData()
                        self.employmentAuthorizedTbl.reloadData()
                        //self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateAuthorizedEmploy)
    }
    
    //MARK: - Delegates
    //CellOptionsDelegate
    func removeCell(cellNumber: Int) {
        if cellNumber >= 0 && cellNumber < authorizedEmploymentArr.count {
            authorizedEmploymentArr[cellNumber].bDeleted = true
            updateAuthorizedEmploy(authorizedEmploy: authorizedEmploymentArr[cellNumber], indexInArray: cellNumber)
        }
    }
    
    func editCell(cellNumber: Int) {
        openDialog(cellNumber: cellNumber)
    }
    
    //SaveEmployerDelegate
    func saveAuthorizedEmploy(authorizedEmploy: AuthorizedEmploy, indexInArray: Int) {
        getEmployerCodeTables()
        //        if authorizedEmploy.bDeleted { // delete row
        //            self.authorizedEmploymentArr.remove(at: indexInArray)
        //            Alert.sharedInstance.showAlert(mess: "נמחק בהצלחה!")
        //        } else
     /*   if authorizedEmploy.user.iEmployerId > 0 {  // update row
            self.authorizedEmploymentArr.remove(at: indexInArray)
            self.authorizedEmploymentArr.insert(authorizedEmploy, at: indexInArray)
            Alert.sharedInstance.showAlert(mess: "התעדכן בהצלחה!")
        } else { // add row
            self.authorizedEmploymentArr.insert(authorizedEmploy, at: self.authorizedEmploymentArr.count)
            Alert.sharedInstance.showAlert(mess: "נוסף בהצלחה!")
            self.tblHeightCon.constant += (self.rowHeight + self.spaceBetweenRows)
            //            if self.authorizedEmploymentArr.count > 3 {
            self.changeScrollViewDelegate.changeScrollViewHeight(heightToadd: self.rowHeight + self.spaceBetweenRows)
            //            }
        }
        
        //                        self.resetData()
        
        //        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
        //            self.abc()
        //        }
        //        DispatchQueue.main.async {
        DispatchQueue.main.async {
            self.employmentAuthorizedTbl.reloadData()
        }
        //
        //        }
        */
        
        
        
    }
    
    func abc() -> Void {
        self.employmentAuthorizedTbl.reloadData()
    }
    
    //MARK: - Open New Employ Authorized Pop-UP
    func openDialog(cellNumber : Int) {
        let employerStory =  UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        if let vc = employerStory.instantiateViewController(withIdentifier: "NewEmploymentAuthorizedViewController") as? NewEmploymentAuthorizedViewController
        {
            vc.modalPresentationStyle = UIModalPresentationStyle.custom
            //                vc.changePasswordDelegate = self
            if cellNumber >= 0 && cellNumber < authorizedEmploymentArr.count {
                vc.authorizedEmploy = authorizedEmploymentArr[cellNumber]
                vc.index = cellNumber
            }
            vc.saveEmployerDelegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: - OpenAnotherPageDelegate
    func openViewController(VC: UIViewController) {
        if VC is BasePopUpViewController {
            self.present(VC, animated: true, completion: nil)
        }
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        print("handleTap")
        //        if self.openAnotherPageDelegate != nil {
        self.openAnotherPageDelegate?.openNextTab?(tag: 2)
        //       }
        // let story =  UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        // let viewCon = story.instantiateViewController(withIdentifier: "ChooseBankViewController") as! ChooseBankViewController
        //self.navigationController?.pushViewController(viewCon, animated: true)
    }
}
