//
//  ChooseBankViewController.swift
//  
//
//  Created by User on 4.4.2017.
//
//

import UIKit

class ChooseBankViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    var banksArr : Array<SysTable> = []

    //MARK: - Views
    @IBOutlet weak var lblArrow: UILabel!
    @IBOutlet weak var lblBank: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var tblBanks: UITableView!
    
    //MARK: - Actions
    @IBAction func continueBtnClicked(_ sender: Any) {
        if bankSelectedId != -1 {
            getGuidFromApi(iBankId : bankSelectedId)
        } else {
            Alert.sharedInstance.showAlert(mess: "לא נבחר בנק!")
        }
    }
    
    //MARK: - Variables
    var bankSelectedId : Int = -1
    var barContainer = BarContainerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getWorkerCodeTables()
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChooseBankViewController.setTblState))
        lblBank.addGestureRecognizer(tap)
        tblBanks.isHidden = true
        lblArrow.text = "\u{f107}"
        Global.sharedInstance.setButtonCircle(sender: continueBtn, isBorder: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        barContainer.setTitle(title: "הוראת קבע")
    }
    
    func setTblState()
    {
        tblBanks.isHidden = !tblBanks.isHidden
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return banksArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell") as! FamilyStatusTableViewCell
        cell.setDisplayData(txtStatus: banksArr[indexPath.row].nvName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lblBank.text = banksArr[indexPath.row].nvName
        bankSelectedId = banksArr[indexPath.row].iId
        setTblState()
        //tblBanks.isHidden = true
    }
    
    //MARK: GetWorkerCodeTables
    func getWorkerCodeTables() {
        if Global.sharedInstance.workerCodeTables.count > 0{
            for item in Global.sharedInstance.workerCodeTables { // 1
                if item.Key == Global.sharedInstance.bankCodeTable {
                    self.banksArr = item.Value
                    self.tblBanks.reloadData()
                    break                }
            } // 1
        }else{
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            
            dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
            
            api.sharedInstance.goServer(params : dic,success : { // 8
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] { // 7
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 { // 5
                        if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> { // 3
                            
                            var workerCodeTables : Array<SysTableDictionary> = []
                            let sysTable = SysTableDictionary()
                            
                            for workerCodeTableDic in dicResult { // 0
                                workerCodeTables.append(sysTable.getSysTableDictionaryFromDic(dic: workerCodeTableDic))
                            } // 0
                            
                            Global.sharedInstance.workerCodeTables = workerCodeTables
                            for item in workerCodeTables { // 1
                                if item.Key == Global.sharedInstance.bankCodeTable {
                                
                                    self.banksArr = item.Value
                                    self.tblBanks.reloadData()
                                    break
                                }
                            }
                            
                        } else { // 3 , 4
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        } // 4
                    } else { // 5 , 6
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    } // 6
                } // 7
            } ,failure : { // 8 , 9
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : "GetWorkerCodeTables") // 9
        }
    }
    
    func getGuidFromApi(iBankId:Int){
        var dic = Dictionary<String, AnyObject>()
        
        dic["isFromApp"] = true as AnyObject?
        dic["iBankId"] = iBankId as AnyObject?
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        
        api.sharedInstance.goServer(params : dic,success : { // 8
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] { // 7
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 { // 5
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>>  { // 3
                        var bankAccountGuide = BankAccountGuide()
                        
                        bankAccountGuide = bankAccountGuide.getBankAccountGuideFromDic(dic: dicResult[0] )
                        GlobalEmpolyerDetails.sharedInstance.bankAccountGuideImages = bankAccountGuide
                        let story =  UIStoryboard(name: "StoryboardEmployer", bundle: nil)
                        let viewCon = story.instantiateViewController(withIdentifier: "GuidViewController") as! GuidViewController
                        //let navigationController = story.instantiateViewController(withIdentifier: "navEmployer")as!UINavigationController
                        self.navigationController?.pushViewController(viewCon, animated: true)
                        
                        Global.sharedInstance.openUrl(url: bankAccountGuide.nvLink)
                        
                        //for bankAccount in dicResult { // 0
                        //                          workerCodeTables.append(sysTable.getSysTableDictionaryFromDic(dic: workerCodeTableDic))
                        //}
                    }
                    //self.visibleProfileTbl.reloadData()
                    
                } else { // 3 , 4
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                } // 4
            } else { // 5 , 6
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
            } // 6
        } // 7
            ,failure : { // 8 , 9
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetBankAccountGuide") // 9
    }
    
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        //        barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "הוראת קבע"
        
    }
    
}
