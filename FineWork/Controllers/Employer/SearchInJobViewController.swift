//
//  SearchInJobViewController.swift
//  FineWork
//
//  Created by Racheli Kroiz on 27/07/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

//בבחירת חיפוש עובד חדש או מוכר - פותח את המשרה לשיכפול
protocol SearchNewOrKnowWorkerDelegate {
    func searchNewOrKnow(isNewWorker:Bool,idJob:Int)
    func searchNewOrKnow(isNewWorker:Bool,idJob:Int,number:String)
    func changeHeight(position : Int , isUpdate :Bool)
}

class SearchInJobViewController: UIViewController ,SearchNewOrKnowWorkerDelegate{
    
    //MARK:- Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Vareble
    var bSave : Bool = false
    var listJob : Array<SavedOrExistsEmployerJob> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetSavedOrExistsEmployerJobs()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendData(bSave : Bool ){
        self.bSave = bSave
    }
    
    //MARK: - Server Function
    func GetSavedOrExistsEmployerJobs() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iEmployerId"] = Global.sharedInstance.employer.iEmployerId as AnyObject?
        dic["bSaved"] = self.bSave as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> {
                        
                        
                        //  listJob : Array<SavedOrExistsEmployerJob> = []
                        let save = SavedOrExistsEmployerJob()
                        
                        for list in dicResult { // 0
                            self.listJob.append(save.getSavedOrExistsEmployerJobFromDic(dic: list))
                        }
                        self.initTableView()
                        
                        
                        
                    } } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetSavedOrExistsEmployerJobs")
    }
    
    func initTableView (){
//        tableView.delegate = self
//        tableView.dataSource = self
        tableView.reloadData()
    }
    


    
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        //        barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "חיפוש במאגר משרות"
        
    }
    //MARK: - SearchNewOrKnowWorkerDelegate
    func searchNewOrKnow(isNewWorker:Bool,idJob:Int){
        var viewCon : UIViewController?
        let storyEmployer = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        
        viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchWorkerModelViewController") as? SearchWorkerModelViewController
        (viewCon as? SearchWorkerModelViewController)?.sendData(isNewWorker: isNewWorker,idJob: idJob, isCopyJob: true)
        self.navigationController?.pushViewController(viewCon!, animated: true)
        
        
    }
    func searchNewOrKnow(isNewWorker:Bool,idJob:Int,number:String){
        var viewCon : UIViewController?
        let storyEmployer = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        
        viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchWorkerModelViewController") as? SearchWorkerModelViewController
        (viewCon as? SearchWorkerModelViewController)?.sendData(isNewWorker: isNewWorker,idJob: idJob, isCopyJob: true, number:number)
        self.navigationController?.pushViewController(viewCon!, animated: true)
        

    }
    func changeHeight(position : Int ,  isUpdate : Bool){
        if  listJob.count > 0 &&  listJob.count > position {
            if isUpdate {
                listJob[position].isShowDetails = !listJob[position].isShowDetails
            }
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }
}

extension SearchInJobViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listJob.count
    }
    
 
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    print("mmm cellForRowAt")
        let cell : UITableViewCell
         cell = tableView.dequeueReusableCell(withIdentifier: "SearchInViewTableViewCell", for: indexPath as IndexPath) as! SearchInViewTableViewCell
        
      
        (cell as! SearchInViewTableViewCell).setDisplayData(savedOrExistsEmployerJob: listJob[indexPath.row] , isSave: bSave , position : indexPath.row)         //   (cell as! ActiveJobsToEmployeeTableViewCell).openJobDatailsDel = self
        (cell as! SearchInViewTableViewCell).searchWorkerDel = self
        let bgColorView = UIView()
    //    (cell as! SearchInViewTableViewCell).searchWorkerNewBtn.setTitleColor(UIColor.green, for: .normal)

        bgColorView.backgroundColor = UIColor.clear
       cell.selectedBackgroundView = bgColorView
             return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SearchInViewTableViewCell!

        //let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "SearchInViewTableViewCell") as! SearchInViewTableViewCell
       

                if  listJob.count > 0 &&  listJob.count > indexPath.row {
            print ("indexPath \(indexPath) \(listJob[indexPath.row].isShowDetails)")
            listJob[indexPath.row].isShowDetails = !listJob[indexPath.row].isShowDetails
            print ("indexPath \(indexPath) \(listJob[indexPath.row].isShowDetails)")
            cell?.setColor()
            tableView.beginUpdates()
            tableView.endUpdates()
 
        }
        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        if !listJob[indexPath.row].isShowDetails {
            return 65
        } else{
            if bSave {
                return 600
                
            }else{
                return 500
                
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    

}



