//
//  SearchNewWorker_JobDescriptionViewController.swift
//  FineWork
//
//  Created by User on 21/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import TTRangeSlider

//class NumberFormatter: NumberFormatter {
//
//    override func string(from number: NSNumber) -> String? {
//
//        //        return String(describing: (number.intValue / 10) * 10)
//        return String(describing: number.intValue)
//    }
//}

class SearchNewWorker_JobDescriptionViewController: UIViewController, SSRadioButtonControllerDelegate, UITextFieldDelegate {
    
    //MARK: - Views
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var jobDescriptionTxt: UITextField!
    @IBOutlet var risksLbl: UILabel!
    @IBOutlet var noRisksRdb: SSRadioButton!
    @IBOutlet var noRisksLbl: UILabel!
    @IBOutlet var yesRisksRdb: SSRadioButton!
    @IBOutlet var yesRisksLbl: UILabel!
    @IBOutlet var risksDetailsView: UIView!
    @IBOutlet var riskDetailsTxt: UITextField!
    @IBOutlet var safetyRequirementsViewHeightFromTop: NSLayoutConstraint!
    @IBOutlet var safetyRequirementsView: UIView!
    @IBOutlet var safetyRequirementLbl: UILabel!
    @IBOutlet var noSafetyReqsRdb: SSRadioButton!
    @IBOutlet var noSafetyReqsLbl: UILabel!
    @IBOutlet var yesSafetyReqsRdb: SSRadioButton!
    @IBOutlet var yesSafetyReqsLbl: UILabel!
    @IBOutlet var safetyReqsDetailsView: UIView!
    @IBOutlet var safetyReqsDetailsTxt: UITextField!
    @IBOutlet var physicalDifficultyLblHeightFromTop: NSLayoutConstraint!
    @IBOutlet var physicalDifficultyLbl: UILabel!
    @IBOutlet var physicalDifficultySlider: TTRangeSlider!
    @IBOutlet var workSpaceLbl: UILabel!
    @IBOutlet var workSpaceTxt: UITextField!
    @IBOutlet var baseConditionsLbl: UILabel!
    @IBOutlet var experienceBtn: CheckBox!
    @IBOutlet var experienceLbl: UILabel!
    @IBOutlet var experienceView: UIView!
    @IBOutlet var experienceTxt: UITextField!
    @IBOutlet var seniorityBtn: CheckBox!
    @IBOutlet var seniorityLbl: UILabel!
    @IBOutlet var seniorityView: UIView!
    @IBOutlet var seniorityTxt: UITextField!
    @IBOutlet var languageBtn: CheckBox!
    @IBOutlet var languageLbl: UILabel!
    @IBOutlet var languageView: UIView!
    @IBOutlet var languageTxt: UITextField!
    @IBOutlet var professionalSeniorityBtn: CheckBox!
    @IBOutlet var professionalSeniorityLbl: UILabel!
    @IBOutlet var professionalSeniorityView: UIView!
    @IBOutlet var professionalSeniorityTxt: UITextField!
    @IBOutlet var diplomaBtn: CheckBox!
    @IBOutlet var diplomaLbl: UILabel!
    @IBOutlet var phoneInterviewBtn: CheckBox!
    @IBOutlet var phoneInterviewLbl: UILabel!
    @IBOutlet var languageHeightFromTop: NSLayoutConstraint!
    @IBOutlet var professionalSeniorityHeightFromTop: NSLayoutConstraint!
    @IBOutlet var dimplomaHeightFromTop: NSLayoutConstraint!
    @IBOutlet var phoneInterviewHeightFromTop: NSLayoutConstraint!
    @IBOutlet var startingSearchNewWorkerBtn: UIButton!
    @IBOutlet var startingWorkerUnderDiplomaBtn: UIButton!
    @IBOutlet var startingSearchHeightFromTop: NSLayoutConstraint!
    @IBOutlet weak var exsperiensTitle: UILabel!
    @IBOutlet weak var exspriensView1: UIView!
    @IBOutlet weak var exsperienseView2: UIView!
    @IBOutlet weak var exsperienseView3: UIView!
    @IBOutlet weak var exsperienseView4: UIView!
    @IBOutlet weak var exsperienseView5: UIView!
    
    
    //MARK: - Actions
    
    @IBAction func checkBoxAction(_ sender: CheckBox) {
        sender.isChecked = !sender.isChecked
        var heightToAdd : CGFloat = sender.isChecked ? (constrainChecked - constrainUnChecked) : (constrainUnChecked - constrainChecked)
        switch sender {
        case experienceBtn:
            experienceView.isHidden = !sender.isChecked
            languageHeightFromTop.constant = sender.isChecked ? constrainChecked : constrainUnChecked
            break
        case seniorityBtn:
            seniorityView.isHidden = !sender.isChecked
            professionalSeniorityHeightFromTop.constant = sender.isChecked ? constrainChecked : constrainUnChecked
            break
        case languageBtn:
            languageView.isHidden = !sender.isChecked
            dimplomaHeightFromTop.constant = sender.isChecked ? constrainChecked : constrainUnChecked
            break
        case professionalSeniorityBtn:
            professionalSeniorityView.isHidden = !sender.isChecked
            phoneInterviewHeightFromTop.constant = sender.isChecked ? constrainChecked : constrainUnChecked
            break
        case diplomaBtn:
            heightToAdd = 0
            break
        case phoneInterviewBtn:
            heightToAdd = 0
            break
        default:
            break
        }
        
        //--
        if sender != diplomaBtn && sender != phoneInterviewBtn {
            if sender == experienceBtn || sender == languageBtn || sender == diplomaBtn {
                if sender.isChecked {
                    numberOfCheckedDiploma += 1
                } else {
                    numberOfCheckedDiploma -= 1
                }
            } else {
                if sender.isChecked {
                    numberOfCheckedInterview += 1
                } else {
                    numberOfCheckedInterview -= 1
                }
            }
        }
        
        if numberOfCheckedDiploma > numberOfCheckedInterview {
            startingSearchNewWorkerBtn.isHidden = true
            startingWorkerUnderDiplomaBtn.isHidden = false
        } else {
            startingWorkerUnderDiplomaBtn.isHidden = true
            startingSearchNewWorkerBtn.isHidden = false
        }
        
        changeScrollViewDelegate.changeScrollViewHeight(heightToadd: heightToAdd)
    }
    
    @IBAction func startingSearchNewWorkerBtnAction(_ sender: Any) {
        if validateFields() {
            openAnotherPageDelegate.openNextTab!(tag: 5)
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים / אינם תקינים")
        }
    }
    //MARK: - Variables
    var constrainChecked : CGFloat = 47
    var constrainUnChecked : CGFloat = 15
    var risksRadioButtonController: SSRadioButtonsController?
    var safetyReqsRadioButtonController: SSRadioButtonsController?
    var changeScrollViewDelegate : ChangeScrollViewHeightDelegate! = nil
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    var numberOfCheckedDiploma = 0, numberOfCheckedInterview = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        physicalDifficultySlider.tintColorBetweenHandles = UIColor.blue2
        //secondRangeSlider.tintColor = UIColor.white
        physicalDifficultySlider.handleColor = UIColor.blue2
        
        physicalDifficultySlider.handleDiameter = 20
        physicalDifficultySlider.selectedHandleDiameterMultiplier = 1.0
        
        physicalDifficultySlider.minValue = Float(0)
        physicalDifficultySlider.maxValue = Float(10)
        
        //        radiusSlider.selectedMinimum = Float(10)
        physicalDifficultySlider.selectedMaximum = Float(0)
        
        let numFormat = ElapsedMetersFormatter()
        physicalDifficultySlider.numberFormatterOverride = numFormat
        
        physicalDifficultySlider.disableRange = true
        
        //--
        experienceView.isHidden = true
        languageHeightFromTop.constant = 15
        
        seniorityView.isHidden = true
        professionalSeniorityHeightFromTop.constant = 15
        
        languageView.isHidden = true
        dimplomaHeightFromTop.constant = 15
        
        professionalSeniorityView.isHidden = true
        phoneInterviewHeightFromTop.constant = 15
        
        risksDetailsView.isHidden = true
        safetyRequirementsViewHeightFromTop.constant = 15
        
        safetyReqsDetailsView.isHidden = true
        physicalDifficultyLblHeightFromTop.constant = 15
        
        
        risksRadioButtonController = SSRadioButtonsController(buttons: noRisksRdb, yesRisksRdb)
        risksRadioButtonController?.delegate = self
        risksRadioButtonController?.shouldLetDeSelect = true
        
        safetyReqsRadioButtonController = SSRadioButtonsController(buttons: noSafetyReqsRdb, yesSafetyReqsRdb)
        safetyReqsRadioButtonController?.delegate = self
        safetyReqsRadioButtonController?.shouldLetDeSelect = true
        
        Global.sharedInstance.setButtonCircle(sender: startingSearchNewWorkerBtn, isBorder: false)
        Global.sharedInstance.setButtonCircle(sender: startingWorkerUnderDiplomaBtn, isBorder: false)
        
        noRisksRdb.isSelected = true
        noSafetyReqsRdb.isSelected = true
        
        
        jobDescriptionTxt.delegate = self
        workSpaceTxt.delegate = self
        experienceTxt.delegate = self
        seniorityTxt.delegate = self
        languageTxt.delegate = self
        professionalSeniorityTxt.delegate = self
        
        jobDescriptionTxt.keyboardType = .default
        workSpaceTxt.keyboardType = .default
        experienceTxt.keyboardType = .default
        seniorityTxt.keyboardType = .default
        languageTxt.keyboardType = .default
        professionalSeniorityTxt.keyboardType = .default
        
        jobDescriptionTxt.returnKeyType = .done
        workSpaceTxt.returnKeyType = .done
        experienceTxt.returnKeyType = .done
        seniorityTxt.returnKeyType = .done
        languageTxt.returnKeyType = .done
        professionalSeniorityTxt.returnKeyType = .done
        
        experienceTxt.tag = TextFieldType.OnView.rawValue
        seniorityTxt.tag = TextFieldType.OnView.rawValue
        languageTxt.tag = TextFieldType.OnView.rawValue
        professionalSeniorityTxt.tag = TextFieldType.OnView.rawValue
    }
    override func viewDidAppear(_ animated: Bool) {
        if !((parent as? SearchWorkerModelViewController)?.isNewWorker)!{
            exsperiensTitle.isHidden = true
            experienceView.isHidden = true
            exspriensView1.isHidden = true
            seniorityView.isHidden = true
            exsperienseView2.isHidden = true
            exsperienseView3.isHidden = true
            languageView.isHidden = true
            professionalSeniorityView.isHidden = true
            exsperienseView4.isHidden = true
            exsperienseView5.isHidden = true
        }
        setDisplayData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Set data to edit
    func setDisplayData(){
        let  employerJob = ((parent as? SearchWorkerModelViewController)?.employerJob)!
        jobDescriptionTxt.text = employerJob.nvJobDescription
        if  employerJob.bRisk {
            riskDetailsTxt.text = employerJob.nvRiskDetails
            noRisksRdb.isSelected = false
          yesRisksRdb.isSelected = true
            didSelectButton(yesRisksRdb)
        }
        if employerJob.bSafetyRequirement {
            noSafetyReqsRdb.isSelected = false
            yesSafetyReqsRdb.isSelected = true
            didSelectButton(yesSafetyReqsRdb)
            safetyReqsDetailsTxt.text = employerJob.nvSafetyRequirementDetails
            
        }
        if employerJob.iPhysicalDifficultyRank != nil &&  employerJob.iPhysicalDifficultyRank! > 0 {
        physicalDifficultySlider.selectedMaximum = Float(Int(employerJob.iPhysicalDifficultyRank!))
        }
            workSpaceTxt.text = employerJob.nvWorkSpaceDetails
        if employerJob.employerJobNewWorker.nvExperienceDetails != "" {
            // experienceBtn.isChecked = true
            checkBoxAction(experienceBtn)
            experienceTxt.text = employerJob.employerJobNewWorker.nvExperienceDetails
        }
        
        if employerJob.employerJobNewWorker.nvLanguageDetails != "" {
            checkBoxAction(languageBtn)
            languageTxt.text = employerJob.employerJobNewWorker.nvLanguageDetails
        }
        if employerJob.employerJobNewWorker.nvTimeDescription != "" {
            checkBoxAction(seniorityBtn)
            seniorityTxt.text = employerJob.employerJobNewWorker.nvTimeDescription
        }
        if employerJob.employerJobNewWorker.nvProfessionalTimeDetails != "" {
            checkBoxAction(professionalSeniorityBtn)
            professionalSeniorityTxt.text = employerJob.employerJobNewWorker.nvProfessionalTimeDetails
        }
        diplomaBtn.isChecked = employerJob.employerJobNewWorker.bDiploma
        phoneInterviewBtn.isChecked = employerJob.employerJobNewWorker.bPhoneInterview
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - SSRadioButtonControllerDelegate
    func didSelectButton(_ aButton: UIButton?) {
        var heightToAdd : CGFloat = 0
        if aButton != nil {
            switch aButton! {
            case noRisksRdb:
                risksDetailsView.isHidden = true
                safetyRequirementsViewHeightFromTop.constant = constrainUnChecked
                heightToAdd = constrainUnChecked - constrainChecked
                break
            case yesRisksRdb:
                risksDetailsView.isHidden = false
                safetyRequirementsViewHeightFromTop.constant = constrainChecked
                heightToAdd = constrainChecked - constrainUnChecked
                break
            case noSafetyReqsRdb:
                safetyReqsDetailsView.isHidden = true
                physicalDifficultyLblHeightFromTop.constant = constrainUnChecked
                heightToAdd = constrainUnChecked - constrainChecked
                break
            case yesSafetyReqsRdb:
                safetyReqsDetailsView.isHidden = false
                physicalDifficultyLblHeightFromTop.constant = constrainChecked
                heightToAdd = constrainChecked - constrainUnChecked
                break
            default:
                break
            }
        }
        changeScrollViewDelegate.changeScrollViewHeight(heightToadd: heightToAdd)
    }
    
    //MARK: - Text Field Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        getTextFieldDelegate.getTextField(textField: textField, originY: 0)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.textAlignment = NSTextAlignment.justified
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        limitLength = 100
        //        switch textField {
        //        case jobDescriptionTxt:
        //            limitLength = 100
        //            break
        //        case descriptionTxt:
        //            limitLength = 200
        //            break
        //        case mainAreaToActivityTxt:
        //            limitLength = 100
        //            break
        //        default:
        //            break
        //        }
        
        return newLength <= limitLength // Bool
        
    }
    
    //MARK: - Validate Fields
    func validateFields() -> Bool {
        var isValid = true
        
        if jobDescriptionTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: jobDescriptionTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: jobDescriptionTxt)
        }
        
        if yesRisksRdb.isSelected && riskDetailsTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: riskDetailsTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: riskDetailsTxt)
        }
        
        if yesSafetyReqsRdb.isSelected && safetyReqsDetailsTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: safetyReqsDetailsTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: safetyReqsDetailsTxt)
        }
        
        if workSpaceTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: workSpaceTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: workSpaceTxt)
        }
        
        if experienceBtn.isChecked && experienceTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: experienceTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: experienceTxt)
        }
        
        if languageBtn.isChecked && languageTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: languageTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: languageTxt)
        }
        
        if seniorityBtn.isChecked && seniorityTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: seniorityTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: seniorityTxt)
        }
        
        if professionalSeniorityBtn.isChecked && professionalSeniorityTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: professionalSeniorityTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: professionalSeniorityTxt)
        }
        
        return isValid
        
    }
    
}
