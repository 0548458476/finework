//
//  SearchNewWorker_WhereViewController.swift
//  FineWork
//
//  Created by User on 21/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import GooglePlaces

class SearchNewWorker_WhereViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SSRadioButtonControllerDelegate, UITextFieldDelegate {

    //MARK: - Views
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var myAddressRdb: SSRadioButton!
    @IBOutlet var myAddressLbl: UILabel!
    @IBOutlet var anotherAddressRdb: SSRadioButton!
    
    @IBOutlet var anotherAddressLbl: UILabel!
    @IBOutlet var anotherAddressView: UIView!
    @IBOutlet var anotherAddressTxt: UITextField!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var placesTbl: UITableView!
    
    //MARK: - Actions
    
    @IBAction func nextBtnAction(_ sender: Any) {
        openAnotherPageDelegate.openNextTab!(tag: 4)
    }
    
    //MARK: - Variables
    // google vars
    var placesClient : GMSPlacesClient! = nil
    var placesArr = Array<GMSAutocompletePrediction>()
    var placeSelected : CLLocationCoordinate2D? = nil
    
    var radioButtonController: SSRadioButtonsController?
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // google places
        placesClient = GMSPlacesClient.shared()
        anotherAddressTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        Global.sharedInstance.setButtonCircle(sender: nextBtn, isBorder: true)
        
        radioButtonController = SSRadioButtonsController(buttons: myAddressRdb, anotherAddressRdb)
        radioButtonController?.delegate = self
        radioButtonController?.shouldLetDeSelect = true
        
        myAddressRdb.isSelected = true
        anotherAddressView.isHidden = true
        
        anotherAddressTxt.delegate = self
        anotherAddressTxt.keyboardType = .default
        anotherAddressTxt.returnKeyType = .done
        
        if  Global.sharedInstance.employer.nvAddress != ""{
            myAddressLbl.text = "אצלי (\(Global.sharedInstance.employer.nvAddress))"
        }else{
             myAddressLbl.text = "אצלי"
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        setDisplayData()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Set data to edit
    func setDisplayData(){
      let  employerJob = ((parent as? SearchWorkerModelViewController)?.employerJob)!
        if employerJob.iWorkingLocationType == WorkingLocationType.myAddress {
            anotherAddressRdb.isSelected = false
            myAddressRdb.isSelected = true
            didSelectButton(myAddressRdb)
        }else {
            myAddressRdb.isSelected = false
            anotherAddressRdb.isSelected = true
            didSelectButton(anotherAddressRdb)
            anotherAddressTxt.text =  employerJob.nvGoogleAddress
        }
        
        
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - Text Field Functions
    func textFieldDidChange(_ textField: UITextField) {
        if textField.text != "" {
            placeAutocomplete(stringToSearch: textField.text!)
        } else {
            placesTbl.isHidden = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfRowsInSection = 0
//        if tableView == autoCompletePlacesTbl {
            numOfRowsInSection = placesArr.count
//        } else {
//            numOfRowsInSection = workerLocationsArr.count
//        }
        
        return numOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell
//        if tableView == autoCompletePlacesTbl {
            //numOfRowsInSection = placesArr.count
            cell = placesTbl.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell") as! FamilyStatusTableViewCell
            (cell as! FamilyStatusTableViewCell).lblStatus.text = placesArr[indexPath.section/*row*/].attributedFullText.string
//        } else {
//            // : LocationTableViewCell = LocationTableViewCell()
//            cell = locationsTbl.dequeueReusableCell(withIdentifier: "LocationTableViewCell") as! LocationTableViewCell
//            (cell as! LocationTableViewCell).setDisplayData(workerLocation: workerLocationsArr[indexPath.section/*row*/])
//            (cell as! LocationTableViewCell).remomeCellDelegate = self
//            (cell as! LocationTableViewCell).openAnotherPageDelegate = self
//            (cell as! LocationTableViewCell).cellNumber = indexPath.section/*row*/
//        }
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     var rowHeight = 0.0
     
     rowHeight = 40.0
     
     return CGFloat(rowHeight)
     }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10/*0.001*/
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        Global.sharedInstance.bWereThereAnyChanges = true
        
//        if tableView == autoCompletePlacesTbl {
            anotherAddressTxt.text = placesArr[indexPath.section/*row*/].attributedFullText.string
            placesTbl.isHidden = true
            getPlaceByID(placeId: placesArr[indexPath.section/*row*/].placeID!)
            
            
//        } else {
//            rowToUpdate = indexPath.section/*row*/
//            placeTxt.text = workerLocationsArr[rowToUpdate].nvAddress
//            //            radiusSlider.setValue((workerLocationsArr[rowToUpdate].nRadius!) / 1000, animated: true)
//            radiusSlider.selectedMaximum = (workerLocationsArr[rowToUpdate].nRadius!) / 1000
//            
//            addMarkerAndCircle(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(workerLocationsArr[rowToUpdate].nvLat)!, longitude: CLLocationDegrees(workerLocationsArr[rowToUpdate].nvLng)!), radius: CLLocationDistance((workerLocationsArr[rowToUpdate].nRadius!)), zoom: 8)
//            
//            
//        }
    }

    //MARK: - Google Places
    func placeAutocomplete(stringToSearch : String) {
        
        //        let filter = GMSAutocompleteFilter()
        //        filter.type = GMSPlacesAutocompleteTypeFilter.geocode|GMSPlacesAutocompleteTypeFilter.city
        
        
        
        placesClient.autocompleteQuery(/*"Sydney Oper"*/stringToSearch, bounds: nil, filter: nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.placesArr = results
                self.placesTbl.isHidden = false
                self.placesTbl.reloadData()
                for result in results {
                    print("Result \(result.attributedFullText) with placeID \(String(describing: result.placeID))")
                }
            }
        })
    }
    
    func getPlaceByID(placeId : String) {
        //let placeID = "ChIJV4k8_9UodTERU5KXbkYpSYs"
        
        placesClient.lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeId)")
                return
            }
            
            //print("Place name \(place.name)")
            //print("Place address \(place.formattedAddress)")
            //print("Place placeID \(place.placeID)")
            //print("Place attributions \(place.attributions)")
            self.placeSelected = place.coordinate
//            self.addMarkerAndCircle(position: place.coordinate, radius: 10 * 1000, zoom: 8)
            
        })
        
    }
    
    //MARK: - SSRadioButtonControllerDelegate
    func didSelectButton(_ aButton: UIButton?) {
        if aButton != nil {
            switch aButton! {
            case anotherAddressRdb:
                anotherAddressView.isHidden = false
                break
            case myAddressRdb:
                anotherAddressView.isHidden = true
                break
            default:
                break
            }
        }
    }


}
