//
//  SearchNewWorker_WhatViewController.swift
//  FineWork
//
//  Created by User on 15/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SearchNewWorker_WhatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {
    
    //MARK: - Views
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var chooseDomainTitleLbl: UILabel!
    @IBOutlet var chooseRoleTitleLbl: UILabel!
    @IBOutlet var workersNumberTitleLbl: UILabel!
    @IBOutlet var jobIsMatchToLbl: UILabel!
    @IBOutlet var chooseDomainLbl: UILabel!
    @IBOutlet var chooseRoleLbl: UILabel!
    
    @IBOutlet weak var viewMache: UIView!
    @IBOutlet weak var viewRoleDomain: UIView!
    @IBOutlet var workersNumberTxt: UITextField!
    @IBOutlet var arrowLbl: UILabel!
    @IBOutlet var arrowRoleLbl: UILabel!
    @IBOutlet var matchToGeneralBtn: UIButton!
    @IBOutlet var matchToYouthBtn: UIButton!
    @IBOutlet var matchToRetiredBtn: UIButton!
    @IBOutlet var matchToForeignWorkersBtn: UIButton!
    //    @IBOutlet var constantWorkerBtn: UIButton!
    //    @IBOutlet var temporaryWorkerBtn: UIButton!
    @IBOutlet var limitedPeriodForConstantWorkerLbl: UILabel!
    @IBOutlet var workersNumberFoundLbl: UILabel!
    @IBOutlet var oldWorkerCheckBox: CheckBox!
    @IBOutlet var oldWorkerLbl: UILabel!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var stateWorkerSeg: UISegmentedControl!
    
    @IBOutlet var domainsTbl: UITableView!
    @IBOutlet var rolesTbl: UITableView!
    
    @IBOutlet weak var whoView: UIView!
    
    @IBOutlet weak var whoPhone: UITextField!
    
    @IBOutlet weak var statusWorkerConstraint: NSLayoutConstraint!
    
    //MARK: - Actions
    @IBAction func matchToBtnAction(_ sender: UIButton) {
        if sender.backgroundColor == UIColor.blue2 {
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor.blue2, for: .normal)
        } else {
            sender.backgroundColor = UIColor.blue2
            sender.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    @IBAction func availabilityBtnAction(_ sender: UIButton) {
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        if validateFields() {
            openAnotherPageDelegate.openNextTab!(tag: 1)
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהפרטים חסרים / אינם תקינים")
        }
    }
    
    @IBAction func oldWorkerCheckBoxAction(_ sender: Any) {
        oldWorkerCheckBox.isChecked = !oldWorkerCheckBox.isChecked
    }
    
    @IBAction func stateWorkerSegAction(_ sender: UISegmentedControl) {
        //        if send
        limitedPeriodForConstantWorkerLbl.isHidden = (stateWorkerSeg.selectedSegmentIndex == 1)
        //        if stateWorkerSeg.selectedSegmentIndex
    }
    
    //MARK: - Variables
    //delegattes
    var changeScrollViewDelegate : ChangeScrollViewHeightDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    var workerRoleSysTables = WorkerRoleSysTables()
    var rolesFilterArray : Array<EmploymentDomainRole> = Array<EmploymentDomainRole>()
    var domainSelectedId : Int = 0
    var domainRoleSelectedId = 0
    var employerJob = EmployerJob()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        Global.sharedInstance.setButtonCircle(sender: matchToGeneralBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: matchToYouthBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: matchToRetiredBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: matchToForeignWorkersBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: nextBtn, isBorder: true)
        
        stateWorkerSeg.layer.cornerRadius = stateWorkerSeg.frame.height / 2
        stateWorkerSeg.layer.borderColor = UIColor.orange2.cgColor//ControlsDesign.sharedInstance.colorFucsia.cgColor
        stateWorkerSeg.layer.borderWidth = 1
        stateWorkerSeg.clipsToBounds = true
        
        workersNumberTxt.keyboardType = .numberPad
        
        
        matchToBtnAction(matchToGeneralBtn)
        
        arrowLbl.text = FontAwesome.sharedInstance.arrow
        arrowRoleLbl.text = FontAwesome.sharedInstance.arrow
        
        workersNumberFoundLbl.isHidden = true
        
        chooseDomainLbl.isUserInteractionEnabled = true
        chooseRoleLbl.isUserInteractionEnabled = true
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(setDominsTblState))
        chooseDomainLbl.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(setRolesTblState))
        chooseRoleLbl.addGestureRecognizer(tapGestureRecognizer2)
        
        getWorkerRoleSysTables()
        
        workersNumberTxt.delegate = self
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if changeScrollViewDelegate != nil {
            changeScrollViewDelegate.setScrollViewEnable!(isEnable: false)
        }
        
        
        if !((parent as? SearchWorkerModelViewController)?.isNewWorker)!{
            whoView.isHidden = false
            titleLbl.text = "מי?"
            viewRoleDomain.isHidden = true
            jobIsMatchToLbl.isHidden = true
            viewMache.isHidden = true
            workersNumberFoundLbl.isHidden = true
            oldWorkerCheckBox.isHidden = true
            oldWorkerLbl.isHidden = true
            statusWorkerConstraint.constant = 100
        }
    }
    //MARK: - Set data to edit
    func setDisplayData(){
        employerJob = ((parent as? SearchWorkerModelViewController)?.employerJob)!
        
        
        //עובד מוכר
        if !((parent as? SearchWorkerModelViewController)?.isNewWorker)! {
            if ((parent as? SearchWorkerModelViewController)?.isCopyJob)!{
                whoPhone.text = (parent as? SearchWorkerModelViewController)?.numberPhone
            }else{
                whoPhone.text = employerJob.nvKnownWorkerPhone
            }
        }
        if employerJob.iJobQuantity > 0 {
            workersNumberTxt.text = String(employerJob.iJobQuantity)
        }
        if employerJob.lPopulationType.count > 0 {
            
            //לבטל את הבחירה הדיפולטיבית
            matchToBtnAction(matchToGeneralBtn)

            for population in employerJob.lPopulationType {
                if population.simpleProp == PopulationType.General.rawValue {
                    matchToBtnAction(matchToGeneralBtn)
                }
                else if population.simpleProp == PopulationType.Youth.rawValue {
                    matchToBtnAction(matchToYouthBtn)
                }
                else  if population.simpleProp == PopulationType.Retired.rawValue {
                    matchToBtnAction(matchToRetiredBtn)
                }
                else   if population.simpleProp == PopulationType.ForeignWorkers.rawValue {
                    matchToBtnAction(matchToForeignWorkersBtn)
                }
                
            }
            oldWorkerCheckBox.isChecked = employerJob.employerJobNewWorker.bWorkedWorkerPreference
            
            if  employerJob.iEmployerJobPeriodType == EmployerJobPeriodType.ConstantWorker {
                stateWorkerSeg.selectedSegmentIndex = 0 }
            if  employerJob.iEmployerJobPeriodType == EmployerJobPeriodType.TemporaryWorker {
                stateWorkerSeg.selectedSegmentIndex = 1 }
            
            setRoleAndDomainSelected()
            
        }}
        func setRoleAndDomainSelected(){
            if employerJob.iEmploymentDomainType > 0 {
                getdomainSelected(employerJob:  employerJob)
                
                domainSelectedId = Global.sharedInstance.getIndexOfSysTableList(iId:  employerJob.iEmploymentDomainType, sysTableList: workerRoleSysTables.lEmploymentDomainType)
                chooseDomainLbl.text = workerRoleSysTables.lEmploymentDomainType[self.domainSelectedId].nvName
                
            }
            
        }
        func  getdomainSelected(employerJob : EmployerJob)     {
            for employmentDomainRole in workerRoleSysTables.lEmploymentDomainRole {
                if employerJob.iEmploymentDomainType == employmentDomainRole.iEmploymentDomainType {
                    domainSelectedId = employerJob.iEmploymentDomainType
                    chooseRoleLbl.text = employmentDomainRole.nvEmploymentRoleTypeName
                    return
                }
            }
        }
        
        
        
        //MARK: - Table View Functions
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if tableView == domainsTbl {
                return workerRoleSysTables.lEmploymentDomainType.count
            } else {
                return rolesFilterArray.count
            }
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath) as! FamilyStatusTableViewCell
            
            cell.setDisplayData(txtStatus: tableView == domainsTbl ? workerRoleSysTables.lEmploymentDomainType[indexPath.row].nvName + " (\(workerRoleSysTables.lEmploymentDomainType[indexPath.row].iWorkersByEmploymentDomainCount) )" : rolesFilterArray[indexPath.row].nvEmploymentRoleTypeName + " (\(rolesFilterArray[indexPath.row].iWorkersByEmploymentDomainRoleCount)")
            
            return cell
            
            
            
            
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if tableView == domainsTbl {
                chooseDomainLbl.text = workerRoleSysTables.lEmploymentDomainType[indexPath.row].nvName
                if workerRoleSysTables.lEmploymentDomainType[indexPath.row].iId != domainSelectedId {
                    chooseRoleLbl.text = ""
                    self.workersNumberFoundLbl.isHidden = true
                    
                }
                domainSelectedId = workerRoleSysTables.lEmploymentDomainType[indexPath.row].iId
                filterRolesByRole()
                setDominsTblState()
                
            } else {
                setRolesTblState()
                chooseRoleLbl.text = rolesFilterArray[indexPath.row].nvEmploymentRoleTypeName
                domainRoleSelectedId = rolesFilterArray[indexPath.row].iEmploymentDomainRoleId
                setRoleSelected(dominRole : rolesFilterArray[indexPath.row].iEmploymentDomainRoleId)
            }
            
        }
        
        func filterRolesByRole() {
            rolesFilterArray.removeAll()
            for domainRole in workerRoleSysTables.lEmploymentDomainRole {
                if domainRole.iEmploymentDomainType == domainSelectedId {
                    rolesFilterArray.append(domainRole)
                }
            }
            rolesTbl.reloadData()
        }
        
        func setRoleSelected(dominRole : Int) {
            if(domainRoleSelectedId>0){
                getWorkerByEmploymentDomainRole(domainRole: dominRole)
            }
        }
        
        func setDominsTblState() {
            domainsTbl.isHidden = !domainsTbl.isHidden
        }
        
        func setRolesTblState() {
            rolesTbl.isHidden = !rolesTbl.isHidden
        }
        
        func reloadDataOfTables() {
            DispatchQueue.main.async {
                self.domainsTbl.reloadData()
                self.rolesTbl.reloadData()
                
                
                
                
            }
        }
        
        
        //MARK: - Server Function
        func getWorkerRoleSysTables() {
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
            api.sharedInstance.goServer(params : dic,success : {
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                        if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                            self.workerRoleSysTables = self.workerRoleSysTables.getWorkerRoleSysTablesFromDictionary(dic: dicResult)
                            //                        self.getWorkerDomains()
                            //                        self.domainsTbl.reloadData()
                            //                        self.rolesTbl.reloadData()
                            self.reloadDataOfTables()
                            self.setDisplayData()
                            
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : api.sharedInstance.GetWorkerRoleSysTables)
        }
        
        func getWorkerByEmploymentDomainRole(domainRole : Int) {
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            dic["iEmploymentDomainRoleId"] = domainRole as AnyObject?
            api.sharedInstance.goServer(params : dic,success : {
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                        if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                            var workerByEmploymentDomainRole = WorkerByEmploymentDomainRole()
                            workerByEmploymentDomainRole = workerByEmploymentDomainRole.getWorkerByEmploymentDomainRoleFromDic(dic: dicResult)
                            //                        self.workerRoleSysTables = self.workerRoleSysTables.getWorkerRoleSysTablesFromDictionary(dic: dicResult)
                            
                            self.workersNumberFoundLbl.text = "מתוך \(workerByEmploymentDomainRole.iWorkersCount) עובדים במערכת נמצאו \(workerByEmploymentDomainRole.iWorkersByEmploymentDomainRoleCount) עובדים המתאימים לתחום ולתפקיד שבחרת"
                            self.workersNumberFoundLbl.isHidden = false
                            //                        self.getWorkerDomains()
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : api.sharedInstance.GetWorkerByEmploymentDomainRole)
        }
        
        func validateFields() -> Bool {
            var isValid = true
            
            if !((parent as? SearchWorkerModelViewController)?.isNewWorker)!{
                if whoPhone.text == "" {
                    isValid = false
                    Global.sharedInstance.setErrorToTextField(sender: whoPhone)}
            }else{
                
                
                if chooseDomainLbl.text == "" {
                    isValid = false
                    Global.sharedInstance.setErrorToLabel(sender: chooseDomainLbl)
                } else {
                    Global.sharedInstance.setValidToLabel(sender: chooseDomainLbl)
                }
                
                if chooseRoleLbl.text == "" {
                    isValid = false
                    Global.sharedInstance.setErrorToLabel(sender: chooseRoleLbl)
                } else {
                    Global.sharedInstance.setValidToLabel(sender: chooseRoleLbl)
                }
                
                if workersNumberTxt.text == "" {
                    isValid = false
                    Global.sharedInstance.setErrorToTextField(sender: workersNumberTxt)
                } else {
                    Global.sharedInstance.setValidToTextField(sender: workersNumberTxt)
                }
                
                if matchToGeneralBtn.backgroundColor != UIColor.blue2 && matchToYouthBtn.backgroundColor != UIColor.blue2 && matchToRetiredBtn.backgroundColor != UIColor.blue2 && matchToForeignWorkersBtn.backgroundColor != UIColor.blue2 {
                    isValid = false
                }
            }
            //        print("self.parent: \(self.parent)")
            return isValid
        }
        
        
        
        //MARK: - TextField Delegate
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            var limitLength : Int = newLength
            switch textField {
            case workersNumberTxt:
                limitLength = 2
                break
                
            default:
                break
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true
            
            return newLength <= limitLength // Bool
            
        }
        
}
