//
//  SearchNewWorker_WhenViewController.swift
//  FineWork
//
//  Created by User on 16/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
//import JTAppleCalendar

class SearchNewWorker_WhenViewController: UIViewController, SSRadioButtonControllerDelegate, SetDateTextFieldTextDelegate {
    
    //MARK: - Views
    @IBOutlet var calendarView: JTAppleCalendarView!
    @IBOutlet var monthAndYearLbl: UILabel!
    @IBOutlet var prevBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var oneTimeJobRdb: SSRadioButton!
    @IBOutlet var oneTimeJobLbl: UILabel!
    //    @IBOutlet var oneTimeJobLbl: UILabel!
    //    @IBOutlet var constantHoursJobRdb: SSRadioButton!
    @IBOutlet var constantHoursJobRdb: SSRadioButton!
    @IBOutlet var constantHoursJobLbl: UILabel!
    @IBOutlet var changingHoursJobRdb: SSRadioButton!
    @IBOutlet var changingHoursJobLbl: UILabel!
    
    @IBOutlet var chooseDateByCalendarView: UIView!
    @IBOutlet var calendarDateLbl: UILabel!
    //    @IBOutlet var calendarDateIconLbl: UILabel!
    @IBOutlet var calendarDateIconLbl: UILabel!
    @IBOutlet var calendarFromHourLbl: UILabel!
    @IBOutlet var calendarFromHourIconLbl: UILabel!
    @IBOutlet var calendarToHourLbl: UILabel!
    @IBOutlet var calendarToHourIconLbl: UILabel!
    @IBOutlet var chooseDateByCalendarHeightFromTopCon: NSLayoutConstraint!
    
    
    @IBOutlet var saveDateView: UIView!
    @IBOutlet var removeDateBtn: UIButton!
    @IBOutlet var saveDateBtn: UIButton!
    
    
    @IBOutlet var chooseDateFromPickerView: UIView!
    //    @IBOutlet var pickerFromDateLbl: UILabel!
    @IBOutlet var chooseDateByPickerrHeightFromTopCon: NSLayoutConstraint!
    @IBOutlet var pickerFromDateLbl: UILabel!
    @IBOutlet var pickerFromDateIconLbl: UILabel!
    @IBOutlet var pickerToDateLbl: UILabel!
    @IBOutlet var pickerToDateIconLbl: UILabel!
    @IBOutlet var pickerFromHourLbl: UILabel!
    @IBOutlet var pickerFromHourIconLbl: UILabel!
    @IBOutlet var pickerToHourLbl: UILabel!
    
    @IBOutlet var pickerToHourIconLbl: UILabel!
    
    @IBOutlet var nextPageBtnHeightFromTopCon: NSLayoutConstraint!
    //    @IBOutlet var nextPageBtnHeightFromTopCon: NSLayoutConstraint!
    @IBOutlet var nextPageBtn: UIButton!
    //MARK: - Actions
    @IBAction func prevBtnAction(_ sender: Any) {
        calendarView.scrollToSegment(SegmentDestination.previous)
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        calendarView.scrollToSegment(SegmentDestination.next)
    }
    
    @IBAction func removeDateBtnAction(_ sender: Any) {
        if indexToRemoveOrUpdate > -1 && indexToRemoveOrUpdate < lEmployerJobTimes.count {
            lEmployerJobTimes.remove(at: indexToRemoveOrUpdate)
            indexToRemoveOrUpdate = -1
            calendarView.reloadData()
        }
    }
    
    @IBAction func saveDateBtnAction(_ sender: Any) {
        //validate fields
        if indexToRemoveOrUpdate > -1 && indexToRemoveOrUpdate < lEmployerJobTimes.count {
            if validateDates() {
                if !validateDatesByCheckHolidays(date: formatter.date(from: calendarDateLbl.text!)!, fromTimeString: calendarFromHourLbl.text!, toTimeString: calendarToHourLbl.text!) {
                    
                } else {
                    lEmployerJobTimes[indexToRemoveOrUpdate].tFromHoure = calendarFromHourLbl.text!
                    lEmployerJobTimes[indexToRemoveOrUpdate].tToHoure = calendarToHourLbl.text!
                }
            }
        } else {
            if validateFields() {
                
                if validateDates() {
                    if !validateDatesByCheckHolidays(date: formatter.date(from: calendarDateLbl.text!)!, fromTimeString: calendarFromHourLbl.text!, toTimeString: calendarToHourLbl.text!) {
                        
                    } else {
                        let time = EmployerJobTime()
                        time.dFromDate = calendarDateLbl.text!
                        time.tFromHoure = calendarFromHourLbl.text!
                        time.tToHoure = calendarToHourLbl.text!
                        lEmployerJobTimes.append(time)
                        calendarView.reloadData()
                        Alert.sharedInstance.showAlert(mess: "השמירה הצליחה")
                        calendarFromHourLbl.text! = ""
                        calendarToHourLbl.text! = ""
                        
                    }
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים!")
            }
        }
    }
    @IBAction func nextTabBtnAction(_ sender: Any) {
        if (changingHoursJobRdb.isSelected && lEmployerJobTimes.count > 0) || (!changingHoursJobRdb.isSelected && validateFields()) {
            
            if validateDates() {
                if oneTimeJobRdb.isSelected && !validateDatesByCheckHolidays(date: formatter.date(from: calendarDateLbl.text!)!, fromTimeString: calendarFromHourLbl.text!, toTimeString: calendarToHourLbl.text!) {
                    
                } else {
                    setlEmployerJobTimes()
                }
                
                
            }
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים!")
        }
    }
    //MARK: - Variables
    // calendar vars
    var numberOfRows = 6
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    var prePostVisibility: ((CellState,EmployerDayCalendarCollectionViewCell?)->())?
    var hasStrictBoundaries = true
    let firstDayOfWeek: DaysOfWeek = .sunday
    let disabledColor = UIColor.lightGray
    let enabledColor = UIColor.blue
    let dateCellSize: CGFloat? = nil
    var monthSize: MonthSize? = nil
    var prepostHiddenValue = false
    
    // general vars
    var radioButtonController: SSRadioButtonsController?
    var indexToRemoveOrUpdate : Int = -1
    let timeFormater = DateFormatter()
    var globalParams = Array<GlobalParameters>()
    
    // delegates
    var changeScrollViewDelegate : ChangeScrollViewHeightDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        calendarView.register
        calendarView.register(UINib(nibName: "EmployerHeaderCalendarCollectionReusableView", bundle: Bundle.main),
                              forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                              withReuseIdentifier: "EmployerHeaderCalendarCollectionReusableView")
        
        calendarView.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        calendarView.minimumLineSpacing = 0
        
        calendarView.scrollToMonthOfDate(Date())
        calendarView.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        //        calendarView.sect = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        //        calendarView.scrollToHeaderForDate(Date())
        
        //        calendarView.cellSi
        //            = CGSize(width: calendarView.frame.width / 7, height: calendarView.frame.height / 6)
        if AppDelegate.isDeviceLanguageRTL() {
            calendarView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        
        
        
        calendarDateIconLbl.text = FlatIcons.sharedInstance.CALANDER
        calendarFromHourIconLbl.text = FlatIcons.sharedInstance.CALANDER
        calendarToHourIconLbl.text = FlatIcons.sharedInstance.CALANDER
        pickerFromDateIconLbl.text = FlatIcons.sharedInstance.CALANDER
        pickerToDateIconLbl.text = FlatIcons.sharedInstance.CALANDER
        pickerFromHourIconLbl.text = FlatIcons.sharedInstance.CALANDER
        pickerToHourIconLbl.text = FlatIcons.sharedInstance.CALANDER
        
        
        radioButtonController = SSRadioButtonsController(buttons: oneTimeJobRdb, constantHoursJobRdb, changingHoursJobRdb)
        radioButtonController?.delegate = self
        radioButtonController?.shouldLetDeSelect = true
        
        calendarDateLbl.tag = 13
        pickerFromDateLbl.tag = 14
        pickerToDateLbl.tag = 15
        
        calendarDateLbl.isUserInteractionEnabled = true
        pickerFromDateLbl.isUserInteractionEnabled = true
        pickerToDateLbl.isUserInteractionEnabled = true
        pickerFromHourLbl.isUserInteractionEnabled = true
        pickerToHourLbl.isUserInteractionEnabled = true
        calendarFromHourLbl.isUserInteractionEnabled = true
        calendarToHourLbl.isUserInteractionEnabled = true
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapCalendarDateLbl))
        calendarDateLbl.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPickerFromDateLbl))
        pickerFromDateLbl.addGestureRecognizer(tap2)
        
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPickerToDateLbl))
        pickerToDateLbl.addGestureRecognizer(tap3)
        
        let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapCalendarFromHourLbl))
        calendarFromHourLbl.addGestureRecognizer(tap4)
        
        let tap5: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapCalendarToHourLbl))
        calendarToHourLbl.addGestureRecognizer(tap5)
        
        let tap6: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPickerFromHourLbl))
        pickerFromHourLbl.addGestureRecognizer(tap6)
        
        let tap7: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPickerToHourLbl))
        pickerToHourLbl.addGestureRecognizer(tap7)
        
        
        Global.sharedInstance.setButtonCircle(sender: saveDateBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: removeDateBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: nextPageBtn, isBorder: true)
        
        timeFormater.dateFormat = "HH:mm"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        resetData()
        setDisplayData()
    }
    
    //MARK: - Set data to edit
    func setDisplayData(){
        let  employerJob = ((parent as? SearchWorkerModelViewController)?.employerJob)!
        
        var time = EmployerJobTime()
        if employerJob.lEmployerJobTimes.count > 0 {
        time = employerJob.lEmployerJobTimes[0]
        
        
        if employerJob.iEmployerJobScheduleType ==  EmployerJobScheduleType.OneTimeJob {
            oneTimeJobRdb.isSelected = true
            calendarDateLbl.text =  Global.sharedInstance.getStringFromDateString( dateString: time.dFromDate!)
            calendarFromHourLbl.text =  time.tFromHoure
            calendarToHourLbl.text = time.tToHoure
        }else if employerJob.iEmployerJobScheduleType == EmployerJobScheduleType.ConstantHoursJob   {
            constantHoursJobRdb.isSelected = true
            pickerFromDateLbl.text =  Global.sharedInstance.getStringFromDateString(dateString: time.dFromDate!)
            pickerToDateLbl.text =  Global.sharedInstance.getStringFromDateString(dateString: time.dToDate!)
            pickerFromHourLbl.text = time.tFromHoure
            pickerToHourLbl.text = time.tToHoure
            
        } else {
            changingHoursJobRdb.isSelected = true
                    lEmployerJobTimes = employerJob.lEmployerJobTimes
            for timeConvert in lEmployerJobTimes {
                timeConvert.dFromDate = Global.sharedInstance.getStringFromDateString(dateString: timeConvert.dFromDate!)
            }

            calendarView.reloadData()

                   }
        
        }
        
        ///
        
        
//        if oneTimeJobRdb.isSelected {
//            lEmployerJobTimes.removeAll()
//            let time = EmployerJobTime()
//            time.dFromDate = calendarDateLbl.text!
//            
//            time.tFromHoure = calendarFromHourLbl.text!
//            time.tToHoure = calendarToHourLbl.text!
//            lEmployerJobTimes.append(time)
//        } else if constantHoursJobRdb.isSelected {
//            lEmployerJobTimes.removeAll()
//            let time = EmployerJobTime()
//            time.dFromDate = pickerFromDateLbl.text!
//            
//            time.dToDate = pickerToDateLbl.text!
//            
//            time.tFromHoure = pickerFromHourLbl.text!
//            time.tToHoure = pickerToHourLbl.text!
//            lEmployerJobTimes.append(time)
//        }
//        
        
    }
    ///
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - Calendar Functions
    var prevSection = 0
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo, isEndScroll: Bool) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
        print("month : \(monthName)")
        print("\(year)")
        monthAndYearLbl.text = monthName + " " + String(year)
        
        //        calendarView.calendarOffsetIsAlreadyAtScrollPosition(forIndexPath: IndexPath(row: 0, section: calendarView.currentSection()!))
        
        //        calendarView.offse
        //        calendarView.scrollToDate(Date())
        //        SegmentDestination
        
        //        if isEndScroll {
        //            if let section = calendarView.currentSection() {
        ////                calendarView.scrollToItem(at: IndexPath(row: 0, section: section), at: .right, animated: true)
        //                //            calendarView.scrollToHeaderInSection(section, triggerScrollToDateDelegate: false, withAnimation: true, extraAddedOffset: 0, completionHandler: nil)
        //
        //
        //                if prevSection > section {
        //                    calendarView.scrollToSegment(SegmentDestination.previous)
        //                } else if prevSection < section {
        //                    calendarView.scrollToSegment(SegmentDestination.next)
        //                }
        //
        //                prevSection = section
        //                }
        
        //                calendarView.scrollToHeaderInSection(section, extraAddedOffset: 0)
        //        }
        
        //        print("calendarView.currentSection() \(calendarView.currentSection())")
        //28-05-2017
        if isEndScroll {
            calendarView.scrollToSection()
            changeScrollViewDelegate.changeScrollViewHeight(heightToadd: 0)
        }
    }
    
    func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        prePostVisibility?(cellState, cell as? EmployerDayCalendarCollectionViewCell)
        
    }
    
    // Function to handle the calendar selection
    func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? EmployerDayCalendarCollectionViewCell else {return }
        //        switch cellState.selectedPosition() {
        //        case .full:
        //            myCustomCell.backgroundColor = .green
        //        case .left:
        //            myCustomCell.backgroundColor = .yellow
        //        case .right:
        //            myCustomCell.backgroundColor = .red
        //        case .middle:
        //            myCustomCell.backgroundColor = .blue
        //        case .none:
        //            myCustomCell.backgroundColor = nil
        //        }
        //
        if cellState.isSelected {
            //            myCustomCell.selectedView.layer.cornerRadius =  13
            //            myCustomCell.selectedView.isHidden = false
            //            myCustomCell.layer.cornerRadius =  13
            if testCalendar.isDateInToday(cellState.date) || (cellState.date).compare(Date()) == ComparisonResult.orderedDescending {
                myCustomCell.backgroundColor = UIColor.myTurquoise
                calendarDateLbl.text = formatter.string(from: cellState.date)
            } else {
                Alert.sharedInstance.showAlert(mess: "תאריך לא תקין!")
                myCustomCell.backgroundColor = UIColor.white
                calendarDateLbl.text = ""
            }
            
        } else {
            //            myCustomCell.selectedView.isHidden = true
            //            myCustomCell.layer.cornerRadius =  0
            myCustomCell.backgroundColor = UIColor.white
        }
        
        
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? EmployerDayCalendarCollectionViewCell  else {
            return
        }
        
        if cellState.isSelected {
            //            myCustomCell.dayLbl.textColor = UIColor.white
            //            myCustomCell.backgroundColor = UIColor.myTurquoise
            calendarFromHourLbl.text = ""
            calendarToHourLbl.text = ""
            indexToRemoveOrUpdate = -1
            for index in stride(from: 0, to: lEmployerJobTimes.count, by: 1) {
                if /*Global.sharedInstance.getDateFromString(dateString: Global.sharedInstance.getStringFromDateString(dateString: lEmployerJobTimes[index].dFromDate!)) as Date*/formatter.date(from: lEmployerJobTimes[index].dFromDate!) == cellState.date {
                    indexToRemoveOrUpdate = index
                    calendarFromHourLbl.text = lEmployerJobTimes[index].tFromHoure
                    calendarToHourLbl.text = lEmployerJobTimes[index].tToHoure
                    break
                }
            }
            
        } else {
            //            myCustomCell.backgroundColor = UIColor.white
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.dayLbl.textColor = UIColor.black
                if changingHoursJobRdb.isSelected {
                    for item in lEmployerJobTimes {
                        if /*Global.sharedInstance.getDateFromString(dateString: Global.sharedInstance.getStringFromDateString(dateString: item.dFromDate!)) as Date*/formatter.date(from: item.dFromDate!) == cellState.date {
                            myCustomCell.dayLbl.textColor = UIColor.myFucsia
                            break
                        } else {
                            myCustomCell.dayLbl.textColor = UIColor.black
                        }
                    }
                }
            } else {
                myCustomCell.dayLbl.textColor = UIColor.gray
            }
        }
        
        
    }
    
    //MARK: - SSRadioButtonControllerDelegate
    func didSelectButton(_ aButton: UIButton?) {
        changeScrollViewDelegate.scrollTop!()
        lEmployerJobTimes.removeAll()
        if aButton != nil {
            setButtonBehavior(aButton!)
            //            switch aButton! {
            //            case oneTimeJobRdb:
            //                setOneTimeJobActiivy()
            //                break
            //            case constantHoursJobRdb:
            //                setConstantHoursJobActiivy()
            //                break
            //            case changingHoursJobRdb:
            //                setChangingHoursJobActiivy()
            //                break
            //            default:
            //                break
            //            }
        }
    }
    
    func setButtonBehavior(_ aButton: UIButton) {
        switch aButton {
        case oneTimeJobRdb:
            setOneTimeJobActivity()
            break
        case constantHoursJobRdb:
            setConstantHoursJobActivity(isConstantWorker: false)
            break
        case changingHoursJobRdb:
            setChangingHoursJobActivity()
            break
        default:
            break
        }
    }
    
    func setOneTimeJobActivity() {
        chooseDateByCalendarHeightFromTopCon.constant = 15
        //        chooseDateByPickerrHeightFromTopCon.constant = 15
        chooseDateByCalendarView.isHidden = false
        chooseDateFromPickerView.isHidden = true
        saveDateView.isHidden = true
        changeScrollViewDelegate.setScrollViewEnable!(isEnable: true)
        nextPageBtnHeightFromTopCon.constant = chooseDateByCalendarView.frame.height + 10 + 15 + 10
    }
    
    func setConstantHoursJobActivity(isConstantWorker : Bool) {
        //        chooseDateByCalendarHeightFromTopCon.constant = 15
        
        chooseDateByPickerrHeightFromTopCon.constant = isConstantWorker ?  15 : (changingHoursJobRdb.frame.height * 3) + 30
        chooseDateByCalendarView.isHidden = true
        chooseDateFromPickerView.isHidden = false
        saveDateView.isHidden = true
        changeScrollViewDelegate.setScrollViewEnable!(isEnable: false)
        nextPageBtnHeightFromTopCon.constant = (chooseDateFromPickerView.frame.height * 2) + 20
    }
    
    func setChangingHoursJobActivity() {
        chooseDateByCalendarHeightFromTopCon.constant = 15
        //        chooseDateByPickerrHeightFromTopCon.constant = 15
        chooseDateByCalendarView.isHidden = false
        chooseDateFromPickerView.isHidden = true
        saveDateView.isHidden = false
        changeScrollViewDelegate.setScrollViewEnable!(isEnable: true)
        changeScrollViewDelegate.changeScrollViewHeight(heightToadd: heightForChangingHourJob)
        heightForChangingHourJob = 0
        nextPageBtnHeightFromTopCon.constant = chooseDateByCalendarView.frame.height + saveDateView.frame.height + 10 + 15 + 10
    }
    
    var heightForChangingHourJob : CGFloat = 40
    
    //    func setVisibleToRdb() {
    //
    //    }
    //
    //    func setConstantWorkerActivity() {
    //
    //    }
    
    func resetData(employerJobPeriodType : EmployerJobPeriodType, employerJobScheduleType : EmployerJobScheduleType) {
        //        chooseDateByCalendarHeightFromTopCon.constant = 15
        //        chooseDateByPickerrHeightFromTopCon.constant = 15
        switch employerJobPeriodType {
        case EmployerJobPeriodType.ConstantWorker:
            setConstantHoursJobActivity(isConstantWorker: true)
            oneTimeJobRdb.isSelected = false
            constantHoursJobRdb.isSelected = true
            changingHoursJobRdb.isSelected = false
            break
        case EmployerJobPeriodType.TemporaryWorker:
            switch employerJobScheduleType {
            case EmployerJobScheduleType.OneTimeJob:
                oneTimeJobRdb.isSelected = true
                constantHoursJobRdb.isSelected = false
                changingHoursJobRdb.isSelected = false
                setButtonBehavior(oneTimeJobRdb)
                break
            case EmployerJobScheduleType.ConstantHoursJob:
                oneTimeJobRdb.isSelected = false
                constantHoursJobRdb.isSelected = true
                changingHoursJobRdb.isSelected = false
                setButtonBehavior(constantHoursJobRdb)
                break
            case EmployerJobScheduleType.ChangingHoursJob:
                oneTimeJobRdb.isSelected = false
                constantHoursJobRdb.isSelected = false
                changingHoursJobRdb.isSelected = true
                setButtonBehavior(changingHoursJobRdb)
                break
            }
            //            chooseDateByCalendarView.isHidden = true
            //            chooseDateFromPickerView.isHidden = true
            //            saveDateView.isHidden = false
            //            changeScrollViewDelegate.setScrollViewEnable!(isEnable: false)
            //            nextPageBtnHeightFromTopCon.constant = (chooseDateFromPickerView.frame.height * 2) + 20
            break
        }
        
    }
    
    //MARK: - openDatePicker
    //MARK: - taps
    func tapCalendarDateLbl()
    {
        openDatePicker(dateLbl : calendarDateLbl, isTimeMode: false)
    }
    
    func tapPickerFromDateLbl()
    {
        openDatePicker(dateLbl : pickerFromDateLbl, isTimeMode: false)
    }
    
    func tapPickerToDateLbl()
    {
        openDatePicker(dateLbl : pickerToDateLbl, isTimeMode: false)
    }
    
    func tapCalendarToHourLbl ()
    {
        openDatePicker(dateLbl : calendarToHourLbl, isTimeMode: true)
    }
    
    func tapCalendarFromHourLbl()
    {
        openDatePicker(dateLbl : calendarFromHourLbl, isTimeMode: true)
    }
    
    func tapPickerToHourLbl()
    {
        openDatePicker(dateLbl : pickerToHourLbl, isTimeMode: true)
    }
    
    func tapPickerFromHourLbl()
    {
        openDatePicker(dateLbl : pickerFromHourLbl, isTimeMode: true)
    }
    
    func openDatePicker(dateLbl : UILabel, isTimeMode: Bool) {
        
        //dismissKeyBoard()
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let datePickerPopUp: DatePickerPopUpViewController = story.instantiateViewController(withIdentifier:"DatePickerPopUpViewController")as! DatePickerPopUpViewController
        
        datePickerPopUp.setDateTextFieldTextDelegate = self
        datePickerPopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        datePickerPopUp.tag = dateLbl.tag
        datePickerPopUp.dateDefault = dateLbl.text
        datePickerPopUp.isTimeMode = isTimeMode
        if isTimeMode {
            datePickerPopUp.lblDateTiem = dateLbl
        } else {
            datePickerPopUp.lblDateTiem = nil
        }
        
        self.present(datePickerPopUp, animated: true, completion: nil)
    }
    
    func SetDateFieldText(dateAsString : String, tagOfField : Int) {
        //        validateHebrewDate(date: formatter.date(from: dateAsString)!)
        switch tagOfField {
        case calendarDateLbl.tag:
            calendarDateLbl.text = dateAsString
            let date : Date = Global.sharedInstance.getDateFromString(dateString: calendarDateLbl.text!) as Date
            calendarView.selectDates(from: date, to: date)
            calendarView.scrollToMonthOfDate(date)
            //            Global.sharedInstance.getDateFromString(dateString: calendarDateLbl.text!)
            break
        case pickerFromDateLbl.tag:
            pickerFromDateLbl.text = dateAsString
            break
        case pickerToDateLbl.tag:
            pickerToDateLbl.text = dateAsString
            break
        default:
            break
        }
        
        changeScrollViewDelegate.changeScrollViewHeight(heightToadd: 0)
        //        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func validateFields() -> Bool {
        var isValid = true
        
        //        if chooseDateByPickerrHeightFromTopCon.constant == 15 {
        ////            constantHoursJobRdb.isSelected = true
        ////            oneTimeJobRdb.isSelected
        //            radioButtonController?.selectedButton() = constantHoursJobRdb
        //        }
        
        if oneTimeJobRdb.isSelected || changingHoursJobRdb.isSelected {
            if calendarDateLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: calendarDateLbl)
                isValid = false
            } else {
                Global.sharedInstance.setValidToLabel(sender: calendarDateLbl)
            }
            
            if calendarFromHourLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: calendarFromHourLbl)
                isValid = false
            } else {
                Global.sharedInstance.setValidToLabel(sender: calendarFromHourLbl)
            }
            
            if calendarToHourLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: calendarToHourLbl)
                isValid = false
            } else {
                Global.sharedInstance.setValidToLabel(sender: calendarToHourLbl)
            }
        } else if constantHoursJobRdb.isSelected {
            if pickerFromDateLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: pickerFromDateLbl)
                isValid = false
            } else {
                Global.sharedInstance.setValidToLabel(sender: pickerFromDateLbl)
            }
            
            if pickerToDateLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: pickerToDateLbl)
                isValid = false
            } else {
                Global.sharedInstance.setValidToLabel(sender: pickerToDateLbl)
            }
            
            if pickerFromHourLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: pickerFromHourLbl)
                isValid = false
            } else {
                Global.sharedInstance.setValidToLabel(sender: pickerFromHourLbl)
            }
            
            if pickerToHourLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: pickerToHourLbl)
                isValid = false
            } else {
                Global.sharedInstance.setValidToLabel(sender: pickerToHourLbl)
            }
        } else {
            if lEmployerJobTimes.count == 0 {
                isValid = false
            }
        }
        
        return isValid
    }
    
    func validateDates() -> Bool {
        var isValid = true
        
        //        if chooseDateByPickerrHeightFromTopCon.constant == 15 {
        //                        constantHoursJobRdb.isSelected = true
        //                        oneTimeJobRdb.isSelected
        //            radioButtonController?.selectedButton() = constantHoursJobRdb
        //        }
        
        if oneTimeJobRdb.isSelected || changingHoursJobRdb.isSelected &&  calendarFromHourLbl.text! != "" && calendarToHourLbl.text! != ""{
            if (timeFormater.date(from: calendarFromHourLbl.text!))?.compare(timeFormater.date(from: calendarToHourLbl.text!)!) == ComparisonResult.orderedAscending {
                Global.sharedInstance.setValidToLabel(sender: calendarToHourLbl)
                
            } else {
                
                Alert.sharedInstance.showAlert(mess: "שעת סיום צריכה להיות אחרי שעת התחלה!")
                isValid = false
                
                Global.sharedInstance.setErrorToLabel(sender: calendarToHourLbl)
            }
        } else if constantHoursJobRdb.isSelected {
            if (formatter.date(from: pickerFromDateLbl.text!))?.compare(formatter.date(from: pickerToDateLbl.text!)!) == ComparisonResult.orderedAscending {
                Global.sharedInstance.setValidToLabel(sender: pickerToDateLbl)
                
            } else {
                Alert.sharedInstance.showAlert(mess: "תאריך סיום צריך להיות אחרי תאריך התחלה!")
                Global.sharedInstance.setErrorToLabel(sender: pickerToDateLbl)
                
                return false
                
                
            }
            
            if (timeFormater.date(from: pickerFromHourLbl.text!))?.compare(timeFormater.date(from: pickerToHourLbl.text!)!) == ComparisonResult.orderedAscending {
                Global.sharedInstance.setValidToLabel(sender: pickerToHourLbl)
                
            } else {
                Alert.sharedInstance.showAlert(mess: "שעת סיום צריכה להיות אחרי שעת התחלה!")
                isValid = false
                
                Global.sharedInstance.setErrorToLabel(sender: pickerToHourLbl)
                
            }
        }
        
        return isValid
    }
    
    func validateDatesByCheckHolidays(date : Date, fromTimeString : String, toTimeString : String) -> Bool {
        
        switch checkHolidays(date: date) {
        case DateType.Friday:
            return checkHolidayEve(isShabbat: true, timeString: toTimeString)
        case DateType.HolidayEve:
            return checkHolidayEve(isShabbat: false, timeString: toTimeString)
        case DateType.Shabbat:
            return checkHoliday(isShabbat: true, timeString: fromTimeString)
        case DateType.Holiday:
            return checkHoliday(isShabbat: false, timeString: fromTimeString)
        default:
            return true
        }
        
    }
    
    func checkHolidayEve(isShabbat : Bool, timeString : String) -> Bool {
        for item in globalParams {
            if item.iGlobalCodeId == GlobalParametersType.HolidayEve.rawValue {
                if (timeFormater.date(from: item.nvGlobalParamValue))?.compare(timeFormater.date(from: timeString)!) == ComparisonResult.orderedAscending {
                    if isShabbat {
                        Alert.sharedInstance.showAlert(mess: "לא ניתן לבחור תאריך החל בשבת")
                    } else {
                        Alert.sharedInstance.showAlert(mess: "לא ניתן לבחור תאריך החל באחד מחגי ישראל")
                    }
                    return false
                } else {
                    return true
                }
            }
        }
        return true
    }
    
    func checkHoliday(isShabbat : Bool, timeString : String) -> Bool {
        for item in globalParams {
            if item.iGlobalCodeId == GlobalParametersType.Holiday.rawValue {
                if (timeFormater.date(from: timeString))?.compare(timeFormater.date(from: item.nvGlobalParamValue)!) == ComparisonResult.orderedAscending {
                    if isShabbat {
                        Alert.sharedInstance.showAlert(mess: "לא ניתן לבחור תאריך החל בשבת")
                    } else {
                        Alert.sharedInstance.showAlert(mess: "לא ניתן לבחור תאריך החל באחד מחגי ישראל")
                    }
                    return false
                } else {
                    return true
                }
            }
        }
        return true
    }
    
    //return: 1 = holiday, 2 = holiday evening, 3 = shabbat, 4 = friday, 0 = regular
    func checkHolidays(date : Date) -> DateType {
        let hebrew = Locale(identifier: "he_IL")  // Hebrew, Israel
        let calendar = Calendar(identifier: Calendar.Identifier.hebrew)
        let dateFormat = DateFormatter()
        dateFormat.locale = hebrew
        dateFormat.calendar = calendar
        dateFormat.dateStyle = .long
        
        //check shabbat, shabbat evening
        let dayOfWeek = calendar.component(.weekday, from: date)
        
        if dayOfWeek == 6 {
            return DateType.Friday
        }
        if dayOfWeek == 7 {
            return DateType.Shabbat
        }
        
        //check holiday, holiday evening
        let month = calendar.component(.month, from: date)
        
        let dayOfMonth = calendar.component(.day, from: date)
        
        //Nisan
        if month == 8 {
            if dayOfMonth == 15 || dayOfMonth == 21 {
                return DateType.Holiday
            }
            if dayOfMonth == 14 || dayOfMonth == 20 {
                return DateType.HolidayEve
            }
        }
        //Sivan
        if month == 10 {
            if dayOfMonth == 6 { return DateType.Holiday }
            if dayOfMonth == 5 { return DateType.HolidayEve }
        }
        //Tishrei
        if month == 1 {
            if dayOfMonth == 1 || dayOfMonth == 2 || dayOfMonth == 10 || dayOfMonth == 15 ||
                dayOfMonth == 22 {
                return DateType.Holiday
            }
            if dayOfMonth == 9 || dayOfMonth == 14 || dayOfMonth == 21 {
                return DateType.HolidayEve
            }
        }
        //Elul
        if month == 13 {
            if dayOfMonth == 29 {
                return DateType.HolidayEve
            }
        }
        
        return DateType.Regular;
    }
    
    
    var lEmployerJobTimes : Array<EmployerJobTime> = []
    func setlEmployerJobTimes() {
        
        
        if oneTimeJobRdb.isSelected {
            lEmployerJobTimes.removeAll()
            let time = EmployerJobTime()
            time.dFromDate = calendarDateLbl.text!
            //            time.dFromDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: calendarDateLbl.text!))
            //                Global.sharedInstance.getStringFromDateString(dateString: calendarDateLbl.text!)
            time.tFromHoure = calendarFromHourLbl.text!
            time.tToHoure = calendarToHourLbl.text!
            lEmployerJobTimes.append(time)
        } else if constantHoursJobRdb.isSelected {
            lEmployerJobTimes.removeAll()
            let time = EmployerJobTime()
            time.dFromDate = pickerFromDateLbl.text!
            //            time.dFromDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: pickerFromDateLbl.text!))
            //                Global.sharedInstance.getStringFromDateString(dateString: pickerFromDateLbl.text!)
            time.dToDate = pickerToDateLbl.text!
            //            time.dToDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: pickerToDateLbl.text!))
            //                Global.sharedInstance.getStringFromDateString(dateString: pickerToDateLbl.text!)
            time.tFromHoure = pickerFromHourLbl.text!
            time.tToHoure = pickerToHourLbl.text!
            lEmployerJobTimes.append(time)
        }
        
        openAnotherPageDelegate.openNextTab!(tag: 2)
        //        if let button = radioButtonController?.selectedButton() {
        //        switch button {
        //        case oneTimeJobRdb:
        //            let time = EmployerJobTime()
        //            time.dFromDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: calendarDateLbl.text!))
        ////                Global.sharedInstance.getStringFromDateString(dateString: calendarDateLbl.text!)
        //            time.tFromHoure = calendarFromHourLbl.text!
        //            time.tToHoure = calendarToHourLbl.text!
        //            lEmployerJobTimes.append(time)
        //            break
        //        case constantHoursJobRdb:
        //            let time = EmployerJobTime()
        //            time.dFromDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: pickerFromDateLbl.text!))
        ////                Global.sharedInstance.getStringFromDateString(dateString: pickerFromDateLbl.text!)
        //            time.dToDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: pickerToDateLbl.text!))
        ////                Global.sharedInstance.getStringFromDateString(dateString: pickerToDateLbl.text!)
        //            time.tFromHoure = pickerFromHourLbl.text!
        //            time.tToHoure = pickerToHourLbl.text!
        //            lEmployerJobTimes.append(time)
        //            break
        //        case changingHoursJobRdb:
        ////            let time = EmployerJobTime()
        ////            time.dFromDate = Global.sharedInstance.getStringFromDateString(dateString: calendarDateLbl.text!)
        ////            time.tFromHoure = calendarFromHourLbl.text!
        ////            time.tToHoure = calendarToHourLbl.text!
        ////            lEmployerJobTimes.append(time)
        //            break
        //        default:
        //            break
        //        }
        //        }
    }
    
}


// MARK : JTAppleCalendarDelegate
extension SearchNewWorker_WhenViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.timeZone = testCalendar.timeZone
        formatter.locale = testCalendar.locale
        
        
        
        let startDate = /*formatter.date(from: "01/01/2017")!*/Date()
        
        var components = DateComponents()
        components.setValue(10, for: Calendar.Component.year)
        let date = Date()
        let endDate = Calendar.current.date(byAdding: components, to: date)
        
        //        let endDate = /*formatter.date(from: "2018 02 01")!*/Date(timeIntervalSinceNow: 30)
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate!,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: firstDayOfWeek,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "EmployerDayCalendarCollectionViewCell", for: indexPath) as! EmployerDayCalendarCollectionViewCell
        
        //        let width = (calendarView.frame.width-7) / 7
        myCustomCell.frame.size = CGSize(width: (calendarView.frame.width-7-20) / 7, height: (calendarView.frame.height-6) / 6)
        
        
        myCustomCell.dayLbl.text = cellState.text
        if AppDelegate.isDeviceLanguageRTL() {
            myCustomCell.dayLbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        }
        if testCalendar.isDateInToday(date) {
            myCustomCell.layer.borderWidth = 2
            myCustomCell.layer.borderColor = UIColor.red.cgColor
        } else {
            //            myCustomCell.backgroundColor = UIColor.white
            
            //            myCustomCell.layer.borderWidth = 0
            myCustomCell.layer.borderWidth = 0.5
            myCustomCell.layer.borderColor = UIColor.gray.cgColor
        }
        
        if changingHoursJobRdb.isSelected {
            for item in lEmployerJobTimes {
                if /*Global.sharedInstance.getDateFromString(dateString: Global.sharedInstance.getStringFromDateString(dateString: item.dFromDate!)) as Date*/formatter.date(from: item.dFromDate!) == date {
                    myCustomCell.dayLbl.textColor = UIColor.myFucsia
                    break
                } else {
                    myCustomCell.dayLbl.textColor = UIColor.black
                }
            }
        } else {
            myCustomCell.dayLbl.textColor = UIColor.black
        }
        
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
        return myCustomCell
    }
    
    
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
        
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates, isEndScroll: false)
    }
    
    func scrollDidEndDecelerating(for calendar: JTAppleCalendarView) {
        let visibleDates = calendarView.visibleDates()
        //        let dateWeShouldNotCross = formatter.date(from: "2017 08 07")!
        //        let dateToScrollBackTo = formatter.date(from: "2017 07 03")!
        //        if visibleDates.monthDates.contains (where: {$0.date >= dateWeShouldNotCross}) {
        //            calendarView.scrollToDate(dateToScrollBackTo)
        //            return
        //        }
        self.setupViewsOfCalendar(from: visibleDates, isEndScroll: true)
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
        let date = range.start
        //        let month = testCalendar.component(.month, from: date)
        
        let header: JTAppleCollectionReusableView
        //        if month % 2 > 0 {
        //            header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "WhiteSectionHeaderView", for: indexPath)
        //            (header as! WhiteSectionHeaderView).title.text = formatter.string(from: date)
        //        } else {
        header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "EmployerHeaderCalendarCollectionReusableView", for: indexPath)
        (header as! EmployerHeaderCalendarCollectionReusableView).monthLbl.text = formatter.string(from: date)
        //        }
        return header
    }
    
    //    func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
    //        let stride = calendarView.frame.width * CGFloat(indexPath.section)
    //        return CGRect(x: stride + 5, y: 5, width: calendarView.frame.width - 10, height: calendarView.frame.height - 10)
    //    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return monthSize
    }
}

class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            
            layoutAttribute.frame.origin.x = leftMargin
            
            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }
        
        return attributes
    }
}
