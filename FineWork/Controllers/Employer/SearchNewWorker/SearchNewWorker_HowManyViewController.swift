//
//  SearchNewWorker_HowManyViewController.swift
//  FineWork
//
//  Created by User on 21/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SearchNewWorker_HowManyViewController: UIViewController {
    
    //MARK: - Views
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var hourlySalaryTxt: UITextField!
    @IBOutlet var brutoSalaryLbl: UILabel!
    @IBOutlet var salaryTotalLbl: UILabel!
    @IBOutlet var salaryDetailsBtn: UIButton!
    @IBOutlet var bonusOptionBtn: CheckBox!
    @IBOutlet var bonusOptionLbl: UILabel!
    @IBOutlet var nextBtn: UIButton!
    
    @IBOutlet var salaryDetailsBtnHeightFromTopCon: NSLayoutConstraint!
    
    @IBOutlet weak var sumWage: UIButton!
    //MARK: - Actions
    @IBAction func salaryDetailsBtnAction(_ sender: Any) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.text = "פרוט שכר"
        //
        self.present(basePopUp, animated: true, completion: nil)
        
    }
    
    @IBAction func bonusOptionBtnAction(_ sender: Any) {
        bonusOptionBtn.isChecked = !bonusOptionBtn.isChecked
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        if validateFields() {
            openAnotherPageDelegate.openNextTab!(tag: 3)
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים / אינם תקינים")
        }
    }
    
    @IBAction func sumWage(_ sender: Any) {
        if hourlySalaryTxt.text != "" {
            getSumWageEmployerJob()
        }else {
            salaryTotalLbl.isHidden = true
            salaryDetailsBtnHeightFromTopCon.constant = 20

    }
    }
    
    
    //MARK: - Variables
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    var hoursNumber : Double = 1
    var employerJobPeriodType : EmployerJobPeriodType = EmployerJobPeriodType.ConstantWorker
    var iEmployerJobScheduleType : Int = 0
    var employerJobTimes : Array<EmployerJobTime> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        Global.sharedInstance.setButtonCircle(sender: nextBtn, isBorder: true)
        salaryTotalLbl.isHidden = true
        salaryDetailsBtnHeightFromTopCon.constant = 20
        
       // hourlySalaryTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        //hourlySalaryTxt.delegate = self
        
        hourlySalaryTxt.keyboardType = .decimalPad
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = salaryDetailsBtn.titleLabel?.textColor.cgColor//UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: salaryDetailsBtn.frame.size.height * 0.75, width:  salaryDetailsBtn.frame.size.width, height: 1)
        
        border.borderWidth = width
        salaryDetailsBtn.layer.addSublayer(border)
        salaryDetailsBtn.layer.masksToBounds = true
        
        Global.sharedInstance.setButtonCircle(sender: sumWage, isBorder: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        setDisplayData()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Set data to edit
    func setDisplayData(){
        let  employerJob = ((parent as? SearchWorkerModelViewController)?.employerJob)!
        if employerJob.mHourlyWage > 0 {
        hourlySalaryTxt.text = String(employerJob.mHourlyWage)
        }
            bonusOptionBtn.isChecked = employerJob.bBonusOption
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //MARK: - Text Field Functions
//    func textFieldDidChange(_ textField: UITextField) {
//        if textField.text != "" {
//            setSalaryTotal()
//            salaryTotalLbl.isHidden = false
//            switch employerJobPeriodType {
//            case EmployerJobPeriodType.ConstantWorker:
//                salaryTotalLbl.text = "סה״כ עלות לחודש \((Double(textField.text!))! * hoursNumber * 21)"
//                break
//            case EmployerJobPeriodType.TemporaryWorker:
//                salaryTotalLbl.text = "סה״כ עלות המשרה \((Double(textField.text!))! * hoursNumber)"
//                break
//            }
//            salaryDetailsBtnHeightFromTopCon.constant = 50
//        } else {
//            salaryTotalLbl.isHidden = true
//            
//            
//            salaryDetailsBtnHeightFromTopCon.constant = 20
//        }
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case hourlySalaryTxt:
            limitLength = 7
            break
        default:
            break
        }
        
        //        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
        
    }
    
    //MARK: - set data
    func setTotalSalary(employerJobPeriodType : EmployerJobPeriodType, hoursNumber : Double, iEmployerJobScheduleType : Int , lEmployerJobTimes : Array<EmployerJobTime>) {
        self.hoursNumber = hoursNumber
        self.iEmployerJobScheduleType = iEmployerJobScheduleType
        self.employerJobPeriodType = employerJobPeriodType
        self.employerJobTimes = lEmployerJobTimes
    }
    
    func validateFields() -> Bool {
        if hourlySalaryTxt.text == "" {
            Global.sharedInstance.setErrorToTextField(sender: hourlySalaryTxt)
            return false
        } else {
            if Double(hourlySalaryTxt.text!)! > 0 {
                Global.sharedInstance.setValidToTextField(sender: hourlySalaryTxt)
                return true
            } else {
                Global.sharedInstance.setErrorToTextField(sender: hourlySalaryTxt)
                return false
            }
        }
    }
    
    func setSalaryTotal(sumWageEmployerJob : SumWageEmployerJob){
        salaryTotalLbl.isHidden = false
        switch employerJobPeriodType {
        case EmployerJobPeriodType.ConstantWorker:
            salaryTotalLbl.text = "סה״כ עלות לחודש \(sumWageEmployerJob.iSumWage)"
            break
        case EmployerJobPeriodType.TemporaryWorker:
            salaryTotalLbl.text = "סה״כ עלות המשרה \(sumWageEmployerJob.iSumWage)"
            break
        }
        salaryDetailsBtnHeightFromTopCon.constant = 50
        
    }
    //MARK: - Server Function
    
    func getSumWageEmployerJob() {
         //Generic.sharedInstance.showNativeActivityIndicator(cont: parent != nil ? parentVC! : self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerJobScheduleType"] = self.iEmployerJobScheduleType as AnyObject?
        dic["mHourlyWage"] = self.hourlySalaryTxt.text as AnyObject?
        dic["lEmployerJobTimes"] = createDicToListEmployerJobTimes(employerJobTimes: self.employerJobTimes) as AnyObject
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                  // Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parentVC != nil ? self.parentVC! : self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                   let sumWageEmployerJob =  SumWageEmployerJob()
                 // sumWageEmployerJob =   sumWageEmployerJob.getSumWageEmployerJobFromDic(dic: dicResult)
              self.setSalaryTotal(sumWageEmployerJob: sumWageEmployerJob.getSumWageEmployerJobFromDic(dic: dicResult))
                    } } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
          //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetSumWageEmployerJob")
    }
    
    func createDicToListEmployerJobTimes(employerJobTimes : Array<EmployerJobTime>)-> Array<Dictionary<String, AnyObject>>
    {
//        var i : String = "0"
//        var dic = Dictionary<String, AnyObject>()
        var dicArr : Array<Dictionary<String, AnyObject>> = []

        for item in employerJobTimes {
            dicArr.append(item.getDicFromEmployerJobTime())
            
        }
        
        
        return dicArr
    }
    
}
