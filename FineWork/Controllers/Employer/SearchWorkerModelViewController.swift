//
//  SearchWorkerModelViewController.swift
//  FineWork
//
//  Created by User on 15/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SearchWorkerModelViewController: UIViewController, OpenAnotherPageDelegate, UIScrollViewDelegate, ChangeScrollViewHeightDelegate, getTextFieldDelegate {

    //MARK: - Views
    @IBOutlet var barContainerView: UIView!
    @IBOutlet var menuView: UIView!
    @IBOutlet var whatBtn: UIButton!
    @IBOutlet var whenBtn: UIButton!
    @IBOutlet var howManyBtn: UIButton!
    @IBOutlet var whereBtn: UIButton!
    @IBOutlet var jobDescriptionBtn: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    
    
    //MARK: - Actions
    @IBAction func menuBtnAction(_ sender: UIButton) {
        if sender.backgroundColor == UIColor.myDarkPurple {
            setBtnSelectedByTag(tag: sender.tag)
        }
    }
    
    
    //MARK: - Variables
    var searchNewWorker_whatVC = SearchNewWorker_WhatViewController()
    var searchNewWorker_whenVC = SearchNewWorker_WhenViewController()
    var searchNewWorker_howManyVC = SearchNewWorker_HowManyViewController()
    var searchNewWorker_whereVC = SearchNewWorker_WhereViewController()
    var searchNewWorker_jobDescriptionVC = SearchNewWorker_JobDescriptionViewController()
    var currentTab = 0
    var employerJob = EmployerJob()
    var params = Array<GlobalParameters>()
    var isNewWorker : Bool = true
    var isCopyJob : Bool = false
    var numberJob : Int =  0
    var numberPhone : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setGeneralDesign()
        
        // Do any additional setup after loading the view.
//        jobPreferencesVC.view.frame.origin.y = menuView.frame.height
        let story = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        searchNewWorker_whatVC = story.instantiateViewController(withIdentifier: "SearchNewWorker_WhatViewController") as! SearchNewWorker_WhatViewController
//        searchNewWorker_whatVC.view.frame.origin.y = menuView.frame.height + barContainerView.frame.height
        
        
//        self.scrollView.addSubview(searchNewWorker_whatVC.view)
//        getSearchNewWorker_whatForm()
        
        
        
        searchNewWorker_whenVC = story.instantiateViewController(withIdentifier: "SearchNewWorker_WhenViewController") as! SearchNewWorker_WhenViewController
//        self.scrollView.addSubview(searchNewWorker_whenVC.view)
        
        searchNewWorker_howManyVC = story.instantiateViewController(withIdentifier: "SearchNewWorker_HowManyViewController") as! SearchNewWorker_HowManyViewController
        
        searchNewWorker_whereVC = story.instantiateViewController(withIdentifier: "SearchNewWorker_WhereViewController") as! SearchNewWorker_WhereViewController
        
        searchNewWorker_jobDescriptionVC = story.instantiateViewController(withIdentifier: "SearchNewWorker_JobDescriptionViewController") as! SearchNewWorker_JobDescriptionViewController
        
        
        whatBtn.tag = 0
        whenBtn.tag = 1
        howManyBtn.tag = 2
        whereBtn.tag = 3
        jobDescriptionBtn.tag = 4
        
        
        if(numberJob > 0){
            getEmployeJob()
        }
    }
    func sendData(isNewWorker : Bool , idJob : Int , isCopyJob : Bool){
        self.isNewWorker = isNewWorker
        numberJob = idJob
        self.isCopyJob = isCopyJob
    }
  
    func sendData(isNewWorker : Bool , idJob : Int , isCopyJob : Bool , number : String){
        self.isNewWorker = isNewWorker
        numberJob = idJob
        self.isCopyJob = isCopyJob
        self.numberPhone = number
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setBtnSelectedByTag(tag: 0)
//        getGlobalParameters()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
//        barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "חיפוש עובד חדש"
        
    }
    
    //MARK: - General Design
    func setGeneralDesign() {
        menuView.backgroundColor = UIColor.bluelight
        barContainerView.backgroundColor = UIColor.blue2

//        setBackgroundcolor()
        setBorders()
        setTextcolor()
        setCornerRadius()
        setIconFont()
        
        hideKeyboardWhenTappedAround()
    }
    
    func setBorders(){
        setBorderToBtn(btn: whatBtn, color: UIColor.white, width: 0.5)
        setBorderToBtn(btn: whenBtn, color: UIColor.white, width: 0.5)
        setBorderToBtn(btn: whereBtn, color: UIColor.white, width: 0.5)
        setBorderToBtn(btn: howManyBtn, color: UIColor.white, width: 0.5)
        setBorderToBtn(btn: jobDescriptionBtn, color: UIColor.white, width: 0.5)
    }
    
    func setTextcolor(){
        setTextcolorToBtn(btn: whatBtn, color: UIColor.white)
        setTextcolorToBtn(btn: whenBtn, color: UIColor.white)
        setTextcolorToBtn(btn: whereBtn, color: UIColor.white)
        setTextcolorToBtn(btn: howManyBtn, color: UIColor.white)
        setTextcolorToBtn(btn: jobDescriptionBtn, color: UIColor.white)
    }
    
    func setCornerRadius(){
        self.configureButton(btn: whatBtn)
        self.configureButton(btn: whenBtn)
        self.configureButton(btn: whereBtn)
        self.configureButton(btn: howManyBtn)
        self.configureButton(btn: jobDescriptionBtn)
    }
    
    func setIconFont(){
        
        // employer
        jobDescriptionBtn.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_DetaisCard, for: .normal)//people-2
        //whenBtn.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)//people-2
    }
    
    func setBackgroundcolorToBtn(btn:UIButton, color:UIColor){
        btn.backgroundColor = color
    }
    
    func setTextcolorToBtn(btn:UIButton, color:UIColor)
    {
        btn.titleLabel?.textColor = color
        btn.tintColor = color
    }
    func setBorderToBtn(btn:UIButton, color:UIColor , width:CGFloat)
    {
        btn.layer.borderColor = color.cgColor
        btn.layer.borderWidth = width
    }
    
    func setFontToButton(btn:UIButton, name:String, size:CGFloat)
    {
        btn.titleLabel!.font =  UIFont(name: name, size: size)
        //btn.titleLabel?.backgroundColor = UIColor.clear
    }
    
    func configureButton(btn:UIButton){
        let f : CGFloat = btn.frame.width / 2
        btn.layer.cornerRadius = f
    }
    
    func setBtnSelectedByTag(tag: Int) {
        removeViews()
        currentTab = tag
        scrollView.contentOffset = .zero
        switch tag {
        case 0:
            setBackgroundcolorToBtn(btn: whatBtn, color: UIColor.blue2)
            setBorders()
            setBorderToBtn(btn: whatBtn, color: UIColor.bluelightlight, width: 2.5)
            getSearchNewWorker_whatForm()
            break
        case 1:
            setBackgroundcolorToBtn(btn: whenBtn, color: UIColor.blue2)
            setBorders()
            setBorderToBtn(btn: whenBtn, color: UIColor.bluelightlight, width: 2.5)
            getSearchNewWorker_whenForm()
            break
        case 2:
            setBackgroundcolorToBtn(btn: howManyBtn, color: UIColor.blue2)
            setBorders()
            setBorderToBtn(btn: howManyBtn, color: UIColor.bluelightlight, width: 2.5)
            getSearchNewWorker_howManyForm()
            break
        case 3:
            setBackgroundcolorToBtn(btn: whereBtn, color: UIColor.blue2)
            setBorders()
            setBorderToBtn(btn: whereBtn, color: UIColor.bluelightlight, width: 2.5)
            getSearchNewWorker_whereForm()
            break
        case 4:
            setBackgroundcolorToBtn(btn: jobDescriptionBtn, color: UIColor.blue2)
            setBorders()
            setBorderToBtn(btn: jobDescriptionBtn, color: UIColor.bluelightlight, width: 2.5)
            getSearchNewWorker_jobDescriptionForm()
            break
        default:
            break
        }
    }


    func openNextTab(tag: Int) {
        switch tag-1 {
        case 0:
            setDataFromWhatVC()
            break
        case 1:
            setDataFromWhenVC()
            
            break
        case 2:
            setDataFromHowManyVC()
            break
        case 3:
            setDataFromWhereVC()
            break
        case 4:
            setDataFromJobDescriptionVCAndSave()
            break
        default:
            break
        }
        
        if tag-1 != 4 {
            setBtnSelectedByTag(tag: tag)
        }
    }
    
    //MARK: - screens (tabs)
    func getSearchNewWorker_whatForm() {
        scrollView.addSubview(searchNewWorker_whatVC.view)
        setHeightToScroll(height: searchNewWorker_whatVC.view.frame.height)
        searchNewWorker_whatVC.changeScrollViewDelegate = self
        searchNewWorker_whatVC.openAnotherPageDelegate = self
        addChildViewController(searchNewWorker_whatVC)
        //jobPreferencesVC.scrollView = self.scrollView
//        jobPreferencesVC.changeScrollViewHeightDelegate = self
//        jobPreferencesVC.getTextFieldDelegate = self
//        jobPreferencesVC.openAnotherPageDelegate = self
        
//        registerKeyboardNotifications()
    }
    
    func getSearchNewWorker_whenForm() {
        setScrollViewEnable(isEnable: true)
        scrollView.addSubview(searchNewWorker_whenVC.view)
        setHeightToScroll(height: /*searchNewWorker_whenVC.view.frame.height*/667)
        searchNewWorker_whenVC.changeScrollViewDelegate = self
        searchNewWorker_whenVC.openAnotherPageDelegate = self
        addChildViewController(searchNewWorker_whenVC)

        searchNewWorker_whenVC.resetData(employerJobPeriodType: employerJob.iEmployerJobPeriodType, employerJobScheduleType: employerJob.iEmployerJobScheduleType)
        searchNewWorker_whenVC.globalParams = self.params
        //jobPreferencesVC.scrollView = self.scrollView
        //        jobPreferencesVC.changeScrollViewHeightDelegate = self
        //        jobPreferencesVC.getTextFieldDelegate = self
        //        jobPreferencesVC.openAnotherPageDelegate = self
        
        //        registerKeyboardNotifications()
    }
    
    func getSearchNewWorker_howManyForm() {
        setScrollViewEnable(isEnable: false)
        scrollView.addSubview(searchNewWorker_howManyVC.view)
        setHeightToScroll(height: /*searchNewWorker_whenVC.view.frame.height*/667)
        searchNewWorker_howManyVC.openAnotherPageDelegate = self
        addChildViewController(searchNewWorker_howManyVC)

        var hours : Double = 0
        
        for item in employerJob.lEmployerJobTimes {
            var arr = item.tFromHoure.characters.split(separator: ":")
            let timeInMinutesFrom = (Int(String(arr[0]))! * 60) + Int(String(arr[1]))!
            arr = item.tToHoure.characters.split(separator: ":")
            let timeInMinutesTo = (Int(String(arr[0]))! * 60) + Int(String(arr[1]))!
            
            hours += Double(timeInMinutesTo - timeInMinutesFrom) / 60.0
        }
        
        searchNewWorker_howManyVC.setTotalSalary(employerJobPeriodType: employerJob.iEmployerJobPeriodType,hoursNumber: hours , iEmployerJobScheduleType:employerJob.iEmployerJobScheduleType.rawValue, lEmployerJobTimes: employerJob.lEmployerJobTimes )
        
        
    }
    
    func getSearchNewWorker_whereForm() {
        setScrollViewEnable(isEnable: false)
        scrollView.addSubview(searchNewWorker_whereVC.view)
        setHeightToScroll(height: /*searchNewWorker_whenVC.view.frame.height*/667)
        searchNewWorker_whereVC.openAnotherPageDelegate = self
        addChildViewController(searchNewWorker_whereVC)

    }
    
    func getSearchNewWorker_jobDescriptionForm() {
        registerKeyboardNotifications()
        setScrollViewEnable(isEnable: true)
        scrollView.addSubview(searchNewWorker_jobDescriptionVC.view)
        setHeightToScroll(height: /*searchNewWorker_whenVC.view.frame.height*/667 * 0.85)
        searchNewWorker_jobDescriptionVC.changeScrollViewDelegate = self
        searchNewWorker_jobDescriptionVC.getTextFieldDelegate = self
        searchNewWorker_jobDescriptionVC.openAnotherPageDelegate = self
        addChildViewController(searchNewWorker_jobDescriptionVC)

    }
    
    func removeViews() {
        unregisterKeyboardNotifications()
        // -- remove pages
        searchNewWorker_whenVC.view.removeFromSuperview()
        searchNewWorker_whatVC.view.removeFromSuperview()
        searchNewWorker_howManyVC.view.removeFromSuperview()
        searchNewWorker_whereVC.view.removeFromSuperview()
        searchNewWorker_jobDescriptionVC.view.removeFromSuperview()
    }

    
    //MARK: - scroll view delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < CGPoint.zero.y {
            scrollView.contentOffset = CGPoint.zero
        }
        
    }
    
    func changeScrollViewHeight(heightToadd : CGFloat) {
        //   scrollView.contentSize = CGSize(width: scrollView.frame.width, height: scrollView.contentSize.height + heightToadd)
        setScrollViewEnable(isEnable: true)
        scrollView.contentSize.height += heightToadd
        switch currentTab {
        case 0:
            searchNewWorker_whatVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: searchNewWorker_whatVC.titleLbl, toItem: searchNewWorker_whatVC.view)
            break
        case 1:
            searchNewWorker_whenVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: searchNewWorker_whenVC.titleLbl, toItem: searchNewWorker_whenVC.view)
            break
        case 2:
            break
        case 3:
            break
        case 4:
            searchNewWorker_jobDescriptionVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: searchNewWorker_jobDescriptionVC.titleLbl, toItem: searchNewWorker_jobDescriptionVC.view)
            break
        default:
            break
        }
    }
    
    func setScrollViewEnable(isEnable: Bool) {
        scrollView.isScrollEnabled = isEnable
    }
    
    func scrollTop() {
        scrollView.contentOffset = .zero
    }
    
    func setHeightToScroll(height : CGFloat) {
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: menuView.frame.height + height)
        
        //        scrollHeightCons.constant = menuView.frame.height + height
        
        //viewUnderScroll.frame = CGRect(x: 0, y: 0, width: viewUnderScroll.frame.width , height: scrollView.contentSize.height)
    }
    
    func setConstrainToTitle(item : Any, toItem : Any?) {
        let horizontalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 10)
        let widthConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.width, multiplier: 0.9, constant: 100)
        
        (toItem as! UIView).addConstraints([horizontalConstraint, verticalConstraint, widthConstraint])
    }
    
    //MARK: - KeyBoard Function
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    //MARK: - keyBoard notifications
    var txtSelected = UITextField()
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShowTemp(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    var scrollHeight : CGFloat = 0
    
    func keyboardDidShowTemp(notification : NSNotification) {
        var aRect: CGRect = view.frame
        if txtSelected.tag != TextFieldType.OpenListUnder.rawValue {
            var info: [AnyHashable: Any]? = notification.userInfo
            let kbSize: CGSize? = (info?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
            let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (kbSize?.height)!, 0.0)
            scrollView.contentInset = contentInsets!
            scrollView.scrollIndicatorInsets = contentInsets!
            
            // If active text field is hidden by keyboard, scroll it so it's visible
            // Your application might not need or want this behavior.
            
            aRect.size.height -= (kbSize?.height)!
        }
        //brect
        var bRect: CGRect = txtSelected.frame
        
        if txtSelected.tag == TextFieldType.OnView.rawValue || txtSelected.tag == TextFieldType.OpenListUnder.rawValue {
            bRect.origin.y += (txtSelected.superview?.frame.origin.y)!
        }
        
        if txtSelected.tag == TextFieldType.OpenListUnder.rawValue {
            bRect.origin.y += 100
            let scrollPoint = CGPoint(x: CGFloat(0.0), y: bRect.origin.y)
            scrollView.setContentOffset(scrollPoint, animated: true)
        }
        else
            if !aRect.contains(bRect.origin) {
                let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y))
                //            let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y - (kbSize?.height)!))
                scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func getTextField(textField: UITextField, originY: CGFloat) {
        self.txtSelected = textField
    }
    


  //MARK: - Set Data From Screens
    
    
    
    func setDataFromWhatVC() {
        if !isNewWorker {
            employerJob.iEmployerJobWorkerType = EmployerJobWorkerType.KnownWorker.rawValue
            employerJob.nvKnownWorkerPhone = searchNewWorker_whatVC.whoPhone.text!
        }else{
            employerJob.iEmployerJobWorkerType = EmployerJobWorkerType.NewWorker.rawValue

        employerJob.iEmploymentDomainRoleId = searchNewWorker_whatVC.domainRoleSelectedId
        employerJob.iJobQuantity = Int(searchNewWorker_whatVC.workersNumberTxt.text!)!
            employerJob.lPopulationType = []
        if searchNewWorker_whatVC.matchToGeneralBtn.backgroundColor == UIColor.myTurquoise {
            let populationType = SimpleObj<Int>()
            populationType.simpleProp = PopulationType.General.rawValue
            employerJob.lPopulationType.append(populationType)
        }
        
        if searchNewWorker_whatVC.matchToYouthBtn.backgroundColor == UIColor.myTurquoise {
            let populationType = SimpleObj<Int>()
            populationType.simpleProp = PopulationType.Youth.rawValue
            employerJob.lPopulationType.append(populationType)
        }
        
        if searchNewWorker_whatVC.matchToRetiredBtn.backgroundColor == UIColor.myTurquoise {
            let populationType = SimpleObj<Int>()
            populationType.simpleProp = PopulationType.Retired.rawValue
            employerJob.lPopulationType.append(populationType)
        }
        
        if searchNewWorker_whatVC.matchToForeignWorkersBtn.backgroundColor == UIColor.myTurquoise {
            let populationType = SimpleObj<Int>()
            populationType.simpleProp = PopulationType.ForeignWorkers.rawValue
            employerJob.lPopulationType.append(populationType)
        }
    }
        employerJob.iEmployerJobPeriodType = searchNewWorker_whatVC.stateWorkerSeg.selectedSegmentIndex == 0 ? EmployerJobPeriodType.ConstantWorker : EmployerJobPeriodType.TemporaryWorker
        employerJob.employerJobNewWorker.bWorkedWorkerPreference = searchNewWorker_whatVC.oldWorkerCheckBox.isChecked
    }
    
    func setDataFromWhenVC() {
        if searchNewWorker_whenVC.oneTimeJobRdb.isSelected {
            employerJob.iEmployerJobScheduleType = EmployerJobScheduleType.OneTimeJob
            
        } else if searchNewWorker_whenVC.constantHoursJobRdb.isSelected {
            employerJob.iEmployerJobScheduleType = EmployerJobScheduleType.ConstantHoursJob
        } else {
            employerJob.iEmployerJobScheduleType = EmployerJobScheduleType.ChangingHoursJob
        }
        employerJob.lEmployerJobTimes = []
        employerJob.lEmployerJobTimes = searchNewWorker_whenVC.lEmployerJobTimes
    }
    
    func setDataFromHowManyVC() {
        employerJob.mHourlyWage = Double(searchNewWorker_howManyVC.hourlySalaryTxt.text!)!
        employerJob.bBonusOption = searchNewWorker_howManyVC.bonusOptionBtn.isChecked
    }
    
    func setDataFromWhereVC() {
        if searchNewWorker_whereVC.myAddressRdb.isSelected {
            employerJob.iWorkingLocationType = WorkingLocationType.myAddress
        } else {
            employerJob.iWorkingLocationType = WorkingLocationType.anotherAddress
            employerJob.nvGoogleAddress = searchNewWorker_whereVC.anotherAddressLbl.text!
//            employerJob.nvLat
//            employerJob.nvLng
        }
    }
    
    func setDataFromJobDescriptionVCAndSave() {
        employerJob.nvJobDescription = searchNewWorker_jobDescriptionVC.jobDescriptionTxt.text!
        if searchNewWorker_jobDescriptionVC.yesRisksRdb.isSelected {
            employerJob.bRisk = true
            employerJob.nvRiskDetails = searchNewWorker_jobDescriptionVC.riskDetailsTxt.text!
        } else {
            employerJob.bRisk = false
            employerJob.nvRiskDetails = ""
        }
        
        if searchNewWorker_jobDescriptionVC.yesSafetyReqsRdb.isSelected {
            employerJob.bSafetyRequirement = true
            employerJob.nvSafetyRequirementDetails = searchNewWorker_jobDescriptionVC.safetyReqsDetailsTxt.text!
        } else {
            employerJob.bSafetyRequirement = false
            employerJob.nvSafetyRequirementDetails = ""
        }
        
        employerJob.iPhysicalDifficultyRank = Int(searchNewWorker_jobDescriptionVC.physicalDifficultySlider.selectedMaximum)
        employerJob.nvWorkSpaceDetails = searchNewWorker_jobDescriptionVC.workSpaceTxt.text!
        
        if searchNewWorker_jobDescriptionVC.experienceBtn.isChecked {
            employerJob.employerJobNewWorker.nvExperienceDetails = searchNewWorker_jobDescriptionVC.experienceTxt.text!
        } else {
            employerJob.employerJobNewWorker.nvExperienceDetails = ""
        }
        
        if searchNewWorker_jobDescriptionVC.languageBtn.isChecked {
            employerJob.employerJobNewWorker.nvLanguageDetails = searchNewWorker_jobDescriptionVC.languageTxt.text!
        } else {
            employerJob.employerJobNewWorker.nvLanguageDetails = ""
        }
        
        if searchNewWorker_jobDescriptionVC.seniorityBtn.isChecked {
            employerJob.employerJobNewWorker.nvTimeDescription = searchNewWorker_jobDescriptionVC.seniorityTxt.text!
        } else {
            employerJob.employerJobNewWorker.nvTimeDescription = ""
        }
        
        if searchNewWorker_jobDescriptionVC.professionalSeniorityBtn.isChecked {
            employerJob.employerJobNewWorker.nvProfessionalTimeDetails = searchNewWorker_jobDescriptionVC.professionalSeniorityTxt.text!
        } else {
            employerJob.employerJobNewWorker.nvProfessionalTimeDetails = ""
        }
        
        employerJob.employerJobNewWorker.bDiploma = searchNewWorker_jobDescriptionVC.diplomaBtn.isChecked
        
        employerJob.employerJobNewWorker.bPhoneInterview = searchNewWorker_jobDescriptionVC.phoneInterviewBtn.isChecked
        
        // --- save ---
        updateEmployerJob()
        
    }
    
    //MARK: - Server Functions
    
    func getEmployeJob(){
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        employerJob.iEmployerId = Global.sharedInstance.user.iEmployerId
        dic["iEmployerJobId"] = numberJob as AnyObject?
        print(dic as Any)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            //            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject>  {
                        
                        self.employerJob = self.employerJob.getEmployerJobFromDic(dic: dicResult  )
                        self.getGlobalParameters()
                        
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetEmployerJob)
    }

    
    
    func updateEmployerJob() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        employerJob.iEmployerId = Global.sharedInstance.user.iEmployerId
        dic["employerJob"] = employerJob.getDicFromEmployerJob() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject?
        Global.sharedInstance.employer.employerPaymentMethod.bBankAccountDetailsUpdated = false
        print(dic as Any)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            //            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Int {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        if dicResult > 0 {
                           // Alert.sharedInstance.showAlert(mess: "נשמר בהצלחה!")
                            self.openLookingForYou(id : dicResult)
                        } else {
                            Alert.sharedInstance.showAlert(mess: "ארעה בעיה בשמירת הפרטים!")
                        }
                        //                        self.openFirstPage(isEmployer: true)
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateEmployerJob)
    }

    func getGlobalParameters() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        employerJob.iEmployerId = Global.sharedInstance.user.iEmployerId
      //  dic["employerJob"] = employerJob.getDicFromEmployerJob() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        Global.sharedInstance.employer.employerPaymentMethod.bBankAccountDetailsUpdated = false
        print(dic as Any)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            //            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        
//                        self.params = dicResult
                        self.params.removeAll()
                        let param = GlobalParameters()
                        for item in dicResult {
                            self.params.append(param.getGlobalParametersFromDic(dic: item))
                        }
                        
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetGlobalParameters)
    }
    
    func openLookingForYou( id : Int){
        if #available(iOS 11.0, *) {
            
            let workerStory = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
            let vc = workerStory.instantiateViewController(withIdentifier: "LookingForYouModelViewController") as! LookingForYouModelViewController
            vc.sendData(mJobID: 0 ,mEmployerJobId:id, mStatusJob: "" , mRole: searchNewWorker_whatVC.chooseRoleLbl.text! , mOpenCandidateDetailsFromList: false)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // Fallback on earlier versions
        }
}
}
