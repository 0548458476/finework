//
//  CompanyVisibleProfileViewController.swift
//  FineWork
//
//  Created by User on 19.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class CompanyVisibleProfileViewController: UIViewController, UploadFilesDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    // Views
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var uploadLogoBtn: UIButton!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet var uploadBannerBtn: UIButton!
    @IBOutlet var bannerImg: UIImageView!
    @IBOutlet weak var descriptionTxt: UITextField!
    @IBOutlet weak var companyNameTxt: UITextField!
    @IBOutlet weak var workersNumberTbl: UITableView!
    @IBOutlet weak var activityDomainsTbl: UITableView!
    
    @IBOutlet weak var workersNumberLbl: UILabel!
    @IBOutlet weak var activityDomainLbl: UILabel!
    
    @IBOutlet weak var mainAreaToActivityTxt: UITextField!
    @IBOutlet weak var companySiteLinkTxt: UITextField!
    @IBOutlet weak var ratingBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var arrowLbl: UILabel!
    @IBOutlet weak var arrow2Lbl: UILabel!
    // Actions
    @IBAction func uploadLogoBtnAction(_ sender: Any) {
        imageTappedLogo()
    }
    
    @IBAction func uploadBannerBtnAction(_ sender: Any) {
        imageTappedBanner()
    }
    
    @IBAction func rankingActionBtn(_ sender: Any) {
        let story =  UIStoryboard(name: "StoryboardGeneral", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "RankViewController") as! RankViewController
      //  saveEmployerDelegate.
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
       @IBAction func saveBtnAction(_ sender: Any) {
        if validateFields() {
            saveData()
        } else {
            // error
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים/אינם תקינים")
        }
    }
    
    //MARK: - Variables
    var uploadFilesDelegates : UploadFilesDelegate! = nil
    var saveEmployerDelegate : SaveEmployerDelegate! = nil
    var getTextFieldDelegate : getTextFieldDelegate! = nil
//    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    var imageString = "", imageName = ""  // logo
    var bannerImageString = "", bannerImageName = ""  // banner
    var workersNumberArr = Array<SysTable>()
    var activityDomainsArr = Array<SysTable>()
    var workerNumbersSelectedId = -1
    var activityDomainSelectedId = -1
    
    var isUploadLogo : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.imageTappedLogo))
        logoImg.isUserInteractionEnabled = true
        logoImg.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizerBanner = UITapGestureRecognizer(target:self, action:#selector(self.imageTappedBanner))
        bannerImg.isUserInteractionEnabled = true
        bannerImg.addGestureRecognizer(tapGestureRecognizerBanner)
        
        let workerNumbersTblTap = UITapGestureRecognizer(target:self, action:#selector(self.setWorkersNumberTblState))
        workersNumberLbl.addGestureRecognizer(workerNumbersTblTap)
        
        let activityDomainsTblTap = UITapGestureRecognizer(target:self, action:#selector(self.setActivityDomainsTblState))
        activityDomainLbl.addGestureRecognizer(activityDomainsTblTap)
        
        Global.sharedInstance.setButtonCircle(sender: uploadLogoBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: uploadBannerBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: ratingBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: saveBtn, isBorder: true)
        
        descriptionTxt.keyboardType = .default
        companyNameTxt.keyboardType = .default
        mainAreaToActivityTxt.keyboardType = .default
        companySiteLinkTxt.keyboardType = .asciiCapable
        
        descriptionTxt.returnKeyType = .next
        companyNameTxt.returnKeyType = .next
        mainAreaToActivityTxt.returnKeyType = .next
        companySiteLinkTxt.returnKeyType = .done
        
        descriptionTxt.delegate = self
        companyNameTxt.delegate = self
        mainAreaToActivityTxt.delegate = self
        companySiteLinkTxt.delegate = self
        
        arrowLbl.text = FontAwesome.sharedInstance.arrow
                arrow2Lbl.text = FontAwesome.sharedInstance.arrow
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !Global.sharedInstance.bIsOpenFiles {
            // logo
            if Global.sharedInstance.employer.logoImage.nvFileURL == "" {
                uploadLogoBtn.isHidden = false
                logoImg.isHidden = true
            } else {
                uploadLogoBtn.isHidden = true
                logoImg.isHidden = false
                setImage(fileUrl: Global.sharedInstance.employer.logoImage.nvFileURL, image: logoImg)
            }
            imageString = ""
            imageName = ""
            
            // banner
            if Global.sharedInstance.employer.banner.nvFileURL == "" {
                uploadBannerBtn.isHidden = false
                bannerImg.isHidden = true
            } else {
                uploadBannerBtn.isHidden = true
                bannerImg.isHidden = false
                setImage(fileUrl: Global.sharedInstance.employer.banner.nvFileURL, image: bannerImg)
            }
            bannerImageString = ""
            bannerImageName = ""
            let s : SysTable = SysTable()
            s.iId = 0
            s.nvName = ""
            for item in Global.sharedInstance.employerCodeTables {
                if item.Key == Global.sharedInstance.EmployeeNumberTypeCodeTable {
                    workersNumberArr.removeAll()
                    workersNumberArr = item.Value
                    workersNumberArr.insert(s, at: 0)
                    workersNumberTbl.reloadData()
                } else {
                    activityDomainsArr.removeAll()
                    activityDomainsArr = item.Value
                    activityDomainsArr.insert(s, at: 0)
                    activityDomainsTbl.reloadData()
                }
            }
            
            setData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        setConstrainToTitle(item: titleLbl, toItem: self.view)
    }
    
    func setConstrainToTitle(item : Any, toItem : Any?) {
        let horizontalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 10)
        let widthConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.width, multiplier: 0.9, constant: 100)
        
        (toItem as! UIView).addConstraints([horizontalConstraint, verticalConstraint, widthConstraint])
    }
    
    func imageTappedLogo()
    {
        isUploadLogo = true
        uploadFilesDelegates.uploadFile!(fileUrl: "")
    }
    
    func imageTappedBanner()
    {
        isUploadLogo = false
        uploadFilesDelegates.uploadFile!(fileUrl: "")
    }
    
    func setImage(fileUrl: String, image: UIImageView) {
//        DispatchQueue.main.async {
            //        let fileUrl = Global.sharedInstance.employer.logoImage.nvFileURL
//            let url : NSString = api.sharedInstance.buldUrlFile(fileName: fileUrl) as NSString
//            let urlStr : NSString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! as String as NSString//url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
//            
//            if let url = NSURL(string: urlStr as String) {
//                if let data = NSData(contentsOf: url as URL) {
//                    DispatchQueue.main.async {
//                        
//                    image.contentMode = .scaleToFill
//                    /*logoImg.*/image.image = UIImage(data: data as Data)
//                        }
//                    //                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
//                } else {
//                    //                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
//                }
//            } else {
//                //            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
//            }
//        }
        var stringPath = api.sharedInstance.buldUrlFile(fileName: fileUrl)
        stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
        if let imageUrl = URL(string:stringPath) {
        
            Global.sharedInstance.getDataFromUrl(url: imageUrl) { (data, response, error)  in
                guard let data = data, error == nil else { return }
                if let imageShow = UIImage(data: data){
                    DispatchQueue.main.async {
                        image.contentMode = .scaleToFill
                        image.image = imageShow
                    }
                    
                }
            }
        }

    }
    

    // delegates
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        // set logo image
        if displayImg != nil {
            if isUploadLogo {
                uploadLogoBtn.isHidden = true
                logoImg.isHidden = false
                logoImg.image = displayImg
                imageString = img
                self.imageName = imgName
            } else {
                uploadBannerBtn.isHidden = true
                bannerImg.isHidden = false
                bannerImg.image = displayImg
                bannerImageString = img
                self.bannerImageName = imgName
            }
            
            Global.sharedInstance.bWereThereAnyChanges = true
        }
    }
    
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numOfRowsInSection = 0
        
        if tableView == workersNumberTbl {
            numOfRowsInSection = workersNumberArr.count
        } else {
            numOfRowsInSection = activityDomainsArr.count
        }
        
        return numOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell
        
        //numOfRowsInSection = placesArr.count
        cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell") as! FamilyStatusTableViewCell
        if tableView == workersNumberTbl {
            (cell as! FamilyStatusTableViewCell).lblStatus.text = workersNumberArr[indexPath.row].nvName
        } else {
            (cell as! FamilyStatusTableViewCell).lblStatus.text = activityDomainsArr[indexPath.row].nvName
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == workersNumberTbl {
            workersNumberLbl.text = workersNumberArr[indexPath.row].nvName
            workerNumbersSelectedId = workersNumberArr[indexPath.row].iId
        } else {
            activityDomainLbl.text = activityDomainsArr[indexPath.row].nvName
            activityDomainSelectedId = activityDomainsArr[indexPath.row].iId
        }
        workersNumberTbl.isHidden = true
        activityDomainsTbl.isHidden = true
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
    }
    
    func setWorkersNumberTblState() {
        workersNumberTbl.isHidden = !workersNumberTbl.isHidden
        activityDomainsTbl.isHidden = true
    }
    
    func setActivityDomainsTblState() {
        activityDomainsTbl.isHidden = !activityDomainsTbl.isHidden
        workersNumberTbl.isHidden = true
    }
    
    //MARK: - Text Field Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        getTextFieldDelegate.getTextField(textField: textField, originY: 0)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case descriptionTxt:
            companyNameTxt.becomeFirstResponder()
            break
        case companyNameTxt:
            mainAreaToActivityTxt.becomeFirstResponder()
            break
        case mainAreaToActivityTxt:
            companySiteLinkTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case companyNameTxt:
            limitLength = 50
            break
        case descriptionTxt:
            limitLength = 200
            break
        case mainAreaToActivityTxt:
            limitLength = 100
            break
        case companySiteLinkTxt:
            limitLength = 100
            break
        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
        
    }
    
    func validateFields() -> Bool {
        var isValid = true
        if companyNameTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: companyNameTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: companyNameTxt)
        }
        
        return isValid
    }
    
    func saveData() {
        Global.sharedInstance.employer.nvEmployerName = companyNameTxt.text!

            Global.sharedInstance.employer.nvBusinessDescription = descriptionTxt.text!

            Global.sharedInstance.employer.nvCentralAreaActivity = mainAreaToActivityTxt.text!
        
            Global.sharedInstance.employer.nvSiteLink = companySiteLinkTxt.text!
        
        if workerNumbersSelectedId != -1 {
            Global.sharedInstance.employer.iEmployeeNumberType = workerNumbersSelectedId
        }
        if activityDomainSelectedId != -1 {
            Global.sharedInstance.employer.iMainActivityFieldType = activityDomainSelectedId
        }
        
        if imageString != "" {
            Global.sharedInstance.employer.logoImage.nvFileURL = ""
            Global.sharedInstance.employer.logoImage.nvFile = imageString
            Global.sharedInstance.employer.logoImage.nvFileName = imageName
        }
        
        if bannerImageString != "" {
            Global.sharedInstance.employer.banner.nvFileURL = ""
            Global.sharedInstance.employer.banner.nvFile = bannerImageString
            Global.sharedInstance.employer.banner.nvFileName = bannerImageName
        }
        
        saveEmployerDelegate.saveEmployer!(goNextTab: 2)
    }

    func setData() {
        companyNameTxt.text = Global.sharedInstance.employer.nvEmployerName
        
        descriptionTxt.text = Global.sharedInstance.employer.nvBusinessDescription
        
        mainAreaToActivityTxt.text = Global.sharedInstance.employer.nvCentralAreaActivity
        
        companySiteLinkTxt.text = Global.sharedInstance.employer.nvSiteLink
        
        for item in workersNumberArr {
            if item.iId == Global.sharedInstance.employer.iEmployeeNumberType {
                workersNumberLbl.text = item.nvName
                break
            }
        }
        for item in activityDomainsArr {
            if item.iId == Global.sharedInstance.employer.iMainActivityFieldType {
                activityDomainLbl.text = item.nvName
                break
            }
        }
        
    }

}
