//
//  LookingForYouStartViewController.swift
//  FineWork
//
//  Created by User on 03/11/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class LookingForYouStartViewController: UIViewController {

    //MARK: - Views

    @IBOutlet weak var animStatus: UIImageView!
    @IBOutlet weak var lookingTitle: UILabel!
    @IBOutlet weak var lookingTxt: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UILabel!
    @IBOutlet weak var showDetails: UILabel!
    
        //MARK: - Action
    @IBAction func editJobAction(_ sender: Any) {
        
    }
        //MARK: - Variables
    //delegattes
    var changeScrollViewDelegate : ChangeScrollViewHeightDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        initDisplay()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func initDisplay(){
        btnDelete.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
        btnDelete.layer.borderWidth = 1
        btnDelete.layer.cornerRadius = btnDelete.frame.height / 2
        
        Global.sharedInstance.setButtonCircle(sender: btnEdit, isBorder: false)
        animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "search-small").image

        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(deleteJob))
        btnDelete.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(showDetailsJobAction))
        showDetails.addGestureRecognizer(tapGestureRecognizer2)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
//MARK: - Taps
    
    func deleteJob(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.deleteEmployerJobShowAlert()
        } else {
            // Fallback on earlier versions
        }

    }
    
    
    func showDetailsJobAction(){
        if #available(iOS 11.0, *) {
            
            (parent as? LookingForYouModelViewController)?.getDetailsJob()
            
        } else {
            
            // Fallback on earlier versions
            
        }
    }
}
