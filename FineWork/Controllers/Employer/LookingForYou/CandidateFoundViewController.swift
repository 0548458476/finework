//
//  CandidateFoundViewController.swift
//  FineWork
//
//  Created by User on 06/11/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)


class CandidateFoundViewController: UIViewController {
    
    @IBOutlet var viewBase: UIView!
    //MARK: - Views
    @IBOutlet weak var animStatus: UIImageView!
    @IBOutlet weak var titleStatus: UILabel!
    @IBOutlet weak var txtStstus: UILabel!
    
    @IBOutlet weak var viewCandidateScema: UIView!
    @IBOutlet weak var viewCandidate1: UIView!
    @IBOutlet weak var viewCandidate2: UIView!
    @IBOutlet weak var viewCandidate3: UIView!

    @IBOutlet weak var scema1: UILabel!
    @IBOutlet weak var scema2: UILabel!
    @IBOutlet weak var scema3: UILabel!
    
    @IBOutlet weak var viewImgCandidate3: UIView!
    @IBOutlet weak var imgCandideate1: UIImageView!
    @IBOutlet weak var imgCandideate2: UIImageView!
    @IBOutlet weak var imgCandideate3: UIImageView!
    
    @IBOutlet weak var matchPercentages1: UILabel!
    @IBOutlet weak var matchPercentages2: UILabel!
    @IBOutlet weak var matchPercentages3: UILabel!
    
    @IBOutlet weak var amountCandidate1: UILabel!
    @IBOutlet weak var amountCandidate2: UILabel!
    @IBOutlet weak var amountCandidate3: UILabel!
    
    @IBOutlet weak var showDetailsCandidate1: UILabel!
    @IBOutlet weak var showDetailsCandidate2: UILabel!
    @IBOutlet weak var showDetailsCandidate3: UILabel!
    
    
    @IBOutlet weak var txtOKToConect: UILabel!
    
    @IBOutlet weak var btnMoreCandidates: UILabel!
    @IBOutlet weak var btnChangeRank: UILabel!
    @IBOutlet weak var btnAprovalCandidate: UILabel!
    
    @IBOutlet weak var tblMoreCandidates: UITableView!
    @IBOutlet weak var tblMoreCandidateHight: NSLayoutConstraint!
    @IBOutlet weak var lineUnderTblMoreCandidateConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewTimerLeftTimeJob: UIView!
    @IBOutlet weak var txtLeftTime: UILabel!
    @IBOutlet weak var timerLeftTime: UILabel!
    @IBOutlet weak var timerTitleDay: UILabel!
    @IBOutlet weak var timerTitleHours: UILabel!
    @IBOutlet weak var timerTitleMinutes: UILabel!
    @IBOutlet weak var timerTitleSeconds: UILabel!
    
    @IBOutlet weak var viewTimeLeftOption: UIView!
    @IBOutlet weak var timeOverTxt: UILabel!
    @IBOutlet weak var btnFinishSearching: UILabel!
    @IBOutlet weak var btnNewSearch: UILabel!
    @IBOutlet weak var btnExtaTime: UILabel!
    
    @IBOutlet weak var btnDeleteJob: UILabel!
    @IBOutlet weak var btnShowDetailsJob: UILabel!
    

    
    //MARK: - Variable
    var looking = LookingForYouWorkers()
    var candidateFoundList = Array<CandidateFound>()
    
    var dragInteraction1 : UIDragInteraction?
    var dragInteraction2 : UIDragInteraction?
    var dragInteraction3 : UIDragInteraction?
    
    var dropInteraction1 :  UIDropInteraction?
    var dropInteraction2 :  UIDropInteraction?
    var dropInteraction3 :  UIDropInteraction?
    
    var fromImg = UIImageView()
    
    var seconds = 1
    var secondTimer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("www viewDidLoad")
        
        setGeneralDesign()
    }
    override func viewDidAppear(_ animated: Bool) {
        looking =  ((parent as? LookingForYouModelViewController)?.looking)!
       // (parent as? LookingForYouModelViewController)?.changeScrollViewHeight(heightToadd: viewBase.frame.height)
      //  (parent as? LookingForYouModelViewController)?.scrollView.translatesAutoresizingMaskIntoConstraints = true
        print("www viewDidAppear")

        GetCandidatesFound()

    }
    override func viewDidLayoutSubviews() {
        
//
//        //circel buttens
//        cancelCandidate.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
//        cancelCandidate.layer.borderWidth = 1
//        cancelCandidate.layer.cornerRadius = cancelCandidate.frame.height / 2
//
//        Global.sharedInstance.setButtonCircle(sender: shareBtn, isBorder: false)
//
//        Global.sharedInstance.setButtonCircle(sender: okBtn, isBorder: false)
//
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setDisplayData(){
         tblMoreCandidates.reloadData()
        
        animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "swatches-small").image
        if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.FirstCandidate.rawValue || looking.iEmployerAskedJobStatusType == EmployerJobStatusType.SecondCandidate.rawValue{
            titleStatus.text = "נמצא \(looking.nvEmployerAskedJobStatusType)"
        }else{
            titleStatus.text = looking.nvEmployerAskedJobStatusType
        }
        if looking.candidateFound != nil && looking.candidateFound!.iWaitingForReplyTime >= 0 {
            txtStstus.text = "המועמדים כבר ממתינים \(String(describing: looking.candidateFound!.iWaitingForReplyTime)) ימים לתשובה, שים לב, בזמן זה הם יכולים להגיש מועמדות למשרות אחרות"
        }else{
            txtStstus.text = "המועמדים ממתינים לתשובה, שים לב, בזמן זה הם יכולים להגיש מועמדות למשרות אחרות"
            
        }
        setCandidateFound(isClear: true , count: 3)//למחוק את תצוגת המועמדים הקודמים

        setCandidateFound(isClear: false , count: candidateFoundList.count)//להראות את המועמדים המעודכנים
        if looking.iEmployerAskedJobStatusType != EmployerJobStatusType.ModificationOrExtension.rawValue{
            viewTimeLeftOption.isHidden = true
            viewTimerLeftTimeJob.isHidden = false
            if (looking.dtWorkerCandidacyExpiration != nil){
                setTimer()
            }
            
        }else{
            viewTimeLeftOption.isHidden = false
            viewTimerLeftTimeJob.isHidden = true
            
            changeBtnActionNoEnable()

        }
      //   (parent as? LookingForYouModelViewController)?.changeScrollViewHeight(heightToadd: 30)
       // (parent as? LookingForYouModelViewController)?.scrollView.translatesAutoresizingMaskIntoConstraints = true

    }

        func setTimer() {
            seconds = 1
            if secondTimer != nil{
                secondTimer?.invalidate()
            }
            secondTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
        }
    
        func updateUI() {
            seconds += 1
            if looking.dtWorkerCandidacyExpiration != "" {
                timerLeftTime.text = String(Global.sharedInstance.getHourDifferencesToday(serverDate: looking.dtWorkerCandidacyExpiration!))
            }
        }
    
    func setCandidateFound(isClear : Bool , count : Int){
       
        for var i in stride(from: 0, to: count, by: 1) {
            print("www candidateFoundList count i: \(i) isClear: \(isClear)")

            if i == 0 {
                setMatchPercentage(label: matchPercentages1, i: i , isClear: isClear)
                setImageCandidate(img: imgCandideate1 , i: i ,isClear: isClear)
                setAmount(label: amountCandidate1 , i: i ,isClear: isClear)
                showDetailsCandidate1.isHidden = isClear
                if !isClear{
                showDetailsCandidate1.tag = 0
                let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(self.detailsCandidate(_:)))
                showDetailsCandidate1.addGestureRecognizer(tapGestureRecognizer1)
                }

            }
            else  if i == 1 {
                setMatchPercentage(label: matchPercentages2, i: i, isClear: isClear)
                setImageCandidate(img: imgCandideate2 , i: i, isClear: isClear)
                setAmount(label: amountCandidate2 , i: i, isClear: isClear)
              //  setShowDetailsCandidate(label: showDetailsCandidate2 , i: i)
                showDetailsCandidate2.isHidden = isClear
                  if !isClear{
                showDetailsCandidate2.tag = 1
                let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(self.detailsCandidate(_:)))
                showDetailsCandidate2.addGestureRecognizer(tapGestureRecognizer1)
                }

            }
            else  if i == 2 {
                setMatchPercentage(label: matchPercentages3, i: i , isClear: isClear)
                setImageCandidate(img: imgCandideate3 , i: i , isClear: isClear)
                setAmount(label: amountCandidate3 , i: i , isClear: isClear)
              //  setShowDetailsCandidate(label: showDetailsCandidate3 , i: i)
                showDetailsCandidate3.isHidden = isClear
                if !isClear{
                showDetailsCandidate3.tag = 2
                let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(self.detailsCandidate(_:)))
                showDetailsCandidate3.addGestureRecognizer(tapGestureRecognizer1)
                }

            }
        }
        if !isClear{
     //    For an image view, you must explicitly enable user interaction to allow drag and drop.
        imgCandideate1.isUserInteractionEnabled = true
        imgCandideate2.isUserInteractionEnabled = true
        imgCandideate3.isUserInteractionEnabled = true



        // Enable dragging from the image view (see ViewController+Drag.swift).
        dragInteraction1 = UIDragInteraction(delegate: self)
        dragInteraction1!.isEnabled = true
        imgCandideate1.addInteraction(dragInteraction1!)
        dragInteraction2 = UIDragInteraction(delegate: self)
        dragInteraction2!.isEnabled = true
        imgCandideate2.addInteraction(dragInteraction2!)
        dragInteraction3 = UIDragInteraction(delegate: self)
        dragInteraction3!.isEnabled = true
        imgCandideate3.addInteraction(dragInteraction3!)

        // Enable dropping onto the image view (see ViewController+Drop.swift).
        dropInteraction1 = UIDropInteraction(delegate: self)
        imgCandideate1.addInteraction(dropInteraction1!)
        dropInteraction2 = UIDropInteraction(delegate: self)
        imgCandideate2.addInteraction(dropInteraction2!)

        dropInteraction3 = UIDropInteraction(delegate: self)
        imgCandideate3.addInteraction(dropInteraction3!)
        }
    }
    //Enable dragging from the image view (see Drag).
    func setDragCandidate(dragInteraction : UIDragInteraction ,  img : UIImageView ){
        dragInteraction.isEnabled = true
        img.addInteraction(dragInteraction)
    }
       // Enable dropping onto the image view (see Drop).
    func setDropCandidate(dropInteraction : UIDropInteraction ,  img : UIImageView ){
        img.addInteraction(dropInteraction1!)
    }
    func setShowDetailsCandidate(label : UILabel ,  i : Int ){
        label.isHidden = false
        // to do open details candidate

       let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(detailsCandidate))
       label.addGestureRecognizer(tapGestureRecognizer1)
    }
 
    func setImageCandidate(img : UIImageView , i : Int , isClear : Bool){
        
            print ("www candidateFoundList l : \(i) isclear: \(isClear)")
            img.isHidden = isClear
        if i < candidateFoundList.count {
        var stringPath = api.sharedInstance.buldUrlFile(fileName: (candidateFoundList[i].nvImageFilePath))
        stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
        img.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
        img.tag = i
        }
    }
    
    func setMatchPercentage(label : UILabel ,  i : Int  , isClear : Bool) {
       
            label.isHidden = isClear
         if i < candidateFoundList.count {
            label.text = "\(candidateFoundList[i].iMatchPercentage)% התאמה"
        }
        
    }
    func setAmount(label : UILabel ,  i : Int ,isClear : Bool) {
        
            label.isHidden = isClear
         if i < candidateFoundList.count {
        label.text = "\(candidateFoundList[i].mRecommandedHourlyWage!) שח לשעה"
        }
    }
    
    //צביעת הכפתורים הרגילים - ללא פעילים
    func changeBtnActionNoEnable(){
        btnMoreCandidates.textColor = ControlsDesign.sharedInstance.colorGray
        btnChangeRank.textColor = ControlsDesign.sharedInstance.colorGray
        btnAprovalCandidate.textColor = ControlsDesign.sharedInstance.colorGray
        btnAprovalCandidate.backgroundColor = ControlsDesign.sharedInstance.colorGray
        btnAprovalCandidate.textColor = UIColor.white
        setCornerRadius(btn: btnMoreCandidates, borderColor: ControlsDesign.sharedInstance.colorGray.cgColor)
        setCornerRadius(btn: btnChangeRank, borderColor: ControlsDesign.sharedInstance.colorGray.cgColor)
        setCornerRadius(btn: btnAprovalCandidate, borderColor: ControlsDesign.sharedInstance.colorGray.cgColor)
        btnMoreCandidates.isUserInteractionEnabled = false
        btnChangeRank.isUserInteractionEnabled = false
        btnAprovalCandidate.isUserInteractionEnabled = false
    }
    
    func   setGeneralDesign(){
        addTaps()
        setCircleImage(img: imgCandideate1)
        setCircleImage(img: imgCandideate2)
        setCircleImage(img: imgCandideate3)
        setCornerRadius(btn: btnMoreCandidates, borderColor: ControlsDesign.sharedInstance.colorOrang.cgColor)
        setCornerRadius(btn: btnChangeRank, borderColor: ControlsDesign.sharedInstance.colorOrang.cgColor)
        setCornerRadius(btn: btnAprovalCandidate, borderColor: ControlsDesign.sharedInstance.colorOrang.cgColor)
        setCornerRadius(btn: btnNewSearch, borderColor: ControlsDesign.sharedInstance.colorOrang.cgColor)
        setCornerRadius(btn: btnFinishSearching, borderColor: ControlsDesign.sharedInstance.colorOrang.cgColor)
        setCornerRadius(btn: btnExtaTime, borderColor: ControlsDesign.sharedInstance.colorOrang.cgColor)
        setCornerRadius(btn: btnDeleteJob, borderColor: ControlsDesign.sharedInstance.colorGrayText.cgColor)
        btnAprovalCandidate.layer.masksToBounds = true
    lineUnderTblMoreCandidateConstrain.constant = 10
        //setBorders()
        //setCornerRadius()
        //setIconFont()
    }
    func setCornerRadius(btn : UILabel , borderColor : CGColor){
        btn.layer.borderColor = borderColor
        btn.layer.borderWidth = 1
        btn.layer.cornerRadius = btn.frame.height / 2
    
    }
    func setCircleImage(img : UIImageView){
        img.layer.cornerRadius = img.frame.height / 2
        img.layer.masksToBounds = true
        img.contentMode = UIViewContentMode.scaleAspectFill
    }
    func addTaps(){
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(deleteJob))
        btnDeleteJob.addGestureRecognizer(tapGestureRecognizer1)
        
       let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(AprovalCandidate))
        btnAprovalCandidate.addGestureRecognizer(tapGestureRecognizer2)
        
       let tapGestureRecognizer3 = UITapGestureRecognizer(target:self, action:#selector(moreCandidates))
        btnMoreCandidates.addGestureRecognizer(tapGestureRecognizer3)
        
       let tapGestureRecognizer4 = UITapGestureRecognizer(target:self, action:#selector(showDetailsJob))
        btnShowDetailsJob.addGestureRecognizer(tapGestureRecognizer4)
        
        let tapGestureRecognizer5 = UITapGestureRecognizer(target:self, action:#selector(finishSearching))
        btnFinishSearching.addGestureRecognizer(tapGestureRecognizer5)
        
        let tapGestureRecognizer6 = UITapGestureRecognizer(target:self, action:#selector(newSearch))
        btnNewSearch.addGestureRecognizer(tapGestureRecognizer6)
        
        let tapGestureRecognizer7 = UITapGestureRecognizer(target:self, action:#selector(extaTime))
        btnExtaTime.addGestureRecognizer(tapGestureRecognizer7)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    //MARK: - Server Function
    func GetCandidatesFound() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerJobId"] = (parent as? LookingForYouModelViewController)?.employerJobId as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> {
                        let candidate = CandidateFound()
                        self.candidateFoundList = Array<CandidateFound>()

                        for list in dicResult {
                            self.candidateFoundList.append(candidate.getCandidateFoundFromDic(dic: list))
                        }
                        print("www candidateFoundList count \(self.candidateFoundList.count)")

                        self.setDisplayData()
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetCandidatesFound") as AnyObject
    }
    //שינוי דרוג בין 2 מועמדים למשרה
    func UpdateWorkerOfferedJobRank(position1 : Int , position2 : Int) {
        //Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerOfferedJobId1"] = candidateFoundList[position1].iWorkerOfferedJobId as AnyObject?
        dic["iWorkerOfferedJobId2"] = candidateFoundList[position2].iWorkerOfferedJobId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject

        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
            //    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    let found1 = self.candidateFoundList[position1]
                    let found2 = self.candidateFoundList[position2]

               // self.candidateFoundList.remove(at: position1)
                    self.candidateFoundList[position1] = found2
                  //  self.candidateFoundList.remove(at: position2)
                    self.candidateFoundList[position2] = found1
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerOfferedJobRank")
    }
    
    
    // MARK: Tags
    
    func deleteJob(){
        (parent as? LookingForYouModelViewController)?.deleteEmployerJobShowAlert()
        
    }
    
    //מעדכנת את המועמד הנבחר
func AprovalCandidate(){
    (parent as? LookingForYouModelViewController)?.UpdateCandidateFoundStatus(approved: true , workerOfferedJobId: candidateFoundList[0].iWorkerOfferedJobId)
}
    //מציג רשימה של מועמדים נוספים
func moreCandidates(){
    if candidateFoundList.count > 3 {
        if lineUnderTblMoreCandidateConstrain.constant == 10 {
            lineUnderTblMoreCandidateConstrain.constant = 120
            tblMoreCandidates.isHidden = false
          
               (parent as? LookingForYouModelViewController)?.changeScrollViewHeight(heightToadd: 200)
        }else{
           lineUnderTblMoreCandidateConstrain.constant = 10
            tblMoreCandidates.isHidden = true
           (parent as? LookingForYouModelViewController)?.changeScrollViewHeight(heightToadd: -200)

        }
    }

    }
func showDetailsJob(){
    if #available(iOS 11.0, *) {
        (parent as? LookingForYouModelViewController)?.getDetailsJob()

    } else {
        // Fallback on earlier versions 
    }
    

}
    //מציגה את פרטי המועמד
    func detailsCandidate(_ recognizer: UIPanGestureRecognizer){
      let v = recognizer.view!
        let tag = v.tag
        print("detailsCandidate tag : \(tag) \(candidateFoundList[tag].nvWorkerName)")
        (parent as? LookingForYouModelViewController)?.getCandidateDetails(candidateFound: candidateFoundList[tag])
    }

//סיים חיפוש
func finishSearching(){
    self.deleteJob()
}
//חיפוש חדש
func newSearch(){
    let storyEmployer = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
    let   viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchWorkerModelViewController") as? SearchWorkerModelViewController
    viewCon?.sendData(isNewWorker: true,idJob: looking.iJobId, isCopyJob: false)
    self.navigationController?.pushViewController(viewCon!, animated: true)
}
//הארכת זמן
func extaTime(){
    let story = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
    let basePopUp = story.instantiateViewController(withIdentifier: "ExtensionTimePopupViewController") as! ExtensionTimePopupViewController
    basePopUp.getData( looking: looking)
    basePopUp.completion = {
        print("completion")
        (self.parent as? LookingForYouModelViewController)?.GetLookingForYouWorkersByEmployerJobId()

        basePopUp.dismiss(animated: true, completion: {
            print("completion dismiss")

//            if #available(iOS 11.0, *) {
//                print("completion ios 11 ")
//
//                (self.parent as? LookingForYouModelViewController)?.GetLookingForYouWorkersByEmployerJobId()
//             } else {
//               // Fallback on earlier versions
//               }
        })
    }

    basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
    self.present(basePopUp, animated: true, completion: nil)

}
    
}
@available(iOS 11.0, *)
extension CandidateFoundViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count = 0
        if self.candidateFoundList.count > 0 {
            count = self.candidateFoundList.count - 3
            
        }
       
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell
        
        cell = tableView.dequeueReusableCell(withIdentifier: "MoreCandidateTableViewCell") as! MoreCandidateTableViewCell
    (cell as! MoreCandidateTableViewCell).setDisplayData( candidateFound: candidateFoundList[indexPath.section + 3] , IndexPath: indexPath)
       // (cell as! LookingForWorkersTableViewCell).jobsToEmployeeDelegates = self
        
        //            (cell as! ActiveJobsToEmployeeTableViewCell).imgLogo.image = nil
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        return headerView
    }
    
    
    
}

@available(iOS 11.0, *)
extension CandidateFoundViewController: UIDragInteractionDelegate {
    // MARK: - UIDragInteractionDelegate
    
    /*
     The `dragInteraction(_:itemsForBeginning:)` method is the essential method
     to implement for allowing dragging from a view.
     */
    @available(iOS 11.0, *)
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        if interaction == dragInteraction1! {
            print(" fromImage is img1")
            fromImg = imgCandideate1
            fromImg.tag = imgCandideate1.tag
            
        }else if interaction == dragInteraction2! {
            fromImg = imgCandideate2
            print(" fromImage is img2")
            fromImg.tag = imgCandideate2.tag

            
            
        }else if interaction == dragInteraction3! {
            fromImg = imgCandideate3
            print(" fromImage is img3")
            fromImg.tag = imgCandideate3.tag

            
        }
        
    
        guard let image = fromImg.image else { return [] }
        
        let provider = NSItemProvider(object: image)
        let item = UIDragItem(itemProvider: provider)
        item.localObject = image
        
        /*
         Returning a non-empty array, as shown here, enables dragging. You
         can disable dragging by instead returning an empty array.
         */
        return [item]
    }
    
    /*
     Code below here adds visual enhancements but is not required for minimal
     dragging support. If you do not implement this method, the system uses
     the default lift animation.
     */
    @available(iOS 11.0, *)
    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        guard let image = item.localObject as? UIImage else { return nil }
        
        // Scale the preview image view frame to the image's size.
        let frame: CGRect
        if image.size.width > image.size.height {
            let multiplier = fromImg.frame.width / image.size.width
            frame = CGRect(x: 0, y: 0, width: fromImg.frame.width, height: image.size.height * multiplier)
        } else {
            let multiplier = fromImg.frame.height / image.size.height
            frame = CGRect(x: 0, y: 0, width: image.size.width * multiplier, height: fromImg.frame.height)
        }
        
        // Create a new view to display the image as a drag preview.
        let previewImageView = UIImageView(image: image)
        previewImageView.contentMode = .scaleAspectFit
        previewImageView.frame = frame
        
        /*
         Provide a custom targeted drag preview that lifts from the center
         of imageView. The center is calculated because it needs to be in
         the coordinate system of imageView. Using imageView.center returns
         a point that is in the coordinate system of imageView's superview,
         which is not what is needed here.
         */
        let center = CGPoint(x: fromImg.bounds.midX, y: fromImg.bounds.midY)
        let target = UIDragPreviewTarget(container: fromImg, center: center)
        return UITargetedDragPreview(view: previewImageView, parameters: UIDragPreviewParameters(), target: target)
    }
}
@available(iOS 11.0, *)
extension CandidateFoundViewController: UIDropInteractionDelegate {
    // MARK: - UIDropInteractionDelegate
    
    /**
     Ensure that the drop session contains a drag item with a data representation
     that the view can consume.
     */
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.hasItemsConforming(toTypeIdentifiers: [kUTTypeImage as String]) && session.items.count == 1
    }
    
    // Update UI, as needed, when touch point of drag session enters view.
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidEnter session: UIDropSession) {
      //  let dropLocation = session.location(in: self.view)
    }
    
    /**
     Required delegate method: return a drop proposal, indicating how the
     view is to handle the dropped items.
     */
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        let dropLocation = session.location(in: view)
        
        let operation: UIDropOperation
        
       //if viewCandidate1.frame.contains(dropLocation) || viewCandidate2.frame.contains(dropLocation) || viewImgCandidate3.frame.contains(dropLocation) {
        if viewCandidateScema.frame.contains(dropLocation){
            /*
             If you add in-app drag-and-drop support for the .move operation,
             you must write code to coordinate between the drag interaction
             delegate and the drop interaction delegate.
             */
            operation = session.localDragSession == nil ? .copy : .move
        } else {
            // Do not allow dropping outside of the image view.
            operation = .cancel
        }
        
        return UIDropProposal(operation: operation)
    }
    
    /**
     This delegate method is the only opportunity for accessing and loading
     the data representations offered in the drag item.
     */
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        print("performDrop")
        // Consume drag items (in this example, of type UIImage).
        session.loadObjects(ofClass: UIImage.self) { imageItems in
            let images = imageItems as! [UIImage]
            let dropLocation = session.location(in: self.viewCandidateScema)
            if self.viewCandidate1.frame.contains(dropLocation)  {
                self.UpdateWorkerOfferedJobRank(position1: self.fromImg.tag, position2: self.imgCandideate1.tag)

                self.fromImg.image = self.imgCandideate1.image
                self.imgCandideate1.image = images.first
                print("fromImg.tag: \(self.fromImg.tag) self.fromImg.tag \(self.imgCandideate1.tag)")
           
            } else if self.viewCandidate2.frame.contains(dropLocation) {
                self.UpdateWorkerOfferedJobRank(position1: self.fromImg.tag, position2: self.imgCandideate2.tag)

                self.fromImg.image  = self.imgCandideate2.image
                self.imgCandideate2.image = images.first
                print("fromImg.tag: \(self.fromImg.tag) self.fromImg.tag \(self.imgCandideate2.tag)")

            }else if self.viewCandidate3.frame.contains(dropLocation) {
                self.UpdateWorkerOfferedJobRank(position1: self.fromImg.tag, position2: self.imgCandideate3.tag)

                self.fromImg.image  = self.imgCandideate3.image
                self.imgCandideate3.image = images.first
                print("fromImg.tag: \(self.fromImg.tag) self.fromImg.tag \(self.imgCandideate3.tag)")

            }
            /*
             If you do not employ the loadObjects(ofClass:completion:) convenience
             method of the UIDropSession class, which automatically employs
             the main thread, explicitly dispatch UI work to the main thread.
             For example, you can use `DispatchQueue.main.async` method.
             */
            
        }
        
        // Perform additional UI updates as needed.
        //updateLayers(forDropLocation: dropLocation)
    }
    
    // Update UI, as needed, when touch point of drag session leaves view.
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidExit session: UIDropSession) {
        //let dropLocation = session.location(in: view)
    }
    
    // Update UI and model, as needed, when drop session ends.
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidEnd session: UIDropSession) {
     //   let dropLocation = session.location(in: view)
    }
    
    
   
}
