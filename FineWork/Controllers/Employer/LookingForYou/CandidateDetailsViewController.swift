//
//  CandidateDetailsViewController.swift
//  FineWork
//
//  Created by User on 20/11/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import AVKit
import Foundation

class CandidateDetailsViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

      //MARK: - Views
    
    @IBOutlet weak var viewBase: UIView!
    
    @IBOutlet weak var closeDetails: UILabel!
    
    @IBOutlet weak var nameCandidate: UILabel!
    @IBOutlet weak var imgCandidate: UIImageView!
    
    @IBOutlet weak var viewNumberRank: UIView!
    @IBOutlet weak var rankTitle: UILabel!
    @IBOutlet weak var numberRank: UILabel!
    @IBOutlet weak var rankArrow: UILabel!
    
    @IBOutlet weak var viewRankDetails: UIView!
    @IBOutlet weak var progres1Title: UILabel!
    @IBOutlet weak var progres2Title: UILabel!
    @IBOutlet weak var progres3Title: UILabel!
    @IBOutlet weak var progres4Title: UILabel!
    @IBOutlet weak var progres1: UIProgressView!
    @IBOutlet weak var progres2: UIProgressView!
    @IBOutlet weak var progres3: UIProgressView!
    @IBOutlet weak var progres4: UIProgressView!
    @IBOutlet weak var viewRankDetailsConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewWebView: UIView!
    @IBOutlet weak var viewWebViewConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewAvailability: UIView!
    @IBOutlet weak var availabilityTitle: UILabel!
    @IBOutlet weak var availabilityTxt: UILabel!
    @IBOutlet weak var availabilityArrow: UILabel!
    
    
    @IBOutlet weak var viewCalanderOrTime: UIView!
    @IBOutlet weak var viewCalanderOrTimeConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewCalander: UIView!
    @IBOutlet weak var calandarCollection: JTAppleCalendarView!
    @IBOutlet weak var nameMonth: UILabel!
    
    @IBOutlet weak var viewWitoutCalandar: UIView!
    
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var fromeHouers: UITextField!
    @IBOutlet weak var toHouers: UITextField!
    
    
    @IBOutlet weak var viewHourlyWage: UIView!
    @IBOutlet weak var HourlyWageTitle: UILabel!
    @IBOutlet weak var HourlyWageTxt: UILabel!
    
    @IBOutlet weak var domainTitle: UILabel!
    @IBOutlet weak var domain: UILabel!
    @IBOutlet weak var roleTilte: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var experiensTitle: UILabel!
    @IBOutlet weak var experiens: UILabel!
    
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var phoneTitle: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var viewPhoneConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var addressTitle: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var viewAddressConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewAge: UIView!
    @IBOutlet weak var ageTitle: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var viewAgeConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewFamalyState: UIView!
    @IBOutlet weak var famalyStateTitle: UILabel!
    @IBOutlet weak var famalyState: UILabel!
    @IBOutlet weak var viewFamalyStaiteCOnstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewNationalService: UIView!
    @IBOutlet weak var nationalServiceTitle: UILabel!
    @IBOutlet weak var nationalService: UILabel!
    @IBOutlet weak var viewNationalServiceCOnstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewLanguage: UIView!
    @IBOutlet weak var llanguageTitle: UILabel!
    @IBOutlet weak var language: UILabel!
     @IBOutlet weak var viewLanguageCnstrain: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewCharacteristics: UIView!
    @IBOutlet weak var characteristicsTitle: UILabel!
    @IBOutlet weak var characteristics: UILabel!
       @IBOutlet weak var viewCharacteristicsCnstrain: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewStudies: UIView!
    @IBOutlet weak var studiesTitle: UILabel!
    @IBOutlet weak var studies: UILabel!
         @IBOutlet weak var viewStudiesCnstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewExperience: UIView!
    @IBOutlet weak var experienceTitle: UILabel!
    @IBOutlet weak var experience: UILabel!
        @IBOutlet weak var viewExperienceCnstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewSuitableWork: UIView!
    @IBOutlet weak var suitableWorkTitle: UILabel!
    @IBOutlet weak var suitableWork: UILabel!
     @IBOutlet weak var viewSuitableWorkCnstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewEmployerSayAboutYou: UIView!
    @IBOutlet weak var employerSayAboutYouTitle: UILabel!
    @IBOutlet weak var employerSayAboutYouTableView: UITableView!
    @IBOutlet weak var viewEmployerSayAboutYouConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var viewAction: UIView!
    @IBOutlet weak var viewBtnChat: UIView!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnOk: UILabel!
    @IBOutlet weak var btnMoreCandidate: UILabel!
    @IBOutlet weak var btnNoSuitableForMe: UILabel!
    
    //MARK: - Variable
    var mCandidateFound : CandidateFound?
    var mIsFromListJobs = true
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var numberOfRows = 6
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    let firstDayOfWeek: DaysOfWeek = .sunday
    var hasStrictBoundaries = true
    var monthSize: MonthSize? = nil
    var myDate: Date? = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("www viewDidLoad")
        initCalander()
        setGeneralDesign()

        setGeneralData()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //looking =  ((parent as? LookingForYouModelViewController)?.looking)!
      setGeneralDesign()
        setGeneralData()

        print("www viewDidAppear")
    }
    override func viewDidLayoutSubviews() {
       // setGeneralData()

        print("www viewDidLayoutSubviews")

        if #available(iOS 11.0, *) {
            ((parent as? LookingForYouModelViewController)?.setHeightToScroll(height: viewBase.frame.height))
            (parent as? LookingForYouModelViewController)?.scrollView.translatesAutoresizingMaskIntoConstraints = true

        } else {
            // Fallback on earlier versions
        }
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //פןנקציה המקבלת את המועמד שאותו מצגים
    func sendData(candidateFound : CandidateFound , isFromListJobs : Bool ){
        mCandidateFound = candidateFound
        mIsFromListJobs = isFromListJobs
    }
    //תצוגה - עיגול כפתורים וכו'
    func setGeneralDesign(){

        closeDetails.text = FlatIcons.sharedInstance.x
        rankArrow.text = FlatIcons.sharedInstance.arrow_down
        
        btnOk.layer.cornerRadius = btnOk.frame.height / 2
   
        btnNoSuitableForMe.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        btnNoSuitableForMe.layer.borderWidth = 1
        btnNoSuitableForMe.layer.cornerRadius = btnNoSuitableForMe.frame.height / 2
        
//        btnMoreCandidate.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
//        btnMoreCandidate.layer.borderWidth = 1
//        btnMoreCandidate.layer.cornerRadius = btnMoreCandidate.frame.height / 2
        
        rankArrow.layer.borderColor = ControlsDesign.sharedInstance.colorBlue.cgColor
        rankArrow.layer.borderWidth = 1
        rankArrow.layer.cornerRadius = rankArrow.frame.height / 2
        
        viewBtnChat.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewBtnChat.layer.borderWidth = 1
        viewBtnChat.layer.cornerRadius = viewBtnChat.frame.height / 2
        
        imgCandidate.layer.cornerRadius = imgCandidate.frame.height / 2
        imgCandidate.layer.masksToBounds = true
        imgCandidate.contentMode = UIViewContentMode.scaleAspectFill
        imgCandidate.clipsToBounds = true
        
        
        progres1.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        progres2.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        progres3.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        progres4.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
        setTaps()
    }
    
    //הצגת הנתונים על המסך
    func setGeneralData(){
        if mCandidateFound != nil {
            viewBase.isHidden = false

        nameCandidate.text = mCandidateFound!.nvWorkerName
        imgCandidate.downloadedFrom(link: api.sharedInstance.buldUrlFile(fileName: mCandidateFound!.nvImageFilePath) )

        if mCandidateFound!.ranking != nil {
        if mCandidateFound!.ranking!.nRankingAvg > 0{
           numberRank.text = "\(mCandidateFound!.ranking!.nRankingAvg)%"
         
        }else{
          viewNumberRank.isHidden = true
        }
            progres1.progress = Float(mCandidateFound!.ranking!.nRanking1Avg ) / Float(10)
            progres2.progress = Float(mCandidateFound!.ranking!.nRanking2Avg ) / Float(10)
            progres3.progress = Float(mCandidateFound!.ranking!.nRanking3Avg ) / Float(10)
            progres4.progress = Float(mCandidateFound!.ranking!.nRanking4Avg ) / Float(10)
            
            progres1Title.text = "אמינות" + " - \(mCandidateFound!.ranking!.nRanking1Avg)"
            progres2Title.text = "הגינות" + " - \(mCandidateFound!.ranking!.nRanking2Avg)"
            progres3Title.text = "תנאי עבודה" + " - \(mCandidateFound!.ranking!.nRanking3Avg)"
            progres4Title.text = "כללי" + " - \(mCandidateFound!.ranking!.nRanking4Avg)"
            
            if mCandidateFound!.ranking!.lRankingComment != nil && mCandidateFound!.ranking!.lRankingComment!.count > 0 {
                setRankWithImg()
                employerSayAboutYouTitle.text = "מה מעסיקים אחרים אומרים על \(mCandidateFound!.nvWorkerName)?"
            }else{
              viewEmployerSayAboutYou.isHidden = true
            }
      
        }else{
            
        }
        
        if mCandidateFound!.nvVideoLink != "" {
            playVideo(from: mCandidateFound!.nvVideoLink)
        }else{
            viewWebView.isHidden = false
            viewWebViewConstrain.constant = 0
        }
       viewCalander.isHidden = true
            viewWitoutCalandar.isHidden = true
            viewCalanderOrTimeConstrain.constant = 0
            viewCalanderOrTime.isHidden = true
        if mCandidateFound!.nPercentAvailability > 0.0 {
            if mCandidateFound!.nPercentAvailability != 100 {
                availabilityTxt.text = "\(mCandidateFound!.nvJobType) \(mCandidateFound!.nPercentAvailability) %"
                initCalander()
            initAvailabilityTimeShow()
                //viewCalanderOrTimeConstrain.constant = 220

            }else {
                availabilityTxt.text = "\(mCandidateFound!.nvJobType)"
                viewCalanderOrTimeConstrain.constant = 0
viewCalanderOrTime.isHidden = true
            }
        } else {
          viewAvailability.isHidden = false
            viewCalanderOrTimeConstrain.constant = 0

        }

        HourlyWageTxt.text = ((mCandidateFound!.mRecommandedHourlyWage)?.description)! + "שח לשעה"
        
        if  mCandidateFound!.Domain != "" {
            domain.text = mCandidateFound!.Domain
        }else{
            domain.isHidden = true
            domainTitle.isHidden = true
        }
        
        if mCandidateFound!.nvRole != "" {
            role.text = mCandidateFound!.nvRole
        }else {
            role.isHidden = true
            roleTilte.isHidden = true
        }
        
        if mCandidateFound!.nYearsOfExperience != nil && mCandidateFound!.nYearsOfExperience! > 0 {
            experiens.text = " \(mCandidateFound!.nYearsOfExperience!.rounded()) שנים"
        }else{
            experiens.isHidden = true
            experiensTitle.isHidden = true
        }
        
        phone.text = mCandidateFound!.nvMobile
        
        if mCandidateFound!.fAge != nil && mCandidateFound!.fAge != 0 {
         age.text =  (mCandidateFound!.fAge)?.description
        }else{
            age.isHidden = true
            ageTitle.isHidden = true
            viewAgeConstrain.constant = 0
        }
        
        if mCandidateFound!.nvAddress != "" {
         address.text = mCandidateFound!.nvAddress
        }else{
            address.isHidden = true
            addressTitle.isHidden = true
            viewAddressConstrain.constant = 0
        }
        
        if mCandidateFound!.nvFamilyStatusType != ""  {
            famalyState.text = mCandidateFound!.nvFamilyStatusType
        }else{
            famalyState.isHidden = true
            famalyStateTitle.isHidden = true
            viewFamalyStaiteCOnstrain.constant = 0
        }
        if mCandidateFound!.nvMilitaryService != ""  {
          nationalService.text = mCandidateFound!.nvMilitaryService
        }else{
            nationalService.isHidden = true
            nationalServiceTitle.isHidden = true
            viewNationalServiceCOnstrain.constant = 0
        }
        
        if  mCandidateFound!.nvLanguages != ""  {
            language.text =  mCandidateFound!.nvLanguages
        }else{
            language.isHidden = true
            llanguageTitle.isHidden = true
            viewLanguageCnstrain.constant = 0
        }
        
        if mCandidateFound!.nvResumeCharacteristics != "" {
       characteristics.text =  mCandidateFound!.nvResumeCharacteristics
        }else
        {
          characteristics.isHidden = true
            characteristicsTitle.isHidden = true
            viewCharacteristicsCnstrain.constant = 0
        }
        
        if mCandidateFound!.nvResumeStudies != "" {
      studies.text =  mCandidateFound!.nvResumeStudies
        }else{
           studies.isHidden = true
            studiesTitle.isHidden = true
            viewStudiesCnstrain.constant = 0
        }
        
        if mCandidateFound!.nvResumeWorkExperience != ""  {
     experience.text = mCandidateFound!.nvResumeWorkExperience
        }else{
          experience.isHidden = true
            experienceTitle.isHidden = true
            viewExperienceCnstrain.constant = 0
        }
        
        if mCandidateFound!.nvResumeSuitableWork != "" {
       suitableWork.text =  mCandidateFound!.nvResumeSuitableWork
        }else{
           suitableWork.isHidden = true
            suitableWorkTitle.isHidden = true
            viewSuitableWorkCnstrain.constant = 0
        }
        
        }
     
        //אם מגיעים לפרוט של מועמד מהמסך הראשי - לא להראות כפתורי פעולה
        if mIsFromListJobs {
            viewAction.isHidden = true
        }else{
            viewAction.isHidden = false
        }
    }
    
   // הפעלת וידיאו
    private func playVideo(from file:String) {
        let videoURL = URL(string: file)
        let player =  AVPlayer(url: videoURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.viewWebView.layer.bounds
    // playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.viewWebView.layer.addSublayer(playerLayer)
        playerLayer.frame = self.viewWebView.layer.bounds
        viewWebView.clipsToBounds = true
        player.play()
    }
        
        // הצגת הזמינות של העובד
        func  initAvailabilityTimeShow(){
            
            viewAvailability.isUserInteractionEnabled = true
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openAvailability))
            viewAvailability.addGestureRecognizer(tap)
            
          
            if mCandidateFound!.iEmployerJobScheduleType != EmployerJobScheduleType.ConstantHoursJob.rawValue {
                         // initCalander()
                        }else{
                            //baseViewHeughtConstrain.constant = baseView.frame.height + viewWitoutCalander.frame.height
    
    
                            viewWitoutCalandar.isHidden = false
                            viewCalander.isHidden = true
                           // viewWithCalanserHeightConstrin.constant = 0
                fromDate.text = Global.sharedInstance.getStringFromDateString(dateString: mCandidateFound!.lWorkerOfferedJobTimeAvailablity[0].dFromDate)
                            toDate.text = Global.sharedInstance.getStringFromDateString(dateString: mCandidateFound!.lWorkerOfferedJobTimeAvailablity[0].dToDate!)
                            fromeHouers.text = mCandidateFound!.lWorkerOfferedJobTimeAvailablity[0].tFromHoure
                            toHouers.text = mCandidateFound!.lWorkerOfferedJobTimeAvailablity[0].tToHoure
        }
    }
    func  setTaps(){
        rankArrow.isUserInteractionEnabled = true
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openRankDetails))
        rankArrow.addGestureRecognizer(tap1)
        
        btnOk.isUserInteractionEnabled = true
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OKAction))
        btnOk.addGestureRecognizer(tap2)
        
        btnNoSuitableForMe.isUserInteractionEnabled = true
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(noSuitableForMeAction))
        btnNoSuitableForMe.addGestureRecognizer(tap3)
        
        btnMoreCandidate.isUserInteractionEnabled = true
        let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeCanidateDetails))
        btnMoreCandidate.addGestureRecognizer(tap4)
        
       closeDetails.isUserInteractionEnabled = true
        let tap5: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeCanidateDetails))
        closeDetails.addGestureRecognizer(tap5)
    }
    
    //פתיחת וסגירת לוח שנה עם הזמנינות של העובד
    func openAvailability(){
       if mCandidateFound!.nPercentAvailability > 0 &&  mCandidateFound!.nPercentAvailability != 100 {
        if mCandidateFound?.iEmployerJobScheduleType == EmployerJobScheduleType.ConstantHoursJob.rawValue{
            if viewWitoutCalandar.isHidden {
                //לפתוח את הזמינות הקבועה
                viewCalanderOrTimeConstrain.constant = 200
                viewCalanderOrTime.isHidden = false
                viewCalander.isHidden = true
                viewWitoutCalandar.isHidden = false
            }else{
                //לסגור את הזמינות הקבועה
                viewCalanderOrTimeConstrain.constant = 0
                viewCalanderOrTime.isHidden = true
                viewCalander.isHidden = true
                viewWitoutCalandar.isHidden = true
            }
        }else{
            if viewCalander.isHidden{
                viewCalanderOrTimeConstrain.constant = 400
                viewCalanderOrTime.isHidden = false
                viewCalander.isHidden = false
                viewWitoutCalandar.isHidden = true
            }else{
                viewCalanderOrTimeConstrain.constant = 0
                viewCalanderOrTime.isHidden = true
                viewCalander.isHidden = true
                viewWitoutCalandar.isHidden = true
            }
        }
        }
//        viewCalanderOrTimeConstrain.constant = 400
//        viewCalander.isHidden = false
//        viewWitoutCalandar.isHidden = true
//        viewCalanderOrTime.isHidden = false
//        if viewCalanderOrTimeConstrain.constant == 0 {
//            viewCalanderOrTimeConstrain.constant = 200
//            viewCalander.isHidden = false
//            viewWitoutCalandar.isHidden = false
//        }else{
//            viewCalanderOrTimeConstrain.constant = 0
//            viewCalander.isHidden = true
//            viewWitoutCalandar.isHidden = true
//
//        }
        
    }
    //הצגת פרוט הדרוג
    func openRankDetails(){
        if viewRankDetails.isHidden {
            viewRankDetails.isHidden = false
            viewRankDetailsConstrain.constant = 190
        }else{
            viewRankDetails.isHidden = true
            viewRankDetailsConstrain.constant = 0
        }
    }
    
    //לחיצה על הכפתור מתאים לי בול
    func OKAction(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.UpdateCandidateFoundStatus(approved: true , workerOfferedJobId: mCandidateFound!.iWorkerOfferedJobId)
        } else {
            // Fallback on earlier versions
        }
    }


// מתאים לי בול לא מתאים לי
func noSuitableForMeAction(){
    if #available(iOS 11.0, *) {
        (parent as? LookingForYouModelViewController)?.UpdateCandidateFoundStatus(approved: false ,workerOfferedJobId: mCandidateFound!.iWorkerOfferedJobId )
    } else {
        // Fallback on earlier versions
    }
}


//סגירת המסך
func closeCanidateDetails(){
    if mIsFromListJobs{
        self.navigationController?.popViewController(animated: true)

    }else{
    viewBase.isHidden = true
    mCandidateFound = nil
    if #available(iOS 11.0, *) {
        (parent as? LookingForYouModelViewController)?.getCandidateFound()
    } else {
        // Fallback on earlier versions
    }
    }
}


    //MARK: -TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mCandidateFound!.ranking!.lRankingComment!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("aaa tableView \(indexPath.row)")
        
        let cell : RankingCellTableViewCell
        
        cell = tableView.dequeueReusableCell(withIdentifier: "RankingCellTableViewCell") as! RankingCellTableViewCell
        
        cell.textRang?.text = self.mCandidateFound!.ranking!.lRankingComment![indexPath.row].nvRankingComment
        cell.imgRank?.image = self.mCandidateFound!.ranking!.lRankingComment![indexPath.row].imageView
        //        cel.del = self
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((self.view.frame.size.height * 0.77) - 3.5) / 7
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.001
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    //MARK: Get Images
    
    
    var counter = 0
    var imageMap = Array<String>()
    var downloadsCounter = 0
    
    
    func setRankWithImg() {
        print("aaa setRonkWithImg")
        counter = 0
        downloadsCounter = 0
        imageMap.removeAll()
        
        for index in stride(from: 0, to: mCandidateFound!.ranking!.lRankingComment!.count, by: 1) {
            
            var isExist = false
            for i in stride(from: 0, to: imageMap.count, by: 1) {
                if mCandidateFound!.ranking!.lRankingComment?[index].nvImage  == imageMap[i] {
                    isExist = true
                    //                            index2 = i
                    break
                }
            }
            if !isExist {
                imageMap.insert((mCandidateFound!.ranking?.lRankingComment?[index].nvImage)!, at: imageMap.count)
                counter += 1
                var stringPath = api.sharedInstance.buldUrlFile(fileName: (mCandidateFound!.ranking!.lRankingComment?[index].nvImage)!)
                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                if let url = URL(string: stringPath) {
                    downloadImage(url: url, index: /*index*/imageMap.count-1)
                }
            }
            
            
            //                item.logoImage.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
        }
        print("aaa finish setRonkWithImg")
        
        employerSayAboutYouTableView.delegate = self
        employerSayAboutYouTableView.dataSource = self
    }
    
    func downloadImage(url: URL, index: Int) {
        print("aaa downloadImage start")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    
                    
                    for i in stride(from: 0, to: self.mCandidateFound!.ranking!.lRankingComment!.count, by: 1) {
                        if self.mCandidateFound!.ranking!.lRankingComment?[i].nvImage == self.imageMap[index] {
                            self.mCandidateFound!.ranking!.lRankingComment?[i].imageView = image
                            print("add image \(i)")
                            print("i: \(i) \(String(describing: self.mCandidateFound!.ranking!.lRankingComment?[i].imageView ))")
                        }
                        
                        
                        self.downloadsCounter += 1
                        
                        print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                        if self.downloadsCounter == self.counter {
                            self.employerSayAboutYouTableView.reloadData()
                            self.employerSayAboutYouTableView.beginUpdates()
                            self.employerSayAboutYouTableView.endUpdates()
                            
                        }
                        
                    }
                } } else {
                self.downloadsCounter += 1
                
                print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                if self.downloadsCounter == self.counter {
                    self.employerSayAboutYouTableView.reloadData()
                }
            }
            
            
            
        }
        print("aaa downloadImage finish")
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func initCalander(){
        // Do any additional setup after loading the view.
        calandarCollection.transform = CGAffineTransform(scaleX: -1, y: 1)
        //        getDetailsFromDicMonth(BShowFullSchedule: btnRadioButton.isSelected)
        //        getHolidays()
        calandarCollection.calendarDataSource = self as? JTAppleCalendarViewDataSource
        calandarCollection.calendarDelegate = self as? JTAppleCalendarViewDelegate
        
        calandarCollection.minimumLineSpacing = 0
        
        calandarCollection.scrollToMonthOfDate(Date())
        
        calandarCollection.register(UINib(nibName: "PinkSectionHeaderView", bundle: Bundle.main),
                                    forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                    withReuseIdentifier: "PinkSectionHeaderView")
        
        self.calandarCollection.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            //   self.setupViewsOfCalendar(from: visibleDates, isEndScroll: <#Bool#>)
            self.calandarCollection.layer.borderWidth = 0.3
            
        }
    }
    
    //MARK: - Calendar Functions
    var prevSection = 0
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo, isEndScroll: Bool) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
        print("month : \(monthName)")
        nameMonth.text = "\(monthName) \(year)"
        print("\(year)")
        if isEndScroll {
            calandarCollection.scrollToSection()
            // changeScrollViewDelegate.changeScrollViewHeight(heightToadd: 0)
        }
    }
    
    
    var prePostVisibility: ((CellState,JobDayCalanderCollectionViewCell?)->())?
    func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        prePostVisibility?(cellState, cell as? JobDayCalanderCollectionViewCell)
        //calendarView.reloadData()
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? JobDayCalanderCollectionViewCell  else {
            return
        }
        if testCalendar.isDateInToday(cellState.date){
            myCustomCell.backgroundColor = UIColor.lightGray
        }
    }
    
    var flagH = false
    func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? JobDayCalanderCollectionViewCell else {return }
        
        var currentTimeAvailablity : WorkerOfferedJobTimeAvailablity? = nil
        
        if mCandidateFound!.lWorkerOfferedJobTimeAvailablity.count > 0{
            flagH = false
            
            for time in mCandidateFound!.lWorkerOfferedJobTimeAvailablity {
                let date1 = Global.sharedInstance.getStringFromDateString(dateString: time.dFromDate)
                
                if date1 == cellState.date.getStringFromDate(today: cellState.date) {
                    self.flagH = true
                    currentTimeAvailablity = time
                    time.date = cellState.date
                    break
                }
            }
            
        }
        
        if currentTimeAvailablity != nil {
            var statusJob = OfferJobStatusType.new.rawValue
            if currentTimeAvailablity!.bWorkerOfferedJobTimeAvailablity {
                statusJob = OfferJobStatusType.interesting.rawValue
            }
            
            if cellState.isSelected && cellState.dateBelongsTo == .thisMonth {
                
                myCustomCell.handleCellSelection(cellState: cellState, timeAvailablity: currentTimeAvailablity!, statusDateInCalander: StatusDateInCalander.regularInMonth.rawValue , statusJob: statusJob ,position :0 )
                myDate = cellState.date
                
            }else{
                myCustomCell.handleCellSelection(cellState: cellState, timeAvailablity: currentTimeAvailablity!, statusDateInCalander: StatusDateInCalander.regularNoInMonth.rawValue , statusJob: statusJob , position :0)
            }
        } else {
            
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.handleCellSelection(cellState: cellState,  statusDateInCalander: StatusDateInCalander.regularInMonth.rawValue)
            } else {
                myCustomCell.handleCellSelection(cellState: cellState, statusDateInCalander: StatusDateInCalander.regularNoInMonth.rawValue)
                
            }
        }
        
    }
    
}

// MARK : JTAppleCalendarDelegate
extension CandidateDetailsViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource{
    
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.timeZone = testCalendar.timeZone
        formatter.locale = testCalendar.locale
        
        
        
        let date = Date()
        var components = DateComponents()
        components.setValue(-10, for: Calendar.Component.year)
        let startDate = Calendar.current.date(byAdding: components, to: date)
        components.setValue(10, for: Calendar.Component.year)
        let endDate = Calendar.current.date(byAdding: components, to: date)
        
        
        //        let endDate = /*formatter.date(from: "2018 02 01")!*/Date(timeIntervalSinceNow: 30)
        
        let parameters = ConfigurationParameters(startDate: startDate!,
                                                 endDate: endDate!,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: firstDayOfWeek,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "JobDayCalanderCollectionViewCell", for: indexPath) as! JobDayCalanderCollectionViewCell
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
        
        //     print("ppppp \(workerOfferedJob.iOfferJobStatusType)")
        ( myCustomCell as JobDayCalanderCollectionViewCell).setDisplayData(cellState: cellState, status: OfferJobStatusType.new.rawValue/*, timeAvailablity: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[indexPath.row]*/)
        
        myCustomCell.frame.size = CGSize(width: self.calandarCollection.frame.width / 7 + 15 , height: 50)
        
        
        
        return myCustomCell
    }
    
    
    
    
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
        print (date)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates , isEndScroll: false)
        print("didScrollToDateSegmentWith")
    }
    
    func scrollDidEndDecelerating(for calendar: JTAppleCalendarView) {
        
        let visibleDates = calandarCollection.visibleDates()
        self.setupViewsOfCalendar(from: visibleDates, isEndScroll: true)
        print("scrollDidEndDecelerating")
    }
    
    func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
        let stride = calandarCollection.frame.width * CGFloat(indexPath.section)
        return CGRect(x: stride + 5, y: 5, width: calandarCollection.frame.width - 10, height: calandarCollection.frame.height - 10)
    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return monthSize
    }
}
