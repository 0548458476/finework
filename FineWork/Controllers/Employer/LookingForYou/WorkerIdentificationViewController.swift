//
//  WaitingForWorkerIdentificationViewController.swift
//  FineWork
//
//  Created by User on 04/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerIdentificationViewController: UIViewController {
    
    //MARK: - Views
    @IBOutlet weak var animStatus : UIImageView!
    @IBOutlet weak var titleStatus: UILabel!
    @IBOutlet weak var imgWorker: UIImageView!
    @IBOutlet weak var statusTxt: UILabel!
    @IBOutlet weak var nameWorkerTitle: UILabel!
    @IBOutlet weak var nameWorker: UILabel!
    @IBOutlet weak var idWorkerTiltle: UILabel!
    @IBOutlet weak var idWorker: UILabel!
    @IBOutlet weak var distanceTitle1: UILabel!
    @IBOutlet weak var distanceTitleTF: UITextField!
    @IBOutlet weak var distanceTitle2: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var okTxt: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var viewChat: UIView!
    @IBOutlet weak var imgChat: UIImageView!
    @IBOutlet weak var btnDeleteJob: UILabel!
    @IBOutlet weak var btnShowJobDetails: UILabel!
    
    //MARK: - Action
    @IBAction func OKAction(_ sender: Any) {
        
        if distanceTitleTF.text != "" {
            if  let distance = Int(distanceTitleTF.text!){
                if distance > 0 && distance < 5000{
                   UpdateWorkerIdentification()
                }else{
                    Alert.sharedInstance.showAlert(mess: "המרחק צריך להיות גדול מאפס וקטן מ - 5000")
                }
            }
            
        }else{
           Alert.sharedInstance.showAlert(mess: "חובה להזין ערך")
        }
        
    }
    
    //MARK: - Variable
    var looking = LookingForYouWorkers()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            looking =  ((parent as? LookingForYouModelViewController)?.looking)!
            setGeneralDesign()
            setGeneralData()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //סידור התצוגה - כפתורים מעוגלים וכו'
    func setGeneralDesign(){
        btnDeleteJob.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
        btnDeleteJob.layer.borderWidth = 1
        btnDeleteJob.layer.cornerRadius = btnDeleteJob.frame.height / 2
        
        viewVideo.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewVideo.layer.borderWidth = 1
        viewVideo.layer.cornerRadius = viewVideo.frame.height / 2
        
        viewChat.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewChat.layer.borderWidth = 1
        viewChat.layer.cornerRadius = viewChat.frame.height / 2
        
        imgWorker.layer.cornerRadius = imgWorker.frame.height / 2
        imgWorker.layer.masksToBounds = true
        imgWorker.contentMode = UIViewContentMode.scaleAspectFill
        imgWorker.clipsToBounds = true
        
        Global.sharedInstance.setButtonCircle(sender: btnOK, isBorder: false)
        
        
        setTaps()
    }
    //סידור כל הנתונים כל המסך
    func setGeneralData(){
        titleStatus.text = "האם \(looking.nvWorkerName) הגיע?"
        imgWorker.downloadedFrom(link: api.sharedInstance.buldUrlFile(fileName: looking.nvImageFilePath) )
        nameWorker.text = looking.nvWorkerName
        idWorker.text = looking.nvWorkerIdentityNumber
        animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "map-small").image
        
        
    }
    
    
    //MARK: - Taps
    
    func setTaps(){
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(deleteJob))
        btnDeleteJob.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(chatAction))
        viewChat.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target:self, action:#selector(videoAction))
        viewVideo.addGestureRecognizer(tapGestureRecognizer3)
        
        let tapGestureRecognizer4 = UITapGestureRecognizer(target:self, action:#selector(showDetailsJobAction))
        btnShowJobDetails.addGestureRecognizer(tapGestureRecognizer4)
    }
    
    func deleteJob(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.deleteEmployerJobShowAlert()
        } else {
            // Fallback on earlier versions
        }
    }
    
    //פתיחת הצאט ביון העובד למעסיק
    func chatAction(){
        
    }
    
    //פתיחת שיחת וידאו בין עובד למעסיק
    func videoAction(){
        
    }
    
    //פתיחת פרטי משרה
    func showDetailsJobAction(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.getDetailsJob()
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    
    //MARK: - Server Function
    
    //מעדכנת מרחק לזהוי
    func UpdateWorkerIdentification(){

        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iEmployerJobId"] = looking.iEmployerJobId as AnyObject?
        dic["iWorkerOfferedJobId"] = looking.iWorkerOfferedJobId as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        dic["iTimeReportingRadius"] = distanceTitleTF.text as AnyObject
        
        
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if #available(iOS 11.0, *) {
                        (self.parent as? LookingForYouModelViewController)?.getEmployeJob()
                    } else {
                        // Fallback on earlier versions
                    }
                    
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerIdentification")
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
