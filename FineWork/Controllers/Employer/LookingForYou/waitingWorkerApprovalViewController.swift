//
//  waitingWorkerApprovalViewController.swift
//  FineWork
//
//  Created by User on 03/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class waitingWorkerApprovalViewController: UIViewController {

    
      //MARK: - Views
    
    @IBOutlet weak var animStatus: UIImageView!
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var statusTxt: UILabel!
    @IBOutlet weak var timeLeft: UILabel!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var viewChat: UIView!
    @IBOutlet weak var imgChat: UIImageView!
    @IBOutlet weak var btnCancelJob: UILabel!
    @IBOutlet weak var showDetailsJob: UILabel!
    
    //MARK: - Variable
    var looking = LookingForYouWorkers()
    
    var seconds = 1
    var secondTimer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            looking =  ((parent as? LookingForYouModelViewController)?.looking)!
        } else {
            // Fallback on earlier versions
        }
        initDisplay()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //סידור כל הנתונים כל המסך
    func initDisplay(){
        btnCancelJob.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
        btnCancelJob.layer.borderWidth = 1
        btnCancelJob.layer.cornerRadius = btnCancelJob.frame.height / 2
        
        viewVideo.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewVideo.layer.borderWidth = 1
        viewVideo.layer.cornerRadius = viewVideo.frame.height / 2
        
        viewChat.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewChat.layer.borderWidth = 1
        viewChat.layer.cornerRadius = viewChat.frame.height / 2
        
        animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "cafe-small").image
        
        setTimer()
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(deleteJob))
        btnCancelJob.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(chatAction))
        viewChat.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target:self, action:#selector(videoAction))
        viewVideo.addGestureRecognizer(tapGestureRecognizer3)
        
        let tapGestureRecognizer4 = UITapGestureRecognizer(target:self, action:#selector(showDetailsJobAction))
        showDetailsJob.addGestureRecognizer(tapGestureRecognizer4)
        
        
    }
    
    //הצגת הטימר של הזמן שנשאר עד תחילת המשרה
    func setTimer() {
        seconds = 1
        if secondTimer != nil{
            secondTimer?.invalidate()
        }
        secondTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
    }
    
    func updateUI() {
        seconds += 1
        if looking.dtWorkerCandidacyExpiration != "" {
            timeLeft.text = String(Global.sharedInstance.getHourDifferencesToday(serverDate: looking.dtWorkerCandidacyExpiration!))
        }
    }
    func deleteJob(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.deleteEmployerJobShowAlert()
        } else {
            // Fallback on earlier versions
        }
    }
    
    //פתיחת הצאט ביון העובד למעסיק
    func chatAction(){
        
    }
    
    //פתיחת שיחת וידאו בין עובד למעסיק
    func videoAction(){
        
    }
    
    //פתיחת פרטי משרה
    func showDetailsJobAction(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.getDetailsJob()
        } else {
            // Fallback on earlier versions
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
