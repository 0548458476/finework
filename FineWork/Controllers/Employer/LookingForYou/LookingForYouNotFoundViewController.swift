//
//  LookingForYouNotFoundViewController.swift
//  FineWork
//
//  Created by User on 17/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class LookingForYouNotFoundViewController: UIViewController {
//MARK: - Views
    @IBOutlet weak var animStatus: UIImageView!
    @IBOutlet weak var titleStatus: UILabel!
    @IBOutlet weak var txtStstus: UILabel!
    @IBOutlet weak var changeDetailsBtn: UILabel!
    @IBOutlet weak var newSearchBtn: UILabel!
    @IBOutlet weak var cancelJob: UILabel!
    @IBOutlet weak var showDetails: UILabel!
    
    //MARK: - Variable
    var looking = LookingForYouWorkers()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDisplay()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            looking =  ((parent as? LookingForYouModelViewController)?.looking)!
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    
    func initDisplay(){
        cancelJob.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
        cancelJob.layer.borderWidth = 1
        cancelJob.layer.cornerRadius = cancelJob.frame.height / 2
        
        changeDetailsBtn.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        changeDetailsBtn.layer.borderWidth = 1
        changeDetailsBtn.layer.cornerRadius = changeDetailsBtn.frame.height / 2
        
        newSearchBtn.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        newSearchBtn.layer.borderWidth = 1
        newSearchBtn.layer.cornerRadius = newSearchBtn.frame.height / 2
       

        animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "search-small").image
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(deleteJob))
        cancelJob.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(showDetailsJobAction))
        showDetails.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target:self, action:#selector(changeDetails))
        changeDetailsBtn.addGestureRecognizer(tapGestureRecognizer3)
        
        let tapGestureRecognizer4 = UITapGestureRecognizer(target:self, action:#selector(newSearch))
        newSearchBtn.addGestureRecognizer(tapGestureRecognizer4)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: - Taps
    
    func deleteJob(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.deleteEmployerJobShowAlert()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    
    func showDetailsJobAction(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.getDetailsJob()
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    func newSearch(){
        deleteJob()
        let storyEmployer = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        let   viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchWorkerModelViewController") as? SearchWorkerModelViewController
        viewCon?.sendData(isNewWorker: true,idJob: 0, isCopyJob: false)
        self.navigationController?.pushViewController(viewCon!, animated: true)
    }
    
    func changeDetails(){
        let storyEmployer = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        let   viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchWorkerModelViewController") as? SearchWorkerModelViewController
        viewCon?.sendData(isNewWorker: true,idJob: looking.iJobId, isCopyJob: false)
        self.navigationController?.pushViewController(viewCon!, animated: true)
    }
}
