//
//  DeleteJobViewController.swift
//  FineWork
//
//  Created by User on 26/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class DeleteJobViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{

     //MARK: - Views
    
    @IBOutlet weak var titleDeleteJob: UILabel!
    @IBOutlet weak var close_x: UILabel!
    @IBOutlet weak var resonTitle: UILabel!
    @IBOutlet weak var resonTF: UITextField!
    @IBOutlet weak var arrowIcon: UILabel!
    @IBOutlet weak var checkBok1: CheckBox!
    @IBOutlet weak var checkBox1Content: UILabel!
    @IBOutlet weak var checkBox2Content: UILabel!
    @IBOutlet weak var ckeckBox2: CheckBox!
    @IBOutlet weak var okBtn: UIButton!
   
    @IBOutlet weak var tblReson: UITableView!
    //MARK: - Action
    @IBAction func checkBok1Action(_ sender: Any) {
          checkBok1.isChecked = !checkBok1.isChecked

    }
    @IBAction func checkBok2Action(_ sender: Any) {
        ckeckBox2.isChecked = !ckeckBox2.isChecked

    }
    @IBAction func okAction(_ sender: Any) {
        if resonSelectedId != -1 && checkBok1.isChecked && ckeckBox2.isChecked {
            self.completion(resonSelectedId)
           
        } else {
            Alert.sharedInstance.showAlert(mess:  "חלק מהשדות חסרים")
        }

    }
    
    //MARK: - Variable
    var codeTables : Array<SysTable> = []
    var resonSelectedId : Int = -1
    var completion : (Int?) -> () = { t in }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getJobCancellationCodeTables()
        
       // arrowIcon.text = FontAwesome.sharedInstance.arrow
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DeleteJobViewController.setTblState))
        resonTF.addGestureRecognizer(tap)
        tblReson.isHidden = true
        arrowIcon.text = "\u{f107}"
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closePopUp))
        close_x.addGestureRecognizer(tap1)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func closePopUp(){
        self.dismiss(animated: true, completion: nil)

    }
    func setTblState()
    {
        tblReson.isHidden = !tblReson.isHidden
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return codeTables.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell") as! FamilyStatusTableViewCell
        cell.setDisplayData(txtStatus: codeTables[indexPath.row].nvName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resonTF.text = codeTables[indexPath.row].nvName
        resonSelectedId = codeTables[indexPath.row].iId
        setTblState()
        //tblBanks.isHidden = true
    }
    
    
    //MARK: - Server Function
    func getJobCancellationCodeTables() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
     
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> { // 3
                        
                        
                        var workerCodeTables : Array<SysTableDictionary> = []
                        let sysTable = SysTableDictionary()
                        
                        for workerCodeTableDic in dicResult { // 0
                            workerCodeTables.append(sysTable.getSysTableDictionaryFromDic(dic: workerCodeTableDic))
                        } // 0
                        
                        //Global.sharedInstance.workerCodeTables = workerCodeTables
                        for item in workerCodeTables { // 1
                            if item.Key == Global.sharedInstance.cancellationReasonType {
                                
                                self.codeTables = item.Value
                                self.tblReson.reloadData()
                                break
                            }
                        }
                    }
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetJobCancellationCodeTables")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
