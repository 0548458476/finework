//
//  ExtensionTimePopupViewController.swift
//  FineWork
//
//  Created by User on 15/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class ExtensionTimePopupViewController: UIViewController , SetDateTextFieldTextDelegate {
  //MARK: - Views
    @IBOutlet weak var titleTxt: UILabel!
    @IBOutlet weak var timeToChangeTF: UITextField!
    @IBOutlet weak var iconCalander: UILabel!
    @IBOutlet weak var btnOK: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    //MARK: - Action

    @IBAction func ok(_ sender: Any) {
        updateExpirationDate()
    }
   
    @IBAction func cancelPopup(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    //MARK: - Variable

    var looking = LookingForYouWorkers()
    let timeFormater = DateFormatter()
    var completion : () -> () = { }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        iconCalander.text = FlatIcons.sharedInstance.CALANDER
        Global.sharedInstance.setButtonCircle(sender: btnOK, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: btnCancel, isBorder: true)

        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(OpenDatePicker))
        timeToChangeTF.addGestureRecognizer(tap1)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        timeToChangeTF.text = Global.sharedInstance.getStringFromDateString(dateString:  looking.dtJobStartDate! )
        
      
        // Do any additional setup after loading the view.
    }

    func getData( looking : LookingForYouWorkers ){
        self.looking = looking
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //פתיחת הפיקר
    func OpenDatePicker(){
        let story = UIStoryboard(name: "Main", bundle: nil)
    let datePickerPopUp: DatePickerPopUpViewController = story.instantiateViewController(withIdentifier:"DatePickerPopUpViewController")as! DatePickerPopUpViewController
    datePickerPopUp.setDateTextFieldTextDelegate = self
    datePickerPopUp.modalPresentationStyle = UIModalPresentationStyle.custom
    datePickerPopUp.dateDefault = timeToChangeTF.text
        datePickerPopUp.tag = 10
    self.present(datePickerPopUp, animated: true, completion: nil)
        
        
    }
    
       //MARK: - delegates
    func SetDateFieldText(dateAsString : String, tagOfField : Int) {
      
      let timeChange =   Global.sharedInstance.getDateFromString(dateString: dateAsString)
        let endTime =   Global.sharedInstance.getDateFromString(dateString: timeToChangeTF.text!)
        if (timeChange as Date).weekdayOrdinal != 7 {
            if looking.iEmployerJobScheduleType != EmployerJobScheduleType.OneTimeJob.rawValue
                && timeChange.compare(endTime as Date) == ComparisonResult.orderedDescending {
                        Alert.sharedInstance.showAlert(mess: "אי אפשר להתחיל את המשרה אחרי תאריך סיום המשרה במקרה כזה מומלץ לחפש מחדש")
                 }
            timeToChangeTF.text = dateAsString
            Global.sharedInstance.bWereThereAnyChanges = true
        }else{
            Alert.sharedInstance.showAlert(mess:"לא ניתן לבחור תאריך החל בשבת!")

        }
        

    }
    
     //MARK: - Server Function
    func updateExpirationDate() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["dtJobStartDate"] =  Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: timeToChangeTF.text!)) as AnyObject?
        dic["iEmployerJobId"] = looking.iEmployerJobId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject

        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    self.dismiss(animated: true, completion: nil)

                self.completion()
                    //
//                    if #available(iOS 11.0, *) {
//                        (self.parent as? LookingForYouModelViewController)?.GetLookingForYouWorkersByEmployerJobId()
//                    } else {
//                        // Fallback on earlier versions
//                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
             Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerCandidacyExpirationDate")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
