//
//  LookingForYouModelViewController.swift
//  FineWork
//
//  Created by User on 03/11/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
@available(iOS 11.0, *)
class LookingForYouModelViewController: UIViewController , OpenAnotherPageDelegate, UIScrollViewDelegate, ChangeScrollViewHeightDelegate , BasePopUpActionsDelegate{
   

    //MARK: - Views
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var statusJobTitle: UILabel!
    @IBOutlet weak var timeJobEnd: UILabel!
    
    @IBOutlet weak var stepView: UIView!
    @IBOutlet weak var step5: UILabel!
    @IBOutlet weak var step4: UILabel!
    @IBOutlet weak var step3: UILabel!
    @IBOutlet weak var step2: UILabel!
    @IBOutlet weak var step1: UILabel!
    
    @IBOutlet weak var numJob: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var menuHightConstrain: NSLayoutConstraint!
    @IBOutlet weak var stepHightConstrain: NSLayoutConstraint!
    //MARK: - Variables
    var looking_start = LookingForYouStartViewController()
    var looking_notFound = LookingForYouNotFoundViewController()
    var looking_CandidateFound = CandidateFoundViewController()
    var looking_CandidateDetails = CandidateDetailsViewController()
    var looking_WorkerDetails = LookinkForWorkerDetailsViewController()
    var looking_WaitingWorkerApproval =  waitingWorkerApprovalViewController()
   var looking_WaitingToGetStarted =  WaitingToGetStartedViewController()
    var looking_WorkerIdentification =  WorkerIdentificationViewController()
    var looking_ActivePresence =  ActivePresenceViewController()
    var looking_SendYourOrder =  SendYourOrderViewController()
    var looking_EndWorker = EndWorkViewController()
    var looking_FoundCandidateAmount = FoundCandidateAmountViewController()
    var looking_RankWorker = RankWorkerViewController()

    
    
    var jobId = 0 , employerJobId = 0
    var statusJob = "" , role = ""
    var currentTab = 0
    var openCandidateDetailsFromList = false
    var employerJob = EmployerJob()
    var looking = LookingForYouWorkers()
    override func viewDidLoad() {
        super.viewDidLoad()

        Global.sharedInstance.notificationRefreshDelegate = self
        
        // Do any additional setup after loading the view.
        let story = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        looking_start = story.instantiateViewController(withIdentifier: "LookingForYouStartViewController") as! LookingForYouStartViewController
        looking_notFound = story.instantiateViewController(withIdentifier: "LookingForYouNotFoundViewController") as! LookingForYouNotFoundViewController

        if #available(iOS 11.0, *) {
            looking_CandidateFound = story.instantiateViewController(withIdentifier: "CandidateFoundViewController") as! CandidateFoundViewController
        } else {
            // Fallback on earlier versions
        }
        looking_CandidateDetails = story.instantiateViewController(withIdentifier: "CandidateDetailsViewController") as! CandidateDetailsViewController
        looking_WaitingWorkerApproval = story.instantiateViewController(withIdentifier: "waitingWorkerApprovalViewController") as! waitingWorkerApprovalViewController
        looking_WaitingToGetStarted = story.instantiateViewController(withIdentifier: "WaitingToGetStartedViewController") as! WaitingToGetStartedViewController
        looking_WorkerIdentification = story.instantiateViewController(withIdentifier: "WorkerIdentificationViewController") as! WorkerIdentificationViewController
        looking_ActivePresence = story.instantiateViewController(withIdentifier: "ActivePresenceViewController") as! ActivePresenceViewController
        looking_SendYourOrder = story.instantiateViewController(withIdentifier: "SendYourOrderViewController") as! SendYourOrderViewController
        looking_EndWorker = story.instantiateViewController(withIdentifier: "EndWorkViewController") as! EndWorkViewController
        looking_WorkerDetails = story.instantiateViewController(withIdentifier: "LookinkForWorkerDetailsViewController") as! LookinkForWorkerDetailsViewController
 looking_FoundCandidateAmount = story.instantiateViewController(withIdentifier: "FoundCandidateAmountViewController") as! FoundCandidateAmountViewController
        looking_RankWorker = story.instantiateViewController(withIdentifier: "RankWorkerViewController") as! RankWorkerViewController
        
      
    }
    override func viewDidLayoutSubviews() {
    }
    override func viewDidAppear(_ animated: Bool) {
          getEmployeJob()
    }
    func sendData(mJobID : Int , mEmployerJobId : Int, mStatusJob : String , mRole : String , mOpenCandidateDetailsFromList : Bool){
      jobId = mJobID
        statusJob = mStatusJob
        role = mRole
        employerJobId = mEmployerJobId
        openCandidateDetailsFromList = mOpenCandidateDetailsFromList
    }

    func displayDataByJob(){
        numJob.text = "מספר משרה: \(looking.iEmployerJobId)"
        statusJobTitle.text = looking.nvEmployerAskedJobStatusType
        timeJobEnd.text = String(Global.sharedInstance.getHourDifferencesToday(serverDate: looking.dtJobStartDate!))
       
    }
    func setStepSchema( position : Int) {
    
        setStepByStatus(step: step1, isFull: position >= 1)
        setStepByStatus(step: step2, isFull: position >= 2)
        setStepByStatus(step: step3, isFull: position >= 3)
        setStepByStatus(step: step4, isFull: position >= 4)
        setStepByStatus(step: step5, isFull: position >= 5)

    
    }

    func setStepByStatus(step : UILabel , isFull : Bool){
        if isFull {
        step.layer.borderColor = ControlsDesign.sharedInstance.colorBrown.cgColor
        step.layer.borderWidth = 1
        step.backgroundColor = ControlsDesign.sharedInstance.colorOrangLight
        }else{
            step.layer.borderColor = ControlsDesign.sharedInstance.colorWhiteGray.cgColor
            step.layer.borderWidth = 1
            step.backgroundColor = ControlsDesign.sharedInstance.colorWhiteGray
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        barContainerViewController.titleText = role
        
    }
    
    //MARK: - screens (tabs)
    
    //פתיחת מסך מחפשים עבורך
    func getLookingForYouStart() {
        scrollView.addSubview(looking_start.view)
        setScrollViewEnable(isEnable: false)
        setHeightToScroll(height: looking_start.view.frame.height)
        looking_start.changeScrollViewDelegate = self
        looking_start.openAnotherPageDelegate = self
        addChildViewController(looking_start)
  
    }
    //פתיחת מסך עדין לא מצאנו
    func getLookingNotFound(){
        scrollView.addSubview(looking_notFound.view)
        setScrollViewEnable(isEnable: false)
        setHeightToScroll(height: looking_notFound.view.frame.height)
        addChildViewController(looking_notFound)
    }
    //פתיחת מסך הפודיום עבור משרה כמותית
    func getFoundCandidateAmount(){
        scrollView.addSubview(looking_FoundCandidateAmount.view)
        setScrollViewEnable(isEnable: false)
        setHeightToScroll(height: looking_FoundCandidateAmount.view.frame.height)
        addChildViewController(looking_FoundCandidateAmount)
    }
    //פתיחת מסך הפודיום
    func getCandidateFound() {
        removeViews()
        scrollView.addSubview(looking_CandidateFound.view)
           setScrollViewEnable(isEnable: true)
    
        setHeightToScroll(height: looking_CandidateFound.view.frame.height + 50)
        looking_CandidateFound.view.frame.size.height = 2000
        addChildViewController(looking_CandidateFound)
        
    }
    
    //פתיחת מסך פרוט מועמד
    func getCandidateDetails(candidateFound : CandidateFound) {
        removeViews()

        scrollView.addSubview(looking_CandidateDetails.view)
        looking_CandidateDetails.sendData(candidateFound: candidateFound, isFromListJobs: openCandidateDetailsFromList)
        looking_CandidateDetails.setGeneralData()
        setScrollViewEnable(isEnable: true)
        setHeightToScroll(height: looking_CandidateDetails.view.frame.height)
        looking_CandidateDetails.view.frame.size.height = 2000

        addChildViewController(looking_CandidateDetails)
        
    }
    
    //פתיחת מסך פרוט משרה
    func getDetailsJob( ) {
        scrollView.addSubview(looking_WorkerDetails.view)
        looking_WorkerDetails.getDisplayData(looking: looking, employerJob: employerJob)
        looking_WorkerDetails.setDisplayData()
        setScrollViewEnable(isEnable: true)
        //setHeightToScroll(height: 1200)
        addChildViewController(looking_WorkerDetails)
      
    }
    
    
    //פתיחת מסך ממתין לאישור סופי
    func getWaitingWorkerApproval(){
        scrollView.addSubview(looking_WaitingWorkerApproval.view)
        setScrollViewEnable(isEnable: false)
        setHeightToScroll(height: looking_WaitingWorkerApproval.view.frame.height)
        addChildViewController(looking_WaitingWorkerApproval)
    }
    //פתיחת מסך סגרנו לך עובד מתאים
    func getWaitingToGetStarted(){
        scrollView.addSubview(looking_WaitingToGetStarted.view)
        setScrollViewEnable(isEnable: false)
        setHeightToScroll(height: looking_WaitingToGetStarted.view.frame.height)
        addChildViewController(looking_WaitingToGetStarted)
    }
    
    //פתיחת מסך ממתין לזהוי עובד
    func getWaitingForWorkerIdentification(){
        scrollView.addSubview(looking_WorkerIdentification.view)
        setScrollViewEnable(isEnable: true)
        setHeightToScroll(height: looking_WorkerIdentification.view.frame.height)
        addChildViewController(looking_WorkerIdentification)
    }
    
    
//פתיחת מסך נוכות פעילה
    func getActivePresence(){
        scrollView.addSubview(looking_ActivePresence.view)
        setScrollViewEnable(isEnable: true)
        setHeightToScroll(height: looking_ActivePresence.view.frame.height)
        addChildViewController(looking_ActivePresence)
    }
    
//פתיחת מסך שלחנו את הזמנתך
    func getSendYourOrder(){
        scrollView.addSubview(looking_SendYourOrder.view)
        setScrollViewEnable(isEnable: true)
        setHeightToScroll(height: looking_SendYourOrder.view.frame.height)
        addChildViewController(looking_SendYourOrder)
    }
    
    //פתיחת מסך סיום עבודתו של עובד
    func getEndJob(){
        scrollView.addSubview(looking_EndWorker.view)
        setScrollViewEnable(isEnable: true)
        setHeightToScroll(height: looking_EndWorker.view.frame.height)
        addChildViewController(looking_EndWorker)
    }
    
    //פתיחת מסך דרוג העובד עי המעסיק
    func getWaitingForRating(){
        scrollView.addSubview(looking_RankWorker.view)
        setScrollViewEnable(isEnable: true)
        setHeightToScroll(height: looking_RankWorker.view.frame.height)
        addChildViewController(looking_RankWorker)
    }
    
    func openInnerControlerByStatus(){
          scrollView.contentOffset = .zero
        removeViews()
        if openCandidateDetailsFromList {
            if looking.candidateFound != nil {
            menuHightConstrain.constant = 0
            stepHightConstrain.constant = 0
                menuView.isHidden = true
                stepView.isHidden = true
            getCandidateDetails(candidateFound: looking.candidateFound!)
            }
        }else{
        if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.LookingForYou.rawValue || looking.iEmployerAskedJobStatusType == EmployerJobStatusType.LookingForYouAmount.rawValue{
            currentTab = 0
            setStepSchema(position: 1)
            getLookingForYouStart()
        }
        else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.NotFoundYet.rawValue{
            currentTab = 0
            setStepSchema(position: 1)
            getLookingNotFound()
        } else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.FoundCandidateAmount.rawValue {
            currentTab = 6
            setStepSchema(position: 2)
            getFoundCandidateAmount()
        }
        else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.FirstCandidate.rawValue ||
            looking.iEmployerAskedJobStatusType == EmployerJobStatusType.SecondCandidate.rawValue ||
            looking.iEmployerAskedJobStatusType == EmployerJobStatusType.WaitingForContinueProcess.rawValue ||
            looking.iEmployerAskedJobStatusType == EmployerJobStatusType.ModificationOrExtension.rawValue
            {
                currentTab = 1
                setStepSchema(position: 2)
                getCandidateFound()
            }
        
        else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.AwaitingWorkerApproval.rawValue {
             currentTab = 2
            if looking.iEmployerJobWorkerType == EmployerJobWorkerType.KnownWorker.rawValue {
                setStepSchema(position: 3)
                getSendYourOrder()
            }else{
                setStepSchema(position: 3)
                getWaitingWorkerApproval()
            }
           
        } else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.WaitingToGetStarted.rawValue {
            currentTab = 3
            setStepSchema(position: 3)
            getWaitingToGetStarted()
        }
        else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.WaitingForWorkerIdentification.rawValue {
            currentTab = 4
            setStepSchema(position: 3)
            getWaitingForWorkerIdentification()
        }
        else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.ActivePresence.rawValue {
            currentTab = 5
            if looking.iEmployerJobWorkerType == EmployerJobWorkerType.KnownWorker.rawValue && looking.bTimeReportingFlexibility == nil{
                setStepSchema(position: 3)
                getSendYourOrder()
            }else{
                setStepSchema(position: 4)
                getActivePresence()
            }
           
        }
        else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.New.rawValue && looking.iEmployerJobWorkerType == EmployerJobWorkerType.KnownWorker.rawValue{
            currentTab = 6
            setStepSchema(position: 3)
            getSendYourOrder()
        }
        else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.EndJob.rawValue {
            currentTab = 7
            setStepSchema(position: 5)
            getEndJob()
      }
            else if looking.iEmployerAskedJobStatusType == EmployerJobStatusType.WaitingForRating.rawValue {
                currentTab = 8
                setStepSchema(position: 5)
                getWaitingForRating()
            }
        }
        
    }
    func removeViews() {
        // -- remove pages
        looking_start.view.removeFromSuperview()
        looking_EndWorker.view.removeFromSuperview()
        looking_SendYourOrder.view.removeFromSuperview()
        looking_WorkerDetails.view.removeFromSuperview()
        looking_ActivePresence.view.removeFromSuperview()
        looking_CandidateFound.view.removeFromSuperview()
        looking_CandidateDetails.view.removeFromSuperview()
        looking_WaitingWorkerApproval.view.removeFromSuperview()
        looking_WaitingToGetStarted.view.removeFromSuperview()
        looking_WorkerIdentification.view.removeFromSuperview()

    }

    func changeScrollViewHeight(heightToadd : CGFloat) {
        //   scrollView.contentSize = CGSize(width: scrollView.frame.width, height: scrollView.contentSize.height + heightToadd)
        setScrollViewEnable(isEnable: true)
        scrollView.contentSize.height += heightToadd
        switch currentTab {
        case 0:
            looking_start.view.frame.size.height += heightToadd
            //setConstrainToTitle(item: searchNewWorker_whatVC.titleLbl, toItem: searchNewWorker_whatVC.view)
            break
//        case 1:
//            searchNewWorker_whenVC.view.frame.size.height += heightToadd
//            setConstrainToTitle(item: searchNewWorker_whenVC.titleLbl, toItem: searchNewWorker_whenVC.view)
//            break
//        case 2:
//            break
//        case 3:
//            break
//        case 4:
//            searchNewWorker_jobDescriptionVC.view.frame.size.height += heightToadd
//            setConstrainToTitle(item: searchNewWorker_jobDescriptionVC.titleLbl, toItem: searchNewWorker_jobDescriptionVC.view)
//            break
        default:
            break
        }
    }

    func setScrollViewEnable(isEnable: Bool) {
        scrollView.isScrollEnabled = isEnable
    }
    
    func scrollTop() {
        scrollView.contentOffset = .zero
    }
    
    func setHeightToScroll(height : CGFloat) {
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: menuView.frame.height + stepView.frame.height + height)
    }
    
    func deleteEmployerJobShowAlert(){
        if looking.iEmployerAskedJobStatusType ==  EmployerJobStatusType.WaitingToGetStarted.rawValue ||
            looking.iEmployerAskedJobStatusType ==  EmployerJobStatusType.ActivePresence.rawValue ||
            looking.iEmployerAskedJobStatusType ==  EmployerJobStatusType.WaitingForWorkerIdentification.rawValue
            {
                let story = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
                let basePopUp = story.instantiateViewController(withIdentifier: "DeleteJobViewController") as! DeleteJobViewController
                //
                basePopUp.completion = { t in
                    basePopUp.dismiss(animated: true, completion: {
                        if t != nil {
                            let cancel = JobCancellation()
                            cancel.iEmployerJobWorkerType = self.employerJob.iEmployerJobWorkerType
                            cancel.iEmployerJobId = self.employerJob.iEmployerJobId
                            cancel.iJobId = self.looking.iJobId
                            cancel.iCancellationReasonType = t!
                            cancel.bOutOfSystemEmplymentPaymentApproved = true
                            cancel.bCancellationCostApproved = true
                            
                            self.DeleteEmployerJob(jobCancellation: cancel)
                        }
                    })
                }
                self.present(basePopUp, animated: true, completion: nil)

                
                
                ///
        }else {
            openBasePopUp()
        }
    }
    //MARK: - BasePopUpActionsDelegate
    func okAction(num: Int) {
        
        let cancel = JobCancellation()
        cancel.iEmployerJobId = employerJob.iEmployerJobId
        cancel.iJobId = looking.iJobId
        cancel.iEmployerJobWorkerType = employerJob.iEmployerJobWorkerType


        DeleteEmployerJob(jobCancellation: cancel)
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך לבטל את המשרה?"
        
        self.present(basePopUp, animated: true, completion: nil)

    }


    //MARK: - scroll view delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < CGPoint.zero.y {
            scrollView.contentOffset = CGPoint.zero
        }
        
    }
    //MARK: - Server Function
    func getEmployeJob(){
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iEmployerJobId"] = employerJobId as AnyObject?
        print(dic as Any)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            //            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject>  {
                        
                        self.employerJob = self.employerJob.getEmployerJobFromDic(dic: dicResult  )
                        self.GetLookingForYouWorkersByEmployerJobId()
                        
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetEmployerJob)
    }

    
    func GetLookingForYouWorkersByEmployerJobId() {
        print("GetLookingForYouWorkersByEmployerJobId")
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerJobId"] = employerJobId as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        self.looking = self.looking.getLookingForYouWorkersFromDic(dic: dicResult)
                         self.displayDataByJob()
                        self.openInnerControlerByStatus()
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetLookingForYouWorkersByEmployerJobId")
    }
  
    
    ////מעדכנת את המועמד הנבחר
    func UpdateCandidateFoundStatus(approved : Bool  , workerOfferedJobId : Int){
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
        dic["bContinueProcess"] = false as AnyObject?
        dic["bApproved"] = approved as AnyObject?
        dic["iWorkerOfferedJobId"] = workerOfferedJobId as AnyObject?
        dic["iEmployerJobId"] = employerJobId as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject

            api.sharedInstance.goServer(params : dic,success : {
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
              
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                        if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                            Alert.sharedInstance.showAlert(mess: " בחירה מצוינת! אנו נפנה אל המועמדים לסגירה סופית של הפרטים הקטנים.במידה והמועמד המוביל כבר אינו זמין עבורך נפנה למועמדים הבאים לפי מידת ההתאמה והדרוג שבחרת. אנא שים לב להודעות שתקבל והמשך התהליך ותענה בהקדם זכור יש עוד מעסיקים הרוצים את המועמדים הטובים ביותר.")
                            self.looking = self.looking.getLookingForYouWorkersFromDic(dic: dicResult)
                            self.displayDataByJob()
                            self.openInnerControlerByStatus()
                            
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : "UpdateCandidateFoundStatus")
    }
    //מבטל את המשרה
    func DeleteEmployerJob( jobCancellation : JobCancellation){
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["jobCancellation"] = jobCancellation.getDic() as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
//todo close
                //   Alert.sharedInstance.showAlert(mess: "נמחק בהצלחה!")
                    self.navigationController?.popViewController(animated: true)

                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "DeleteEmployerJob")
    }
}
;
@available(iOS 11.0, *)
extension LookingForYouModelViewController: NotificationRefreshDelegate   {
    
    func onNorificarionRefresh(userInfo: [AnyHashable : Any]) {
        
        let iNotiType = Int((userInfo["iNotificationType"] as! NSString).intValue)
        let iUserID = Int((userInfo["iUserId"] as! NSString).intValue)
        let iEmployerJobId = Int((userInfo["iEmployerJobId"] as! NSString).intValue)
        
        if iNotiType == 7{ //רפרוש סטטוס משרה למעסיק
            if iUserID == Global.sharedInstance.user.iUserId {
                if self.employerJobId == iEmployerJobId {
                    self.GetLookingForYouWorkersByEmployerJobId()
                }
            }
        }
    }
    
    
}
