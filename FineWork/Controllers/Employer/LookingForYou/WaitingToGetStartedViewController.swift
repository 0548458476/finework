//
//  WaitingToGetStartedViewController.swift
//  FineWork
//
//  Created by User on 03/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WaitingToGetStartedViewController: UIViewController {
    
    //MARK: - Views
    
    @IBOutlet weak var aminStatus: UIImageView!
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var imgWorker: UIImageView!
    @IBOutlet weak var amountToHouer: UILabel!
    @IBOutlet weak var timeBeginWork: UILabel!
    @IBOutlet weak var viewChat: UIView!
    @IBOutlet weak var imgChat: UIImageView!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var nameWorker: UILabel!
    @IBOutlet weak var btnDeleteJob: UILabel!
    @IBOutlet weak var showJobDetails: UILabel!
    
    //MARK: - Variable
    var looking = LookingForYouWorkers()
    var seconds = 1
    var secondTimer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            looking =  ((parent as? LookingForYouModelViewController)?.looking)!
        } else {
            // Fallback on earlier versions
        }
        setGeneralDesign()
        setGeneralData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //סידור התצוגה - כפתורים מעוגלים וכו'
    func setGeneralDesign(){
        btnDeleteJob.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
        btnDeleteJob.layer.borderWidth = 1
        btnDeleteJob.layer.cornerRadius = btnDeleteJob.frame.height / 2
        
        viewVideo.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewVideo.layer.borderWidth = 1
        viewVideo.layer.cornerRadius = viewVideo.frame.height / 2
        
        viewChat.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewChat.layer.borderWidth = 1
        viewChat.layer.cornerRadius = viewChat.frame.height / 2
        
        imgWorker.layer.cornerRadius = imgWorker.frame.height / 2
        imgWorker.layer.masksToBounds = true
        imgWorker.contentMode = UIViewContentMode.scaleAspectFill
        imgWorker.clipsToBounds = true
        
        setTaps()
    }
    //סידור כל הנתונים כל המסך
    func setGeneralData(){
        
        imgWorker.downloadedFrom(link: api.sharedInstance.buldUrlFile(fileName: looking.nvImageFilePath) )
        nameWorker.text = looking.nvWorkerName
        amountToHouer.text = "\(looking.mRecommandedHourlyWage) שח לשעה"
        aminStatus.image = Global.sharedInstance.addGifToImage(nameImage: "star-small").image
        
        setTimer()
        
    }
    
    //הצגת הטימר של הזמן שנשאר עד תחילת המשרה
    func setTimer() {
        seconds = 1
        if secondTimer != nil{
            secondTimer?.invalidate()
        }
        secondTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
    }
    
    func updateUI() {
        seconds += 1
        if looking.dtWorkerCandidacyExpiration != "" {
            timeBeginWork.text = "זמן תחילת העבודה בעוד: "+String(Global.sharedInstance.getHourDifferencesToday(serverDate: looking.dtWorkerCandidacyExpiration!))
            
        }
    }
    
    //MARK: - Taps
    
    func setTaps(){
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(deleteJob))
        btnDeleteJob.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(chatAction))
        viewChat.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer(target:self, action:#selector(videoAction))
        viewVideo.addGestureRecognizer(tapGestureRecognizer3)
        
        let tapGestureRecognizer4 = UITapGestureRecognizer(target:self, action:#selector(showDetailsJobAction))
        showJobDetails.addGestureRecognizer(tapGestureRecognizer4)
    }
    
    func deleteJob(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.deleteEmployerJobShowAlert()
        } else {
            // Fallback on earlier versions
        }
    }
    
    //פתיחת הצאט ביון העובד למעסיק
    func chatAction(){
        
    }
    
    //פתיחת שיחת וידאו בין עובד למעסיק
    func videoAction(){
        
    }
    
    //פתיחת פרטי משרה
    func showDetailsJobAction(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.getDetailsJob()
        } else {
            // Fallback on earlier versions
        }

    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
