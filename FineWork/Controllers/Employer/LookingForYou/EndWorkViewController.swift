//
//  EndWorkViewController.swift
//  FineWork
//
//  Created by User on 04/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EndWorkViewController: UIViewController {

      //MARK: - Views
    @IBOutlet weak var titleEndJob: UILabel!
    @IBOutlet weak var bonusTitle: UILabel!
    @IBOutlet weak var bonusBtn: UIButton!
    @IBOutlet weak var bonusTF: UITextField!
    @IBOutlet weak var bonusLine: UIView!
    @IBOutlet weak var txt1: UILabel!
    @IBOutlet weak var txt2: UILabel!
    @IBOutlet weak var txt3: UILabel!
    @IBOutlet weak var feadbackTxt: UILabel!
    @IBOutlet weak var feadbackTF: UITextField!
    
    @IBOutlet weak var sendBtn: UILabel!
    @IBOutlet weak var showDetails: UILabel!
    
    @IBOutlet weak var txt1TopConstrain: NSLayoutConstraint!
    //MARK: - Action
    @IBAction func bonusOkAction(_ sender: Any) {
        if bonusTF.isHidden{
            bonusTF.isHidden = false
            bonusLine.isHidden = false
            txt1TopConstrain.constant = 70
        }else{
            bonusTF.isHidden = true
            bonusLine.isHidden = true
            txt1TopConstrain.constant = 20
        }
    }
    
    
    //MARK: - Variable
    var looking = LookingForYouWorkers()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            looking =  ((parent as? LookingForYouModelViewController)?.looking)!
        } else {
            // Fallback on earlier versions
        }
        setGeneralDesign()
       
        setGeneralData()
    }
    //תצוגת הנתונים על המסך
    func setGeneralData(){
        titleEndJob.text = "עבודתו של \(looking.nvWorkerName) הסתימה"
    }
    
    //סידור התצוגה - כפתורים מעוגלים וכו'
    func setGeneralDesign(){
        sendBtn.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        sendBtn.layer.borderWidth = 1
        sendBtn.layer.cornerRadius = sendBtn.frame.height / 2
        
        Global.sharedInstance.setButtonCircle(sender: bonusBtn, isBorder: false)

       
        
        setTaps()
    }
    
    //MARK: Taps
    
    func  setTaps(){
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showDetailsAction))
        showDetails.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sendAction))
        sendBtn.addGestureRecognizer(tap2)
       
    }
    //הצגת פרוט המשרה
    func showDetailsAction(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.getDetailsJob()
        } else {
            // Fallback on earlier versions
        }
    }
    
    //אישור
    func sendAction(){
        updateEndJob()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
     //MARK: - Server Function

    func updateEndJob(){
      
            if !bonusTF.isHidden && bonusTF.text == "" {
                Alert.sharedInstance.showAlert(mess: "נא מלא בונוס")

            }else{
                var bonus = 0.0
                if bonusTF.text != "" {
                    bonus = Double(bonusTF.text!)!
                }
          
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iJobId"] = looking.iJobId as AnyObject?
        dic["mBonusAmount"] = bonus as AnyObject?
        dic["nvFeedback"] = feadbackTF.text as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject

        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in

            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)

                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                   self.navigationController?.popViewController(animated: true)
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateEndJob")
            }}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
