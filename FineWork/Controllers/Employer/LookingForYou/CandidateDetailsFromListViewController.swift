//
//  CandidateDetailsFromListViewController.swift
//  FineWork
//
//  Created by User on 14/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class CandidateDetailsFromListViewController: UIViewController {
    
     //MARK: - Variables
    var looking_CandidateDetails = CandidateDetailsViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        let story = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
     
        looking_CandidateDetails = story.instantiateViewController(withIdentifier: "CandidateDetailsViewController") as! CandidateDetailsViewController 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
