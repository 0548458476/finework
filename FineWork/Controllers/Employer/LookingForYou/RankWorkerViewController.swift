//
//  RankWorkerViewController.swift
//  FineWork
//
//  Created by User on 17/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class RankWorkerViewController: UIViewController {

    //MARK: - Views
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var title3: UILabel!
    @IBOutlet weak var lblSlider1: UILabel!
    @IBOutlet weak var slider1: UISlider!
    @IBOutlet weak var lblSlider2: UILabel!
    @IBOutlet weak var slider2: UISlider!
    @IBOutlet weak var lblSlider3: UILabel!
    @IBOutlet weak var slider3: UISlider!
    @IBOutlet weak var lblSlider4: UILabel!
    @IBOutlet weak var slider4: UISlider!
    @IBOutlet weak var freeTextTitle: UILabel!
    @IBOutlet weak var freeTextFeild: UITextField!
    @IBOutlet weak var titleAginWork: UILabel!
    @IBOutlet weak var aginWorkSeg: UISegmentedControl!
    @IBOutlet weak var sendBtnTxt: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var showDetails: UILabel!
    
    //MARK: - Action
    
    @IBAction func btnSendAction(_ sender: Any) {
        if aginWorkSeg.selectedSegmentIndex == 0 || aginWorkSeg.selectedSegmentIndex == 1 || aginWorkSeg.selectedSegmentIndex == 2{
            
            
            var id :Int = 0
            if Global.sharedInstance.user.iUserType.rawValue == UsetType.Employee.rawValue {
                id = RankingType.RankByWorker.rawValue
                
            }else{
                id = RankingType.RankByEmployer.rawValue
            }
            var b :Bool? = nil
            if aginWorkSeg.selectedSegmentIndex == 0 || aginWorkSeg.selectedSegmentIndex == 1 {
                b = true
            }else if aginWorkSeg.selectedSegmentIndex == 2 {
                b = false
            }
            
            let rankingMeasure = RankingMeasure(_iRankingType: id,_iJobId: Looking.iJobId,_iMeasureValue1: Int(slider1.value),_iMeasureValue2: Int(slider2.value),_iMeasureValue3: Int(slider3.value),_iMeasureValue4: Int(slider4.value),_nvRankingComment: freeTextFeild.text!,_bAgreeToWorkAgain: b)
            UpdateRankingMeasure(rank : rankingMeasure)
            
        } else{
            Alert.sharedInstance.showAlert(mess: "נא מלא את הפרטים הנדרשים!")
            
        }
    }
    @IBAction func sliderChenge(_ sender: UISlider) {
        setSliderValue(slider: sender)

    }
    
    //MARK: - Variable
    var Looking = LookingForYouWorkers()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            Looking =  ((parent as? LookingForYouModelViewController)?.looking)!
        } else {
            // Fallback on earlier versions
        }
      setDataOnUI()
    }
    override func viewDidLayoutSubviews() {
        
        aginWorkSeg.layer.cornerRadius = aginWorkSeg.frame.height / 2
        aginWorkSeg.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        aginWorkSeg.layer.borderWidth = 1
        aginWorkSeg.clipsToBounds = true
        
        sendBtn.backgroundColor = ControlsDesign.sharedInstance.colorOrang
        sendBtn.titleLabel!.font = UIFont(name: "FontAwesome", size: 17)!
        sendBtn.setTitle("\u{f1d9}", for: .normal)
        sendBtn.setTitleColor(UIColor.white, for: .normal)
        sendBtn.layer.cornerRadius = 0.5 * sendBtn.bounds.size.width
        
    }
    
   
    
    func setDataOnUI(){
        // txtFreeText.delegate = self
        
        slider1.maximumValue = 10
        slider1.minimumValue = 0
        
        slider2.maximumValue = 10
        slider2.minimumValue = 0
        
        slider3.maximumValue = 10
        slider3.minimumValue = 0
        
        slider4.maximumValue = 10
        slider4.minimumValue = 0
        
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(showDetailsJob))
        showDetails.addGestureRecognizer(tapGestureRecognizer1)
        
        
        self.title2.text = "היום עבד אצלך  \(Looking.nvRole) בשם \(Looking.nvWorkerName) לטובת כולנו נשמח אם תדרג אותו/ה ואת עבודתו/ה"
        self.title1.text = "שלום \(Global.sharedInstance.user.nvUserName)"
       
        
    }
    func setSliderValue(slider:UISlider)
    {
        let handleView = slider.subviews.last!
        
        var label:UILabel = UILabel()
        
        if (handleView.viewWithTag(1000) as? UILabel) != nil
        {
            label = (handleView.viewWithTag(1000)! as! UILabel)
            label.text = (Int(slider.value)).description
        }
        else
        {
            label = UILabel(frame: handleView.bounds)
            label.tag = 1000
            label.text = (Int(slider.value)).description
            label.backgroundColor = UIColor.clear
            label.textAlignment = .center
            handleView.addSubview(label)
        }
    }
    
    func showDetailsJob(){
        if #available(iOS 11.0, *) {
            (parent as? LookingForYouModelViewController)?.getDetailsJob()
            
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - function To Server
    
    func UpdateRankingMeasure(rank : RankingMeasure){
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        //RankingMeasure rankingMeasure, int iUserId
        dic["rankingMeasure"] = rank.getDicFromRankingMeasure()
            as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print("UpdateRankingMeasure \(responseObject as! [String:AnyObject])")
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                var _ = self.navigationController?.popViewController(animated: true)
                
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateRankingMeasure")
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
