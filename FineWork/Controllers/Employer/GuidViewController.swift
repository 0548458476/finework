//
//  GuidViewController.swift
//  FineWork
//
//  Created by User on 4.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class GuidViewController: UIViewController {

    //MARK: - Views
    @IBOutlet weak var myWebView: UIWebView!
    @IBOutlet weak var imgGuid: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBefore: UIButton!
    
    //MARK: - Actions
    @IBAction func btnBefore(_ sender: UIButton) {
        
        if indexList + 1 == listImagesGuideUrl.count {
            btnBefore.isHidden = false
            btnNext.setTitle("הבא", for: .normal)
        }
        
        
        if indexList - 1 >= 0{
            indexList -= 1
            if let url = URL(string: listImagesGuideUrl[indexList]) {
                myWebView.loadRequest(URLRequest(url: url))
                
            }
           if indexList == 0
            {
             btnBefore.isHidden = true

           }
        }
      
       
    }
    @IBAction func btnNext(_ sender: UIButton) {
        
        if indexList + 1 < listImagesGuideUrl.count {
            
            indexList += 1
            if let url = URL(string: listImagesGuideUrl[indexList]) {
                myWebView.loadRequest(URLRequest(url: url))
                
            }
//            let url : NSString = listImagesGuideUrl[indexList] as NSString
//            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
//            let searchURL : NSURL = NSURL(string: urlStr as String)!
//            let requestObj = NSURLRequest(url: searchURL as URL);
//            myWebView.loadRequest(requestObj as URLRequest)
            
            if indexList + 1 == listImagesGuideUrl.count {
                btnBefore.isHidden = false
                btnNext.setTitle("סיום", for: .normal)
            } else {
                btnBefore.isHidden = false
            }
        } else {
            var _ = self.navigationController?.popViewController(animated: true)
            var _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: - Variables
//     var bankAccountGuideImages:BankAccountGuide = BankAccountGuide()
    var listImagesGuideUrl:Array<String> = []
    var indexList = 0
    var barContainer = BarContainerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for item in
            GlobalEmpolyerDetails.sharedInstance.bankAccountGuideImages.lImages{
                listImagesGuideUrl.append(api.sharedInstance.buldUrlFile(fileName: item))
                
        }
        
//        let url : NSString = listImagesGuideUrl[indexList] as NSString
//        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
//        let searchURL : NSURL = NSURL(string: urlStr as String)!
//        let requestObj = NSURLRequest(url: searchURL as URL);
//        myWebView.loadRequest(requestObj as URLRequest)
        
        
//        myWebView.loadRequest(NSURLRequest(url: NSURL(string :listImagesGuideUrl[indexList]) as! URL) as URLRequest)
        if let url = URL(string: listImagesGuideUrl[indexList]) {
            myWebView.loadRequest(URLRequest(url: url))
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        Global.sharedInstance.setButtonCircle(sender: btnBefore, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: btnNext, isBorder: true)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        //        barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "הוראת קבע"
        
    }

    
}
