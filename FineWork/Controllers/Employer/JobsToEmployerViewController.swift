//
//  JobsToEmployerViewController.swift
//  FineWork
//
//  Created by User on 23/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class JobsToEmployerViewController: UIViewController , JobsToEmployeeDelegates{
  
    
    
     //MARK: - Variable
    var  listJob : Array<LookingForYouWorkers> = []
    var imageMap = Array<String>()
var counter = 0
    
       //MARK: - Views
    @IBOutlet var menuPlusView: UIView!
    @IBOutlet var plusBtn: UIButton!
    @IBOutlet var menuItemSerachNewWorkerBtn: UIButton!
    @IBOutlet var menuItemSearchKnownWorkerBtn: UIButton!
    @IBOutlet var menuItemSearchWorkerToExistJobBtn: UIButton!
    @IBOutlet var menuItemReservedJobsDatabaseBtn: UIButton!
    
    @IBOutlet weak var tblJobs: UITableView!
    
    @IBAction func plusBtnAction(_ sender: Any) {
        setMenuPlusState()
    }
    
    @IBAction func menuItemBtnAction(_ sender: UIButton) {
        setMenuPlusState()
        var viewCon : UIViewController?
        let storyEmployer = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        
        switch sender {
        case menuItemSerachNewWorkerBtn:
            viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchWorkerModelViewController") as? SearchWorkerModelViewController
            (viewCon as? SearchWorkerModelViewController)?.sendData(isNewWorker: true,idJob: 0, isCopyJob: false)
            break
        case menuItemSearchKnownWorkerBtn:
            viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchWorkerModelViewController") as? SearchWorkerModelViewController
            (viewCon as? SearchWorkerModelViewController)?.sendData(isNewWorker: false,idJob: 0, isCopyJob: false)
            break
        case menuItemSearchWorkerToExistJobBtn:
            viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchInJobViewController") as? SearchInJobViewController
            (viewCon as? SearchInJobViewController)?.sendData(bSave: false)
            break
        case menuItemReservedJobsDatabaseBtn:
              viewCon = storyEmployer.instantiateViewController(withIdentifier: "SearchInJobViewController") as? SearchInJobViewController
              (viewCon as? SearchInJobViewController)?.sendData(bSave: true)

            break
        default:
            break
        }
    
        if viewCon != nil {
            self.navigationController?.pushViewController(viewCon!, animated: true)
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        Global.sharedInstance.setButtonCircle(sender: plusBtn, isBorder: false)
        
        hideMenuWhenTappedAround()
        GetLookingForYouWorkers()
        //self.tblJobs.dataSource = self
       // self.tblJobs.delegate = self

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        menuPlusView.layer.borderWidth = 1
        menuPlusView.layer.borderColor = UIColor.myTurquoise.cgColor
        menuPlusView.layer.cornerRadius = menuPlusView.frame.width * 0.1
    }
    
    
    //MARK: - Menu Plus
    func setMenuPlusState() {
        self.view.bringSubview(toFront: menuPlusView)
        menuPlusView.isHidden = !menuPlusView.isHidden
    }
    
    // hide menu when tap arround
    func hideMenuWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissMenu))
        self.view.addGestureRecognizer(tap)
        //        tap.cancelsTouchesInView = false
    }
    
    func dismissMenu() {
        menuPlusView.isHidden = true
    }
    //MARK:
    func GetLookingForYouWorkers() {
        self.listJob = Global.sharedInstance.lLookingForYouWorkers
        if self.listJob.count > 0 {
            self.getImageWorker()
            
        }
        else{
            //no jobs
        }
    }
    
    
    // //MARK: - JobsToEmployee Delegates
    func openEmployerDatailsDel(index: IndexPath) {
        if listJob[index.section].candidateFound != nil {
            if #available(iOS 11.0, *) {
                
                let workerStory = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
                let vc = workerStory.instantiateViewController(withIdentifier: "LookingForYouModelViewController") as! LookingForYouModelViewController
                vc.sendData(mJobID: listJob[index.section].iJobId ,mEmployerJobId:listJob[index.section].iEmployerJobId, mStatusJob: listJob[index.section].nvEmployerAskedJobStatusType , mRole: listJob[index.section].nvRole , mOpenCandidateDetailsFromList: true)
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func openJobDatailsDel(index: IndexPath) {
        updateEmployerJobBaloonOccur(index: index, completion: { (isSuccess) in
            
            if isSuccess {
                print(index.count)
                if #available(iOS 11.0, *) {
                    
                    let workerStory = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
                    let vc = workerStory.instantiateViewController(withIdentifier: "LookingForYouModelViewController") as! LookingForYouModelViewController
                    vc.sendData(mJobID: self.listJob[index.section].iJobId ,mEmployerJobId:self.listJob[index.section].iEmployerJobId, mStatusJob: self.listJob[index.section].nvEmployerAskedJobStatusType , mRole: self.listJob[index.section].nvRole , mOpenCandidateDetailsFromList: false)
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    // Fallback on earlier versions
                }
            }
            
        })
        
    }
    
    func startFavoraiteDel(index: IndexPath) {
        
    }
    
    //עדכון לחיצה על הבועה
    func updateEmployerJobBaloonOccur (index: IndexPath, completion : @escaping (Bool) -> Void) {
        
        if Global.sharedInstance.lLookingForYouWorkers[index.section].iBaloonOccur > 0 {
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            
            dic["iEmployerJobId"] = Global.sharedInstance.lLookingForYouWorkers[index.section].iEmployerJobId as AnyObject?
            dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
            dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
            
            api.sharedInstance.goServer(params : dic,success : {
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] {
                    completion(true)
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    completion(false)
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : api.sharedInstance.UpdateEmployerJobBaloonOccur)
        }
        else{
            completion(true)
        }
    }
    
    //קבלת כל התמונות 
    func getImageWorker(){
        counter = 0
        downloadsCounter = 0
        imageMap.removeAll()
      
            
            for index in stride(from: 0, to: listJob.count, by: 1) {
                if listJob[index].nvImageFilePath != "" {
                    var isExist = false
                    //                    var index2 = 0;
                    for i in stride(from: 0, to: imageMap.count, by: 1) {
                        if listJob[index].nvImageFilePath == imageMap[i] {
                            isExist = true
                            //                            index2 = i
                            break
                        }
                    }
                    if !isExist {
                        imageMap.insert(listJob[index].nvImageFilePath, at: imageMap.count)
                        counter += 1
                        var stringPath = api.sharedInstance.buldUrlFile(fileName: (listJob[index].nvImageFilePath))
                        stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                        if let url = URL(string: stringPath) {
                            downloadImage(url: url, index: /*index*/imageMap.count-1)
                        }
                    }
                }
                
                //                item.logoImage.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
            }
        self.tblJobs.dataSource = self
        self.tblJobs.delegate = self
        self.tblJobs.reloadData()
   
    }
    var downloadsCounter = 0
    
    func downloadImage(url: URL, index: Int) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
          
                    for i in stride(from: 0, to: self.listJob.count, by: 1) {
                        if self.listJob[i].nvImageFilePath == self.imageMap[index] {
                            self.listJob[i].logoImage = image
                            }
                        }
                
                    
                    self.downloadsCounter += 1
                    
                    print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                    if self.downloadsCounter == self.counter {
                        self.tblJobs.reloadData()
                    }
                    
                    //                    if (self.isActiveJob && index == Global.sharedInstance.lWorkerOfferedJobs.count - 1) || (!self.isActiveJob && index == Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count - 1) {
                    //                        self.tblJobs.reloadData()
                    //                    }
                    //                            self.imgLogo.image = image
                    //                        }
                    //                    }
                }
            } else {
                self.downloadsCounter += 1
                
                print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                if self.downloadsCounter == self.counter {
                    self.tblJobs.reloadData()
                }
            }
            
            
            
        }
    }
}
extension JobsToEmployerViewController : UITableViewDelegate, UITableViewDataSource {
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        var count = 0
            count = self.listJob.count
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell : UITableViewCell

            cell = tableView.dequeueReusableCell(withIdentifier: "LookingForWorkersTableViewCell") as! LookingForWorkersTableViewCell
        (cell as! LookingForWorkersTableViewCell).setDisplayData(LookingForYouWorker: listJob[indexPath.section] , IndexPath: indexPath)
           (cell as! LookingForWorkersTableViewCell).jobsToEmployeeDelegates = self
            //            (cell as! ActiveJobsToEmployeeTableViewCell).imgLogo.image = nil
        return cell
    }

   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt")
    
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((self.view.frame.size.height * 0.77) - 3.5) / 7
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        return headerView
    }



}

