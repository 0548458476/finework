//
//  EmployerInvisibleProfileViewController.swift
//  FineWork
//
//  Created by Tamy wexelbom on 6.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import GooglePlaces

class EmployerInvisibleProfileViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - Views
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var companyNameTxt: UITextField!
    @IBOutlet weak var companyNumberTxt: UITextField!
    @IBOutlet weak var companyEmailTxt: UITextField!
    @IBOutlet weak var companyPhoneTxt: UITextField!
    @IBOutlet weak var companyAddressTxt: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    
    @IBOutlet weak var placesTbl: UITableView!
    
    //MARK: - Actions
    @IBAction func saveBtnAction(_ sender: Any) {
        // validate details and save them
        if validateFields() {
            saveEmployer()
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים/אינם תקינים")
        }
    }
    
    //MARK: - Variables
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    var placesClient : GMSPlacesClient! = nil
    var placesArr = Array<GMSAutocompletePrediction>()
    var placeSelected : GMSPlace? = nil
    var isChangePlace = false
    // delegates
    var saveEmployerDelegate : SaveEmployerDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.borderColor = saveBtn.backgroundColor?.cgColor
        saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
        
        companyNameTxt.delegate = self
        companyNumberTxt.delegate = self
        companyEmailTxt.delegate = self
        companyPhoneTxt.delegate = self
        companyAddressTxt.delegate = self
        
        companyNameTxt.tag = 1
        companyNumberTxt.tag = 1
        companyEmailTxt.tag = 1
        companyPhoneTxt.tag = 1
        
        companyAddressTxt.tag = 2  // google places
        
        companyNameTxt.keyboardType = .default
        companyNumberTxt.keyboardType = .numberPad
        companyEmailTxt.keyboardType = .emailAddress
        companyPhoneTxt.keyboardType = .phonePad
        companyAddressTxt.keyboardType = UIKeyboardType.default
        
        companyNameTxt.returnKeyType = .next
        companyNumberTxt.returnKeyType = .next
        companyEmailTxt.returnKeyType = .next
        companyPhoneTxt.returnKeyType = .next
        companyAddressTxt.returnKeyType = .done
        
        // google places
        placesClient = GMSPlacesClient.shared()
        companyAddressTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // table view
        placesTbl.delegate = self
        placesTbl.dataSource = self
        
        getEmployerCodeTables()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        companyNameTxt.text = Global.sharedInstance.employer.nvCompanyName
        companyNumberTxt.text = Global.sharedInstance.employer.nvBusinessIdentity
        companyEmailTxt.text = Global.sharedInstance.employer.nvMail
        companyPhoneTxt.text = Global.sharedInstance.employer.nvPhone
        
        companyAddressTxt.text = Global.sharedInstance.employer.nvAddress
        
        placeSelected = nil
        isChangePlace = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        setConstrainToTitle(item: titleLbl, toItem: self.view)
    }
    
    func setConstrainToTitle(item : Any, toItem : Any?) {
        let horizontalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 10)
        let widthConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.width, multiplier: 0.9, constant: 100)
        
        (toItem as! UIView).addConstraints([horizontalConstraint, verticalConstraint, widthConstraint])
    }
    
    
    //MARK: - Text Field Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        getTextFieldDelegate.getTextField(textField: textField, originY: 0)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case companyNameTxt:
            companyNumberTxt.becomeFirstResponder()
            break
        case companyNumberTxt:
            companyEmailTxt.becomeFirstResponder()
            break
        case companyEmailTxt:
            companyPhoneTxt.becomeFirstResponder()
            break
        case companyPhoneTxt:
            companyAddressTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case companyNameTxt:
            limitLength = 30
            break
        case companyNumberTxt:
            limitLength = 10
            break
        case companyPhoneTxt:
            limitLength = 10
            break
        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
        
    }
    
    // MARK: - Google Places
    func textFieldDidChange(_ textField: UITextField) {
        if textField.text != "" {
            placeAutocomplete(stringToSearch: textField.text!)
            isChangePlace = true
        } else {
            placesTbl.isHidden = true
        }
    }
    
    func placeAutocomplete(stringToSearch : String) {
        
//        let filter = GMSAutocompleteFilter()
//        filter.type = .address
        
        placesClient.autocompleteQuery(/*"Sydney Oper"*/stringToSearch, bounds: nil, filter: /*filter*/nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.placesArr = results
                self.placesTbl.isHidden = false
                self.placesTbl.reloadData()
                for result in results {
                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
            }
        })
    }
    
    func getPlaceByID(placeId : String) {
        //let placeID = "ChIJV4k8_9UodTERU5KXbkYpSYs"
        
        placesClient.lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeId)")
                return
            }
            
            //print("Place name \(place.name)")
            //print("Place address \(place.formattedAddress)")
            //print("Place placeID \(place.placeID)")
            //print("Place attributions \(place.attributions)")
            self.placeSelected = place
            //            self.addMarkerAndCircle(position: place.coordinate, radius: 10 * 1000)
            
        })
        
    }
    
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numOfRowsInSection = 0
        numOfRowsInSection = placesArr.count
        
        
        return numOfRowsInSection
    }
    //
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        var numOfRowsInSection = 0
    //            numOfRowsInSection = placesArr.count
    //
    //
    //        return numOfRowsInSection
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell
        
        //numOfRowsInSection = placesArr.count
        cell = placesTbl.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell") as! FamilyStatusTableViewCell
        (cell as! FamilyStatusTableViewCell).lblStatus.text = placesArr[indexPath.row].attributedFullText.string
        
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     var rowHeight = 0.0
     
     rowHeight = 40.0
     
     return CGFloat(rowHeight)
     }*/
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //
    //        return 10/*0.001*/
    //    }
    //
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let headerView = UIView()
    //        headerView.backgroundColor = UIColor.clear
    //        return headerView
    //    }
    //
    //    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //        return 0.001
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        companyAddressTxt.text = placesArr[indexPath.row].attributedFullText.string
        getPlaceByID(placeId: placesArr[indexPath.row].placeID!)
        
        placesTbl.isHidden = true
        
        Global.sharedInstance.bWereThereAnyChanges = true
        //            getPlaceByID(placeId: placesArr[indexPath.row].placeID!)
        
    }
    
    //MARK: - Server Function
    func getEmployerCodeTables() {
        if Global.sharedInstance.employerCodeTables.count > 0{
//            for item in Global.sharedInstance.workerCodeTables { // 1
//                if item.Key == Global.sharedInstance.bankCodeTable {
//                    cell.banksArr = item.Value
//                }
//            } // 1
            
        } else {
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            
            dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
            
            api.sharedInstance.goServer(params : dic,success : { // 8
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] { // 7
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 { // 5
                        if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> { // 3
                            
                            var employerCodeTables : Array<SysTableDictionary> = []
                            let sysTable = SysTableDictionary()
                            
                            for workerCodeTableDic in dicResult { // 0
                                employerCodeTables.append(sysTable.getSysTableDictionaryFromDic(dic: workerCodeTableDic))
                            } // 0
                            
                            Global.sharedInstance.employerCodeTables = employerCodeTables
//                            for item in employerCodeTables { // 1
//                                if item.Key == Global.sharedInstance.bankCodeTable {
////                                    self.cell.banksArr = item.Value
//                                }
//                            }
//                            self.cell.tblBanks.reloadData()
//                            self.cell.setDisplayData()
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : api.sharedInstance.GetEmployerCodeTables) // 9
        }
    }
    
    
    func updateEmployer() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["employer"] = Global.sharedInstance.employer.getDictionaryFromEmployer() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            //            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        if dicResult {
                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                            if self.openAnotherPageDelegate != nil {
                        
                                self.openAnotherPageDelegate.openNextTab!(tag: 1)
                            }
                        } else {
                            Alert.sharedInstance.showAlert(mess: "ארעה בעיה בשמירת הפרטים!")
                        }
//                        self.openFirstPage(isEmployer: true)
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
                
                //if dic["Error"]?["iErrorCode"] as! Int ==  2
                //{
                //    Alert.sharedInstance.showAlert(mess: "מייל לא קיים במערכת")
                //}
                //else
                //{
                //    Alert.sharedInstance.showAlert(mess: "נשלח אליך מייל עם הסיסמה")
                //}
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateEmployer)
    }
    
    //MARK: - Validate fields
    func validateFields() -> Bool {
        var isValid = true
        
        if companyNameTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: companyNameTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: companyNameTxt)
        }
        
        if companyNumberTxt.text == "" {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: companyNumberTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: companyNumberTxt)
        }

        
        if companyEmailTxt.text == "" || !Validation.sharedInstance.isValidEmail(testStr: companyEmailTxt.text!) {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: companyEmailTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: companyEmailTxt)
        }

        if companyPhoneTxt.text == "" || (!Validation.sharedInstance.cellPhoneValidation(string: companyPhoneTxt.text!) && !Validation.sharedInstance.phoneValidation(string: companyPhoneTxt.text!)) {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: companyPhoneTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: companyPhoneTxt)
        }
        
        return isValid
    }
    
    func saveEmployer() {
        
        Global.sharedInstance.employer.nvCompanyName = companyNameTxt.text!
        Global.sharedInstance.employer.nvBusinessIdentity = companyNumberTxt.text!
        Global.sharedInstance.employer.nvMail = companyEmailTxt.text!
        Global.sharedInstance.employer.nvPhone = companyPhoneTxt.text!
        
        Global.sharedInstance.employer.nvAddress = companyAddressTxt.text!
        
        if placeSelected != nil && isChangePlace {
            Global.sharedInstance.employer.nvLat = (placeSelected?.coordinate.latitude.description)!
            Global.sharedInstance.employer.nvLng = (placeSelected?.coordinate.longitude.description)!
        }
        
        saveEmployerDelegate.saveEmployer!(goNextTab: 1)
//        updateEmployer()
    }
}
