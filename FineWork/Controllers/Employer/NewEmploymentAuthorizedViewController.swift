//
//  NewEmploymentAuthorizedViewController.swift
//  FineWork
//
//  Created by User on 23.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class NewEmploymentAuthorizedViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {

    //MARK: - Views
    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var mobileTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var maximumAmountTxt: UITextField!
    @IBOutlet weak var oneTimeBtn: CheckBox!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backgroundView: UIView!
    
    //MARK: - Actions
    @IBAction func oneTimeBtnAction(_ sender: Any) {
        oneTimeBtn.isChecked = !oneTimeBtn.isChecked
    }
    @IBAction func okBtnAction(_ sender: Any) {
        
        if validateFields() {
            if authorizedEmploy != nil {
          //      authorizedEmploy?.iAuthorizedEmployId = 2
            } else {
                authorizedEmploy = AuthorizedEmploy()
                authorizedEmploy?.iEmployerFatherId = Global.sharedInstance.user.iEmployerId
                authorizedEmploy?.user.iEmployerId = 0
            }
            authorizedEmploy?.user.nvUserName = userNameTxt.text!
            authorizedEmploy?.user.nvMail = emailTxt.text!
            authorizedEmploy?.user.nvMobile = mobileTxt.text!
            authorizedEmploy?.user.nvPassword = passwordTxt.text!
            if maximumAmountTxt.text != "" {
             
                authorizedEmploy?.mAmount = Int(maximumAmountTxt.text!)
            }
            authorizedEmploy?.bOneTime = oneTimeBtn.isChecked
            
            
            updateAuthorizedEmploy(authorizedEmploy: authorizedEmploy!)
            
        } else {
           Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים/אינם תקינים")
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Variables
    var txtSelected = UITextField()
    var authorizedEmploy : AuthorizedEmploy? = nil
    var index : Int = -1
    //delegates
    var saveEmployerDelegate : SaveEmployerDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Global.sharedInstance.setButtonCircle(sender: okBtn, isBorder: false)
        Global.sharedInstance.setButtonCircle(sender: cancelBtn, isBorder: true)
        backgroundView.layer.borderWidth = 1
        backgroundView.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        
        hideKeyboardWhenTappedAround()
        
        userNameTxt.returnKeyType = .next
        emailTxt.returnKeyType = .next
        mobileTxt.returnKeyType = .next
        passwordTxt.returnKeyType = .next
        maximumAmountTxt.returnKeyType = .next
        
        userNameTxt.keyboardType = .default
        emailTxt.keyboardType = .emailAddress
        mobileTxt.keyboardType = .phonePad
        passwordTxt.keyboardType = .default
        maximumAmountTxt.keyboardType = .numberPad
        
        userNameTxt.delegate = self
        emailTxt.delegate = self
        mobileTxt.delegate = self
        passwordTxt.delegate = self
        maximumAmountTxt.delegate = self
        
        userNameTxt.tag = 1
        emailTxt.tag = 1
        mobileTxt.tag = 1
        passwordTxt.tag = 1
        maximumAmountTxt.tag = 1
        
        if authorizedEmploy != nil {
            userNameTxt.text = authorizedEmploy?.user.nvUserName
            emailTxt.text = authorizedEmploy?.user.nvMail
            mobileTxt.text = authorizedEmploy?.user.nvMobile
            passwordTxt.text = authorizedEmploy?.user.nvPassword
            
            if authorizedEmploy?.mAmount != nil {
                maximumAmountTxt.text = String(describing: authorizedEmploy!.mAmount!).description
            }
            
            oneTimeBtn.isChecked = (authorizedEmploy?.bOneTime)!
        } else {
            userNameTxt.text = ""
            emailTxt.text = ""
            mobileTxt.text = ""
            passwordTxt.text = ""
            maximumAmountTxt.text = ""
            
            oneTimeBtn.isChecked = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterKeyboardNotifications()
    }

    //MARK: - keyBoard notifications
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification)
    {
        if self.scrollView.contentOffset.y <= 0 {
            //Need to calculate keyboard exact size due to Apple suggestions
            self.scrollView.isScrollEnabled = true
            let info : NSDictionary = notification.userInfo! as NSDictionary
            let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
            let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            
            var aRect : CGRect = self.view.frame
            aRect.size.height -= keyboardSize!.height
            
            //        if txtSelected.tag == 1 {  // text field on a view
            
            var bRect : CGRect = txtSelected.frame
            
            bRect.origin.y += txtSelected.tag == 1 ? (txtSelected.superview?.frame.origin.y)! : txtSelected.frame.origin.y
            if (!aRect.contains(bRect)) {
                bRect.origin.y = bRect.origin.y - aRect.height  + bRect.height
                bRect.origin.x = 0
                self.scrollView.setContentOffset(bRect.origin, animated: true)
            }
            
        }
        
    }
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        //characteristicsTxt.resignFirstResponder()
        //studiesTxt.resignFirstResponder()
        //jobExperienceTxt.resignFirstResponder()
        //matchingJobsTxt.resignFirstResponder()
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        txtSelected = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case userNameTxt:
            emailTxt.becomeFirstResponder()
            break
        case emailTxt:
            mobileTxt.becomeFirstResponder()
            break
        case mobileTxt:
            passwordTxt.becomeFirstResponder()
            break
        case passwordTxt:
            maximumAmountTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case userNameTxt:
            limitLength = 30
            break
        case mobileTxt:
            limitLength = 10
            break
        case maximumAmountTxt:
            limitLength = 7
            break
        default:
            break
        }
        return newLength <= limitLength // Bool
        
    }
    
    //MARK: - Validation
    func validateFields() -> Bool {
        var isValid = true
        if !Validation.sharedInstance.nameValidation(string: userNameTxt.text!) {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: userNameTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: userNameTxt)
        }
        
        if !Validation.sharedInstance.isValidEmail(testStr: emailTxt.text!) {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: emailTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: emailTxt)
        }
        
        if !Validation.sharedInstance.cellPhoneValidation(string: mobileTxt.text!) {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: mobileTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: mobileTxt)
        }
        
        if !Validation.sharedInstance.isValidPasswordFinal(testStr: passwordTxt.text!) {
            isValid = false
            Global.sharedInstance.setErrorToTextField(sender: passwordTxt)
        } else {
            Global.sharedInstance.setValidToTextField(sender: passwordTxt)
        }
        
        return isValid
    }
    
    //MARK: - Server Functions
    func updateAuthorizedEmploy(authorizedEmploy : AuthorizedEmploy) {
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        //WorkerAskingLocation workerAskingLocation, int iUserId
        dic["authorizedEmploy"] = authorizedEmploy.getDictionaryFromAuthorizedEmploy() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        print(dic)
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String, AnyObject> {
                        self.unregisterKeyboardNotifications()
                        var authorized = AuthorizedEmploy()
                        authorized = authorized.getAuthorizedEmployFromDic(dic: dicResult)
                        
                      //  authorized.iAuthorizedEmployId = authorizedEmploy.iAuthorizedEmployId
                        
                      //  if authorized.iAuthorizedEmployId == 2 {  // update
                       // self.saveEmployerDelegate.saveAuthorizedEmploy!(authorizedEmploy: authorizedEmploy, indexInArray: self.index)
                      //  } else {                                  // add
                            self.saveEmployerDelegate.saveAuthorizedEmploy!(authorizedEmploy: authorized, indexInArray: self.index)
                      //  }
                        
//                        Alert.sharedInstance.showAlert(mess: "נשמר בהצלחה!")
                        self.dismiss(animated: true, completion: nil)
//                        if authorizedEmploy.bDeleted { // delete row
//                            self.authorizedEmploymentArr.remove(at: indexInArray)
//                            Alert.sharedInstance.showAlert(mess: "נמחק בהצלחה!")
//                        } else if authorizedEmploy.iAuthorizedEmployId > 0 {  // update row
//                            self.authorizedEmploymentArr.remove(at: indexInArray)
//                            self.authorizedEmploymentArr.insert(authorized, at: indexInArray)
//                            Alert.sharedInstance.showAlert(mess: "התעדכן בהצלחה!")
//                        } else { // add row
//                            self.authorizedEmploymentArr.insert(authorized, at: self.authorizedEmploymentArr.count)
//                            Alert.sharedInstance.showAlert(mess: "נוסף בהצלחה!")
//                            if self.authorizedEmploymentArr.count > 3 {
//                                self.changeScrollViewDelegate.changeScrollViewHeight(heightToadd: self.rowHeight + self.spaceBetweenRows)
//                            }
//                        }
//                        
//                        //                        self.resetData()
//                        self.employmentAuthorizedTbl.reloadData()
                        //self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        self.authorizedEmploy = nil
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                    self.authorizedEmploy = nil
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateAuthorizedEmploy)
    }
    
}
