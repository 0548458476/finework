//
//  CompanyPaymentDetailsViewController.swift
//  FineWork
//
//  Created by User on 19.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class CompanyPaymentDetailsViewController: UIViewController, UploadFilesDelegate, BasePopUpActionsDelegate {

    // Views
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var paymentOptionsSeg: UISegmentedControl!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var rememberMeLaterBtn: UIButton!
    
    // כרטיס אשראי
    @IBOutlet weak var creditCardView: UIView!
    @IBOutlet weak var cardNumberTxt: UITextField!
    @IBOutlet weak var changeCardBtn: UIButton!
    
    // הוראת קבע
    @IBOutlet weak var directDebitView: UIView!
    @IBOutlet weak var bankTxt: UITextField!
    @IBOutlet weak var statusTxt: UITextField!
    @IBOutlet weak var brunchTxt: UITextField!
    @IBOutlet weak var accountNumberTxt: UITextField!
    @IBOutlet weak var manualBtn: UIButton!
    @IBOutlet weak var uploadFileBtn: UIButton!
    @IBOutlet weak var removeFileBtn: UIButton!
    
    // constrains
    @IBOutlet weak var saveBtnHeightFromSegCon: NSLayoutConstraint!
    @IBOutlet weak var creditCardViewHeightFromSegCon: NSLayoutConstraint!
    @IBOutlet weak var directDebitHeightFromSegCon: NSLayoutConstraint!
    @IBOutlet var uploadFileHeigjtFromTopCon: NSLayoutConstraint!
    
    // Actions
    @IBAction func PaymentOptionsSegValueChanged(_ sender: Any) {
        
        setBehaviorByPaymentMethodType(paymentMethodType: paymentOptionsSeg.selectedSegmentIndex == 0 ? PaymentMethodType.CreditCard.rawValue : PaymentMethodType.DirectDebit.rawValue)
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func changeCardBtnAction(_ sender: Any) {
    }
    
    @IBAction func manualBtnAction(_ sender: Any) {
        openAnotherPageDelegate.openManual!()
//        openManual()
    }
    
    @IBAction func uploadFileBtnAction(_ sender: Any) {
        uploadFilesDelegate.uploadFile!(fileUrl: Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFileURL)
    }
    
    @IBAction func removeFileBtnAction(_ sender: Any) {
        openBasePopUp()
        
    }
    
    @IBAction func saveBtnAction(_ sender: Any) {
        if paymentOptionsSeg.selectedSegmentIndex == 0 || paymentOptionsSeg.selectedSegmentIndex == 1 {
            if paymentOptionsSeg.selectedSegmentIndex == 1 && Global.sharedInstance.employer.employerPaymentMethod.nvPaymentMethodStatusTypeName == "בוטל" && Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFile == "" && Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFileURL == "" {
                Alert.sharedInstance.showAlert(mess: "לא הועלה קובץ!")
            } else {
                saveData()
            }
        } else {
            Alert.sharedInstance.showAlert(mess: "יש לבחור אופציה לתשלום!")
        }
    }
    
    @IBAction func rememberMeLaterBtnAction(_ sender: Any) {
        openAnotherPageDelegate.openNextTab!(tag: 1)
    }
    
    //MARK: - Variables
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    var saveEmployerDelegate : SaveEmployerDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    var heightFromTopWithManual : CGFloat = 67, heightFromTopWithoutManual : CGFloat = 15
    var isRemoveFile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBehaviorByPaymentMethodType(paymentMethodType: Global.sharedInstance.employer.employerPaymentMethod.iPaymentMethodType)
        
        Global.sharedInstance.setButtonCircle(sender: changeCardBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: saveBtn, isBorder: true)
        Global.sharedInstance.setButtonCircle(sender: manualBtn, isBorder: false)
        Global.sharedInstance.setButtonCircle(sender: uploadFileBtn, isBorder: true)
        
        
        
        
        
//        let borderLayer = CAShapeLayer()
//        borderLayer.strokeColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
//        borderLayer.fillColor = UIColor.clear.cgColor
//        //        borderLayer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
//        borderLayer.borderWidth = 1
//        
//        let borderPath = UIBezierPath(roundedRect: paymentOptionsSeg.bounds,
//                                      byRoundingCorners: [  .allCorners], cornerRadii: CGSize(width: paymentOptionsSeg.frame.height/2, height: paymentOptionsSeg.frame.height/2))
//        borderLayer.path = borderPath.cgPath
//        
////        paymentOptionsSeg.layer.mask = borderLayer
//        
//        paymentOptionsSeg.layer.insertSublayer(borderLayer, at: 1)

        // Do any additional setup after loading the view.
        
        
        paymentOptionsSeg.layer.cornerRadius = paymentOptionsSeg.frame.height / 2
        paymentOptionsSeg.layer.borderColor = UIColor.orange2.cgColor
        paymentOptionsSeg.layer.borderWidth = 1
        paymentOptionsSeg.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isRemoveFile = false
        setData()
    }
    
    func setBehaviorByPaymentMethodType(paymentMethodType : Int) {
        switch paymentMethodType {
        case PaymentMethodType.CreditCard.rawValue:
            creditCardView.isHidden = false
            directDebitView.isHidden = true
            rememberMeLaterBtn.isHidden = true
            creditCardViewHeightFromSegCon.constant = 15
            saveBtnHeightFromSegCon.constant = creditCardView.frame.height + 10 + 15
            paymentOptionsSeg.selectedSegmentIndex = 0
            break
        case PaymentMethodType.DirectDebit.rawValue:
            creditCardView.isHidden = true
            directDebitView.isHidden = false
            rememberMeLaterBtn.isHidden = true
            directDebitHeightFromSegCon.constant = 15
            saveBtnHeightFromSegCon.constant = directDebitView.frame.height + 10 + 15
            paymentOptionsSeg.selectedSegmentIndex = 1
            break
        default:
            paymentOptionsSeg.selectedSegmentIndex = -1
            creditCardView.isHidden = true
            directDebitView.isHidden = true
            rememberMeLaterBtn.isHidden = false
            saveBtnHeightFromSegCon.constant = /*15*/45
            
            break
        }
    }

    //MARK: - UploadFilesdelegate
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFileName = imgName
        Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFile = img
        
        uploadFileBtn.setTitle(imgName, for: .normal)
        removeFileBtn.isHidden = false
        uploadFileBtn.setTitleColor(UIColor.lightGray, for: .normal)
        
        manualBtn.isHidden = true
        uploadFileHeigjtFromTopCon.constant = heightFromTopWithoutManual
        
        Global.sharedInstance.bWereThereAnyChanges = true
        isRemoveFile = false
    }
    
    func setData() {
        print("iPaymentMethodType: \(Global.sharedInstance.employer.employerPaymentMethod.iPaymentMethodType)")
        setBehaviorByPaymentMethodType(paymentMethodType: Global.sharedInstance.employer.employerPaymentMethod.iPaymentMethodType)
//        paymentOptionsSeg.selectedSegmentIndex = Global.sharedInstance.employer.employerPaymentMethod.iPaymentMethodType == PaymentMethodType.CreditCard.rawValue ? 1 : 0
        
        bankTxt.text = Global.sharedInstance.employer.employerPaymentMethod.nvBank
        statusTxt.text = Global.sharedInstance.employer.employerPaymentMethod.nvPaymentMethodStatusTypeName
        if Global.sharedInstance.employer.employerPaymentMethod.iBranchNumber != nil {
            brunchTxt.text = Global.sharedInstance.employer.employerPaymentMethod.iBranchNumber!.description
        }
        if Global.sharedInstance.employer.employerPaymentMethod.iAccountNumber != nil {
            accountNumberTxt.text = Global.sharedInstance.employer.employerPaymentMethod.iAccountNumber!.description
        }
        
        if Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFileURL != "" {
            uploadFileBtn.setTitle("צפיה בקובץ", for: .normal)
            removeFileBtn.isHidden = false
            
            manualBtn.isHidden = true
            uploadFileHeigjtFromTopCon.constant = heightFromTopWithoutManual
        } else {
            manualBtn.isHidden = false
            uploadFileHeigjtFromTopCon.constant = heightFromTopWithManual
        }
        
        cardNumberTxt.text = Global.sharedInstance.employer.employerPaymentMethod.nvCardLastFourDigits
        
    }
    
    func saveData() {
        Global.sharedInstance.employer.employerPaymentMethod.bBankAccountDetailsUpdated = true
        
        Global.sharedInstance.employer.employerPaymentMethod.iPaymentMethodType = paymentOptionsSeg.selectedSegmentIndex == 0 ? PaymentMethodType.CreditCard.rawValue : PaymentMethodType.DirectDebit.rawValue
        
//        if paymentOptionsSeg.selectedSegmentIndex == 1 {  // credit card
//            
//        } else {
//            
//        }
        saveEmployerDelegate.saveEmployer!(goNextTab: 3)
    }
    
    //MARK: - Delegates
    func openManual() {
        let story = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ChooseBankViewController") as! ChooseBankViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // BasePopUpActionsDelegate
    func okAction(num: Int) {
        isRemoveFile = true
        removeFileBtn.isHidden = true
        uploadFileBtn.setTitle("צרף קובץ", for: .normal)
        uploadFileBtn.setTitleColor(UIColor.orange2, for: .normal)
        Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFileURL = ""//worker.workerResumeDetails.resume.nvFileURL = ""
        Global.sharedInstance.employer.employerPaymentMethod.directDebit.nvFile = ""
        Global.sharedInstance.employer.employerPaymentMethod.directDebit.iFileId = -1
        
        manualBtn.isHidden = false
        uploadFileHeigjtFromTopCon.constant = heightFromTopWithManual
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך להסיר את הקובץ?"
        
        self.present(basePopUp, animated: true, completion: nil)
    }
}
