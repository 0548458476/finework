//
//  AfterRegisterViewController.swift
//  FineWork
//
//  Created by Tamy wexelbom on 20.2.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class AfterRegisterViewController: UIViewController {

    @IBOutlet weak var btnCompleteProfile: UIButton!
    
    @IBOutlet weak var btnNotNow: UIButton!
    
    @IBAction func actionCompleteProfile(_ sender: UIButton) {
        let viewCon = storyboard?.instantiateViewController(withIdentifier: "PersonalInformationModelViewController")as?PersonalInformationModelViewController
        self.navigationController?.pushViewController(viewCon!, animated: true)
    }

    @IBAction func actionNotNow(_ sender: UIButton) {
        let employeeModelViewController = (storyboard?.instantiateViewController(withIdentifier: "EmployeeModelViewController")as?EmployeeModelViewController)!
        self.navigationController?.pushViewController(employeeModelViewController, animated: true)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        lblTitle.text = "שלום \(Global.sharedInstance.user.nvUserName)"
        
        btnCompleteProfile.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnCompleteProfile.layer.borderWidth = 1
        btnCompleteProfile.layer.cornerRadius = btnCompleteProfile.frame.height/2
        
        btnNotNow.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        btnNotNow.layer.borderWidth = 1
        btnNotNow.layer.cornerRadius = btnNotNow.frame.height/2
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        lblTitle.text = "שלום \(Global.sharedInstance.user.nvUserName)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
