//
//  ReportPresenceViewController.swift
//  FineWork
//
//  Created by User on 19/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import CoreLocation
class ReportPresenceViewController: UIViewController , BasePopUpActionsDelegate , CLLocationManagerDelegate {
  
    
    
    //MARK: - Outlet
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewHeiggtConstrain: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var numberJob: UILabel!
    @IBOutlet weak var titleReporting: UILabel!
    @IBOutlet weak var imgCompeny: UIImageView!
    @IBOutlet weak var nameCompeny: UILabel!
    @IBOutlet weak var addressCompeny: UILabel!
    @IBOutlet weak var viewAddressCompeny: UIView!
    @IBOutlet weak var iconAddress: UILabel!
    @IBOutlet weak var reportingBetwen: UILabel!
    @IBOutlet weak var timeReportong: UILabel!
    @IBOutlet weak var viewBtnTimer: UIView!
    @IBOutlet weak var imgTimer: UIImageView!
    @IBOutlet weak var txtTimer: UILabel!
    @IBOutlet weak var txtTimerLast: UILabel!
    @IBOutlet weak var lineOnNumber: UILabel!
    @IBOutlet weak var pauseView: UIView!
    @IBOutlet weak var pauseImg: UIImageView!
    @IBOutlet weak var pauseTxt: UILabel!
    @IBOutlet weak var finishView: UIView!
    @IBOutlet weak var finishImg: UIImageView!
    @IBOutlet weak var finishTxt: UILabel!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var showDetails: UILabel!
    @IBOutlet weak var viewDetailsJob: UIView!
    
    //MARK: - Action
    
    @IBAction func chatAction(_ sender: Any) {
    }
    @IBAction func videoAction(_ sender: Any) {
    }
    
    
    //MARK: Object
    var workerOfferedJob : WorkerOfferedJob!
    var indexl = 0
    var seconds = 1
    var secondTimer : Timer?
    var timeToFinishJob : CLong = 0
    var isBrake = true
    let locationManager = CLLocationManager()
    var lat  = 0.0 , lng = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        Global.sharedInstance.notificationRefreshDelegate = self
        // Do any additional setup after loading the view.
        setData()
        JobTimeReportingCheckStatus()
        
    }
    
    override func viewDidLayoutSubviews() {
        
        //circel buttens
        finishView.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        finishView.layer.borderWidth = 1
        finishView.layer.cornerRadius = finishView.frame.height / 2
        
        pauseView.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        pauseView.layer.borderWidth = 1
        pauseView.layer.cornerRadius = pauseView.frame.height / 2
        
        btnChat.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        btnChat.layer.borderWidth = 1
        btnChat.layer.cornerRadius = btnChat.frame.height / 2
        
        btnVideo.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        btnVideo.layer.borderWidth = 1
        btnVideo.layer.cornerRadius = btnVideo.frame.height / 2
        
        //  Global.sharedInstance.setButtonCircle(sender: moreAction, isBorder: false)
        
        imgCompeny.layer.cornerRadius = imgCompeny.frame.height / 2
        imgCompeny.layer.masksToBounds = true
        imgCompeny.contentMode = UIViewContentMode.scaleAspectFill
        
        iconAddress.text = FlatIcons.sharedInstance.location
        
        viewAddressCompeny.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        viewAddressCompeny.layer.borderWidth = 1
        viewAddressCompeny.layer.cornerRadius = viewAddressCompeny.frame.height / 2
        
        // heightJobDetails = controller.baseView.frame.height
        
        let addressCompenyTap = UITapGestureRecognizer(target:self, action:#selector(self.showAddressCompeny))
        viewAddressCompeny.addGestureRecognizer(addressCompenyTap)
        
        
        let showDetailsTap = UITapGestureRecognizer(target:self, action:#selector(self.showDetailsJob))
        showDetails.addGestureRecognizer(showDetailsTap)
    }
    
    func setDisplayData(index: Int ){
        workerOfferedJob =  Global.sharedInstance.lWorkerOfferedJobs[index]
        print(workerOfferedJob.nvEmployerName)
        indexl = index
    }
    
    //הצגת הנתונים עג המסך
    func setData(){
        
        //add job deatils
        var controller = JobDetailsViewController()
        controller = storyboard?.instantiateViewController(withIdentifier: "JobDetailsViewController") as! JobDetailsViewController
        controller.workerOfferedJob = workerOfferedJob
        
        addChildViewController(controller)
        controller.view.frame = CGRect(x: 0, y: 0, width: 375 , height: 1000)
        controller.statusTitleView.isHidden = true
        controller.viewImgAndTitle.isHidden = true
        controller.viewAllDetails.isHidden = false
        controller.numJob.isHidden = true
        controller.viewAllDetailsTopConstrain.constant = -150
        viewDetailsJob.addSubview((controller.view)!)
        
        
        numberJob.text = "מספר משרה: \(workerOfferedJob.iEmployerJobId)/ \(workerOfferedJob.iWorkerOfferedJobId)"
        nameCompeny.text = workerOfferedJob.nvEmployerName
        addressCompeny.text =  workerOfferedJob.nvAddress
        imgCompeny.downloadedFrom(link: api.sharedInstance.buldUrlFile(fileName: workerOfferedJob.nvLogoImageFilePath) )
        timeReportong.text = "זמן המשמרת \(convertSecondToHouers(sum: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].nSumHour))"
        
        
        let timerTap = UITapGestureRecognizer(target:self, action:#selector(self.clickTimer))
        viewBtnTimer.addGestureRecognizer(timerTap)
        
        let pauseTap = UITapGestureRecognizer(target:self, action:#selector(self.clickPause))
        pauseView.addGestureRecognizer(pauseTap)
        
        let finishTap = UITapGestureRecognizer(target:self, action:#selector(self.clickFinish))
        finishView.addGestureRecognizer(finishTap)
        
        initLocation()
    }
    
 
   
    func  convertSecondToHouers(sum : Int)-> String {//second
        if sum != 0 {
            let  minit_all = sum/60 //minitall
            if minit_all != 0 {
                let hour = minit_all/60//hour
                let minit = minit_all%60 //minit
                var s=""
                if hour>1 {
                    s += "\(hour) שעות"
                }
                else if hour == 1 {
                    s = "\(s) \(hour) שעה"
                }
                if minit >= 1 && s != "" {
                    s = " \(s) ו "}
                if  minit > 1{
                    s = "\(s) \(minit) דקות"}
                else if minit==1 {
                    s = "\(s) דקה"}
                return  s
            }
        }
        return "";
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        barContainerViewController.titleText = "\(workerOfferedJob.nvDomain) - \(workerOfferedJob.nvRole)"
    }
    // MARK: - Taps
    
    //הצגת כתובת בית העסק
    func showAddressCompeny(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddressMapViewController") as! AddressMapViewController
        vc.setDisplayData(_lat: workerOfferedJob.nvLat , _lng: workerOfferedJob.nvLng , title: "\(workerOfferedJob.nvDomain) - \(workerOfferedJob.nvRole)")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //הצגת פרטי המשרה
    func showDetailsJob(){
        if viewDetailsJob.isHidden {
            viewDetailsJob.isHidden = false
            mainViewHeiggtConstrain.constant = mainView.frame.height + 1000
            
        }else{
            viewDetailsJob.isHidden = true
            mainViewHeiggtConstrain.constant = mainView.frame.height - 1000
            
            
        }
    }
    //לחיצה על הטימר
    func clickTimer(){
        changeStatus(status:  StepType.START.rawValue, isDefault: true)
    }
    
    
    func   changeStatus( status : Int,  isDefault : Bool) {
        if status == StepType.START.rawValue {
            if locationValidation(){
            if isDefault {
                AddTimeReporting( iJobId : workerOfferedJob.iJobId , iStepType : StepType.START.rawValue)
            } else {
                //                checkPresence.setOnCheckedChangeListener(null);
                //                checkPresence.setChecked(true);
                //                checkPresence.setOnCheckedChangeListener(this);
                GetStartDateJobTimeReporting(iJobId: workerOfferedJob.iJobId , isBreak: false);
            }
            }
            }else if status == StepType.START_SHIFT.rawValue {
              if locationValidation(){
            viewBtnTimer.isUserInteractionEnabled = true
            txtTimerLast.isHidden = true
            GetCloseShiftTime(status: StepType.START_SHIFT.rawValue)
            }
        }else if status == StepType.BETWEEN_SHIFTS.rawValue {
            
              if locationValidation(){
            txtTimerLast.isHidden = false
            GetCloseShiftTime(status: StepType.BETWEEN_SHIFTS.rawValue)
            viewBtnTimer.isUserInteractionEnabled = true
            }
        }
        else if status == StepType.CONTINU.rawValue {
              if locationValidation(){
            viewBtnTimer.isUserInteractionEnabled = true
            AddTimeReporting( iJobId : workerOfferedJob.iJobId , iStepType : StepType.CONTINU.rawValue)
            lineOnNumber.isHidden = true
            pauseImg.image = #imageLiteral(resourceName: "pause.png")
            pauseImg.tag = 1
            pauseTxt.text = "הפסקה"
            }
        }
        else if status == StepType.BREAK.rawValue {
            viewBtnTimer.isUserInteractionEnabled = false
            if isDefault{
                isBrake = true
                let story = UIStoryboard(name: "Main", bundle: nil)
                let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
                basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
                
                basePopUp.basePopUpActionDelegate = self
                basePopUp.text = "ההפסקה הינה ללא תשלום"
                
                
                self.present(basePopUp, animated: true, completion: nil)
                
            }else{
                pauseImg.image = #imageLiteral(resourceName: "play.png")
                pauseImg.tag = 0
                pauseTxt.text = "המשך"
                lineOnNumber.isHidden = false
                self.GetStartDateJobTimeReporting(iJobId : workerOfferedJob.iJobId, isBreak : true)
              
            }
        }else if status == StepType.END.rawValue {
              if locationValidation(){
            viewBtnTimer.isUserInteractionEnabled = false

            secondTimer?.invalidate()
          txtTimer.text = "העבודה הסתימה"
            AddTimeReporting( iJobId : workerOfferedJob.iJobId , iStepType : StepType.END.rawValue)
changeTimerBtnImg(iStepType: StepType.END.rawValue)
            }}
    }
    //משנה את תמונת הכפתור לפי המצב
    func changeTimerBtnImg(iStepType : Int){
        if iStepType == StepType.START.rawValue  {
            imgTimer.image = #imageLiteral(resourceName: "timer2.png")
        }else  if iStepType == StepType.END.rawValue  {
            imgTimer.image = #imageLiteral(resourceName: "timer3.png")
        }
    }
    func openTimer (time : Int){
        timeToFinishJob = time
        seconds = 1
        if secondTimer != nil {
            secondTimer?.invalidate()
        }
        secondTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
        
    }
    
    
    func updateUI() {
        seconds += 1
        if timeToFinishJob > 0 && secondTimer != nil {
            //            txtTimer.text = String(Global.sharedInstance.getHourDifferencesToday(serverDate: timeToFinishJob))
            txtTimer.text = displayMSOnTimer(time : timeToFinishJob )
            timeToFinishJob = timeToFinishJob-1
        }else {
            secondTimer?.invalidate()
            secondTimer = nil
            changeStatus(status: StepType.END.rawValue, isDefault: true)

        }
    }
    func displayMSOnTimer(time : CLong)->String{
        var mm = 0 , hh = 0 , ss = 0
        let  Minute = 60
        
        
        
        hh = time / Minute / Minute
        let   allMinute = time - (hh * Minute * Minute)
        mm = allMinute / Minute
        ss = allMinute % Minute
        
        return "\(String(format: "%02d", hh )):\(String(format: "%02d",mm)):\(String(format: "%02d", ss))"
    }
    //לחיצה על הפסקה
    func clickPause(){
        if pauseImg.tag == 1 {
            self.changeStatus(status: StepType.BREAK.rawValue, isDefault: true)
        }else{
            self.changeStatus(status: StepType.CONTINU.rawValue, isDefault: true)
            
        }
    }
    
    //לחיצה על סיום משמרת
    func clickFinish(){
        isBrake = false
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך לסיים?"
        
        
        self.present(basePopUp, animated: true, completion: nil)
        //   self.changeStatus(status: StepType.BREAK.rawValue, isDefault: true)
    }
    
    //MARK: - BasePopUpActionsDelegate
    func okAction(num: Int) {
        if isBrake {
        //לחיצה על אישור בהפסקה בלי תשלום
        secondTimer?.invalidate()
        secondTimer = nil
        AddTimeReporting( iJobId : workerOfferedJob.iJobId , iStepType : StepType.BREAK.rawValue)
        lineOnNumber.isHidden = false
        pauseImg.image = #imageLiteral(resourceName: "play.png")
        pauseImg.tag = 0
        pauseTxt.text = "המשך"
            
        }
        else{
            //לחיצה על אישור סיום עבודה
           changeStatus(status: StepType.END.rawValue, isDefault: true)

        }
    }
    
    
      // MARK: - Location
    func initLocation(){
        
        // Ask for Authorisation from the User.
        locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            // locationManager.allowsBackgroundLocationUpdates = true
            //locationManager.distanceFilter = CLLocationDistance(exactly: 5)!
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        lat = locValue.latitude
        lng = locValue.longitude
        
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    
    //בדיקה האם המרחק מהעבודה מאפשר דיווח נוכחות
    func locationValidation() -> Bool{
        if workerOfferedJob.iTimeReportingRadius != nil && workerOfferedJob.iTimeReportingRadius != 0{
        if lng > 0.0 && lat > 0.0 {
            let distanceInMeters = getDistanceInMeters()
            if  distanceInMeters > workerOfferedJob.iTimeReportingRadius! {
                Alert.sharedInstance.showAlert(mess: "מעוד \(distanceInMeters - workerOfferedJob.iTimeReportingRadius!) מטר תוכל לדווח נוכחות")
                viewBtnTimer.isUserInteractionEnabled = false
                return false
            }
          
        }else {
            Alert.sharedInstance.showAlert(mess: "עליך להפעיל את הנוכחות במכשיר כדי לדווח נוכחות")
            viewBtnTimer.isUserInteractionEnabled = false
            return false
            }
        }
        return true
    }
    
    //מחזיר מרחק בין 2 נקודות במטרים
    func getDistanceInMeters()->Int{
        let coordinate₀ = CLLocation(latitude: lat, longitude: lng)
        let coordinate₁ = CLLocation(latitude: Double(workerOfferedJob.nvLat)!, longitude: Double(workerOfferedJob.nvLng)!)
        
        let distanceInMeters = coordinate₀.distance(from: coordinate₁) // result is in meters
        return Int(distanceInMeters)
    }
    
    
    // MARK: - function to server
    func JobTimeReportingCheckStatus(){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobId"] = workerOfferedJob.iJobId as AnyObject?
        
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                if let dicResult = dic["Result"] as? Int {
                    
                    if dicResult == ReportingCheckStatus.REPORTING_ACTIVE.rawValue {
                        self.changeStatus(status: StepType.START.rawValue, isDefault: false)
                    } else if dicResult == ReportingCheckStatus.BREAK.rawValue {
                        self.changeStatus(status: StepType.BREAK.rawValue,isDefault: false /*false*/)
                    } else if dicResult == ReportingCheckStatus.BETWEEN_SHIFTS.rawValue {
                        self.changeStatus(status: StepType.BETWEEN_SHIFTS.rawValue, isDefault: false)
                        self.txtTimer.text = "לחץ כאן להתחלת הנוכחות"

                    } else if dicResult == ReportingCheckStatus.START_SHIFT.rawValue {
                        self.changeStatus(status: StepType.START_SHIFT.rawValue, isDefault: false)
                        self.txtTimer.text = "לחץ כאן להתחלת הנוכחות"
                    }
                    self.GetCloseShiftTime(status: dicResult)

                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "JobTimeReportingCheckStatus")
        
        
    }
    
    func AddTimeReporting(iJobId : Int , iStepType : Int){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobId"] = iJobId as AnyObject?
        dic["iStepType"] = iStepType as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["nvLat"] = iJobId as AnyObject?
        dic["nvLng"] = iJobId as AnyObject?
        dic["iTimeReportingType"] = 219 as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let r = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                if let dicResult = r as? Int {
                    if dicResult == 0{
                        if iStepType == StepType.START.rawValue || iStepType == StepType.CONTINU.rawValue {
                            self.GetStartDateJobTimeReporting(iJobId : iJobId, isBreak : false)
                        } else {
                            if (iStepType == StepType.END.rawValue) {
                                // GetWorkerOfferedJob(iJobId);
                            }
                        }}
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "AddTimeReporting")
        //
        
    }
    
    
    
    func GetStartDateJobTimeReporting(iJobId : Int , isBreak : Bool){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobId"] = iJobId as AnyObject?
        
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let r = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                if let dicResult = r as? Int {
                    if dicResult == 0 {
                        if dic["Result"] != nil {
                            if isBreak {
                                let str2 = self.displayMSOnTimer(time : dic["Result"] as! Int )
                                self.txtTimer.text = str2
                                
                            }else {
                                self.openTimer(time: dic["Result"] as! Int)
                            }
                        } else {
                            //                            beginTxt.setVisibility(View.VISIBLE);
                            //                            timerRl.setVisibility(View.GONE);
                            //                            checkPresence.setOnCheckedChangeListener(null);
                            //                            checkPresence.setChecked(false);
                            //                            checkPresence.setOnCheckedChangeListener(ReportPresenceFragment.this);
                        }
                        
                    }else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        
                    }
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetStartDateJobTimeReporting")
        //
        
    }
    //מחזיר את זמן המשמרת הקרובה
    func GetCloseShiftTime(status : Int ){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerOfferedJobId"] = workerOfferedJob.iWorkerOfferedJobId as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?

        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let r = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                ///
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                     
                       // Global.sharedInstance.lWorkerOfferedJobs.removeAll()
                        var shiftTime = ShiftTime()
                        shiftTime = shiftTime.getShiftTimeFromDic(dic: dicResult)
                        if status == StepType.BETWEEN_SHIFTS.rawValue {
                            self.txtTimerLast.text = "בצעת כבר \(shiftTime.nSumHour) שעות"
                            self.txtTimer.text = "המשמרת הבאה  \(Global.sharedInstance.getStringFromDateString(dateString: shiftTime.dFromDate)) \(shiftTime.tFromHoure)-\(shiftTime.tToHoure)"
                        }else if  status == StepType.START_SHIFT.rawValue {
                             self.txtTimer.text = " תחילת עבודה \(Global.sharedInstance.getStringFromDateString(dateString: shiftTime.dFromDate)) \(shiftTime.tFromHoure)-\(shiftTime.tToHoure)"
                        }
                        self.reportingBetwen.text = "ניתן לדווח נוכחות רק בין השעות \(shiftTime.tFromHoure)-\(shiftTime.tToHoure)"
                        
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
               

            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetCloseShiftTime")
        //
        
    }
    
    
    func GetWorkerOfferedJob(){
        //nt iJobId, int iLanguageId, int iWorkerOfferedJobId
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iWorkerOfferedJobId"] =   self.workerOfferedJob.iWorkerOfferedJobId as AnyObject
        
        print(dic)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                        if (dic["Result"] as? Dictionary<String,AnyObject>) != nil {
                                var offerdJob = WorkerOfferedJob()
                                offerdJob = offerdJob.getWorkerOfferedJobFromDic(dic: (dic["Result"] as? Dictionary<String,AnyObject>)!)
                                self.workerOfferedJob = offerdJob
                            Global.sharedInstance.lWorkerOfferedJobs[self.indexl] = offerdJob
                            
                            if self.workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.whiting_for_rank.rawValue  {
                                Global.sharedInstance.lWorkerOfferedJobs[self.indexl] = self.workerOfferedJob
                                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                                    let workerStory = UIStoryboard(name: "StoryboardGeneral", bundle: nil)
                                    let   vc = workerStory.instantiateViewController(withIdentifier: "HowWasTheJobViewController") as! HowWasTheJobViewController
                                vc.setDisplayData(index: self.indexl)
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                    
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont:  self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorkerOfferedJob")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
};
extension ReportPresenceViewController: NotificationRefreshDelegate   {
    func onNorificarionRefresh(userInfo: [AnyHashable : Any]) {
        let iNotiType = Int((userInfo["iNotificationType"] as! NSString).intValue)
        let iUserID = Int((userInfo["iUserId"] as! NSString).intValue)
        let iWorkerId = Int((userInfo["iWorkerOfferedJobId"] as! NSString).intValue)
        
        if iNotiType == 6{ //רפרוש סטטוס משרה לעובד
            if iUserID == Global.sharedInstance.user.iUserId {
                if self.workerOfferedJob.iWorkerOfferedJobId == iWorkerId {
                    GetWorkerOfferedJob()
                }
            }
        }
    }
    
    
}
