//
//  statusJobWhitCalanderViewController.swift
//  FineWork
//
//  Created by User on 06/09/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

protocol TimeAvailablityDelegates {
    func ChangeStatusDel(position: Int , checked :Bool)
    
    
}

class statusJobWhitCalanderViewController: UIViewController , BasePopUpActionsDelegate , TimeAvailablityDelegates , UITextFieldDelegate {
    
    // MARK : Outlet
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var viewBaseHeightConstain: NSLayoutConstraint!
    @IBOutlet weak var detailsJobView: UIView!
    @IBOutlet weak var dtailsJobHeiggtConstain: NSLayoutConstraint!
    @IBOutlet weak var titleBeforeCalander: UILabel!
    
    //view date with calander
    @IBOutlet weak var calanderView: UIView!
    @IBOutlet weak var arrowMonthNext: UILabel!
    @IBOutlet weak var arrowMonthPrev: UILabel!
    @IBOutlet weak var nameCurrentMonth: UILabel!
    @IBOutlet weak var selectAll: UILabel!
    @IBOutlet weak var titleDayFroCalander: UIView!
    @IBOutlet weak var collectionDate: JTAppleCalendarView!
    @IBOutlet weak var numberAvilabilty: UILabel!
    
    //view date without calander
    @IBOutlet weak var viewDateWithoutCalander: UIView!
    @IBOutlet weak var fromDate: UILabel!
    @IBOutlet weak var fromDateContent: UITextField!
    @IBOutlet weak var toDate: UILabel!
    @IBOutlet weak var toDateContent: UITextField!
    @IBOutlet weak var fromHoures: UILabel!
    @IBOutlet weak var fromHouresContent: UITextField!
    @IBOutlet weak var toHoures: UILabel!
    @IBOutlet weak var toHouresContent: UITextField!
    @IBOutlet weak var dateWithoutCalanderConstrainTop: NSLayoutConstraint!
    
    //data of job after dates
    @IBOutlet weak var dataJobView: UIView!
    @IBOutlet weak var txtData: UILabel!

    @IBOutlet weak var hourlyWage: UILabel!
    @IBOutlet weak var hourlyWageContent: UITextField!
    @IBOutlet weak var thresholdConditionsCheckBox: CheckBox!
    @IBOutlet weak var thresholdConditions: UILabel!
    @IBOutlet weak var detailsSalaryCosts: UILabel!
    @IBOutlet weak var detailsSalaryCostsLink: UIView!
    
    @IBOutlet weak var allAmountJob: UILabel!
    @IBOutlet weak var thresholdConditaonHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var dataJobViewTopConstrin: NSLayoutConstraint!
    @IBOutlet weak var dataJobViewHeightConstain: NSLayoutConstraint!
    
    //general buttens
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelCandidate: UILabel!
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var chatBtn: UIButton!
    
    //MARK : Action
    
    @IBAction func chatBtnAction(_ sender: Any) {
    }
    
    @IBAction func checkBoxAction(_ sender: Any) {
        thresholdConditionsCheckBox.isChecked = !thresholdConditionsCheckBox.isChecked

    }
    
    @IBAction func okBtnAction(_ sender: Any) {
        if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.new.rawValue {
            let  status = OfferJobStatusType.interesting.rawValue
            UpdateWorkerOfferedJobStatus(newStatus: status)
        }else if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.interesting.rawValue {
            if workerOfferedJob.iEmployerJobScheduleType != EmployerJobScheduleType.ConstantHoursJob.rawValue {
                //workerOfferedJob.lWorkerOfferedJobTimeAvailablity = getTimeAvailablityForCalander()
                //when is checked the data update
            }else {
                workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].bWorkerOfferedJobTimeAvailablity = true;
            }
            
            if (hourlyWageContent.text != "" && (thresholdConditionsCheckBox.isHidden  || thresholdConditionsCheckBox.isChecked)
                && ( workerOfferedJob.iEmployerJobScheduleType == EmployerJobScheduleType.ConstantHoursJob.rawValue || isTimeAvailablity())) {
                workerOfferedJob.mAskingHourlyWage = Float(hourlyWageContent.text!);
                //   setSumHour(workerOfferedJob.mAskingHourlyWage);
                workerOfferedJob.iOfferJobStatusType = OfferJobStatusType.apply.rawValue
                UpdateWorkerOfferedJob();
                
            } else {
                Alert.sharedInstance.showAlert(mess: "נא מלא את הפרטים הנדרשים!")
            }
        }
    }
    
    @IBAction func selsectAllDayInCalanderAction(_ sender: Any) {
        
       
            if isSelectAllDay == nil || isSelectAllDay == false {
                isSelectAllDay = true
            }else {
                isSelectAllDay = false
            }
            collectionDate.reloadData()
      
    }
    
    @IBAction func shareBtnAction(_ sender: Any) {
        
       GetLink()

    }
    
    //MARK : Varible
    var workerOfferedJob = WorkerOfferedJob()
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var numberOfRows = 6
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    let firstDayOfWeek: DaysOfWeek = .sunday
    var hasStrictBoundaries = true
    var monthSize: MonthSize? = nil
    var index : Int = 0
    var myDate: Date? = Date()
    var isSelectAllDay : Bool? = nil
    var heightBaseView : CGFloat = 0
    var allTimeJob = 0
    var workerChooseTimeAvailablity  = [0,0]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        heightBaseView = viewBase.frame.height

        
        //add job deatils
        let controller = storyboard?.instantiateViewController(withIdentifier: "JobDetailsViewController") as! JobDetailsViewController
        controller.workerOfferedJob = workerOfferedJob
        addChildViewController(controller)
        
        controller.view.frame = CGRect(x: 0, y: 0, width: Int(scrollView.frame.width), height: 900)
        self.detailsJobView.addSubview((controller.view)!)
        
        
        initCalander()
        
        updateUIBydata()
     
        hourlyWageContent.delegate = self
         hourlyWageContent.keyboardType = .numberPad
    }
    
    func setHeightToBaseView(height : CGFloat) {
        print("height 1  self: \(self.heightBaseView)  height to add : \(height)")
        self.heightBaseView =  self.heightBaseView + height
        print("height 2  self: \(self.heightBaseView)")
        
        scrollView.contentSize = CGSize(width: viewBase.frame.width, height:  self.heightBaseView)
        
            }
    
    override func viewDidLayoutSubviews() {
        //  setHeightToScroll(height : 0)
setHeightToBaseView(height: 0)
      //  scrollView.contentSize = CGSize(width: scrollView.frame.width, height: 1800)
        
        //circel buttens
        cancelCandidate.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
        cancelCandidate.layer.borderWidth = 1
        cancelCandidate.layer.cornerRadius = cancelCandidate.frame.height / 2
        
        Global.sharedInstance.setButtonCircle(sender: shareBtn, isBorder: false)
        
        shareBtn.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        shareBtn.layer.borderWidth = 1
        shareBtn.layer.cornerRadius = shareBtn.frame.height / 2
        
        chatBtn.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        chatBtn.layer.borderWidth = 1
        chatBtn.layer.cornerRadius = chatBtn.frame.height / 2
        
        Global.sharedInstance.setButtonCircle(sender: okBtn, isBorder: false)
        
    }
    
    
    func initCalander(){
        // Do any additional setup after loading the view.
        collectionDate.transform = CGAffineTransform(scaleX: -1, y: 1)
        //        getDetailsFromDicMonth(BShowFullSchedule: btnRadioButton.isSelected)
        //        getHolidays()
        collectionDate.calendarDataSource = self as? JTAppleCalendarViewDataSource
        collectionDate.calendarDelegate = self as? JTAppleCalendarViewDelegate
        
        collectionDate.minimumLineSpacing = 0
        
        collectionDate.scrollToMonthOfDate(Date())
        
        collectionDate.register(UINib(nibName: "PinkSectionHeaderView", bundle: Bundle.main),
                                forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                withReuseIdentifier: "PinkSectionHeaderView")
        
        self.collectionDate.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            //   self.setupViewsOfCalendar(from: visibleDates, isEndScroll: <#Bool#>)
            self.collectionDate.layer.borderWidth = 0.3
            
        }
    }
    
    func updateUIBydata(){
        
        
        if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.new.rawValue {
            titleBeforeCalander.isHidden = true
            selectAll.isHidden = true
            dataJobView.isHidden = true
            setHeightToBaseView(height : -dataJobViewHeightConstain.constant)
            dataJobViewHeightConstain.constant = 0
numberAvilabilty.isHidden = true
            cancelCandidate.text = "לא מעוניין"
            // dtailsJobHeiggtConstain.constant = 900
            //  titleBeforeConstainTop.constant = CGFloat(height)
            //dataJobView.frame.height
            //setHeightToScroll(height: CGFloat(2000))
        }else if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.interesting.rawValue {
numberAvilabilty.isHidden = false
            titleBeforeCalander.isHidden = false
            selectAll.isHidden = false
            cancelCandidate.text = "לא מעוניין"

            dataJobView.isHidden = false
            if workerOfferedJob.mExpectationsCostJob != nil && workerOfferedJob.mExpectationsCostJob! >= 0{
                txtData.text = txtData.text! + " המעסיק מחפש מתמודדים בשכר של \(workerOfferedJob.mExpectationsCostJob!) תן את הצעתך."
            }
            var mm = [0,0]
          
            for timeAvailablity  in  workerOfferedJob.lWorkerOfferedJobTimeAvailablity {
              
             var p =  getHourDifferences(stringHoursFormatHHmm1: timeAvailablity.tFromHoure, stringHoursFormatHHmm2: timeAvailablity.tToHoure)
                    mm[0] += p[0]
                    mm[1] += p[1]

             
            }
            
            //אם כל אורך המשרה פחות משעה מעגלים לאחד
            if mm[0] == 0 && mm[1] > 0{
                allTimeJob = 1

            }else{
            allTimeJob = mm[0]
            }
             dataJobViewHeightConstain.constant = 190
            setHeightToBaseView(height : dataJobViewHeightConstain.constant)
            numberAvilabilty.text = "התחיבת ל0 שעות מתוך  \(mm[0])"

            threshold()
            
            collectionDate.reloadData()
        }
        
        //set date in calander or without by iEmployerJobScheduleType
        if workerOfferedJob.iEmployerJobScheduleType != EmployerJobScheduleType.ConstantHoursJob.rawValue {
            if workerOfferedJob.iEmployerJobScheduleType == EmployerJobScheduleType.OneTimeJob.rawValue {
                selectAll.isHidden = true
            }else{
            
            }
            
            // initCalander()
            
            
        }else{
            viewDateWithoutCalander.isHidden = false
            calanderView.isHidden = true
            dataJobViewTopConstrin.constant = 140
            dateWithoutCalanderConstrainTop.constant = 0
            setHeightToBaseView(height : -viewDateWithoutCalander.frame.height)

            titleBeforeCalander.text = "מלא שכר נדרש"
            fromDateContent.text = Global.sharedInstance.getStringFromDateString(dateString: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].dFromDate)
            toDateContent.text = Global.sharedInstance.getStringFromDateString(dateString: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].dToDate!)
            fromHouresContent.text = workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].tFromHoure
            toHouresContent.text = workerOfferedJob.lWorkerOfferedJobTimeAvailablity[0].tToHoure
            
            
        }
        
        // add tap
        let showDetailsTap = UITapGestureRecognizer(target:self, action:#selector(self.cancelAction))
        cancelCandidate.addGestureRecognizer(showDetailsTap)
        
        let arrowNext = UITapGestureRecognizer(target:self, action:#selector(self.chengeNext))
        arrowMonthNext.addGestureRecognizer(arrowNext)

        let arrowPrev = UITapGestureRecognizer(target:self, action:#selector(self.chengePrev))
        arrowMonthPrev.addGestureRecognizer(arrowPrev)
        
        let detailsSalary = UITapGestureRecognizer(target:self, action:#selector(self.detailsSalary))
        detailsSalaryCosts.addGestureRecognizer(detailsSalary)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func threshold(){
        var s  = ""
        //diploma
        if workerOfferedJob.employerJobNewWorker.bDiploma {
            s = "דיפלומה, "
        }
        //phoneInterview
        if workerOfferedJob.employerJobNewWorker.bPhoneInterview {
            s += "נדרש ראיון טלפוני, "
        }
        //bWorkedWorkerPreference
        if workerOfferedJob.employerJobNewWorker.bWorkedWorkerPreference {
            s += ",מעדיף עובדים שכבר עבדו אצלי בעבר "
        }
        
        // ExperienceDetails
        if workerOfferedJob.employerJobNewWorker.nvExperienceDetails != "" {
            s += "נסיון: "  + workerOfferedJob.employerJobNewWorker.nvExperienceDetails + ", "
        }
        
        
        // language
        if workerOfferedJob.employerJobNewWorker.nvLanguageDetails != "" {
            s += "שפה: " + workerOfferedJob.employerJobNewWorker.nvLanguageDetails + ", "
        }
        
        //            professionalExperience
        if workerOfferedJob.employerJobNewWorker.nvProfessionalTimeDetails != "" {
            s += "וותק מקצועי: " + workerOfferedJob.employerJobNewWorker.nvProfessionalTimeDetails + ", "
        }
        
        //time
        if workerOfferedJob.employerJobNewWorker.nvTimeDescription != ""{
            s += "וותק: "  + workerOfferedJob.employerJobNewWorker.nvTimeDescription + ", "
        }
        if s.characters.count > 2 {
            let myNSString = s as NSString
            s =   myNSString.substring(with: NSRange(location: 0, length: s.characters.count-2))
            
            thresholdConditions.text = s
        }else {
            thresholdConditions.isHidden = true
            thresholdConditionsCheckBox.isHidden = true
        //    thresholdConditaonHeightConstrain.constant = 20
        }
        
        
    }
    
    
    func getTimeAvailablityForCalander() ->  Array<WorkerOfferedJobTimeAvailablity>  {
        var lWorkerOfferedJobTimeAvailablity : Array<WorkerOfferedJobTimeAvailablity> = []
        for time in workerOfferedJob.lWorkerOfferedJobTimeAvailablity {
      let ss =  collectionDate.cellStatus(for: time.date!)
          //  guard let myCustomCell = view as? JobDayCalanderCollectionViewCell else {return }

            if (ss?.isSelected)! {
           // if (ss as JobDayCalanderCollectionViewCell).checkBoxBtn.isSelected == true {
                time.bWorkerOfferedJobTimeAvailablity = true
                lWorkerOfferedJobTimeAvailablity.append(time)
                
            }
            
        }
        return lWorkerOfferedJobTimeAvailablity
    }
    
    func isTimeAvailablity () -> Bool {
        if workerOfferedJob.lWorkerOfferedJobTimeAvailablity.count == 0 {
            return false
        }
        for timeAvailablity  in  workerOfferedJob.lWorkerOfferedJobTimeAvailablity {
            if timeAvailablity.bWorkerOfferedJobTimeAvailablity {
                return true
            }
        }
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDisplayData(index: Int){
        workerOfferedJob =  Global.sharedInstance.lWorkerOfferedJobs[index]
        self.index = index
        print(workerOfferedJob.nvEmployerName)
    }
    func share(link :String){
        // text to share
        let text = "טקסט דינמי שיגיע ממערכת הניהול" + link
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        barContainerViewController.titleText = "\(workerOfferedJob.nvDomain) - \(workerOfferedJob.nvRole)"
        
    }
    //MARK: - Tap
    
    func cancelAction(){
        
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שאינך מעוניין במשרה הנוכחית?"
        self.present(basePopUp, animated: true, completion: nil)
        
    }
    
   
    func chengeNext(){
         collectionDate.scrollToSegment(SegmentDestination.next)
    }
    func chengePrev(){
        collectionDate.scrollToSegment(SegmentDestination.previous)
    }
    
    func dismissKeyboard(){
         view.endEditing(true)
    }
    
    // BasePopUpActionsDelegate
    func okAction(num: Int) {
        let  status = OfferJobStatusType.cancel.rawValue
        UpdateWorkerOfferedJobStatus(newStatus: status)
    }
    func detailsSalary(){
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "פרוט עלויות שכר - יתקבל מהלקוח"
        basePopUp.showCancelBtn = false
    
        self.present(basePopUp, animated: true, completion: nil)

    }
    //TimeAvailablityDelegates
    func ChangeStatusDel(position: Int , checked :Bool){
        workerOfferedJob.lWorkerOfferedJobTimeAvailablity[position].bWorkerOfferedJobTimeAvailablity = checked
workerChooseTimeAvailablity  = [0,0]
        
        for timeAvailablity  in  workerOfferedJob.lWorkerOfferedJobTimeAvailablity {
            if timeAvailablity.bWorkerOfferedJobTimeAvailablity {
                if timeAvailablity.bWorkerOfferedJobTimeAvailablity{
                    var p =  getHourDifferences(stringHoursFormatHHmm1: timeAvailablity.tFromHoure, stringHoursFormatHHmm2: timeAvailablity.tToHoure)
                    workerChooseTimeAvailablity[0] += p[0]
                    workerChooseTimeAvailablity[1] += p[1]
                }
              
                
            }
        }
        //אם כל המשרה פחות משעה מעגלים לשעה
        if workerChooseTimeAvailablity[0] == 0 && workerChooseTimeAvailablity[1] > 0 {
            workerChooseTimeAvailablity[0]  = 1
        }
        numberAvilabilty.text = "התחייבת ל \(workerChooseTimeAvailablity[0]) שעות מתוך \(allTimeJob)"
        calcAllAmountJob()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      calcAllAmountJob()
    }
    
    //חישוב סך  עלות המשרה
    func calcAllAmountJob(){
        if hourlyWageContent.text != ""
        {        let amunt : Double =  NSString(string: hourlyWageContent.text!).doubleValue
            allAmountJob.text = "\(amunt * Double(workerChooseTimeAvailablity[0])) שח"
        }else{
            allAmountJob.text = ""
        }
    }
    //מחזיר הפרשי זמן בין 2 שעות
    func getHourDifferences(stringHoursFormatHHmm1 : String,stringHoursFormatHHmm2 : String) -> Array<Int> {
      formatter.dateFormat = "HH:mm"
        
        var arrStartHour = stringHoursFormatHHmm1.split(separator: ":")
        var arrEndtHour = stringHoursFormatHHmm2.split(separator: ":")
        var hoursMe : Int = 0, minutesMe : Int = 0
       // if bIsDifferenceFromToday {
          //  hoursMe = Int(String(arrCurrentHour[0]))! - Int(String(arrStartHour[0]))!// dateFormatter.date(from: startHour)?.hours(from: Date())
           // minutesMe = Int(String(arrCurrentHour[1]))! - Int(String(arrStartHour[1]))! // dateFormatter.date(from: startHour)?.minutes(from: Date())
//        } else {
            hoursMe =  Int(String(arrEndtHour[0]))! - Int(String(arrStartHour[0]))! // dateFormatter.date(from: startHour)?.hours(from: Date())
           minutesMe = Int(String(arrEndtHour[1]))! - Int(String(arrStartHour[1]))!  // dateFormatter.date(from: startHour)?.minutes(from: Date())
//
//        }
        
        if hoursMe > 0 && minutesMe < 0 {
            hoursMe -= 1
            minutesMe += 60
        } else
            
            if hoursMe < 0 && minutesMe > 0 {
                hoursMe -= 1
                minutesMe -= 60
        }
        
        
        return [hoursMe,minutesMe]//"\(hoursMe):\(minutesMe)"
    }
    //MARK: - Calendar Functions
    var prevSection = 0
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo, isEndScroll: Bool) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
        print("month : \(monthName)")
        nameCurrentMonth.text = "\(monthName) \(year)"
        print("\(year)")
        if isEndScroll {
            collectionDate.scrollToSection()
            // changeScrollViewDelegate.changeScrollViewHeight(heightToadd: 0)
        }
    }
    
   
    var prePostVisibility: ((CellState,JobDayCalanderCollectionViewCell?)->())?
    func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        
        prePostVisibility?(cellState, cell as? JobDayCalanderCollectionViewCell)
        //calendarView.reloadData()
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? JobDayCalanderCollectionViewCell  else {
            return
        }
        if testCalendar.isDateInToday(cellState.date){
            myCustomCell.backgroundColor = UIColor.lightGray
        }
        
//        if cellState.isSelected {
//            if cellState.dateBelongsTo != .thisMonth {
//                myCustomCell.dayLabel.textColor = colGrey
//                myCustomCell.lblHebrewDate.textColor = colGrey
//            }
//            else{
//                myCustomCell.dayLabel.textColor = black
//                myCustomCell.lblHebrewDate.textColor = black
//            }
//        } else {
//            if cellState.dateBelongsTo == .thisMonth {
//                myCustomCell.dayLabel.textColor = black
//                myCustomCell.lblHebrewDate.textColor = black
//            }
//            else {
//                myCustomCell.dayLabel.textColor = gray
//                myCustomCell.lblHebrewDate.textColor = gray
//                myCustomCell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//                
//                
//            }
//        }
    }
    
    var flagH = false
    func handleCellSelection(view: JTAppleCell?, cellState: CellState) {
        guard let myCustomCell = view as? JobDayCalanderCollectionViewCell else {return }
        
        var currentTimeAvailablity : WorkerOfferedJobTimeAvailablity? = nil
        
        if workerOfferedJob.lWorkerOfferedJobTimeAvailablity.count > 0{
            flagH = false
           
            for time in workerOfferedJob.lWorkerOfferedJobTimeAvailablity {
                let date1 = Global.sharedInstance.getStringFromDateString(dateString: time.dFromDate)
               
                if date1 == cellState.date.getStringFromDate(today: cellState.date) {
                self.flagH = true
                    currentTimeAvailablity = time
                    time.date = cellState.date
                break
            }
            }
            
        }
        
        if currentTimeAvailablity != nil {
      //  if cellState.isSelected {
       //     flag = 1
          
            if cellState.isSelected && cellState.dateBelongsTo == .thisMonth {
                
                myCustomCell.handleCellSelection(cellState: cellState, timeAvailablity: currentTimeAvailablity!, statusDateInCalander: StatusDateInCalander.yesSelected.rawValue , statusJob: workerOfferedJob.iOfferJobStatusType ,position :0 )
                myDate = cellState.date
                
            }else{
                var status = StatusDateInCalander.noSelected.rawValue
                if isSelectAllDay == true {
                    status = StatusDateInCalander.yesSelected.rawValue
                }
                 myCustomCell.handleCellSelection(cellState: cellState, timeAvailablity: currentTimeAvailablity!, statusDateInCalander: status , statusJob: workerOfferedJob.iOfferJobStatusType , position :0)
                       }
        } else {
            
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.handleCellSelection(cellState: cellState,  statusDateInCalander: StatusDateInCalander.regularInMonth.rawValue)
            } else {
                   myCustomCell.handleCellSelection(cellState: cellState, statusDateInCalander: StatusDateInCalander.regularNoInMonth.rawValue)
                
                     }
        }
        
    }

    
    // MARK: - function to server
    func UpdateWorkerOfferedJobStatus (newStatus : Int){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerOfferedJobId"] = workerOfferedJob.iWorkerOfferedJobId as AnyObject?
        dic["iOfferJobStatusType"] = workerOfferedJob.iOfferJobStatusType as AnyObject?
        dic["iNewOfferJobStatusType"] = newStatus as AnyObject?
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                    self.GetWorkerOfferedJob()
                          } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
              Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerOfferedJobStatus")
    }
    
    func UpdateWorkerOfferedJob (){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["workerOfferedJob"] = workerOfferedJob.getDicFromWorkerOfferedJob() as AnyObject?
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        print(dic)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Global.sharedInstance.lWorkerOfferedJobs[self.index] = self.workerOfferedJob
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
                let   vc = workerStory.instantiateViewController(withIdentifier: "StatusJobViewController") as! StatusJobViewController
                vc.setDisplayData(index: self.index)
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont:  self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerOfferedJob")
        
    }
    func GetLink(){
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        print(dic)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if let dicResult = dic["Result"] as? String {
                        self.share(link: dicResult)
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont:  self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetLink")
        
        
    }

    func GetWorkerOfferedJob(){
        //nt iJobId, int iLanguageId, int iWorkerOfferedJobId
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
               dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iWorkerOfferedJobId"] =   self.workerOfferedJob.iWorkerOfferedJobId as AnyObject

        print(dic)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)

                    if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                        if (dic["Result"] as? Dictionary<String,AnyObject>) != nil {
                      var offerdJob = WorkerOfferedJob()
                            offerdJob = offerdJob.getWorkerOfferedJobFromDic(dic: (dic["Result"] as? Dictionary<String,AnyObject>)!)
                            self.workerOfferedJob = offerdJob
                            Global.sharedInstance.lWorkerOfferedJobs[self.index] = offerdJob
                            if ((self.workerOfferedJob.iOfferJobStatusType != OfferJobStatusType.new.rawValue) && (self.workerOfferedJob.iOfferJobStatusType != OfferJobStatusType.interesting.rawValue)) {
                                Global.sharedInstance.lWorkerOfferedJobs[self.index] = self.workerOfferedJob
                                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                                let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
                                let   vc = workerStory.instantiateViewController(withIdentifier: "StatusJobViewController") as! StatusJobViewController
                                vc.setDisplayData(index: self.index)
                                self.navigationController?.pushViewController(vc, animated: true)
                            
                            }else {
                            self.updateUIBydata()
                            }
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }

                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont:  self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorkerOfferedJob")
        

    }
    
}


// MARK : JTAppleCalendarDelegate
extension statusJobWhitCalanderViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "dd/MM/yyyy"
        formatter.timeZone = testCalendar.timeZone
        formatter.locale = testCalendar.locale
        
        
        
        let date = Date()
        var components = DateComponents()
        components.setValue(-10, for: Calendar.Component.year)
        let startDate = Calendar.current.date(byAdding: components, to: date)
        components.setValue(10, for: Calendar.Component.year)
        let endDate = Calendar.current.date(byAdding: components, to: date)
        
        
        //        let endDate = /*formatter.date(from: "2018 02 01")!*/Date(timeIntervalSinceNow: 30)
        
        let parameters = ConfigurationParameters(startDate: startDate!,
                                                 endDate: endDate!,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: firstDayOfWeek,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "JobDayCalanderCollectionViewCell", for: indexPath) as! JobDayCalanderCollectionViewCell
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)

    //    print("ppppp \(workerOfferedJob.iOfferJobStatusType)")
        ( myCustomCell as JobDayCalanderCollectionViewCell).setDisplayData(cellState: cellState, status: workerOfferedJob.iOfferJobStatusType/*, timeAvailablity: workerOfferedJob.lWorkerOfferedJobTimeAvailablity[indexPath.row]*/)
        ( myCustomCell as JobDayCalanderCollectionViewCell).timeAvailablityDel = self
        
        return myCustomCell
    }
//    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: CGFloat((collectionView.frame.size.width / 3) - 20), height: CGFloat(100))
//    }
 
    
    
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
       handleCellConfiguration(cell: cell, cellState: cellState)
        print (date)
       
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellConfiguration(cell: cell, cellState: cellState)
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        self.setupViewsOfCalendar(from: visibleDates , isEndScroll: false)
        print("didScrollToDateSegmentWith")
    }
    
    func scrollDidEndDecelerating(for calendar: JTAppleCalendarView) {
        
        let visibleDates = collectionDate.visibleDates()
        self.setupViewsOfCalendar(from: visibleDates, isEndScroll: true)
        print("scrollDidEndDecelerating")
        
        
        
    }
    
    //
    //        func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
    //            let date = range.start
    //            let month = testCalendar.component(.month, from: date)
    //
    //            let header: JTAppleCollectionReusableView
    //            if month % 2 > 0 {
    //                header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "WhiteSectionHeaderView", for: indexPath)
    //                (header as! WhiteSectionHeaderView).title.text = formatter.string(from: date)
    //            } else {
    //                header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "PinkSectionHeaderView", for: indexPath)
    //                (header as! PinkSectionHeaderView).title.text = formatter.string(from: date)
    //            }
    //            return header
    //        }
    
    func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
        let stride = collectionDate.frame.width * CGFloat(indexPath.section)
        return CGRect(x: stride + 5, y: 5, width: collectionDate.frame.width - 10, height: collectionDate.frame.height - 10)
    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return monthSize
        
    }

}


