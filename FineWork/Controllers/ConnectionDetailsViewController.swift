//
//  ConnectionDetailsViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 16/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//
protocol ChangePasswordDelegate {
    func verificationPassword(newPassword : String)
    func isSuccess(isSuccess : Bool?, nvErrorMessage : String)
}

import UIKit

class ConnectionDetailsViewController: UIViewController, UITextFieldDelegate, ChangePasswordDelegate  {

    //MARK: -- views
    
    @IBOutlet var nameTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var mobileTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var changePasswordBtn: UIButton!
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet var passwordEyeBtn: UIButton!
    
    //MARK: -- actions
    @IBAction func changePasswordBtnAction(_ sender : UIButton) {
        // open dialog change password
//        if validationFields() {
        isSaveOtherDetails = false
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordPopUpViewController") as? ChangePasswordPopUpViewController
            {
                vc.modalPresentationStyle = UIModalPresentationStyle.custom
                vc.changePasswordDelegate = self
                self.present(vc, animated: true, completion: nil)
            }
//        }
    }
    
    @IBAction func saveBtnAction(_ sender : UIButton) {
        if validationFields() {
//            updateUser()
            isSaveOtherDetails = true
            checkValidMailAndMobile()
//            openSmsPopUp()
//            verificationPassword(newPassword: passwordTxt.text!)
//            verificationCodeSMS(newPassword: )
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים/אינם תקינים")
        }
    }
    
    @IBAction func passwordEyeBtnAction(_ sender: Any) {
        
        if passwordTxt.isSecureTextEntry{
            passwordTxt.isSecureTextEntry = false
          //  passwordEyeBtn.setTitle(FontAwesome.sharedInstance.openEye, for: .normal)//colse eye
         passwordEyeBtn.setTitle(FontAwesome.sharedInstance.closedEye, for: .normal)//open eye
        }else{
            passwordTxt.isSecureTextEntry = true
           // passwordEyeBtn.setTitle(FontAwesome.sharedInstance.closedEye, for: .normal)//open eye
            passwordEyeBtn.setTitle(FontAwesome.sharedInstance.openEye, for: .normal)//colse eye

        }
    }
    
    //MARK: -- variables
    var txtSelected = UITextField()
    var user : User = Global.sharedInstance.user
    var isSaveOtherDetails = false
    
    //MARK: -- delegates
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        changePasswordBtn.layer.borderWidth = 1
        changePasswordBtn.layer.borderColor = UIColor.orange2.cgColor
        changePasswordBtn.layer.cornerRadius = changePasswordBtn.frame.height / 2
        
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.borderColor = UIColor.orange2.cgColor
        saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
        
        nameTxt.text = Global.sharedInstance.user.nvUserName
        emailTxt.text = Global.sharedInstance.user.nvMail
        mobileTxt.text = Global.sharedInstance.user.nvMobile
        passwordTxt.text = Global.sharedInstance.user.nvPassword
        
        nameTxt.delegate = self
        emailTxt.delegate = self
        mobileTxt.delegate = self
        passwordTxt.delegate = self
        
        // add tag for keyboard
        nameTxt.tag = 1
        emailTxt.tag = 1
        mobileTxt.tag = 1
        passwordTxt.tag = 1
        
        nameTxt.keyboardType = .default
        emailTxt.keyboardType = .emailAddress
        mobileTxt.keyboardType = .phonePad
        passwordTxt.isUserInteractionEnabled = false
        
        hideKeyboardWhenTappedAround()
        
        passwordEyeBtn.setTitle(FontAwesome.sharedInstance.openEye, for: .normal)//colse eye
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        nameTxt.text = Global.sharedInstance.user.nvUserName
        emailTxt.text = Global.sharedInstance.user.nvMail
        mobileTxt.text = Global.sharedInstance.user.nvMobile
        passwordTxt.text = Global.sharedInstance.user.nvPassword
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        txtSelected = textField
        getTextFieldDelegate.getTextField(textField: textField, originY: 0)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTxt:
            emailTxt.becomeFirstResponder()
            break
        case emailTxt:
            mobileTxt.becomeFirstResponder()
            break
        case mobileTxt:
            passwordTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        switch textField {
        case nameTxt:
            limitLength = 20
            break

        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        //characteristicsTxt.resignFirstResponder()
        //studiesTxt.resignFirstResponder()
        //jobExperienceTxt.resignFirstResponder()
        //matchingJobsTxt.resignFirstResponder()
        
    }
    
    func openSmsPopUp() {
        user.nvUserName = nameTxt.text!
        user.nvMail = emailTxt.text!
        user.nvMobile = mobileTxt.text!
        verificationCodeSMS(newPassword: passwordTxt.text!)
    }

    func verificationPassword(newPassword : String) {
        // validation fields
        user.nvPassword = newPassword
        //
//        user.nvUserName = nameTxt.text!
//        user.nvMail = emailTxt.text!
//        user.nvMobile = mobileTxt.text!
        verificationCodeSMS(newPassword: newPassword)
    }
    
    func isSuccess(isSuccess: Bool?, nvErrorMessage: String) {
        if isSuccess == nil {
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: "") )
        } else {
            if isSuccess! {
                if isSaveOtherDetails {
                    Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                    isSaveOtherDetails = false
                    Global.sharedInstance.bWereThereAnyChanges = false
                   // if Global.sharedInstance.user.iUserType == UserType.employer {
                     //   openAnotherPageDelegate.openNextTab!(tag: 3)
                    //}
                } else {
                Alert.sharedInstance.showAlert(mess: "הסיסמא נשמרה בהצלחה!")
                }
            } else {
                Alert.sharedInstance.showAlert(mess: nvErrorMessage)
            }
        }
        
        passwordTxt.text = Global.sharedInstance.user.nvPassword
    }
    
    func checkValidMailAndMobile() {
        var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicToServer["iUserId"] = user.iUserId as AnyObject
        dicToServer["nvMail"] = emailTxt.text as AnyObject?
        dicToServer["nvMobile"] = mobileTxt.text as AnyObject?
        
        api.sharedInstance.CheckValidMailAndMobile(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            var dicValid = responseObject as! [String:AnyObject]
            if dicValid["Error"]?["iErrorCode"] as! Int ==  0
            {
                //אם הטלפון והמייל אינם קיימים במערכת מופעלת פונק׳ לקבלת sms
                
                self.openSmsPopUp()
            }
            else
            {
                self.dismissKeyboard()
                Alert.sharedInstance.showAlert(mess: dicValid["Error"]?["nvErrorMessage"] as! String)
                
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            self.dismissKeyboard()
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        })
    }
    
    func updateUser() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        Global.sharedInstance.user.nvUserName = nameTxt.text!
        Global.sharedInstance.user.nvMail = emailTxt.text!
        Global.sharedInstance.user.nvMobile = mobileTxt.text!
        
        dic["user"] = Global.sharedInstance.user.getUserDictionaryForServer() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let /*dicResult*/_ = dic["Result"] as? Int {
                        
                        Global.sharedInstance.bWereThereAnyChanges = false
                        
//                        if Global.sharedInstance.user.iUserType == UserType.employer {
//                            if self.openAnotherPageDelegate != nil {
//                                self.openAnotherPageDelegate.openNextTab!(tag: 3)
//                            }
//                        }
                        //                        if dicResult {
                        //                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                        //                        } else {
                        //                            Alert.sharedInstance.showAlert(mess: "הפרטים לא נשמרו!")
                        //                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateUser")
    }
    
    func verificationCodeSMS(newPassword : String) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
//        dic["user"] = Global.sharedInstance.user.getUserDictionaryForServer() as AnyObject?
        dic["nvMobile"] = mobileTxt.text as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0
                {
                    let verCode:VerficationCodePopUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerficationCodePopUpViewController") as! VerficationCodePopUpViewController
                    verCode.modalPresentationStyle = UIModalPresentationStyle.custom
                    verCode.user = self.user//שליחת ה-user לשמירה בשרת
//                    verCode.newPassword = newPassword
                    // דגל האם הקוד הוא לצורך הרשמה או לצורך החלפת סיסמה
                    verCode.isChangePassword = true
                    verCode.changePasswordDelegate = self
                    verCode.codeVerification = String(dic["Result"] as! Int)
                    self.present(verCode, animated: true, completion: nil)
                }
                else
                {
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: ""))
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "VerificationCodeSMS")
    }
    
    // validation
    func validationFields() -> Bool {
        var isOk = true
        if !Validation.sharedInstance.nameValidation(string: nameTxt.text!) {
//            Alert.sharedInstance.showAlert(mess: "שם לא תקין")
            setErrorToTextField(sender: nameTxt)
            isOk = false
        } else {
            setValidToTextField(sender: nameTxt)
        }
        if !Validation.sharedInstance.isValidEmail(testStr: emailTxt.text!) {
//            Alert.sharedInstance.showAlert(mess: "כתובת מייל אינה תקינה")
            setErrorToTextField(sender: emailTxt)
            isOk = false
        } else {
            setValidToTextField(sender: emailTxt)
        }
        if !Validation.sharedInstance.cellPhoneValidation(string: mobileTxt.text!) {
//            Alert.sharedInstance.showAlert(mess: "מספר טלפון לא תקין")
            setErrorToTextField(sender: mobileTxt)
            isOk = false
        } else {
            setValidToTextField(sender: mobileTxt)
        }
        return isOk
    }
    
    
    func setErrorToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.red.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setValidToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.clear.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }

}
