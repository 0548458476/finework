
//
//  RegistrationViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 06/12/2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
import UserNotifications
import GoogleSignIn

//לאחר חזרה מקוד אימות, מחזיר את הודעת השגיאה / הצלחה
protocol OpenNextPageDelegate {
    func OpenNextPage(sendMessage:String)
}

class RegistrationViewController: UIViewController,UITextFieldDelegate ,UIScrollViewDelegate,OpenNextPageDelegate,GIDSignInUIDelegate,GIDSignInDelegate{
   
    @IBOutlet var scrollView: UIScrollView!
    //MARK:-- Buttons
    @IBOutlet var btnFacebookRegistration: UIButton!
    @IBOutlet var btnGoogleRegistration: UIButton!
    @IBOutlet var btnEmployeeRegistration: UIButton!
    @IBOutlet var btnEmployerRegistration: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    //MARK:-- Views
    @IBOutlet weak var viewDevideBottom: UIView!
    @IBOutlet weak var viewTopInBottom: UIView!
    @IBOutlet var viewTopUnderBtnRegiset: UIView!
    @IBOutlet var view2: UIView!
    //MARK:-- TextFields
    @IBOutlet var txtUserName: UITextField!
   // @IBOutlet var txtMail: UITextField!
    @IBOutlet var txtMobile: PhonTextField!
    @IBOutlet var txtPassword: UITextField!
    //@IBOutlet var txtRecommendPhone: PhonTextField!
    //MARK:-- labels
    
    @IBOutlet weak var lblReadRegulations: UILabel!
    @IBOutlet var lblHeader: UILabel!
    //MARK:*Icon font buttons
    //@IBOutlet weak var btnIconUserName: UIButton!
   // @IBOutlet weak var btnIconMail: UIButton!
    //@IBOutlet weak var btnIconMobile: UIButton!
    //@IBOutlet weak var btnIconPassword: UIButton!
   // @IBOutlet weak var btnIconRecommendPhone: UIButton!
    @IBOutlet weak var btnReadRegulations: CheckBox!
    //    @IBOutlet var lblIconUserName: UILabel!
    //    @IBOutlet var lblIconMail: UILabel!
    //    @IBOutlet var lblIconMobile: UILabel!
    //    @IBOutlet var lblIconPassword: UILabel!
    //    @IBOutlet var lblIconRecommendPhone: UILabel!
    
    //MARK:- Varibals
    var user:User = User()
    var userPicture:FileObj?
    var viewCon:PersonalInformationModelViewController?
    var employeeModelViewController:EmployeeModelViewController?
    var afterRegisterViewController:AfterRegisterViewController?
    var check:Bool = false
    var isSimpleRegister:Bool = false
    var selectedTxt:UITextField = UITextField()
    
    //MARK:-- Actions
    @IBAction func btnFacebookRegistrationAction(_ sender: Any) {
        let login = FBSDKLoginManager()
         login.logIn(withReadPermissions: ["email"], from: self, handler: { (result, error) -> Void in
            if (error != nil) {
                // Process error
            }
            else if (result?.isCancelled)! {
                // Handle cancellations
            }
            else {
                if (result?.grantedPermissions.contains("email"))! {
                    print("result is:\(result)")
                    self.fetchUserInfo()
                }
            }
         })
    }
    
    @IBAction func btnGoogleRegistrationAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    //הרשמה כעובד
    @IBAction func btnEmployeeRegistrationAction(_ sender: Any) {
        if !isSimpleRegister
        {
            isSimpleRegister = true
            self.Save(userType: UserType.worker)
        }
    }
    
    //הרשמה כמעסיק
    @IBAction func btnEmployerRegistrationAction(_ sender: Any) {
        //        checkSaveFile()
        if !isSimpleRegister
        {
            isSimpleRegister = true
            self.Save(userType: UserType.employer)
        }
    }
    
    @IBAction func btnReadRegulationsAction(_ sender: Any) {
        btnReadRegulations.isChecked = !btnReadRegulations.isChecked
    }

    @IBAction func tapBack(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionEye(_ sender: UIButton) {
        if txtPassword.isSecureTextEntry{
           txtPassword.isSecureTextEntry = false
            btnEye.setTitle("\u{f070}", for: .normal)//colse eye
        }else{
            txtPassword.isSecureTextEntry = true
            btnEye.setTitle("\u{f06e}", for: .normal)//open eye
            
        }
    }
    
    //MARK:- Initial
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let name = "תקנון השימוש"
        let string = "אני מאשר שקראתי את תקנון השימוש \(name)"
        lblReadRegulations.text = string
        let nsString = string as NSString
        let range = nsString.range(of: name)

        let newsString: NSMutableAttributedString = NSMutableAttributedString(string: "אני מאשר שקראתי את \(name)")
        newsString.addAttributes([NSUnderlineStyleAttributeName: NSUnderlineStyle.styleDouble.rawValue], range: range)
        lblReadRegulations.attributedText = newsString.copy() as? NSAttributedString
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistrationViewController.tapResponse))
        tapGesture.numberOfTapsRequired = 1
        lblReadRegulations.isUserInteractionEnabled =  true
        lblReadRegulations.addGestureRecognizer(tapGesture)
        
        //lbl.attributedText = attributedString
        let bottomOffset = CGPoint(x: CGFloat(0), y: CGFloat(self.scrollView.contentSize.height - self.scrollView.bounds.size.height + 50))
        self.scrollView.setContentOffset(bottomOffset, animated: true)
        
        txtUserName.delegate = self
      //  txtMail.delegate = self
        txtMobile.delegate = self
        txtPassword.delegate = self
      //  txtRecommendPhone.delegate = self
        
        txtUserName.returnKeyType = UIReturnKeyType.next
      //  txtMail.returnKeyType = UIReturnKeyType.next
        txtPassword.returnKeyType = UIReturnKeyType.next
        
        if Global.sharedInstance.rtl {
            txtUserName.textAlignment = .right
         //   txtMail.textAlignment = .right
            txtPassword.textAlignment = .right
        }

        self.setDesign()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistrationViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        viewCon = storyboard?.instantiateViewController(withIdentifier: "PersonalInformationModelViewController")as?PersonalInformationModelViewController
        employeeModelViewController = (storyboard?.instantiateViewController(withIdentifier: "EmployeeModelViewController")as?EmployeeModelViewController)!
        afterRegisterViewController = (storyboard?.instantiateViewController(withIdentifier: "AfterRegisterViewController")as? AfterRegisterViewController)!
        
        //registerKeyboardNotifications()
        // Define identifier
    }
    
    
    func tapResponse(recognizer: UITapGestureRecognizer) {
        self.dismissKeyboard()
        // Create the alert controller
        let alertController = UIAlertController(title: "", message: "תקנון - עדיין לא התקבל מהלקוח.", preferredStyle: .alert)
        
        // Create the actions
        
        let okAction = UIAlertAction(title: NSLocalizedString(LocalizableStringStatic.sharedInstance.OK_ALERT, comment: ""), style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.btnReadRegulations.isChecked = true
            alertController.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString(LocalizableStringStatic.sharedInstance.CANCEL_ALERT, comment: ""), style: UIAlertActionStyle.cancel)
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    
    override func viewWillAppear(_ animated: Bool) {
         txtUserName.text = ""
        txtPassword.text = ""
      //  txtMail.text = ""
        txtMobile.text = ""
      //  txtRecommendPhone.text = ""
        btnReadRegulations.isChecked = false
        self.user = User()
        registerKeyboardNotifications()
        //        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        unregisterKeyboardNotifications()
    }
    override func viewWillLayoutSubviews() {
        // self.view.frame = UIScreen.main.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    //MARK:-- local Functions
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK:-- SETTERS Functions
    func setDesign()
    {
        setImages()
        setFont()
        setBorders()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
    }
   
    func setFont()
    {
        //lblHeader.font = ControlsDesign.sharedInstance.fontAppicationLight
    }
    func setImages()
    {
        
    }
    func setBorders()
    {
        let myColor : UIColor = UIColor.blue2
        txtUserName.layer.borderColor = myColor.cgColor
        txtMobile.layer.borderColor = myColor.cgColor
        txtPassword.layer.borderColor = myColor.cgColor
    }
 
    func setCornerRadius()
    {
        btnGoogleRegistration.layer.cornerRadius = 0.07 * btnGoogleRegistration.bounds.size.width
        btnFacebookRegistration.layer.cornerRadius = 0.07 * btnFacebookRegistration.bounds.size.width
    }
    
    func setIconFont()
    {
      //  btnIconUserName.setTitle(FlatIcons.sharedInstance.REGISTER_USER_NAME,for: .normal)//"\u{f007}"
      //  btnIconMail.setTitle(FlatIcons.sharedInstance.REGISTER_MAIL,for: .normal)//"\u{f003}"
     //   btnIconMobile.setTitle(FlatIcons.sharedInstance.REGISTER_MOBILE,for: .normal)//\u{f095}"
     //   btnIconPassword.setTitle(FlatIcons.sharedInstance.REGISTER_PASSWORD,for: .normal)//"\u{f023}"
     //   btnIconRecommendPhone.setTitle(FlatIcons.sharedInstance.REGISTER_RECCOMEND_PHONE,for: .normal)//"\u{f023}"
        
      /*  if Global.sharedInstance.rtl == true
        {
            btnIconUserName.contentHorizontalAlignment = .right
            btnIconMail.contentHorizontalAlignment = .right
            btnIconMobile.contentHorizontalAlignment = .right
            btnIconPassword.contentHorizontalAlignment = .right
            btnIconRecommendPhone.contentHorizontalAlignment = .right
        }
        else
        {
            btnIconUserName.contentHorizontalAlignment = .left
            btnIconMail.contentHorizontalAlignment = .left
            btnIconMobile.contentHorizontalAlignment = .left
            btnIconPassword.contentHorizontalAlignment = .left
            btnIconRecommendPhone.contentHorizontalAlignment = .left
        }*/
        btnEye.setTitle("\u{f06e}", for: .normal)
    }
    
    func setLocalizableString()
    {
        //SET TEXT TO BUTTONS
        btnGoogleRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_GOOGLE, comment: ""), for: .normal)
        btnFacebookRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_WITH_FACEBOOK, comment: ""), for: .normal)
        btnEmployeeRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_AS_EMPLOYEE, comment: ""), for: .normal)
        btnEmployerRegistration.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_AS_EMPLOYER, comment: ""), for: .normal)
        
        //SET TEXT TO LABELS
        lblHeader.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_TITLE, comment: "")
        
        //set text and color to txtPlaceHolder
        txtUserName.attributedPlaceholder = NSAttributedString(string: NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_USER_NAME, comment: ""),attributes: [NSForegroundColorAttributeName: UIColor.lightBlue1])
        
     //   txtMail.attributedPlaceholder = NSAttributedString(string: NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_MAIL, comment: ""),attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string: NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_PASSWORD, comment: ""),attributes: [NSForegroundColorAttributeName: UIColor.lightBlue1])
        
        txtMobile.attributedPlaceholder = NSAttributedString(string: NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_MOBILE, comment: ""),attributes: [NSForegroundColorAttributeName: UIColor.lightBlue1])
        
     //   txtRecommendPhone.attributedPlaceholder = NSAttributedString(string: NSLocalizedString(LocalizableStringStatic.sharedInstance.REGISTER_RECOMEND_PHONE, comment: ""),attributes: [NSForegroundColorAttributeName: UIColor.white])
    }
    
    func Save(userType : UserType){
//        let user = User()
        if checkValidation() == true
        {
            user.nvUserName = txtUserName.text!
          //  user.nvMail = txtMail.text!
            user.nvMobile = txtMobile.getOriginalText()!
            user.nvPassword = txtPassword.text!
          //  user.nvRecommendPhone = txtRecommendPhone.getOriginalText()!
            user.iUserType = userType
            
            
            var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
           //  dicToServer["nvMail"] = txtMail.text as AnyObject?
             dicToServer["nvMobile"] = user.nvMobile as AnyObject?
            
            api.sharedInstance.CheckValidMailAndMobile(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
                -> Void in
                var dicValid = responseObject as! [String:AnyObject]
                if dicValid["Error"]?["iErrorCode"] as! Int ==  0
                {
                    //אם הטלפון והמייל אינם קיימים במערכת מופעלת פונק׳ לקבלת sms
                    var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
                    dicToServer["nvMobile"] = self.user.nvMobile as AnyObject?
                    
                    api.sharedInstance.VerificationCodeSMS(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
                        -> Void in
                        
                        var dicWorker101 = responseObject as! [String:AnyObject]
                        if dicWorker101["Error"]?["iErrorCode"] as! Int ==  0
                        {
                         //   UserDefaultManagment.sharedInstance.setUserName(value: self.txtMail.text!)
                            UserDefaultManagment.sharedInstance.setPassword(value: self.txtPassword.text!)
                         //   print("ww \(self.txtMail.text!)")
                            print("ww \(self.txtPassword.text!)")
                            print("ww \(UserDefaultManagment.sharedInstance.getUserName())")

                            let verCode:VerficationCodePopUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "VerficationCodePopUpViewController") as! VerficationCodePopUpViewController
                            verCode.modalPresentationStyle = UIModalPresentationStyle.custom
                            verCode.user = self.user//שליחת ה-user לשמירה בשרת
                            verCode.delegate = self
                            verCode.codeVerification = String(dicWorker101["Result"] as! Int)
                            self.present(verCode, animated: true, completion: nil)
                        }
                        else
                        {
                            self.isSimpleRegister = false
                             self.dismissKeyboard()
                            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: ""))
                        }
                    } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
                        self.isSimpleRegister = false
                         self.dismissKeyboard()
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
                    })
                }
                else
                {
                    self.isSimpleRegister = false
                     self.dismissKeyboard()
                    Alert.sharedInstance.showAlert(mess: dicValid["Error"]?["nvErrorMessage"] as! String)
                    
                }
            } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
                self.isSimpleRegister = false
                 self.dismissKeyboard()
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            })
 
        }
        else
        {
            self.isSimpleRegister = false
        }
    }
    
    func checkValidation() -> Bool{
        check = true
        //check validation:
                //user name
        if txtUserName.text == "" || Validation.sharedInstance.nameValidation(string: txtUserName.text!) == false || Validation.sharedInstance.isContainsEmojy(string: txtUserName.text!) == false {
            
            check = false
            txtUserName.layer.borderWidth = 1
            txtUserName.layer.borderColor = UIColor.red.cgColor
        }
        else {
            txtUserName.layer.borderWidth = 0
        }
        
        //password
        if self.user.nvFacebookId.isEmpty && self.user.nvGoogleId.isEmpty{
//        if txtPassword.text! == "" || (txtPassword.text?.characters.count)! < 6 || (txtPassword.text?.characters.count)!  > 10 || Validation.sharedInstance.isContainsEmojy(string: txtPassword.text!) == false
//        {
            if txtPassword.text! == "" || (txtPassword.text?.characters.count)! < 6 || (txtPassword.text?.characters.count)!  > 10 || Validation.sharedInstance.isValidPassword(testStr: txtPassword.text!) == false
            {
            
            check = false
            txtPassword.layer.borderWidth = 1
            txtPassword.layer.borderColor = UIColor.red.cgColor
        }
        else {
            txtPassword.layer.borderWidth = 0
        }
        }else{
            if txtPassword.text! != ""{
//                if (txtPassword.text?.characters.count)! < 6 || (txtPassword.text?.characters.count)!  > 10 || Validation.sharedInstance.isContainsEmojy(string: txtPassword.text!) == false
                if txtPassword.text! == "" || (txtPassword.text?.characters.count)! < 6 || (txtPassword.text?.characters.count)!  > 10 || Validation.sharedInstance.isValidPassword(testStr: txtPassword.text!) == false
                {
                    check = false
                    txtPassword.layer.borderWidth = 1
                    txtPassword.layer.borderColor = UIColor.red.cgColor
                }else{
                    txtPassword.layer.borderWidth = 0
                }
            }else{
                 txtPassword.layer.borderWidth = 0
            }
        }
        
        //email address
//        if txtMail.text! == "" || Validation.sharedInstance.isContainsEmojy(string: txtMail.text!) == false || Validation.sharedInstance.validationOfEmail(string: txtMail.text!) == false
//        {
       /* if txtMail.text! == "" || Validation.sharedInstance.isContainsEmojy(string: txtMail.text!) == false || Validation.sharedInstance.isValidEmail(testStr: txtMail.text!) == false
        {
            check = false
          ///  txtMail.layer.borderWidth = 1
          ///  txtMail.layer.borderColor = UIColor.red.cgColor
        }
        else {
            //txtMail.layer.borderWidth = 0
        }*/
        
        
        //mobile number
        if txtMobile.getOriginalText() == "" || txtMobile.getOriginalText()!.characters.count != 10 || String(txtMobile.getOriginalText()!.characters.prefix(2)) != "05"
        {
            
            check = false
            txtMobile.layer.borderWidth = 1
            txtMobile.layer.borderColor = UIColor.red.cgColor
        }
        else {
            txtMobile.layer.borderWidth = 0
        }
        
        //recommend mobile number
       /* if txtRecommendPhone.getOriginalText() != "" && (txtRecommendPhone.getOriginalText()!.characters.count != 10 || String(txtRecommendPhone.getOriginalText()!.characters.prefix(2)) != "05")
        {
            check = false
            txtRecommendPhone.layer.borderWidth = 1
            txtRecommendPhone.layer.borderColor = UIColor.red.cgColor
        }
        else {
            txtRecommendPhone.layer.borderWidth = 0
        }*/

        if check == false
        {
            self.showAlert(sendMessage: (NSLocalizedString("FIELDS_NOT_FILLED", comment: "")))
        }else{
            if !btnReadRegulations.isChecked {
                self.showAlert(sendMessage: (NSLocalizedString(LocalizableStringStatic.sharedInstance.OK_READING_REGULATION, comment: "")))
                check = false
            }
        }
        return check
    }
    
    func checkSaveFile()
    {
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        /*
         var image:UIImage = UIImage(named: "logo.png")!
         let base64String = convertImageToBase64(image: image)
         var strImage:String = base64String
         */
        
//        let documentsURL = try! FileManager().url(for: .documentDirectory,
//                                                  in: .userDomainMask,
//                                                  appropriateFor: nil,
//                                                  create: true)
        
        let filePath = Bundle.main.path(forResource: "vcheck", ofType:"pdf")
        let data = NSData(contentsOfFile:filePath!)
        let dataStr = data?.base64EncodedString(options: [])
        dic["nvFileName"] = "vcheck.pdf" as AnyObject?
        dic["nvDoc"] = dataStr as AnyObject?
        api.sharedInstance.checkSaveFile(params: dic, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            
            
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            self.isSimpleRegister = false
        })
        
        
    }

    func showAlert(sendMessage: String)
    {
         self.dismissKeyboard()
        let alert : UIAlertController = UIAlertController(title: "", message: sendMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction:UIAlertAction = UIAlertAction(title: NSLocalizedString((LocalizableStringStatic.sharedInstance.OK_ALERT), comment: ""), style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            //self.dismiss()
            if sendMessage == "is registration Success!"{
//                self.navigationController?.pushViewController(self.viewCon!, animated: true)
//                self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
                self.openNextPageByUserType()
//                self.navigationController?.pushViewController(self.afterRegisterViewController!, animated: true)
                
            }
            else if sendMessage == NSLocalizedString("REGISTER_SUCCESS",comment: "")
            {
                if self.userPicture != nil{//אם יש תמונת פרופיל מפייסבוק או גוגל פלוס
                    self.savePictureInServer()
                }
                else{
//            let viewCon101 = (self.storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailsViewController")as?EmployeeDetailsViewController?)!
//            self.navigationController?.pushViewController(viewCon101!, animated: true)
//                    let viewCon101 = (self.storyboard?.instantiateViewController(withIdentifier: "PersonalInformationModelViewController")as?PersonalInformationModelViewController?)!
//                    self.navigationController?.pushViewController(viewCon101!, animated: true)
//                    self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
                    self.openNextPageByUserType()
//                    self.navigationController?.pushViewController(self.afterRegisterViewController!, animated: true)
                }
            }
        })
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func savePictureInServer(){
        var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
//        let imageString = userPicture?.nvFile
//        let imageString = Global.sharedInstance.convertImageToBase64(image: userPicture!)
//        dicToServer["image"] = imageString as AnyObject?
        dicToServer["image"] = userPicture?.getImageDic() as AnyObject?
        dicToServer["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dicToServer["iUserType"] = Global.sharedInstance.user.iUserType.rawValue as AnyObject?
        api.sharedInstance.UpdateImage(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
//                let viewCon101 = (self.storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailsViewController")as?EmployeeDetailsViewController?)!
//                self.navigationController?.pushViewController(viewCon101!, animated: true)
//                self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
                self.openNextPageByUserType()
//                self.navigationController?.pushViewController(self.afterRegisterViewController!, animated: true)
            }else{
//                let viewCon101 = (self.storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailsViewController")as?EmployeeDetailsViewController?)!
//                self.navigationController?.pushViewController(viewCon101!, animated: true)
//                self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
                self.openNextPageByUserType()
//                self.navigationController?.pushViewController(self.afterRegisterViewController!, animated: true)
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - TextFields
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxt = textField
       // if textField == txtRecommendPhone{
       //     let bottomOffset = CGPoint(x: CGFloat(0), y: CGFloat(self.scrollView.contentSize.height - self.scrollView.bounds.size.height + 50))
       //     self.scrollView.setContentOffset(bottomOffset, animated: true)
            
       // }
        
        
//        if textField == txtRecommendPhone || textField == txtPassword{
//            let bottomOffset = CGPoint(x: CGFloat(0), y: CGFloat(self.scrollView.contentSize.height - self.scrollView.bounds.size.height + 50))
//            self.scrollView.setContentOffset(bottomOffset, animated: true)
//        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
//        self.scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(-self.scrollView.contentInset.top)), animated: true)
//        var ph = textField.placeholder
//        
//        ph = ph! + " "
//        
//        textField.placeholder = ph
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        /* switch textField {
        case txtUserName:
            //txtMail.becomeFirstResponder()
        case //txtMail:
            txtMobile.becomeFirstResponder()
        case txtMobile:
            txtPassword.becomeFirstResponder()
        case txtPassword:
           // txtRecommendPhone.becomeFirstResponder()
        default:
           // txtRecommendPhone.resignFirstResponder()
            return true
        }
        */
        return false
    }
    
    
    //MARK: -NOTIFICATION KEYBOARD
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (userInfo.object(forKey: UIKeyboardFrameBeginUserInfoKey)! as AnyObject).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        if (!aRect.contains(selectedTxt.frame.origin)){
            let scrollPoint = CGPoint(x: 0, y: keyboardSize.height + selectedTxt.frame.size.height)
            scrollView.setContentOffset(scrollPoint, animated: true)
            
            //            let bottomOffset = CGPoint(x: CGFloat(0), y: CGFloat(self.scrollView.contentSize.height - self.scrollView.bounds.size.height))
            //            self.scrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    //MARK: - Facebook
    func fetchUserInfo() {
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    if let data:[String:AnyObject] = result as? [String : AnyObject]{
                        if let name = data["name"] as? String{
                            self.txtUserName.text = name
                            self.txtUserName.isEnabled = false
                        }
                        if let email = data["email"] as? String{
                          //  self.txtMail.text = email
                           // self.txtMail.isEnabled = false
                        }
                        if let facebookId = data["id"] as? String{
                            self.user.nvFacebookId = facebookId
                        }
                        if let pictureData = (data["picture"] as? NSDictionary)?["data"] as? NSDictionary {
//                            self.userPicture = FileObj()
//                            self.userPicture?.nvFileName = name
                            if let pictureURLString = pictureData["url"] as? String{
                            if let urlImage = URL(string: pictureURLString){
                                self.downloadImage(url: urlImage)
                            }
                            }
//                            if let image = {
//                                self.userPicture = image
//                            }
                           print(pictureData)
                        }
                    }
                }
            })
        }
    }
  
    func OpenNextPage(sendMessage:String)
    {
        self.isSimpleRegister = false
        self.showAlert(sendMessage:sendMessage)
    }

    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            var image = UIImage(data: data)!
            image = Global.sharedInstance.resizeImage(image: image, newWidth: 80)
            let imageString = Global.sharedInstance.convertImageToBase64(image: image)
            self.userPicture = FileObj()
            self.userPicture?.nvFile = imageString
            self.userPicture?.nvFileName = imageString + ".png"
//            self.userPicture = image

//            DispatchQueue.main.async() { () -> Void in
//                self.imageView.image = UIImage(data: data)
//            }
        }
    }
    
    //MARK: GoolePlus
    func sign(_ signIn: GIDSignIn, didSignInFor user: GIDGoogleUser, withError error: Error?) {
        // Perform any operations on signed in user here.
        if (error == nil) {
            // Perform any operations on signed in user here.
            if let name = user.profile.name{
                self.txtUserName.text = name
                self.txtUserName.isEnabled = false
            }
            if let email = user.profile.email{
                //self.txtMail.text = email
                //self.txtMail.isEnabled = false
            }
            if let googleId = user.userID {
                self.user.nvGoogleId = googleId
            }

                    if let urlImage = user.profile.imageURL(withDimension: 400){
                        self.downloadImage(url: urlImage)
            }
            
//            self.loginToServer(user: userNew)
            
            GIDSignIn.sharedInstance().disconnect()
            GIDSignIn.sharedInstance().signOut()
            // ...
        } else {
            print("\(error?.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // MARK: - Open Next Screen
    func openNextPageByUserType() {
        
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker:
            self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
            break
        case UserType.employer:
            
            let viewCon = storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
            self.navigationController?.pushViewController(viewCon!, animated: true)
            
            break
        default:
            break
        }
        
    }

}
