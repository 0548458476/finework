//
//  RankViewController.swift
//  FineWork
//
//  Created by Racheli Kroiz on 20/07/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class RankViewController: UIViewController ,UITableViewDelegate , UITableViewDataSource , BasePopUpActionsDelegate{

    //MARK: - Outlet
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var rankNunber: UILabel!
    @IBOutlet weak var titleTableView: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chengeRank: UIButton!
    
      //MARK: - Action
    
    @IBAction func cengeRankBtn(_ sender: Any) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                      Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        var resetRanking = ResetRanking()
                        resetRanking = resetRanking.getRResetRankingObj(dic: dicResult)
                        if resetRanking.isReset {
                             Alert.sharedInstance.showAlert(mess: "נשמר בהצלחה")
                        }else{
                            let story = UIStoryboard(name: "Main", bundle: nil)
                            let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
                            basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
                            
                            
                            self.present(basePopUp, animated: true, completion: nil)
                            if resetRanking.NextResetFromDate == nil {
                           
                               Alert.sharedInstance.showAlert(mess: "אין לך עדיין אפשרות לבקש דרוג או לאפסו")

                            }else{
                                var startHour : String = ""
                                  startHour = startHour.convertDateFromServer(dateString: resetRanking.NextResetFromDate!, dateFormat: "dd/MM/yyyy", bAddHours: false)

                               Alert.sharedInstance.showAlert(mess:" אין אפשרות לבקש כעת דרוג חדש, תוכל לבקש דרוג חדש החל מתאריך \(startHour)" )
                            }
                     //   basePopUp.basePopUpActionDelegate = self
                            

                        }
                        
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "ResetRanking")
  
      
    }
    
     //MARK: - variable
    var ranking = Ranking()
    
    override func viewDidLoad() {
        super.viewDidLoad()


        getRanking()
    }
    override func viewDidLayoutSubviews() {
        
        img.layer.cornerRadius = img.frame.height / 2
        img.layer.masksToBounds = true
        img.contentMode = UIViewContentMode.scaleAspectFill
        
        chengeRank.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        chengeRank.layer.borderWidth = 1
        chengeRank.layer.cornerRadius = chengeRank.frame.height / 2
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // BasePopUpActionsDelegate
    func okAction(num: Int) {
        
    }
    
  //MARK: - Server Function
    
    func getRanking(){
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        let rank = Ranking()
                        self.ranking = rank.getRankingFromDic(dic: dicResult)
                        print(self.ranking.nRanking1Avg)
//                        self.cellVisibleProfileHeader.setDisplayData(languages: self.languagesArray, workerLanguage: Global.sharedInstance.worker.lWorkerLanguages.count > 0 ? Global.sharedInstance.worker.lWorkerLanguages[0] : nil)
                       // self.cellVisibleProfileEnd.setDisplayData()
                        self.setDisplayData()
                       
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
                
                           }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetRanking")

    }
    
    
    
    func setDisplayData(){
        //let dataDecoded : Data = Data(base64Encoded: Global.sharedInstance.user.picture.nvFile, options: .ignoreUnknownCharacters)!
       /// let decodedimage = UIImage(data: dataDecoded)
        img.image = Global.sharedInstance.user.profileImage
        
        rankNunber.text = "\(ranking.nRankingAvg) "
     setRankWithImg()
       // getUserImage()
    }
    
    //MARK: -TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ranking.lRankingComment!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("aaa tableView \(indexPath.row)")
        
        let cell : RankingCellTableViewCell
        
        cell = tableView.dequeueReusableCell(withIdentifier: "RankingCellTableViewCell") as! RankingCellTableViewCell
        
        cell.textRang?.text = "\(self.ranking.lRankingComment![indexPath.row].nvRankingComment) דירוג: \(self.ranking.lRankingComment![indexPath.row].nRankingSum)"
        cell.imgRank?.image = self.ranking.lRankingComment![indexPath.row].imageView
        
        return cell
    }
    
    //MARK: Get Images
    
    
    var counter = 0
    var imageMap = Array<String>()
    var downloadsCounter = 0
    
    
    func setRankWithImg() {
        print("aaa setRonkWithImg")
        counter = 0
        downloadsCounter = 0
        imageMap.removeAll()
        
        for index in stride(from: 0, to: ranking.lRankingComment!.count, by: 1) {
            
            var isExist = false
            for i in stride(from: 0, to: imageMap.count, by: 1) {
                if ranking.lRankingComment?[index].nvImage  == imageMap[i] {
                    isExist = true
                    //                            index2 = i
                    break
                }
            }
            if !isExist {
                imageMap.insert((ranking.lRankingComment?[index].nvImage)!, at: imageMap.count)
                counter += 1
                var stringPath = api.sharedInstance.buldUrlFile(fileName: (ranking.lRankingComment?[index].nvImage)!)
                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                if let url = URL(string: stringPath) {
                    downloadImage(url: url, index: /*index*/imageMap.count-1)
                }
            }
            
            
            //                item.logoImage.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
        }
        print("aaa finish setRonkWithImg")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func downloadImage(url: URL, index: Int) {
        print("aaa downloadImage start")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    
                    
                    for i in stride(from: 0, to: self.ranking.lRankingComment!.count, by: 1) {
                        if self.ranking.lRankingComment?[i].nvImage == self.imageMap[index] {
                            self.ranking.lRankingComment?[i].imageView = image
                            print("add image \(i)")
                            //print("i: \(i) \(self.ranking.lRankingComment?[i].imageView )")
                        }
                        
                        
                        self.downloadsCounter += 1
                        
                        print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                        if self.downloadsCounter == self.counter {
                            self.tableView.reloadData()
                        }
                        
                    }
                } } else {
                self.downloadsCounter += 1
                
                print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                if self.downloadsCounter == self.counter {
                    self.tableView.reloadData()
                }
            }
            
            
            
        }
        print("aaa downloadImage finish")
        
    }
    
    
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        //        barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "דירוג"
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
