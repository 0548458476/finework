//
//  DialogTerminationWorkerViewController.swift
//  FineWork
//
//  Created by User on 25/03/2018.
//  Copyright © 2018 webit. All rights reserved.
//

import UIKit


@objc protocol DialogTerminationWorkerDelegate {
    func okdialogTerminationAction(iJobTerminationtId :Int, iEmployerJobId: Int, iWorkerOfferedJobId: Int)
    func nextdialogTerminationAction(iJobTerminationtId :Int, iEmployerJobId: Int, iWorkerOfferedJobId: Int)
}

class DialogTerminationWorkerViewController: UIViewController {

    @IBOutlet weak var LnumMisra: UILabel!
    @IBOutlet weak var Tv1: UITextView!
    @IBOutlet weak var Tv2: UITextView!
    @IBOutlet weak var LnoteTxt: UILabel!
    @IBOutlet weak var BtnOk: UIButton!
    @IBOutlet weak var BtnNext: UIButton!
    
    var numMisra :String = ""
    var iJobTerminationtId :Int = 0
    var employerJobId :Int = 0
    var workerOfferedJobId :Int = 0
    var dialogTerminationWorkerDelegate : DialogTerminationWorkerDelegate! = nil
    
    @IBAction func OkAction(_ sender: UIButton) {
        if dialogTerminationWorkerDelegate != nil {
            dismissPopUp()
            dialogTerminationWorkerDelegate.okdialogTerminationAction(iJobTerminationtId :iJobTerminationtId, iEmployerJobId: employerJobId, iWorkerOfferedJobId: workerOfferedJobId)
        } else {
            dismissPopUp()
        }
    }
    
    @IBAction func NextAction(_ sender: UIButton) {
        if dialogTerminationWorkerDelegate != nil {
            dismissPopUp()
            dialogTerminationWorkerDelegate.nextdialogTerminationAction(iJobTerminationtId :iJobTerminationtId, iEmployerJobId: employerJobId, iWorkerOfferedJobId: workerOfferedJobId)
        } else {
            dismissPopUp()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if numMisra != ""{
            LnumMisra.text = "מספר המשרה: "+numMisra
        }
        else{
            LnumMisra.isHidden = true
        }
        Tv1.text = "המעסיק בפועל בקש את הפסקת עבודתך מסיבות שלו, אם ברצונך להמנע מתהליך השימוע בכתב על פי חוק"
        Tv2.text = "אחרת, הנך מוזמן להליך שימוע בכתב, ביכולתך להעזר ביועץ משפטי."
        LnoteTxt.text = "*ראה הודעה זו כמתן הודעה לפני פיטורין"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func dismissPopUp() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
