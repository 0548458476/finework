//
//  BasePopUpViewController.swift
//  FineWork
//
//  Created by User on 26.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

@objc protocol BasePopUpActionsDelegate {
    func okAction(num: Int)
    @objc optional func cancelAction(num: Int)
}

// open this pop-up
//func openBasePopUp() {
//    let basePopUp = self.storyboard?.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
//    basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
//    
//    self.present(basePopUp, animated: true, completion: nil)
//}

class BasePopUpViewController: UIViewController{

    //MARK: - Views
    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var Titel: UILabel!
    @IBOutlet weak var NumMisra: UILabel!
    
    
    //MARK: - Actions
    @IBAction func okBtnAction(_ sender: Any) {
        if basePopUpActionDelegate != nil {
            dismissPopUp()
            basePopUpActionDelegate.okAction(num: num)
        } else {
            dismissPopUp()
        }
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        if basePopUpActionDelegate != nil {
            if let _ = basePopUpActionDelegate.cancelAction?(num: num) {
                dismissPopUp()
            } else {
                dismissPopUp()
            }
        } else {
            dismissPopUp()
        }
    }
    
    //MARK: - Variables
    var basePopUpActionDelegate : BasePopUpActionsDelegate! = nil
    var text : String = ""
    var showCancelBtn : Bool = true
    var titelText : String = ""
    var nummisra : String = ""
    var num :Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textLbl.text = text
        if titelText != ""{
            Titel.text = titelText
        }else{
            Titel.isHidden = true
        }
        if nummisra != ""{
            NumMisra.text = "מספר המשרה: "+nummisra
        }else{
            NumMisra.isHidden = true
        }
        Global.sharedInstance.setButtonCircle(sender: okBtn, isBorder: false)
        Global.sharedInstance.setButtonCircle(sender: cancelBtn, isBorder: true)
        
        backgroundView.layer.borderWidth = 1
        backgroundView.layer.borderColor = ControlsDesign.sharedInstance.colorFucsia.cgColor
        
        if !showCancelBtn {
            cancelBtn.isHidden = true
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func dismissPopUp() {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
