//
//  MenuViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 20/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//
@objc protocol ChangeScrollViewHeightDelegate {
    @objc func changeScrollViewHeight(heightToadd : CGFloat)
    @objc optional func setScrollViewEnable(isEnable : Bool)
    @objc optional func scrollTop()
}

@objc protocol SaveEmployerDelegate {
//    @objc optional func saveEmployer()
    @objc optional func saveEmployer(goNextTab : Int)
    @objc optional func saveAuthorizedEmploy(authorizedEmploy : AuthorizedEmploy, indexInArray : Int)
}

@objc protocol OpenAnotherPageDelegate {
    @objc optional func openManual()
    @objc optional func openNextTab(tag : Int)
    @objc optional func openViewController(VC : UIViewController)
}

import UIKit

class MenuViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, ChangeScrollViewHeightDelegate, getTextFieldDelegate, UploadFilesDelegate, SaveEmployerDelegate, OpenAnotherPageDelegate, BasePopUpActionsDelegate {

    //MARK: -- views
    
    @IBOutlet var menuView: UIView!
    @IBOutlet var menuContainerView: UIView!
    @IBOutlet var scrollView: UIScrollView!

    @IBOutlet var backBtn: UIButton!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var containerNameLbl: UILabel!
    @IBOutlet var descriptionMenuLbl: UILabel!
    @IBOutlet var containerDescriptionLbl: UILabel!
    
    @IBOutlet var userImg: UIImageView!
    @IBOutlet var imgBorderView: UIView!
    @IBOutlet var viewUnderScroll: UIView!
    
    // worker menu
    @IBOutlet weak var jobPreferencesWorkerView: UIView!
    @IBOutlet var cJobPreferencesWorkerView: UIView!
    @IBOutlet var jobPreferencesBtn: UIButton!
//    @IBOutlet var cJobPreferencesBtn: UIButton!
    @IBOutlet var cJobPreferencesBtn: UIButton!
    @IBOutlet var jobLocatioBtn: UIButton!
    @IBOutlet var cJobLocatioBtn: UIButton!
    @IBOutlet var jobAvailabilityBtn: UIButton!
    @IBOutlet var cJobAvailabilityBtn: UIButton!
//    @IBOutlet var cJobAvailabilityBtn: UIButton!
    
    // employer menu
    @IBOutlet weak var personalDetailsEmployerView: UIView!
    @IBOutlet var cPersonalDetailsEmployerView: UIView!
    @IBOutlet weak var invisibleCompanyProfileBtn: UIButton!
//    @IBOutlet var cInvisibleCompanyProfileBtn: UIButton!
    @IBOutlet var cInvisibleCompanyProfileBtn: UIButton!
    @IBOutlet weak var visibleCompanyProfileBtn: UIButton!
//    @IBOutlet var cVisibleCompanyProfileBtn: UIButton!
    @IBOutlet var cVisibleCompanyProfileBtn: UIButton!
    @IBOutlet weak var connectionDetailsBtn: UIButton!
    @IBOutlet var cConnectionDetailsBtn: UIButton!
    @IBOutlet weak var paymentDetailsBtn: UIButton!
    @IBOutlet var cPaymentDetailsBtn: UIButton!
    @IBOutlet weak var jobPermissionBtn: UIButton!
    @IBOutlet var cJobPermissionBtn: UIButton!
    
    
    //MARK: -- actions
    
    @IBAction func backBtnAction(_ sender : UIButton) {
        if Global.sharedInstance.bWereThereAnyChanges {
            isTapBack = true
            openBasePopUp()
        } else {
            setPreviusPage()
        }
    }

    @IBAction func btnUserProphilImgChangeAction(_ sender: Any) {
        showCameraActionSheet()
    }
    
    @IBAction func menuBtnAction(_ sender : UIButton) {
        if Global.sharedInstance.bWereThereAnyChanges {
            tabSenderBtn = sender
            openBasePopUp()
        } else {
            setNextPageIfThereWereNotAnyChanges(sender: sender)
        }
    }
    
    //MARK: - Variables
    // controllers of worker
    var jobPreferencesVC : JobPreferencesViewController = JobPreferencesViewController()
    var jobLocationVC : JobLocationViewController = JobLocationViewController()
    var jobAvailabilityVC : JobAvailabilityViewController = JobAvailabilityViewController()
    // controllers of employer
    var employerInvisibleProfileVC : EmployerInvisibleProfileViewController = EmployerInvisibleProfileViewController()
    var companyVisibleProfileVC : CompanyVisibleProfileViewController = CompanyVisibleProfileViewController()
    var connectionDetailsVC : ConnectionDetailsViewController = ConnectionDetailsViewController()
    var companyPaymentDetailsVC : CompanyPaymentDetailsViewController = CompanyPaymentDetailsViewController()
    var employmentAuthorizedVC : EmploymentAuthorizedViewController = EmploymentAuthorizedViewController()
    // delegates
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    var isUserImage = false
    var isTapBack = false
    
    // variables that fill if arrived from menuPlus
    var menuTag : Int = 0
    var tabSenderBtn : UIButton? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true,animated: false)
        
        menuContainerView.isHidden = true
        setGeneralDesign()
        
        scrollView.delegate = self
        
        setScreenByUserType()
        
        
        
        // Do any additional setup after loading the view.
        
        // getWorker()
        
        hideKeyboardWhenTappedAround()
        
//        open preferences job screen
//                setBackgroundcolorToBtn(btn: jobPreferencesBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                setBackgroundcolorToBtn(btn: jobLocatioBtn, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: jobAvailabilityBtn, color: UIColor.clear)
//                getJobPreferencesForm()
        
        
        
    }
    
    override func viewDidLayoutSubviews() {
        if self.scrollView != nil && self.scrollView.contentOffset.y < 0 {
            self.scrollView.contentOffset = CGPoint.zero
        }
        
        print("viewDidLayoutSubviews")
        
        
        //open preferences job screen
//        setBackgroundcolorToBtn(btn: jobPreferencesBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//        setBackgroundcolorToBtn(btn: jobLocatioBtn, color: UIColor.clear)
//        setBackgroundcolorToBtn(btn: jobAvailabilityBtn, color: UIColor.clear)
//        getJobPreferencesForm()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNextPageIfThereWereNotAnyChanges(sender : UIButton) {
        
        setScrollViewEnable(isEnable: true)
        removeAllViews()
        self.scrollView.contentOffset = CGPoint.zero
        Global.sharedInstance.bIsOpenFiles = false
        
        switch sender {
        // worker screen
        case jobPreferencesBtn, cJobPreferencesBtn:
            setBackgroundcolorToBtn(btn: jobPreferencesBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobPreferencesBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getJobPreferencesForm()
            break
        case jobLocatioBtn, cJobLocatioBtn:
            setBackgroundcolorToBtn(btn: jobLocatioBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobLocatioBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getJobLocationForm()
            break
        case jobAvailabilityBtn, cJobAvailabilityBtn:
            setBackgroundcolorToBtn(btn: jobAvailabilityBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobAvailabilityBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getJobAvailibilityForm()
            break
        // employer screen
        case invisibleCompanyProfileBtn, cInvisibleCompanyProfileBtn:
            setBackgroundcolorToBtn(btn: invisibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cInvisibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getEmployerInvisibleProfileForm()
            break
        case visibleCompanyProfileBtn, cVisibleCompanyProfileBtn:
            setBackgroundcolorToBtn(btn: visibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cVisibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getCompanyVisibleProfileForm()
            break
        case connectionDetailsBtn, cConnectionDetailsBtn:
            setBackgroundcolorToBtn(btn: connectionDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cConnectionDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getConnectionDetailsForm()
            break
        case paymentDetailsBtn, cPaymentDetailsBtn:
            setBackgroundcolorToBtn(btn: paymentDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cPaymentDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getPaymentDetailsForm()

            break
        case jobPermissionBtn, cJobPermissionBtn:
            setBackgroundcolorToBtn(btn: jobPermissionBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobPermissionBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getEmploymentAuthorizedForm()
            
            break
        default:
            break
        }
        
        tabSenderBtn = nil
    }
    
    func setScreenByUserType() {
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker:
            descriptionMenuLbl.text = "העדפות משרה"
            containerDescriptionLbl.text = "העדפות משרה"
            jobPreferencesWorkerView.isHidden = false
            cJobPreferencesWorkerView.isHidden = false
            personalDetailsEmployerView.isHidden = true
            cPersonalDetailsEmployerView.isHidden = true
            setScreenToWorker()
            getWorker()
            break
        case UserType.employer:
            descriptionMenuLbl.text = "פרופיל"
            containerDescriptionLbl.text = "פרופיל"
            jobPreferencesWorkerView.isHidden = true
            cJobPreferencesWorkerView.isHidden = true
            personalDetailsEmployerView.isHidden = false
            cPersonalDetailsEmployerView.isHidden = false
            setScreenToEmployer()
            getEmployer()
            break
        default:
            break
        }
    }
    
    func setScreenToWorker() {
        // init JobPreferencesViewController
        jobPreferencesVC = self.storyboard?.instantiateViewController(withIdentifier: "JobPreferencesViewController") as! JobPreferencesViewController
        jobPreferencesVC.view.frame.origin.y = menuView.frame.height//CGRect(x: 0, y: menuView.frame.height, width: menuView.frame.width, height: jobPreferencesVC.view.frame.height)
        
        // init JobLocationViewController
        jobLocationVC = self.storyboard?.instantiateViewController(withIdentifier: "JobLocationViewController") as! JobLocationViewController
        jobLocationVC.view.frame.origin.y = menuView.frame.height//CGRect(x: 0, y: menuView.frame.height, width: menuView.frame.width, height: jobLocationVC.view.frame.height)
        
        // init JobAvailabilityViewController
        jobAvailabilityVC = self.storyboard?.instantiateViewController(withIdentifier: "JobAvailabilityViewController") as! JobAvailabilityViewController
        jobAvailabilityVC.view.frame.origin.y = menuView.frame.height
    }
    
    func setScreenToEmployer() {
        // init employerInvisibleProfileViewController
        employerInvisibleProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EmployerInvisibleProfileViewController") as! EmployerInvisibleProfileViewController
        employerInvisibleProfileVC.view.frame.origin.y = menuView.frame.height
        
        // init companyVisibleProfileViewController
        let employerStory =  UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        companyVisibleProfileVC = employerStory.instantiateViewController(withIdentifier: "CompanyVisibleProfileViewController") as! CompanyVisibleProfileViewController
        companyVisibleProfileVC.view.frame.origin.y = menuView.frame.height
        
        // init ConnectionDetailsViewController
        connectionDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ConnectionDetailsViewController") as! ConnectionDetailsViewController
        connectionDetailsVC.view.frame.origin.y = menuView.frame.height
        
        // init CompanyPaymentDetailsViewController
        companyPaymentDetailsVC = employerStory.instantiateViewController(withIdentifier: "CompanyPaymentDetailsViewController") as! CompanyPaymentDetailsViewController
        companyPaymentDetailsVC.view.frame.origin.y = menuView.frame.height

        // init EmploymentAuthorizedViewController
        employmentAuthorizedVC = employerStory.instantiateViewController(withIdentifier: "EmploymentAuthorizedViewController") as! EmploymentAuthorizedViewController
        employmentAuthorizedVC.view.frame.origin.y = menuView.frame.height
    }

    //MARK: - Set Design Of All Views
    func setGeneralDesign() {
        setBackgroundcolor()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        setDesignToUserImg()
        getUserImage()
        
        nameLbl.text = Global.sharedInstance.user.nvUserName
        containerNameLbl.text = Global.sharedInstance.user.nvUserName

    }
    
    func setBackgroundcolor()
    {
      //  self.menuView.backgroundColor = ControlsDesign.sharedInstance.colorDarkPurple
      //  self.menuContainerView.backgroundColor = ControlsDesign.sharedInstance.colorDarkPurple
       // setBackgroundcolorToBtn(btn: backBtn, color: ControlsDesign.sharedInstance.colorDarkPurple)
    }
    
    func setBorders(){
        // worker
        setBorderToBtn(btn: jobPreferencesBtn, color: UIColor.white , width: 1.0)
        setBorderToBtn(btn: jobLocatioBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: jobAvailabilityBtn, color: UIColor.white, width: 1.0)
        // container
        setBorderToBtn(btn: cJobPreferencesBtn, color: UIColor.white , width: 1.0)
        setBorderToBtn(btn: cJobLocatioBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: cJobAvailabilityBtn, color: UIColor.white, width: 1.0)
        
        // employer
        setBorderToBtn(btn: invisibleCompanyProfileBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: visibleCompanyProfileBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: connectionDetailsBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: paymentDetailsBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: jobPermissionBtn, color: UIColor.white, width: 1.0)
        //container
        setBorderToBtn(btn: cInvisibleCompanyProfileBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: cVisibleCompanyProfileBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: cConnectionDetailsBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: cPaymentDetailsBtn, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: cJobPermissionBtn, color: UIColor.white, width: 1.0)
    }
    
    func setTextcolor(){
        nameLbl.textColor = UIColor.white
        descriptionMenuLbl.textColor = UIColor.white
        // container
        containerNameLbl.textColor = UIColor.white
        containerDescriptionLbl.textColor = UIColor.white
        
        // worker
        setTextcolorToBtn(btn: jobPreferencesBtn, color: UIColor.white)
        setTextcolorToBtn(btn: jobLocatioBtn, color: UIColor.white)
        setTextcolorToBtn(btn: jobAvailabilityBtn, color: UIColor.white)
        // container
        setTextcolorToBtn(btn: cJobPreferencesBtn, color: UIColor.white)
        setTextcolorToBtn(btn: cJobLocatioBtn, color: UIColor.white)
        setTextcolorToBtn(btn: cJobAvailabilityBtn, color: UIColor.white)
        
        // employer
        setTextcolorToBtn(btn: invisibleCompanyProfileBtn, color: UIColor.white)
        setTextcolorToBtn(btn: visibleCompanyProfileBtn, color: UIColor.white)
        setTextcolorToBtn(btn: connectionDetailsBtn, color: UIColor.white)
        setTextcolorToBtn(btn: paymentDetailsBtn, color: UIColor.white)
        setTextcolorToBtn(btn: jobPermissionBtn, color: UIColor.white)
        // container
        setTextcolorToBtn(btn: cInvisibleCompanyProfileBtn, color: UIColor.white)
        setTextcolorToBtn(btn: cVisibleCompanyProfileBtn, color: UIColor.white)
        setTextcolorToBtn(btn: cConnectionDetailsBtn, color: UIColor.white)
        setTextcolorToBtn(btn: cPaymentDetailsBtn, color: UIColor.white)
        setTextcolorToBtn(btn: cJobPermissionBtn, color: UIColor.white)
    }
    
    func setCornerRadius(){
        // worker
        self.configureButton(btn: jobPreferencesBtn)
        self.configureButton(btn: jobLocatioBtn)
        self.configureButton(btn: jobAvailabilityBtn)
        // container
        self.configureButton(btn: cJobPreferencesBtn)
        self.configureButton(btn: cJobLocatioBtn)
        self.configureButton(btn: cJobAvailabilityBtn)
        
        // employer
        self.configureButton(btn: invisibleCompanyProfileBtn)
        self.configureButton(btn: visibleCompanyProfileBtn)
        self.configureButton(btn: connectionDetailsBtn)
        self.configureButton(btn: paymentDetailsBtn)
        self.configureButton(btn: jobPermissionBtn)
        // container
        self.configureButton(btn: cInvisibleCompanyProfileBtn)
        self.configureButton(btn: cVisibleCompanyProfileBtn)
        self.configureButton(btn: cConnectionDetailsBtn)
        self.configureButton(btn: cPaymentDetailsBtn)
        self.configureButton(btn: cJobPermissionBtn)
    }
    
    func setIconFont(){
        
        // employer
        invisibleCompanyProfileBtn.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_DetaisCard, for: .normal)//people-2
       visibleCompanyProfileBtn.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)//people-2
        // container
        cInvisibleCompanyProfileBtn.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_DetaisCard, for: .normal)//people-2
        cVisibleCompanyProfileBtn.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)//people-2
    }
    
    func setBackgroundcolorToBtn(btn:UIButton, color:UIColor){
        btn.backgroundColor = color
    }
    
    func setTextcolorToBtn(btn:UIButton, color:UIColor)
    {
        btn.titleLabel?.textColor = color
        btn.tintColor = color
    }
    func setBorderToBtn(btn:UIButton, color:UIColor , width:CGFloat)
    {
        btn.layer.borderColor = color.cgColor
        btn.layer.borderWidth = width
    }
    
    func setFontToButton(btn:UIButton, name:String, size:CGFloat)
    {
        btn.titleLabel!.font =  UIFont(name: name, size: size)
        //btn.titleLabel?.backgroundColor = UIColor.clear
    }
    
    func configureButton(btn:UIButton){
        let f : CGFloat = btn.frame.width / 2
        btn.layer.cornerRadius = f
    }
    
    func setDesignToUserImg() {
        // 1
        let layer = CALayer()
        layer.frame = userImg.bounds
        
        // 2
        layer.contents = userImg.image
        layer.contentsGravity = kCAGravityCenter
        
        // 3
        layer.magnificationFilter = kCAFilterLinear
        layer.isGeometryFlipped = false
        
        // 4
        layer.backgroundColor = UIColor.clear.cgColor
        layer.opacity = 1.0
        layer.isHidden = false
        layer.masksToBounds = false
        
        // 5
        layer.cornerRadius = userImg.frame.height/2
        layer.borderWidth = 7.0
        layer.borderColor = UIColor.gray.cgColor
        
        // 6
        layer.shadowOpacity = 0.75
        layer.shadowOffset = CGSize(width: 3, height: 2)
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize.zero
        userImg.layer.addSublayer(layer)
        
        userImg.layer.borderColor = UIColor.gray.cgColor
        userImg.layer.borderWidth = 7
        
        userImg.layer.cornerRadius = userImg.frame.height/2
        userImg.layer.masksToBounds = true
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(self.imageTapped(img:)))
        userImg.isUserInteractionEnabled = true
        userImg.addGestureRecognizer(tapGestureRecognizer1)
        
        let layer2 = layer
        layer2.frame = imgBorderView.bounds
        layer2.contents = UIColor.clear
        layer2.cornerRadius = imgBorderView.frame.height/2
        layer2.backgroundColor = UIColor.clear.cgColor
        layer2.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 0.25
        imgBorderView.layer.addSublayer(layer2)
        imgBorderView.layer.cornerRadius = imgBorderView.frame.height/2
        imgBorderView.backgroundColor = UIColor.clear
    }
    //MARK: -- End Of Set Design Of All Views
    
    //MARK: - Camera Func
    func imageTapped(img: AnyObject)
    {
        //        var stringURL: String = "ibooks://"
        //        var url = URL(string: stringURL)
        //        UIApplication.shared.openURL(url!)
        
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
//            print("Button capture")
//            
//            
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
//            imagePicker.allowsEditing = false
//            
//            self.present(imagePicker, animated: true, completion: nil)
//        }
        isUserImage = true
        showCameraActionSheet()
    }
    
    func showCameraActionSheet()
    {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "צלם", style: .default, handler: {(alert: UIAlertAction!) in self.openCamera()})
        let libraryAction = UIAlertAction(title: "העלה תמונה", style: .default, handler: {(alert: UIAlertAction!) in self.openLibrary()})
        
        let cancelAction = UIAlertAction(title: "ביטול", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            Global.sharedInstance.bIsOpenFiles = true
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
//            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            Global.sharedInstance.bIsOpenFiles = true
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL{
            if let imageName = imageUrl.lastPathComponent{
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage{
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        if isUserImage {
                            isUserImage = false
                            userImg.contentMode = .scaleToFill
                            userImg.image = image
                            Global.sharedInstance.user.profileImage = image
                            setImageProfileInServer(imageName: imageName, image: imageString)
                        } else {
                            self.uploadFilesDelegate.getFile!(imgName: imageName, img: imageString, displayImg: image)
                        }
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
            }
        } else {
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    if isUserImage {
                        isUserImage = false
                        userImg.contentMode = .scaleToFill
                        userImg.image = image
                        Global.sharedInstance.user.profileImage = image
                        setImageProfileInServer(imageName: imageName, image: imageString)
                    } else {
                        self.uploadFilesDelegate.getFile!(imgName: imageName, img: imageString, displayImg: image)
                    }
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func setImageProfileInServer(imageName:String,image:String){
        let imageObj = FileObj()
        imageObj.nvFile = image
        imageObj.nvFileName = imageName
        var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicToServer["image"] = imageObj.getImageDic() as AnyObject?
        dicToServer["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dicToServer["iUserType"] = Global.sharedInstance.user.iUserType.rawValue as AnyObject?
        api.sharedInstance.UpdateImage(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
            }else{
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })
        
    }

    func getUserImage() {
        api.sharedInstance.GetImage(params: ["iUserId":(Global.sharedInstance.user.iUserId as AnyObject?)!], success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
                if let imagString = dicObj["Result"]?["nvFileURL"] as? String{
                    var stringPath = api.sharedInstance.buldUrlFile(fileName: imagString)
                    //                    let aString = "This is my string"
                    stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                    if let imageUrl = URL(string:stringPath){
                        self.downloadImage(url: imageUrl)
                    }
                }
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    self.userImg.contentMode = .scaleToFill
                    self.userImg.image = image
                }
                Global.sharedInstance.user.profileImage = image
                if Global.sharedInstance.user.picture.nvFile.isEmpty {
                    Global.sharedInstance.user.picture.nvFile = Global.sharedInstance.convertImageToBase64(image: image)
                }
            }
        }
        
    }
    
    //MARK: - scroll view delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < CGPoint.zero.y {
            scrollView.contentOffset = CGPoint.zero
        }
        
        if scrollView.contentOffset.y > 185{
            menuContainerView.isHidden = false
        }else{
            menuContainerView.isHidden = true
        }
    }
    
    func changeScrollViewHeight(heightToadd : CGFloat) {
     //   scrollView.contentSize = CGSize(width: scrollView.frame.width, height: scrollView.contentSize.height + heightToadd)
        setScrollViewEnable(isEnable: true)
        scrollView.contentSize.height += heightToadd
        
        if jobLocatioBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            jobLocationVC.view.frame.size.height += heightToadd
            
            setConstrainToTitle(item: jobLocationVC.titleLbl, toItem: jobLocationVC.view)
        } else if jobPreferencesBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            jobPreferencesVC.view.frame.size.height += heightToadd
            //jobPreferencesVC.textHeightFronTopCon.constant = 0
            
            setConstrainToTitle(item: jobPreferencesVC.titleLbl, toItem: jobPreferencesVC.view)

        } else if jobAvailabilityBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            jobAvailabilityVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: jobAvailabilityVC.titleLbl, toItem: jobAvailabilityVC.view)
            
        } else if invisibleCompanyProfileBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            employerInvisibleProfileVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: employerInvisibleProfileVC.titleLbl, toItem: employerInvisibleProfileVC.view)
            
        } else if visibleCompanyProfileBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            companyVisibleProfileVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: companyVisibleProfileVC.titleLbl, toItem: companyVisibleProfileVC.view)
            
        } else if connectionDetailsBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            connectionDetailsVC.view.frame.size.height += heightToadd
//            setConstrainToTitle(item: connectionDetailsVC.titleLbl, toItem: connectionDetailsVC.view)
            
        } else if visibleCompanyProfileBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            companyVisibleProfileVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: companyVisibleProfileVC.titleLbl, toItem: companyVisibleProfileVC.view)
            
        } else if jobPermissionBtn.backgroundColor == ControlsDesign.sharedInstance.colorTurquoiseWithOpacity {
            employmentAuthorizedVC.view.frame.size.height += heightToadd
            setConstrainToTitle(item: employmentAuthorizedVC.titleLbl, toItem: employmentAuthorizedVC.view)
        }
    }
    
    func setScrollViewEnable(isEnable: Bool) {
        scrollView.isScrollEnabled = isEnable
    }
    
    func setHeightToScroll(height : CGFloat) {
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: menuView.frame.height + height)
        
//        scrollHeightCons.constant = menuView.frame.height + height
        
        //viewUnderScroll.frame = CGRect(x: 0, y: 0, width: viewUnderScroll.frame.width , height: scrollView.contentSize.height)
    }
    
    func setConstrainToTitle(item : Any, toItem : Any?) {
        let horizontalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 10)
        let widthConstraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.width, multiplier: 0.9, constant: 100)
        
        (toItem as! UIView).addConstraints([horizontalConstraint, verticalConstraint, widthConstraint])
    }
    
    //MARK: - open first screen
    func openFirstPage(isEmployer : Bool) {
        DispatchQueue.main.async {
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            self.openNextTab(tag: self.menuTag)
        }
        
//        if isEmployer {
//            // open employer invisible profile
//            setBackgroundcolorToBtn(btn: invisibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//            setBackgroundcolorToBtn(btn: visibleCompanyProfileBtn, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: connectionDetailsBtn, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: paymentDetailsBtn, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: jobPermissionBtn, color: UIColor.clear)
//            getEmployerInvisibleProfileForm()
//        } else {
//            // open preferences job screen
//            setBackgroundcolorToBtn(btn: jobPreferencesBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//            setBackgroundcolorToBtn(btn: jobLocatioBtn, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: jobAvailabilityBtn, color: UIColor.clear)
//            getJobPreferencesForm()
//        }
    }
    
    //MARK: - screens of employee : preferences job
    func getJobPreferencesForm() {
        scrollView.addSubview(jobPreferencesVC.view)
        setHeightToScroll(height: /*jobPreferencesVC.view.frame.height*/667 * 0.6)
        //jobPreferencesVC.scrollView = self.scrollView
        jobPreferencesVC.changeScrollViewHeightDelegate = self
        jobPreferencesVC.getTextFieldDelegate = self
        jobPreferencesVC.openAnotherPageDelegate = self
        
        registerKeyboardNotifications()
    }
    
    func getJobLocationForm() {
        scrollView.addSubview(jobLocationVC.view)
        setHeightToScroll(height: jobLocationVC.view.frame.height)
        jobLocationVC.changeScrollView = self
        
        jobLocationVC.getTextFieldDelegate = self
        
        registerKeyboardNotifications()
//        unregisterKeyboardNotifications()
    }
    
    func getJobAvailibilityForm() {
        scrollView.addSubview(jobAvailabilityVC.view)
        setHeightToScroll(height: /*jobAvailabilityVC.view.frame.height*/667 * 0.6)
        jobAvailabilityVC.changeScrollViewHeightDelegate = self
        unregisterKeyboardNotifications()
    }
    
    //MARK: - screen of employer : personal details
    func getEmployerInvisibleProfileForm() {
        scrollView.addSubview(employerInvisibleProfileVC.view)
        setHeightToScroll(height: /*jobAvailabilityVC.view.frame.height*/667 * 0.6)
        employerInvisibleProfileVC.getTextFieldDelegate = self
        employerInvisibleProfileVC.saveEmployerDelegate = self
        employerInvisibleProfileVC.openAnotherPageDelegate = self
        registerKeyboardNotifications()
//        unregisterKeyboardNotifications()
    }
    
    func getCompanyVisibleProfileForm() {
        scrollView.addSubview(companyVisibleProfileVC.view)
        setHeightToScroll(height: companyVisibleProfileVC.view.frame.height)
        companyVisibleProfileVC.uploadFilesDelegates = self
        self.uploadFilesDelegate = companyVisibleProfileVC
        companyVisibleProfileVC.saveEmployerDelegate = self
        companyVisibleProfileVC.getTextFieldDelegate = self
        registerKeyboardNotifications()
//        unregisterKeyboardNotifications()
    }
    
    func getConnectionDetailsForm(){
        scrollView.addSubview(connectionDetailsVC.view)
        setHeightToScroll(height: 667 * 0.5)
        connectionDetailsVC.getTextFieldDelegate = self
        //connectionDetailsVC.openAnotherPageDelegate = self
        registerKeyboardNotifications()
    }
    
    func getPaymentDetailsForm() {
        scrollView.addSubview(companyPaymentDetailsVC.view)
        setHeightToScroll(height: companyPaymentDetailsVC.view.frame.height)
        companyPaymentDetailsVC.uploadFilesDelegate = self
        self.uploadFilesDelegate = companyPaymentDetailsVC
        companyPaymentDetailsVC.saveEmployerDelegate = self
        companyPaymentDetailsVC.openAnotherPageDelegate = self
        unregisterKeyboardNotifications()
    }
    
    func getEmploymentAuthorizedForm() {
        scrollView.addSubview(employmentAuthorizedVC.view)
        setHeightToScroll(height: /*employmentAuthorizedVC.view.frame.height*/667 * 0.4)
        employmentAuthorizedVC.changeScrollViewDelegate = self
        employmentAuthorizedVC.openAnotherPageDelegate = self
        unregisterKeyboardNotifications()
    }
    
    func removeAllViews() {
//        registerKeyboardNotifications()
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker:
            removeViewsOfWorker()
            break
        case UserType.employer:
            removeViewOfEmployer()
            break
        default:
            break
        }
    }
    
    func removeViewsOfWorker() {
        // -- remove background
        setBackgroundcolorToBtn(btn: jobPreferencesBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: jobLocatioBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: jobAvailabilityBtn, color: UIColor.clear)
        // container buttons
        setBackgroundcolorToBtn(btn: cJobPreferencesBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: cJobLocatioBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: cJobAvailabilityBtn, color: UIColor.clear)
        
        // -- remove pages
        jobPreferencesVC.view.removeFromSuperview()
        jobLocationVC.view.removeFromSuperview()
        jobAvailabilityVC.view.removeFromSuperview()
    }
    
    func removeViewOfEmployer() {
        // -- remove background
        setBackgroundcolorToBtn(btn: invisibleCompanyProfileBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: visibleCompanyProfileBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: connectionDetailsBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: paymentDetailsBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: jobPermissionBtn, color: UIColor.clear)
        // container buttons
        setBackgroundcolorToBtn(btn: cInvisibleCompanyProfileBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: cVisibleCompanyProfileBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: cConnectionDetailsBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: cPaymentDetailsBtn, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: cJobPermissionBtn, color: UIColor.clear)
        
        // -- remove pages
        employerInvisibleProfileVC.view.removeFromSuperview()
        companyVisibleProfileVC.view.removeFromSuperview()
        connectionDetailsVC.view.removeFromSuperview()
        companyPaymentDetailsVC.view.removeFromSuperview()
        employmentAuthorizedVC.view.removeFromSuperview()
    }
    

    //MARK: - Server functions
    func getWorker() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        let worker = Worker()
                        Global.sharedInstance.worker = worker.getWorkerFromDictionary(dic: dicResult)
                        self.openFirstPage(isEmployer: false)
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
                
                //if dic["Error"]?["iErrorCode"] as! Int ==  2
                //{
                //    Alert.sharedInstance.showAlert(mess: "מייל לא קיים במערכת")
                //}
                //else
                //{
                //    Alert.sharedInstance.showAlert(mess: "נשלח אליך מייל עם הסיסמה")
                //}
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorker")
    }
    
    func getEmployer() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iEmployerId"] = Global.sharedInstance.user.iEmployerId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
//            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        let employer = Employer()
                        Global.sharedInstance.employer = employer.getEmployerFromDic(dic: dicResult)
                        
//                        let quewe = OperationQueue()
//                        quewe.addOperation {
                        
                        
                            self.openFirstPage(isEmployer: true)
                        
//                        }
                    
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
                
                //if dic["Error"]?["iErrorCode"] as! Int ==  2
                //{
                //    Alert.sharedInstance.showAlert(mess: "מייל לא קיים במערכת")
                //}
                //else
                //{
                //    Alert.sharedInstance.showAlert(mess: "נשלח אליך מייל עם הסיסמה")
                //}
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetEmployer)
    }
    
    func updateEmployer() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["employer"] = Global.sharedInstance.employer.getDictionaryFromEmployer() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        Global.sharedInstance.employer.employerPaymentMethod.bBankAccountDetailsUpdated = false
                    print(dic as Any)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            //            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        if dicResult {
                            Global.sharedInstance.bWereThereAnyChanges = false
                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                            if self.menuTag == 2 && self.companyVisibleProfileVC.logoImg.image != nil && Global.sharedInstance.employer.logoImage.nvFileURL == "" {
                                self.userImg.image = self.companyVisibleProfileVC.logoImg.image
                                self.setImageProfileInServer(imageName: Global.sharedInstance.employer.logoImage.nvFileName, image: Global.sharedInstance.employer.logoImage.nvFile)
                            }
                            
                            if Global.sharedInstance.user.iUserType == UserType.employer {
//                                OperationQueue.main.addOperation {
//                                self.openNextTab(tag: self.menuTag)
                                    self.getEmployer()
//                                }
                              
                            } else {
//                                OperationQueue.main.addOperation {
                                    self.openNextTab(tag: self.menuTag)
//                                }
                            }
                            
//                            self.userImg.contentMode = .scaleToFill
//                            self.userImg.image = image
                        } else {
                            Alert.sharedInstance.showAlert(mess: "ארעה בעיה בשמירת הפרטים!")
                        }
                        //                        self.openFirstPage(isEmployer: true)
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
                
                //if dic["Error"]?["iErrorCode"] as! Int ==  2
                //{
                //    Alert.sharedInstance.showAlert(mess: "מייל לא קיים במערכת")
                //}
                //else
                //{
                //    Alert.sharedInstance.showAlert(mess: "נשלח אליך מייל עם הסיסמה")
                //}
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateEmployer)
    }

    //MARK: - KeyBoard
    //MARK: - keyBoard notifications
    var txtSelected = UITextField()
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShowTemp(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    var scrollHeight : CGFloat = 0
    
    func keyboardDidShow(notification: NSNotification)
    {
//        if self.scrollView.contentOffset.y <= 0 {
            //Need to calculate keyboard exact size due to Apple suggestions
        scrollHeight = scrollView.contentSize.height
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        
        //        if txtSelected.tag == 1 {  // text field on a view
        
        var bRect : CGRect = txtSelected.frame
        
        // to do : slash to check
        if txtSelected.tag == 1 {
            bRect.origin.y += (txtSelected.superview?.frame.origin.y)!
        } else if txtSelected.tag == 2 {
            bRect.origin.y += ((txtSelected.superview?.frame.origin.y)! + 130)
        }
        
        if (!aRect.contains(bRect)) {
            bRect.origin.y = bRect.origin.y - aRect.height  + bRect.height
            bRect.origin.x = 0
            self.scrollView.setContentOffset(bRect.origin, animated: true)
        }
        
        if txtSelected.tag == 2 {
            bRect.origin.x = 0
            self.scrollView.setContentOffset(bRect.origin, animated: true)
        }
//        }
    }
    
    func keyboardDidShowTemp(notification : NSNotification) {
        var aRect: CGRect = view.frame
        if txtSelected.tag != 2 {
        var info: [AnyHashable: Any]? = notification.userInfo
        let kbSize: CGSize? = (info?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (kbSize?.height)!, 0.0)
        scrollView.contentInset = contentInsets!
        scrollView.scrollIndicatorInsets = contentInsets!
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        
        aRect.size.height -= (kbSize?.height)!
        }
        //brect
        var bRect: CGRect = txtSelected.frame
        
        if txtSelected.tag == 1 || txtSelected.tag == 2 {
            bRect.origin.y += (txtSelected.superview?.frame.origin.y)!
        }
        
        if txtSelected.tag == 2 {
            bRect.origin.y += 100
            let scrollPoint = CGPoint(x: CGFloat(0.0), y: bRect.origin.y)
            scrollView.setContentOffset(scrollPoint, animated: true)
        }
     else
       if !aRect.contains(bRect.origin) {
                        let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y))
//            let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y - (kbSize?.height)!))
            scrollView.setContentOffset(scrollPoint, animated: true)
       }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        //Once keyboard disappears, restore original positions
//        let info : NSDictionary = notification.userInfo! as NSDictionary
//        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
//        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
//        
////        self.scrollView.contentInset = contentInsets
//        
//        
//        self.scrollView.scrollIndicatorInsets = contentInsets
////        changeScrollViewHeight(heightToadd: -((keyboardSize?.height)!))
//        
//        self.view.endEditing(true)
//        
////        scrollView.contentSize.height = scrollHeight
////        self.scrollView.isScrollEnabled = false
        
        
        
        
        var contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func getTextField(textField: UITextField, originY: CGFloat) {
        self.txtSelected = textField
    }
    
    // UploadFileDelegate
    func uploadFile(fileUrl : String) {
        if fileUrl != "" { // האם שמור קובץ בשרת
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
            
            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
            controller?.fileUrl = fileUrl
            
            self.present(controller!, animated: true, completion: nil)
        } else {
            showCameraActionSheet()
        }
    }
    
    // SaveEmployerDelegate
    func saveEmployer(goNextTab : Int) {
        self.menuTag = goNextTab
        
        
//        openNextTab(tag: goNextTab)
        
        self.updateEmployer()
        
//        self.getEmployer()
       
        
    }
    
    // OpenAnotherPageDelegate
    func openManual() {
        let story = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ChooseBankViewController") as! ChooseBankViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openNextTab(tag: Int) {
        
        // now just for employer
        removeAllViews()
        Global.sharedInstance.bIsOpenFiles = false
        setScrollViewEnable(isEnable: true)
        self.scrollView.contentOffset = CGPoint.zero
        
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker:
            setScreenByTagForWorker(tag: tag)
            break
        case UserType.employer:
            setScreenByTagForEmployer(tag: tag)
            break
        default:
            break
        }
        
  scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
//        scrollView.scrollsToTop = true
        

    }
    
    func setScreenByTagForWorker(tag : Int) {
        switch tag {
        // worker screen
        case 0:
            setBackgroundcolorToBtn(btn: jobPreferencesBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobPreferencesBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getJobPreferencesForm()
            break
        case 1:
            setBackgroundcolorToBtn(btn: jobLocatioBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobLocatioBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getJobLocationForm()
            break
        case 2:
            setBackgroundcolorToBtn(btn: jobAvailabilityBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobAvailabilityBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getJobAvailibilityForm()
            break
            
        default:
            break
        }

    }
    
    func setScreenByTagForEmployer(tag : Int) {
        switch tag {
        // employer screen
        case 0:
            setBackgroundcolorToBtn(btn: invisibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cInvisibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getEmployerInvisibleProfileForm()
            
            break
        case 1:
            setBackgroundcolorToBtn(btn: visibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cVisibleCompanyProfileBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getCompanyVisibleProfileForm()
//            getEmploymentAuthorizedForm()
            break
        case 2:
            setBackgroundcolorToBtn(btn: paymentDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cPaymentDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getPaymentDetailsForm()
            break
        case 3:
            setBackgroundcolorToBtn(btn: jobPermissionBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cJobPermissionBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getEmploymentAuthorizedForm()
//            getCompanyVisibleProfileForm()
            break
        case 4:
            setBackgroundcolorToBtn(btn: connectionDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            setBackgroundcolorToBtn(btn: cConnectionDetailsBtn, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
            getConnectionDetailsForm()
            break

        default:
            break
        }

    }
    
    //MARK: - BasePopUpActionsDelegate
    func okAction(num: Int) {
        
    }
    
    func cancelAction(num: Int) {
        Global.sharedInstance.bWereThereAnyChanges = false
        if isTapBack {
            setPreviusPage()
//            isTapBack = false
//            unregisterKeyboardNotifications()
//            let _ = self.navigationController?.popViewController(animated: true)
        }else {
            if tabSenderBtn != nil {
                setNextPageIfThereWereNotAnyChanges(sender: tabSenderBtn!)
            
            }
        }
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם ברצונך לשמור לפני היציאה?"
        
        self.present(basePopUp, animated: true, completion: nil)
    }
    
    func setPreviusPage() {
        isTapBack = false
        unregisterKeyboardNotifications()
        if /*Global.sharedInstance.user.iUserType == UserType.employer, */let vc = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2] {
            if vc is EmployeeModelViewController {
                let _ = self.navigationController?.popViewController(animated: true)
            } else {
                let story = UIStoryboard(name: "Main", bundle: nil)
                let viewCon = story.instantiateViewController(withIdentifier: "EmployeeModelViewController") as? EmployeeModelViewController
                self.navigationController?.pushViewController(viewCon!, animated: true)
            }
        } else {
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }

}
