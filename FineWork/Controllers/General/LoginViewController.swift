

import UIKit
//import Google
import GoogleSignIn

class
LoginViewController: UIViewController,GIDSignInUIDelegate,GIDSignInDelegate,UITextFieldDelegate {
 
    var employeeModelViewController:EmployeeModelViewController?
    
    @IBOutlet weak var Beye_pass: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnLoginWithFacebook: UIButton!
    @IBOutlet var btnLoginWithGoogle: UIButton!
    @IBOutlet var btnRegister: UIButton!
    
    //MARK:-- TextFields
    @IBOutlet var txtMail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    //MARK:-- Labels
    @IBOutlet var lblForgotPassword: UILabel!
    @IBOutlet var lblIsNotYetRegister: UILabel!
    //MARK:-- Actions
    
    @IBAction func btnchange(_ sender: UIButton) {
    
        if txtPassword.isSecureTextEntry{
            txtPassword.isSecureTextEntry = false
            Beye_pass.setTitle("\u{f070}", for: .normal)//colse eye
        }else{
            txtPassword.isSecureTextEntry = true
            Beye_pass.setTitle("\u{f06e}", for: .normal)//open eye
            
        }
    }
    @IBAction func btnLoginAction(_ sender: Any) {
        
        //        if !isSimpleLogin
        //        {
        //            isSimpleLogin = true
        if isFirst == true
        {
            isFirst = false
            self.login()
        }
        //}
        
    }
    func login()
    {
        let user = User()
        if checkValidation() == true
        {
            user.nvMobile = txtMail.text!
            user.nvPassword = txtPassword.text!
            loginToServer(user: user)
        }
        else
        {
            isFirst = true
        }
        
    }
    
    func checkValidation() -> Bool{
        check = true
        //check validation:
        if txtMail.text == "" || txtPassword.text! == ""
           // || Validation.sharedInstance.isValidEmail(testStr: txtMail.text!) == false
        {
            check = false
        }
        
        if check == false
        {
            self.showAlert(sendMessage: (NSLocalizedString("FIELDS_NOT_FILLED", comment: "")))
        }
        
        //        else if check{
        //            self.check = false
        //        }
        
        return check
    }
    
    func loginToServer(user:User){
        Global.sharedInstance.bWereThereAnyChanges = false
        var dic = Dictionary<String, AnyObject>()
        dic = user.getUserDictionaryForLogin()
        print("---Login:\n\(dic)")
        api.sharedInstance.Login(params: dic,success:{(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0
                {
                    UserDefaultManagment.sharedInstance.setUserName(value: self.txtMail.text!)
                    UserDefaultManagment.sharedInstance.setPassword(value: self.txtPassword.text!)

                    Global.sharedInstance.user = Global.sharedInstance.user.dicToUser(dicUser: dic["Result"] as! NSDictionary)
                    self.openNextPageByUserType()
//                    self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
                }
                else if dic["Error"]?["iErrorCode"] as! Int ==  2
                {
                    self.isFirst = true
                    self.showAlert(sendMessage:dic["Error"]?["nvErrorMessage"] as! String)
                }
                else
                {
                    self.isFirst = true
                    self.isSimpleLogin = false
                    self.showAlert(sendMessage:dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            self.isSimpleLogin = false
            self.isFirst = true
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        })
 
    }
    
    @IBAction func btnLoginWithFacebookAction(_ sender: Any) {
        deleteDataFromUserDefault()
        let login = FBSDKLoginManager()
        login.logIn(withReadPermissions: ["email"], from: self, handler: { (result, error) -> Void in
            if (error != nil) {
                // Process error
            }
            else if (result?.isCancelled)! {
                // Handle cancellations
            }
            else {
                if (result?.grantedPermissions.contains("email"))! {
                    print("result is:\(result)")
                    self.fetchUserInfo()
                }
            }
        })
    }
    
    @IBAction func btnLoginWithGoogleAction(_ sender: Any) {
        deleteDataFromUserDefault()
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        
        let destination = (storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController")as? RegistrationViewController?)!
        
        self.navigationController?.pushViewController(destination!, animated: true)
        
    }
    
    //MARK:- Varibals
    var check:Bool = false
    var isSimpleLogin:Bool = false
    var isFirst = true
    
    //MARK:- Function
    //MARK:-- initial Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.setDesign()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
//        viewCon = storyboard?.instantiateViewController(withIdentifier: "PersonalInformationModelViewController")as?PersonalInformationModelViewController
        
//        viewCon101 = (storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailsViewController")as?EmployeeDetailsViewController?)!
        employeeModelViewController = (storyboard?.instantiateViewController(withIdentifier: "EmployeeModelViewController")as?EmployeeModelViewController)!
        
        txtMail.returnKeyType = UIReturnKeyType.next
        txtPassword.returnKeyType = UIReturnKeyType.done
        
        txtMail.delegate = self
        txtPassword.delegate = self
        
       /// txtMail.frame = CGRectMake(20,20,200,800)
//xtMail = .sizeToFit
        txtMail.textAlignment = .right
        txtPassword.textAlignment = .right

        let myColor : UIColor = UIColor.blue2
        txtMail.layer.borderColor = myColor.cgColor
        txtPassword.layer.borderColor = myColor.cgColor
        
        txtMail.layer.borderWidth = 1.0
        txtPassword.layer.borderWidth = 1.0
    
        let forgotPasswordTap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.forgotPassword))
        lblForgotPassword.isUserInteractionEnabled = true
        lblForgotPassword.addGestureRecognizer(forgotPasswordTap)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
       // txtMail.text = ""
        //txtPassword.text = ""
        isFirst = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    //MARK:-- local Functions
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(sendMessage: String){
        let alert : UIAlertController = UIAlertController(title: "", message: sendMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction:UIAlertAction = UIAlertAction(title: NSLocalizedString((LocalizableStringStatic.sharedInstance.OK_ALERT), comment: ""), style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            //self.dismiss()
        })
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func forgotPassword(){
        lblForgotPassword.text = "שלח סיסמא"
        txtPassword.isHidden = true
        Beye_pass.isHidden = true
        
        if txtMail.text == ""{
            Alert.sharedInstance.showAlert(mess: "יש להזין טלפון תחילה")
        }else{
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            dic["nvMail"] = txtMail.text as AnyObject?
            dic["bLoginFromApp"] = true as AnyObject?
//            print("---ForgotPassword:\n\(dic)")
            api.sharedInstance.ForgotPassword(params: dic,success:{(operation:AFHTTPRequestOperation?, responseObject:Any?)
                -> Void in
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    if dic["Error"]?["iErrorCode"] as! Int ==  3
                    {
                        Alert.sharedInstance.showAlert(mess: "שליחת המייל נכשלה")
                    }
                    else if dic["Error"]?["iErrorCode"] as! Int ==  2
                    {
                       Alert.sharedInstance.showAlert(mess: "מייל לא קיים במערכת")
                    }
                    else
                    {
                        Alert.sharedInstance.showAlert(mess: "נשלח אליך מייל עם הסיסמה")
                        self.lblForgotPassword.text = "שכחתי סיסמא"
                        self.txtPassword.isHidden = false
                      //  self.lblIconFontPassword.isHidden = false
                        
                    }
                }
            } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            })

        }
            }
    
    //MARK:-- SETTERS Functions
    
    func setDesign(){
        setBackgroundcolor()
        setImages()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
        self.setLocalizableString()
        setUserDefault()
    }
    
    func setBackgroundcolor(){
        self.view.backgroundColor = ControlsDesign.sharedInstance.colorTurquoise
        //self.btnLogin.backgroundColor = ControlsDesign.sharedInstance.colorPurple
    }
    
    func setImages(){
       // imgLogo.image = UIImage(named: NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_IMAGE_LOGO, comment: ""))
    }
    
    func setBorders(){
        //ControlsDesign.sharedInstance.addBottomBorderWithColor(color: UIColor.orange, width: 1.5, any: txtMail)
        //ControlsDesign.sharedInstance.addBottomBorderWithColor(color: ControlsDesign.sharedInstance.colorGray, width: 1.5, any: txtPassword)
    }
    
    func setTextcolor(){
        Beye_pass.setTitle("\u{f06e}", for: .normal)
       // btnLoginWithGoogle.titleLabel?.textColor = ControlsDesign.sharedInstance.colorRed
       // btnLoginWithFacebook.titleLabel?.textColor = ControlsDesign.sharedInstance.colorBlue
      //  txtMail.textColor = ControlsDesign.sharedInstance.colorGray
       // txtPassword.textColor = ControlsDesign.sharedInstance.colorGray
        //lblHeaderLogin.textColor = ControlsDesign.sharedInstance.colorDarkPurple
      //  lblForgotPassword.textColor = ControlsDesign.sharedInstance.colorPurple
       // lblIsNotYetRegister.textColor = ControlsDesign.sharedInstance.colorPurple
        btnRegister.titleLabel?.textColor = ControlsDesign.sharedInstance.colorPurple
        //lblIconFontMail.textColor = ControlsDesign.sharedInstance.colorDarkPurple
       // lblIconFontPassword.textColor = ControlsDesign.sharedInstance.colorDarkPurple
    }
    
    func setCornerRadius(){
      //  btnLoginWithGoogle.layer.cornerRadius = btnLoginWithGoogle.frame.height / 2
       // btnLoginWithFacebook.layer.cornerRadius = btnLoginWithFacebook.frame.height / 2
       // btnLogin.layer.cornerRadius = btnLogin.frame.height / 2
    }
    
    func setIconFont(){
       // lblIconFontMail.text = FlatIcons.sharedInstance.LOGIN_MAIL//"\u{f003}"
      //  lblIconFontPassword.text = FlatIcons.sharedInstance.LOGIN_PASSWORD//"\u{f023}"
    }
    
    func setLocalizableString(){
        //SET TEXT TO BUTTONS
        btnLogin.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN, comment: ""), for: .normal)
        //btnLoginWithFacebook.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_WITH_FACEBOOK, comment: ""), for: .normal)
        //btnLoginWithGoogle.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_WITH_GOOGLE, comment: ""), for: .normal)
        btnRegister.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_REGISTER, comment: ""), for: .normal)
        
        //SET TEXT TO LABELS
        lblForgotPassword.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_FORGOT_PASSWORD, comment: "")
        lblIsNotYetRegister.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_IS_NOT_YET_REGISTER, comment: "")
     //   lblHeaderLogin.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_TITLE, comment: "")
        //SET TEXT TO txtPlaceHolder
        txtPassword.placeholder = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_PASSWORD, comment: "")
        txtMail.placeholder = "טלפון" //NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_MAIL, comment: "")
    }
    
    func setUserDefault(){
        
      txtMail.text =   UserDefaultManagment.sharedInstance.getUserName()
      txtPassword.text =   UserDefaultManagment.sharedInstance.getPassword()
print("ww \(UserDefaultManagment.sharedInstance.getUserName())")
        print("ww \(UserDefaultManagment.sharedInstance.getPassword())")

    }
    func deleteDataFromUserDefault(){
        
       UserDefaultManagment.sharedInstance.setUserName(value: "")
       UserDefaultManagment.sharedInstance.setPassword(value: "")
        
    }
    //MARK: - TextFields
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        switch textField {
        case txtMail:
            txtPassword.becomeFirstResponder()
        default:
            txtMail.resignFirstResponder()
            dismissKeyboard()
            return true
        }
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.textAlignment = NSTextAlignment.justified
        return true
    }

    
    //MARK: - Facebook
    func fetchUserInfo() {
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    if let data:[String:AnyObject] = result as? [String : AnyObject]{
//                        if let name = data["name"] as? String{
//                            self.txtUserName.text = name
//                        }
                        let user = User()
                        if let email = data["email"] as? String{
                            user.nvMail = email
                        }
                        if let facebookId = data["id"] as? String{
                            user.nvFacebookId = facebookId
                        }
                        self.loginToServer(user: user)
                    }
                }
            })
        }
    }
    
    
    //MARK: GoolePlus
    func sign(_ signIn: GIDSignIn, didSignInFor user: GIDGoogleUser, withError error: Error?) {
        // Perform any operations on signed in user here.
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userNew = User()
            if let userId = user.userID     {
                userNew.nvGoogleId = userId
            }// For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
            if let email = user.profile.email{
                userNew.nvMail = email
            }
            
            

            self.loginToServer(user: userNew)

            GIDSignIn.sharedInstance().disconnect()
            GIDSignIn.sharedInstance().signOut()
            // ...
        } else {
            print("\(error?.localizedDescription)")
        }
    }
    
//    func sign(_ signIn: GIDSignIn, didDisconnectWith user: GIDGoogleUser, withError error: Error?) {
//        // Perform any operations when the user disconnects from app here.
//        print("didDisconnectWith")
//    }
    
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        print("present")
         self.present(viewController, animated: true, completion: nil)
    }
    
//    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
//        myActivityIndicator.stopAnimating()
//    }
//    
//    // Present a view that prompts the user to sign in with Google
//    func signIn(signIn: GIDSignIn!,
//                presentViewController viewController: UIViewController!) {
//        self.presentViewController(viewController, animated: true, completion: nil)
//    }
//    
//    // Dismiss the "Sign in with Google" view
//    func signIn(signIn: GIDSignIn!,
//                dismissViewController viewController: UIViewController!) {
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Open Next Screen
    func openNextPageByUserType() {
        
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker:
            self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
            break
        case UserType.employer:
           // if Global.sharedInstance.user.bPaymentMethod {
                self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
            //} else {
             //   let viewCon =storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
                //self.navigationController?.pushViewController(viewCon!, animated: true)
        // }
            break
        default:
            break
        }
    }
    
}
