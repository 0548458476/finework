//
//  BarContainerViewController.swift
//  FineWork
//
//  Created by User on 20.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class BarContainerViewController: UIViewController {

    //MARK: - Views
    @IBOutlet weak var btn3Points: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var btnExitWidthCon: NSLayoutConstraint!
    //MARK: - Actions
    @IBAction func btn3PointsClicked(_ sender: Any) {
        if btnExitWidthCon.constant == 0{
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.btnExitWidthCon.constant = 150
                self.view.layoutIfNeeded()
            }
        }else{
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.btnExitWidthCon.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnExitClicked(_ sender: Any) {
        GlobalEmployeeDetails.sharedInstance.replaceUser()
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = UINavigationController(rootViewController: loginViewController)
        appdelegate.window!.rootViewController = nav
    }
    
    var titleText : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btn3Points.setTitle("\u{f142}", for: .normal)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        btnExitWidthCon.constant = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        titleLbl.text = titleText
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5) {
            self.btnExitWidthCon.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func setTitle(title : String) {
        if titleLbl != nil {
            titleLbl.text = title
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
