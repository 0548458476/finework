//
//  DialogHearingProcessViewController.swift
//  FineWork
//
//  Created by User on 27/03/2018.
//  Copyright © 2018 webit. All rights reserved.
//

import UIKit

@objc protocol DialogHearingProcessDelegate {
    
    func stopDialogHearingAction(iJobTerminationtId :Int, s:String)
    func nextDialogHearingAction(iJobTerminationtId :Int, s:String)
}

class DialogHearingProcessViewController: UIViewController {

    @IBOutlet weak var LNumMisra: UILabel!
    @IBOutlet weak var Lti: UILabel!
    @IBOutlet weak var TV1: UITextView!
    @IBOutlet weak var TFtguva: UITextField!
    @IBOutlet weak var TV2: UITextView!
    @IBOutlet weak var BtnStop: UIButton!
    @IBOutlet weak var BtnNext: UIButton!
    
    var numMisra :String = ""
    var thana :String = ""
    var iJobTerminationtId :Int = 0
    var dialogHearingProcessDelegate : DialogHearingProcessDelegate! = nil
    
    @IBAction func ActionStop(_ sender: UIButton) {
        if TFtguva.text == ""{
            Alert.sharedInstance.showAlert(mess: "יש להזין תגובה")
        }else{
            if dialogHearingProcessDelegate != nil {
                dismissPopUp()
                dialogHearingProcessDelegate.stopDialogHearingAction(iJobTerminationtId :iJobTerminationtId, s:TFtguva.text!)
            } else {
                dismissPopUp()
            }
        }
    }
    
    @IBAction func ActionNext(_ sender: UIButton) {
        if TFtguva.text == ""{
            Alert.sharedInstance.showAlert(mess: "יש להזין תגובה")
        }else{
            if dialogHearingProcessDelegate != nil {
                dismissPopUp()
                dialogHearingProcessDelegate.nextDialogHearingAction(iJobTerminationtId :iJobTerminationtId, s:TFtguva.text!)
            } else {
                dismissPopUp()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DialogHearingProcessViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        if numMisra != ""{
            LNumMisra.text = "מספר המשרה: "+numMisra
        }
        else{
            LNumMisra.isHidden = true
        }
        TV1.text = "מעסיק בפועל החליט לבקש את הפסקת עבודתך ובטרם נחליט על המשך צעדנו אנו מעונינים לשמוע את טענותיך, הטענה שהועלתה מצד הלקוח שעבדת אצלו:"+" "+thana
        TV2.text = "תגובתך תועבר לבדיקה ונודיעה לך על החלטותנו בהקדם. לסיכום האם הינך מעוניין להמשיך בעבודה או בהפסקה?"
        Lti.text = "אנא תגובתך"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissPopUp() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
