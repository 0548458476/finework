//
//  ChangePasswordPopUpViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 16/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class ChangePasswordPopUpViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {

    //MARK: -- views
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var currentPasswordTxt: UITextField!
    @IBOutlet var newPasswordTxt: UITextField!
    @IBOutlet var verifyPasswordTxt: UITextField!
    @IBOutlet var okBtn: UIButton!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var currentPasswordEyeBtn: UIButton!
    @IBOutlet weak var newPasswordEyeBtn: UIButton!
    @IBOutlet weak var verifyPasswordEyeBtn: UIButton!
    
    //MARK: -- actions
    @IBAction func cancelBtnAction(_ sender : UIButton) {
//        let _ = self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okBtnAction(_ sender : UIButton) {
        //validation
        if validationPasswords() {
            changePasswordDelegate.verificationPassword(newPassword: newPasswordTxt.text!)
            self.dismiss(animated: true, completion: nil)
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים שגויים / אינם תקינים")
        }
    }
    @IBAction func EyeAction(_ sender: UIButton) {
        switch sender {
        case currentPasswordEyeBtn:
            textToChange = currentPasswordTxt
            break
        case newPasswordEyeBtn:
            textToChange = newPasswordTxt
            break
        default:
            textToChange = verifyPasswordTxt
        }
        if textToChange.isSecureTextEntry{
            textToChange.isSecureTextEntry = false
            sender.setTitle(FontAwesome.sharedInstance.openEye, for: .normal)//colse eye
        }else{
            textToChange.isSecureTextEntry = true
            sender.setTitle(FontAwesome.sharedInstance.closedEye, for: .normal)//open eye
            
        }
    }
    
    //MARK: -- variables
    var changePasswordDelegate : ChangePasswordDelegate! = nil
    var txtSelected = UITextField()
    var textToChange : UITextField! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        okBtn.layer.borderWidth = 1
        okBtn.layer.borderColor = UIColor.orange2.cgColor
        okBtn.layer.cornerRadius = okBtn.frame.height / 2
        
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.borderColor = UIColor.orange2.cgColor
        cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2
        
        backgroundView.layer.borderWidth = 1
        backgroundView.layer.borderColor = UIColor.orange2.cgColor
        
        hideKeyboardWhenTappedAround()
        
        currentPasswordTxt.delegate = self
        newPasswordTxt.delegate = self
        verifyPasswordTxt.delegate = self
        
        currentPasswordTxt.tag = 1
        newPasswordTxt.tag = 1
        verifyPasswordTxt.tag = 1
        
        currentPasswordEyeBtn.setTitle(FontAwesome.sharedInstance.closedEye, for: .normal)
        newPasswordEyeBtn.setTitle(FontAwesome.sharedInstance.closedEye, for: .normal)
        verifyPasswordEyeBtn.setTitle(FontAwesome.sharedInstance.closedEye, for: .normal)
        
        currentPasswordTxt.isSecureTextEntry = true
        newPasswordTxt.isSecureTextEntry = true
        verifyPasswordTxt.isSecureTextEntry = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterKeyboardNotifications()
    }
    
    
    func validationPasswords() -> Bool {
        var isValid = true
        if !Validation.sharedInstance.isValidPasswordFinal(testStr: currentPasswordTxt.text!) || currentPasswordTxt.text != Global.sharedInstance.user.nvPassword {
            Global.sharedInstance.setErrorToTextField(sender: currentPasswordTxt)
            isValid = false
//            Alert.sharedInstance.showAlert(mess: "סיסמה נוכחית אינה תקינה")
//            return false
        } else {
            Global.sharedInstance.setValidToTextField(sender: currentPasswordTxt)
        }
        
        if !Validation.sharedInstance.isValidPasswordFinal(testStr: newPasswordTxt.text!) {
            Global.sharedInstance.setErrorToTextField(sender: newPasswordTxt)
            isValid = false
//            Alert.sharedInstance.showAlert(mess: "סיסמה חדשה אינה תקינה")
//            return false
        } else {
            Global.sharedInstance.setValidToTextField(sender: newPasswordTxt)
        }
//        if currentPasswordTxt.text != Global.sharedInstance.user.nvPassword {
//            Global.sharedInstance.setErrorToTextField(sender: currentPasswordTxt)
//            isValid = false
////            Alert.sharedInstance.showAlert(mess: "סיסמה נוכחית שגויה")
////            return false
//        }
        if newPasswordTxt.text != verifyPasswordTxt.text {
            Global.sharedInstance.setErrorToTextField(sender: verifyPasswordTxt)
            isValid = false
//            Alert.sharedInstance.showAlert(mess: "הסיסמאות אינן תואמות")
//            return false
        } else {
            Global.sharedInstance.setValidToTextField(sender: verifyPasswordTxt)
        }
        return isValid
    }

    //MARK: - keyBoard notifications
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification)
    {
        if self.scrollView.contentOffset.y <= 0 {
            //Need to calculate keyboard exact size due to Apple suggestions
            self.scrollView.isScrollEnabled = true
            let info : NSDictionary = notification.userInfo! as NSDictionary
            let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
            let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            
            var aRect : CGRect = self.view.frame
            aRect.size.height -= keyboardSize!.height
            
            //        if txtSelected.tag == 1 {  // text field on a view
            
            var bRect : CGRect = txtSelected.frame
            
            bRect.origin.y += txtSelected.tag == 1 ? (txtSelected.superview?.frame.origin.y)! : txtSelected.frame.origin.y
            if (!aRect.contains(bRect)) {
                bRect.origin.y = bRect.origin.y - aRect.height  + bRect.height
                bRect.origin.x = 0
                self.scrollView.setContentOffset(bRect.origin, animated: true)
            }

        }
        
    }
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        //characteristicsTxt.resignFirstResponder()
        //studiesTxt.resignFirstResponder()
        //jobExperienceTxt.resignFirstResponder()
        //matchingJobsTxt.resignFirstResponder()
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        txtSelected = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case currentPasswordTxt:
            newPasswordTxt.becomeFirstResponder()
            break
        case newPasswordTxt:
            verifyPasswordTxt.becomeFirstResponder()
            break
        default:
            textField.endEditing(true)
        }
        return false
    }
 }
