//
//  StatusJobViewController.swift
//  FineWork
//
//  Created by User on 01/09/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class StatusJobViewController: UIViewController , BasePopUpActionsDelegate , JobDetailsDelegate{
    
    //MARK: -- Variables
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewHeightConstrin: NSLayoutConstraint!
    
    @IBOutlet weak var viewJobDetails: UIView!
    @IBOutlet weak var viewJobDetailsHeigthConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var mainStatusView: UIView!
    @IBOutlet weak var mainStatusViewHeigthConstain: NSLayoutConstraint!
    
    @IBOutlet weak var animStatus: UIImageView!
    @IBOutlet weak var ststusText: UILabel!
    @IBOutlet weak var statusContent: UILabel!
    
    @IBOutlet weak var cancelBtn: UILabel!
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    
    //want_your_view
    @IBOutlet weak var moreDetailsView: UIView!
    @IBOutlet weak var hourlyWageTxt: UILabel!
    @IBOutlet weak var hourlyWageContent: UITextField!
    @IBOutlet weak var hourlyWageLine: UIView!
    @IBOutlet weak var moreTxt: UILabel!
    @IBOutlet weak var moreTxtLine: UIView!
    @IBOutlet weak var moreAction: UIButton!
    @IBOutlet weak var moreActionToCancel: UIButton!
    //MARK: -- Action
    
    @IBAction func chatBtnClick(_ sender: Any) {
    }
    
    @IBAction func videoBtnClick(_ sender: Any) {
    }
    
    @IBAction func moreActionBtnClick(_ sender: Any) {
        var status : Int = 0
        if  workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.want_your.rawValue
        {
            if workerOfferedJob.iOfferJobStatusType == EmployerJobWorkerType.KnownWorker.rawValue {
                if hourlyWageContent.text != "" {
                    workerOfferedJob.mAskingHourlyWage = Float(hourlyWageContent.text!)
                    status = OfferJobStatusType.active.rawValue
                    UpdateWorkerOfferedJobStatusKnownWorker(newStatus: status)
                    return
                }else {
                    Alert.sharedInstance.showAlert(mess: "נא מלא את הפרטים הנדרשים!")
                    return
                }
            }else{
                status = OfferJobStatusType.wait_begin_work.rawValue
                UpdateWorkerOfferedJobStatus(newStatus: status)
                
            }
        }
    }
    @IBAction func moreActionToCancelBtn(_ sender: Any) {
        if  workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.process_extened.rawValue{
            UpdateWorkerOfferedJobStatusAgreeToWait()
        }else if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.apply.rawValue{
            if  hourlyWageContent.text != "" {
                 let d = Float(hourlyWageContent.text!)!
                UpdateWorkerOfferedJobAskingHourlyWage(mAskingHourlyWage: d)
            }else{
                Alert.sharedInstance.showAlert(mess: "נא מלא את הפרטים הנדרשים!")

            }
        }
        else {
        var _ = self.navigationController?.popViewController(animated: true)
        }}
    
    //MARK: Object
    var workerOfferedJob : WorkerOfferedJob!
    var heightJobDetails : CGFloat = 900
    var controller = JobDetailsViewController()
    var isCancel : Bool = false
    var indexl = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global.sharedInstance.notificationRefreshDelegate = self
        
        //add job deatils
        controller = storyboard?.instantiateViewController(withIdentifier: "JobDetailsViewController") as! JobDetailsViewController
        controller.workerOfferedJob = workerOfferedJob
        
        controller.jobDetailsDel = self
        addChildViewController(controller)
        controller.view.frame = CGRect(x: 0, y: 0, width: 375, height: 199)
        viewJobDetails.addSubview((controller.view)!)
        
        
        //tap
        moreTxt.isUserInteractionEnabled = true
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openBasePopUp))
        moreTxt.addGestureRecognizer(tap1)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        updateUIByStatus()
    }
    
    override func viewDidLayoutSubviews() {
        
        //circel buttens
        cancelBtn.layer.borderColor = ControlsDesign.sharedInstance.colorGrayText.cgColor
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.cornerRadius = cancelBtn.frame.height / 2
        
       // Global.sharedInstance.setButtonCircle(sender: chatBtn, isBorder: false)
        //Global.sharedInstance.setButtonCircle(sender: videoBtn, isBorder: false)
        
        chatBtn.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        chatBtn.layer.borderWidth = 1
        chatBtn.layer.cornerRadius = chatBtn.frame.height / 2
        
        videoBtn.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        videoBtn.layer.borderWidth = 1
        videoBtn.layer.cornerRadius = videoBtn.frame.height / 2
        
        Global.sharedInstance.setButtonCircle(sender: moreAction, isBorder: false)
        
        moreActionToCancel.layer.cornerRadius = cancelBtn.frame.height / 2
        
        heightJobDetails = controller.baseView.frame.height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination is  JobDetailsViewController)
        {
            let vc: JobDetailsViewController = segue.destination as! JobDetailsViewController
            vc.workerOfferedJob = workerOfferedJob
        }
        
        let barContainerViewController = segue.destination as! BarContainerViewController
        //    barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "\(workerOfferedJob.nvDomain) - \(workerOfferedJob.nvRole)"
    }
    
    
    func setDisplayData(index: Int){
        workerOfferedJob =  Global.sharedInstance.lWorkerOfferedJobs[index]
        print(workerOfferedJob.nvEmployerName)
        indexl = index
    }
    
    
    func updateUIByStatus(){
        
        if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.apply.rawValue {
            ststusText.text = "נשלחה מועמדות"
            statusContent.text = "במידה ותמצא התאמה תקבל פניה בזמן הקרוב"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "heart").image
            videoBtn.isHidden = true
            chatBtn.isHidden = true
            getJobMatchPercentage()
            moreDetailsView.isHidden = false
            moreTxt.isHidden = true
            moreTxtLine.isHidden = true
            moreActionToCancel.isHidden = false
            moreAction.isHidden = true
            moreActionToCancel.setTitle("שלח למעסיק", for: .normal)
        }else  if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.want_your.rawValue {
            
            ststusText.text = "רוצים אותך!"
            statusContent.text = "נא אשר קבלת המשרה"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "heart").image
            moreDetailsView.isHidden = false
            
            moreAction.isHidden = false
            moreActionToCancel.isHidden = true
            
            if workerOfferedJob.iOfferJobStatusType == EmployerJobWorkerType.KnownWorker.rawValue {
                moreTxt.text =  "פירוט עלויות שכר"
            }else {
                hourlyWageContent.isHidden = true
                hourlyWageLine.isHidden = true
                hourlyWageTxt.isHidden = true
                moreTxt.isHidden = true
                moreTxtLine.isHidden = true
            }
            
            
        }
        else  if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.wait_begin_work.rawValue {
            ststusText.text = "ממתין לתחילת עבודה"
            statusContent.text = "נא עיין בשינויים בהסכם העבודה בהתאם לנתוני משרה זו"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "clock2").image
            moreDetailsView.isHidden = false
            hourlyWageTxt.isHidden = true
            hourlyWageContent.isHidden = true
            hourlyWageLine.isHidden = true
            moreAction.isHidden = true
            moreTxt.text = "טקסט דינאמי שמגיע מהשרת"
        }
        else  if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.whiting_verification_details.rawValue {
            ststusText.text = "ממתין לאימות פרטים"
            statusContent.text = "בהגעיך למקום העבודה, הזדהה בעזרת תעודה מזהה בפני מקבל השירות כדי להתחיל לדווח נוכחות"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "text-2").image
            cancelBtn.isHidden = false
            
        }
        else  if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.cancel.rawValue || workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.job_cancel.rawValue{
            ststusText.text = "הצעת העבודה בוטלה"
            statusContent.text = "אנו נחפש עבורך משרה מתאימה אחרת"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "icon-x").image
            moreActionToCancel.isHidden = false
            cancelBtn.isHidden = true
            chatBtn.isHidden = true
            videoBtn.isHidden = true
            
            
        }
        else  if workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.occupied.rawValue ||   workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.staffed_and_not_run.rawValue{
            ststusText.text = "לצערנו המשרה אוישה"
            statusContent.text = "אנו נחפש עבורך משרה מתאימה אחרת"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "icon-x").image
            
        }
        else if  workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.finish_time.rawValue {
            ststusText.text = "לצערנו תם הזמן להגשת מועמדות"
            statusContent.text = "בפעם הבאה השתדל לענות מהר יותר"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "icon-x").image
            cancelBtn.isHidden = true
            chatBtn.isHidden = true
            videoBtn.isHidden = true
        }
        else if  workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.process_extened.rawValue {
            ststusText.text = "לצערנו תהליך הגיוס מתארך"
            statusContent.text = "ועדין לא נבחרו עובדים, המשרה בהמתנה עד שיבחר עובד מתאים"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "clock2").image
            moreActionToCancel.isHidden = false
            moreActionToCancel.setTitle("מאשר וממתין", for: .normal)
            cancelBtn.text = "לא מעוניין"
            chatBtn.isHidden = true
            videoBtn.isHidden = true
        }
        else if  workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.waiting.rawValue {
            ststusText.text = "לצערנו תהליך הגיוס מתארך"
            statusContent.text = "ועדין לא נבחרו עובדים, המשרה בהמתנה עד שיבחר עובד מתאים"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "clock2").image
            moreActionToCancel.isHidden = false
            moreActionToCancel.setTitle("מאשר וממתין", for: .normal)
            moreActionToCancel.isEnabled = false
            moreActionToCancel.backgroundColor = UIColor.gray
            cancelBtn.text = "לא מעוניין"
            chatBtn.isHidden = true
            videoBtn.isHidden = true
        }
        else if  workerOfferedJob.iOfferJobStatusType == OfferJobStatusType.not_answer.rawValue {
            ststusText.text = "לצערינו לא ענית בזמן"
            statusContent.text = "והמשרה עברה לעובד אחר, להבא שים לב לענות בזמן, ונוכל למצוא משרות מתאימות לך"
            animStatus.image = Global.sharedInstance.addGifToImage(nameImage: "icon-x").image
            cancelBtn.isHidden = true
            chatBtn.isHidden = true
            videoBtn.isHidden = true
        }
        // add tap
        let cancelBtnTap = UITapGestureRecognizer(target:self, action:#selector(self.cancelAction))
        cancelBtn.addGestureRecognizer(cancelBtnTap)
    }
    //הגשת מועמדות - מידע על מועמדים מתמודדים
    func setDataFromMatchPercentage(jobMatchPercentage : WorkerOfferedJobMatchPercentage){
        if jobMatchPercentage.iMatchPercentageCount >= 1 {
            var s = "הנך מדורג \(jobMatchPercentage.iMatchPercentageCount) למשרה זו...למועמדים שמקדימים אותך "
            if jobMatchPercentage.bRankingAvgMatchPercentage {
                s = s + "יש דרוג גבוה יותר ממעסיקים אחרים "
            }
            if jobMatchPercentage.bTimeMatchPercentage {
                s = s + "זמינות גבוהה יותר למשרה "
            }
            if jobMatchPercentage.bHourlyWageMatchPercentage {
                s = s + "השכר שבקשו נמוך יותר "
            }
            s = s + "אם ברצונך לשפר את סיכוייך לקבלת המשרה ניתן לשנות את דרישות השכר ולשלוח מחדש"
            statusContent.text = statusContent.text! + s
        }
        
      


            
        }
    
    
    
    //MARK: - Taps
    func cancelAction(){
        isCancel = true
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך לבטל את המועמדות למשרה??"
        self.present(basePopUp, animated: true, completion: nil)
        
    }
    func openBasePopUp() {
        isCancel = false
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "פרוט עלויות שכר - יתקבל מהלקוח"
        basePopUp.showCancelBtn = false
        self.present(basePopUp, animated: true, completion: nil)
        
        //openAnotherPageDelegate.openViewController!(VC: basePopUp)
    }
    func dismissKeyboard(){
        view.endEditing(true)
    }
    
    // BasePopUpActionsDelegate
    func okAction(num: Int) {
        if isCancel {
            let  status = OfferJobStatusType.cancel.rawValue
            UpdateWorkerOfferedJobStatus(newStatus: status)
        }
    }
    // JobDetailsDelegate
    func viewAllDetailsDel( checked :Bool){
        print("iooooo")
        if  viewJobDetailsHeigthConstrain.constant == 170{
            viewJobDetailsHeigthConstrain.constant = heightJobDetails
            mainStatusView.isHidden = true
            mainStatusViewHeigthConstain.constant = 0
            mainViewHeightConstrin.constant = heightJobDetails + 200
            
        }else{
            viewJobDetailsHeigthConstrain.constant = 170
            mainStatusView.isHidden = false
            mainStatusViewHeigthConstain.constant = 287
            mainViewHeightConstrin.constant = 700
            
        }
        
    }
    
    
    
    // MARK: - function to server
    func UpdateWorkerOfferedJobStatusKnownWorker(newStatus : Int){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerOfferedJobId"] = workerOfferedJob.iWorkerOfferedJobId as AnyObject?
        dic["mAskingHourlyWage"] = self.hourlyWageContent.text as AnyObject?
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                self.workerOfferedJob.iOfferJobStatusType = newStatus
                self.updateUIByStatus()
                
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetSumWageEmployerJob")
        
        
    }
    func UpdateWorkerOfferedJobStatus (newStatus : Int){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerOfferedJobId"] = workerOfferedJob.iWorkerOfferedJobId as AnyObject?
        dic["iOfferJobStatusType"] = workerOfferedJob.iOfferJobStatusType as AnyObject?
        dic["iNewOfferJobStatusType"] = newStatus as AnyObject?
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                self.workerOfferedJob.iOfferJobStatusType = newStatus
                self.updateUIByStatus()
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerOfferedJobStatus")
    }
        
        
        func UpdateWorkerOfferedJobStatusAgreeToWait (){
            
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
    
            dic["iWorkerOfferedJobId"] = workerOfferedJob.iWorkerOfferedJobId as AnyObject?
            dic["iOfferJobStatusType"] = workerOfferedJob.iOfferJobStatusType as AnyObject?
            dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
            dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
            
            api.sharedInstance.goServer(params : dic,success : {
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] {
                     if let dicResult = dic["Result"] as? Int {
                        Alert.sharedInstance.showAlert(mess: "נשמר בהצלחה!")

                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    self.workerOfferedJob.iOfferJobStatusType = dicResult
                    self.updateUIByStatus()
                    }   } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : "UpdateWorkerOfferedJobStatusAgreeToWait")
            
      
    }
    
    //נשלחה מועמדות - פרטים על המועמדים האחרים
    func getJobMatchPercentage (){
        
       // Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerOfferedJobId"] = workerOfferedJob.iWorkerOfferedJobId as AnyObject?
        dic["iEmployerJobId"] = workerOfferedJob.iEmployerJobId as AnyObject?
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                    var workerOfferedJobMatchPercentage = WorkerOfferedJobMatchPercentage()
                    workerOfferedJobMatchPercentage = workerOfferedJobMatchPercentage.getWorkerOfferedJobMatchPercentageFromDic(dic: dicResult)
                    self.setDataFromMatchPercentage(jobMatchPercentage: workerOfferedJobMatchPercentage)
                }  } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "WorkerOfferedJobMatchPercentage")
        
        
    }
    
    //עדכון שכר חדש של מועמד
    func UpdateWorkerOfferedJobAskingHourlyWage (mAskingHourlyWage : Float?){
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
//int iWorkerOfferedJobId, decimal? mAskingHourlyWage, int iUserId, int iLanguageId)
        dic["iWorkerOfferedJobId"] = workerOfferedJob.iWorkerOfferedJobId as AnyObject?
        if ((mAskingHourlyWage) != nil){
        dic["mAskingHourlyWage"] = mAskingHourlyWage as AnyObject?
        }
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject

        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in

            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if let dicResult = dic["Result"] as? Bool {
                    if dicResult{
                  //  Alert.sharedInstance.showAlert(mess: "נשמר בהצלחה!")
                    }
                  
                }   } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")

            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerOfferedJobAskingHourlyWage")
        
    }
    
    
    func GetWorkerOfferedJob(){
        //nt iJobId, int iLanguageId, int iWorkerOfferedJobId
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
        dic["iWorkerOfferedJobId"] =   self.workerOfferedJob.iWorkerOfferedJobId as AnyObject
        
        print(dic)
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                        if (dic["Result"] as? Dictionary<String,AnyObject>) != nil {
                            var offerdJob = WorkerOfferedJob()
                            offerdJob = offerdJob.getWorkerOfferedJobFromDic(dic: (dic["Result"] as? Dictionary<String,AnyObject>)!)
                            self.workerOfferedJob = offerdJob
                            Global.sharedInstance.lWorkerOfferedJobs[self.indexl] = offerdJob
                            
                            if Global.sharedInstance.lWorkerOfferedJobs[self.indexl].iOfferJobStatusType == OfferJobStatusType.active.rawValue {
                                let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
                                let    vc = workerStory.instantiateViewController(withIdentifier: "ReportPresenceViewController") as! ReportPresenceViewController
                                vc.setDisplayData(index: self.indexl)
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            else{
                                self.updateUIByStatus()
                            }
                            
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                    
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont:  self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorkerOfferedJob")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
};
extension StatusJobViewController: NotificationRefreshDelegate   {
    
    func onNorificarionRefresh(userInfo: [AnyHashable : Any]) {
        
        let iNotiType = Int((userInfo["iNotificationType"] as! NSString).intValue)
        let iUserID = Int((userInfo["iUserId"] as! NSString).intValue)
      
        
        if iNotiType == 6{ //רפרוש סטטוס משרה לעובד
            if iUserID == Global.sharedInstance.user.iUserId {
                let iWorkerId = Int((userInfo["iWorkerOfferedJobId"] as! NSString).intValue)
                if self.workerOfferedJob.iWorkerOfferedJobId == iWorkerId {
                     self.GetWorkerOfferedJob()
                }
            }
        }
    }
    
}
