//
//  ContractWorkerViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 20/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class ContractWorkerViewController: UIViewController {

    //MARK: --views
    @IBOutlet var contractTxt: UITextView!
    @IBOutlet var isContractAgreeBtn: CheckBox!
    @IBOutlet var okBtn: UIButton!
    
    //MARK: --actions
    @IBAction func isContractAgreeBtnAction(_ sender: CheckBox) {
        if !Global.sharedInstance.worker.bContractApproved {
            isContractAgreeBtn.isChecked = !isContractAgreeBtn.isChecked
        }
    }
    
    @IBAction func okBtnAction(_ sender: UIButton) {
        if isContractAgreeBtn.isChecked {
            Global.sharedInstance.worker.bContractApproved = true
            okBtn.isEnabled = false
            updateWorker()
        } else {
            Alert.sharedInstance.showAlert(mess: "נא לאשר את קריאת התקנון")
        }
    }
    
    //MARK: -- variables
    var parentVC : UIViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        contractTxt.layer.borderColor = ControlsDesign.sharedInstance.colorGray.cgColor
        contractTxt.layer.borderWidth = 1
        //contractTxt.layer.cornerRadius = contractTxt.frame.height / 2
        
        okBtn.layer.borderColor = UIColor.orange2.cgColor
        okBtn.layer.borderWidth = 1
        okBtn.layer.cornerRadius = okBtn.frame.height / 2
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getWorker()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func displayContractFromHtml() {
        let contractText : String = Global.sharedInstance.getStringFromHtml(string: Global.sharedInstance.worker.nvContractDetails)!
        if contractTxt != nil {
            contractTxt.text = contractText
        }
        
        
    }
    
    //MARK: --Server Function
    
    func getWorker() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: parentVC != nil ? parentVC! : self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parentVC != nil ? self.parentVC! : self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        let worker = Worker()
                        Global.sharedInstance.worker = worker.getWorkerFromDictionary(dic: dicResult)
                        self.displayContractFromHtml()
                        self.isContractAgreeBtn.isChecked = Global.sharedInstance.worker.bContractApproved
                        self.okBtn.isEnabled = !Global.sharedInstance.worker.bContractApproved

                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parentVC != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorker")
    }
    
    func updateWorker() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: parentVC != nil ? parentVC! : self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["worker"] = Global.sharedInstance.worker.getDictionaryFromWorker() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parentVC != nil ? self.parentVC! : self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool/*Dictionary<String,AnyObject>*/ {
                        if dicResult {
                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                        } else {
                            Alert.sharedInstance.showAlert(mess: "הפרטים לא נשמרו!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parentVC != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorker")
    }

}
