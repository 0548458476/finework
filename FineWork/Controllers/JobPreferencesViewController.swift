//
//  JobPreferencesViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 21/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class JobPreferencesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CellOptionsDelegate, ChangeScrollViewHeightDelegate, getTextFieldDelegate, OpenAnotherPageDelegate {


    //MARK: - Views
    @IBOutlet var domainsTbl: UITableView!
    @IBOutlet var addDomainBtn: UIButton!
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var paymentDetailsBtn: UIButton!
    
    @IBOutlet var buttonsHeightFromTop: NSLayoutConstraint!
    
    @IBOutlet var tblHeightCon: NSLayoutConstraint!
    @IBOutlet var textHeightFronTopCon: NSLayoutConstraint!
    
    //MARK: - Actions
    @IBAction func addDomainBtnAction(_ sender: UIButton) {
        addDomain(isScrollToMiddle: false)
    }
    
    @IBAction func saveBtnAction(_ sender: UIButton) {
        // after validation :
        if validate() {
            saveWorkerDomains()
        }
    }
    
    @IBAction func paymentDetailsBtnAction(_ sender: Any) {
        // open this pop-up
        //func openBasePopUp() {
            let basePopUp = self.storyboard?.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
            basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.text = "פרוט שכר"
        //
            self.present(basePopUp, animated: true, completion: nil)
        //}
    }
    
    //MARK: - Variables
    var numberOfRows = 1
    var scrollView : UIScrollView? = nil
    var changeScrollViewHeightDelegate : ChangeScrollViewHeightDelegate! = nil
    var cellHeight : CGFloat = 260 + 30
    var workerRoleSysTables = WorkerRoleSysTables()
    var workerDomainArr = Array<WorkerDomain>()
    //domain cell array
    var domainsArray = Array<DomainTableViewCell>()
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        paymentDetailsBtn.clipsToBounds = true
//        let buttomBorder = CALayer()
//        buttomBorder.borderColor = paymentDetailsBtn.titleLabel?.textColor.cgColor
//        //textColor.cgColor
//        buttomBorder.borderWidth = 1
//        buttomBorder.frame = CGRect(x: paymentDetailsBtn.frame.origin.x, y: (paymentDetailsBtn.frame.origin.y + paymentDetailsBtn.frame.height + (paymentDetailsBtn.superview?.frame.origin.y)!), width: CGFloat(paymentDetailsBtn.frame.width), height: CGFloat(1))
//        paymentDetailsBtn.layer.addSublayer(buttomBorder)
//        paymentDetailsBtn.layer.masksToBounds = true
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = paymentDetailsBtn.titleLabel?.textColor.cgColor//UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: paymentDetailsBtn.frame.size.height * 0.75, width:  paymentDetailsBtn.frame.size.width, height: 1)
        
        border.borderWidth = width
        paymentDetailsBtn.layer.addSublayer(border)
        paymentDetailsBtn.layer.masksToBounds = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        domainsTbl.isScrollEnabled = false
        domainsTbl.allowsSelection = false
        
        addDomainBtn.layer.borderColor = UIColor.orange2.cgColor
        addDomainBtn.layer.borderWidth = 1
        addDomainBtn.layer.cornerRadius = addDomainBtn.frame.height / 2
        
        saveBtn.layer.borderColor = UIColor.orange2.cgColor
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
        
        //tblHeightCon.constant = UITableViewAutomaticDimension
        tblHeightCon.constant = 200 + 30
        
        // Do any additional setup after loading the view.
        workerDomainArr.removeAll()
        domainsArray.removeAll()
        
        
        domainsArray.insert(domainsTbl.dequeueReusableCell(withIdentifier: "DomainTableViewCell") as! DomainTableViewCell, at: 0)
        
        domainsTbl.reloadData()
        getWorkerRoleSysTables()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numOfRowsInSection = /*workerDomainArr*/domainsArray.count > 0 ? /*workerDomainArr*/domainsArray.count : 1

        return numOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : DomainTableViewCell = DomainTableViewCell()
        
        cell = domainsTbl.dequeueReusableCell(withIdentifier: "DomainTableViewCell") as! DomainTableViewCell
        //cell.setDisplayData(isFirst: indexPath.row == 0 ? true : false, _cellNumber: indexPath.row, _workerRoleSysTables: workerRoleSysTables, _workerDomain: indexPath.row < workerDomainArr.count ? workerDomainArr[indexPath.row] : nil)
        cell = domainsArray[indexPath.row]
        cell.setDesignData(isFirst: indexPath.row == 0 ? true : false, _cellNumber: indexPath.row)
        cell.removeDomainDelegate = self
        cell.changeScrollViewHeightDelegate = self
        cell.getTextFieldDelegate = self
        cell.openAnotherPageDelegate = self
        
        return cell
        
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight = 0.0
        
        rowHeight = Double(indexPath.row == 0 ? 200 + 30 : cellHeight)
        
        if !domainsArray[indexPath.row].domainsOrRolesTbl.isHidden {
            rowHeight += 100
        }
        
        //domainsTbl.cellForRow(at: indexPath)
//        tableView.cellForRow(at: indexPath)
        //guard let cell = tableView.cellForRow(at: indexPath) as? DomainTableViewCell else {return CGFloat(rowHeight)}
//        if let cell = (tableView.cellForRow(at: indexPath) as? DomainTableViewCell) {
//            if cell.domainsOrRolesTbl.isHidden {
//                rowHeight += 100
//            }
//        }

        return CGFloat(rowHeight)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
//    func setHeightToTbl(height : CGFloat) {
//        tblHeightCon.constant += height
//    }
    
    func addDomain(isScrollToMiddle : Bool) {
//        let cellLanguage = LanguageTableViewCell()
//        cellLanguagesArray2.insert(cellLanguage, at: cellLanguagesArray2.count)
        
        numberOfRows += 1
        let cell = domainsTbl.dequeueReusableCell(withIdentifier: "DomainTableViewCell") as! DomainTableViewCell
        cell.setDisplayData(_workerRoleSysTables: workerRoleSysTables, _workerDomain: domainsArray.count < workerDomainArr.count ? workerDomainArr[domainsArray.count] : nil)
        domainsArray.insert(cell, at: domainsArray.count)
        //workerDomainArr.insert(WorkerDomain(), at: workerDomainArr.count)
        
            domainsTbl.beginUpdates()
            domainsTbl.insertRows(at: [IndexPath(row: /*workerDomainArr*/domainsArray.count - 1, section: 0)], with: .automatic)
            domainsTbl.endUpdates()
        
        
        if isScrollToMiddle {
            scrollTableViewToMiddleScreen(indexPath: IndexPath(row: /*workerDomainArr*/domainsArray.count - 1, section: 0))
        }
        
       // domainsTbl.frame = CGRect(x: domainsTbl.frame.origin.x, y: domainsTbl.frame.origin.y, width: domainsTbl.frame.width, height: domainsTbl.frame.height + 250)
        tblHeightCon.constant += cellHeight
        changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: cellHeight)
        //textHeightFronTopCon.constant = 0
        
//        if scrollView != nil {
//            scrollView?.contentSize = CGSize(width: (scrollView?.frame.width)!, height: (scrollView?.frame.height)! + 250)
//        }
        
    }
    
    func removeCell(cellNumber : Int) {
        //cellLanguagesArray2.remove(at: cellNumber)
        
//        var _cellNumber : Int = 0
//        for cell in cellLanguagesArray2 {
//            print("A-log cellNumber : \(cell.cellNumber)")
//            cell.cellNumber = _cellNumber
//            _cellNumber += 1
//        }
        
        numberOfRows -= 1
        //workerDomainArr.remove(at: cellNumber)
        domainsArray.remove(at: cellNumber)
        
        domainsTbl.deleteRows(at: [IndexPath(row: cellNumber, section: 0)], with: UITableViewRowAnimation.automatic)
        domainsTbl.reloadData()
        
        tblHeightCon.constant -= cellHeight
        changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: -cellHeight)
        //textHeightFronTopCon.constant = 0
        
//        if (cellNumber - 1) > 0 && (cellNumber - 1) < domainsTbl.numberOfRows(inSection: 0) {
//            self.scrollTableViewToMiddleScreen(indexPath: IndexPath(row: cellNumber - 1, section: 0))
//        }
    }
    
    func scrollTableViewToMiddleScreen(indexPath : IndexPath) {
        domainsTbl.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
    }
    
    func changeScrollViewHeight(heightToadd: CGFloat) {
        tblHeightCon.constant += heightToadd
        changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: heightToadd)
    }
    
    func saveWorkerDomains() {
        
        var workerDomainArrToSend = Array<WorkerDomain>()
        
        // loop on domain cells and get worker domain
        for index in stride(from: 0, to: domainsArray.count, by: 1) {
            workerDomainArrToSend.insert(domainsArray[index].getWorkerDomain(), at: index)
        }
        
        // insert all worker roles to array for check multiple roles
        var workerRolesArr = Array<WorkerRole>()
        for index in stride(from: 0, to: workerDomainArrToSend.count, by: 1) {
            
            for indexRole in stride(from: 0, to: workerDomainArrToSend[index].lWorkerRoles.count, by: 1) {
                workerRolesArr.append(workerDomainArrToSend[index].lWorkerRoles[indexRole])
            }
        }
        
        // loop on workerRolesArr for check multiple roles
        var isFindSameDoamins = false
        for index in stride(from: 0, to: workerRolesArr.count, by: 1) {
          
            for indexIn in stride(from: index + 1, to: workerRolesArr.count, by: 1) {
                if workerRolesArr[index].iEmploymentDomainRoleId == workerRolesArr[indexIn].iEmploymentDomainRoleId {
                    isFindSameDoamins = true
                    break
                }
            }
            
            if isFindSameDoamins {
                break
            }
        }
        
        if isFindSameDoamins {
            Alert.sharedInstance.showAlert(mess: "לא ניתן להגדיר תחומי משרה זהים!")
        } else {
            updateWorkerDomain(workerDoaminArr: workerDomainArrToSend)
        }
        
    }
    
    func validate() -> Bool {
        var isOK = true, isChooseJobType = true
        for index in stride(from: 0, to: domainsArray.count, by: 1) {
            if domainsArray[index].constantJobBtn.isChecked || domainsArray[index].partialJobBtn.isChecked {
                
            } else {
                isChooseJobType = false
                
            }
            
            if domainsArray[index].hourlySalaryTxt.text == "" {
                setErrorToTextField(sender: domainsArray[index].hourlySalaryTxt)
                
                isOK = false
            } else {
                setValidToTextField(sender: domainsArray[index].hourlySalaryTxt)
            }
            
            
            if domainsArray[index].getWorkerDomain().iEmploymentDomainType > 0 {
                setValidToLabel(sender: domainsArray[index].domainLbl)
            } else {
                setErrorToLabel(sender: domainsArray[index].domainLbl)
                isOK = false
            }
            
            if domainsArray[index].getWorkerDomain().lWorkerRoles.count > 0 {
                setValidToLabel(sender: domainsArray[index].roleLbl)
            } else {
                
                setErrorToLabel(sender: domainsArray[index].roleLbl)
                isOK = false
            }
        }
        if !isChooseJobType {
            Alert.sharedInstance.showAlert(mess: "לא נבחר סוג משרה!")
            return false
        } else if !isOK {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים/אינם תקינים")
            return false
        }
        return true
    }
    
    func setErrorToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.red.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setValidToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.clear.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setErrorToLabel(sender : UILabel) {
        sender.layer.borderColor = UIColor.red.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setValidToLabel(sender : UILabel) {
        sender.layer.borderColor = UIColor.clear.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
  
    //MARK: - Text Field Delegate
    func getTextField(textField: UITextField, originY: CGFloat) {
        getTextFieldDelegate.getTextField(textField: textField, originY: originY)
    }
    
    

    
    //MARK: - Server Function
    func getWorkerRoleSysTables() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        self.workerRoleSysTables = self.workerRoleSysTables.getWorkerRoleSysTablesFromDictionary(dic: dicResult)
                        self.getWorkerDomains()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetWorkerRoleSysTables)
    }
    
    func getWorkerDomains() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> {
                        let workerDomain = WorkerDomain()
                        if dicResult.count == 0 {
                            if self.domainsArray.count > 0 {
                                self.domainsArray[0].setDisplayData(_workerRoleSysTables: self.workerRoleSysTables, _workerDomain: nil)
                            }
                        } else {
                            for dicIndex in stride(from: 0, to: dicResult.count, by: 1) {
                                
                                self.workerDomainArr.insert(workerDomain.getWorkerDomainFromDictionary(dic: dicResult[dicIndex]), at: self.workerDomainArr.count)
                                if dicIndex == 0 {
                                    self.domainsArray[0].setDisplayData(_workerRoleSysTables: self.workerRoleSysTables, _workerDomain: self.workerDomainArr[0])
                                    
                                } else {
                                    self.addDomain(isScrollToMiddle: false)
                                }
                            }
                        }
                        self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetWorkerDomains)
    }
    
    func updateWorkerDomain(workerDoaminArr : Array<WorkerDomain>) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["lWorkerDomains"] = getlWorkerDomainsAsDic(_workerDoaminArr: workerDoaminArr) as AnyObject?
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool {
                        if dicResult {
                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                            Global.sharedInstance.bWereThereAnyChanges = false
                            if self.openAnotherPageDelegate != nil {
                                self.openAnotherPageDelegate.openNextTab!(tag: 1)
                            }
                        } else {
                            Alert.sharedInstance.showAlert(mess: "אירעה בעיה בשמירת הנתונים!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateWorkerDomain)
    }

    func getlWorkerDomainsAsDic(_workerDoaminArr : Array<WorkerDomain>)->Array<AnyObject>
    {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for workerdomain in _workerDoaminArr
        {
            dic = workerdomain.getDictionaryFromWorkerDomain()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    //MARK: - OpenAnotherPageDelegate
    func openViewController(VC: UIViewController) {
        if VC is BasePopUpViewController {
            self.present(VC, animated: true, completion: nil)
        }
    }

}
