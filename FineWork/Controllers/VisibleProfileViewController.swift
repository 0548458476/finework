//
//  VisibleProfileViewController.swift
//  FineWork
//
//  Created by User on 27.2.2017.
//  Copyright © 2017 webit. All rights reserved.
//

//@objc protocol SaveWorkerDelegate {
//    @objc optional func saveWorkerFinal()
//    @objc optional func saveWorkerDetails()
//    @objc optional func saveWorkerLanguages()
//}

protocol SaveWorkerDelegate {
    func saveWorkerFinal()
}

@objc protocol UploadFilesDelegate {
    @objc optional func uploadFile(fileUrl : String)
    @objc optional func getFile(imgName : String, img : String, displayImg : UIImage?)
}

import UIKit

class VisibleProfileViewController: UIViewController , /*UIAlertController, */UITableViewDataSource, UITableViewDelegate, AddLanguageDelegate, CellOptionsDelegate, getTextFieldDelegate, SaveWorkerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UploadFilesDelegate,ChangeSubViewProfileDelegate,UIScrollViewDelegate, OpenAnotherPageDelegate {

    @IBOutlet weak var visibleProfileTbl: UITableView!
    @IBOutlet var constrainTopSmallMenu: NSLayoutConstraint!
    
    //MARK: -- Variables
    var cellHeader : ProfileHeaderTableViewCell = ProfileHeaderTableViewCell()
    var cellVisibleProfileHeader : VisibleProfileHeaderTableViewCell = VisibleProfileHeaderTableViewCell()
    var cellVisibleProfileEnd : VisibleProfileEndTableViewCell = VisibleProfileEndTableViewCell()
//    var cellLanguagesArray2 : Array<LanguageTableViewCell> = Array<LanguageTableViewCell>()
    var cellLanguagesArray2 : Array<WorkerLanguage?> = Array<WorkerLanguage?>()
    var cellLanguagesArr : Array<LanguageTableViewCell?> = Array<LanguageTableViewCell?>()
    
    var languagesArray : Array<SysTable> = []
    var txtSelected : UITextField = UITextField()
    var cellOriginY : CGFloat = 0.0
    let screenSize = UIScreen.main.bounds
    var isFromCamera = false
    //    var isSetData = false
    
    // delegates
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    var uploadFilesDelegateParent : UploadFilesDelegate! = nil
    var fromContainerDelegate:ChangeSubViewProfileDelegate! = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        visibleProfileTbl.allowsSelection = false
        cellHeader = visibleProfileTbl.dequeueReusableCell(withIdentifier: "ProfileHeaderTableViewCell") as! ProfileHeaderTableViewCell
//        cellHeader.uploadFilesDelegate = self
        
        // initial cellVisibleProfileHeader
        cellVisibleProfileHeader = visibleProfileTbl.dequeueReusableCell(withIdentifier: "VisibleProfileHeaderTableViewCell") as! VisibleProfileHeaderTableViewCell

        // initial cellVisibleProfileEnd
        cellVisibleProfileEnd = visibleProfileTbl.dequeueReusableCell(withIdentifier: "VisibleProfileEndTableViewCell") as! VisibleProfileEndTableViewCell
        
        // initial cellLanguage
        //cellLanguage = visibleProfileTbl.dequeueReusableCell(withIdentifier: "LanguageTableViewCell") as! LanguageTableViewCell
        
        //cellLanguagesArray.append(1)
        
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isFromCamera {
            constrainTopSmallMenu.constant = -150
            registerKeyboardNotifications()
            cellLanguagesArray2.removeAll()
            cellLanguagesArr.removeAll()
            //        visibleProfileTbl.delegate = self
            //        visibleProfileTbl.dataSource = self
            //        visibleProfileTbl.reloadData()
            
            getWorker()
            
            cellVisibleProfileHeader.isChangePlace = false
            cellVisibleProfileHeader.placeSelected = nil
            
            visibleProfileTbl.contentOffset = .zero
        } else {
            isFromCamera = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        visibleProfileTbl.delegate = nil
//        visibleProfileTbl.dataSource = nil
        unregisterKeyboardNotifications()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfRowsInSection = 1
        switch section {
        case 0:
            numOfRowsInSection = 1
            break
        case 1:
            numOfRowsInSection = /*cellLanguagesArray.count - 1*/ cellLanguagesArray2.count
            break
        case 2:
            numOfRowsInSection = 1
            break
        default:
            break
        }
        return numOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell = UITableViewCell()
        
        switch indexPath.section {
        case 0:
            cellVisibleProfileHeader.getTextFieldDelegate = self
            cellVisibleProfileHeader.cellOriginY = visibleProfileTbl.rectForRow(at: indexPath).origin.y
            return cellVisibleProfileHeader
        case 1:
//            cell = cellLanguagesArray2[indexPath.row]
            cell = visibleProfileTbl.dequeueReusableCell(withIdentifier: "LanguageTableViewCell") as! LanguageTableViewCell
            
            (cell as! LanguageTableViewCell).cellNumber = indexPath.row
            (cell as! LanguageTableViewCell).setDisplayData(languages: languagesArray, workerLanguage: indexPath.row + 1 < Global.sharedInstance.worker.lWorkerLanguages.count ? Global.sharedInstance.worker.lWorkerLanguages[indexPath.row + 1] : nil)
            (cell as! LanguageTableViewCell).removeLanguageDelegate = self
            (cell as! LanguageTableViewCell).openAnotherPageDelegate = self
            
            if cellLanguagesArr.count > indexPath.row {
                cellLanguagesArr[indexPath.row] = cell as! LanguageTableViewCell
            } else {
                cellLanguagesArr.insert(cell as! LanguageTableViewCell, at: indexPath.row)
            }
            
//            if isSetData {
//                (cell as! LanguageTableViewCell).isSetData = true
//            }
//            
//            if indexPath.row == (cellLanguagesArray2.count - 1) {
//                isSetData = false
//            }
            
            
            
            print("indexPath.row : \(indexPath.row) , cell.ishidden : \(cell.isHidden)")
//            cellLanguagesArray2[indexPath.row] = (cell as! LanguageTableViewCell)
            break
        case 2:
            cellVisibleProfileEnd.addLanguageDelegate = self
            cellVisibleProfileEnd.getTextFieldDelegate = self
            cellVisibleProfileEnd.saveWorkerDelegate = self
            cellVisibleProfileEnd.openAnotherPageDelegate = self
            cellVisibleProfileEnd.uploadFileDelegate = self
            self.uploadFilesDelegate = cellVisibleProfileEnd
            cellVisibleProfileEnd.cellOriginY = tableView.rectForRow(at: indexPath).origin.y
            return cellVisibleProfileEnd
        default:
            break
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            cellHeader.uploadFilesDelegate = self
            self.uploadFilesDelegate = cellHeader
            return cellHeader.contentView
        }
        return UIView(frame: CGRect.zero)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 335
        }
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight = 0.0
        switch indexPath.section {
        case 0:
            rowHeight = 350 + 200 // first language cell
            break
        case 1:
            rowHeight = 280
            break
        case 2:
            rowHeight = Global.sharedInstance.worker.bMilitaryService || cellVisibleProfileEnd.militaryServiceBtn.isChecked ? (570 + 80) : (500 + 80)
            break
        default:
            break
        }
        return CGFloat(rowHeight)
    }
    
    //MARK: - scrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if visibleProfileTbl.contentOffset.y > 185{
            if constrainTopSmallMenu.constant < 0{
                constrainTopSmallMenu.constant = 0
            }
        }else{
            if constrainTopSmallMenu.constant == 0{
                constrainTopSmallMenu.constant = -150
            }
        }
    }
    
    //MARK: - reload cell header according subview(btn , image)
    func reloadData() {
        cellHeader.reloadData(tagMenu: 2)
    }
    
    func addLanguage(isScrollToMiddle : Bool) {
//        let cellLanguage = visibleProfileTbl.dequeueReusableCell(withIdentifier: "LanguageTableViewCell") as! LanguageTableViewCell
//        cellLanguage.setDisplayData(languages: languagesArray, workerLanguage: cellLanguagesArray2.count + 1 < Global.sharedInstance.worker.lWorkerLanguages.count ? Global.sharedInstance.worker.lWorkerLanguages[cellLanguagesArray2.count + 1] : nil)
//        cellLanguagesArray2.insert(cellLanguage, at: cellLanguagesArray2.count)
        cellLanguagesArray2.insert(cellLanguagesArray2.count + 1 < Global.sharedInstance.worker.lWorkerLanguages.count ? Global.sharedInstance.worker.lWorkerLanguages[cellLanguagesArray2.count + 1] : nil, at: cellLanguagesArray2.count)
    

//        visibleProfileTbl.reloadData()
        if cellLanguagesArray2.count > visibleProfileTbl.numberOfRows(inSection: 1) {
            visibleProfileTbl.beginUpdates()
            visibleProfileTbl.insertRows(at: [IndexPath(row: cellLanguagesArray2.count - 1, section: 1)], with: .automatic)
            visibleProfileTbl.endUpdates()
        }
        
        if isScrollToMiddle {
            scrollTableViewToMiddleScreen(indexPath: IndexPath(row: cellLanguagesArray2.count - 1, section: 1))
        }
    }
    
    func removeCell(cellNumber : Int) {
        if cellNumber < cellLanguagesArray2.count {
        cellLanguagesArray2.remove(at: cellNumber)
        }
        
        if cellNumber < cellLanguagesArr.count {
            cellLanguagesArr.remove(at: cellNumber)
        }
        
        var _cellNumber : Int = 0
        for cell in cellLanguagesArr {
            if cell != nil {
                print("A-log cellNumber : \(cell!.cellNumber)")
                cell!.cellNumber = _cellNumber
                _cellNumber += 1
            }
        }
//        for i in stride(from: 0, to: cellLanguagesArray2.count, by: 1) {
//            if let cell = visibleProfileTbl.cellForRow(at: IndexPath(row: i, section: 1)) as! LanguageTableViewCell? {
//                cell.cellNumber = _cellNumber
//                _cellNumber += 1
//            }
//        }
        
        visibleProfileTbl.deleteRows(at: [IndexPath(row: cellNumber, section: 1)], with: UITableViewRowAnimation.automatic)
        visibleProfileTbl.reloadData()
        
        if cellLanguagesArray2.count > 0 {
            scrollTableViewToMiddleScreen(indexPath: IndexPath(row: cellLanguagesArray2.count - 1, section: 1))
        }
    }
    
    func scrollTableViewToMiddleScreen(indexPath : IndexPath) {
        visibleProfileTbl.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
    }
    
    //MARK: - keyBoard notifications
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        
            let userInfo: NSDictionary = notification.userInfo! as NSDictionary
            let keyboardSize = (userInfo.object(forKey: UIKeyboardFrameBeginUserInfoKey)! as AnyObject).cgRectValue.size

            DispatchQueue.main.async {
                var scrollPoint : CGPoint
                if self.txtSelected.tag == 1 || self.txtSelected.tag == 2 {
                    
                    let y1 = (self.txtSelected.frame.origin.y + (self.txtSelected.superview?.frame.origin.y)!)
                    
                    scrollPoint = CGPoint(x: 0, y: y1 + self.cellOriginY - self.screenSize.height + keyboardSize.height + self.txtSelected.frame.height)
                } else {
                    scrollPoint = CGPoint(x: 0, y:self.txtSelected.frame.origin.y + self.cellOriginY - self.screenSize.height + keyboardSize.height + self.txtSelected.frame.height)
                }
                
                if self.txtSelected.tag == 2 {  // open google places list
                    scrollPoint.y += 100
                }
                
                self.visibleProfileTbl.setContentOffset(scrollPoint, animated: true)
            }
        
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        //        scrollView.contentInset = UIEdgeInsets.zero
        //        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        //characteristicsTxt.resignFirstResponder()
        //studiesTxt.resignFirstResponder()
        //jobExperienceTxt.resignFirstResponder()
        //matchingJobsTxt.resignFirstResponder()
        
    }
    
    func getTextField(textField:UITextField,originY:CGFloat)
    {
        txtSelected = textField
        cellOriginY = originY
    }
    
    func saveWorkerFinal() {
        // first section - personl details
        
        if validateWorker() {
            if validateLanguages() && validateAddress() {
                Global.sharedInstance.worker.nvUserName = cellVisibleProfileHeader.fullNameTxt.text!
                Global.sharedInstance.worker.nvNickName = cellVisibleProfileHeader.nickNameTxt.text!
                Global.sharedInstance.worker.nvPhone = cellVisibleProfileHeader.phoneTxt.text!
                Global.sharedInstance.worker.nvMobile = cellVisibleProfileHeader.mobileTxt.text!
                
                Global.sharedInstance.worker.iFamilyStatusType = cellVisibleProfileHeader.familyStatusSelectedId
                if cellVisibleProfileHeader.ageTxt.text != "" {
                    Global.sharedInstance.worker.fAge = Float(cellVisibleProfileHeader.ageTxt.text!)
                }
                Global.sharedInstance.worker.nvAddress = cellVisibleProfileHeader.addressTxt.text!
                
                if cellVisibleProfileHeader.isChangePlace {
                    if cellVisibleProfileHeader.placeSelected != nil {
                        Global.sharedInstance.worker.nvLat = (cellVisibleProfileHeader.placeSelected?.coordinate.latitude.description)!
                        Global.sharedInstance.worker.nvLng = (cellVisibleProfileHeader.placeSelected?.coordinate.longitude.description)!
                    } else {
                        Global.sharedInstance.worker.nvLat = ""
                        Global.sharedInstance.worker.nvLng = ""
                    }
                }
                
                // second section - languages
                var languages = Array<WorkerLanguage>()
                
                if cellVisibleProfileHeader.firstLanguageSelectedId != -1 {
                    let languageFirst = WorkerLanguage()
                    languageFirst.iLanguageType = cellVisibleProfileHeader.firstLanguageSelectedId
                    languageFirst.iLanguageLevelType = cellVisibleProfileHeader.getLanguageLevelType()
                    languages.insert(languageFirst, at: languages.count)
                }
                for languageCell in cellLanguagesArr {
                    if languageCell != nil && languageCell!.languageSelectedId != -1 {
                        let language = WorkerLanguage()
                        language.iLanguageType = languageCell!.languageSelectedId
                        language.iLanguageLevelType = languageCell!.getLanguageLevelType()
                        languages.insert(language, at: languages.count)
                    }
                }
                
//                for i in stride(from: 0, to: cellLanguagesArray2.count, by: 1) {
//                    if let languageCell = visibleProfileTbl.cellForRow(at: IndexPath(row: i, section: 1)) as! LanguageTableViewCell? {
//                        if languageCell.languageSelectedId != -1 {
//                            let language = WorkerLanguage()
//                            language.iLanguageType = languageCell.languageSelectedId
//                            language.iLanguageLevelType = languageCell.getLanguageLevelType()
//                            languages.insert(language, at: languages.count)
//                        }
//                    }
//                }
                
                Global.sharedInstance.worker.lWorkerLanguages = languages
                
//                // check if there is no same languages
//                if !isExistSameLanguages(workerLanguagesArray: languages) {
//                    Global.sharedInstance.worker.lWorkerLanguages = languages
//                } else {
//                    Alert.sharedInstance.showAlert(mess: "לא ניתן להזין שפות זהות!")
//                    return
//                }
//                
//                if !isChooseLanguageLevel(workerLanguagesArray: languages) {
//                    Global.sharedInstance.worker.lWorkerLanguages = languages
//                } else {
//                    Alert.sharedInstance.showAlert(mess: "יש לבחור רמת שליטה בשפה הרצויה!")
//                    return
//                }
                
                
                
                // thread section - resume
                Global.sharedInstance.worker.bMilitaryService = cellVisibleProfileEnd.militaryServiceBtn.isChecked
                if Global.sharedInstance.worker.bMilitaryService {
                    if cellVisibleProfileEnd.militaryServiceYearsTxt.text != "" {
                        Global.sharedInstance.worker.iMilitaryServiceYears = Int(cellVisibleProfileEnd.militaryServiceYearsTxt.text!)
                    }
                    Global.sharedInstance.worker.nvMilitaryServiceRole = cellVisibleProfileEnd.militaryServiceRoleTxt.text!
                }
                Global.sharedInstance.worker.workerResumeDetails.nvResumeCharacteristics = cellVisibleProfileEnd.characteristicsTxt.text!
                Global.sharedInstance.worker.workerResumeDetails.nvResumeStudies = cellVisibleProfileEnd.studiesTxt.text!
                Global.sharedInstance.worker.workerResumeDetails.nvResumeWorkExperience = cellVisibleProfileEnd.jobExperienceTxt.text!
                Global.sharedInstance.worker.workerResumeDetails.nvResumeSuitableWork = cellVisibleProfileEnd.matchingJobsTxt.text!
                
                updateWorker()
            }
        } else {
            Alert.sharedInstance.showAlert(mess: "חלק מהנתונים חסרים/אינם תקינים")
        }
    }
    
    func validateWorker() -> Bool {
//        setErrorToTextField
        
        var isOk = true
        // validate to visible profille header
        if !Validation.sharedInstance.nameValidation(string: cellVisibleProfileHeader.fullNameTxt.text!) {
            cellVisibleProfileHeader.setErrorToTextField(sender: cellVisibleProfileHeader.fullNameTxt)
            isOk = false
        } else {
            cellVisibleProfileHeader.setValidToTextField(sender: cellVisibleProfileHeader.fullNameTxt)
        }
        
        if cellVisibleProfileHeader.nickNameTxt.text! != "" && !Validation.sharedInstance.nameValidation(string: cellVisibleProfileHeader.nickNameTxt.text!) {
            cellVisibleProfileHeader.setErrorToTextField(sender: cellVisibleProfileHeader.nickNameTxt)
            isOk = false
        } else {
            cellVisibleProfileHeader.setValidToTextField(sender: cellVisibleProfileHeader.nickNameTxt)
        }
        
        if cellVisibleProfileHeader.phoneTxt.text! != "" && !Validation.sharedInstance.phoneValidation(string: cellVisibleProfileHeader.phoneTxt.text!) {
            cellVisibleProfileHeader.setErrorToTextField(sender: cellVisibleProfileHeader.phoneTxt)
            isOk = false
        } else {
            cellVisibleProfileHeader.setValidToTextField(sender: cellVisibleProfileHeader.phoneTxt)
        }
        
        return isOk
    }
    
    func validateLanguages() -> Bool {
        var languages = Array<WorkerLanguage>()
        
        if cellVisibleProfileHeader.firstLanguageSelectedId != -1 {
            let languageFirst = WorkerLanguage()
            languageFirst.iLanguageType = cellVisibleProfileHeader.firstLanguageSelectedId
            languageFirst.iLanguageLevelType = cellVisibleProfileHeader.getLanguageLevelType()
            languages.insert(languageFirst, at: languages.count)
        }
        for languageCell in cellLanguagesArr {
            if languageCell != nil && languageCell!.languageSelectedId != -1 {
                let language = WorkerLanguage()
                language.iLanguageType = languageCell!.languageSelectedId
                language.iLanguageLevelType = languageCell!.getLanguageLevelType()
                languages.insert(language, at: languages.count)
            }
        }
        
//        for i in stride(from: 0, to: cellLanguagesArray2.count, by: 1) {
//            if let languageCell = visibleProfileTbl.cellForRow(at: IndexPath(row: i, section: 1)) as! LanguageTableViewCell? {
//                if languageCell.languageSelectedId != -1 {
//                    let language = WorkerLanguage()
//                    language.iLanguageType = languageCell.languageSelectedId
//                    language.iLanguageLevelType = languageCell.getLanguageLevelType()
//                    languages.insert(language, at: languages.count)
//                }
//            }
//        }
        
        // check if there is no same languages
        if !isExistSameLanguages(workerLanguagesArray: languages) {
            Global.sharedInstance.worker.lWorkerLanguages = languages
        } else {
            Alert.sharedInstance.showAlert(mess: "לא ניתן להזין שפות זהות!")
            return false
        }
        
        if !isChooseLanguageLevel(workerLanguagesArray: languages) {
            Global.sharedInstance.worker.lWorkerLanguages = languages
        } else {
            Alert.sharedInstance.showAlert(mess: "יש לבחור רמת שליטה בשפה הרצויה!")
            return false
        }
        
        return true
        
    }
    
    func validateAddress() -> Bool{
        var isValid = true
        if cellVisibleProfileHeader.addressTxt.text != "" && cellVisibleProfileHeader.isChangePlace {
            if cellVisibleProfileHeader.placeSelected == nil {
                Alert.sharedInstance.showAlert(mess: "יש לבחור כתובת מגוגל!")
                isValid = false
            }
        }
        
        return isValid
    }
    
    //MARK: --Server Function
    
    func getWorker() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        let worker = Worker()
                        Global.sharedInstance.worker = worker.getWorkerFromDictionary(dic: dicResult)
                        self.getWorkerCodeTables()
                        self.cellVisibleProfileHeader.setDisplayData(languages: self.languagesArray, workerLanguage: Global.sharedInstance.worker.lWorkerLanguages.count > 0 ? Global.sharedInstance.worker.lWorkerLanguages[0] : nil)
                        self.cellVisibleProfileEnd.setDisplayData()
                        
//                        for cellNumber in stride(from: 1, to: Global.sharedInstance.worker.lWorkerLanguages.count, by: 1) {
//                        //for cellNumber in 1 ..< Global.sharedInstance.worker.lWorkerLanguages.count {
//                            print("cellNumber : \(cellNumber)")
//                            //self.cellLanguagesArray2[cellNumber].setDisplayData(languages: self.languagesArray, )
//                            self.addLanguage(isScrollToMiddle: false)
//                        }
                        self.visibleProfileTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
                
                //if dic["Error"]?["iErrorCode"] as! Int ==  2
                //{
                //    Alert.sharedInstance.showAlert(mess: "מייל לא קיים במערכת")
                //}
                //else
                //{
                //    Alert.sharedInstance.showAlert(mess: "נשלח אליך מייל עם הסיסמה")
                //}
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorker")
    }
    
    func updateWorker() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["worker"] = Global.sharedInstance.worker.getDictionaryFromWorker() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool/*Dictionary<String,AnyObject>*/ {
                        if dicResult {
                            Global.sharedInstance.bWereThereAnyChanges = false
                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                            
                            self.cellHeader.btnChangeViewAction(self.cellHeader.btnEmployeeContract)
                        
                        } else {
                            Alert.sharedInstance.showAlert(mess: "הפרטים לא נשמרו!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorker")
    }

    //MARK: GetWorkerCodeTables
    func getWorkerCodeTables() {
        if Global.sharedInstance.workerCodeTables.count > 0{
            for item in Global.sharedInstance.workerCodeTables { // 1
                if item.Key == Global.sharedInstance.languageCodeTable {
                    self.languagesArray = item.Value
                }
            } // 1
            
            self.cellVisibleProfileHeader.setDisplayData(languages: self.languagesArray, workerLanguage: Global.sharedInstance.worker.lWorkerLanguages.count > 0 ? Global.sharedInstance.worker.lWorkerLanguages[0] : nil)
            
            for cellNumber in stride(from: 1, to: Global.sharedInstance.worker.lWorkerLanguages.count, by: 1) {
                //for cellNumber in 1 ..< Global.sharedInstance.worker.lWorkerLanguages.count {
                print("cellNumber : \(cellNumber)")
                //self.cellLanguagesArray2[cellNumber].setDisplayData(languages: self.languagesArray, )
                self.addLanguage(isScrollToMiddle: false)
//                self.isSetData = true
            }
            self.visibleProfileTbl.reloadData()
        }else{
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : { // 8
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] { // 7
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 { // 5
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> { // 3
                        
                        var workerCodeTables : Array<SysTableDictionary> = []
                        let sysTable = SysTableDictionary()
                        
                        for workerCodeTableDic in dicResult { // 0
                            workerCodeTables.append(sysTable.getSysTableDictionaryFromDic(dic: workerCodeTableDic))
                        } // 0
                        
                        Global.sharedInstance.workerCodeTables = workerCodeTables
                        for item in workerCodeTables { // 1
                            if item.Key == Global.sharedInstance.languageCodeTable {
                                self.languagesArray = item.Value
                            }
                        } // 1
                        
                        self.cellVisibleProfileHeader.setDisplayData(languages: self.languagesArray, workerLanguage: Global.sharedInstance.worker.lWorkerLanguages.count > 0 ? Global.sharedInstance.worker.lWorkerLanguages[0] : nil)
                        
                        for cellNumber in stride(from: 1, to: Global.sharedInstance.worker.lWorkerLanguages.count, by: 1) {
                            //for cellNumber in 1 ..< Global.sharedInstance.worker.lWorkerLanguages.count {
                            print("cellNumber : \(cellNumber)")
                            //self.cellLanguagesArray2[cellNumber].setDisplayData(languages: self.languagesArray, )
//                            self.isSetData = true
                            self.addLanguage(isScrollToMiddle: false)
                        }
                        self.visibleProfileTbl.reloadData()
                        
                    } else { // 3 , 4
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    } // 4
                } else { // 5 , 6
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                } // 6
            } // 7
        } ,failure : { // 8 , 9
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorkerCodeTables") // 9
        }
    }
    
    func isExistSameLanguages(workerLanguagesArray : Array<WorkerLanguage>) -> Bool {
        
        for index in 0 ..< workerLanguagesArray.count {
            for indexIn in index+1 ..< workerLanguagesArray.count {
                if workerLanguagesArray[index].iLanguageType == workerLanguagesArray[indexIn].iLanguageType {
                    return true
                }
            }
        }
        
        return false
    }
    
    func isChooseLanguageLevel(workerLanguagesArray : Array<WorkerLanguage>) -> Bool {
        
        for index in 0 ..< workerLanguagesArray.count {
            for indexIn in index+1 ..< workerLanguagesArray.count {
                if workerLanguagesArray[index].iLanguageLevelType == -1 {
                    return true
                }
            }
        }
        
        return false
    }
    
    //MARK: - camera func
    func showCameraActionSheet()
    {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "צלם", style: .default, handler: {(alert: UIAlertAction!) in self.openCamera()})
        let libraryAction = UIAlertAction(title: "העלה תמונה", style: .default, handler: {(alert: UIAlertAction!) in self.openLibrary()})
      
        let cancelAction = UIAlertAction(title: "ביטול", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        
                        //do whatever with your file name
                        //self.changeBtnDesign(imgName: imageName!,img: imageString)
                        self.uploadFilesDelegate.getFile!(imgName: imageName!, img: imageString, displayImg: nil)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    //self.changeBtnDesign(imgName: imageName,img: imageString)
                    self.uploadFilesDelegate.getFile!(imgName: imageName, img: imageString, displayImg: nil)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - UploadFileDelegate
    func uploadFile(fileUrl : String) {
        uploadFilesDelegateParent.uploadFile!(fileUrl: fileUrl)

//        if fileUrl != "" { // האם שמור קובץ בשרת
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
//            
//            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
//            controller?.fileUrl = fileUrl
//            
//            self.present(controller!, animated: true, completion: nil)
//        } else {
//            showCameraActionSheet()
//        }
    }
    
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        uploadFilesDelegate.getFile!(imgName: imgName, img: img, displayImg: displayImg)

    }
  
    //MARK: - ChangeSubViewProfileDelegate
    func changeSubViewProfile(tagNewView:Int){
        fromContainerDelegate.changeSubViewProfile(tagNewView: tagNewView)
    }
    
    func tapBack(){
        fromContainerDelegate.tapBack()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "visibleProfileFormSegue" {
            let connectContainerViewController = segue.destination as! PersonalInformationModelViewController
            connectContainerViewController.containerDelegate = self
            connectContainerViewController.subviewTag = 2
            //            containerDelegate = connectContainerViewController
            //            containerViewController = connectContainerViewController
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    //MARK: - OpenAnotherPageDelegate
    func openViewController(VC: UIViewController) {
        if VC is BasePopUpViewController {
            self.present(VC, animated: true, completion: nil)
       
        }else{
            
           // self.openViewController.pushViewController(VC, animated: true)
          // self.pushViewController(VC, animated: true)
            openAnotherPageDelegate?.openViewController?(VC: VC)
//            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
}
