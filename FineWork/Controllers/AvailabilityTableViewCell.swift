//
//  AvailabilityTableViewCell.swift
//  FineWork
//
//  Created by Tamy wexelbom on 29.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
//import SwiftRangeSlider
import TTRangeSlider

extension NumberFormatter.Style {
//    func hour() -> NumberFormatter.Style {
//        
//    }
}

class ElapsedTimeFormatter: NumberFormatter {
    
    lazy var dateFormatterHours: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        return dateFormatter
    }()
    
    lazy var dateFormatterMinutes: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "mm"
        return dateFormatter
    }()
    
    override func string(from number: NSNumber) -> String? {
        
        let timeInterval = TimeInterval(number)
        var hours = dateFormatterHours.string(from: NSDate(timeIntervalSinceReferenceDate: timeInterval) as Date)
        let minutes = dateFormatterMinutes.string(from: NSDate(timeIntervalSinceReferenceDate: timeInterval) as Date)
        //var result : String = hours
        if hours == "23" && minutes == "59" {
            hours = "24"
        }
        return hours + ":00"
    }
}


class AvailabilityTableViewCell: UITableViewCell, TTRangeSliderDelegate, BasePopUpActionsDelegate{
    


    //MARK: - Views
    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var firstRangeSlider: TTRangeSlider!
    @IBOutlet weak var secondRangeSlider: TTRangeSlider!

    @IBOutlet weak var addSecondRangeSliderBtn: UIButton!
    @IBOutlet weak var removeSecondRangeSliderBrn: UIButton!
    @IBOutlet weak var secondView: UIView!
    //MARK: - Actions
    @IBAction func firstRangeSliderValueChangeAction(_ sender: Any) {
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func secondRangeSliderValueChangeAction(_ sender: Any) {
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    @IBAction func addSecondRangeSliderBtnAction(_ sender: Any) {
        secondView.isHidden = false
        // refresh table
        if let tbl = self.superview?.superview as? UITableView {

//            changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: domainsOrRolesTbl.isHidden ? -100 : 100)
            tbl.beginUpdates()
            tbl.endUpdates()
            
            changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: secondView.frame.height)
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    @IBAction func removeSecondRangeSliderBrnAction(_ sender: Any) {
        openBasePopUp()
        
    }
    
    //MARK: - Variables
    var changeScrollViewHeightDelegate : ChangeScrollViewHeightDelegate! = nil
    var workerAvailablityDay : WorkerAvailabiltyDay? = nil
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    var iDayId : Int = -1
    var isTouchInBeginOrEnd = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        // init firstRangeSlider
        firstRangeSlider.tintColorBetweenHandles = ControlsDesign.sharedInstance.colorFucsia
        //firstRangeSlider.tintColor = UIColor.white
        firstRangeSlider.handleColor = ControlsDesign.sharedInstance.colorFucsia

        firstRangeSlider.handleDiameter = 20
        firstRangeSlider.selectedHandleDiameterMultiplier = 1.0
        
        firstRangeSlider.minValue = Float(-2 * 60 * 60)
        firstRangeSlider.maxValue = Float((22 * 60 * 60) - 60)
        
        firstRangeSlider.selectedMinimum = firstRangeSlider.minValue
        firstRangeSlider.selectedMaximum = firstRangeSlider.maxValue
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapOnFirstSlider))
//        firstRangeSlider.gestureRecognizerShouldBegin(tap)
        firstRangeSlider.delegate = self
//        firstRangeSlider.handleImage
//            firstRangeSlider.handleImage.addGestureRecognizer(tap)
        
        //then make a action method :
        
        
        
        // init secondRangeSlider
        secondRangeSlider.tintColorBetweenHandles = ControlsDesign.sharedInstance.colorFucsia
        //secondRangeSlider.tintColor = UIColor.white
        secondRangeSlider.handleColor = ControlsDesign.sharedInstance.colorFucsia
        
        secondRangeSlider.handleDiameter = 20
        secondRangeSlider.selectedHandleDiameterMultiplier = 1.0
        
        secondRangeSlider.minValue = Float(10 * 60 * 60)
        secondRangeSlider.maxValue = Float((22 * 60 * 60) - 60)
        
        secondRangeSlider.selectedMinimum = secondRangeSlider.minValue
        secondRangeSlider.selectedMaximum = secondRangeSlider.maxValue
        
        secondRangeSlider.delegate = self
        
        let numFormat = ElapsedTimeFormatter()
        firstRangeSlider.numberFormatterOverride = numFormat
        secondRangeSlider.numberFormatterOverride = numFormat
        
        // init addSecondRangeSliderBtn
        addSecondRangeSliderBtn.layer.borderColor = addSecondRangeSliderBtn.currentTitleColor.cgColor
        addSecondRangeSliderBtn.layer.borderWidth = 1
        addSecondRangeSliderBtn.layer.cornerRadius = addSecondRangeSliderBtn.frame.height / 2
        
        // init removeSecondRangeSliderBrn
        removeSecondRangeSliderBrn.layer.borderColor = removeSecondRangeSliderBrn.currentTitleColor.cgColor
        removeSecondRangeSliderBrn.layer.borderWidth = 1
        removeSecondRangeSliderBrn.layer.cornerRadius = removeSecondRangeSliderBrn.frame.height / 2
        
        // init dayView
        let rectShape = CAShapeLayer()
        rectShape.bounds = dayView.frame
        rectShape.position = dayView.center
        rectShape.path = UIBezierPath(roundedRect: dayView.bounds, byRoundingCorners: [ .topLeft], cornerRadii: CGSize(width: dayView.frame.height/2, height: dayView.frame.height/2)).cgPath
        
        
        //dayView.layer.backgroundColor = UIColor.green.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        dayView.layer.mask = rectShape
        
        
        setDisableSlider(sender: firstRangeSlider)
        setDisableSlider(sender: secondRangeSlider)
    }
    
    func tapOnFirstSlider() {
        print("Button Clicked")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDisplayData(_workerAvailibilityDay : WorkerAvailabiltyDay?) {
        if _workerAvailibilityDay != nil {
            workerAvailablityDay = _workerAvailibilityDay!
            
            
            for index in stride(from: 0, to: workerAvailablityDay!.lWorkerAvailabiltyTimes.count, by: 1) {
//                print("from hour : \(workerAvailablityDay?.lWorkerAvailabiltyTimes[index].tFromHoure)")
//                print("to hour : \(workerAvailablityDay?.lWorkerAvailabiltyTimes[index].tToHoure)")
                
                // from hour
                var fromHour = getHour(str: (workerAvailablityDay?.lWorkerAvailabiltyTimes[index].tFromHoure)!)
                fromHour -= 2
                let fromHourInMinutes = Float(fromHour * 60 * 60)
                // to hour
                var toHour = getHour(str: (workerAvailablityDay?.lWorkerAvailabiltyTimes[index].tToHoure)!)
                toHour -= 2
                let toHourInMinutes = Float(toHour * 60 * 60)
                if index == 0 {
                    setEnableSlider(sender: firstRangeSlider)
                    firstRangeSlider.selectedMinimum = fromHourInMinutes
                    firstRangeSlider.selectedMaximum = toHourInMinutes
                } else {
                    setEnableSlider(sender: secondRangeSlider)
                    secondView.isHidden = false
                    changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: secondView.frame.height)
//                    print("step 1 ishidden : \(secondRangeSlider.isHidden)")
                    secondRangeSlider.selectedMinimum = fromHourInMinutes
                    secondRangeSlider.selectedMaximum = toHourInMinutes
                }
                
            }
        }
    }
    
    func getHour(str : String) -> Int {
        if str != "" {
            var arr = str.characters.split(separator: ":")
            
            let subStr1 = String(arr[0])
            if let num1 = Int(subStr1) {
                return num1
            }
        }
        
        return -1
    }
    
    func save() -> WorkerAvailabiltyDay {
        
        if workerAvailablityDay == nil {
            workerAvailablityDay = WorkerAvailabiltyDay()
        }
        
        workerAvailablityDay?.iDayType = iDayId
        
        var lTimes = Array<WorkerAvailabiltyTime>()
        
        if firstRangeSlider.handleColor != UIColor.lightGray {
            // insert first range slider values
            var time : WorkerAvailabiltyTime
            if (workerAvailablityDay?.lWorkerAvailabiltyTimes.count)! > 0 {
                time = (workerAvailablityDay?.lWorkerAvailabiltyTimes[0])!
            } else {
                time = WorkerAvailabiltyTime()
            }
            time.iDayType = iDayId
            
            let numFormat = ElapsedTimeFormatter()
            
            time.tFromHoure = numFormat.string(from: NSNumber(value: firstRangeSlider.selectedMinimum))!
            time.tToHoure = numFormat.string(from: NSNumber(value: firstRangeSlider.selectedMaximum))!
            
            lTimes.insert(time, at: lTimes.count)
        }
        
        // insert second range slider values
        if !secondView.isHidden {
            if secondRangeSlider.handleColor != UIColor.lightGray {
                var time2 : WorkerAvailabiltyTime
                if (workerAvailablityDay?.lWorkerAvailabiltyTimes.count)! > 1 {
                    time2 = (workerAvailablityDay?.lWorkerAvailabiltyTimes[1])!
                } else {
                    time2 = WorkerAvailabiltyTime()
                }
                time2.iDayType = iDayId
                
                let numFormat = ElapsedTimeFormatter()
                
                time2.tFromHoure = numFormat.string(from: NSNumber(value: secondRangeSlider.selectedMinimum))!
                time2.tToHoure = numFormat.string(from: NSNumber(value: secondRangeSlider.selectedMaximum))!
                
                lTimes.insert(time2, at: lTimes.count)
            }
        }
        
        workerAvailablityDay?.lWorkerAvailabiltyTimes = lTimes
        
        return workerAvailablityDay!
    }
    
    
//    firstRangeSlider.maxValue = Float((22 * 60 * 60) - 60)
    
    func didEndTouches(in sender: TTRangeSlider!) {
        
//        if firstRangeSlider.selectedMaximum == (-2 * 60 * 60) {
//            isFirst = true
//        }
        
        if isTouchInBeginOrEnd && sender.selectedMinimum == sender.minValue && sender.selectedMaximum == sender.maxValue {
            print("didEndTouches")
            isTouchInBeginOrEnd = false
            
            if /*sender.isEnabled*/ sender.handleColor == UIColor.lightGray {
                setEnableSlider(sender: sender)
            } else {
                setDisableSlider(sender: sender)
            }
        } else {
            isTouchInBeginOrEnd = false
        }
        
    }
    
    func didStartTouches(in sender: TTRangeSlider!) {
        
        if sender.selectedMinimum == sender.minValue && sender.selectedMaximum == sender.maxValue {
            isTouchInBeginOrEnd = true
            print("didStartTouches")
        }
    }
    
    /**
     * Called when the RangeSlider values are changed
     */
    public func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
        
        setEnableSlider(sender: sender)
        
        Global.sharedInstance.bWereThereAnyChanges = true
//        if self.selectedMaximumTemp != selectedMaximum {
//            isFirst = false
//            selectedMaximumTemp = selectedMaximum
//        }
        
//        if sender.handleColor == UIColor.lightGray {
//            sender.selectedMaximum = sender.maxValue
//            sender.selectedMinimum = sender.minValue
//        }
    }
    
    func setDisableSlider(sender : TTRangeSlider) {
        sender.handleColor = UIColor.lightGray
        sender.tintColorBetweenHandles = UIColor.lightGray
        sender.maxLabelColour = UIColor.lightGray
        sender.minLabelColour = UIColor.lightGray
        
//        sender.isEnabled = false
//        sender.disableRange = true
    }
    
    func setEnableSlider(sender : TTRangeSlider) {
        sender.handleColor = UIColor.blue2
        sender.tintColorBetweenHandles = UIColor.blue2
        sender.maxLabelColour = UIColor.blue2
        sender.minLabelColour = UIColor.blue2
        
//        sender.isEnabled = true
//        sender.disableRange = false
    }
    
    //MARK: - BasePopUpActionsDelegate
    func okAction(num: Int) {
        secondView.isHidden = true
        // refresh table
        if let tbl = self.superview?.superview as? UITableView {
            
            //            changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: domainsOrRolesTbl.isHidden ? -100 : 100)
            tbl.beginUpdates()
            tbl.endUpdates()
            
            changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: -(secondView.frame.height))
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם אתה בטוח שברצונך להסיר את השעות?"
        
        openAnotherPageDelegate.openViewController!(VC: basePopUp)
    }

}
