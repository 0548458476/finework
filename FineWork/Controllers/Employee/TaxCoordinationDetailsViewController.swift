//
//  TaxCoordinationDetailsViewController.swift
//  FineWork
//
//  Created by User on 12.1.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
//פופאפ המוצג בבחירת ״אני מבקש תיאום מס לפי אישור פקיד״
class TaxCoordinationDetailsViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate{
    
    //MARK: - outlet
    
    //MARK: - labels
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblenterDetails: UILabel!

    @IBOutlet weak var lblToSum: UILabel!
    
    @IBOutlet weak var lblPercentofDeduction: UILabel!
    
    @IBOutlet weak var lblOverTaxRate: UILabel!
    
    @IBOutlet weak var lblCertifyCorretValues: UILabel!
      @IBOutlet weak var lblAddFileTitle: UILabel!
    
    //MARK: textField
    
    @IBOutlet weak var txtToSum: UITextField!
    
    @IBOutlet weak var txtPercentofDeduction: UITextField!
    
    @IBOutlet weak var txtOverTaxRate: UITextField!
    
    //MARK: - buttons
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var btnCertifyCorrectValues: CheckBox!
    
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnAddFile: UIButton!
    
    @IBOutlet weak var btnCancelAddFile: UIButton!
    
    //MARK: - actions
    @IBAction func btnCloseAction(_ sender: UIButton) {
        
        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.registerKeyboardNotifications()
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnCertifyCorrectAction(_ sender: Any) {
        btnCertifyCorrectValues.isChecked = !btnCertifyCorrectValues.isChecked
    }
    
    @IBAction func btnAddFileAction(_ sender: UIButton) {
        if GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileURL != ""//מהשרת ויש קובץ
        {
//            let url : NSString = api.sharedInstance.buldUrlFile(fileName: GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileURL) as NSString
//            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
//            if let url = NSURL(string: urlStr as String) {
//                if let data = NSData(contentsOf: url as URL) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
            
            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
            //                    controller?.img = UIImage(data: data as Data)
            controller?.fileUrl = GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileURL
            self.present(controller!, animated: true, completion: nil)
//                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
//                }
//            }
        }
        else
        {
            showCameraActionSheet()
        }
    }
    
    @IBAction func btnCancelAddFileAction(_ sender: Any) {
        Alert.sharedInstance.showAskAlertWithDelegate(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.SURE_DELETE_IMAGR_FILE, comment: ""), delegate: self)
        
            }

    @IBAction func btnSaveAction(_ sender: UIButton) {
        var mess = ""
        var mustCheck = false
        if txtToSum.text != ""{
            if (txtToSum.text?.characters.count)! < 8{
                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nTillAmount = Float(txtToSum.text!)!
                mustCheck = true
            }else{
               mess = "הסכום המקסימילי צריך להיות עד 7 ספרות\n"
            }
        }else{
             GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nTillAmount = 0.0
        }
        if txtPercentofDeduction.text != ""{
            if Float(txtPercentofDeduction.text!)! < 100{
               GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fDeductedPercent = Float(txtPercentofDeduction.text!)!
                mustCheck = true
            }else{
                mess += "אחוזי הניכוי צריכים להיות נמוכים מ 100\n"
            }
        }else{
          GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fDeductedPercent = 0.0
        }
        if txtOverTaxRate.text != ""{
            if Float(txtOverTaxRate.text!)! < 100{
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nBeyondTaxDeducted = Float(txtOverTaxRate.text!)!
                mustCheck = true
            }else{
                mess += "שעור המס מהעבר לסך צריך להיות נמוך מ 100\n"
            }
        }else{
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nBeyondTaxDeducted = 0.0
        }
        if btnCancelAddFile.isHidden == true{
//            GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj = FileObj()
//           mess += "חובה לצרף קובץ"
        }else{
            mustCheck = true
        }
        if mess != ""{
            Alert.sharedInstance.showAlert(mess: mess)
        }else{
            if mustCheck{
                if btnCertifyCorrectValues.isChecked == false{
                    Alert.sharedInstance.showAlert(mess: "לא ניתן לשמור את הנתונים ללא הצהרה כי הפרטים שהוזנו נכונים! ")
                }else{
                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.registerKeyboardNotifications()
                    
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.registerKeyboardNotifications()
                
                self.dismiss(animated: true, completion: nil)
            }
        }

//
//        if btnCertifyCorrectValues.isChecked == true
//        {
//            if txtToSum.text != ""{
//                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nTillAmount = Float(txtToSum.text!)!
//            }
//            
//            if txtPercentofDeduction.text != ""
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fDeductedPercent = Float(txtPercentofDeduction.text!)!
//            }
//            
//            if txtOverTaxRate.text != ""{
//                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nBeyondTaxDeducted = Float(txtOverTaxRate.text!)!
//            }
//            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.registerKeyboardNotifications()
//            
//            self.dismiss(animated: true, completion: nil)
//        }
//        else
//        {
//            if txtToSum.text != "" || txtPercentofDeduction.text != "" || txtOverTaxRate.text != ""{
//                Alert.sharedInstance.showAlert(mess: "לא ניתן לשמור את הנתונים ללא הצהרה כי הפרטים שהוזנו נכונים! ")
//            }
//            else{
//                self.dismiss(animated: true, completion: nil)
//            }
//        }
    }
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setDesign()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setDesign()
    {
        setBorders()
        setLocalizableString()
    }
    
    func setBorders()
    {
        btnSave.layer.cornerRadius = 20
        
        btnAddFile.layer.borderColor = UIColor.orange2.cgColor
        btnAddFile.layer.borderWidth = 1
        btnAddFile.layer.cornerRadius = 20
    }
    
    func setLocalizableString()
    {
       //SET TEXT TO BUTTONS
       btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
        btnSave.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.SAVE, comment: ""), for: .normal)
    }
    
    func setData()
    {
        var str =
        String(GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nTillAmount)
        
        if str != "0.0"
        {
            if String(str.characters.suffix(2)) == ".0"
            {
                txtToSum.text = String(str.characters.dropLast(2))
            }
            else
            {
                txtToSum.text = str
            }
        }
        else
        {
            txtToSum.text = ""
        }
        
        
        str = String(GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fDeductedPercent)
        
        if str != "0.0"
        {
            if String(str.characters.suffix(2)) == ".0"
            {
                txtPercentofDeduction.text = String(str.characters.dropLast(2))
            }
            else
            {
                txtPercentofDeduction.text = str
            }
        }
        else
        {
            txtPercentofDeduction.text = ""
        }
        
        
        str = String(GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.nBeyondTaxDeducted)
        
        if str != "0.0"
        {
            if String(str.characters.suffix(2)) == ".0"
            {
                txtOverTaxRate.text = String(str.characters.dropLast(2))
            }
            else
            {
                txtOverTaxRate.text = str
            }
        }
        else
        {
            txtOverTaxRate.text = ""
        }
        
        if GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileURL != ""//מהשרת ויש קובץ
        {
            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.VIEW_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = false
        }
        else if GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileName != ""//עכשיו תו״כ עריכת הטופס
        {
            //הצגת שם הקובץ
            btnAddFile.setTitle(GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileName, for: .normal)
            btnCancelAddFile.isHidden = false
        }
        else// אין קובץ-הצגת הטקסט הדיפולטיבי:״צירוף אישור זכאות״
        {
            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = true
        }
    }

    // MARK: - KeyBoard
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    //MARK: - camera func
    func showCameraActionSheet()
    {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "ביטול ", destructiveButtonTitle: nil, otherButtonTitles:"צלם","העלה")
        
        actionSheet.show(in: self.view)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            openCamera()
        }
        else if buttonIndex == 2
        {
            openLibrary()
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: NSDictionary) {
        
        if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
            ALAssetsLibrary().asset(for: referenceUrl as URL!, resultBlock: { asset in
                let imageName = asset?.defaultRepresentation().filename()
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        self.changeBtnDesign(imgName: imageName!,img: imageString)
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
                
                //do whatever with your file name
//                self.changeBtnDesign(imgName: imageName!,img: tempImage)
//                picker.dismiss(animated: true, completion: nil);
            }, failureBlock: nil)
        }else{
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    self.changeBtnDesign(imgName: imageName,img: imageString)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }

    }
    
    //MARK: - changeDesign
    
    func changeBtnDesign(imgName:String,img:String)
    {
        GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFile = img
        GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileName = imgName
        btnAddFile.setTitle(imgName, for: .normal)
        btnCancelAddFile.isHidden = false
    }
    

    //MARK: - AlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0{
            btnAddFile.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.ADD_FILE, comment: ""), for: .normal)
            btnCancelAddFile.isHidden = true
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileName = ""//מחיקת הקובץ מהאוביקט
            GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFile = ""
            if GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileURL != ""//מהשרת ויש קובץ
            {
                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.nvFileURL = ""
                GlobalEmployeeDetails.sharedInstance.worker101Form.workerTaxCoordinationFileDet.fileObj.iFileId = -1
            }
        }
    }

}
