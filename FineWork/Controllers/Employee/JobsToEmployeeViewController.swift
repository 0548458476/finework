//
//  JobsToEmployeeViewController.swift
//  FineWork
//
//  Created by User on 28.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

protocol JobsToEmployeeDelegates {
    func openEmployerDatailsDel(index: IndexPath)
    func openJobDatailsDel(index: IndexPath)
    func startFavoraiteDel(index: IndexPath)


}
class JobsToEmployeeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, OpenAnotherPageDelegate, BasePopUpActionsDelegate, JobsToEmployeeDelegates{

    //MARK: - Views
    // open menu
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var menuPlusView: UIView!
    
    @IBOutlet weak var menuPlusTitleLbl: UILabel!
    @IBOutlet weak var menuItemLocationBtn: UIButton!
    @IBOutlet weak var menuItemAvailabilityBtn: UIButton!
    @IBOutlet weak var menuItemJobDominsBtn: UIButton!
    @IBOutlet weak var menuItemPaymentBtn: UIButton!
    
    // מציג משרות לעובד : 
    // עובד שהשלים/ שלא השלים פרופיל ויש לו משרות מתאימות
    @IBOutlet weak var tblJobs:UITableView!
    
    // לא נמצאו משרות לעובד שהשלים פרופיל
    // מוצגים לו נתונים מספריים אודות עבודתו בעבר
    @IBOutlet weak var existWorkerWithNotJobsView: UIView!
 
    @IBOutlet weak var totalEarnedLbl: UILabel! //ש”ח שהרווחת דרכנו
    @IBOutlet weak var employersFoundYouCountLbl: UILabel! //מעסיקים שמצאו אותך
    @IBOutlet weak var reportedHoursCountLbl: UILabel! //שעות שדיווחת במערכת
    
    // מחפשים משרות מתאימות ל :
    // עובד  שהשלים / שלא השלים פרופיל ועדיין לא נמצאו לו משרות מתאמות
    @IBOutlet weak var searchMatcingJobView: UIView!
    @IBOutlet weak var completeProfileView: UIView!
    @IBOutlet weak var completeProfileBtn: UIButton!
    // constrains
    @IBOutlet weak var sesrchMatchingJobHeightFromTopCon: NSLayoutConstraint!
    
    
    //MARK: - Actions
    @IBAction func btnPlusAction(_ sender: UIButton) {
        setMenuPlusState()
    }
    
    @IBAction func completeProfileBtnAction(_ sender: Any) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let viewCon = story.instantiateViewController(withIdentifier: "PersonalInformationModelViewController")as?PersonalInformationModelViewController
        openAnotherPageDelegate.openViewController!(VC: viewCon!)
    }
    
    //menu items Actions
    @IBAction func menuItemLocationBtnAction(_ sender: Any) {
        setMenuPlusState()
        let story = UIStoryboard(name: "Main", bundle: nil)
        let viewCon = story.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        viewCon?.menuTag = 1
        openAnotherPageDelegate.openViewController!(VC: viewCon!)
    }
    
    @IBAction func menuItemAvailabilityBtnAction(_ sender: Any) {
        setMenuPlusState()
        let story = UIStoryboard(name: "Main", bundle: nil)
        let viewCon = story.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        viewCon?.menuTag = 2
        openAnotherPageDelegate.openViewController!(VC: viewCon!)
    }
    
    @IBAction func menuItemJobDominsBtnAction(_ sender: Any) {
        setMenuPlusState()
        let story = UIStoryboard(name: "Main", bundle: nil)
        let viewCon = story.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        viewCon?.menuTag = 0
        openAnotherPageDelegate.openViewController!(VC: viewCon!)
    }
    
    @IBAction func menuItemPaymentBtnAction(_ sender: Any) {
        setMenuPlusState()
        let story = UIStoryboard(name: "Main", bundle: nil)
        let viewCon = story.instantiateViewController(withIdentifier: "PersonalInformationModelViewController") as? PersonalInformationModelViewController
        viewCon?.subviewTag = 1
//        viewCon?.view.tag = 1
        openAnotherPageDelegate.openViewController!(VC: viewCon!)
    }
    
    //MARK: - Variables
    var isActiveJob : Bool = false
    // delegates
    var openAnotherPageDelegate : OpenAnotherPageDelegate! = nil
    // worker home page
//    var workerHomePage = WorkerHomePage()
//    var lWorkerOfferedJobs : Array<WorkerOfferedJob> = []
 
    
    override func viewDidLoad() {
        super.viewDidLoad()

       tblJobs.separatorStyle = .none
        btnPlus.layer.cornerRadius = btnPlus.frame.width/2
        
        
        setHomePageByType()
        
        hideMenuWhenTappedAround()
        
      getWorker()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        menuPlusView.layer.borderWidth = 1
        menuPlusView.layer.borderColor = ControlsDesign.sharedInstance.colorTurquoise.cgColor
        menuPlusView.layer.cornerRadius = menuPlusView.frame.width * 0.1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count = 0
        if isActiveJob {
            count = Global.sharedInstance.lWorkerOfferedJobs.count
        } else {
            count = Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count
        }
        
        return count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell
        if isActiveJob {
          cell = tableView.dequeueReusableCell(withIdentifier: "ActiveJobsToEmployeeTableViewCell") as! ActiveJobsToEmployeeTableViewCell
            (cell as! ActiveJobsToEmployeeTableViewCell).indexPath = indexPath
            (cell as! ActiveJobsToEmployeeTableViewCell).setDisplayData(workerOfferJob: indexPath.section < Global.sharedInstance.lWorkerOfferedJobs.count ? Global.sharedInstance.lWorkerOfferedJobs[indexPath.section] : nil)
            
            (cell as! ActiveJobsToEmployeeTableViewCell).jobsToEmployeeDelegates = self

//            (cell as! ActiveJobsToEmployeeTableViewCell).imgLogo.image = nil
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "DisplayJobsToEmployeeTableViewCell") as! DisplayJobsToEmployeeTableViewCell
            (cell as! DisplayJobsToEmployeeTableViewCell).openAnotherPageDelegate = self
//            (cell as! DisplayJobsToEmployeeTableViewCell).indexPath = indexPath
            (cell as! DisplayJobsToEmployeeTableViewCell).setDisplayData(workerHomePageJob: indexPath.section < Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count ? Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[indexPath.section] : nil)
            
//            (cell as! DisplayJobsToEmployeeTableViewCell).logoImg.image = nil
            // or cell.poster.image = [UIImage imageNamed:@"placeholder.png"];
//            if indexPath.section < Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count && Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[indexPath.section].nvLogoImageFilePath != "" {
//                var stringPath = api.sharedInstance.buldUrlFile(fileName: (Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[indexPath.section].nvLogoImageFilePath))
//                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
//                let url = URL(string: stringPath)
//                var _: URLSessionTask? = URLSession.shared.dataTask(with: url!, completionHandler: {(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void in
//                    if (data != nil) {
//                        let image = UIImage(data: data!)
//                        if image != nil {
//                            DispatchQueue.main.async(execute: {() -> Void in
//                                let updateCell: DisplayJobsToEmployeeTableViewCell? = (tableView.cellForRow(at: indexPath) as! DisplayJobsToEmployeeTableViewCell)
//                                if updateCell != nil {
//                                    updateCell?.logoImg.image = image
//                                }
//                            })
//                        }
//                    }
//                })
//            }

        }
        
        
        return cell
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     //    (cell as! ActiveJobsToEmployeeTableViewCell).secondTimer = nil
    }
  
 func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   print("didSelectRowAt")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((self.view.frame.size.height * 0.77) - 3.5) / 7
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView()
//        headerView.backgroundColor = UIColor.lightGray
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.lightGray
        return headerView
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func getWorker() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        let worker = Worker()
                        Global.sharedInstance.worker = worker.getWorkerFromDictionary(dic: dicResult)
                        self.getWorkerCodeTables()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorker")
    }
    func getWorkerCodeTables() {
      
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            
            dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
            
            api.sharedInstance.goServer(params : dic,success : { // 8
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] { // 7
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 { // 5
                        if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> { // 3
                            
                            var workerCodeTables : Array<SysTableDictionary> = []
                            let sysTable = SysTableDictionary()
                            
                            for workerCodeTableDic in dicResult { // 0
                                workerCodeTables.append(sysTable.getSysTableDictionaryFromDic(dic: workerCodeTableDic))
                            } // 0
                            
                            Global.sharedInstance.workerCodeTables = workerCodeTables
                            
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : "GetWorkerCodeTables") // 9
        
    }

    func UpdateFavorite(bFavorite: Bool , iWorkerOfferedJobId : Int) {
        
       
              Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        dic["iWorkerOfferedJobId"] = iWorkerOfferedJobId as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["bFavorite"] =  bFavorite as AnyObject?
        
        print("ss")
        print(dic)
        
        api.sharedInstance.goServer(params : dic,success : { // 8
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] { // 7
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool {
                        if dicResult {
                                                      print("reloadData")
                            self.tblJobs.beginUpdates()
                            self.tblJobs.endUpdates()
                            self.tblJobs.reloadData()
                            
                            
                        } else {
                            Alert.sharedInstance.showAlert(mess: "הפעולה לא נשמרה")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }  else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateFavorite")
        
    }

    func updateWorkerOfferedJob(index: IndexPath) {
        
        let workerOfferedJobs  = Global.sharedInstance.lWorkerOfferedJobs[index.section]
        workerOfferedJobs.bFavorite = !workerOfferedJobs.bFavorite
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        dic["workerOfferedJob"] = workerOfferedJobs.getDicFromWorkerOfferedJob() as AnyObject?
            dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?

 print("ss")
        print(dic)

        api.sharedInstance.goServer(params : dic,success : { // 8
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] { // 7
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool {
                        if dicResult {
                            print(Global.sharedInstance.lWorkerOfferedJobs[index.section].bFavorite)
                              Global.sharedInstance.lWorkerOfferedJobs[index.section].bFavorite = !Global.sharedInstance.lWorkerOfferedJobs[index.section].bFavorite
                            print(Global.sharedInstance.lWorkerOfferedJobs[index.section].bFavorite)
print("reloadData")
                            self.tblJobs.beginUpdates()
                            self.tblJobs.endUpdates()
                            self.tblJobs.reloadData()


                        } else {
                            Alert.sharedInstance.showAlert(mess: "הפעולה לא נשמרה")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }  else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorkerOfferedJob")
        
    }

    func setHomePageByType() {
        switch Global.sharedInstance.workerHomePage.iWorkerHomePageType {
        case WorkerHomePageType.NewWorkerCompleteProfile.rawValue:
//            isActiveJob = true
            if !Global.sharedInstance.workerHomePage.bWorkerOfferedJob {
                setWorkerWithoutJobs(isSearchMatchingJob: true, isCompleteProfile: true)
            } else {
                setWorkerWithJobs(_isActiveJobs: true)
            }
            break
        case WorkerHomePageType.NewWorkerNotCompleteProfile.rawValue:
//            isActiveJob = false
            if Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count > 0 {
                setWorkerWithJobs(_isActiveJobs: false)
            } else {
                setWorkerWithoutJobs(isSearchMatchingJob: true, isCompleteProfile: false)
            }
            break
        case WorkerHomePageType.ExistWorker.rawValue:
            if !Global.sharedInstance.workerHomePage.bWorkerOfferedJob {
                setWorkerWithoutJobs(isSearchMatchingJob: false, isCompleteProfile: false)
            } else {
//                isActiveJob = true
                setWorkerWithJobs(_isActiveJobs: true)
            }
            break
        default:
            break
        }
        
        
    }
    
    var counter = 0
//    var _ : UIImage
//    var imageMap = Dictionary<String, UIImage>()
    var imageMap = Array<String>()
    
    func setWorkerWithJobs(_isActiveJobs : Bool) {
        isActiveJob = _isActiveJobs
        
        existWorkerWithNotJobsView.isHidden = true
        searchMatcingJobView.isHidden = true
        tblJobs.isHidden = false
        
        //--
        counter = 0
        downloadsCounter = 0
        imageMap.removeAll()
        if isActiveJob {
            
            for index in stride(from: 0, to: Global.sharedInstance.lWorkerOfferedJobs.count, by: 1) {
                if Global.sharedInstance.lWorkerOfferedJobs[index].nvLogoImageFilePath != "" {
                    var isExist = false
//                    var index2 = 0;
                    for i in stride(from: 0, to: imageMap.count, by: 1) {
                        if Global.sharedInstance.lWorkerOfferedJobs[index].nvLogoImageFilePath == imageMap[i] {
                            isExist = true
//                            index2 = i
                            break
                        }
                    }
                    if !isExist {
                        imageMap.insert(Global.sharedInstance.lWorkerOfferedJobs[index].nvLogoImageFilePath, at: imageMap.count)
                        counter += 1
                        var stringPath = api.sharedInstance.buldUrlFile(fileName: (Global.sharedInstance.lWorkerOfferedJobs[index].nvLogoImageFilePath))
                        stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                        if let url = URL(string: stringPath) {
                            downloadImage(url: url, index: /*index*/imageMap.count-1)
                        }
                    }
                }
                
//                item.logoImage.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
            }
        }
        else {
            for index in stride(from: 0, to: Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count, by: 1) {
                if Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[index].nvLogoImageFilePath != "" {
                    var isExist = false
//                    var index2 = 0;
                    for i in stride(from: 0, to: imageMap.count, by: 1) {
                        if Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[index].nvLogoImageFilePath == imageMap[i] {
                            isExist = true
//                            index2 = i
                            break
                        }
                    }
                    if !isExist {
                        imageMap.insert(Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[index].nvLogoImageFilePath, at: imageMap.count)
                        counter += 1
                        var stringPath = api.sharedInstance.buldUrlFile(fileName: (Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[index].nvLogoImageFilePath))
                        stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                        if let url = URL(string: stringPath) {
                            downloadImage(url: url, index: /*index*/imageMap.count-1)
                        }
                    }
//                    counter += 1
//                    var stringPath = api.sharedInstance.buldUrlFile(fileName: (Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[index].nvLogoImageFilePath))
//                    stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
//                    if let url = URL(string: stringPath) {
//                        downloadImage(url: url, index: index)
//                    }
                }
                
                //                item.logoImage.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
            }
//            for item in Global.sharedInstance.workerHomePage.lWorkerHomePageJobs {
//                var stringPath = api.sharedInstance.buldUrlFile(fileName: (item.nvLogoImageFilePath))
//                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
//                item.logoImage.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
//            }
        }
        //--
        
        tblJobs.delegate = self
        tblJobs.dataSource = self
    }
    
    func setWorkerWithoutJobs(isSearchMatchingJob : Bool, isCompleteProfile : Bool) {
        if isSearchMatchingJob {
            searchMatcingJobView.isHidden = false
            existWorkerWithNotJobsView.isHidden = true
            tblJobs.isHidden = true
            
            if isCompleteProfile {
                completeProfileView.isHidden = true
                sesrchMatchingJobHeightFromTopCon.constant = 10
            } else {
                Global.sharedInstance.setButtonCircle(sender: completeProfileBtn, isBorder: false)
                completeProfileView.isHidden = false
                sesrchMatchingJobHeightFromTopCon.constant = 10 + completeProfileView.frame.height
            }
        } else {
            totalEarnedLbl.text = String(Global.sharedInstance.workerHomePage.iTotalEarned)
            employersFoundYouCountLbl.text = String(Global.sharedInstance.workerHomePage.iEmployersFoundYouCount)
            reportedHoursCountLbl.text = String(Global.sharedInstance.workerHomePage.iReportedHoursCount)
            
            existWorkerWithNotJobsView.isHidden = false
            tblJobs.isHidden = true
            searchMatcingJobView.isHidden = true
            
        }
        
    }
    
    //MARK: OpenAnotherPageDelegate
    var viewCon : UIViewController? = nil
    func openViewController(VC: UIViewController) {
        if !searchMatcingJobView.isHidden {
            openAnotherPageDelegate.openViewController!(VC: VC)
        } else {
            viewCon = VC
            openBasePopUp(text: "יש להשלים את תהליך ההרשמה המלא, ולאחר מכן יוכל מציע המשרה לעיין בפרטיך.")
        }
    }
    
    func openEmployerDatailsDel(index: IndexPath) {
        print(index.count)
        
        let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
         let vc = workerStory.instantiateViewController(withIdentifier: "EmployerDetailsPopUpViewController") as! EmployerDetailsPopUpViewController
        vc.setDisplayData(index: index.section)
        self.navigationController?.pushViewController(vc, animated: true)
      // self.present(vc, animated: true, completion: nil)
    }
    
    func openJobDatailsDel(index: IndexPath) {
       updateWorkerOfferedJobBaloonOccur(index: index, completion: { (isSuccess) in
            
           if isSuccess {
                
                var workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
                
                if Global.sharedInstance.lWorkerOfferedJobs[index.section].iOfferJobStatusType == OfferJobStatusType.new.rawValue  || Global.sharedInstance.lWorkerOfferedJobs[index.section].iOfferJobStatusType == OfferJobStatusType.interesting.rawValue {
                    let    vc = workerStory.instantiateViewController(withIdentifier: "statusJobWhitCalanderViewController") as! statusJobWhitCalanderViewController
                    vc.setDisplayData(index: index.section)
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else   if Global.sharedInstance.lWorkerOfferedJobs[index.section].iOfferJobStatusType == OfferJobStatusType.whiting_for_rank.rawValue {
                    workerStory = UIStoryboard(name: "StoryboardGeneral", bundle: nil)
                    let    vc = workerStory.instantiateViewController(withIdentifier: "HowWasTheJobViewController") as! HowWasTheJobViewController
                    vc.setDisplayData(index: index.section)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else   if Global.sharedInstance.lWorkerOfferedJobs[index.section].iOfferJobStatusType == OfferJobStatusType.active.rawValue {
                    let    vc = workerStory.instantiateViewController(withIdentifier: "ReportPresenceViewController") as! ReportPresenceViewController
                    vc.setDisplayData(index: index.section)
                    self.navigationController?.pushViewController(vc, animated: true)
                }else
                {
                    let   vc = workerStory.instantiateViewController(withIdentifier: "StatusJobViewController") as! StatusJobViewController
                    vc.setDisplayData(index: index.section)
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                print(index.count)

          }
            
        })

       
        
    }
      

    func startFavoraiteDel(index: IndexPath){
          //  updateWorkerOfferedJob(index: index)
            Global.sharedInstance.lWorkerOfferedJobs[index.section].bFavorite = !Global.sharedInstance.lWorkerOfferedJobs[index.section].bFavorite
        UpdateFavorite(bFavorite: Global.sharedInstance.lWorkerOfferedJobs[index.section].bFavorite, iWorkerOfferedJobId: Global.sharedInstance.lWorkerOfferedJobs[index.section].iWorkerOfferedJobId)
        
           }

    //MARK: - BasePopUpViewController
    func openBasePopUp(text : String) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = /*self.storyboard?*/story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        basePopUp.text = text
        basePopUp.basePopUpActionDelegate = self
        
        self.present(basePopUp, animated: true, completion: nil)
    }
    
    func okAction(num: Int) {
        if viewCon != nil {
            openAnotherPageDelegate.openViewController!(VC: viewCon!)
        }
    }
    
    //MARK: - Menu Plus 
    func setMenuPlusState() {
        self.view.bringSubview(toFront: menuPlusView)
        menuPlusView.isHidden = !menuPlusView.isHidden
    }
    
    // hide menu when tap arround 
    func hideMenuWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissMenu))
        self.view.addGestureRecognizer(tap)
//        tap.cancelsTouchesInView = false
    }
    
    func dismissMenu() {
        menuPlusView.isHidden = true
    }
    
    var downloadsCounter = 0

    func downloadImage(url: URL, index: Int) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
//                    if let tbl = self.superview?.superview as? UITableView {
//                        if self.indexPath != nil && tbl.cellForRow(at: self.indexPath!) != nil {
//                            self.imgLogo.contentMode = .scaleToFill
                    if self.isActiveJob {
//                        Global.sharedInstance.lWorkerOfferedJobs[index].logoImage = image
                        
                        for i in stride(from: 0, to: Global.sharedInstance.lWorkerOfferedJobs.count, by: 1) {
                            if Global.sharedInstance.lWorkerOfferedJobs[i].nvLogoImageFilePath == self.imageMap[index] {
                                Global.sharedInstance.lWorkerOfferedJobs[i].logoImage = image
                            }
                        }
                    } else {
//                        Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[index].logoImage = image
                        for i in stride(from: 0, to: Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count, by: 1) {
                            if Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[i].nvLogoImageFilePath == self.imageMap[index] {
                                Global.sharedInstance.workerHomePage.lWorkerHomePageJobs[i].logoImage = image
                            }
                        }
                    }
                    
                    self.downloadsCounter += 1
                    
                    print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                    if self.downloadsCounter == self.counter {
                        self.tblJobs.reloadData()
                    }
                    
//                    if (self.isActiveJob && index == Global.sharedInstance.lWorkerOfferedJobs.count - 1) || (!self.isActiveJob && index == Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count - 1) {
//                        self.tblJobs.reloadData()
//                    }
//                            self.imgLogo.image = image
//                        }
//                    }
                }
            } else {
                self.downloadsCounter += 1
                
                print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                if self.downloadsCounter == self.counter {
                    self.tblJobs.reloadData()
                }
            }
            
            
            
        }
    }
    

    func updateWorkerOfferedJobBaloonOccur (index: IndexPath, completion : @escaping (Bool) -> Void) {
    
        if Global.sharedInstance.lWorkerOfferedJobs[index.section].iBaloonOccur > 0 {
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            
            dic["iWorkerOfferedJobId"] = Global.sharedInstance.lWorkerOfferedJobs[index.section].iWorkerOfferedJobId as AnyObject?
            dic["iLanguageId"] =  Global.sharedInstance.iLanguageId as AnyObject
            dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
            
            api.sharedInstance.goServer(params : dic,success : {
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] {
                    completion(true)
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    completion(false)
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : api.sharedInstance.UpdateWorkerOfferedJobBaloonOccur)
        }
        else{
            completion(true)
        }
            
    }
    
    
    
//    func downloadImageToHomePageJob(url: URL, index: Int) {
//        print("Download Started")
//        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
//            guard let data = data, error == nil else { return }
//            if let image = UIImage(data: data){
//                DispatchQueue.main.async {
//                    //                    if let tbl = self.superview?.superview as? UITableView {
//                    //                        if self.indexPath != nil && tbl.cellForRow(at: self.indexPath!) != nil {
//                    //                            self.imgLogo.contentMode = .scaleToFill
//                    Global.sharedInstance.lWorkerOfferedJobs[index].logoImage = image
//                    //                            self.imgLogo.image = image
//                    //                        }
//                    //                    }
//                }
//            }
//            
//            if index == Global.sharedInstance.lWorkerOfferedJobs.count - 1 {
//                self.tblJobs.reloadData()
//            }
//            
//        }
//    }
}
