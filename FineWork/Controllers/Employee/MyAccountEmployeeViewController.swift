//
//  MyAccountEmployeeViewController.swift
//  FineWork
//
//  Created by User on 28.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class MyAccountEmployeeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var tblNMyAccount: UITableView!
    
    //MARK: - variables
    let arrText:Array<String> = ["תלושים","דוח נוכחות","בונוס שכר","בונוסי המלצות","הסכמי עבודה"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblNMyAccount.separatorStyle = .none
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAccountEmployeeTableViewCell", for: indexPath as IndexPath) as! MyAccountEmployeeTableViewCell
        cell.setDisplayData(text: arrText[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height / 7
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
