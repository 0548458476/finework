//
//  EmployeeModelViewController.swift
//  FineWork
//
//  Created by User on 27.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class EmployeeModelViewController: UIViewController/*, UIPageViewControllerDelegate*/, UIPageViewControllerDataSource, OpenAnotherPageDelegate , DialogTerminationWorkerDelegate, DialogHearingProcessDelegate,  BasePopUpActionsDelegate{
    
    //MARK:- Outlet
    
    @IBOutlet weak var imgNotification: UIImageView!
    //MARK:-- Buttons
    @IBOutlet weak var btn3Points: UIButton!
    
    @IBOutlet weak var btnProfile: UIButton!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var btnJobs: UIButton!
    
    @IBOutlet weak var btnMyAcccount: UIButton!
    
    @IBOutlet weak var btnMessages: UIButton!
    
    @IBOutlet weak var btnPreferencesJobs: UIButton!//העדפות  משרה
    
    @IBOutlet weak var btnPreferredPositions: UIButton!//משרות מעדפות
  
    @IBOutlet weak var btnPersonalInformation: UIButton!//פרטים אישיים
   
    @IBOutlet weak var btnExit: UIButton!
    
    //MARK:-- Labels
    @IBOutlet weak var lblFineWork: UILabel!
    
    @IBOutlet weak var lblJobsbtn: UILabel!
    
    @IBOutlet weak var lblNumJobs: UILabel!
    @IBOutlet weak var viewNumJob: UIView!
    
    //MARK: - Views
    
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var viewUnderJobs: UIView!
    
    @IBOutlet weak var viewUnderMyAccount: UIView!
    
    @IBOutlet weak var viewUnderMessages: UIView!
    
    @IBOutlet weak var viewTable: UIView!
    //MARK:-- Actions
    
    @IBAction func btnJobAction(_ sender: Any) {
        viewUnderJobs.isHidden = false
        viewUnderMessages.isHidden = true
        viewUnderMyAccount.isHidden = true
        
        setPageInPageViewController(index: 0, isAnimation: true)
        
//        self.view.addSubview(JobsViewCon.view)
    }
    
    @IBAction func btnMyAccountAction(_ sender: Any) {
        viewUnderJobs.isHidden = true
        viewUnderMessages.isHidden = true
        viewUnderMyAccount.isHidden = false
        setPageInPageViewController(index: 1, isAnimation: true)
//        self.view.addSubview(myAccountViewCon.view)
    }
    
    @IBAction func btnMessagesAction(_ sender: Any) {
        viewUnderJobs.isHidden = true
        viewUnderMessages.isHidden = false
        viewUnderMyAccount.isHidden = true
        setPageInPageViewController(index: 2, isAnimation: true)
//        self.view.addSubview(messagesViewCon.view)
    }
    
    @IBAction func btn3PointsAction(_ sender: Any) {
//        let viewCon101 = (self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as?LoginViewController?)!
//        self.navigationController?.pushViewController(viewCon101!, animated: true)
        
//        let _ = self.navigationController?.popViewController(animated: true)
//        GlobalEmployeeDetails.sharedInstance.replaceUser()

        if conBtnExitWidth.constant == 0{
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.conBtnExitWidth.constant = 150
                self.view.layoutIfNeeded()
            }
        }else{
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5) {
                self.conBtnExitWidth.constant = 0
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    @IBAction func btnProfileAction(_ sender: Any) {
        self.view.bringSubview(toFront: viewTable)
        
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker:
            viewTable.isHidden = !viewTable.isHidden
            break
        case UserType.employer:
            let viewCon = storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
            self.navigationController?.pushViewController(viewCon!, animated: true)
            break
        default:
            break
        }
    }
    
    
    @IBAction func btnSearchAction(_ sender: Any) {
    }
    
    @IBAction func btnPreferencesJobsAction(_ sender: UIButton) {//העדפות משרה
        let viewCon = storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        self.navigationController?.pushViewController(viewCon!, animated: true)
    }
    
    @IBAction func btnPreferredPositionsAction(_ sender: UIButton) {//משרות מעדפות
        
    }
    
    @IBAction func btnPersonalInformationAction(_ sender: UIButton) {//פרטים אישיים
        let viewCon = storyboard?.instantiateViewController(withIdentifier: "PersonalInformationModelViewController")as?PersonalInformationModelViewController
        self.navigationController?.pushViewController(viewCon!, animated: true)
    }
    
    @IBAction func btnExitAction(_ sender: UIButton) {
//        let _ = self.navigationController?.popViewController(animated: true)
        GlobalEmployeeDetails.sharedInstance.replaceUser()
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = UINavigationController(rootViewController: loginViewController)
        appdelegate.window!.rootViewController = nav
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.setLoginRoot()
        
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let loginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//        UIApplication.shared.keyWindow?.rootViewController = loginViewController
    }
    
    //MARK:- Constrain
    
    @IBOutlet weak var conBtnExitWidth: NSLayoutConstraint!
    
    //MARK:- Varibals
    var whichNotificationShow : Int = 0
    // jobs to employee
    var JobsViewCon : JobsToEmployeeViewController?
    // jobs to employer
    var jobsToEmployerViewCon : JobsToEmployerViewController?
    
    var messagesViewCon : MessagesEmployeeUIViewController = MessagesEmployeeUIViewController()
    var myAccountViewCon : MyAccountEmployeeViewController = MyAccountEmployeeViewController()
    var pageViewController : UIPageViewController?
//    var workerHomePage = WorkerHomePage()
    
    //MARK:- initial
    override func viewDidLoad() {
        super.viewDidLoad()
        Global.sharedInstance.receiveRemoteNotificationDelegate = self
        lblNumJobs.layer.cornerRadius = lblNumJobs.frame.width/2
        lblNumJobs.layer.masksToBounds = true
        btn3Points.setTitle("\u{f142}", for: .normal)
        btnSearch.setTitle("\u{f002}", for: .normal)
        btnProfile.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)
        viewUnderJobs.isHidden = false
        viewUnderMessages.isHidden = true
        viewUnderMyAccount.isHidden = true
        
        self.imgNotification.isHidden = true
        self.lblNumJobs.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
//        JobsViewCon = workerStory.instantiateViewController(withIdentifier: "JobsToEmployeeViewController") as! JobsToEmployeeViewController
//        
//        JobsViewCon.view.frame = CGRect(x: 0,y: subView.frame.origin.y + subView.frame.height,width: view.frame.width,height:view.frame.height - subView.frame.height)
        
        messagesViewCon = workerStory.instantiateViewController(withIdentifier: "MessagesEmployeeUIViewController") as! MessagesEmployeeUIViewController
        
        messagesViewCon.view.frame = CGRect(x: 0,y: subView.frame.origin.y + subView.frame.height,width: view.frame.width,height:view.frame.height - subView.frame.height)
        
        myAccountViewCon = workerStory.instantiateViewController(withIdentifier: "MyAccountEmployeeViewController") as! MyAccountEmployeeViewController
        
        myAccountViewCon.view.frame = CGRect(x: 0,y: subView.frame.origin.y + subView.frame.height,width: view.frame.width,height:view.frame.height - subView.frame.height)
        
        //24-04-17
//        self.view.addSubview(JobsViewCon.view)
//        createPageViewController()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        self.view.addGestureRecognizer(tap)
        
        
//        getWorkerHomePage()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.conBtnExitWidth.constant = 0
        viewTable.isHidden = true
        
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker:
            getWorkerHomePage()
            break
        case UserType.employer:
            GetLookingForYouWorkers()
            break
        default:
            break
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        viewTable.isHidden = true
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        viewTable.isHidden = true
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5) {
            self.conBtnExitWidth.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //MARK: - Page View Controller
    var viewContArr : Array<UIViewController> = []
    var viewUnderVCArr : Array<UIView> = []
    
    func createPageViewController() {
        
        let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
        /*let pageController*/pageViewController = workerStory.instantiateViewController(withIdentifier: "PageController") as? UIPageViewController
        /*pageController*/pageViewController!.dataSource = self
//        pageViewController!.delegate = self
        
//        viewContArr.insert(JobsViewCon, at: 0)
        viewContArr.insert(myAccountViewCon, at: 1)
        viewContArr.insert(messagesViewCon, at: 2)
        
        viewUnderVCArr.insert(viewUnderJobs, at: 0)
        viewUnderVCArr.insert(viewUnderMyAccount, at: 1)
        viewUnderVCArr.insert(viewUnderMessages, at: 2)
        
//        if contentImages.count > 0 {
        setPageInPageViewController(index: 0, isAnimation: false)
//            let firstController = viewContArr[0]
//            let startingViewControllers = [firstController]
//            pageController.setViewControllers(startingViewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
//        }
        
//        pageViewController = pageController
        addChildViewController(pageViewController!)
        pageViewController!.view.frame.origin.y = subView.frame.height
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParentViewController: self)
    }
    
//    func setupPageControl() {
//        let appearance = UIPageControl.appearance()
//        appearance.pageIndicatorTintColor = UIColor.grayColor()
//        appearance.currentPageIndicatorTintColor = UIColor.whiteColor()
//        appearance.backgroundColor = UIColor.darkGrayColor()
//    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = viewContArr.index(of: viewController) else {
            return nil
        }
        
        for i in stride(from: 0, to: viewUnderVCArr.count, by: 1) {
            if i == viewControllerIndex {
                viewUnderVCArr[i].isHidden = false
            } else {
                viewUnderVCArr[i].isHidden = true
            }
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard viewContArr.count > previousIndex else {
            return nil
        }
        
        return viewContArr[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = viewContArr.index(of: viewController) else {
            return nil
        }
        
        for i in stride(from: 0, to: viewUnderVCArr.count, by: 1) {
            if i == viewControllerIndex {
                viewUnderVCArr[i].isHidden = false
            } else {
                viewUnderVCArr[i].isHidden = true
            }
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = viewContArr.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return viewContArr[nextIndex]
    }
    
//    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
//        
//        for item in previousViewControllers {
//            print("con : \(item)")
//        }
//        if let firstViewController = previousViewControllers.first,
//            let index = viewContArr.index(of: firstViewController) {
//            for i in stride(from: 0, to: viewUnderVCArr.count, by: 1) {
//                if i == index+1 {
//                    viewUnderVCArr[i].isHidden = false
//                } else {
//                    viewUnderVCArr[i].isHidden = true
//                }
//            }
//        }
        
//    }
    

    
    func setPageInPageViewController(index : Int, isAnimation : Bool) {
        let firstController = viewContArr[index]
        let startingViewControllers = [firstController]
        pageViewController?.setViewControllers(startingViewControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: isAnimation, completion: nil)
    }
    
    //MARK: - Server Functions
    func getWorkerHomePage() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        Global.sharedInstance.workerHomePage = Global.sharedInstance.workerHomePage.getWorkerHomePageFromDic(dic: dicResult)
                        self.setHomePageByType()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetWorkerHomePage)
    }
    
    func getWorkerOfferedJobs() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        Global.sharedInstance.lWorkerOfferedJobs.removeAll()
                        let offerdJob = WorkerOfferedJob()
                        for dic in dicResult {
                            Global.sharedInstance.lWorkerOfferedJobs.insert(offerdJob.getWorkerOfferedJobFromDic(dic: dic), at: Global.sharedInstance.lWorkerOfferedJobs.count)
                        }
//                        Global.sharedInstance.workerHomePage = Global.sharedInstance.workerHomePage.getWorkerHomePageFromDic(dic: dicResult)
                        self.UpdateBaloonOccur()
                        self.openJobsToEmployeeForm()
//                        self.setHomePageByType()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetWorkerOfferedJobs)
    }
    
    //MARK: - Server Function - טעינת משרות למעסיק
    func GetLookingForYouWorkers() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerId"] = Global.sharedInstance.user.iEmployerId  as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> {
                        Global.sharedInstance.lLookingForYouWorkers.removeAll()
                        let save = LookingForYouWorkers()
                        for list in dicResult { // 0
                            Global.sharedInstance.lLookingForYouWorkers.append(save.getLookingForYouWorkersFromDic(dic: list))
                        }
                        self.UpdateBaloonOccur()
                        self.openJobsToEmployerForm()
                    } } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetLookingForYouWorkers")
    }
    
    
    func setHomePageByType() {
        self.imgNotification.isHidden = false
        self.lblNumJobs.isHidden = false
        switch Global.sharedInstance.workerHomePage.iWorkerHomePageType {
        case WorkerHomePageType.NewWorkerCompleteProfile.rawValue:
            if Global.sharedInstance.workerHomePage.bWorkerOfferedJob {
                getWorkerOfferedJobs()
            } else {
                self.imgNotification.isHidden = true
                lblNumJobs.isHidden = true
                openJobsToEmployeeForm()
            }
            break
        case WorkerHomePageType.NewWorkerNotCompleteProfile.rawValue:
            if Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count > 0 {
                lblNumJobs.text = String(Global.sharedInstance.workerHomePage.lWorkerHomePageJobs.count)
            } else {
                 self.imgNotification.isHidden = true
                lblNumJobs.isHidden = true
            }
            openJobsToEmployeeForm()
            break
        case WorkerHomePageType.ExistWorker.rawValue:
            if Global.sharedInstance.workerHomePage.bWorkerOfferedJob {
                getWorkerOfferedJobs()
            } else {
                 self.imgNotification.isHidden = true
                lblNumJobs.isHidden = true
                openJobsToEmployeeForm()
            }
            break
        default:
            break
        }
    }
    
    func openJobsToEmployeeForm() {
        
        JobsViewCon = JobsToEmployeeViewController()
        
        let workerStory = UIStoryboard(name: "StoryboardWorker", bundle: nil)
        JobsViewCon = workerStory.instantiateViewController(withIdentifier: "JobsToEmployeeViewController") as? JobsToEmployeeViewController
        
        JobsViewCon?.openAnotherPageDelegate = self
        
        JobsViewCon!.view.frame = CGRect(x: 0,y: subView.frame.origin.y + subView.frame.height,width: view.frame.width,height:view.frame.height - subView.frame.height)
        
        viewContArr.removeAll()
        viewContArr.insert(JobsViewCon!, at: 0)
        
        createPageViewController()
    }
  
    func openJobsToEmployerForm() {//מעסיק
        
        jobsToEmployerViewCon = JobsToEmployerViewController()
        
        let workerStory = UIStoryboard(name: "StoryboardEmployer", bundle: nil)
        jobsToEmployerViewCon = workerStory.instantiateViewController(withIdentifier: "JobsToEmployerViewController") as? JobsToEmployerViewController
        
  //  jobsToEmployerViewCon?.openAnotherPageDelegate = self
        
        jobsToEmployerViewCon!.view.frame = CGRect(x: 0,y: subView.frame.origin.y + subView.frame.height,width: view.frame.width,height:view.frame.height - subView.frame.height)
        
        viewContArr.removeAll()
        viewContArr.insert(jobsToEmployerViewCon!, at: 0)
        
        createPageViewController()
    }

    //MARK: - Open Another Page Delegate
    func openViewController(VC: UIViewController) {
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    
    func UpdateBaloonOccur() {
        var sum = 0
        switch Global.sharedInstance.user.iUserType {
        case UserType.worker: //עובד
            for job in Global.sharedInstance.lWorkerOfferedJobs {
                sum += job.iBaloonOccur
            }
            break
        case UserType.employer:
            for Look in Global.sharedInstance.lLookingForYouWorkers {
                sum += Look.iBaloonOccur
            }
            break
        default:
            break
        }
        
        if  sum > 0  {
             self.lblNumJobs.text = String(sum)
             self.imgNotification.isHidden = false
             self.lblNumJobs.isHidden = false
            
        } else {
             self.imgNotification.isHidden = true
             self.lblNumJobs.isHidden = true
        }
    }
    
    //dialogTerminationWorkerDelegate //שייך לדיאלוג סיום עבודה
    func okdialogTerminationAction(iJobTerminationtId :Int, iEmployerJobId: Int, iWorkerOfferedJobId: Int) {
         JobTerminationIsHearingProcessUpdate(b : false, iJobTerminationtId :iJobTerminationtId, iEmployerJobId: iEmployerJobId, iWorkerOfferedJobId: iWorkerOfferedJobId)
    }
    func nextdialogTerminationAction(iJobTerminationtId :Int, iEmployerJobId: Int, iWorkerOfferedJobId: Int) {
        JobTerminationIsHearingProcessUpdate(b : true, iJobTerminationtId :iJobTerminationtId, iEmployerJobId: iEmployerJobId, iWorkerOfferedJobId: iWorkerOfferedJobId)
    }
    
    //DialogHearingProcessDelegate //שייך לדיאלוג שימוע
    func stopDialogHearingAction(iJobTerminationtId: Int, s:String) {
        jobTerminationWorkerWantContinueUpdate(iJobTerminationtId:iJobTerminationtId, b:false, s:s)
    }
    func nextDialogHearingAction(iJobTerminationtId: Int, s:String) {
        jobTerminationWorkerWantContinueUpdate(iJobTerminationtId:iJobTerminationtId, b:true, s:s)
    }
    
                //הסכמה לסיום או מעבר לשימוע//
    func JobTerminationIsHearingProcessUpdate(b :Bool, iJobTerminationtId :Int, iEmployerJobId: Int, iWorkerOfferedJobId: Int) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobTerminationtId"] = iJobTerminationtId as AnyObject
        dic["bHearingProcess"] =  b as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if dic["Result"]?["bHearingProcess"] as! Bool ==  false { //סיום עבודה
                        if (dic["Result"]?["bWorkerGiveUpOnNotice"]) != nil{
                            let b = dic["Result"]!["bWorkerGiveUpOnNotice"] as! Bool
                            let s = dic["Result"]?["iJobTerminationNoticeDays"] as! Int
                            self.notHearingProcess(b :b, s :s)
                        }
                    }
                    else{ //מעבר לשימוע
                         self.hearingProcess(iJobTerminationtId:iJobTerminationtId, sthana: dic["Result"]?["nvEmployeeExplain"] as! String ,iEmployerJobId: iEmployerJobId, iWorkerOfferedJobId: iWorkerOfferedJobId)
                    }
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "JobTerminationIsHearingProcessUpdate")
    }
    // סיום עבודה סופי//
    func notHearingProcess(b :Bool, s :Int) {
        var msg :String = ""
        
        if b ==  true {
            msg = "העבודה תסתיים בעוד "  + String(s) + " " + "ימים"
        } else {
            msg = "העבודה תסתים מידית, תזוכה בגין ימי הודעה מוקדמת בסך " + String(s) + " " + "ימים"
        }
        self.openBasePopUp(msg :msg, titel : "", nummisra : "", num:0, showCancelBtn: false)
        
    }
    // פתיחת דיאלוג שימוע//
    func hearingProcess(iJobTerminationtId: Int, sthana: String, iEmployerJobId: Int, iWorkerOfferedJobId: Int) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "DialogHearingProcessViewController") as! DialogHearingProcessViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
       
        if iEmployerJobId != 0 && iWorkerOfferedJobId != 0{
            basePopUp.numMisra = String(iEmployerJobId)+"/"+String(iWorkerOfferedJobId)
        }
        basePopUp.iJobTerminationtId = iJobTerminationtId
        basePopUp.thana = sthana
        basePopUp.dialogHearingProcessDelegate = self
        
        self.present(basePopUp, animated: true, completion: nil)
        
    }
    
    //להמשיך או להפסיק את העבודה//
    func jobTerminationWorkerWantContinueUpdate(iJobTerminationtId:Int, b:Bool, s:String) {
    
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobTerminationtId"] = iJobTerminationtId as AnyObject
        dic["bWorkerWantContinue"] =  b as AnyObject
        dic["nvWorkerResponse"] = s as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if dic["Result"]?["bWorkerWantContinue"] as! Bool ==  false { //סיום עבודה
                        self.openBasePopUp(msg :"העבודה הסתימה", titel : "", nummisra : "", num:0, showCancelBtn: false)
                    }
                    else{
                        self.openBasePopUp(msg :"העבודה ממשיכה", titel : "", nummisra : "", num:0, showCancelBtn: false)
                    }
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "JobTerminationWorkerWantContinueUpdate")
    }
    
    //MARK: - BasePopUpViewController
    func openBasePopUp(msg : String, titel : String, nummisra : String , num: Int , showCancelBtn: Bool) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = /*self.storyboard?*/story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        basePopUp.text = msg
        basePopUp.titelText = titel
        basePopUp.nummisra = nummisra
        basePopUp.basePopUpActionDelegate = self
        basePopUp.num = num
        basePopUp.showCancelBtn = showCancelBtn
        self.present(basePopUp, animated: true, completion: nil)
    }
    
    func openBasePopUp(msg : String, titel : String, okText : String, cancelText : String, nummisra : String , num: Int , showCancelBtn: Bool) {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = /*self.storyboard?*/story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        basePopUp.text = msg
        basePopUp.titelText = titel
        basePopUp.nummisra = nummisra
        basePopUp.basePopUpActionDelegate = self
        basePopUp.num = num
        basePopUp.showCancelBtn = showCancelBtn
        basePopUp.okBtn.setTitle(okText, for: .normal)
        basePopUp.cancelBtn.setTitle(cancelText, for: .normal)
        self.present(basePopUp, animated: true, completion: nil)
    }
    
    func okAction(num: Int) {
        if self.whichNotificationShow == NotificationType.JOB_EXTENSION.rawValue {  //"הארכת עבודה"
            self.UpdateJobExtension(iJobExtensionId:num, iStatusExtension:jobExtension.OK.rawValue)
        }
       else if self.whichNotificationShow == NotificationType.JOB_ABANDONMENT_CLOSE_JOB.rawValue { // סופית הודעת נטישה לעובד //
            self.UpdateJobAbandonmentConfiremToNotification(iJobAbandonmentId:num, isWorker:true)
        }
        else if self.whichNotificationShow == NotificationType.JOB_ABANDONMENT.rawValue {//הודעת נטישה לעובד וקבלת תגובה //
            self.UpdateJobAbandonmentWorker(iJobAbandonmentId:num, bWorkerWantContinue:false)
        }
        else if self.whichNotificationShow == NotificationType.JOB_ABANDONMENT_ANSWER_MISUNDERSTANDING.rawValue || self.whichNotificationShow == NotificationType.JOB_ABANDONMENT_ANSWER_NOT_WANT_CONTINUE.rawValue { // הודעת נטישת עבודה למעסיק
            self.UpdateJobAbandonmentConfiremToNotification(iJobAbandonmentId:num, isWorker:false)
        }
        else if self.whichNotificationShow == NotificationType.JOB_TERMINATION_WORKER_WANT_CONTINUE.rawValue {//הודעה למעסיק המשך עבודה כן או לא
            jobTerminationEmployerWantContinueUpdate(iJobTerminationtId:num, employeeWantContinue:false)
        }
        
    }
    
    func cancelAction(num: Int){
        if self.whichNotificationShow == NotificationType.JOB_EXTENSION.rawValue {  //"הארכת עבודה"
             self.UpdateJobExtension(iJobExtensionId:num, iStatusExtension:jobExtension.NOT_OK.rawValue)
        }
        else if self.whichNotificationShow == NotificationType.JOB_ABANDONMENT.rawValue {//הודעת נטישה לעובד וקבלת תגובה //
            self.UpdateJobAbandonmentWorker(iJobAbandonmentId:num, bWorkerWantContinue:true)
        }
        else if self.whichNotificationShow == NotificationType.JOB_TERMINATION_WORKER_WANT_CONTINUE.rawValue {//הודעה למעסיק המשך עבודה כן או לא
            jobTerminationEmployerWantContinueUpdate(iJobTerminationtId:num, employeeWantContinue:true)
        }
    }
    
        // הארכת עבודה כן או לא //
    func UpdateJobExtension(iJobExtensionId:Int, iStatusExtension:Int) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobExtensionId"] = iJobExtensionId as AnyObject
        dic["iStatusExtension"] =  iStatusExtension as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                      Alert.sharedInstance.showAlert(mess: "נשלח בהצלחה")
                    if iStatusExtension ==  jobExtension.OK.rawValue  { //הארכה
                        //אם מסך נוכחות פתוח צריך לרענן אותו
                    }
                }else {
                    Alert.sharedInstance.showAlert(mess: "ישנה תקלה בשרת")
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateJobExtension")
    }
    
    
  // נטישת עבודה//
    func UpdateJobAbandonmentConfiremToNotification(iJobAbandonmentId:Int, isWorker:Bool) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobAbandonmentId"] = iJobAbandonmentId as AnyObject
        dic["bWorkerAgreeToClose"] =  isWorker as AnyObject
        dic["bEmploerAgreeToWorkerAnswer"] =  !isWorker as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    Alert.sharedInstance.showAlert(mess: "נשלח בהצלחה")
                }else {
                    Alert.sharedInstance.showAlert(mess: "ישנה תקלה בשרת")
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateJobAbandonmentConfiremToNotification")
    }
    
    //כן או לא נטישת עבודה//
    func UpdateJobAbandonmentWorker(iJobAbandonmentId:Int, bWorkerWantContinue:Bool) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobAbandonmentId"] = iJobAbandonmentId as AnyObject
        dic["bWorkerWantContinue"] =  bWorkerWantContinue as AnyObject
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if !bWorkerWantContinue{
                         self.openBasePopUp(msg :"העבודה הסתימה", titel : "הפסקת עבודה", nummisra : "", num:0, showCancelBtn: false)
                    }
                }else {
                    Alert.sharedInstance.showAlert(mess: "ישנה תקלה בשרת")
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateJobAbandonmentWorker")
    }
    
    
    //כן או לא מאפשר המשך עבודה//
    func jobTerminationEmployerWantContinueUpdate(iJobTerminationtId:Int, employeeWantContinue:Bool) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        dic["iJobTerminationtId"] = iJobTerminationtId as AnyObject
        dic["bEmployeeWantContinue"] =  employeeWantContinue as AnyObject
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                  
                }else {
                    Alert.sharedInstance.showAlert(mess: "ישנה תקלה בשרת")
                }
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "jobTerminationEmployerWantContinueUpdate")
    }
    
}
;
extension EmployeeModelViewController: ReceiveRemoteNotificationDelegate   {
    
    func onReceiveRemoteNorificarion(userInfo: [AnyHashable : Any]) {
        print(userInfo as! [String:AnyObject])
        let iNotiType = Int((userInfo["iNotificationType"] as! NSString).intValue)
        let iUserID = Int((userInfo["iUserId"] as! NSString).intValue)
        
        self.whichNotificationShow = iNotiType
        
        if iNotiType == NotificationType.WORKER_JOB_UPDATE.rawValue { //רפרוש סטטוס משרה לעובד
            if iUserID == Global.sharedInstance.user.iUserId {
                if Global.sharedInstance.notificationRefreshDelegate == nil{
                    getWorkerOfferedJobs()
                }
                Global.sharedInstance.notificationRefreshDelegate?.onNorificarionRefresh(userInfo: userInfo)
            }
        }
        else if iNotiType == 23{ //רפרוש סטטוס משרה לעובד
            getWorkerOfferedJobs()
        }
        else if iNotiType == NotificationType.EMPLOYER_JOB_UPDATE.rawValue { //רפרוש סטטוס משרה למעסיק
            if iUserID == Global.sharedInstance.user.iUserId {
                GetLookingForYouWorkers()
            }
        }
        else if iNotiType == NotificationType.JOB_TERMINATION_WORKER_STOP_JOB.rawValue { //הודעת הפסקת עבודה
            if iUserID == Global.sharedInstance.user.iUserId {
                 self.openBasePopUp(msg :"העבודה הסתימה", titel : "", nummisra : "", num:0, showCancelBtn: false)
            }
        }
        else if iNotiType == NotificationType.JOB_TERMINATION.rawValue { //הודעת סיום עבודה
            if iUserID == Global.sharedInstance.user.iUserId {
                let story = UIStoryboard(name: "Main", bundle: nil)
                let basePopUp = story.instantiateViewController(withIdentifier: "DialogTerminationWorkerViewController") as! DialogTerminationWorkerViewController
                basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom

                print(userInfo as! [String:AnyObject])
                
                if userInfo["iEmployerJobId"] != nil && userInfo["iWorkerOfferedJobId"] != nil{
                    basePopUp.numMisra = String((userInfo["iEmployerJobId"] as! NSString).intValue)+"/"+String((userInfo["iWorkerOfferedJobId"] as! NSString).intValue)
                    basePopUp.employerJobId = Int((userInfo["iEmployerJobId"] as! NSString).intValue)
                    basePopUp.workerOfferedJobId = Int((userInfo["iWorkerOfferedJobId"] as! NSString).intValue)
                }
                basePopUp.iJobTerminationtId = Int((userInfo["iJobTerminationtId"] as! NSString).intValue)
                basePopUp.dialogTerminationWorkerDelegate = self
                self.present(basePopUp, animated: true, completion: nil)
            }
        }
        else if iNotiType == NotificationType.JOB_EXTENSION.rawValue { //הודעת הארכת עבודה  //
            if iUserID == Global.sharedInstance.user.iUserId {
                let ss =  "המעסיק מעוניין להאריך את עבודתך בתאריך"
                    + " " + String((userInfo["dDate"] as! NSString).intValue)  + " " + "משעה"
                    + " " + String((userInfo["tFromHoure"] as! NSString).intValue)  + " " + "עד שעה"
                    + " " + String((userInfo["tToHoure"] as! NSString).intValue)  + " " + "האם הינך מעוניין?"
                
                self.openBasePopUp(msg :ss , titel : "הארכת עבודה", nummisra : String((userInfo["iEmployerJobId"] as! NSString).intValue)+"/"+String((userInfo["iWorkerOfferedJobId"] as! NSString).intValue) , num: Int((userInfo["iJobExtensionId"] as! NSString).intValue), showCancelBtn: true)
            }
        }
        else if iNotiType == NotificationType.JOB_ABANDONMENT_CLOSE_JOB.rawValue { // סופית הודעת נטישה לעובד //
            if iUserID == Global.sharedInstance.user.iUserId {
                let ss =  "המעסיק שלך דיווח שנטשת את העבודה, ולא קיבלנו ממך תגובה כלשהי,לכן אנו רואים בכך התפטרות מהעבודה על דרך של נטישת מקום העבודה"
                
                self.openBasePopUp(msg :ss , titel : "נטישת עבודה", nummisra : String((userInfo["iEmployerJobId"] as! NSString).intValue)+"/"+String((userInfo["iWorkerOfferedJobId"] as! NSString).intValue) , num: Int((userInfo["iJobAbandonmentId"] as! NSString).intValue), showCancelBtn: false)
            }
        }
        else if iNotiType == NotificationType.JOB_ABANDONMENT.rawValue {//הודעת נטישה לעובד וקבלת תגובה //
            if iUserID == Global.sharedInstance.user.iUserId {
                let ss =  "באופן חריג המעסיק שלך דיווח שנטשת את העבודה, במידה ומדובר באי הבנה, עמך הסליחה. ואם כן אינך מתבקש לחזור מידית למקום העבודה. אם אתה לא מעונין לחזור עליך לאשר את התפטרותך מהעבודה."
                
                self.openBasePopUp(msg :ss , titel : "נטישת עבודה", okText : "מעוניין להתפטר", cancelText : "אי הבנה, אני כאן" , nummisra : String((userInfo["iEmployerJobId"] as! NSString).intValue)+"/"+String((userInfo["iWorkerOfferedJobId"] as! NSString).intValue) , num: Int((userInfo["iJobAbandonmentId"] as! NSString).intValue), showCancelBtn: true)
            }
        }
            
        else if iNotiType == NotificationType.JOB_ABANDONMENT_ANSWER_NOT_WANT_CONTINUE.rawValue {// הודעת נטישת עבודה למעסיק
             if iUserID == Global.sharedInstance.user.iUserId {
                self.openBasePopUp(msg :"העובד החליט לנטוש את העבודה" , titel : "נטישת עבודה",  nummisra : String((userInfo["iEmployerJobId"] as! NSString).intValue)+"/"+String((userInfo["iWorkerOfferedJobId"] as! NSString).intValue) , num: Int((userInfo["iJobAbandonmentId"] as! NSString).intValue), showCancelBtn: false)
            }
        }
        else if iNotiType == NotificationType.JOB_ABANDONMENT_ANSWER_MISUNDERSTANDING.rawValue {// הודעת נטישת עבודה למעסיק
            if iUserID == Global.sharedInstance.user.iUserId {
                self.openBasePopUp(msg :"העובד טוען על אי הבנה. נא ליצור עימו קשר" , titel : "נטישת עבודה",  nummisra : String((userInfo["iEmployerJobId"] as! NSString).intValue)+"/"+String((userInfo["iWorkerOfferedJobId"] as! NSString).intValue) , num: Int((userInfo["iJobAbandonmentId"] as! NSString).intValue), showCancelBtn: false)
            }
        }
            
        else if iNotiType == NotificationType.JOB_TERMINATION_WORKER_WANT_CONTINUE.rawValue {//הודעה למעסיק המשך עבודה כן או לא
            if iUserID == Global.sharedInstance.user.iUserId {
                let ss = String(userInfo["alert"] as! NSString)

                self.openBasePopUp(msg :ss , titel : "הפסקת עבודה", okText : "לא מאפשר", cancelText : "מוכן לאפשר המשך עבודה" , nummisra : String((userInfo["iEmployerJobId"] as! NSString).intValue)+"/"+String((userInfo["iWorkerOfferedJobId"] as! NSString).intValue) , num: Int((userInfo["iJobTerminationtId"] as! NSString).intValue), showCancelBtn: true)
            }
        }
        
        else if iNotiType == NotificationType.TURN_TO_NEXT_CANDIDATE.rawValue {// "פניה למועמד הבא"
            if iUserID == Global.sharedInstance.user.iUserId {
                let ss = String(userInfo["alert"] as! NSString)
                self.openBasePopUp(msg :ss , titel : "פניה למועמד הבא", nummisra : String((userInfo["iEmployerJobId"] as! NSString).intValue), num: 0, showCancelBtn: false)
            }
        }
        
    }
    
    
}
