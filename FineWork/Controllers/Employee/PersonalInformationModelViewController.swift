////
//  PersonalInformationModelViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 13/12/2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
//model personal
class PersonalInformationModelViewController: UIViewController,UIImagePickerControllerDelegate,
UINavigationControllerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate ,EmployeeDetails_SupperView_Delegate, ChangeSubViewProfileDelegate ,UIScrollViewDelegate, getTextFieldDelegate, BasePopUpActionsDelegate, UploadFilesDelegate, OpenAnotherPageDelegate {
    
 //MARK:- Outlet
    
    //MARK:-- Buttons
    
    //MARK:--- Menu Buttons

    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var btnEmployeeDetaisCard: UIButton!
    
    @IBOutlet var btnEmployeeSalaryDetails: UIButton!
    @IBOutlet var btnEmployeeVisibleProfile: UIButton!
    
    @IBOutlet var btnEmployeeContract: UIButton!
    @IBOutlet var btnEmployeeRegisterDetails: UIButton!
    @IBOutlet var btnUserProphilImgChange: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
      //MARK:-- Images
    
    @IBOutlet var imgViewUserProphilImage: UIImageView!
    
    //MARK:-- TextFields
    //MARK:-- Labels
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblUserDetails: UILabel!
    
     //MARK:--Views
    
    @IBOutlet var viewDivider: UIView!
    @IBOutlet var viewMenu: UIView!
    @IBOutlet var viewUnderIMGViewProphilUserName: UIView!
    
    //MARK:-- Actions
   
//    @IBAction func btnUserProphilImgChangeAction(_ sender: Any) {
//        ChangePicture()
//    }
    
    @IBAction func btnChangeViewAction(_ sender: UIButton) {
        if self.view.tag == 0{
//            visibleProfile.view.removeFromSuperview()
//            connectionDetails.view.removeFromSuperview()
//            contractWorker.view.removeFromSuperview()
//            salary.view.removeFromSuperview()
//            unregisterKeyboardNotifications()
//            switch sender.tag {
//            case 0:
//                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
//                GlobalEmployeeDetails.sharedInstance.replaceUser()
//                get101Form()
//                break
//            case 1:
//                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                getSalaryForm()
//                break
//            case 2:
//                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
//                getVisibleProfileForm()
//                break
//            case 3:
//                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
//                getContractWorker()
//                break
//            case 4:
//                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
//                getConnectedDetailsForm()
//                break
//            default:
//                break
//            }
            changeSubViewProfile(tagNewView: sender.tag)
        }else{
            containerDelegate.changeSubViewProfile(tagNewView: sender.tag)
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if self.view.tag == 1{
            containerDelegate.tapBack()
        }else{
            if Global.sharedInstance.bWereThereAnyChanges {
                isTabBack = true
                openBasePopUp()
            } else {
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
    //MARK:- Variabels
    var strImage = ""
    var flagChangePictiure = false
    var imagePicker = UIImagePickerController()
    
    var visibleProfile : VisibleProfileViewController! = VisibleProfileViewController()
    var connectionDetails : ConnectionDetailsViewController! = ConnectionDetailsViewController()
    var contractWorker : ContractWorkerViewController! = ContractWorkerViewController()
    var salary : SalaryViewController! = SalaryViewController()
    
    var containerDelegate:ChangeSubViewProfileDelegate! = nil
    
    var subviewTag = 0
    
    //MARK:- Function
    //MARK:-- initial Functions
    
    var viewCon101 : EmployeeDetailsViewController! = EmployeeDetailsViewController()
    var subView = UIView()
    var HeightView:CGFloat = CGFloat()
    
    var txtSelected = UITextField()
    var isTabBack = false
 
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.view.tag == 0{
            self.navigationController?.setNavigationBarHidden(true,animated: false)
            
            Global.sharedInstance.personalInformationModelHeight = self.view.frame.size.height
            
            subView.frame = CGRect(x: view.frame.origin.x,y: subView.frame.origin.y + subView.frame.height ,width: view.frame.width,height:viewMenu.frame.height)
            if GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController == nil{
                viewCon101 = self.storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailsViewController") as! EmployeeDetailsViewController
            }else{
                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.setUserData()
                viewCon101 = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
                viewCon101.errorArray = ["nvFirstName":false,"nvLastName":false,"nvIdentityNumber":false,"dBirthDate":false,"iCityId":false,"nvStreet":false,"nvHouseNumber":false,"nvPhone":false,"lWorkerChilds":false,"nvSpouseFirstName":false,"nvSpouseLastName":false,"dSpouseBirthDate":false,"nvSpouseIdentityNumber":false,"bHaveOtherIncome":false,"lWorkerTaxCoordinations":false,"mTillAmount":false,"fDeductedPercent":false,"fBeyondTaxDeducted":false,"fileObj":false,"reason":false,"2":false,"3":false,"4":false,"5":false,"6":false,"7":false,"8":false,"9":false,"10":false,"11":false,"12":false,"13":false,"14":false]
            }
            // init ConnectionDetailsViewController
            connectionDetails = self.storyboard?.instantiateViewController(withIdentifier: "ConnectionDetailsViewController") as! ConnectionDetailsViewController
            connectionDetails.getTextFieldDelegate = self
            connectionDetails.view.frame = CGRect(x: 0,y: subView.frame.height,width: view.frame.width,height:connectionDetails.view.frame.height)
            
            // init VisibleProfileViewController
            visibleProfile = self.storyboard?.instantiateViewController(withIdentifier: "VisibleProfileViewController") as! VisibleProfileViewController
            visibleProfile.view.frame = CGRect(x: 0,y: 0,width: view.frame.width,height:visibleProfile.view.frame.height)
            visibleProfile.cellHeader.changeSubViewProfileDelegate = self
            visibleProfile.fromContainerDelegate = self
            
            // init ContractWorkerViewController
            contractWorker = self.storyboard?.instantiateViewController(withIdentifier: "ContractWorkerViewController") as! ContractWorkerViewController
            contractWorker.view.frame = CGRect(x: 0, y: subView.frame.height, width: view.frame.width, height: contractWorker.view.frame.height)
            
            // init SalaryViewController
            salary = self.storyboard?.instantiateViewController(withIdentifier: "SalaryViewController") as! SalaryViewController
            salary.view.frame = CGRect(x: 0,y: 0,width: view.frame.width,height:salary.view.frame.height)
            salary.cellHeader.changeSubViewProfileDelegate = self

            // 101 form
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
            viewCon101.employeeDetails_SupperView_Delegate = self
            viewCon101.fromContainerDelegate = self
            //---
            viewCon101.uploadFilesDelegate = self
            self.uploadFilesDelegate = viewCon101
            //---
            //        view1.opendat
            viewCon101.view.frame = CGRect(x: 0,y: 0,width: view.frame.width,height:viewCon101.view.frame.height)
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController = viewCon101
            //        self.view.addSubview(viewCon101.view)
//                get101Form()
            changeSubViewProfile(tagNewView: subviewTag)

            
            
            // 1
            let layer = CALayer()
            layer.frame = imgViewUserProphilImage.bounds
            
            // 2
            layer.contents = imgViewUserProphilImage.image
            layer.contentsGravity = kCAGravityCenter
            
            // 3
            layer.magnificationFilter = kCAFilterLinear
            layer.isGeometryFlipped = false
            
            // 4
            layer.backgroundColor = UIColor.clear.cgColor
            layer.opacity = 1.0
            layer.isHidden = false
            layer.masksToBounds = false
            
            // 5
            layer.cornerRadius = imgViewUserProphilImage.frame.height/2
            layer.borderWidth = 7.0
            layer.borderColor = UIColor.gray.cgColor
            
            // 6
            layer.shadowOpacity = 0.75
            layer.shadowOffset = CGSize(width: 3, height: 2)
            layer.shadowRadius = 3.0
            layer.shadowOffset = CGSize.zero
            imgViewUserProphilImage.layer.addSublayer(layer)
            
            imgViewUserProphilImage.layer.borderColor = UIColor.gray.cgColor
            imgViewUserProphilImage.layer.borderWidth = 7
            
            
            //
            imgViewUserProphilImage.layer.cornerRadius = imgViewUserProphilImage.frame.height/2
            imgViewUserProphilImage.layer.masksToBounds = true
            let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(self.imageTapped(img:)))
            imgViewUserProphilImage.isUserInteractionEnabled = true
            imgViewUserProphilImage.addGestureRecognizer(tapGestureRecognizer1)
            
            let layer2 = layer
            layer2.frame = viewUnderIMGViewProphilUserName.bounds
            layer2.contents = UIColor.clear
            layer2.cornerRadius = viewUnderIMGViewProphilUserName.frame.height/2
            layer2.backgroundColor = UIColor.clear.cgColor
            layer2.borderColor = UIColor.gray.cgColor
            layer.borderWidth = 0.25
            viewUnderIMGViewProphilUserName.layer.addSublayer(layer2)
            viewUnderIMGViewProphilUserName.layer.cornerRadius = viewUnderIMGViewProphilUserName.frame.height/2
            viewUnderIMGViewProphilUserName.backgroundColor = UIColor.clear
            getUserImage()
        }
        
        setDesign()
        
        hideKeyboardWhenTappedAround()

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if let image : UIImage = Global.sharedInstance.user.profileImage {
            if imgViewUserProphilImage != nil {
                imgViewUserProphilImage.image = image
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.scrollView.contentOffset = CGPoint.zero
        lblUserName.text = Global.sharedInstance.user.nvUserName
        
//        print("A-log getUserImage()")
        
        if self.view.tag == 1{
            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
            switch subviewTag {
            case 0:
                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                break
            case 1:
                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                break
            case 2:
                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                break
            case 3:
                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                break
            case 4:
                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                break
            default:
                break
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        if self.scrollView != nil{
            self.scrollView.contentOffset = CGPoint.zero
        }
        
        if let image : UIImage = Global.sharedInstance.user.profileImage {
            if imgViewUserProphilImage != nil {
                imgViewUserProphilImage.image = image
            }
        }
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func getUserImage(){
        api.sharedInstance.GetImage(params: ["iUserId":(Global.sharedInstance.user.iUserId as AnyObject?)!], success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
                if let imagString = dicObj["Result"]?["nvFileURL"] as? String{
                    var stringPath = api.sharedInstance.buldUrlFile(fileName: imagString)
                    //                    let aString = "This is my string"
                    stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                    if let imageUrl = URL(string:stringPath){
                        self.downloadImage(url: imageUrl)
                    }
                }
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    self.imgViewUserProphilImage.contentMode = .scaleToFill
                    self.imgViewUserProphilImage.image = image
                    Global.sharedInstance.user.profileImage = image
                }
                
                if Global.sharedInstance.user.picture.nvFile.isEmpty {
                    Global.sharedInstance.user.picture.nvFile = Global.sharedInstance.convertImageToBase64(image: image)
                }
            }
        }
        
    }
    
    //MARK: ----camera func
//    func ChangePicture()
//    {
//        openAlert()
//      
//    }
//    func openAlert()
//    {
//        Alert.sharedInstance.AlertopenPictures(controller: self, tag: 1)
//    }
//    
//    func openAction(img: UIImageView)
//    {
//        Alert.sharedInstance.AlertopenPictures(controller: self,tag: 1)
//    }
//    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool
//    {
//        self.dismissKeyboard()
//        return false
//    }
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
//    {
//        imgViewUserProphilImage.image = image
//        Global.sharedInstance.user.profileImage = image
//
//        self.dismiss(animated: true, completion: nil);
//    }
    
    
    func imageTapped(img: AnyObject)
    {
//        var stringURL: String = "ibooks://"
//        var url = URL(string: stringURL)
//        UIApplication.shared.openURL(url!)
        
        isUserImage = true
        showCameraActionSheet()
        
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
//            print("Button capture")
//            
//            
//            imagePicker.delegate = self
//            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
//            imagePicker.allowsEditing = false
//            
//            self.present(imagePicker, animated: true, completion: nil)
//        }

    }
//    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL{
//            if let imageName = imageUrl.lastPathComponent{
//                if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
//                    var image = imageOriginal
//                    if picker.allowsEditing {
//                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
//                            image = imageEdit
//                        }
//                    }
//                    let imageString = Global.sharedInstance.setImageToString(image: image)
//                    if imageString != ""{
//                        imgViewUserProphilImage.contentMode = .scaleToFill
//                        imgViewUserProphilImage.image = image
//                        Global.sharedInstance.user.profileImage = image
//                        setImageProfileInServer(imageName: imageName, image: imageString)
//                        picker.dismiss(animated: true, completion: nil)
//                    }else{
//                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
//                        picker.dismiss(animated: true, completion: nil)
//                    }
//                    
////                    image = Global.sharedInstance.resizeImage(image: image, newWidth: 100)
////                    imgViewUserProphilImage.contentMode = .scaleToFill
////                    imgViewUserProphilImage.image = image
////                    setImageProfileInServer(imageName: imageName, image: image)
//                }
//            }
//        }else{
//            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
//                var image = imageOriginal
//                if picker.allowsEditing {
//                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
//                        image = imageEdit
//                    }
//                }
//                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//                let imageName = "img.JPEG"
//                let imageString = Global.sharedInstance.setImageToString(image: image)
//                if imageString != ""{
//                    imgViewUserProphilImage.contentMode = .scaleToFill
//                    imgViewUserProphilImage.image = image
//                     Global.sharedInstance.user.profileImage = image
//                    setImageProfileInServer(imageName: imageName, image: imageString)
//                    picker.dismiss(animated: true, completion: nil)
//                    
//                }else{
//                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
//                    picker.dismiss(animated: true, completion: nil)
//                }
//            }
//        }
//
////        picker.dismiss(animated: true, completion: nil)
//        
//        
////        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
////            imgViewUserProphilImage.contentMode = .scaleToFill
////            imgViewUserProphilImage.image = pickedImage
////        }
////        
////        picker.dismiss(animated: true, completion: nil)
//    }
    
    func setImageProfileInServer(imageName:String,image:String){
        let imageObj = FileObj()
        imageObj.nvFile = image
        imageObj.nvFileName = imageName
        var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicToServer["image"] = imageObj.getImageDic() as AnyObject?
        dicToServer["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dicToServer["iUserType"] = Global.sharedInstance.user.iUserType.rawValue as AnyObject?
        api.sharedInstance.UpdateImage(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
            }else{
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })
        
    }
    
  //MARK:-- SETTERS Functions
    func configureButton(btn:UIButton){
        let f:CGFloat = btn.frame.width / 2
       btn.layer.cornerRadius = f
    }
    
    func setDesign(){
    setBackgroundcolor()
    setBorders()
    setTextcolor()
    setCornerRadius()
    self.setIconFont()
    self.setLocalizableString()
        setImages()
    }
    
    func setBackgroundcolor()
    {
       // if self.view.tag == 0{
          //  self.viewMenu.backgroundColor = ControlsDesign.sharedInstance.colorDarkPurple
      //  }else{
      //      self.view.backgroundColor = ControlsDesign.sharedInstance.colorDarkPurple
//            switch subviewTag {
//            case 0:
//                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                break
//            case 1:
//                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                break
//            case 2:
//                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                break
//            case 3:
//                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                break
//            case 4:
//                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//                break
//            default:
//                break
//            }
    //    }
//        setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//        setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//        setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//        setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//        setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
       // setBackgroundcolorToBtn(btn: btnBack, color: ControlsDesign.sharedInstance.colorDarkPurple)

//        self.btnLogin.backgroundColor = ControlsDesign.sharedInstance.colorPurple
    }
    func setBackgroundcolorToBtn(btn:UIButton, color:UIColor){
       btn.backgroundColor = color
    }
    
    func setTextcolorToBtn(btn:UIButton, color:UIColor)
    {
        btn.titleLabel?.textColor = color
        btn.tintColor = color
    }
    func setBorderToBtn(btn:UIButton, color:UIColor , width:CGFloat)
    {
        btn.layer.borderColor = color.cgColor
         btn.layer.borderWidth = width
    }

    func setFontToButton(btn:UIButton, name:String, size:CGFloat)
 {
     btn.titleLabel!.font =  UIFont(name: name, size: size)
        //btn.titleLabel?.backgroundColor = UIColor.clear
    }
    
    func setImages(){
            let backgroundButtonImage = UIImage(named: "money")
            btnEmployeeSalaryDetails.setImage(backgroundButtonImage, for: .normal)
    }
    
    func setBorders(){
        setBorderToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.white , width: 1.0)
        setBorderToBtn(btn: btnEmployeeDetaisCard, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeContract, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.white, width: 1.0)
    }
    
    func setTextcolor(){
        lblUserName.textColor = UIColor.white
        lblUserDetails.textColor = UIColor.white
        setTextcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeContract, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.white)
    }
    
    func setCornerRadius(){
        self.configureButton(btn: btnEmployeeDetaisCard)
        self.configureButton(btn: btnEmployeeVisibleProfile)
        self.configureButton(btn: btnEmployeeContract)
        self.configureButton(btn: btnEmployeeSalaryDetails)
        self.configureButton(btn: btnEmployeeRegisterDetails)
    }
    
    func setIconFont(){
        btnEmployeeDetaisCard.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_DetaisCard, for: .normal)//facebook
        btnEmployeeVisibleProfile.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)//-profile
        btnEmployeeContract.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_Contract, for: .normal)//people-2
        btnEmployeeRegisterDetails.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_RegisterDetails, for: .normal)//interface-5
    }
    
    func setLocalizableString()
    {
//        //SET TEXT TO BUTTONS
//        btnLogin.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN, comment: ""), for: .normal)
//        btnLoginWithFacebook.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_WITH_FACEBOOK, comment: ""), for: .normal)
//        btnLoginWithGoogle.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_WITH_GOOGLE, comment: ""), for: .normal)
//        btnRegister.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_REGISTER, comment: ""), for: .normal)
//        
//        //SET TEXT TO LABELS
//        lblForgotPassword.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_FORGOT_PASSWORD, comment: "")
//        lblIsNotYetRegister.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_IS_NOT_YET_REGISTER, comment: "")
//        lblHeaderLogin.text = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_TITLE, comment: "")
//        //SET TEXT TO txtPlaceHolder
//        txtPassword.placeholder = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_PASSWORD, comment: "")
//        txtMail.placeholder = NSLocalizedString(LocalizableStringStatic.sharedInstance.LOGIN_MAIL, comment: "")
    }
    
    //MARK:--  Other Delegate
    func remove101FormFromSuperView(tagNewView:Int){
        
        changeSubViewProfile(tagNewView: tagNewView)
//        viewCon101.view.removeFromSuperview()
//        visibleProfile.view.removeFromSuperview()
//        connectionDetails.view.removeFromSuperview()
//        contractWorker.view.removeFromSuperview()
//        salary.view.removeFromSuperview()
//        unregisterKeyboardNotifications()
//        switch tagNewView {
//        case 1:
//            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//            getSalaryForm()
//            break
//        case 2:
//            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
//            getVisibleProfileForm()
//            break
//        case 3:
//            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
//            getContractWorker()
//            break
//        case 4:
//            setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
//            setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
//            setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
//            getConnectedDetailsForm()
//            break
//        default:
//            break
//        }
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func tapBack(){
        if Global.sharedInstance.bWereThereAnyChanges {
            isTabBack = true
            openBasePopUp()
        } else {
            let _ = self.navigationController?.popViewController(animated: true)
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    func setImageProfile(image:UIImage){
        imgViewUserProphilImage.image = image
        Global.sharedInstance.user.profileImage = image
    }
    
    func openDatePicker1(datePickerPopUpViewController:DatePickerPopUpViewController){
        self.present(datePickerPopUpViewController, animated: true, completion: nil)
    }
    
    //MARK: - ChangeSubViewProfileDelegate
    
    func changeSubViewProfile(tagNewView: Int) {
        if Global.sharedInstance.bWereThereAnyChanges {
            //            tabSenderBtn = sender
            subviewTag = tagNewView
            openBasePopUp()
        } else {
            scrollView.contentOffset = .zero
            connectionDetails.view.removeFromSuperview()
            visibleProfile.view.removeFromSuperview()
            contractWorker.view.removeFromSuperview()
            salary.view.removeFromSuperview()
            viewCon101.view.removeFromSuperview()
            unregisterKeyboardNotifications()
            switch tagNewView {
            case 0:
                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
                GlobalEmployeeDetails.sharedInstance.replaceUser()
                get101Form()
                if self.view.tag == 1 {
                    self.view.addSubview(viewCon101.view)
                }
                break
            case 1:
                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                getSalaryForm()
                break
            case 2:
                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
                getVisibleProfileForm()
                break
            case 3:
                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
                getContractWorker()
                break
            case 4:
                setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
                setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
                setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
                getConnectedDetailsForm()
                break
            default:
                break
            }
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    

    
    func get101Form(){
        GlobalEmployeeDetails.sharedInstance.reload101Form()
        var dicToServer = Dictionary<String, AnyObject>()
        dicToServer["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.GetWorker101Form(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            
            var dicWorker101 = responseObject as! [String:AnyObject]
            if dicWorker101["Error"]?["iErrorCode"] as! Int ==  0
            {
                
                //print(responseObject as! [String:AnyObject])
                //print(responseObject as Any)
                print((responseObject as! [String:AnyObject])["Result"]!)
                
                let worker101:Worker101Form = Worker101Form()
                
                if !((responseObject as! [String:AnyObject])["Result"]! is NSNull)
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form = worker101.dicToWorker101Form(dic: (responseObject as! [String:AnyObject])["Result"]! as! Dictionary<String, AnyObject>)
                    
                    
                    //לאחר קבלת טופס 101 מהשרת
                    //שינוי מספר הסלים וגובה הגלילה בהתאם לטופס.
                    GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame = Float((self.viewCon101?.view.frame.size.height)!)
                    GlobalEmployeeDetails.sharedInstance.set_dicHeightOfCellsOpened()
                    //הוספת סלים לילדים
                    
                    GlobalEmployeeDetails.sharedInstance.numRowsForSection[0] += GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count
                    
                    //הוספת סלים של ילדים
                    GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (285/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count))
                    
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2//האם ההורה לא נשוי
                    {
                        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 21
                        {
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (85/750))//אם הסטטוס הוא פרוד/ה מוסיפים את הגובה של הכפתור - ׳צירוף אישור׳.
                        }
                        
                        //הוספת סל הוסף ילדים
                        //בדיקה האם להוסיף גובה עבור הכפתור ׳צרף אישור זכאות׳
                        var showWithButton = false
                        
                        for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
                        {
                            if item.bWithMe == true
                            {
                                showWithButton = true
                                break
                            }
                        }
                        
                        if showWithButton == true
                        {
                            //הוספת גובה לכפתור צרף זכאות
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (60/750))
                        }
                    }
                    else//נשוי
                    {
                        //הוספת סל בן זוג אם נשוי
                        GlobalEmployeeDetails.sharedInstance.numRowsForSection[0] += 1
                        
                        if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType == 19 || GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.iSpouseIncomSourceType == 20
                        {
                            GlobalEmployeeDetails.sharedInstance.incomePartnerSelected = true
                            
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (480/750))
                        }
                        else
                        {
                            GlobalEmployeeDetails.sharedInstance.incomePartnerSelected = false
                            
                            GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (320/750))
                        }
                    }
                    
                    //הוספת סלים אם יש לי הכנסות נוספות
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bHaveOtherIncome == true
                    {
                        GlobalEmployeeDetails.sharedInstance.numRowsForSection[1] = 2
                        GlobalEmployeeDetails.sharedInstance.numSectionsInTbl = 6
                        
                        GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (250/750)) + CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (70/750) * 2)
                    }
                    //הוספת סליםGlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count אם בחר- אני מבקש תיאום מס...
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == true
                    {
                        GlobalEmployeeDetails.sharedInstance.numRowsForSection[3] += GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count
                        
                        GlobalEmployeeDetails.sharedInstance.howManyToScroll2 += CGFloat(CGFloat(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame) * (320/750) * CGFloat(GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations.count))
                    }
                    
                    for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons
                    {
                        GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.append(item.iTaxRelifeReasonId
                            
                        )
                    }
                    
                    //המערך של הסיבות בנוי מאובייקטים ריקים
                    //מהשרת חוזר רק הסיבות שנבחרו, לכן מוסיפים את הסיבות שלא נבחרו כאובייקטים ריקים בהתאמה.
                    GlobalEmployeeDetails.sharedInstance.setArrReasonFull()
                    
//                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit == true
//                    {
                        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2
                        {
                            GlobalEmployeeDetails.sharedInstance.CheckForMarried()
                        }
                        else
                        {
                            GlobalEmployeeDetails.sharedInstance.CheckForNotMarried()
                        }
//                    }
                    
                    GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected  = GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit
                    
                    
                    if !GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit {
                        GlobalEmployeeDetails.sharedInstance.howManyToScroll = 0
                        GlobalEmployeeDetails.sharedInstance.arrReasonsBool = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
                        GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                        GlobalEmployeeDetails.sharedInstance.numRowsForSection[2] = 1
                        
//                        GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected = false
                        GlobalEmployeeDetails.sharedInstance.selectRowsForSection[2] = false
                        GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit = false

//                        selectRowsForSection
//                                    GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
                    }
                    //GlobalEmployeeDetails.sharedInstance.isFromLogin = true
                    
                }else{
                    
                    //לאחר קבלת טופס 101 מהשרת
                    //שינוי מספר הסלים וגובה הגלילה בהתאם לטופס.
                    GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame = Float((self.viewCon101?.view.frame.size.height)!)
                    GlobalEmployeeDetails.sharedInstance.set_dicHeightOfCellsOpened()
                    GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone = Global.sharedInstance.user.nvMobile
                }
                //                                self.navigationController?.pushViewController(self.viewCon101!, animated: true)
                //                                self.navigationController?.pushViewController(self.viewCon!, animated: true)
//                GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone = Global.sharedInstance.user.nvMobile
                //MARK: כאן להוסיף את ה טופס!!!!
                
               
                
               self.viewCon101.extTbl.reloadData()
               self.view.addSubview(self.viewCon101.view)
                
                self.viewCon101.uploadFilesDelegate = self
                self.uploadFilesDelegate = self.viewCon101
//                self.navigationController?.pushViewController(self.employeeModelViewController!, animated: true)
                
                
            }
            else if dicWorker101["Error"]?["iErrorCode"] as! Int ==  2
            {
                self.showAlert(sendMessage: "לא מולא טופס 101")
            }
            else
            {
                self.showAlert(sendMessage: "error")
            }
            
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
            
            self.showAlert(sendMessage: "error")
            // self.navigationController?.pushViewController(self.viewCon101!, animated: true)
        })
    }

    //MARK:-- local Functions
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(sendMessage: String)
    {
        let alert : UIAlertController = UIAlertController(title: "", message: sendMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction:UIAlertAction = UIAlertAction(title: NSLocalizedString((LocalizableStringStatic.sharedInstance.OK_ALERT), comment: ""), style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
            //self.dismiss()
        })
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    

    //MARK: screen of worker personal details
    func getVisibleProfileForm() {
        self.view.addSubview(visibleProfile.view)
        visibleProfile.uploadFilesDelegateParent = self
        visibleProfile.openAnotherPageDelegate = self
        self.uploadFilesDelegate = visibleProfile
//        visibleProfile.cellHeader.uploadFilesDelegate = self
//        self.uploadFilesDelegate = visibleProfile.cellHeader
//        
//        visibleProfile.cellVisibleProfileEnd.uploadFileDelegate = self
//        self.uploadFilesDelegate = visibleProfile.cellVisibleProfileEnd
        visibleProfile.reloadData()
    }
    
    func getSalaryForm(){
        self.view.addSubview(salary.view)
        salary.cellHeader.uploadFilesDelegate = self
        self.uploadFilesDelegate = salary.cellHeader
        salary.reloadData()
        salary.getWorker()
    }
    
    func getContractWorker() {
        contractWorker.parentVC = self
        self.scrollView.addSubview(contractWorker.view)
        
        if scrollView != nil {
            scrollView.contentSize = CGSize(width: scrollView.frame.width, height: 335 + (contractWorker.view.frame.height * 0.75))
            self.scrollView.contentOffset = CGPoint.zero
            scrollView.delegate = self
        }
    }
    
    func getConnectedDetailsForm() {
        self.scrollView.addSubview(connectionDetails.view)
        registerKeyboardNotifications()
        
        if scrollView != nil {
            scrollView.isScrollEnabled = true
            scrollView.contentSize = CGSize(width: scrollView.frame.width, height: 335 + (connectionDetails.view.frame.height * 0.4))
        }
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "101FormSegue" {
            let connectContainerViewController = segue.destination as! EmployeeDetailsViewController
            containerDelegate = connectContainerViewController
//            containerViewController = connectContainerViewController
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    //MARK: - keyBoard notifications
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification)
    {
        if self.scrollView.contentOffset.y <= 0 {
            //Need to calculate keyboard exact size due to Apple suggestions
            self.scrollView.isScrollEnabled = true
            let info : NSDictionary = notification.userInfo! as NSDictionary
            let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
            let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
            
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            
            var aRect : CGRect = self.view.frame
            aRect.size.height -= keyboardSize!.height
            
            //        if txtSelected.tag == 1 {  // text field on a view
            
            var bRect : CGRect = txtSelected.frame
            
            bRect.origin.y += txtSelected.tag == 1 ? ((txtSelected.superview?.frame.origin.y)! + 335) : 335
            if (!aRect.contains(bRect)) {
                bRect.origin.y = bRect.origin.y - aRect.height  + bRect.height
                bRect.origin.x = 0
                self.scrollView.setContentOffset(bRect.origin, animated: true)
            }
            
            //        } else {
            //
            //            if (!aRect.contains(txtSelected.frame.origin))
            //            {
            //                self.scrollView.scrollRectToVisible(txtSelected.frame, animated: true)
            //            }
            //        }
        }
        
    }
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func getTextField(textField: UITextField, originY: CGFloat) {
        txtSelected = textField
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < CGPoint.zero.y {
            scrollView.contentOffset = CGPoint.zero
        }
    }
    
//    func dismissKeyboard() {
//        self.view.endEditing(true)
//        //characteristicsTxt.resignFirstResponder()
//        //studiesTxt.resignFirstResponder()
//        //jobExperienceTxt.resignFirstResponder()
//        //matchingJobsTxt.resignFirstResponder()
//        
//    }
    
    //MARK: - BasePopUpActionsDelegate
    func okAction(num: Int) {
        
    }
    
    func cancelAction(num: Int) {
        Global.sharedInstance.bWereThereAnyChanges = false
       
        if isTabBack {
            isTabBack = false
            let _ = self.navigationController?.popViewController(animated: true)
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.setContentOffset(CGPoint.zero, animated: true)
        } else {
            changeSubViewProfile(tagNewView: subviewTag)
        }
    }
    
    func openBasePopUp() {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let basePopUp = story.instantiateViewController(withIdentifier: "BasePopUpViewController") as! BasePopUpViewController
        basePopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        basePopUp.basePopUpActionDelegate = self
        basePopUp.text = "האם ברצונך לשמור לפני היציאה?"
        
        self.present(basePopUp, animated: true, completion: nil)
    }
    
    //MARK: - Camera
    
    var isUserImage = false
    
    func showCameraActionSheet()
    {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "צלם", style: .default, handler: {(alert: UIAlertAction!) in self.openCamera()})
        let libraryAction = UIAlertAction(title: "העלה תמונה", style: .default, handler: {(alert: UIAlertAction!) in self.openLibrary()})
        
        let cancelAction = UIAlertAction(title: "ביטול", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            Global.sharedInstance.bIsOpenFiles = true
            
            if btnEmployeeVisibleProfile.backgroundColor == UIColor.myTurquoiseWithOpacity {
                visibleProfile.isFromCamera = true
            }
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            //            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openLibrary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            Global.sharedInstance.bIsOpenFiles = true
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL{
            if let imageName = imageUrl.lastPathComponent{
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage{
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = Global.sharedInstance.setImageToString(image: image)
                    if imageString != ""{
                        if isUserImage {
                            isUserImage = false
                            imgViewUserProphilImage.contentMode = .scaleToFill
                            imgViewUserProphilImage.image = image
                            Global.sharedInstance.user.profileImage = image
                            setImageProfileInServer(imageName: imageName, image: imageString)
                        } else {
                            self.uploadFilesDelegate.getFile!(imgName: imageName, img: imageString, displayImg: image)
                        }
                        picker.dismiss(animated: true, completion: nil)
                    }else{
                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
            }
        } else {
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                let imageName = "img.JPEG"
                let imageString = Global.sharedInstance.setImageToString(image: image)
                if imageString != ""{
                    if isUserImage {
                        isUserImage = false
                        imgViewUserProphilImage.contentMode = .scaleToFill
                        imgViewUserProphilImage.image = image
                        Global.sharedInstance.user.profileImage = image
                        setImageProfileInServer(imageName: imageName, image: imageString)
                    } else {
                        self.uploadFilesDelegate.getFile!(imgName: imageName, img: imageString, displayImg: image)
                    }
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: - uploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    func uploadFile(fileUrl: String) {
        if fileUrl != "" { // האם שמור קובץ בשרת
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ShowImagePopUpViewController") as? ShowImagePopUpViewController
            
            controller?.modalPresentationStyle = UIModalPresentationStyle.custom
            controller?.fileUrl = fileUrl
            
            self.present(controller!, animated: true, completion: nil)
        } else {
            showCameraActionSheet()
        }
    }
    
    //another
    func openViewController(VC: UIViewController) {
        self.navigationController?.pushViewController(VC, animated: true)
    }

}
