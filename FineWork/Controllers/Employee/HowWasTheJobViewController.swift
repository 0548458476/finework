//
//  HowWasTheJobViewController.swift
//  FineWork
//
//  Created by User on 26.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class HowWasTheJobViewController: UIViewController,UITextViewDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    //MARK:-- Labels
    
    @IBOutlet weak var lblTitleHowWasIt: UILabel!
    @IBOutlet weak var lblHelloEmployee: UILabel!
    @IBOutlet weak var finishWorker: UILabel!
    @IBOutlet weak var lblPleaseRate: UILabel!
    
    @IBOutlet weak var lblFreeText: UITextField!
    @IBOutlet weak var lblWorkAgain: UILabel!
    @IBOutlet weak var lblSend: UILabel!
    @IBOutlet weak var txtFreeText: UILabel!
    
    //MARK:-- Buttons
    @IBOutlet weak var YesNo: UISegmentedControl!
    

    @IBOutlet weak var btnSend: UIButton!
    
    //MARK:-- Sliders
    @IBOutlet weak var lblSlider1: UILabel!
    @IBOutlet weak var slider1: UISlider!
    @IBOutlet weak var lblSlider2: UILabel!
    @IBOutlet weak var slider2: UISlider!
    @IBOutlet weak var lblSlider3: UILabel!
    @IBOutlet weak var slider3: UISlider!
    @IBOutlet weak var lblSlider4: UILabel!
    @IBOutlet weak var slider4: UISlider!
    
    
    //MARK:-- Slider actions
    @IBAction func reliabilitySliderChanged(_ sender: UISlider) {
        
        setSliderValue(slider: slider1)
    }
    
    @IBAction func FairnessSliderChanged(_ sender: UISlider) {
        
        setSliderValue(slider: slider2)
    }
    
    @IBAction func WorkingConditionsSliderChanged(_ sender: UISlider) {
        
        setSliderValue(slider: slider3)
    }
    
    @IBAction func generalSliderChanged(_ sender: UISlider) {
        
        setSliderValue(slider: slider4)
    }
    
    //MARK:-- Actions
    @IBAction func btnNoAction(_ sender: Any) {
    }
    
    @IBAction func btnYesAction(_ sender: Any) {
    }
    
    
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        if YesNo.selectedSegmentIndex == 0 || YesNo.selectedSegmentIndex == 1 ||  YesNo.selectedSegmentIndex == 2 {
            
            
            var id :Int = 0
            if Global.sharedInstance.user.iUserType.rawValue == UsetType.Employee.rawValue {
                id = RankingType.RankByWorker.rawValue

            }else{
                id = RankingType.RankByEmployer.rawValue
            }
            var b :Bool? = nil
            if YesNo.selectedSegmentIndex == 0 || YesNo.selectedSegmentIndex == 1{
              b = true
            }else if YesNo.selectedSegmentIndex == 2 {
                b = false
            }
            
           let rankingMeasure = RankingMeasure(_iRankingType: id,_iJobId: workerOfferedJob.iJobId,_iMeasureValue1: Int(slider1.value),_iMeasureValue2: Int(slider2.value),_iMeasureValue3: Int(slider3.value),_iMeasureValue4: Int(slider4.value),_nvRankingComment: lblFreeText.text!,_bAgreeToWorkAgain: b)
            UpdateRankingMeasure(rank : rankingMeasure)
      
        } else{
            Alert.sharedInstance.showAlert(mess: "נא מלא את הפרטים הנדרשים!")

        }
    }
    
    //MARK: - Variable
    var workerOfferedJob = WorkerOfferedJob()
    
    //MARK: - initial
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDataOnUI()
        registerForKeyboardNotifications()
    }
   
    override func viewDidLayoutSubviews() {
        
        YesNo.layer.cornerRadius = YesNo.frame.height / 2
        YesNo.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
        YesNo.layer.borderWidth = 1
        YesNo.clipsToBounds = true
        
        btnSend.backgroundColor = ControlsDesign.sharedInstance.colorOrang
        btnSend.titleLabel!.font = UIFont(name: "FontAwesome", size: 17)!
        btnSend.setTitle("\u{f1d9}", for: .normal)
        btnSend.setTitleColor(UIColor.white, for: .normal)
        btnSend.layer.cornerRadius = 0.5 * btnSend.bounds.size.width
        
    }
    func setDisplayData(index: Int){
        workerOfferedJob =  Global.sharedInstance.lWorkerOfferedJobs[index]
        print(workerOfferedJob.nvEmployerName)
    }
    
    func setDataOnUI(){
        // txtFreeText.delegate = self
        
        slider1.maximumValue = 10
        slider1.minimumValue = 0
        
        slider2.maximumValue = 10
        slider2.minimumValue = 0
        
        slider3.maximumValue = 10
        slider3.minimumValue = 0
        
        slider4.maximumValue = 10
        slider4.minimumValue = 0
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        if Global.sharedInstance.user.iUserType.rawValue == UsetType.Employee.rawValue {
            
            self.lblSlider2.text = "הגינות"
            self.lblSlider3.text = "תנאי עבודה"
            self.finishWorker.text = "סיימת את עבודתך אצל \(workerOfferedJob.nvEmployerName)"
            self.lblPleaseRate.text = "נא דרג מ-0 עד 10 לפי הפרמטרים הבאים:"
            self.lblHelloEmployee.text = "שלום \(Global.sharedInstance.user.nvUserName)"
            self.lblWorkAgain.text = "האם היית מעוניין לעבוד שוב אצל אותו מעסיק?"
        } else {
            self.lblTitleHowWasIt.isHidden = true
            
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setSliderValue(slider:UISlider)
    {
        let handleView = slider.subviews.last!
        
        var label:UILabel = UILabel()
        
        if (handleView.viewWithTag(1000) as? UILabel) != nil
        {
            label = (handleView.viewWithTag(1000)! as! UILabel)
            label.text = (Int(slider.value)).description
        }
        else
        {
            label = UILabel(frame: handleView.bounds)
            label.tag = 1000
            label.text = (Int(slider.value)).description
            label.backgroundColor = UIColor.clear
            label.textAlignment = .center
            handleView.addSubview(label)
        }
    }
//    
//    func valid() -> Bool {
//       // if YesNo.is
//    
//    }
    //MARK: - UITextView
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        let desiredOffset = CGPoint(x: 0, y: -scrollView.contentInset.bottom)
        scrollView.setContentOffset(desiredOffset, animated: true)
    }
    
    
    func dismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        //    barContainerViewController.setTitle(title: "חיפוש עובד חדש")
        barContainerViewController.titleText = "\(workerOfferedJob.nvDomain) - \(workerOfferedJob.nvRole)"
    }
    
    // MARK: - function To Server

    func UpdateRankingMeasure(rank : RankingMeasure){
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        //RankingMeasure rankingMeasure, int iUserId
        dic["rankingMeasure"] = rank.getDicFromRankingMeasure()
            as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId  as AnyObject
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print("UpdateRankingMeasure \(responseObject as! [String:AnyObject])")
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                self.deregisterFromKeyboardNotifications()
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                var _ = self.navigationController?.popViewController(animated: true)
            
            } else {
                Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            //  Generic.sharedInstance.hideNativeActivityIndicator(cont: self.parent != nil ? self.parentVC! : self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateRankingMeasure")
        
        

    }
    
    //MARK: - keyBoard notifications
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.lblFreeText {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
       // self.scrollView.contentInset = contentInsets
      //  self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
      //  self.scrollView.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        lblFreeText = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        lblFreeText = nil
    }
}
