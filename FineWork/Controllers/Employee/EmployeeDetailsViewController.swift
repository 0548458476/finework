
//
//  EmployeeDetailsViewController.swift
//  FineWork
//
//  Created by Racheli Kroiz on 5.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
import FileBrowser


protocol openFileBrowserDelegate{
    func openFileBrowser()
}

protocol OpenDatePickerDelegate {
    func OpenDatePicker(viewCon:UITableViewCell,tag:Int,dateToShow:String)
}

protocol getTextFieldDelegate {
    func getTextField(textField:UITextField, originY:CGFloat)
}

//protocol scrollToBottomDelegate {
//    func scrollToBottom()
//}

protocol EmployeeDetails_SupperView_Delegate {
    func remove101FormFromSuperView(tagNewView:Int)
    func tapBack()
    func setImageProfile(image:UIImage)
    func openDatePicker1(datePickerPopUpViewController:DatePickerPopUpViewController)
}

class EmployeeDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate, UIGestureRecognizerDelegate,openFileBrowserDelegate,OpenDatePickerDelegate,UIScrollViewDelegate,getTextFieldDelegate,ChangeSubViewProfileDelegate/*,scrollToBottomDelegate,disableScrollTblDelegate*/, UploadFilesDelegate {
    
    //MARK - Outlet
//    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:-- Buttons
    //MARK:--- Menu Buttons
    @IBOutlet var btnEmployeeDetaisCard: UIButton!
    
    @IBOutlet var btnEmployeeSalaryDetails: UIButton!
    @IBOutlet var btnEmployeeVisibleProfile: UIButton!
    
    @IBOutlet var btnEmployeeContract: UIButton!
    @IBOutlet var btnEmployeeRegisterDetails: UIButton!
    @IBOutlet var btnUserProphilImgChange: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnBackSmallView: UIButton!
    //MARK:-- Images
    @IBOutlet var imgViewUserProphilImage: UIImageView!
    
    @IBOutlet weak var imgViewBackground: UIImageView!
    //MARK:-- Labels
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblUserDetails: UILabel!
    
    @IBOutlet var lblUserName2: UILabel!
    @IBOutlet var lblUserDetails2: UILabel!
    
    //MARK:--Views
    
//    @IBOutlet weak var viewInScrollView: UIView!
    
    
    @IBOutlet weak var viewMenuSmall: UIView!
    @IBOutlet var viewDivider: UIView!
    @IBOutlet var viewMenu: UIView!
    @IBOutlet var viewUnderIMGViewProphilUserName: UIView!
    @IBOutlet weak var viewBottomSeperator: UIView!
    @IBOutlet weak var buttonView: UIView!
    
    @IBOutlet weak var containerView: UIView!
    //MARK:-- Actions
//    @IBAction func btnUserProphilImgChangeAction(_ sender: Any) {
//        ChangePicture()
//    }
    
    @IBAction func btnRemoveSelfAction(_ sender: UIButton) {
        
        employeeDetails_SupperView_Delegate.remove101FormFromSuperView(tagNewView: sender.tag)
        
//
//        let destination = (storyboard?.instantiateViewController(withIdentifier: "PersonalInformationModelViewController")as? PersonalInformationModelViewController?)!
//        
//        self.navigationController?.pushViewController(destination!, animated: false)
    }
    @IBAction func backAction(_ sender: UIButton) {
        employeeDetails_SupperView_Delegate.tapBack()
    }
    //MARK: -- TableView
    @IBOutlet weak var extTbl: UITableView!
    
    @IBOutlet weak var tblCity: UITableView!
    @IBOutlet weak var conTopViewMenuSmall: NSLayoutConstraint!
  
    @IBOutlet weak var conTopContainerView: NSLayoutConstraint!
    //MARK: - Variables
    var strImage = ""
    var flagChangePictiure = false
    var imagePicker:UIImagePickerController?
    var subView = UIView()
    var HeightView:CGFloat = CGFloat()
    var txtSelected:UITextField = UITextField()
    var cellOriginY:CGFloat = 0.0
    var lastContentOffset:CGFloat = 0.0
    var rectOfCellInSuperview:CGRect = CGRect()
    var employeeDetails_SupperView_Delegate:EmployeeDetails_SupperView_Delegate! = nil
    var firstTextFieldError:UITextField?
    var firstCellError_Y:CGFloat = -1
    
    var viewHeader = UIView()
    var image = UIImageView()
    
    var imageBackgraund = UIImageView()
    
    var viewseperator = UIView()
    var viewUnderImage = UIView()
    var lblUserName1 = UILabel()
    var lblUserDetails1 = UILabel()
    let buttonView2 = UIView()
    var btnEmployeeDetaisCard2 = UIButton()
    var btnEmployeeSalaryDetails2 = UIButton()
    var btnEmployeeVisibleProfile2 = UIButton()
    var btnEmployeeContract2 = UIButton()
    var btnEmployeeRegisterDetails2 = UIButton()
    var btnBack2 = UIButton()
    
    
    var errorArray = ["nvFirstName":false,"nvLastName":false,"nvIdentityNumber":false,"dBirthDate":false,"iCityId":false,"nvStreet":false,"nvHouseNumber":false,"nvPhone":false,"lWorkerChilds":false,"nvSpouseFirstName":false,"nvSpouseLastName":false,"dSpouseBirthDate":false,"nvSpouseIdentityNumber":false,"bHaveOtherIncome":false,"lWorkerTaxCoordinations":false,"mTillAmount":false,"fDeductedPercent":false,"fBeyondTaxDeducted":false,"fileObj":false,"reason":false,"2":false,"3":false,"4":false,"5":false,"6":false,"7":false,"8":false,"9":false,"10":false,"11":false,"12":false,"13":false,"14":false]
    
    var errorChildrenArray = Dictionary<Int,Dictionary<String,Bool>>()
    
    let screenSize = UIScreen.main.bounds
    var didFirstDelegate = true
    
    var citiesArr = [String:Int]()
    var citiesName = [String]()
    var citiesFilter = [String]()
    
    var fromContainerDelegate:ChangeSubViewProfileDelegate! = nil
    var isUserImage : Bool = false

    //MARK: - Initial
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //extTbl.alwaysBounceVertical = false
        //extTbl.isScrollEnabled = false
        
        
//        self.edgesForExtendedLayout = []
//        self.extendedLayoutIncludesOpaqueBars = false
//        self.automaticallyAdjustsScrollViewInsets = false

        
//        scrollView.delegate = self
        //with scrollView
        Global.sharedInstance.personalInformationModelHeight = self.view.frame.size.height
        
        imagePicker = UIImagePickerController()
        setDesign()
        
        GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame = Float(view.frame.size.height)
        
        //        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened = ["Reason101HandicappedTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)),"Reason101ResidentPlaceTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((90/750) * 2)),"Reason101NewImmegrantTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((75/750) * 2)),"Reason101AddFileDisabledBlindTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)),"reason101ManSingleParentTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((120/750) * 2)),"Reason101SingleParentTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((110/750) * 2)),"Reason101ChildrenNotHoldTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)),"Reason101FoodToPartnerTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)),"Reason101SoldierTableViewCell":
        //            (GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((75/750) * 2)),"Reason101EndLearnTableViewCell":(GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2))]
        
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((90/750) * 2)
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((75/750) * 2)
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)
//        
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((120/750) * 2)
//        
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101SingleParentTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((110/750) * 2)
//        
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101ChildrenNotHoldTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)
//        
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101SoldierTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((75/750) * 2)
//        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((47/750) * 2)
        
        
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController = self
        extTbl.delegate = self
        extTbl.separatorStyle = .none
        extTbl.contentSize = CGSize(width: self.extTbl.frame.width, height: self.extTbl.frame.height)
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
        image.frame = imgViewUserProphilImage.frame
        // 1
        let layer = CALayer()
        layer.frame = image.bounds
        
        // 2
        layer.contents = image.image
        layer.contentsGravity = kCAGravityCenter
        
        // 3
        layer.magnificationFilter = kCAFilterLinear
        layer.isGeometryFlipped = false
        
        // 4
        layer.backgroundColor = UIColor.clear.cgColor
        layer.opacity = 1.0
        layer.isHidden = false
        layer.masksToBounds = false
        
        // 5
        layer.cornerRadius = image.frame.height/2
        layer.borderWidth = 7.0
        layer.borderColor = UIColor.gray.cgColor
        
        // 6
        layer.shadowOpacity = 0.75
        layer.shadowOffset = CGSize(width: 3, height: 2)
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize.zero
        image.layer.addSublayer(layer)
        image.layer.borderColor = UIColor.gray.cgColor
        image.layer.borderWidth = 7
        
        //        // 1
//        let layer = CALayer()
//        layer.frame = imgViewUserProphilImage.bounds
//        
//        // 2
//        layer.contents = imgViewUserProphilImage.image
//        layer.contentsGravity = kCAGravityCenter
//        
//        // 3
//        layer.magnificationFilter = kCAFilterLinear
//        layer.isGeometryFlipped = false
//        
//        // 4
//        layer.backgroundColor = UIColor.clear.cgColor
//        layer.opacity = 1.0
//        layer.isHidden = false
//        layer.masksToBounds = false
//        
//        // 5
//        layer.cornerRadius = imgViewUserProphilImage.frame.height/2
//        layer.borderWidth = 7.0
//        layer.borderColor = UIColor.gray.cgColor
//        
//        // 6
//        layer.shadowOpacity = 0.75
//        layer.shadowOffset = CGSize(width: 3, height: 2)
//        layer.shadowRadius = 3.0
//        layer.shadowOffset = CGSize.zero
//        imgViewUserProphilImage.layer.addSublayer(layer)
//        
//        image.layer.addSublayer(layer)
        
        
        //
        imgViewUserProphilImage.layer.cornerRadius = imgViewUserProphilImage.frame.height/2
        imgViewUserProphilImage.layer.masksToBounds = true
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(self.imageTapped(img:)))
        imgViewUserProphilImage.isUserInteractionEnabled = true
        imgViewUserProphilImage.addGestureRecognizer(tapGestureRecognizer1)
        
        image.layer.cornerRadius = image.frame.height/2
        image.layer.masksToBounds = true
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(tapGestureRecognizer1)
        
        let layer2 = layer
        layer2.frame = viewUnderIMGViewProphilUserName.bounds
        layer2.contents = UIColor.clear
        layer2.cornerRadius = viewUnderIMGViewProphilUserName.frame.height/2
        layer2.backgroundColor = UIColor.clear.cgColor
        layer2.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 0.25
        viewUnderIMGViewProphilUserName.layer.addSublayer(layer2)
        viewUnderIMGViewProphilUserName.layer.cornerRadius = viewUnderIMGViewProphilUserName.frame.height/2
        
        viewUnderImage.layer.addSublayer(layer2)
        viewUnderImage.layer.cornerRadius = viewUnderImage.frame.height/2
        
        lblUserName.text = Global.sharedInstance.user.nvUserName
        
        lblUserName1.text = Global.sharedInstance.user.nvUserName
        lblUserName1.font = lblUserName.font
        lblUserName1.textColor = lblUserName.textColor
        lblUserName1.textAlignment = .center
        
        lblUserDetails1.text = lblUserDetails.text
        lblUserDetails1.font = lblUserDetails.font
        lblUserDetails1.textColor = lblUserDetails.textColor
        
        lblUserName2.text = Global.sharedInstance.user.nvUserName
        lblUserName2.font = lblUserName.font
        lblUserName2.textColor = lblUserName.textColor
        lblUserName2.textAlignment = .center
        
        lblUserDetails2.text = lblUserDetails.text
        lblUserDetails2.font = lblUserDetails.font
        lblUserDetails2.textColor = lblUserDetails.textColor
        
        image.frame = imgViewUserProphilImage.frame
        lblUserName1.frame = lblUserName.frame
        lblUserDetails1.frame = lblUserDetails.frame
        viewUnderImage.frame = viewUnderIMGViewProphilUserName.frame
        viewseperator.backgroundColor = viewDivider.backgroundColor
        viewseperator.frame = viewDivider.frame
        
         imageBackgraund.frame = imgViewBackground.frame
        imageBackgraund.image = UIImage(named:"background_profile")
        
        viewHeader.frame = viewMenu.frame
     //   viewHeader.backgroundColor = UIColor.blue
        
        btnEmployeeDetaisCard2.frame = btnEmployeeDetaisCard.frame
        btnEmployeeSalaryDetails2.frame = btnEmployeeSalaryDetails.frame
        btnEmployeeVisibleProfile2.frame = btnEmployeeVisibleProfile.frame
        btnEmployeeContract2.frame = btnEmployeeContract.frame
        btnEmployeeRegisterDetails2.frame = btnEmployeeRegisterDetails.frame
        btnBack2.frame = btnBack.frame
        
        btnEmployeeDetaisCard2.backgroundColor = btnEmployeeDetaisCard.backgroundColor
        btnEmployeeSalaryDetails2.backgroundColor = btnEmployeeSalaryDetails.backgroundColor
        btnEmployeeVisibleProfile2.backgroundColor = btnEmployeeVisibleProfile.backgroundColor
        btnEmployeeContract2.backgroundColor = btnEmployeeContract.backgroundColor
        btnEmployeeRegisterDetails2.backgroundColor = btnEmployeeRegisterDetails.backgroundColor
       // btnBack2.backgroundColor = btnBack.backgroundColor
        
        let backgroundButtonImage = UIImage(named: "money")
        btnEmployeeSalaryDetails2.setImage(backgroundButtonImage, for: .normal)
        
        btnEmployeeVisibleProfile2.titleLabel!.font = btnEmployeeVisibleProfile.titleLabel!.font
        btnEmployeeDetaisCard2.titleLabel!.font = btnEmployeeVisibleProfile.titleLabel!.font
        btnEmployeeRegisterDetails2.titleLabel!.font = btnEmployeeVisibleProfile.titleLabel!.font
        btnEmployeeContract2.titleLabel!.font = btnEmployeeVisibleProfile.titleLabel!.font
        btnBack2.titleLabel?.font = btnBack.titleLabel?.font
        
        setBorderToBtn(btn: btnEmployeeVisibleProfile2, color: UIColor.white , width: 1.0)
        setBorderToBtn(btn: btnEmployeeDetaisCard2, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeRegisterDetails2, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeContract2, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeSalaryDetails2, color: UIColor.white, width: 1.0)
        
        setTextcolorToBtn(btn: btnEmployeeVisibleProfile2, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeDetaisCard2, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeRegisterDetails2, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeContract2, color: UIColor.white)
        setTextcolorToBtn(btn: btnBack2, color: UIColor.white)
        
        btnEmployeeDetaisCard2.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_DetaisCard, for: .normal)//facebook
        btnEmployeeVisibleProfile2.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)//-profile
        btnEmployeeContract2.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_Contract, for: .normal)//people-2
        btnEmployeeRegisterDetails2.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_RegisterDetails, for: .normal)//interface-5
        
        btnBack2.setTitle(btnBack.titleLabel?.text, for: .normal)
        
        configureButton(btn: btnEmployeeDetaisCard2)
        configureButton(btn: btnEmployeeVisibleProfile2)
        configureButton(btn: btnEmployeeContract2)
        configureButton(btn: btnEmployeeSalaryDetails2)
        configureButton(btn: btnEmployeeRegisterDetails2)
        
        btnEmployeeSalaryDetails2.tag = 1
        btnEmployeeVisibleProfile2.tag = 2
        btnEmployeeContract2.tag = 3
        btnEmployeeRegisterDetails2.tag = 4
        
        btnEmployeeVisibleProfile2.addTarget(self, action: #selector(btnRemoveSelfAction), for: .touchUpInside)
        btnEmployeeContract2.addTarget(self, action: #selector(btnRemoveSelfAction), for: .touchUpInside)
        btnEmployeeSalaryDetails2.addTarget(self, action: #selector(btnRemoveSelfAction), for: .touchUpInside)
        btnEmployeeRegisterDetails2.addTarget(self, action: #selector(btnRemoveSelfAction), for: .touchUpInside)
        btnBack2.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        viewHeader.addSubview(viewUnderImage)
        viewHeader.addSubview(imageBackgraund)
        viewHeader.addSubview(image)
        viewHeader.addSubview(lblUserName1)
        viewHeader.addSubview(lblUserDetails1)
        viewHeader.addSubview(viewseperator)
        viewHeader.addSubview(btnBack2)
        
        buttonView2.frame = buttonView.frame
        buttonView2.backgroundColor = UIColor.clear
        
        buttonView2.addSubview(btnEmployeeDetaisCard2)
        buttonView2.addSubview(btnEmployeeSalaryDetails2)
        buttonView2.addSubview(btnEmployeeVisibleProfile2)
        buttonView2.addSubview(btnEmployeeContract2)
        buttonView2.addSubview(btnEmployeeRegisterDetails2)
//        for view in buttonView.subviews{
//           buttonView2.addSubview(view)
//        }
//        buttonView.frame = CGRect(origin: CGPoint(x: 50,y :300), size: buttonView.frame.size)
        viewHeader.addSubview(buttonView2)
        
//        viewHeader.addSubview(buttonView2)
        
        
        
        getCitiesList()
//        tblCity.delegate = self
//        tblCity.dataSource = self
//        tblCity.backgroundColor = UIColor.red
        view.bringSubview(toFront: tblCity)
//        tblCity.frame = CGRect(origin: CGPoint(x:10,y:770), size: CGSize(width: 50, height: 240))
//        conTopTblCities.constant = 770
//        conTopTblCities.constant = 250
        
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        unregisterKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let image : UIImage = Global.sharedInstance.user.profileImage {
            if imgViewUserProphilImage.image != nil && self.image != nil {
                imgViewUserProphilImage.image = image
                self.image.image = image
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        conTopViewMenuSmall.constant = -150
        conTopContainerView.constant = -150
        if let image : UIImage = Global.sharedInstance.user.profileImage {
            if imgViewUserProphilImage.image != nil && self.image != nil {
                imgViewUserProphilImage.image = image
                self.image.image = image
            }
        }
        registerKeyboardNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        buttonView2.frame = buttonView.frame
//        btnEmployeeDetaisCard2.frame = btnEmployeeDetaisCard.frame
//        btnEmployeeSalaryDetails2.frame = btnEmployeeSalaryDetails.frame
//        btnEmployeeVisibleProfile2.frame = btnEmployeeVisibleProfile.frame
//        btnEmployeeContract2.frame = btnEmployeeContract.frame
//        btnEmployeeRegisterDetails2.frame = btnEmployeeRegisterDetails.frame
        btnEmployeeDetaisCard2.frame = CGRect(origin: CGPoint(x: btnEmployeeDetaisCard.frame.origin.x, y:buttonView2.frame.size.height/2 - btnEmployeeDetaisCard2.frame.size.height/2), size: btnEmployeeDetaisCard.frame.size)
        btnEmployeeSalaryDetails2.frame = CGRect(origin: CGPoint(x: btnEmployeeSalaryDetails.frame.origin.x, y:buttonView2.frame.size.height/2 - btnEmployeeSalaryDetails2.frame.size.height/2), size: btnEmployeeSalaryDetails.frame.size)
        btnEmployeeVisibleProfile2.frame = CGRect(origin: CGPoint(x: btnEmployeeVisibleProfile.frame.origin.x, y:buttonView2.frame.size.height/2 - btnEmployeeVisibleProfile2.frame.size.height/2), size: btnEmployeeVisibleProfile.frame.size)
        btnEmployeeContract2.frame = CGRect(origin: CGPoint(x: btnEmployeeContract.frame.origin.x, y:buttonView2.frame.size.height/2 - btnEmployeeContract2.frame.size.height/2), size: btnEmployeeContract.frame.size)
        btnEmployeeRegisterDetails2.frame = CGRect(origin: CGPoint(x: btnEmployeeRegisterDetails.frame.origin.x, y:buttonView2.frame.size.height/2 - btnEmployeeRegisterDetails2.frame.size.height/2), size: btnEmployeeRegisterDetails.frame.size)
        
        lblUserDetails1.frame = lblUserDetails.frame
        btnBack2.frame = btnBack.frame
        
        if let image : UIImage = Global.sharedInstance.user.profileImage {
            if imgViewUserProphilImage.image != nil && self.image != nil {
                imgViewUserProphilImage.image = image
                self.image.image = image
            }
        }
        
//        self.scrollView.contentSize.height = (self.view.frame.size.height * 2.54) + GlobalEmployeeDetails.sharedInstance.howManyToScroll + GlobalEmployeeDetails.sharedInstance.howManyToScroll2
//                self.scrollView.contentSize.height = (self.view.frame.size.height * 2.7) + GlobalEmployeeDetails.sharedInstance.howManyToScroll + GlobalEmployeeDetails.sharedInstance.howManyToScroll2
    }
    
    //MARK: ----camera func
//    func ChangePicture()
//    {
//        openAlert()
//    }
//    func openAlert()
//    {
//        Alert.sharedInstance.AlertopenPictures(controller: self,tag: 2)
//    }
//    
//    func openAction(img: UIImageView)
//    {
//        Alert.sharedInstance.AlertopenPictures(controller: self,tag: 2)
//    }
//    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool
//    {
//        self.dismissKeyBoard()
//        return false
//    }
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
//    {
//        imgViewUserProphilImage.image = image
//        Global.sharedInstance.user.profileImage = image
//        self.image.image = image
//        self.dismiss(animated: true, completion: nil);
//    }
    
    func imageTapped(img: AnyObject)
    {
        isUserImage = true
        uploadFilesDelegate.uploadFile!(fileUrl: "")
        
        
//        openAlert()
        
        //        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
        //            print("Button capture")
        //
        //            imagePicker?.delegate = self
        //            imagePicker?.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
        //            imagePicker?.allowsEditing = false
        //
        //            self.present(imagePicker!, animated: true, completion: nil)
        //        }
        
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL{
//            if let imageName = imageUrl.lastPathComponent{
//                if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
//                    var image = imageOriginal
//                    if picker.allowsEditing {
//                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
//                            image = imageEdit
//                        }
//                    }
//                    let imageString = Global.sharedInstance.setImageToString(image: image)
//                    if imageString != ""{
//                        //                        image = Global.sharedInstance.resizeImage(image: image, newWidth: 100)
//                        imgViewUserProphilImage.contentMode = .scaleToFill
//                        imgViewUserProphilImage.image = image
//                        Global.sharedInstance.user.profileImage = image
//                        self.image.image = image
//                        employeeDetails_SupperView_Delegate.setImageProfile(image: image)
//                        setImageProfileInServer(imageName: imageName, image: imageString)
//                        picker.dismiss(animated: true, completion: nil)
//                    }else{
//                        Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
//                        picker.dismiss(animated: true, completion: nil)
//                    }
//
//                }
//            }
//        }else{
//            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
//                var image = imageOriginal
//                if picker.allowsEditing {
//                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
//                        image = imageEdit
//                    }
//                }
//                //                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//                let imageName = "img.JPEG"
//                let imageString = Global.sharedInstance.setImageToString(image: image)
//                if imageString != ""{
//                    imgViewUserProphilImage.contentMode = .scaleToFill
//                    imgViewUserProphilImage.image = image
//                    Global.sharedInstance.user.profileImage = image
//                    self.image.image = image
//                    employeeDetails_SupperView_Delegate.setImageProfile(image: image)
//                    setImageProfileInServer(imageName: imageName, image: imageString)
//                    picker.dismiss(animated: true, completion: nil)
//                    
//                }else{
//                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
//                    picker.dismiss(animated: true, completion: nil)
//                }
//            }
//        }
//    }
    
    func setImageFromFile(imgName: String, img: String, displayImg: UIImage?) {
        if displayImg != nil {
            imgViewUserProphilImage.contentMode = .scaleToFill
            imgViewUserProphilImage.image = displayImg
            Global.sharedInstance.user.profileImage = displayImg
            self.image.image = displayImg
            employeeDetails_SupperView_Delegate.setImageProfile(image: displayImg!)
            setImageProfileInServer(imageName: imgName, image: img)
        }
    }
    
    func setImageProfileInServer(imageName:String,image:String){
        let imageObj = FileObj()
        imageObj.nvFile = image
        imageObj.nvFileName = imageName
        var dicToServer:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dicToServer["image"] = imageObj.getImageDic() as AnyObject?
        dicToServer["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dicToServer["iUserType"] = Global.sharedInstance.user.iUserType.rawValue as AnyObject?
        api.sharedInstance.UpdateImage(params: dicToServer, success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
            }else{
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })

    }
    
    //        func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
    //            self.dismiss(animated: true, completion: { () -> Void in
    //            })
    //            imgViewUserProphilImage.image = image
    //        }
    //MARK:-- SETTERS Functions
    func configureButton(btn:UIButton)
    {
        let f:CGFloat = btn.frame.width / 2
        btn.layer.cornerRadius = f
        //        btn.layer.cornerRadius = 0.5 * btn.bounds.size.width
        //        btn.layer.borderColor = UIColor.white.cgColor
        //        btn.layer.borderWidth = 2.0
        //btn.clipsToBounds = true
    }
    
    func setDesign()    {
        setBackgroundcolor()
//        setImages()
        setBorders()
        setTextcolor()
        setCornerRadius()
        self.setIconFont()
//        getUserImage()
        self.setLocalizableString()
        setImages()
        setUserData()
    }
    func setBackgroundcolor()
    {
//        self.viewMenu.backgroundColor = ControlsDesign.sharedInstance.colorDarkPurple
//        viewMenuSmall.backgroundColor = viewMenu.backgroundColor
//        self.viewDivider.backgroundColor = ControlsDesign.sharedInstance.colorGrayShadow
        //        btnEmployeeVisibleProfile.backgroundColor = UIColor.clear
        //        btnEmployeeDetaisCard.backgroundColor = UIColor.clear
        //        btnEmployeeRegisterDetails.backgroundColor = UIColor.clear
        //        btnEmployeeContract.backgroundColor = UIColor.clear
        //        btnEmployeeSalaryDetails.backgroundColor = UIColor.clear
        setBackgroundcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: btnEmployeeDetaisCard, color: ControlsDesign.sharedInstance.colorTurquoiseWithOpacity)
        setBackgroundcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: btnEmployeeContract, color: UIColor.clear)
        setBackgroundcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.clear)
        
     //   setBackgroundcolorToBtn(btn: btnBack, color: ControlsDesign.sharedInstance.colorDarkPurple)
      //  setBackgroundcolorToBtn(btn: btnBack2, color: ControlsDesign.sharedInstance.colorDarkPurple)
     //   setBackgroundcolorToBtn(btn: btnBackSmallView, color: ControlsDesign.sharedInstance.colorDarkPurple)
        
        //        self.btnLogin.backgroundColor = ControlsDesign.sharedInstance.colorPurple
    }
    func setBackgroundcolorToBtn(btn:UIButton, color:UIColor)
    {
         btn.backgroundColor = color
    }
    func setTextcolorToBtn(btn:UIButton, color:UIColor)
    {
        btn.titleLabel?.textColor = color
        btn.tintColor = color
    }
    func setBorderToBtn(btn:UIButton, color:UIColor , width:CGFloat)
    {
        btn.layer.borderColor = color.cgColor
        btn.layer.borderWidth = width
    }
    
    func setImages(){
        let backgroundButtonImage = UIImage(named: "money")
          btnEmployeeSalaryDetails.setImage(backgroundButtonImage, for: .normal)
    }
    func setBorders()
    {
        setBorderToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.white , width: 1.0)
        setBorderToBtn(btn: btnEmployeeDetaisCard, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeContract, color: UIColor.white, width: 1.0)
        setBorderToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.white, width: 1.0)
    }
    func setTextcolor()
    {
        lblUserName.textColor = UIColor.white
        lblUserDetails.textColor = UIColor.white
        setTextcolorToBtn(btn: btnEmployeeVisibleProfile, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeDetaisCard, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeRegisterDetails, color: UIColor.white)
        setTextcolorToBtn(btn: btnEmployeeContract, color: UIColor.white)
//        setTextcolorToBtn(btn: btnEmployeeSalaryDetails, color: UIColor.white)
    }
    func setCornerRadius()
    {
        self.configureButton(btn: btnEmployeeDetaisCard)
        self.configureButton(btn: btnEmployeeVisibleProfile)
        self.configureButton(btn: btnEmployeeContract)
        self.configureButton(btn: btnEmployeeSalaryDetails)
        self.configureButton(btn: btnEmployeeRegisterDetails)
    }
    func setIconFont()
    {
        btnEmployeeDetaisCard.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_DetaisCard, for: .normal)//facebook
//        btnEmployeeSalaryDetails.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_SalaryDetails, for: .normal)//interface-6
        btnEmployeeVisibleProfile.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_VisibleProfile, for: .normal)//-profile
        btnEmployeeContract.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_Contract, for: .normal)//people-2
        btnEmployeeRegisterDetails.setTitle(FlatIcons.sharedInstance.PERSONAL_PARENT_VC_RegisterDetails, for: .normal)//interface-5
        
    }
    func setLocalizableString()
    {
    }
    
    func setUserData(){
        self.imgViewUserProphilImage.image = UIImage()
        self.image.image = UIImage()
        getUserImage()
        lblUserName.text = Global.sharedInstance.user.nvUserName
        lblUserName1.text = Global.sharedInstance.user.nvUserName
        lblUserName2.text = Global.sharedInstance.user.nvUserName
    }
    
    func getUserImage(){
        api.sharedInstance.GetImage(params: ["iUserId":(Global.sharedInstance.user.iUserId as AnyObject?)!], success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
                if let imagString = dicObj["Result"]?["nvFileURL"] as? String{
                    var stringPath = api.sharedInstance.buldUrlFile(fileName: imagString)
                    stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                    if let imageUrl = URL(string:stringPath){
                    self.downloadImage(url: imageUrl)
                    }
                }
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                self.imgViewUserProphilImage.contentMode = .scaleToFill
                self.imgViewUserProphilImage.image = image
                    Global.sharedInstance.user.profileImage = image
                    self.image.contentMode = .scaleToFill
                    self.image.image = image
                }
                Global.sharedInstance.user.picture.nvFile = Global.sharedInstance.convertImageToBase64(image: image)
            }
            
            //            print(response?.suggestedFilename ?? url.lastPathComponent)
            //            print("Download Finished")
        }
        
    }
    
    func getCitiesList(){
//        var citiesArr = [String:Int]()
//        var citiesName = [String]()
        api.sharedInstance.GetWorker101FormCodeTables(params: ["iLanguageId":(Global.sharedInstance.iLanguageId as AnyObject?)!], success: {(operation:AFHTTPRequestOperation?, responseObject:Any?)
            -> Void in
            print(responseObject as Any)
            var dicObj = responseObject as! [String:AnyObject]
            if dicObj["Error"]?["iErrorCode"] as! Int ==  0{
                if let dicArr = dicObj["Result"] as? [Dictionary<String,AnyObject>]{
                        if let citiesArrDic = dicArr[0]["Value"] as? [Dictionary<String,AnyObject>]{
                            for cityDic in citiesArrDic{
                                if let id = cityDic["iId"] as? Int{
                                    if let name = cityDic["nvName"] as? String{
                                        self.citiesArr[name] = id
                                        self.citiesName.append(name)
                                    }
                                }
                            }
                            self.citiesName.sort{$0.compare($1) == .orderedAscending }
//                            citiesName.sort{$0.compare($1) == .orderedDescending }
                            Global.sharedInstance.citiesArr = self.citiesArr
                            self.citiesFilter = self.citiesName
                            if let personaldetailsCell:PersonaldetailsTableViewCell = self.extTbl.cellForRow(at: IndexPath(row: 0, section: 0)) as? PersonaldetailsTableViewCell{//פרטים אישיים
                                var cityName = ""
                                for (key, value) in Global.sharedInstance.citiesArr
                                {
                                    if value == GlobalEmployeeDetails.sharedInstance.worker101Form.iCityId{
                                        cityName = key
                                    }
                                }
                                DispatchQueue.main.async {
                                    personaldetailsCell.citiesName = self.citiesName
                                    personaldetailsCell.citiesFilter = self.citiesName
                                    
                                    personaldetailsCell.txtCity.text = cityName
                                    personaldetailsCell.tblCities.reloadData()
//                                    self.tblCity.reloadData()
                                }
                            }
                    }
                }
            }
        } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
        })

    }
    
    //MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblCity{
            return 1
        }else{
        return GlobalEmployeeDetails.sharedInstance.numSectionsInTbl
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCity{
           return citiesFilter.count
        }else{
        return GlobalEmployeeDetails.sharedInstance.numRowsForSection[section]
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblCity{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell", for: indexPath as IndexPath) as! FamilyStatusTableViewCell
            cell.setTitle(text: citiesFilter[indexPath.row])
//            (cell as! PersonaldetailsTableViewCell).setDisplayData()
//            cell.tag = indexPath.section
//            cell.selectionStyle = .none
//            setPersonalDetailsError(cell:cell as! PersonaldetailsTableViewCell)
            return cell

        }else{
        var rectOfCellInTableView:CGRect = CGRect()
        switch indexPath.row
        {
        case 0://שורה ראשונה=סל בסיסי
            switch indexPath.section//על איזה נתון כללי מדברים
            {
            case 0://פרטים אישיים
                let cell = tableView.dequeueReusableCell(withIdentifier: "PersonaldetailsTableViewCell", for: indexPath as IndexPath)
                cell.tag = indexPath.section
                
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! PersonaldetailsTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                (cell as! PersonaldetailsTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                
                (cell as! PersonaldetailsTableViewCell).openDatePickerDelegate = self
                (cell as! PersonaldetailsTableViewCell).delegateTextField = self
                
                (cell as! PersonaldetailsTableViewCell).txtFamilyStatus.text = GlobalEmployeeDetails.sharedInstance.familyStatus
                (cell as! PersonaldetailsTableViewCell).viewBottomSeperator.isHidden = false
                (cell as! PersonaldetailsTableViewCell).setDisplayData()
//                (cell as! PersonaldetailsTableViewCell).txtCity.addTarget(self, action: #selector(EmployeeDetailsViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
                //---
//                (cell as! PersonaldetailsTableViewCell).uploadFilesDelegate = self
//                self.uploadFilesDelegate2 = (cell as! PersonaldetailsTableViewCell)
                //---
                
                cell.selectionStyle = .none
                setPersonalDetailsError(cell:cell as! PersonaldetailsTableViewCell)
//                let tblFrame = (cell as! PersonaldetailsTableViewCell).tblCities.frame
//                tblCity.frame = CGRect(origin: CGPoint(x:tblFrame.origin.x,y:tblFrame.origin.y + rectOfCellInTableView.origin.y), size: tblFrame.size)
//                conTopTblCities.constant = tblFrame.origin.y + rectOfCellInTableView.origin.y - extTbl.contentOffset.y
                return cell
            case 1://פרטים על הכנסותי ממעביד זה
                let cell = tableView.dequeueReusableCell(withIdentifier: "EarningsDetailsTableViewCell", for: indexPath as IndexPath)
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! EarningsDetailsTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                (cell as! EarningsDetailsTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                //ע״מ לדעת אם לסמן משכורת נוספת כבחור(אם פתוחים הסל של ״יש לי הכנסות נוספות״)
                if GlobalEmployeeDetails.sharedInstance.selectRowsForSection[indexPath.section] == true
                {
                    (cell as! EarningsDetailsTableViewCell).btnHasMoreEarn.isSelected = true
                }
                else
                {
                    (cell as! EarningsDetailsTableViewCell).btnHasMoreEarn.isSelected = false
                }
                (cell as! EarningsDetailsTableViewCell).setDisplayData()
                cell.tag = indexPath.section
                if GlobalEmployeeDetails.sharedInstance.numRowsForSection[indexPath.section] == 2
                {
                    (cell as! EarningsDetailsTableViewCell).viewBottomSeperator.isHidden = true
                }
                else
                {
                    (cell as! EarningsDetailsTableViewCell).viewBottomSeperator.isHidden = false
                }
                
                cell.selectionStyle = .none
                setEarningsDetailsError(cell:cell as! EarningsDetailsTableViewCell)
                return cell
            case 2://אני מבקש פטור או זכוי ממס
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExemptionTaxCreditTableViewCell", for: indexPath as IndexPath)
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! ExemptionTaxCreditTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                (cell as! ExemptionTaxCreditTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                //if GlobalEmployeeDetails.sharedInstance.selectRowsForSection[indexPath.section] == true
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit == true
                {
                    (cell as! ExemptionTaxCreditTableViewCell).btnExemptionTaxCredit.isChecked = true
                    (cell as! ExemptionTaxCreditTableViewCell).lblUnderTitle.text = "אני מבקש פטור או זיכוי ממס מהסיבות הבאות"
                }
                else
                {
                    (cell as! ExemptionTaxCreditTableViewCell).btnExemptionTaxCredit.isChecked = false
                    (cell as! ExemptionTaxCreditTableViewCell).lblUnderTitle.text = ""
                }
                
                cell.tag = indexPath.section
                cell.selectionStyle = .none
                exemptionTaxCreditError(cell:cell as! ExemptionTaxCreditTableViewCell)
                return cell
            case 3:
                if GlobalEmployeeDetails.sharedInstance.numSectionsInTbl == 4//לא סימן שיש לו הכנסות נוספות
                {//end cell
                    let cell = tableView.dequeueReusableCell(withIdentifier: "End101TableViewCell", for: indexPath as IndexPath)
                    (cell as! End101TableViewCell).delegateTextField = self
                    (cell as! End101TableViewCell).changeSubViewProfileDelegate = self
                    
                    rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                    (cell as! End101TableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                    (cell as! End101TableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                    (cell as! End101TableViewCell).setDisplayData()
                    //---
//                    (cell as! End101TableViewCell).uploadFilesDelegate = self
//                    self.uploadFilesDelegate2 = (cell as! End101TableViewCell)
                    //---

                    cell.selectionStyle = .none
                    return cell
                }
                else//סימן שיש לו הכנסות נוספות ולכן יש להציג את הסל של:״אני מבקש תיאום מס מכיוון שיש לי הכנסות נוספות....
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Default101TableViewCell", for: indexPath as IndexPath)
                    rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                   (cell as! Default101TableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                    (cell as! Default101TableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodination == true
                    //if GlobalEmployeeDetails.sharedInstance.selectRowsForSection[indexPath.section] == true
                    {
                        (cell as! Default101TableViewCell).btnReason.isChecked = true
                    }
                    else
                    {
                        (cell as! Default101TableViewCell).btnReason.isChecked = false
                    }
                    (cell as! Default101TableViewCell).lblReason.text = "אני מבקש תיאום מס מכיון שיש לי הכנסות נוספות ממשכורת / קיצבה / מילגה"
                    cell.tag = indexPath.section
                    (cell as! Default101TableViewCell).setDisplayData()
                    (cell as! Default101TableViewCell).viewTopSeperator.isHidden = false
                    cell.selectionStyle = .none
                    setWorkerTaxCoordinations(cell:cell as! Default101TableViewCell)
                    return cell
                }
            case 4://מגיע לפה אם סימן שיש לו הכנסות נוספות ולכן יש להציג את הסל של:״אני מבקש תיאום מס לפי אישור פקיד שומה....
                let cell = tableView.dequeueReusableCell(withIdentifier: "Default101TableViewCell", for: indexPath as IndexPath)
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
               (cell as! Default101TableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                (cell as! Default101TableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                
                if GlobalEmployeeDetails.sharedInstance.worker101Form.bAskingTaxCoodinationByAssessor == true
                
                //if GlobalEmployeeDetails.sharedInstance.selectRowsForSection[indexPath.section] == true
                {
                    (cell as! Default101TableViewCell).btnReason.isChecked = true
                }
                else
                {
                    (cell as! Default101TableViewCell).btnReason.isChecked = false
                }
                (cell as! Default101TableViewCell).lblReason.text =
                "אני מבקש תיאום מס לפי אישור פקיד שומה (בתנאי וקיים אישור מצורף)"
                
                cell.tag = indexPath.section
                
                (cell as! Default101TableViewCell).setDisplayData()
                
                
                (cell as! Default101TableViewCell).viewTopSeperator.isHidden = false
                
                cell.selectionStyle = .none
                setTaxConError(cell: cell as! Default101TableViewCell)
                return cell
            default://if section == 5 end cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "End101TableViewCell", for: indexPath as IndexPath)
                
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! End101TableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                (cell as! End101TableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                (cell as! End101TableViewCell).setDisplayData()
                (cell as! End101TableViewCell).delegateTextField = self
                 (cell as! End101TableViewCell).changeSubViewProfileDelegate = self
                //---
//                (cell as! End101TableViewCell).uploadFilesDelegate = self
//                self.uploadFilesDelegate2 = (cell as! End101TableViewCell)
                //---
                cell.selectionStyle = .none
                return cell
            }
        default://יצירת השורות שמתווספות
            switch indexPath.section
            {
            case 0://פרטים אישיים
                //אין ילדים ואין בן זוג
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count == 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildTableViewCell", for: indexPath as IndexPath)
                    cell.tag = indexPath.section
                    (cell as! AddChildTableViewCell).viewBottom.isHidden = false
                    (cell as! AddChildTableViewCell).setDisplayData()
                    rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                    (cell as! AddChildTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                    (cell as! AddChildTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
//                    //---
//                    (cell as! AddChildTableViewCell).uploadFilesDelegate = self
//                    self.uploadFilesDelegate2 = (cell as! AddChildTableViewCell)
//                    //---
//                    setAddChildError(cell:cell as! AddChildTableViewCell)
                    return cell
                }
                //יש רק ילדים ולא בן זוג
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count > 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
                {
                    //הצגת הסל של הילדים
                    if indexPath.row < GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                    {
                        //children cell
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildrenDetailsFor101TableViewCell", for: indexPath as IndexPath)
                        
                        rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                        (cell as! ChildrenDetailsFor101TableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                        (cell as! ChildrenDetailsFor101TableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                        
                        (cell as! ChildrenDetailsFor101TableViewCell).tagSection = indexPath.section
                        
                        (cell as! ChildrenDetailsFor101TableViewCell).openDatePickerDelegate = self
                        
                        (cell as! ChildrenDetailsFor101TableViewCell).viewBottomSeperator.isHidden = true
                        (cell as! ChildrenDetailsFor101TableViewCell).delegateTextField = self

                        cell.selectionStyle = .none
                        cell.tag = indexPath.row - 1
                        (cell as! ChildrenDetailsFor101TableViewCell).setDisplayData(workerChild: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[cell.tag])
                        if indexPath.row == 1 {
                            setAddChildError(cell:cell as! ChildrenDetailsFor101TableViewCell)
                        }else{
                           (cell as! ChildrenDetailsFor101TableViewCell).lblErrorChildren.isHidden = true
                        }
                        setChieldError(cell: cell as! ChildrenDetailsFor101TableViewCell, childIndex: indexPath.row)
                        return cell
                    }
                    else//סל של הוספת ילדים
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildTableViewCell", for: indexPath as IndexPath)
                        cell.tag = indexPath.section
                        (cell as! AddChildTableViewCell).viewBottom.isHidden = false
                        (cell as! AddChildTableViewCell).setDisplayData()
                        rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                        (cell as! AddChildTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                        (cell as! AddChildTableViewCell).cellOriginY =  rectOfCellInTableView.origin.y
                        //---
//                        (cell as! AddChildTableViewCell).uploadFilesDelegate = self
//                        self.uploadFilesDelegate2 = (cell as! AddChildTableViewCell)
                        //---
                        
                        return cell
                    }
                }
                //יש רק בן זוג בלי ילדים
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count == 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2
                {
                    //הצגת הסל של הוסף ילדים תחילה
                    if indexPath.row == GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildTableViewCell", for: indexPath as IndexPath)
                        cell.tag = indexPath.section
                        (cell as! AddChildTableViewCell).viewBottom.isHidden = false
                        (cell as! AddChildTableViewCell).setDisplayData()
                        rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                        (cell as! AddChildTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                        (cell as! AddChildTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                        //---
//                        (cell as! AddChildTableViewCell).uploadFilesDelegate = self
//                        self.uploadFilesDelegate2 = (cell as! AddChildTableViewCell)
                        //---
//                        setAddChildError(cell:cell as! AddChildTableViewCell)
                        return cell
                    }
                    //הצגת הסל של הבן זוג
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PartnerDeatilsTableViewCell", for: indexPath as IndexPath)
                    
                    rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                    (cell as! PartnerDeatilsTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                    (cell as! PartnerDeatilsTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                    
                    (cell as! PartnerDeatilsTableViewCell).openDatePickerDelegate = self
                    
                    (cell as! PartnerDeatilsTableViewCell).delegateTextField = self
                    (cell as! PartnerDeatilsTableViewCell).viewBottomSeperator.isHidden = false
                    if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true
                    {
                        (cell as! PartnerDeatilsTableViewCell).btnIncome.isChecked = true
                    }
                    else
                    {
                        (cell as! PartnerDeatilsTableViewCell).btnIncome.isChecked = false
                    }
                    (cell as! PartnerDeatilsTableViewCell).setDisplayData()
                    
                    cell.selectionStyle = .none
                    cell.tag = indexPath.section
                    setPartnerDeatilsError(cell: cell as! PartnerDeatilsTableViewCell)
                    return cell
                }
                //אם יש בן זוג ויש ילדים
                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count > 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2
                {
                    //הצגת הסל של הילדים תחילה
                    if indexPath.row < GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                    {
                        //children cell
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ChildrenDetailsFor101TableViewCell", for: indexPath as IndexPath)
                        
                        rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                        (cell as! ChildrenDetailsFor101TableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                        (cell as! ChildrenDetailsFor101TableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                        
                        (cell as! ChildrenDetailsFor101TableViewCell).tagSection = indexPath.section
                        
                        (cell as! ChildrenDetailsFor101TableViewCell).openDatePickerDelegate = self
                        
                        (cell as! ChildrenDetailsFor101TableViewCell).viewBottomSeperator.isHidden = true
                        (cell as! ChildrenDetailsFor101TableViewCell).delegateTextField = self
                        
                        cell.selectionStyle = .none
                        cell.tag = indexPath.row - 1
                        (cell as! ChildrenDetailsFor101TableViewCell).setDisplayData(workerChild: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[cell.tag])
                        if indexPath.row == 1 {
                            setAddChildError(cell:cell as! ChildrenDetailsFor101TableViewCell)
                        }else{
                            (cell as! ChildrenDetailsFor101TableViewCell).lblErrorChildren.isHidden = true
                        }
                        setChieldError(cell: cell as! ChildrenDetailsFor101TableViewCell, childIndex: indexPath.row)
                        return cell
                    }
                    //הצגת הסל של הוסף ילדים
                    if indexPath.row == GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildTableViewCell", for: indexPath as IndexPath)
                        cell.tag = indexPath.section
                        (cell as! AddChildTableViewCell).viewBottom.isHidden = false
                        (cell as! AddChildTableViewCell).setDisplayData()
                        rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                        (cell as! AddChildTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                        (cell as! AddChildTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                        //---
//                        (cell as! AddChildTableViewCell).uploadFilesDelegate = self
//                        self.uploadFilesDelegate2 = (cell as! AddChildTableViewCell)
                        //---
//                        setAddChildError(cell:cell as! AddChildTableViewCell)
                        return cell
                    }
                    //הצגת הסל של הבן זוג
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PartnerDeatilsTableViewCell", for: indexPath as IndexPath)
                    
                    rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                    (cell as! PartnerDeatilsTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                    (cell as! PartnerDeatilsTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                    
                    (cell as! PartnerDeatilsTableViewCell).openDatePickerDelegate = self
                    
                    (cell as! PartnerDeatilsTableViewCell).delegateTextField = self
                    (cell as! PartnerDeatilsTableViewCell).viewBottomSeperator.isHidden = false
                    if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true
                    {
                        (cell as! PartnerDeatilsTableViewCell).btnIncome.isChecked = true
                    }
                    else
                    {
                        (cell as! PartnerDeatilsTableViewCell).btnIncome.isChecked = false
                    }
                    (cell as! PartnerDeatilsTableViewCell).setDisplayData()
                    
                    cell.selectionStyle = .none
                    cell.tag = indexPath.section
                    setPartnerDeatilsError(cell: cell as! PartnerDeatilsTableViewCell)
                    return cell
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddChildTableViewCell", for: indexPath as IndexPath)
                cell.tag = indexPath.section
                (cell as! AddChildTableViewCell).viewBottom.isHidden = false
                (cell as! AddChildTableViewCell).setDisplayData()
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! AddChildTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                (cell as! AddChildTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                //---
//                (cell as! AddChildTableViewCell).uploadFilesDelegate = self
//                self.uploadFilesDelegate2 = (cell as! AddChildTableViewCell)
                //---
//                setAddChildError(cell:cell as! AddChildTableViewCell)
                return cell
                
                /*if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2 && indexPath.row == 1
                {
                    //בן זוג
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PartnerDeatilsTableViewCell", for: indexPath as IndexPath)
                    
                    rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                    (cell as! PartnerDeatilsTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                    
                    (cell as! PartnerDeatilsTableViewCell).openDatePickerDelegate = self
                    
                    (cell as! PartnerDeatilsTableViewCell).delegateTextField = self
                    (cell as! PartnerDeatilsTableViewCell).viewBottomSeperator.isHidden = false
                    if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true
                    {
                        (cell as! PartnerDeatilsTableViewCell).btnIncome.isChecked = true
                    }
                    else
                    {
                        (cell as! PartnerDeatilsTableViewCell).btnIncome.isChecked = false
                    }
                    (cell as! PartnerDeatilsTableViewCell).setDisplayData()
                    
                    //                    (cell as! PartnerDeatilsTableViewCell).delegate = self
                    cell.selectionStyle = .none
                    cell.tag = indexPath.section
                    return cell
                }
                //children cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChildrenDetailsFor101TableViewCell", for: indexPath as IndexPath)
                
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! ChildrenDetailsFor101TableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                
                
                (cell as! ChildrenDetailsFor101TableViewCell).tagSection = indexPath.section
                
                (cell as! ChildrenDetailsFor101TableViewCell).openDatePickerDelegate = self
                
                (cell as! ChildrenDetailsFor101TableViewCell).viewBottomSeperator.isHidden = false
                (cell as! ChildrenDetailsFor101TableViewCell).delegateTextField = self
                //init default
                
                cell.selectionStyle = .none
                
                if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2
                {
                    cell.tag = indexPath.row - 2
                }
                else
                {
                    cell.tag = indexPath.row - 1
                }
                //                if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.indices.contains(cell.tag)
                //                {
                //                    if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[cell.tag].isObjectNull(btnChanged: GlobalEmployeeDetails.sharedInstance.isButtonsChanged[cell.tag])
                //                    {
                //                        (cell as! ChildrenDetailsFor101TableViewCell).setDisplayDataNull()
                //                    }
                //                    else
                //                    {
                
                
                (cell as! ChildrenDetailsFor101TableViewCell).setDisplayData(workerChild: GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[cell.tag])
                //                    }
                //                }
                //                else
                //                {
                //                    (cell as! ChildrenDetailsFor101TableViewCell).setDisplayDataNull()
                //                }
                return cell*/
            case 1://פרטים על הכנסותי ממעביד זה
                let cell = tableView.dequeueReusableCell(withIdentifier: "MoreSalaryTableViewCell", for: indexPath as IndexPath)
                (cell as! MoreSalaryTableViewCell).viewBottomSeperator.isHidden = false
                cell.selectionStyle = .none
                (cell as! MoreSalaryTableViewCell).delegateTextField = self
                
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! MoreSalaryTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                 (cell as! MoreSalaryTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                (cell as! MoreSalaryTableViewCell).setDisplayData()
                
                return cell
            case 2://לחצו על אני מבקש פטור או זכוי ממס
                
                var numParentBefore = 0
                //עובר על המערך של הסלים המוצגים ומונה את מספר האבות הפתוחים
                for i in 0..<indexPath.row
                {
                    if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[i] == 1//אבא
                    {
                        if i != GlobalEmployeeDetails.sharedInstance.arrReasonsBool.count - 2//כדי שלא יקרוס במקום האחרון שאין לו אחד אחריו
                        {
                            if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[i + 1] == 2
                            {
                                numParentBefore += 1
                            }
                        }
                    }
                }
                
                if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[indexPath.row] == 2//בן
                {
                    //כדי להחזיר את הבן הנכון ממילון הבנים,צריך את אינדקס האב שחושב ע״י מיקם האינדקס הנוכחי פחות מספר הסיבות(אבות) הפתוחים
                    rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                    GlobalEmployeeDetails.sharedInstance.arrReasonsPossision[GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_Kods[indexPath.row - numParentBefore]] = rectOfCellInTableView.origin.y
                    return GlobalEmployeeDetails.sharedInstance.dicReasonsCell[indexPath.row - numParentBefore]!
                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReasonTableViewCell", for: indexPath as IndexPath)
                (cell as! ReasonTableViewCell).setDisplayData(txtReason: GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit[indexPath.row])
                (cell as! ReasonTableViewCell).btnSelectReason.isChecked = convertIntArrToBool(int: GlobalEmployeeDetails.sharedInstance.arrReasonsBool[indexPath.row])
                
                cell.tag = indexPath.row
                (cell as! ReasonTableViewCell).tagParent = indexPath.row - numParentBefore
                (cell as! ReasonTableViewCell).section = indexPath.section
                
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit[indexPath.row]] = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                
                GlobalEmployeeDetails.sharedInstance.arrReasonsForScroll[GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit[indexPath.row]] = rectOfCellInTableView.origin.y
                
                cell.selectionStyle = .none
                setReasonError(cell: cell as! ReasonTableViewCell)
                return cell
            case 3://אני מבקש תיאום מס מכיוון שיש לי הכנסות נוספות
                //סל של מעסיקים
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReasonForTaxCoordinationTableViewCell", for: indexPath as IndexPath)
                cell.selectionStyle = .none
                cell.tag = indexPath.section
                
                (cell as! ReasonForTaxCoordinationTableViewCell).delegateTextField = self
                
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! ReasonForTaxCoordinationTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                 (cell as! ReasonForTaxCoordinationTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
//                (cell as! ReasonForTaxCoordinationTableViewCell).delegateScrollView = self
                
                (cell as! ReasonForTaxCoordinationTableViewCell).tagRow = indexPath.row - 1
                (cell as! ReasonForTaxCoordinationTableViewCell).setDisplayData()
                
                //---
//                (cell as! ReasonForTaxCoordinationTableViewCell).uploadFilesDelegate = self
//                self.uploadFilesDelegate2 = (cell as! ReasonForTaxCoordinationTableViewCell)
                //---
                
                return cell
            //הצגת pop up
            default://אני מבקש תיאום מס לפי אישור פקיד שומה
                let cell = tableView.dequeueReusableCell(withIdentifier: "PersonaldetailsTableViewCell", for: indexPath as IndexPath)
                
                rectOfCellInTableView = tableView.rectForRow(at: indexPath)
                (cell as! PersonaldetailsTableViewCell).cellOriginY = tableView.convert(rectOfCellInTableView, to: extTbl.superview).origin.y
                (cell as! PersonaldetailsTableViewCell).cellOriginY = rectOfCellInTableView.origin.y
                
                (cell as! PersonaldetailsTableViewCell).openDatePickerDelegate = self
                (cell as! PersonaldetailsTableViewCell).setDisplayData()
                cell.tag = indexPath.section
                //---
//                (cell as! PersonaldetailsTableViewCell).uploadFilesDelegate = self
//                self.uploadFilesDelegate2 = (cell as! PersonaldetailsTableViewCell)
                //---
                cell.selectionStyle = .none
                setPersonalDetailsError(cell:cell as! PersonaldetailsTableViewCell)
//                let tblFrame = (cell as! PersonaldetailsTableViewCell).tblCities.frame
//                conTopTblCities.constant = tblFrame.origin.y + rectOfCellInTableView.origin.y - extTbl.contentOffset.y
//                tblCity.frame = CGRect(origin: CGPoint(x:tblFrame.origin.x,y:tblFrame.origin.y + rectOfCellInTableView.origin.y), size: tblFrame.size)
//                (cell as! PersonaldetailsTableViewCell).txtCity.addTarget(self, action: #selector(EmployeeDetailsViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
                return cell
            }
        }
        }
    }
    
    @nonobjc func tableView(_ tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == extTbl
        {
            switch (indexPath.row)
            {
            case 0:
                switch (indexPath.section)
                {
                case 0:
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 21
                    //if GlobalEmployeeDetails.sharedInstance.familyStatus == FamilyStatusType.Separated.rawValue
                    {
                        return view.frame.size.height * (635/750)
                    }
                    return view.frame.size.height * (550/750)
                    
                case 1://פרטים על הכנסותי ממעביד זה
                    return view.frame.size.height * (410/750)
                case 2:
                    
                    if GlobalEmployeeDetails.sharedInstance.selectRowsForSection[indexPath.section] == false
                    {
                        return view.frame.size.height * (75/750)
                    }
                    return view.frame.size.height * (80/750)
                    
                case 3:
                    if GlobalEmployeeDetails.sharedInstance.numSectionsInTbl == 4
                    {
                        return view.frame.size.height * (/*500*/550/750)/*(400/750)*/
                    }
                    return view.frame.size.height * (70/750)
                case 5:
                    return view.frame.size.height * (/*500*/550/750)/*(400/750)*/
                default:
                    return view.frame.size.height * (70/750)
                }
            default://אם לחצו עליו
                switch indexPath.section
                {
                case 0://פרטים אישיים
                    //אין ילדים ואין בן זוג
                   
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count == 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
                    {
                        return view.frame.size.height * (120/750)//הצגת הסל של הוסף ילדים
                    }
                    //יש רק ילדים ולא בן זוג
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count > 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2
                    {
                        if indexPath.row < GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                        {
                            //return view.frame.size.height * (405/750)//הצגת הסל של הילדים
                            return view.frame.size.height * (285/750)//הצגת הסל של הילדים
                        }
                        else
                        {
                            var showWithButton = false
                            for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
                            {
                                if item.bWithMe == true
                                {
                                    showWithButton = true
                                    break
                                }
                            }
                            if showWithButton == true
                            {
                                return view.frame.size.height * (/*180*/192/750)//סל של הוספת ילדים
                            }
                            else
                            {
                                return view.frame.size.height * (120/750)//סל של הוספת ילדים
                            }
                        }
                    }
                    //יש רק בן זוג בלי ילדים
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count == 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2
                    {
                        
                        if indexPath.row == GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                        {
                           return view.frame.size.height * (120/750)//הצגת הסל של הוסף ילדים תחילה
                        }
                        //הצגת הסל של הבן זוג
                        if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true//אם נשוי/אה ולבן הזוג יש הכנסות
                        {
                            return view.frame.size.height * (480/750)
                        }
                        return view.frame.size.height * (320/750)//אם נשוי/אה ולבן הזוג אין הכנסות
                    }
                    //אם יש בן זוג ויש ילדים
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count > 0 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2
                    {
                        
                        if indexPath.row < GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                        {
                           return view.frame.size.height * (285/750)//הצגת הסל של הילדים תחילה
                        }
                       
                        if indexPath.row == GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
                        {
                            return view.frame.size.height * (120/750) //הצגת הסל של הוסף ילדים
                        }
                        //הצגת הסל של הבן זוג
                        if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true//אם נשוי/אה ולבן הזוג יש הכנסות
                        {
                            return view.frame.size.height * (480/750)
                        }
                        return view.frame.size.height * (320/750)//אם נשוי/אה ולבן הזוג אין הכנסות
                    }
                    return view.frame.size.height * (120/750)//סל של הוסף ילדים
                    
                    
                /*case 0:
                    //פרטים על בן או בת זוג
                    if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true && indexPath.row == 1//אם נשוי/אה ולבן הזוג יש הכנסות
                    {
                        return view.frame.size.height * (480/750)
                    }
                    else if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == false && indexPath.row == 1 && GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2///אם נשוי/אה ולבן הזוג אין הכנסות
                    {
                        return view.frame.size.height * (320/750)
                    }
                    else if indexPath.row != 0//ילדים
                    {
                        if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType != 2//האם ההורה לא נשוי
                        {
                            return view.frame.size.height * (405/750)
                        }
                        return view.frame.size.height * (285/750)
                    }
                    return view.frame.size.height * (320/750)*/
                case 1:
                    //בחר :יש לי הכנסות נוספות
                    return view.frame.size.height * (250/750)
                case 2:
                    var numParentBefore = 0
                    for i in 0..<indexPath.row
                    {
                        if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[i] == 1
                        {
                            if i != GlobalEmployeeDetails.sharedInstance.arrReasonsBool.count - 2//כדי שלא יקרוס במקום האחרון שאין לו אחד אחריו
                            {
                                if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[i + 1] == 2
                                {
                                    numParentBefore += 1
                                }
                            }
                        }
                    }
                    if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[indexPath.row] == 2//בן
                    {
                        return CGFloat(GlobalEmployeeDetails.sharedInstance.dicHeightForRowOpened[indexPath.row - numParentBefore]!)
                    }
                    return CGFloat(GlobalEmployeeDetails.sharedInstance.arrHeightForRowBase[indexPath.row - numParentBefore])
                // return view.frame.size.height * ((100/750) * 2)
                case 3:
                    return view.frame.size.height * (/*320*/370/750)
                case 5:
                    return view.frame.size.height * (/*500*/550/750)/*(400/750)*/
                default:
                    return view.frame.size.height * (70/750)
                }
            }
        }
//        return Global.sharedInstance.personalInformationModelHeight * (50/750)
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == extTbl{
        if section == 0{
            return 335
        }
        return 1.0
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == extTbl{
        return 1.0
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == extTbl{
        if section == 0{
            return viewHeader
        }
        return UIView(frame: CGRect.zero)
        }else{
            return UIView(frame: CGRect.zero)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect.zero)
    }
    
    func setPersonalDetailsError(cell:PersonaldetailsTableViewCell){
        setTxtFError(textField: cell.txtFirstName, isError: errorArray["nvFirstName"]!, cellOriginY: cell.cellOriginY)
        setTxtFError(textField: cell.txtLastName, isError: errorArray["nvLastName"]!, cellOriginY: cell.cellOriginY)
        setTxtFError(textField: cell.txtTzPassport, isError: errorArray["nvIdentityNumber"]!, cellOriginY: cell.cellOriginY)
        setTxtFError(textField: cell.txtDateBorn, isError: errorArray["dBirthDate"]!, cellOriginY: cell.cellOriginY)
        setTxtFError(textField: cell.txtCity, isError: errorArray["iCityId"]!, cellOriginY: cell.cellOriginY)
        setTxtFError(textField: cell.txtAddress, isError: errorArray["nvStreet"]!, cellOriginY: cell.cellOriginY)
        setTxtFError(textField: cell.txtNum, isError: errorArray["nvHouseNumber"]!, cellOriginY: cell.cellOriginY)
        setTxtFError(textField: cell.txtPhone, isError: errorArray["nvPhone"]!, cellOriginY: cell.cellOriginY)
    }
    
    func setAddChildError(cell:ChildrenDetailsFor101TableViewCell){
        setLableError(lable: cell.lblErrorChildren, isError: errorArray["lWorkerChilds"]!, cellOriginY: cell.cellOriginY + 350)
    }
    
    func setChieldError(cell:ChildrenDetailsFor101TableViewCell,childIndex:Int){
        if let dic = errorChildrenArray[childIndex-1]{
            if dic["nvIdentityNumber"] != nil{
                setTxtFError(textField: cell.txtTz, isError: true, cellOriginY: cell.cellOriginY - cell.txtTz.layer.position.y)
            }else{
                setTxtFError(textField: cell.txtTz, isError: false, cellOriginY: cell.cellOriginY - cell.txtTz.layer.position.y)
            }
            if dic["nvName"] != nil{
                setTxtFError(textField: cell.txtChildName, isError: true, cellOriginY: cell.cellOriginY - cell.txtChildName.layer.position.y)
            }else{
                setTxtFError(textField: cell.txtChildName, isError: false, cellOriginY: cell.cellOriginY - cell.txtChildName.layer.position.y)
            }
        }else{
            setTxtFError(textField: cell.txtTz, isError: false, cellOriginY: cell.cellOriginY - cell.txtTz.layer.position.y)
            setTxtFError(textField: cell.txtChildName, isError: false, cellOriginY: cell.cellOriginY - cell.txtChildName.layer.position.y)
        }
    }
    
    func setPartnerDeatilsError(cell:PartnerDeatilsTableViewCell){
        setTxtFError(textField: cell.txtFNamePartner, isError: errorArray["nvSpouseFirstName"]!, cellOriginY: cell.cellOriginY - cell.txtFNamePartner.layer.position.y)
        setTxtFError(textField: cell.txtLNamePartner, isError: errorArray["nvSpouseLastName"]!, cellOriginY: cell.cellOriginY - cell.txtLNamePartner.layer.position.y)
        setTxtFError(textField: cell.txtDateBornPartner, isError: errorArray["dSpouseBirthDate"]!, cellOriginY: cell.cellOriginY - cell.txtDateBornPartner.layer.position.y)
        setTxtFError(textField: cell.txtTzPartner, isError: errorArray["nvSpouseIdentityNumber"]!, cellOriginY: cell.cellOriginY - cell.txtTzPartner.layer.position.y)
    }
    
    func setEarningsDetailsError(cell:EarningsDetailsTableViewCell){
       setLableError(lable: cell.lblErrorMoreSaleries, isError: errorArray["bHaveOtherIncome"]!, cellOriginY: cell.cellOriginY+550)
    }
    
    func exemptionTaxCreditError(cell:ExemptionTaxCreditTableViewCell){
        setLableError(lable: cell.lblErrorReason, isError: errorArray["reason"]!, cellOriginY: cell.cellOriginY + 700)
    }
    
    func setWorkerTaxCoordinations(cell:Default101TableViewCell){
        setLableError(lable: cell.lblError, isError: errorArray["lWorkerTaxCoordinations"]!, cellOriginY: cell.cellOriginY+550)
    }
    
    func setReasonError(cell:ReasonTableViewCell){
        
        let reasonId  = GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_Kods[cell.tagParent]
        setLableError(lable: cell.lblError, isError: errorArray[String(reasonId)]!, cellOriginY: cell.cellOriginY)

    }
    
    func setTaxConError(cell:Default101TableViewCell){
        setLableError(lable: cell.lblError, isError: errorArray["mTillAmount"]!, cellOriginY: cell.cellOriginY)
        setLableError(lable: cell.lblError, isError: errorArray["fDeductedPercent"]!, cellOriginY: cell.cellOriginY)
        setLableError(lable: cell.lblError, isError: errorArray["fBeyondTaxDeducted"]!, cellOriginY: cell.cellOriginY)
        setLableError(lable: cell.lblError, isError: errorArray["fileObj"]!, cellOriginY: cell.cellOriginY)
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
     if tableView == extTbl
     {
     
     switch (indexPath.section)
     {
     case 0:
     if indexPath.row == 0
     {
     if GlobalEmployeeDetails.sharedInstance.familyStatus == FamilyStatusType.Separated.rawValue
     {
     return view.frame.size.height * ((710/750) * 2)
     }
     return view.frame.size.height * ((625/750) * 2)
     }
     //פרטים על בן או בת זוג
     if GlobalEmployeeDetails.sharedInstance.incomePartnerSelected == true
     {
     return view.frame.size.height * ((480/750) * 2)
     }
     return view.frame.size.height * ((320/750) * 2)
     case 1://פרטים על הכנסותי ממעביד זה
     if indexPath.row == 0
     {
     return view.frame.size.height * ((320/750) * 2)
     }
     //בחר :יש לי הכנסות נוספות
     return view.frame.size.height * ((150/750) * 2)
     case 2:
     if indexPath.row == 0
     {
     if GlobalEmployeeDetails.sharedInstance.selectRowsForSection[indexPath.section] == false
     {
     return view.frame.size.height * ((70/750) * 2)
     }
     return view.frame.size.height * ((80/750) * 2)
     }
     return view.frame.size.height * ((100/750) * 2)
     case 3:
     if GlobalEmployeeDetails.sharedInstance.numSectionsInTbl == 4
     {
     return view.frame.size.height * ((400/750) * 2)
     }
     if indexPath.row != 0
     {
     return view.frame.size.height * ((320/750) * 2)
     }
     return view.frame.size.height * ((70/750) * 2)
     case 5:
     return view.frame.size.height * ((400/750) * 2)
     default:
     return view.frame.size.height * ((70/750) * 2)
     }
     
     }
     return Global.sharedInstance.personalInformationModelHeight * (50/750)
     }*/
    
    //MARK: - dismissKeyBoard
    func dismissKeyBoard()
    {
        self.view.endEditing(true)
    }
    
    func convertIntArrToBool(int:Int)->Bool
    {
        if int == 1
        {
            return true
        }
        return false
    }
    
//    func updateScrollviewHeight(num:Float)
//    {
//        scrollView.contentSize.height += CGFloat(num)
//    }
    
    func openFileBrowser()
    {
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let fileBrowser = FileBrowser(initialPath: documentsUrl)
        self.present(fileBrowser, animated: true, completion: nil)
    }
    
    func OpenDatePicker(viewCon:UITableViewCell,tag:Int,dateToShow:String)
    {
        dismissKeyBoard()
        
        let datePickerPopUp: DatePickerPopUpViewController = storyboard?.instantiateViewController(withIdentifier:"DatePickerPopUpViewController")as! DatePickerPopUpViewController
        
        if tag == 1 || tag == 2//personal Details
        {
            datePickerPopUp.setDateTextFieldTextDelegate = viewCon as! PersonaldetailsTableViewCell
            datePickerPopUp.viewCon = viewCon as! PersonaldetailsTableViewCell
        }
        else if tag == 3 || tag == 4//partner Details
        {
            datePickerPopUp.setDateTextFieldTextDelegate = viewCon as! PartnerDeatilsTableViewCell
            datePickerPopUp.viewCon = viewCon as! PartnerDeatilsTableViewCell
        }
        else if tag == 5//Child details
        {
            datePickerPopUp.setDateTextFieldTextDelegate = viewCon as! ChildrenDetailsFor101TableViewCell
            datePickerPopUp.viewCon = viewCon as! ChildrenDetailsFor101TableViewCell
        }
        else if tag == 6//new immegrant - fromDate
        {
            datePickerPopUp.setDateTextFieldTextDelegate = viewCon as! Reason101NewImmegrantTableViewCell
            datePickerPopUp.viewCon = viewCon as! Reason101NewImmegrantTableViewCell
        }
        else if tag == 7//recident Place - fromDate
        {
            datePickerPopUp.setDateTextFieldTextDelegate = viewCon as! Reason101ResidentPlaceTableViewCell
            datePickerPopUp.viewCon = viewCon as! Reason101ResidentPlaceTableViewCell
        }
        else if tag == 8 || tag == 9 // soldier - from / to Date
        {
            datePickerPopUp.setDateTextFieldTextDelegate = viewCon as! Reason101SoldierTableViewCell
            datePickerPopUp.viewCon = viewCon as! Reason101SoldierTableViewCell
        }
            
        else
        {
            datePickerPopUp.setDateTextFieldTextDelegate = viewCon as! PersonaldetailsTableViewCell
            datePickerPopUp.viewCon = viewCon as! PersonaldetailsTableViewCell
        }
        
        datePickerPopUp.tag = tag
        datePickerPopUp.dateDefault = dateToShow
        datePickerPopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
//        GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.view.present(datePickerPopUp)
//        self = GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController
        if employeeDetails_SupperView_Delegate != nil{
        employeeDetails_SupperView_Delegate.openDatePicker1(datePickerPopUpViewController: datePickerPopUp)
        }
//        self.present(datePickerPopUp, animated: true, completion: nil)
        
    }
    //MARK: - scrollView
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print(scrollView.contentOffset.y)
        
        if extTbl.contentOffset.y > 185{
            if conTopViewMenuSmall.constant < 0{
                conTopViewMenuSmall.constant = 0
                conTopContainerView.constant = 0
            }
        }else{
            if conTopViewMenuSmall.constant == 0{
                conTopViewMenuSmall.constant = -150
                conTopContainerView.constant = -150
            }
        }
        
//        self.scrollView.contentSize.height = (self.view.frame.size.height * 2.54) + GlobalEmployeeDetails.sharedInstance.howManyToScroll + GlobalEmployeeDetails.sharedInstance.howManyToScroll2
//                self.scrollView.contentSize.height = (self.view.frame.size.height * 2.7) + GlobalEmployeeDetails.sharedInstance.howManyToScroll + GlobalEmployeeDetails.sharedInstance.howManyToScroll2
        
    }
    
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
//        self.lastContentOffset = scrollView.contentOffset.y
//    }
    
    //delegate to update the scrollView size on deleting employer.
//    func scrollToBottom()
//    {
////        self.scrollView.contentSize.height = (self.view.frame.size.height * 2.54) + GlobalEmployeeDetails.sharedInstance.howManyToScroll + GlobalEmployeeDetails.sharedInstance.howManyToScroll2
////                self.scrollView.contentSize.height = (self.view.frame.size.height * 2.7) + GlobalEmployeeDetails.sharedInstance.howManyToScroll + GlobalEmployeeDetails.sharedInstance.howManyToScroll2
//    }
    
    //MARK: - keyBoard notifications
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        if didFirstDelegate{
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (userInfo.object(forKey: UIKeyboardFrameBeginUserInfoKey)! as AnyObject).cgRectValue.size
//        let contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
//        scrollView.contentInset = contentInsets
//        scrollView.scrollIndicatorInsets = contentInsets
        
//        let scrollPoint = CGPoint(x: 0, y:txtSelected.frame.origin.y /*+ keyboardSize.height*/ + cellOriginY/* - 300*/)/*+ abs(scrollView.contentOffset.y - lastContentOffset))*/
//        scrollView.setContentOffset(scrollPoint, animated: true)
         
//            print("cellOriginY:  \(self.cellOriginY)\ntextfield height:  \(self.txtSelected.frame.height)\nto y:  \(self.txtSelected.frame.origin.y + self.cellOriginY - self.screenSize.height + keyboardSize.height + self.txtSelected.frame.height)\n\n")
            DispatchQueue.main.async {
            let scrollPoint = CGPoint(x: 0, y:self.txtSelected.frame.origin.y + self.cellOriginY - self.screenSize.height + keyboardSize.height + self.txtSelected.frame.height)
           self.extTbl.setContentOffset(scrollPoint, animated: true)
            }
            self.didFirstDelegate = false
        }
        
//        var aRect : CGRect = self.view.frame
//        aRect.size.height -= keyboardSize!.height
//        if let activeFieldPresent = sFax
//        {
//            if (!CGRectContainsPoint(aRect, sFax!.frame.origin))
//            {
//                self.scrollView.scrollRectToVisible(sFax!.frame, animated: true)
//            }
//        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
//        scrollView.contentInset = UIEdgeInsets.zero
//        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func getTextField(textField:UITextField,originY:CGFloat)
    {
        didFirstDelegate = true
        print("cellOriginY:  \(originY)\ntextfield height:  \(textField.frame.height)")
        txtSelected = textField
        cellOriginY = originY
    }
    
    
    //MARK: - set red border for errors
    func setTxtFRedBorder(errorFields:NSArray,isError:Bool){
        firstTextFieldError = nil
        firstCellError_Y = -1
//        let personaldetailsCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 0)) as! PersonaldetailsTableViewCell//פרטים אישיים
//        let childNumRow = GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count + 1
//        let addChildCell = extTbl.cellForRow(at: IndexPath(row: childNumRow, section: 0)) as! AddChildTableViewCell//הוסף ילד

        //הקצאת שאר הסלים בתוך הסווטש אחרת היו קריסות על סלים שלא נוצרו...
        for field in errorFields{
            if let fieldName = field as? String{
                errorArray[fieldName] = isError
                /*
                switch fieldName {
                case "nvFirstName":
                    setTxtFError(textField: personaldetailsCell.txtFirstName, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtFirstName.layer.position.y)
                    break
                case "nvLastName":
                    setTxtFError(textField: personaldetailsCell.txtLastName, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtLastName.layer.position.y)
                    break
                case "nvIdentityNumber":
                    setTxtFError(textField: personaldetailsCell.txtTzPassport, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtTzPassport.layer.position.y)
                    break
                case "dBirthDate":
                    setTxtFError(textField: personaldetailsCell.txtDateBorn, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtDateBorn.layer.position.y)
                    break
                case "iCityId":
                    setTxtFError(textField: personaldetailsCell.txtCity, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtCity.layer.position.y)
                    break
                case "nvStreet":
                    setTxtFError(textField: personaldetailsCell.txtAddress, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtAddress.layer.position.y)
                    break
                case "nvHouseNumber":
                    setTxtFError(textField: personaldetailsCell.txtNum, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtNum.layer.position.y)
                    break
                case "nvPhone":
                    setTxtFError(textField: personaldetailsCell.txtPhone, isError: isError,cellOriginY: personaldetailsCell.cellOriginY - personaldetailsCell.txtPhone.layer.position.y)
                    break
                ///lWorkerChilds
                case "lWorkerChilds":
                    setLableError(lable: addChildCell.lblErrorChild, isError: isError, cellOriginY: addChildCell.cellOriginY + 350)
                    //                    personaldetailsCell.lblErrorChild.isHidden = !isError
                    
                    break

                ///workerSpouse
                case "nvSpouseFirstName":
                    let partnerDeatilsCell = extTbl.cellForRow(at: IndexPath(row: childNumRow+1, section: 0)) as! PartnerDeatilsTableViewCell//בן/בת זוג
                    setTxtFError(textField: partnerDeatilsCell.txtFNamePartner, isError: isError,cellOriginY: partnerDeatilsCell.cellOriginY - partnerDeatilsCell.txtFNamePartner.layer.position.y)
                    break
                case "nvSpouseLastName":
                    let partnerDeatilsCell = extTbl.cellForRow(at: IndexPath(row: childNumRow+1, section: 0)) as! PartnerDeatilsTableViewCell//בן/בת זוג
                    setTxtFError(textField: partnerDeatilsCell.txtLNamePartner, isError: isError,cellOriginY: partnerDeatilsCell.cellOriginY - partnerDeatilsCell.txtLNamePartner.layer.position.y)
                    break
                case "dSpouseBirthDate":
                    let partnerDeatilsCell = extTbl.cellForRow(at: IndexPath(row: childNumRow+1, section: 0)) as! PartnerDeatilsTableViewCell//בן/בת זוג
                    setTxtFError(textField: partnerDeatilsCell.txtDateBornPartner, isError: isError,cellOriginY: partnerDeatilsCell.cellOriginY - partnerDeatilsCell.txtDateBornPartner.layer.position.y)
                    break
                case "nvSpouseIdentityNumber":
                    let partnerDeatilsCell = extTbl.cellForRow(at: IndexPath(row: childNumRow+1, section: 0)) as! PartnerDeatilsTableViewCell//בן/בת זוג
                    setTxtFError(textField: partnerDeatilsCell.txtTzPartner, isError: isError,cellOriginY: partnerDeatilsCell.cellOriginY - partnerDeatilsCell.txtTzPartner.layer.position.y)
                    break
                    //iGenderType
                    //                case "iGenderType"://זכר/נקבה
                    //                setMyError(errGender, b);
                //                break
                case "bHaveOtherIncome"://הכנסות נוספות ללא בחירת סוג הכנסה
                    let earningsDetailsCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 1)) as! EarningsDetailsTableViewCell//הכנסות נוספות
                    setLableError(lable: earningsDetailsCell.lblErrorMoreSaleries, isError: isError, cellOriginY: earningsDetailsCell.cellOriginY+550)
                    break
                case "lWorkerTaxCoordinations"://שגיאה במעסיקים
                    let workerTaxCoordinationsCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 3)) as! Default101TableViewCell//מעסיקים
                    setLableError(lable: workerTaxCoordinationsCell.lblError, isError: isError, cellOriginY: workerTaxCoordinationsCell.cellOriginY+550)
                    break
                //שגיאה באחד משדות הפופא מהצקבוקס האחרון
                case "mTillAmount":
                    let taxConCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 4)) as! Default101TableViewCell//פואפ
                    setLableError(lable: taxConCell.lblError, isError: isError, cellOriginY: taxConCell.cellOriginY)
                    break
                case "fDeductedPercent":
                    let taxConCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 4)) as! Default101TableViewCell//פואפ
                    setLableError(lable: taxConCell.lblError, isError: isError, cellOriginY: taxConCell.cellOriginY)
                    break
                case "fBeyondTaxDeducted":
                    let taxConCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 4)) as! Default101TableViewCell//פואפ
                    setLableError(lable: taxConCell.lblError, isError: isError, cellOriginY: taxConCell.cellOriginY)
                    break
                case "fileObj":
                    let taxConCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 4)) as! Default101TableViewCell//פואפ
                    setLableError(lable: taxConCell.lblError, isError: isError, cellOriginY: taxConCell.cellOriginY)
                    break
                default:
                    break
                }
 */
            }
        }
//        extTbl.reloadData()
        if isError{
            if firstTextFieldError != nil{
                firstTextFieldError?.becomeFirstResponder()
            }
//        if firstCellError_Y != -1{
//            if firstTextFieldError != nil{
//                firstTextFieldError?.becomeFirstResponder()
//            }
//            scrollView.setContentOffset(CGPoint(x: 0, y: firstCellError_Y), animated: true)
//            extTbl.contentOffset.y = firstCellError_Y
//        }
        }
        
    }
    
    func scrollToRow(row:Int,section:Int,possion:String) {
        let indexPath = IndexPath(row: row, section: section)
        self.extTbl.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func setTxtFError(textField:UITextField,isError:Bool,cellOriginY:CGFloat){
        if isError{
            textField.layer.borderColor = UIColor.red.cgColor
            textField.layer.borderWidth = 1
            if firstCellError_Y == -1{
                firstTextFieldError = textField
                firstCellError_Y = cellOriginY - textField.layer.position.y
            }else{
                if cellOriginY < firstCellError_Y{
                    firstTextFieldError = textField
                    firstCellError_Y = cellOriginY - textField.layer.position.y
                }
            }
        }else{
            textField.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func setLableError(lable:UILabel,isError:Bool,cellOriginY:CGFloat){
        lable.isHidden = !isError
        if isError{
            if firstCellError_Y == -1{
                firstCellError_Y = cellOriginY
                firstTextFieldError = nil
            }else{
                if cellOriginY < firstCellError_Y{
                    firstCellError_Y = cellOriginY
                    firstTextFieldError = nil
                }
            }
        }
    }

    
    func setReasonTxtFRedBorder(errorReason:NSArray,isError:Bool){
        if errorReason.count > 0{
            errorArray["reason"] = isError
            for reason in errorReason{
                if let reasonArr = reason as? NSDictionary{
                if let reasonId = reasonArr["iTaxRelifeReasonId"] as? Int{
                    errorArray[String(reasonId)] = isError
                }
                }
            }
            
//            let exemptionTaxCreditCell = extTbl.cellForRow(at: IndexPath(row: 0, section: 2)) as! ExemptionTaxCreditTableViewCell//שגיאה בסיבות
//            exemptionTaxCreditCell.lblErrorReason.isHidden = !isError
//            if isError{
//            if firstCellError_Y == -1{
//                firstCellError_Y = exemptionTaxCreditCell.cellOriginY + 700
//                firstTextFieldError = nil
//            }else{
//                if exemptionTaxCreditCell.cellOriginY < firstCellError_Y{
//                    firstCellError_Y = exemptionTaxCreditCell.cellOriginY + 700
//                    firstTextFieldError = nil
//                }
//            }
//            
//                if firstCellError_Y != -1{
//                    if firstTextFieldError != nil{
//                        firstTextFieldError?.becomeFirstResponder()
//                    }
//                    scrollView.setContentOffset(CGPoint(x: 0, y: firstCellError_Y), animated: true)
//                    
//                }
//            }
        }
    }
    
    //MARK: - ChangeSubViewProfileDelegate
    func changeSubViewProfile(tagNewView:Int){
        fromContainerDelegate.changeSubViewProfile(tagNewView: tagNewView)
    }
    
    func tapBack(){
        fromContainerDelegate.tapBack()
    }
    
    //MARK: - TextField
//    func textFieldDidChange(_ textField: UITextField) {
//        searching(searchText: textField.text!)
//    }
//    
//    func searching(searchText: String) {
//        //let s:CGFloat = view.frame.size.height - tableView.frame.origin.x
//        
//        if searchText == ""
//        {
//            //            filtered =  pointsArr
//            //print("count of list in searchin \(vertexsArrayDescription.count)")
//            
//            citiesFilter =  citiesName
//        }
//        else{
//            citiesFilter = citiesFilter.filter{ term in
//                return term.hasPrefix(searchText)
//            }
//        }
//        if(citiesFilter.count == 0){
//            self.tblCity.isHidden = true
//            citiesName.sort{$0.compare($1) == .orderedAscending }
//            //            citiesName.sort{$0.compare($1) == .orderedDescending }
//        }
//        else{
//            self.tblCity.isHidden = false
//        }
//        tblCity.reloadData()
//        //        tabelPoints.reloadData()
//        //        uploadTable()
//        
//    }
//
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "101FormSegue" {
            let connectContainerViewController = segue.destination as! PersonalInformationModelViewController
            connectContainerViewController.containerDelegate = self
            connectContainerViewController.subviewTag = 0
//            containerDelegate = connectContainerViewController
            //            containerViewController = connectContainerViewController
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    //MARK: - UploadFilesDelegate
    var uploadFilesDelegate : UploadFilesDelegate! = nil
    var uploadFilesDelegate2 : UploadFilesDelegate! = nil
    
    func uploadFile(fileUrl: String) {
        uploadFilesDelegate.uploadFile!(fileUrl: fileUrl)
    }
    
    func getFile(imgName: String, img: String, displayImg: UIImage?) {
        if isUserImage {
            isUserImage = false
            setImageFromFile(imgName: imgName, img: img, displayImg: displayImg)
        } else {
            uploadFilesDelegate2.getFile!(imgName: imgName, img: img, displayImg: displayImg)
        }
    }

}
