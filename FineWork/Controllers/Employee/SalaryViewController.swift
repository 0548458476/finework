//
//  SalaryViewController.swift
//  FineWork
//
//  Created by Tamy wexelbom on 16.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SalaryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,SaveWorkerDelegate,UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    //MARK: -- Variables
    var cellHeader : ProfileHeaderTableViewCell = ProfileHeaderTableViewCell()
    var cell : TitleTableViewCell = TitleTableViewCell()
    var txtSelected : UITextField = UITextField()
    var cellOriginY : CGFloat = 0.0
    let screenSize = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        tableView.allowsSelection = false
        self.tableView.separatorStyle = .none

        cellHeader = tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTableViewCell") as! ProfileHeaderTableViewCell
        cell = tableView.dequeueReusableCell(withIdentifier: "TitleTableViewCell") as! TitleTableViewCell
        cell.saveDelegate = self
        cell.txtBranch.delegate = self
        cell.txtNumber.delegate = self
        cell.txtName.delegate = self
        
        cell.txtBranch.keyboardType = .numberPad
        cell.txtNumber.keyboardType = .numberPad

       print("bbb viewDidLoad")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("bbb viewDidAppear")
    }
    override func viewWillAppear(_ animated: Bool) {
        print("aaa viewWillAppear")

        
//        getWorker()
    }
    override func viewDidLayoutSubviews() {
        print("aaa viewDidLayoutSubviews")

    }
    override func viewWillLayoutSubviews() {
        print("aaa viewWillLayoutSubviews")

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        unregisterKeyboardNotifications()
    }
    
    //MARK: - TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        var cell : UITableViewCell = UITableViewCell()
//        var cell = self.tableView.dequeueReusableCell(withIdentifier: "TitleTableViewCell") as! TitleTableViewCell
        cellOriginY = tableView.rectForRow(at: indexPath).origin.y
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if section == 0 {
            return cellHeader.contentView
//        }
//        return UIView(frame: CGRect.zero)
        
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 0 {
            return 335
//        }
//        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1{
            return (500 - 130)
        }else if tableView.tag == 2{
            return (500 - 80)
        }
        return (500 - 130 - 80)
    }
    
    //MARK: - reload cell header according subview(btn , image)
    func reloadData() {
        cellHeader.reloadData(tagMenu: 1)
    }
    
    //MARK: --Server Function
    
    func getWorker() {
        registerKeyboardNotifications()
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        //            print("---ForgotPassword:\n\(dic)")
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String,AnyObject> {
                        //Alert.sharedInstance.showAlert(mess: "עבר בהצלחה!")
                        let worker = Worker()
                        Global.sharedInstance.worker = worker.getWorkerFromDictionary(dic: dicResult)
                        self.getWorkerCodeTables()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
               
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "GetWorker")
    }
    
    func updateWorker() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
        Global.sharedInstance.worker.workerBankAccountDetails.bBankAccountDetailsUpdated = true
        
        dic["worker"] = Global.sharedInstance.worker.getDictionaryFromWorker() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool/*Dictionary<String,AnyObject>*/ {
                        if dicResult {
                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                            Global.sharedInstance.bWereThereAnyChanges = false
                            Global.sharedInstance.worker.workerBankAccountDetails.bBankAccountDetailsUpdated = false
                            self.cellHeader.changeSubViewProfileDelegate.changeSubViewProfile(tagNewView: 2)
                        } else {
                            Alert.sharedInstance.showAlert(mess: "הפרטים לא נשמרו!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : "UpdateWorker")
    }
    
    func getWorkerCodeTables() {
        if Global.sharedInstance.workerCodeTables.count > 0{
            for item in Global.sharedInstance.workerCodeTables { // 1
                if item.Key == Global.sharedInstance.bankCodeTable {
                    cell.banksArr = item.Value
                }
            } // 1
            
            //            self.cellVisibleProfileHeader.setDisplayData(languages: self.languagesArray, workerLanguage: Global.sharedInstance.worker.lWorkerLanguages.count > 0 ? Global.sharedInstance.worker.lWorkerLanguages[0] : nil)
            //            self.visibleProfileTbl.reloadData()
            cell.tblBanks.reloadData()
            cell.setDisplayData()
        }else{
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
            var dic = Dictionary<String, AnyObject>()
            
            dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
            
            api.sharedInstance.goServer(params : dic,success : { // 8
                (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
                
                print(responseObject as! [String:AnyObject])
                print(responseObject as Any)
                var dic  =  responseObject as! [String:AnyObject]
                if let _ = dic["Error"]?["iErrorCode"] { // 7
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    
                    if dic["Error"]?["iErrorCode"] as! Int ==  0 { // 5
                        if let dicResult = dic["Result"] as? Array<Dictionary<String,AnyObject>> { // 3
                            
                            var workerCodeTables : Array<SysTableDictionary> = []
                            let sysTable = SysTableDictionary()
                            
                            for workerCodeTableDic in dicResult { // 0
                                workerCodeTables.append(sysTable.getSysTableDictionaryFromDic(dic: workerCodeTableDic))
                            } // 0
                            
                            Global.sharedInstance.workerCodeTables = workerCodeTables
                            for item in workerCodeTables { // 1
                                if item.Key == Global.sharedInstance.bankCodeTable {
                                    self.cell.banksArr = item.Value
                                }
                            }
                            self.cell.tblBanks.reloadData()
                            self.cell.setDisplayData()
                            
                        } else {
                            Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                        }
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                }
            } ,failure : {
                (AFHTTPRequestOperation, Error) -> Void in
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            }, funcName : "GetWorkerCodeTables") // 9
        }
    }
    
    //MARK: - SaveWorkerDelegate
    func saveWorkerFinal(){
        updateWorker()
    }
    
    //MARK: - TextFieldDelegste
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        txtSelected = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case cell.txtBranch:
            cell.txtNumber.becomeFirstResponder()
        case cell.txtNumber:
            cell.txtName.becomeFirstResponder()
        default:
            dismissKeyboard()
        }
        return false
        //        textField.resignFirstResponder()
        //        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        var limitLength : Int = newLength
        
        switch textField {
        case cell.txtBranch:
            limitLength = 5
            break
//        case cell.txtNumber:
//            limitLength = 10
//            break
        case cell.txtName:
            limitLength = 30
            break
        case cell.txtNumber:
            limitLength = 30
            break

        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
        
        return newLength <= limitLength // Bool
        
    }

    
    //MARK: - keyBoard notifications
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardDidShow(notification: NSNotification) {
        
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (userInfo.object(forKey: UIKeyboardFrameBeginUserInfoKey)! as AnyObject).cgRectValue.size
        
        DispatchQueue.main.async {
            var scrollPoint : CGPoint
//            if self.txtSelected.tag == 1 {
            
                let y1 = (self.txtSelected.frame.origin.y + (self.txtSelected.superview?.frame.origin.y)!)
                
                scrollPoint = CGPoint(x: 0, y: y1 + self.cellOriginY - self.screenSize.height + keyboardSize.height + self.txtSelected.frame.height)
//            } else {
//                scrollPoint = CGPoint(x: 0, y:self.txtSelected.frame.origin.y + self.cellOriginY - self.screenSize.height + keyboardSize.height + self.txtSelected.frame.height)
//            }
            self.tableView.setContentOffset(scrollPoint, animated: true)
        }
        
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        //        scrollView.contentInset = UIEdgeInsets.zero
        //        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        //characteristicsTxt.resignFirstResponder()
        //studiesTxt.resignFirstResponder()
        //jobExperienceTxt.resignFirstResponder()
        //matchingJobsTxt.resignFirstResponder()
        
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
