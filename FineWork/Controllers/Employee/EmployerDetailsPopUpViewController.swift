//
//  EmployerDetails.swift
//  FineWork
//
//  Created by User on 8.6.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmployerDetailsPopUpViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    //MARK: - Views
    
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var close_x: UILabel!
    
    
    @IBOutlet weak var imgEmployer: UIImageView!
    @IBOutlet weak var nameBussines: UILabel!
    @IBOutlet weak var rank: UILabel!
    
    @IBOutlet weak var rankArrowCircel: UIView!
    @IBOutlet weak var rankArrow: UILabel!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descraptionContent: UILabel!
    @IBOutlet weak var numberWorkerTitle: UILabel!
    @IBOutlet weak var numberWorkerContent: UILabel!
    @IBOutlet weak var mainActiveFieldTitle: UILabel!
    @IBOutlet weak var mainActiveField: UILabel!
    @IBOutlet weak var centerAreaTitle: UILabel!
    @IBOutlet weak var centerAreaContent: UILabel!
    @IBOutlet weak var siteLinkTitle: UILabel!
    @IBOutlet weak var siteLink: UILabel!
    
    @IBOutlet weak var siteLinkView: UIView!
    
    @IBOutlet weak var progress1: UIProgressView!
    @IBOutlet weak var progress2: UIProgressView!
    @IBOutlet weak var progress3: UIProgressView!
    @IBOutlet weak var progerss4: UIProgressView!
    
    @IBOutlet weak var progerss1Title: UILabel!
    @IBOutlet weak var progerss2Title: UILabel!
    
    @IBOutlet weak var progerss3Title: UILabel!
    
    @IBOutlet weak var progress4Title: UILabel!
    
    
    @IBOutlet weak var tableViewRank: UITableView!
    @IBOutlet weak var workerSay: UILabel!
    @IBOutlet weak var workerSayLine: UIView!

    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var rankHiddenConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var mainActiveTopConstarin: NSLayoutConstraint!
    
    @IBOutlet weak var centerEreaTopConstrain: NSLayoutConstraint!
    @IBOutlet weak var siteLinkwTopConstrain: NSLayoutConstraint!

    @IBOutlet weak var workerSayTopConstarin: NSLayoutConstraint!
    
    //MARK: Action

    //open and close rank details
    @IBAction func rankArrowOpen(_ sender: Any) {
        if WorkerOfferedJob != nil && WorkerOfferedJob.ranking != nil{
            if progress1.isHidden != true {
                hiddenAllProgerss(isHidden: false)
                rankHiddenConstrain.constant = 20
                setHeightToScroll(height: -250)

            }
            else{
                hiddenAllProgerss(isHidden: true)
                rankHiddenConstrain.constant = 242
                setHeightToScroll(height: 250)

            }
        }else {
            hiddenAllProgerss(isHidden: false)
        }
        
    }
    //close view controler
    @IBAction func close_x_btn(_ sender: Any) {
        var _ = self.navigationController?.popViewController(animated: true)
    }
    
    //open link in website
    @IBAction func openLinkBtn(_ sender: Any) {
         Global.sharedInstance.openUrl(url: WorkerOfferedJob.nvSiteLink)
    }
    

    //MARK Object
    var WorkerOfferedJob: WorkerOfferedJob!
    var height : CGFloat = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        height = baseView.frame.height

        
        
        close_x.text = FlatIcons.sharedInstance.x
        rankArrow.text = FlatIcons.sharedInstance.arrow_down
        scrollView.isScrollEnabled = true
        setDisplayData()
    }
    
    func setDisplayData(){
        
        imgEmployer.downloadedFrom(link: api.sharedInstance.buldUrlFile(fileName: WorkerOfferedJob.nvLogoImageFilePath) )
        
        nameBussines.text = WorkerOfferedJob.nvEmployerName
        descraptionContent.text = WorkerOfferedJob.nvBusinessDescription
        
        if WorkerOfferedJob.nvEmployeeNumberType.characters.count > 0
        {
            numberWorkerContent.text = WorkerOfferedJob.nvEmployeeNumberType
        }else{
            mainActiveTopConstarin.constant = -50
            numberWorkerContent.isHidden = true
            numberWorkerTitle.isHidden = true
            setHeightToScroll(height: -100)

        }
        
        if WorkerOfferedJob.nvMainActivityFieldType.characters.count > 0
        {
            mainActiveField.text = WorkerOfferedJob.nvMainActivityFieldType
        }else{
            centerEreaTopConstrain.constant = -50
            mainActiveField.isHidden = true
            mainActiveFieldTitle.isHidden = true
            setHeightToScroll(height: -100)

        }
        
        if WorkerOfferedJob.nvCentralAreaActivity.characters.count > 0
        {
            centerAreaContent.text = WorkerOfferedJob.nvCentralAreaActivity
        }else{
            siteLinkwTopConstrain.constant = -50
            centerAreaContent.isHidden = true
            centerAreaTitle.isHidden = true
            setHeightToScroll(height: -100)

        }
        
        if WorkerOfferedJob.nvSiteLink.characters.count > 0
        {
            siteLink.text = WorkerOfferedJob.nvSiteLink
        }else{
            workerSayTopConstarin.constant = -50
            //siteLink.isHidden = true
            siteLinkTitle.isHidden = true
            siteLinkView.isHidden = true
            setHeightToScroll(height: -100)

        }
        if WorkerOfferedJob.ranking != nil
        {
            if WorkerOfferedJob.ranking!.nRankingAvg > 0
            {
                let rankingAvg = (WorkerOfferedJob.ranking!.nRankingAvg).description
                let rankingstr = "דירוג"
                rank.text = "\(rankingstr) \(rankingAvg)"
            }
            else {
                rank.isHidden = true
                rankArrow.isHidden = true
                rankArrowCircel.isHidden = true
                
            }
            rankHiddenConstrain.constant = 20

            progress1.progress = Float(WorkerOfferedJob.ranking!.nRanking1Avg ) / Float(10)
            progress2.progress = Float(WorkerOfferedJob.ranking!.nRanking2Avg ) / Float(10)
            progress3.progress = Float(WorkerOfferedJob.ranking!.nRanking3Avg ) / Float(10)
            progerss4.progress = Float(WorkerOfferedJob.ranking!.nRanking4Avg ) / Float(10)
            
            progerss1Title.text = "אמינות" + " - \(WorkerOfferedJob.ranking!.nRanking1Avg)"
            progerss2Title.text = "הגינות" + " - \(WorkerOfferedJob.ranking!.nRanking2Avg)"
            progerss3Title.text = "תנאי עבודה" + " - \(WorkerOfferedJob.ranking!.nRanking3Avg)"
            progress4Title.text = "כללי" + " - \(WorkerOfferedJob.ranking!.nRanking4Avg)"
            
        }
        if WorkerOfferedJob.ranking != nil && WorkerOfferedJob.ranking!.lRankingComment != nil && WorkerOfferedJob.ranking!.lRankingComment!.count > 0
        {
            setRankWithImg()
            workerSay.text =  "\(workerSay.text!) \(WorkerOfferedJob.nvEmployerName)?"
        }else{
            workerSay.isHidden = true
            workerSayLine.isHidden = true
            tableViewRank.isHidden = true
            setHeightToScroll(height: -tableViewRank.frame.height)

        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {

        
        imgEmployer.layer.cornerRadius = imgEmployer.frame.height / 2
        imgEmployer.layer.masksToBounds = true
        imgEmployer.contentMode = UIViewContentMode.scaleAspectFill
        imgEmployer.clipsToBounds = true
        
        
        //circel buttens
        rankArrowCircel.layer.borderColor = ControlsDesign.sharedInstance.colorBlue.cgColor
        rankArrowCircel.layer.borderWidth = 1
        rankArrowCircel.layer.cornerRadius = rankArrowCircel.frame.height / 2
        
progress1.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        progress2.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        progress3.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        progerss4.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

    }
    
    func setDisplayData(index: Int){
        WorkerOfferedJob =  Global.sharedInstance.lWorkerOfferedJobs[index]
        print(WorkerOfferedJob.nvEmployerName)
        
        
        
    }
    
    func hiddenAllProgerss(isHidden : Bool){
        
        progress1.isHidden = !isHidden
        progress2.isHidden = !isHidden
        progress3.isHidden = !isHidden
        progerss4.isHidden = !isHidden
        
        progerss1Title.isHidden = !isHidden
        progerss2Title.isHidden = !isHidden
        progerss3Title.isHidden = !isHidden
        progress4Title.isHidden = !isHidden
        
        
    }
    //MARK: -TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.WorkerOfferedJob.ranking!.lRankingComment!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("aaa tableView \(indexPath.row)")
        
        let cell : RankingCellTableViewCell
        
        cell = tableView.dequeueReusableCell(withIdentifier: "RankingCellTableViewCell") as! RankingCellTableViewCell
        
        cell.textRang?.text = self.WorkerOfferedJob.ranking?.lRankingComment?[indexPath.row].nvRankingComment
        cell.imgRank?.image = self.WorkerOfferedJob.ranking!.lRankingComment![indexPath.row].imageView
        //        cel.del = self
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((self.view.frame.size.height * 0.77) - 3.5) / 7
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.001
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let headerView = UIView()
    //        headerView.backgroundColor = UIColor.lightGray
    //        return headerView
    //    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    //MARK: Get Images
    
    
    var counter = 0
    var imageMap = Array<String>()
    var downloadsCounter = 0
    
    
    func setRankWithImg() {
        print("aaa setRonkWithImg")
        counter = 0
        downloadsCounter = 0
        imageMap.removeAll()
        
        for index in stride(from: 0, to: WorkerOfferedJob.ranking!.lRankingComment!.count, by: 1) {
            
            var isExist = false
            for i in stride(from: 0, to: imageMap.count, by: 1) {
                if WorkerOfferedJob.ranking!.lRankingComment?[index].nvImage  == imageMap[i] {
                    isExist = true
                    //                            index2 = i
                    break
                }
            }
            if !isExist {
                imageMap.insert((WorkerOfferedJob.ranking?.lRankingComment?[index].nvImage)!, at: imageMap.count)
                counter += 1
                var stringPath = api.sharedInstance.buldUrlFile(fileName: (WorkerOfferedJob.ranking!.lRankingComment?[index].nvImage)!)
                stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
                if let url = URL(string: stringPath) {
                    downloadImage(url: url, index: /*index*/imageMap.count-1)
                }
            }
            
            
            //                item.logoImage.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
        }
        print("aaa finish setRonkWithImg")
        
        tableViewRank.delegate = self
        tableViewRank.dataSource = self
    }
    
    func downloadImage(url: URL, index: Int) {
        print("aaa downloadImage start")
        Global.sharedInstance.getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            if let image = UIImage(data: data){
                DispatchQueue.main.async {
                    
                    
                    for i in stride(from: 0, to: self.WorkerOfferedJob.ranking!.lRankingComment!.count, by: 1) {
                        if self.WorkerOfferedJob.ranking!.lRankingComment?[i].nvImage == self.imageMap[index] {
                            self.WorkerOfferedJob.ranking!.lRankingComment?[i].imageView = image
                            print("add image \(i)")
                            print("i: \(i) \(String(describing: self.WorkerOfferedJob.ranking!.lRankingComment?[i].imageView ))")
                        }
                        
                        
                        self.downloadsCounter += 1
                        
                        print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                        if self.downloadsCounter == self.counter {
                            self.tableViewRank.reloadData()
                            self.tableViewRank.beginUpdates()
                            self.tableViewRank.endUpdates()

                        }
                        
                    }
                } } else {
                self.downloadsCounter += 1
                
                print("self.downloadsCounter : \(self.downloadsCounter) , self.counter : \(self.counter)")
                if self.downloadsCounter == self.counter {
                    self.tableViewRank.reloadData()
                }
            }
            
            
            
        }
        print("aaa downloadImage finish")
        
    }
    
    
    func setHeightToScroll(height : CGFloat) {
        print("height 1  self: \(self.height)  height to add : \(height)")
       self.height =  self.height + height
        print("height 2  self: \(self.height)")

        scrollView.contentSize = CGSize(width: baseView.frame.width, height:  self.height)
        
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
