//
//  AddressMapViewController.swift
//  FineWork
//
//  Created by User on 22.6.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import GoogleMaps
class AddressMapViewController: UIViewController ,GMSMapViewDelegate  {

    //MARK: View
    
    
    @IBOutlet  var mapView: GMSMapView!
    @IBOutlet weak var close_x: UILabel!
    //MARK: - action
    
    
    @IBAction func close_x(_ sender: Any) {
         var _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK: - var
    var lat : String = ""
    var lng : String = ""
    var titleMap : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        close_x.text = FlatIcons.sharedInstance.x
  
        //google maps
        //CLLocationDegrees(lat)!let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 31.92272, longitude: 35.04772, zoom: 15)
        
      
     let camera =
            GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(lng)!, zoom: 15)
        mapView.delegate = self

        
        
        self.mapView.isMyLocationEnabled = true
        
        self.mapView.camera = camera
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(CLLocationDegrees(lat)!, CLLocationDegrees(lng)!)
        marker.title = ""
        marker.snippet = ""
        marker.map = mapView
     
        
    }
    func setDisplayData (_lat: String , _lng: String , title : String){
        lat = _lat
        lng = _lng
titleMap = title
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Container
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let barContainerViewController = segue.destination as! BarContainerViewController
        barContainerViewController.titleText = self.titleMap
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
