//
//  VerficationCodePopUpViewController.swift
//  FineWork
//
//  Created by Racheli Kroiz on 22.1.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class VerficationCodePopUpViewController: UIViewController {

    //MARK: - Outlet
    //MARK: -- Label
    
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: -- TextField
    
    @IBOutlet weak var txtCode: UITextField!
    
    //MARK: -- Button
    
    @IBOutlet weak var btnOk: UIButton!
    
    //MARK: -- Action
    
    @IBAction func btnOkAction(_ sender: Any) {
        if txtCode.text == codeVerification
        {
            if isChangePassword {
                updateUser()
            } else {
                var dic = Dictionary<String, AnyObject>()
                dic = (user?.getUserDictionaryForRegister())!
                print("\n---Registration:\n\(dic)")
                api.sharedInstance.Registration(params: dic,success:{(operation:AFHTTPRequestOperation?, responseObject:Any?)
                    -> Void in
                    print(responseObject as! [String:AnyObject])
                    print(responseObject as Any)
                    var dic  =  responseObject as! [String:AnyObject]
                    if let _ = dic["Error"]?["iErrorCode"] {
                        if dic["Error"]?["iErrorCode"] as! Int ==  0
                        {
                        ///    UserDefaultManagment.sharedInstance.setUserName(value: "")
//UserDefaultManagment.sharedInstance.setPassword(value: "")

                            Global.sharedInstance.user = Global.sharedInstance.user.dicToUser(dicUser: dic["Result"] as! NSDictionary)
                            
                            //                        GlobalEmployeeDetails.sharedInstance.worker101Form.nvPhone = Global.sharedInstance.user.nvMobile
                            
                            //on click ok in the alert, 101 form is opened
                            self.dismissPopUp(sendMessage: NSLocalizedString("REGISTER_SUCCESS",comment: ""))
                            
                        }
                        else if dic["Error"]?["iErrorCode"] as! Int ==  2
                        {
                            self.dismissPopUp(sendMessage: dic["Error"]?["nvErrorMessage"] as! String)
                        }
                        else
                        {
                            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.FAILURE_SERVER, comment: "") )
                        }
                    }
                } ,failure: {(AFHTTPRequestOperation, Error) -> Void in
                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: "") )
                })
            }
        }
        else
        {
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.IILEGAL_CODE, comment: "") )
        }
    }

    //MARK: - Variables
    var user:User?
    var delegate:OpenNextPageDelegate!=nil
    var codeVerification:String = ""
    
    // new - for change password
    var isChangePassword : Bool = false
    var newPassword : String = ""
    var changePasswordDelegate : ChangePasswordDelegate! = nil

    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = "אימות נתונים" //NSLocalizedString(LocalizableStringStatic.sharedInstance.ENTER_VERIFICATION, comment: "")
        
        let tapDismiss:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        view.addGestureRecognizer(tapDismiss)
        
        btnOk.setTitle(NSLocalizedString(LocalizableStringStatic.sharedInstance.OK_ALERT, comment: ""), for: .normal)
        txtCode.text = codeVerification
        
        //btnOk.layer.cornerRadius = btnOk.frame.height / 2//20
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyBoard(){
        self.view.endEditing(true)
    }

    //חזרה למסך הרשמה לצורך מעבר לטופס 101
    //מקבל flag המציין את קוד השגיאה כדי לדעת לאיזה מסך לעבור
    func dismissPopUp(sendMessage:String){
        self.dismiss(animated: true, completion: nil)
        delegate.OpenNextPage(sendMessage: sendMessage)
    }
    
    func updateUser() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        
//        if newPassword != "" {
//            Global.sharedInstance.user.nvPassword = newPassword
//        }
        if user != nil {
            Global.sharedInstance.user.nvUserName = (user?.nvUserName)!
            Global.sharedInstance.user.nvMail = (user?.nvMail)!
            Global.sharedInstance.user.nvMobile = (user?.nvMobile)!
            Global.sharedInstance.user.nvPassword = (user?.nvPassword)!
        }
        
        dic["user"] = Global.sharedInstance.user.getUserDictionaryForServer() as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        print(dic as Any)
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    
                    self.dismissPopUp(isSuccess: true, nvErrorMessage: "")
                    
                } else {
                    self.dismissPopUp(isSuccess: false, nvErrorMessage: dic["Error"]?["nvErrorMessage"] as! String)
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
            self.dismissPopUp(isSuccess: nil, nvErrorMessage: "")
        }, funcName : "UpdateUser")
    }
    
    

    //מחזיר למסך הראשי ומשם המשך כרגיל
    func dismissPopUp(isSuccess : Bool?, nvErrorMessage : String) {
        self.dismiss(animated: true, completion: nil)
        self.changePasswordDelegate.isSuccess(isSuccess: isSuccess, nvErrorMessage: nvErrorMessage)
    }

}
