//
//  ShowImagePopUpViewController.swift
//  FineWork
//
//  Created by Racheli Kroiz on 19.1.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class ShowImagePopUpViewController: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var imgVImage: UIImageView!
    
    //MARK: -- Button
    
    @IBOutlet weak var btnCancel: UIButton!
    
    //MARK: -- Action
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    ///MARK: - Variables
    var img:UIImage?
    
    // try - ayala
    var fileUrl : String?
    
    //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)

        if img != nil
        {
            imgVImage.image = img
        }
        
       btnCancel.setTitle(NSLocalizedString("EXIT", comment: ""), for: .normal)
       btnCancel.layer.borderColor = UIColor.orange2.cgColor
       btnCancel.layer.borderWidth = 1
       btnCancel.layer.cornerRadius = btnCancel.frame.height / 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if fileUrl != nil {
            let url : NSString = api.sharedInstance.buldUrlFile(fileName: fileUrl!) as NSString
            let urlStr : NSString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! as String as NSString//url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            
            if let url = NSURL(string: urlStr as String) {
                if let data = NSData(contentsOf: url as URL) {
                    imgVImage.image = UIImage(data: data as Data)
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                } else {
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                }
            } else {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            }
        } else {
            // show alert
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
