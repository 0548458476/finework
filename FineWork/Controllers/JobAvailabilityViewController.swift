//
//  JobAvailabilityViewController.swift
//  FineWork
//
//  Created by Tamy wexelbom on 29.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class JobAvailabilityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SetDateTextFieldTextDelegate, ChangeScrollViewHeightDelegate, OpenAnotherPageDelegate {

    //MARK: - Views
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var availabilityTbl: UITableView!
    @IBOutlet weak var tblHeightCon: NSLayoutConstraint!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var saveBtnHeightFromTopCon: NSLayoutConstraint!
    // job preferences undefined views
    @IBOutlet var jobPreferencesUndefinedView: UIView!
    @IBOutlet var goDefineJobPreferencesLbl: UILabel!
    // constant job views
    @IBOutlet weak var constantJobView: UIView!
    @IBOutlet weak var constantJobBeginLbl: UILabel!
    @IBOutlet weak var constantJobFromDateLbl: UILabel!
    @IBOutlet weak var constantJobDateIconLbl: UILabel!
    @IBOutlet weak var constantJobHeightFromTopCon: NSLayoutConstraint!
    // partial job views
    @IBOutlet weak var partialJobView: UIView!
    @IBOutlet weak var partialJobBeginLbl: UILabel!
    @IBOutlet weak var partialJobFromDateIconLbl: UILabel!
    @IBOutlet weak var partialJobToDateIconLbl: UILabel!
    @IBOutlet weak var partialJobFromDateLbl: UILabel!
    @IBOutlet weak var partialJobToDateLbl: UILabel!
    @IBOutlet weak var partialJobHeightFromTopCon: NSLayoutConstraint!
    
    //MARK: - Actions
    @IBAction func saveBtnAction(_ sender: Any) {
        // validations
        if (!constantJobView.isHidden && constantJobFromDateLbl.text == "") || (!partialJobView.isHidden && partialJobFromDateLbl.text == "") {
            Alert.sharedInstance.showAlert(mess: "נא להזין את התאריכים הדרושים!")
            
            if !constantJobView.isHidden && constantJobFromDateLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: constantJobFromDateLbl)
            } else {
                Global.sharedInstance.setValidToLabel(sender: constantJobFromDateLbl)
            }
            
            if !partialJobView.isHidden && partialJobFromDateLbl.text == "" {
                Global.sharedInstance.setErrorToLabel(sender: partialJobFromDateLbl)
            } else {
                Global.sharedInstance.setValidToLabel(sender: partialJobFromDateLbl)
            }
    
        } else {
            Global.sharedInstance.setValidToLabel(sender: constantJobFromDateLbl)
            Global.sharedInstance.setValidToLabel(sender: partialJobFromDateLbl)
            
            var dayArr = Array<WorkerAvailabiltyDay>()
            var isChooseDays : Bool = false
            if  self.workerAvailabilty.iWorkerAvailabilityType == AvailabiltyType.PartialJob.rawValue {
            for dayCell in daysCellArr {
                let day = dayCell.save()
                if day.lWorkerAvailabiltyTimes.count > 0 {
                    isChooseDays = true
                }
                dayArr.insert(day, at: dayArr.count)
            }
            }else{
                isChooseDays = true

            }
            if !isChooseDays {
                Alert.sharedInstance.showAlert(mess: "יש לבחור שעות של יום אחד לפחות!")
            } else {
                saveData(dayArr: dayArr)
            }
        }
        
    }
    //MARK: - Variables
    var numberOfDays = 0
    var changeScrollViewHeightDelegate : ChangeScrollViewHeightDelegate! = nil
    var workerAvailabilty = WorkerAvailabilty()
    var daysCellArr = Array<AvailabilityTableViewCell>()
    var spaceBetweenSections = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setDesign()
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapConstantJobFromDateLbl))
        constantJobFromDateLbl.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPartialJobFromDateLbl))
        partialJobFromDateLbl.addGestureRecognizer(tap2)
        
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPartialJobToDateLbl))
        partialJobToDateLbl.addGestureRecognizer(tap3)
        
        daysCellArr.removeAll()
        numberOfDays = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tblHeightCon.constant = 130
        saveBtnHeightFromTopCon.constant = 10
        saveBtn.isHidden = true
        daysCellArr.removeAll()
        numberOfDays = 0
         getWorkerAvailabilty()
    }
    
    func setDesign() {
        constantJobDateIconLbl.text = FlatIcons.sharedInstance.CALANDER
        partialJobFromDateIconLbl.text = FlatIcons.sharedInstance.CALANDER
        partialJobToDateIconLbl.text = FlatIcons.sharedInstance.CALANDER
        
        // set tags
        constantJobFromDateLbl.tag = 10
        partialJobFromDateLbl.tag = 11
        partialJobToDateLbl.tag = 12
        
        saveBtn.layer.borderColor = saveBtn.titleLabel?.textColor.cgColor
        saveBtn.layer.borderWidth = 1
        saveBtn.layer.cornerRadius = saveBtn.frame.height / 2
    }
    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numOfRowsInSection = numberOfDays
//        if tableView == autoCompletePlacesTbl {
//            numOfRowsInSection = placesArr.count
//        } else {
//            numOfRowsInSection = workerLocationsArr.count
//        }
        
        return numOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : AvailabilityTableViewCell
        
//        cell = availabilityTbl.dequeueReusableCell(withIdentifier: "AvailabilityTableViewCell") as! AvailabilityTableViewCell
        
        cell = daysCellArr[indexPath.section]
        
        switch indexPath.section {
        case 0:
            cell.dayLbl.text = "א׳"
            cell.iDayId = DayType.Sunday.rawValue
            break
        case 1:
            cell.dayLbl.text = "ב׳"
            cell.iDayId = DayType.Monday.rawValue
            break
        case 2:
            cell.dayLbl.text = "ג׳"
            cell.iDayId = DayType.Tuesday.rawValue
            break
        case 3:
            cell.dayLbl.text = "ד׳"
            cell.iDayId = DayType.Wednesday.rawValue
            break
        case 4:
            cell.dayLbl.text = "ה׳"
            cell.iDayId = DayType.Thursday.rawValue
            break
        case 5:
            cell.dayLbl.text = "ו׳"
            cell.iDayId = DayType.Friday.rawValue
            break
        case 6:
            cell.dayLbl.text = "מוצאי שבת"
            cell.iDayId = DayType.Saturday.rawValue
            break
        default:
            break
        }
        cell.changeScrollViewHeightDelegate = self
        cell.openAnotherPageDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     var rowHeight = 0.0
        
        if daysCellArr[indexPath.section].secondView.isHidden {
            rowHeight = 120.0
        } else {
            rowHeight = 200.0
        }
     
     return CGFloat(rowHeight)
     }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return CGFloat(spaceBetweenSections)/*0.001*/
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func addDayAvailability() {
        for _ in stride(from: 0, to: 7, by: 1) {
            let cell = availabilityTbl.dequeueReusableCell(withIdentifier: "AvailabilityTableViewCell") as! AvailabilityTableViewCell
            cell.changeScrollViewHeightDelegate = self
            daysCellArr.insert(cell, at: daysCellArr.count)
        }
        for i in stride(from: 0, to: workerAvailabilty.lWorkerAvailabiltyDays.count, by: 1) {
            switch workerAvailabilty.lWorkerAvailabiltyDays[i].iDayType {
            case DayType.Sunday.rawValue:
                daysCellArr[0].setDisplayData(_workerAvailibilityDay: workerAvailabilty.lWorkerAvailabiltyDays[i])
                break
            case DayType.Monday.rawValue:
                daysCellArr[1].setDisplayData(_workerAvailibilityDay: workerAvailabilty.lWorkerAvailabiltyDays[i])
                break
            case DayType.Tuesday.rawValue:
                daysCellArr[2].setDisplayData(_workerAvailibilityDay: workerAvailabilty.lWorkerAvailabiltyDays[i])
                break
            case DayType.Wednesday.rawValue:
                daysCellArr[3].setDisplayData(_workerAvailibilityDay: workerAvailabilty.lWorkerAvailabiltyDays[i])
                break
            case DayType.Thursday.rawValue:
                daysCellArr[4].setDisplayData(_workerAvailibilityDay: workerAvailabilty.lWorkerAvailabiltyDays[i])
                break
            case DayType.Friday.rawValue:
                daysCellArr[5].setDisplayData(_workerAvailibilityDay: workerAvailabilty.lWorkerAvailabiltyDays[i])
                break
            case DayType.Saturday.rawValue:
                daysCellArr[6].setDisplayData(_workerAvailibilityDay: workerAvailabilty.lWorkerAvailabiltyDays[i])
                break
            default:
                break
            }
        }
        availabilityTbl.reloadData()
    }
    
//    func addLocation(isScrollToMiddle : Bool, workerLocation : WorkerAskingLocation) {
//        //let cellLanguage = LanguageTableViewCell()
//        //cellLanguagesArray2.insert(cellLanguage, at: cellLanguagesArray2.count)
//        workerLocationsArr.insert(workerLocation, at: workerLocationsArr.count)
//        
//        if workerLocationsArr.count > locationsTbl.numberOfRows(inSection: 1) {
//            locationsTbl.beginUpdates()
//            locationsTbl.insertRows(at: [IndexPath(row: workerLocationsArr.count - 1, section: 1)], with: .automatic)
//            locationsTbl.endUpdates()
//        }
//        
//        if isScrollToMiddle {
//            scrollTableViewToMiddleScreen(indexPath: IndexPath(row: workerLocationsArr.count - 1, section: 1))
//        }
//        
//        changeScrollView.changeScrollViewHeight(heightToadd: 40)
//    }
    
//    func removeLocation(cellNumber : Int) {
//        workerLocationsArr.remove(at: cellNumber)
//        
//        /*
//         var _cellNumber : Int = 0
//         for cell in cellLanguagesArray2 {
//         print("A-log cellNumber : \(cell.cellNumber)")
//         cell.cellNumber = _cellNumber
//         _cellNumber += 1
//         }
//         */
//        
//        locationsTbl.deleteRows(at: [IndexPath(row: cellNumber, section: 1)], with: UITableViewRowAnimation.automatic)
//        locationsTbl.reloadData()
//        
//        if workerLocationsArr.count > 0 {
//            scrollTableViewToMiddleScreen(indexPath: IndexPath(row: workerLocationsArr.count - 1, section: 1))
//        }
//    }
    
//    func scrollTableViewToMiddleScreen(indexPath : IndexPath) {
//        locationsTbl.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
//    }
    
    //MARK: - taps
    func tapConstantJobFromDateLbl()
    {
        openDatePicker(dateLbl : constantJobFromDateLbl)
    }
    
    func tapPartialJobFromDateLbl()
    {
        openDatePicker(dateLbl : partialJobFromDateLbl)
    }

    func tapPartialJobToDateLbl()
    {
        openDatePicker(dateLbl : partialJobToDateLbl)
    }

    //MARK: - delegates
    func openDatePicker(dateLbl : UILabel) {
        
        //dismissKeyBoard()
        
        let datePickerPopUp: DatePickerPopUpViewController = storyboard?.instantiateViewController(withIdentifier:"DatePickerPopUpViewController")as! DatePickerPopUpViewController
        
        datePickerPopUp.setDateTextFieldTextDelegate = self
        datePickerPopUp.modalPresentationStyle = UIModalPresentationStyle.custom
        
        datePickerPopUp.tag = dateLbl.tag
        datePickerPopUp.dateDefault = dateLbl.text
        
        self.present(datePickerPopUp, animated: true, completion: nil)
    }
    
    func SetDateFieldText(dateAsString : String, tagOfField : Int) {
        switch tagOfField {
        case constantJobFromDateLbl.tag:
            constantJobFromDateLbl.text = dateAsString
            break
        case partialJobFromDateLbl.tag:
            partialJobFromDateLbl.text = dateAsString
            break
        case partialJobToDateLbl.tag:
            partialJobToDateLbl.text = dateAsString
            break
        default:
            break
        }
        
        Global.sharedInstance.bWereThereAnyChanges = true
    }
    
    func changeScrollViewHeight(heightToadd: CGFloat) {
        changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: heightToadd)
        tblHeightCon.constant += heightToadd
        saveBtnHeightFromTopCon.constant += heightToadd
    }
    
    //MARK: - Set Data
    func setData() {
        switch self.workerAvailabilty.iWorkerAvailabilityType {
        case AvailabiltyType.PartialJob.rawValue:
            self.saveBtn.isHidden = false
            self.constantJobView.isHidden = true
            self.partialJobView.isHidden = false
            jobPreferencesUndefinedView.isHidden = true
            self.numberOfDays = 7
            self.availabilityTbl.isHidden = false
            self.partialJobHeightFromTopCon.constant = 10
            saveBtnHeightFromTopCon.constant += (self.partialJobView.frame.height + 10)
            self.partialJobBeginLbl.text = "ממתי אתה פנוי?"
            setDataToPartialJob()
            break
        case AvailabiltyType.FullJob.rawValue:
            self.saveBtn.isHidden = false
            self.constantJobView.isHidden = false
            self.partialJobView.isHidden = true
            jobPreferencesUndefinedView.isHidden = true
            self.numberOfDays = 0
            self.availabilityTbl.isHidden = true
            self.partialJobHeightFromTopCon.constant = self.constantJobView.frame.height + 10
            saveBtnHeightFromTopCon.constant += (self.constantJobView.frame.height + 10)
            self.constantJobBeginLbl.text = "ממתי אתה פנוי?"
            setDataToConstantJob()
            break
        case AvailabiltyType.PartialAndFullJob.rawValue:
            self.saveBtn.isHidden = false
            self.constantJobView.isHidden = false
            self.partialJobView.isHidden = false
            jobPreferencesUndefinedView.isHidden = true
            self.numberOfDays = 7
            self.availabilityTbl.isHidden = false
            self.partialJobHeightFromTopCon.constant = self.constantJobView.frame.height + 10
            saveBtnHeightFromTopCon.constant += (self.constantJobView.frame.height + self.partialJobView.frame.height + 20)
            self.constantJobBeginLbl.text = "למשרה הקבועה: ממתי אתה פנוי?"
            self.partialJobBeginLbl.text = "למשרה הזמנית/ חלקית: ממתי אתה פנוי?"
            setDataToConstantJob()
            setDataToPartialJob()
            break
        case AvailabiltyType.NoChooseDomains.rawValue:
            self.constantJobView.isHidden = true
            self.partialJobView.isHidden = true
            self.saveBtn.isHidden = true
            self.numberOfDays = 0
            self.availabilityTbl.isHidden = true
            jobPreferencesUndefinedView.isHidden = false
            break
        default:
            break
        }
        
        
        if self.numberOfDays == 7 {
            changeScrollViewHeightDelegate.setScrollViewEnable!(isEnable: true)
            self.tblHeightCon.constant = self.tblHeightCon.constant * 7
            //                            self.tblHeightCon.constant = (self.spaceBetweenSections * 6)
            saveBtnHeightFromTopCon.constant += self.tblHeightCon.constant
            self.changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: self.tblHeightCon.constant)
            self.addDayAvailability()
        } else {
            changeScrollViewHeightDelegate.setScrollViewEnable!(isEnable: false)
        }
        
        if self.constantJobView.isHidden {
            changeScrollViewHeightDelegate.changeScrollViewHeight(heightToadd: -(self.constantJobView.frame.height))
        }
    }
    
    func setDataToConstantJob() {
        if workerAvailabilty.dFullTimeAvailabilityFromDate != nil {
            constantJobFromDateLbl.text = Global.sharedInstance.getStringFromDateString(dateString: workerAvailabilty.dFullTimeAvailabilityFromDate!)
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            constantJobFromDateLbl.text = dateFormatter.string(from: Date())
        }
    }
    
    func setDataToPartialJob() {
        if workerAvailabilty.dPartTimeAvailabilityFromDate != nil {
            partialJobFromDateLbl.text = Global.sharedInstance.getStringFromDateString(dateString:workerAvailabilty.dPartTimeAvailabilityFromDate!)
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            partialJobFromDateLbl.text = dateFormatter.string(from: Date())
        }
        
        if workerAvailabilty.dPartlTimeAvailabilityToDate != nil {
            partialJobToDateLbl.text = Global.sharedInstance.getStringFromDateString(dateString:workerAvailabilty.dPartlTimeAvailabilityToDate!)
        }
        
    }
    
    func saveData(dayArr : Array<WorkerAvailabiltyDay>) {
        if !constantJobView.isHidden {
            workerAvailabilty.dFullTimeAvailabilityFromDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: constantJobFromDateLbl.text!))
        }
        if !partialJobView.isHidden {
            workerAvailabilty.dPartTimeAvailabilityFromDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: partialJobFromDateLbl.text!))
            if  partialJobToDateLbl.text != "" {
            workerAvailabilty.dPartlTimeAvailabilityToDate = Global.sharedInstance.convertNSDateToString(dateTOConvert: Global.sharedInstance.getDateFromString(dateString: partialJobToDateLbl.text!))
            }
        }
        
//        var dayArr = Array<WorkerAvailabiltyDay>()
//        
//        for dayCell in daysCellArr {
//            dayArr.insert(dayCell.save(), at: dayArr.count)
//        }
        
        workerAvailabilty.lWorkerAvailabiltyDays = dayArr
        UpdateWorkerAvailabilty()
    }
    
    //MARK: - Server Functions
    func getWorkerAvailabilty() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String, AnyObject> {
//                        var workerAvailabilty = WorkerAvailabilty()
                        self.workerAvailabilty = self.workerAvailabilty.getWorkerAvailabiltyFromDic(dic: dicResult)
                        
                        self.setData()
//                        self.availabilityTbl.reloadData()
                        //self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetWorkerAvailabilty)
    }
    
    func UpdateWorkerAvailabilty() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        //WorkerAskingLocation workerAskingLocation, int iUserId
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["workerAvailabilty"] = workerAvailabilty.getDistionaryFromWorkerAvailability() as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Bool {
                        
                        if dicResult {
                            Alert.sharedInstance.showAlert(mess: "הפרטים נשמרו בהצלחה!")
                            Global.sharedInstance.bWereThereAnyChanges = false
                        } else {
                            Alert.sharedInstance.showAlert(mess: "אירעה שגיאה בשמירת הפרטים!")
                        }
                        
//                        self.resetData()
//                        self.locationsTbl.reloadData()
                        //self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateWorkerAvailabilty)
    }
    
    //MARK: - OpenAnotherPageDelegate
    func openViewController(VC: UIViewController) {
        if VC is BasePopUpViewController {
            self.present(VC, animated: true, completion: nil)
        }
    }


}
