//
//  DatePickerPopUpViewController.swift
//  FineWork
//
//  Created by Racheli Kroiz on 3.1.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class DatePickerPopUpViewController: UIViewController {

//MARK: - Outlet
    //MARK: -- DatePicker
    
    @IBOutlet weak var datePicker: UIDatePicker!
    //MARK: - Button
    
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var btnOk: UIButton!
    
    //MARK: - Action
    
    @IBAction func CancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func okAction(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        if isTimeMode {
            dateFormatter.dateFormat = "HH:mm"
            if lblDateTiem != nil {
                lblDateTiem!.text = dateFormatter.string(from: dateSelected! as Date)
            }
        } else {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            switch tag {
            case 1://txtDateBorn - personalDetails
                if dateSelected != nil
                {
                    if abs(Global.sharedInstance.YearsBetween2Dates(date1: dateSelected! as Date, date2: NSDate() as Date)) > 15
                    {
                        GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate = dateSelected
                        setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                    }
                    else
                    {
                        Alert.sharedInstance.showAlert(mess: "לא ניתן להירשם כעובד מתחת גיל 16")
                    }
                }
                
            case 2://txtDateImmigration - personalDetails
                if dateSelected != nil
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.dImmigrationDate = dateSelected
                    setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                }
                
            case 3://txtDateBorn - partner
                if dateSelected != nil
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseBirthDate = dateSelected!
                    setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                    if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.isRequiredFieldsNull() == true
                    {
                        GlobalEmployeeDetails.sharedInstance.ReloadReasons()
                    }
                }
                
            case 4://txtDateImmigration - partner
                if dateSelected != nil
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseImmigrationDate = dateSelected!
                    setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                }
                
            case 5://txtDateBorn - child
                if dateSelected != nil
                {
                    if abs(Global.sharedInstance.YearsBetween2Dates(date1: dateSelected! as Date, date2: NSDate() as Date)) < 21
                    {
                        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[viewCon.tag].dBirthDate = dateSelected!
                        setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                    }
                    else
                    {
                        Alert.sharedInstance.showAlert(mess: "לא ניתן להזין ילד שגילו מעל 20 שנה")
                    }
                }
            case 6://from date - new immegrant
                if dateSelected != nil
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent2 = dateFormatter.string(from: dateSelected! as Date)
                    setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                }
            case 7://from date - recident place
                if dateSelected != nil
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent1 = dateFormatter.string(from: dateSelected! as Date)
                    setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                }
            case 8://from date - soldier
                if dateSelected != nil
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent1 = dateFormatter.string(from: dateSelected! as Date)
                    setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                }
            case 9://to date - soldier
                if dateSelected != nil
                {
                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent2 = dateFormatter.string(from: dateSelected! as Date)
                    setDateTextFieldTextDelegate.SetDateTextFieldText!(tagOfTextField: tag)
                }
                
            // job availability view controller
            case 10, 11, 12, 13, 14, 15: // constant job from date label
                setDateTextFieldTextDelegate.SetDateFieldText!(dateAsString: dateFormatter.string(from: dateSelected! as Date), tagOfField: tag)
                break
                //        case 11: // partial job from date label
                //            setDateTextFieldTextDelegate.SetDateFieldText!(dateAsString: dateFormatter.string(from: dateSelected! as Date), tagOfField: tag)
                //            break
                //        case 12: // partial job to date label
                //            setDateTextFieldTextDelegate.SetDateFieldText!(dateAsString: dateFormatter.string(from: dateSelected! as Date), tagOfField: tag)
                //            break
                
                //לשמור את התאריך הנבחר
                
            default:
                if dateSelected != nil
                {
                    setDateTextFieldTextDelegate.SetDateFieldText!(dateAsString: dateFormatter.string(from: dateSelected! as Date), tagOfField: tag)
                }
                break
            }
        }
        self.dismiss(animated: true, completion: nil)
    }

    
 //MARK: - Variables
    var tag = 0//save which textField opened this controller: 1 = txtDateBorn,2 = txtDateImmigration
    var setDateTextFieldTextDelegate:SetDateTextFieldTextDelegate!=nil
    var dateSelected:NSDate?
    var dateDefault:String?//אם יש תאריך בחור,יש להציג את התאריך לפי מה שכבר בחור ושלא יעמוד בהתחלה על התאריך של היום
    var viewCon:UITableViewCell = UITableViewCell()
    
    //--
    var isTimeMode : Bool = false
    var lblDateTiem : UILabel? = nil
    
 //MARK: - Initial
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dateSelected = NSDate()
        let tapDismiss:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissPicker))
        view.addGestureRecognizer(tapDismiss)
        
        datePicker.addTarget(self, action: #selector(DatePickerPopUpViewController.handleDatePicker(sender:)), for: UIControlEvents.valueChanged)
        
        let dateFormatter = DateFormatter()
        
        if isTimeMode {
            datePicker.datePickerMode = .time
            dateFormatter.dateFormat = "HH:mm"
        }else {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            if tag >= 10/* && tag <= 12*/ {
                datePicker.minimumDate = NSDate() as Date
            } else {
                datePicker.maximumDate = NSDate() as Date
            }
            
        }
        
        if dateDefault != ""
        {
            dateSelected = dateFormatter.date(from: dateDefault!)! as NSDate?
            datePicker.setDate(dateFormatter.date(from: dateDefault!)!, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {
        dateSelected = sender.date as NSDate
    }

    func dismissPicker()
    {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd/MM/yyyy"
//
//        switch tag {
//        case 1://txtDateBorn - personalDetails
//            if dateSelected != nil
//            {
//                if abs(Global.sharedInstance.YearsBetween2Dates(date1: dateSelected as! Date, date2: NSDate() as Date)) > 15
//                {
//                    GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate = dateSelected
//                    setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//                }
//                else
//                {
//                    Alert.sharedInstance.showAlert(mess: "לא ניתן להירשם כעובד מתחת גיל 16")
//                }
//            }
//            
//        case 2://txtDateImmigration - personalDetails
//            if dateSelected != nil
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.dImmigrationDate = dateSelected
//                setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//            }
//            
//        case 3://txtDateBorn - partner
//            if dateSelected != nil
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseBirthDate = dateSelected!
//                setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//                if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.isRequiredFieldsNull() == true
//                {
//                    GlobalEmployeeDetails.sharedInstance.ReloadReasons()
//                }
//            }
//            
//        case 4://txtDateImmigration - partner
//            if dateSelected != nil
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.dSpouseImmigrationDate = dateSelected!
//                setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//            }
//            
//        case 5://txtDateBorn - child
//            if dateSelected != nil
//            {
//                if abs(Global.sharedInstance.YearsBetween2Dates(date1: dateSelected as! Date, date2: NSDate() as Date)) < 21
//                {
//                    GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds[viewCon.tag].dBirthDate = dateSelected!
//                    setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//                }
//                else
//                {
//                    Alert.sharedInstance.showAlert(mess: "לא ניתן להזין ילד שגילו מעל 20 שנה")
//                }
//            }
//        case 6://from date - new immegrant
//            if dateSelected != nil
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[4].nvValueContent2 = dateFormatter.string(from: dateSelected as! Date)
//                setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//            }
//        case 7://from date - recident place
//            if dateSelected != nil
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[3].nvValueContent1 = dateFormatter.string(from: dateSelected as! Date)
//                setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//            }
//        case 8://from date - soldier
//            if dateSelected != nil
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent1 = dateFormatter.string(from: dateSelected as! Date)
//                setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//            }
//        case 9://to date - soldier
//            if dateSelected != nil
//            {
//                GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons[13].nvValueContent2 = dateFormatter.string(from: dateSelected as! Date)
//                setDateTextFieldTextDelegate.SetDateTextFieldText(tagOfTextField: tag)
//            }
//            
//            //לשמור את התאריך הנבחר
//            
//        default:
//            break
//        }

         self.dismiss(animated: true, completion: nil)
    }
}
