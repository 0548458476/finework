//
//  JobLocationViewController.swift
//  FineWork
//
//  Created by Lior Ronen on 23/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import TTRangeSlider

class ElapsedMetersFormatter: NumberFormatter {
    
    override func string(from number: NSNumber) -> String? {
        
//        return String(describing: (number.intValue / 10) * 10)
        return String(describing: number.intValue)
    }
}

class JobLocationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CellOptionsDelegate, GMSMapViewDelegate, TTRangeSliderDelegate, OpenAnotherPageDelegate, UITextFieldDelegate {

    //MARK: - Views
    @IBOutlet var placeTxt: UITextField!
    @IBOutlet var addPlaceBtn: UIButton!
//    @IBOutlet var radiusSlider: UISlider!
    @IBOutlet var locationsTbl: UITableView!
    @IBOutlet var autoCompletePlacesTbl: UITableView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet weak var radiusSlider: TTRangeSlider!
    
    //MARK: - Actions
    @IBAction func addPlaceBtnAction(_ sender : UIButton) {
        // validation
        
        if placeSelected == nil && rowToUpdate < 0 {
            if placeTxt.text == "" {
            Alert.sharedInstance.showAlert(mess: "לא נבחר מיקום!")
            } else {
                Alert.sharedInstance.showAlert(mess: "יש לבחור מיקום מהרשימה!")
            }
        } else {
            let workerLocation : WorkerAskingLocation
            if rowToUpdate >= 0 {    // update
                workerLocation = workerLocationsArr[rowToUpdate]
            } else {                 // add
                workerLocation = WorkerAskingLocation()
            }
            
            if placeSelected != nil {
                workerLocation.nvAddress = /*(placeSelected?.name)!*/placeTxt.text!
                workerLocation.nvLat = (placeSelected?/*.coordinate*/.latitude.description)!
                workerLocation.nvLng = (placeSelected?/*.coordinate*/.longitude.description)!
            }
            
            workerLocation.nvAddress = placeTxt.text!
            workerLocation.nvLat = (marker?.position.latitude.description)!
            workerLocation.nvLng = (marker?.position.longitude.description)!
            
            workerLocation.iWorkerUserId = Global.sharedInstance.user.iUserId
            print("radius   \(Float((circle?.radius)!))")
            workerLocation.nRadius = Float((circle?.radius)!/1000)
            UpdateWorkerAskingLocation(workerAskingLocation: workerLocation, indexInArray: rowToUpdate)
            
            rowToUpdate = -1
            placeSelected = nil
            //addLocation(isScrollToMiddle: false, workerLocation: workerLocation)
        }
        
    }
    
    @IBOutlet var allCountryBtn: UIButton!
    @IBAction func allCountryBtnAction(_ sender: Any) {
        changeCircleRadius(radius: Double(200 * 1000))
        radiusSlider.maxValue = Float(200)
        radiusSlider.selectedMaximum = Float(200)
    }
    
    
    @IBAction func radius10Km(_ sender: Any) {
        changeCircleRadius(radius: Double(10 * 1000))
        radiusSlider.selectedMaximum = Float(10)
        

    }
//    @IBAction func radiusSliderChange(_ sender: UISlider) {
//        print("value : \(sender.value) ")
//        changeCircleRadius(radius: Double(sender.value) * 1000)
//        
//        radiusSlider.setThumbImage(textToImage(drawText: "ayala", inImage: UIImage(named: "money")!, atPoint: CGPoint(x: 0, y: 0)), for: .normal)
//
//    }
    
    @IBAction func radiusSliderDidEndDrag(_ sender: UISlider) {
        print("drag end")
    }
    
    //MARK: - Variables
    var workerLocationsArr = Array<WorkerAskingLocation>()
    var rowToUpdate : Int = -1
    // google vars
    var placesClient : GMSPlacesClient! = nil
    var placesArr = Array<GMSAutocompletePrediction>()
//    var placeSelected : GMSPlace? = nil
    var placeSelected : CLLocationCoordinate2D? = nil

    var circle : GMSCircle? = nil
    var marker : GMSMarker? = nil
    // delegates
    var changeScrollView : ChangeScrollViewHeightDelegate! = nil
    var getTextFieldDelegate : getTextFieldDelegate! = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addPlaceBtn.layer.borderColor = UIColor.orange2.cgColor
        addPlaceBtn.layer.borderWidth = 1
        addPlaceBtn.layer.cornerRadius = addPlaceBtn.frame.height / 2
        
//        radiusSlider.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI))
        
        //google maps
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 31.92272, longitude: 35.04772, zoom: 15)
        mapView.camera = camera
        mapView.delegate = self
        
        // google places
        placesClient = GMSPlacesClient.shared()
        placeTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
//        radiusSlider.setThumbImage(UIImage(named: "money"), for: UIControlState.normal)
//        radiusSlider.setThumbImage(textToImage(drawText: "15", inImage: UIImage(named: "money")!, atPoint: CGPoint(x: 0, y: 0)), for: UIControlState.highlighted)
//
//        radiusSlider.addTarget(self, action: #selector(sliderDragEnd(_:)), for: .touchUpInside)
//        radiusSlider.addTarget(self, action: #selector(sliderDragEnd(_:)), for: .touchUpOutside)
        
        // init radius slider
        radiusSlider.tintColorBetweenHandles = UIColor.blue2
        //secondRangeSlider.tintColor = UIColor.white
        radiusSlider.handleColor = UIColor.blue2
        
        radiusSlider.handleDiameter = 20
        radiusSlider.selectedHandleDiameterMultiplier = 1.0
        
        radiusSlider.minValue = Float(10)
        radiusSlider.maxValue = Float(100)
        
//        radiusSlider.selectedMinimum = Float(10)
        radiusSlider.selectedMaximum = Float(10)
        
        let numFormat = ElapsedMetersFormatter()
        radiusSlider.numberFormatterOverride = numFormat
//        secondRangeSlider.numberFormatterOverride = numFormat
        
        radiusSlider.delegate = self
        
        radiusSlider.disableRange = true
        
        
        
        placeTxt.delegate = self
        placeTxt.tag = 2
        
        
        
        
        //tap to 10 km
        
        print("ccc viewDidLoad")
        
        
    }
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
        if radiusSlider.maxValue == Float(200){
            radiusSlider.maxValue = Float(100)
        }
        print("value (selectedMinimum): \(selectedMinimum) ")
        print("value (selectedMaximum): \(selectedMaximum) ")
//        changeCircleRadius(radius: Double((Int(selectedMaximum) / 10) * 10) * 1000)
        changeCircleRadius(radius: Double(Int(selectedMaximum) * 1000))

    }
    
    func didStartTouches(in sender: TTRangeSlider!) {
        
    }
    
    func didEndTouches(in sender: TTRangeSlider!) {
        print("drag end notification")
        
        if placeSelected != nil {
            getEmployerCount(latitude: (placeSelected?/*.coordinate.*/.latitude.description)!, longitude: (placeSelected?/*.coordinate.*/.longitude.description)!, radiusInMeters: (radiusSlider.selectedMaximum / 10) * 10)
        } else if rowToUpdate > -1 {
            getEmployerCount(latitude: workerLocationsArr[rowToUpdate].nvLat, longitude: workerLocationsArr[rowToUpdate].nvLng, radiusInMeters: (radiusSlider.selectedMaximum / 10) * 10)
        }
    }
    
//    func sliderDragEnd(_ not : NSNotification) {
//        print("drag end notification")
//        
//        if placeSelected != nil {
//            getEmployerCount(latitude: (placeSelected?.coordinate.latitude.description)!, longitude: (placeSelected?.coordinate.longitude.description)!, radiusInMeters: Float(Int(radiusSlider.value)))
//        } else if rowToUpdate > -1 {
//            getEmployerCount(latitude: workerLocationsArr[rowToUpdate].nvLat, longitude: workerLocationsArr[rowToUpdate].nvLng, radiusInMeters: Float(Int(radiusSlider.value)))
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("ccc viewWillAppear")
        getWorkerLocations()
    }
    
    //MARK: - Text Field Functions
    func textFieldDidChange(_ textField: UITextField) {
        if textField.text != "" {
            placeAutocomplete(stringToSearch: textField.text!)
            
        }
    }
     func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        getTextFieldDelegate.getTextField(textField: textField, originY: 0)
        return true
    }

    //MARK: - Table View Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfRowsInSection = 0
        if tableView == autoCompletePlacesTbl {
            numOfRowsInSection = placesArr.count
        } else {
            numOfRowsInSection = workerLocationsArr.count
        }
        
        return numOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell
        if tableView == autoCompletePlacesTbl {
            //numOfRowsInSection = placesArr.count
            cell = autoCompletePlacesTbl.dequeueReusableCell(withIdentifier: "FamilyStatusTableViewCell") as! FamilyStatusTableViewCell
            (cell as! FamilyStatusTableViewCell).lblStatus.text = placesArr[indexPath.section/*row*/].attributedFullText.string
        } else {
            // : LocationTableViewCell = LocationTableViewCell()
            cell = locationsTbl.dequeueReusableCell(withIdentifier: "LocationTableViewCell") as! LocationTableViewCell
            (cell as! LocationTableViewCell).setDisplayData(workerLocation: workerLocationsArr[indexPath.section/*row*/])
            (cell as! LocationTableViewCell).remomeCellDelegate = self
            (cell as! LocationTableViewCell).openAnotherPageDelegate = self
            (cell as! LocationTableViewCell).cellNumber = indexPath.section/*row*/
        }
        return cell
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var rowHeight = 0.0
        
        rowHeight = 40.0
        
        return CGFloat(rowHeight)
    }*/
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10/*0.001*/
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Global.sharedInstance.bWereThereAnyChanges = true
        
        if tableView == autoCompletePlacesTbl {
            placeTxt.text = placesArr[indexPath.section/*row*/].attributedFullText.string
            autoCompletePlacesTbl.isHidden = true
            getPlaceByID(placeId: placesArr[indexPath.section/*row*/].placeID!)
            
            
        } else {
            rowToUpdate = indexPath.section/*row*/
            placeTxt.text = workerLocationsArr[rowToUpdate].nvAddress
//            radiusSlider.setValue((workerLocationsArr[rowToUpdate].nRadius!) / 1000, animated: true)
            radiusSlider.selectedMaximum = (workerLocationsArr[rowToUpdate].nRadius!)
            addMarkerAndCircle(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(workerLocationsArr[rowToUpdate].nvLat)!, longitude: CLLocationDegrees(workerLocationsArr[rowToUpdate].nvLng)!), radius: CLLocationDistance((workerLocationsArr[rowToUpdate].nRadius!)*1000), zoom: 8)

            
        }
    }
    
    func addLocation(isScrollToMiddle : Bool, workerLocation : WorkerAskingLocation) {
        //let cellLanguage = LanguageTableViewCell()
        //cellLanguagesArray2.insert(cellLanguage, at: cellLanguagesArray2.count)
        workerLocationsArr.insert(workerLocation, at: workerLocationsArr.count)
        
        if workerLocationsArr.count > locationsTbl.numberOfRows(inSection: 1) {
            locationsTbl.beginUpdates()
            locationsTbl.insertRows(at: [IndexPath(row: workerLocationsArr.count - 1, section: 1)], with: .automatic)
            locationsTbl.endUpdates()
        }
        
        if isScrollToMiddle {
            scrollTableViewToMiddleScreen(indexPath: IndexPath(row: workerLocationsArr.count - 1, section: 1))
        }
        
        changeScrollView.changeScrollViewHeight(heightToadd: 40)
    }
    
    func removeLocation(cellNumber : Int) {
        workerLocationsArr.remove(at: cellNumber)
        
        /*
        var _cellNumber : Int = 0
        for cell in cellLanguagesArray2 {
            print("A-log cellNumber : \(cell.cellNumber)")
            cell.cellNumber = _cellNumber
            _cellNumber += 1
        }
 */
        
        locationsTbl.deleteRows(at: [IndexPath(row: cellNumber, section: 1)], with: UITableViewRowAnimation.automatic)
        locationsTbl.reloadData()
        
        if workerLocationsArr.count > 0 {
            scrollTableViewToMiddleScreen(indexPath: IndexPath(row: workerLocationsArr.count - 1, section: 1))
        }
        
        rowToUpdate = -1
        placeSelected = nil
    }
    
    func scrollTableViewToMiddleScreen(indexPath : IndexPath) {
        locationsTbl.scrollToRow(at: indexPath, at: UITableViewScrollPosition.middle, animated: true)
    }
    
    //MARK: - Slider
    func textToImage(drawText: NSString, inImage: UIImage, atPoint: CGPoint) -> UIImage{
        
        let textColor = UIColor.red
        let textFont = UIFont(name: "Rubik-Regular", size: 12)!
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(inImage.size, false, scale)
        
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
        inImage.draw(in: CGRect(origin: CGPoint.zero, size: inImage.size))
        
        let rect = CGRect(origin: atPoint, size: inImage.size)
        drawText.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //        (newImage as UIImageView).transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI))
        
        return (newImage?.imageRotatedByDegrees(degrees: 0, flip: true))!//newImage!
    }
    


    //MARK: - Google Places
    func placeAutocomplete(stringToSearch : String) {
        
//        let filter = GMSAutocompleteFilter()
//        filter.type = GMSPlacesAutocompleteTypeFilter.geocode|GMSPlacesAutocompleteTypeFilter.city
        
        
        
        placesClient.autocompleteQuery(/*"Sydney Oper"*/stringToSearch, bounds: nil, filter: nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                self.placesArr = results
                self.autoCompletePlacesTbl.isHidden = false
                self.autoCompletePlacesTbl.reloadData()
                for result in results {
                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
            }
        })
    }
    
    func getPlaceByID(placeId : String) {
        //let placeID = "ChIJV4k8_9UodTERU5KXbkYpSYs"
        
        placesClient.lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeId)")
                return
            }
            
            //print("Place name \(place.name)")
            //print("Place address \(place.formattedAddress)")
            //print("Place placeID \(place.placeID)")
            //print("Place attributions \(place.attributions)")
            self.placeSelected = place.coordinate
            var radius :CLLocationDistance
            radius = CLLocationDistance(self.radiusSlider.selectedMaximum * 1000)
            
            self.addMarkerAndCircle(position: place.coordinate, radius: radius, zoom: 8)
            
        })
        
    }
    
    func addMarkerAndCircle(position : CLLocationCoordinate2D, radius : CLLocationDistance, zoom : Float) {
//        self.mapView.clear()
        marker?.map = nil
        circle?.map = nil
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: /*place.coordinate*/position.latitude, longitude: /*place.coordinate*/position.longitude, zoom: zoom)
        self.mapView.camera = camera
        
        if self.marker == nil {
            self.marker = GMSMarker(position: /*place.coordinate*/position)
            self.marker?.isDraggable = true
        } else {
            //self.marker?.accessibilityElementsHidden = true
            self.marker?.position = /*place.coordinate*/position
        }
        self.marker?.map = self.mapView
        
        
        if self.circle == nil {
            self.circle = GMSCircle(position: /*place.coordinate*/position, radius: radius)
        } else {
            self.circle?.position = /*place.coordinate*/position
            self.circle?.radius = radius
        }
        self.circle?.map = self.mapView
        
        self.circle?.strokeColor = UIColor.orange2
        self.circle?.fillColor = UIColor.orange2
        
        getEmployerCount(latitude: (position.latitude.description), longitude: (position.longitude.description), radiusInMeters: (radiusSlider.selectedMaximum / 10) *
            10
        )
    }

    func changeCircleRadius(radius : Double) {
        if self.circle != nil {
            self.circle?.radius = CLLocationDistance(radius)
        }
    }
    
    func resetData() {
        placeTxt.text = ""
        placeSelected = nil
//        radiusSlider.setValue(radiusSlider.minimumValue, animated: true)
        radiusSlider.selectedMaximum = radiusSlider.minValue
        //mapView.clear()
        
        marker?.map = nil
        circle?.map = nil
    }
    
    func addEmployersMarkers(employersLocationsArr : Array<EmployerLocation>) {
        var positionArr = Array<CLLocationCoordinate2D>()
        for employerLoc in employersLocationsArr {
            var loc = CLLocationCoordinate2D()
            loc.latitude = CLLocationDegrees(employerLoc.nvLat)!
            loc.longitude = CLLocationDegrees(employerLoc.nvLng)!
            
            var isExist = false
            while !isExist {
                for i in stride(from: 0, to: positionArr.count, by: 1) {
                    if getRoundNum(number: loc.longitude) == getRoundNum(number: positionArr[i].longitude) && getRoundNum(number: loc.latitude) == getRoundNum(number: positionArr[i].latitude) {
                        isExist = true
                        loc.longitude = loc.longitude + 0.00005
                        loc.latitude = loc.latitude + 0.00005
                        break
                    }
                }
                
                isExist = !isExist
            }
            
            positionArr.insert(loc, at: positionArr.count)
            
            let marker = GMSMarker(position: /*CLLocationCoordinate2D(latitude: CLLocationDegrees(employerLoc.nvLat)!, longitude: CLLocationDegrees(employerLoc.nvLng)!)*/loc)
            print("\(marker.position.latitude), \(marker.position.longitude)")
            marker.map = self.mapView
            
            let img1 = UIImageView(image: UIImage(named: "marker_busines"))
            let img2 = UIImageView()
            img2.frame = CGRect(x: img1.frame.width * 0.08, y: img1.frame.width * 0.13, width: img1.frame.width * 0.84, height: img1.frame.width * 0.84)
            img2.layer.cornerRadius = img2.frame.width / 2
            img2.layer.masksToBounds = true
            
            var stringPath = api.sharedInstance.buldUrlFile(fileName: (employerLoc.nvLogo))
            stringPath = stringPath.replacingOccurrences(of: "\\", with: "/")
            
            img2.downloadedFrom(link: stringPath, contentMode: .scaleToFill)
            
            img1.addSubview(img2)
            
            marker.iconView = img1
        }
    }
    
    func getRoundNum(number : Double) -> Double {
        return Double(round(10000 * number) / 10000)
    }
    
    //MARK: - Delegates
    // remove location
    func removeCell(cellNumber: Int) {
        //removeLocation(cellNumber: cellNumber)
        workerLocationsArr[cellNumber].bDeleted = true
        UpdateWorkerAskingLocation(workerAskingLocation: workerLocationsArr[cellNumber], indexInArray: cellNumber)
    }
    
    //OpenAnotherPageDelegate
    func openViewController(VC: UIViewController) {
        if VC is BasePopUpViewController {
            self.present(VC, animated: true, completion: nil)
        }
    }
    
    //MARK: - Server Functions
    func getWorkerLocations() {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String, AnyObject> {
                        var workerLocation = WorkerLocation()
                        workerLocation = workerLocation.getWorkerLocationFromDic(dic: dicResult)
                        
                        // diaplay worker locations
                        self.workerLocationsArr.removeAll()
                        self.workerLocationsArr = workerLocation.lWorkerAskingLocations
                        
                        for _ in stride(from: 3, to: self.workerLocationsArr.count, by: 1) {
                            self.changeScrollView.changeScrollViewHeight(heightToadd: 50)
                        }
                        
                        self.locationsTbl.reloadData()
                        
                        // add markers of employers
                        self.addEmployersMarkers(employersLocationsArr: workerLocation.lEmployerLocations)
                        
                        //self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetWorkerAskingLocations)
    }

    func UpdateWorkerAskingLocation(workerAskingLocation : WorkerAskingLocation, indexInArray : Int) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
        //WorkerAskingLocation workerAskingLocation, int iUserId
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["workerAskingLocation"] = workerAskingLocation.getWorkerAskingLocationAsDic() as AnyObject?

        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Dictionary<String, AnyObject> {
                        var workerLocation = WorkerAskingLocation()
                        workerLocation = workerLocation.getWorkerAskingLocationFromDic(dic: dicResult)
                        
                        if workerAskingLocation.bDeleted { // delete row
                            self.workerLocationsArr.remove(at: indexInArray)
                            Alert.sharedInstance.showAlert(mess: "נמחק בהצלחה!")
                        } else if workerAskingLocation.iWorkerAskingLocationId > 0 {  // update row
                            self.workerLocationsArr.remove(at: indexInArray)
                            self.workerLocationsArr.insert(workerLocation, at: indexInArray)
                            Alert.sharedInstance.showAlert(mess: "התעדכן בהצלחה!")
                        } else { // add row
                            self.workerLocationsArr.insert(workerLocation, at: self.workerLocationsArr.count)
                            Alert.sharedInstance.showAlert(mess: "נוסף בהצלחה!")
                            if self.workerLocationsArr.count > 3 {
                                self.changeScrollView.changeScrollViewHeight(heightToadd: 50)
                            }
                        }
                        
                        Global.sharedInstance.bWereThereAnyChanges = false
                        
                        self.resetData()
                        self.locationsTbl.reloadData()
                        //self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.UpdateWorkerAskingLocation)
    }

    func getEmployerCount(latitude : String, longitude : String, radiusInMeters : Float) {
        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
        var dic = Dictionary<String, AnyObject>()
//string nvLng, string nvLat, decimal nRadius, int iWorkerUserId       
        
        dic["nvLat"] = latitude as AnyObject
        dic["nvLng"] = longitude as AnyObject
        dic["nRadius"] = radiusInMeters as AnyObject
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        
        api.sharedInstance.goServer(params : dic,success : {
            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
            
            print(responseObject as! [String:AnyObject])
            print(responseObject as Any)
            var dic  =  responseObject as! [String:AnyObject]
            if let _ = dic["Error"]?["iErrorCode"] {
                Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                
                if dic["Error"]?["iErrorCode"] as! Int ==  0 {
                    if let dicResult = dic["Result"] as? Int {
                        
                        if self.marker != nil {
                            self.marker?.tracksInfoWindowChanges = true
                            self.marker?.snippet = String(dicResult) + " מעסיקים"
                            self.mapView.selectedMarker = self.marker
                            
                        }
                        //self.domainsTbl.reloadData()
                    } else {
                        Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                    }
                } else {
                    Alert.sharedInstance.showAlert(mess: "שגיאה בלתי צפויה!")
                }
            }
        } ,failure : {
            (AFHTTPRequestOperation, Error) -> Void in
            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
            Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.NO_INTERNET, comment: ""))
        }, funcName : api.sharedInstance.GetEmployerCount)
    }
    
    func getPlaceNameByCoordinates(coordinates : CLLocationCoordinate2D) {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            var place : String = ""
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary)
            
            // Location name
            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                print(locationName)
                place = place + (locationName as String)
            }
            
            // Street address
            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                print(street)
                //                    place = place + (street as String)
            }
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                print(city)
                place = place + ", " + (city as String)
            }
            
            // Zip code
            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                print(zip)
            }
            
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? NSString {
                print(country)
                place = place + ", " + (country as String)
            }
            
            self.placeTxt.text = place
            
        })
        
    }

    //MARK: - Drag Marker
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
//        print("did drag")
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
//        print("begin drag")
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        if circle != nil {
            circle?.position = marker.position
        }
        
            self.getPlaceNameByCoordinates(coordinates: marker.position)
            
    }
    
    
    
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.placeSelected = coordinate
        var radius : CLLocationDistance = 10 * 1000
        if circle != nil {
            radius = (circle?.radius)!
        }
        self.addMarkerAndCircle(position: coordinate, radius: radius, zoom: mapView.camera.zoom)
        
        self.getPlaceNameByCoordinates(coordinates: coordinate)
    }
    
    
//    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
//        self.placeSelected = coordinate
//        self.addMarkerAndCircle(position: coordinate, radius: 10 * 1000)
//        
//        self.getPlaceNameByCoordinates(coordinates: coordinate)
//    }

}
