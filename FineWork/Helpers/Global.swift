
//
//  Global.swift
//  FineWork
//
//  Created by Racheli Kroiz on 15.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//
// -- enums

import UIKit


// דלגייט המופעל בעת קבלת נוטיפיקציות
protocol ReceiveRemoteNotificationDelegate {
    func onReceiveRemoteNorificarion(userInfo : [AnyHashable : Any])
}
// דלגייט המופעל בעת קבלת נוט' של רענון מסכים
protocol NotificationRefreshDelegate {
    func onNorificarionRefresh(userInfo : [AnyHashable : Any])
}

class Global: NSObject {
    static let sharedInstance : Global = {
        let instance = Global()
        return instance
    }()
    
 //MARK: - Variables
    
    //MARK: -- PersonalInformationModelViewControllerHeight
    var personalInformationModelHeight:CGFloat = 0.0
    var user:User = User()
    var rtl:Bool = false
    var iLanguageId = 1
    var citiesArr = [String:Int]()
    var worker:Worker = Worker()
    var token = ""
    // worker code tables
    var workerCodeTables : Array<SysTableDictionary> = []
    var languageCodeTable : String = "Language"
    var bankCodeTable : String = "Bank"
    var cancellationReasonType : String = "CancellationReasonType"

    
    var employer = Employer()
    // employer code tables
    var employerCodeTables : Array<SysTableDictionary> = []
    var EmployeeNumberTypeCodeTable : String = "EmployeeNumberType"
    var MainActivityFieldTypeCodeTable : String = "MainActivityFieldType"
    var lLookingForYouWorkers : Array<LookingForYouWorkers> = []
    
    // worker home page
    var workerHomePage = WorkerHomePage()
    var lWorkerOfferedJobs : Array<WorkerOfferedJob> = []
    
    // global - Were There Any Changes ?
    var bWereThereAnyChanges : Bool = false
    var bIsOpenFiles = false
    
       //דלגייט מסוג : ReceiveRemoteNotificationDelegate
     //כל מסך מקושר אליו וכך מזהים את הנוטיפיקציות
    var receiveRemoteNotificationDelegate : ReceiveRemoteNotificationDelegate?
    
    var notificationRefreshDelegate : NotificationRefreshDelegate?
 
    //MARK: - Functions
    
    //MARK: -- Parse Functions
    
    func parseJsonToInt(intToParse:AnyObject)->Int
    {
        let c = intToParse
        let int:String = (c.description)!
        if let checkIfParse = Int(int)
        {
            return checkIfParse
        }
        return 0
    }
    func parseJsonToFloat(floatToParse:AnyObject)->Float
    {
        let num = floatToParse
        let floatNum:String = (num.description)
        if (floatNum as NSString).floatValue != 0
        {
            return (floatNum as NSString).floatValue
        }
        return 0
    }
    func stringToImage(str:String)->UIImage{
 let dataDecoded:NSData = NSData(base64Encoded: str, options: NSData.Base64DecodingOptions(rawValue: 0))!
        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
        
        print(decodedimage)
        return decodedimage
    }
    func parseJsonToDouble(doubleToParse:AnyObject)->Double
    {
        let num = doubleToParse
        let doubleNum:String = (num.description)
        if (doubleNum as NSString).doubleValue != 0
        {
            return (doubleNum as NSString).doubleValue
        }
        return 0
    }
    
    func parseJsonToString (stringToParse:AnyObject)-> String
    {
        let c:String = stringToParse.description
        if c == "<null>"
        {
            return ""
        }
        else if let checkIfParse:String = c
        {
            return checkIfParse
        }
        return ""
    }
    
    func parseJsonToAnyObject (anyToParse:AnyObject)-> AnyObject?
    {
        let c = anyToParse
        let int:String = (c.description)!
        if let checkIfParse = Int(int)
        {
            return checkIfParse as AnyObject
        }
        if let checkIfParse = String(int)
        {
            return checkIfParse as AnyObject
        }
        if let checkIfParse = Double(int)
        {
            return checkIfParse as AnyObject
        }
        if let checkIfParse = Float(int)
        {
            return checkIfParse as AnyObject
        }
        return nil
    }
    
    // parse with return null(ayala avitan)
    //MARK: -- Parse Functions
    
    func parseJsonToIntOptionNil(intToParse:AnyObject)->Int?
    {
        let c = intToParse
        let int:String = (c.description)!
        if let checkIfParse = Int(int)
        {
            return checkIfParse
        }
        return nil
    }
    func parseJsonToFloatOptionNil(floatToParse:AnyObject)->Float?
    {
        let num = floatToParse
        let floatNum:String = (num.description)
        if (floatNum as NSString).floatValue != 0
        {
            return (floatNum as NSString).floatValue
        }
        return nil
    }
    
    func parseJsonToDoubleOptionNil(doubleToParse:AnyObject)->Double?
    {
        let num = doubleToParse
        let doubleNum:String = (num.description)
        if (doubleNum as NSString).doubleValue != 0
        {
            return (doubleNum as NSString).doubleValue
        }
        return nil
    }
    
    func parseJsonToStringOptionNil(stringToParse:AnyObject)-> String?
    {
        let c:String = stringToParse.description
        if c == "<null>"
        {
            return nil
        }
        else if let checkIfParse:String = c
        {
            return checkIfParse
        }
        return nil
    }
    //
    
    func getDateFromString(dateString: String)->NSDate
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.dateStyle = .short
    
     //  dateFormatter.timeZone = TimeZone(identifier: "GMT+3")
        let date = dateFormatter.date(from: dateString)
        
        // add a few hours to date because the date with 00:00 at night, therefore get previus day
        let calendar = Calendar.current
        let date2 = calendar.date(byAdding: .hour, value: 5, to: date!)
        
        return date2! as NSDate
        
    }
   
    func getDateFromString2(dateString: String)->Date
    {
        //let  dateString = "/Date(1503401902803)/"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
       // dateFormatter.dateStyle = .short
        //        dateFormatter.timeZone = TimeZone(identifier: "GMT+3")
        let date = dateFormatter.date(from: dateString)
        
        // add a few hours to date because the date with 00:00 at night, therefore get previus day
        let calendar = Calendar.current
        let date2 = calendar.date(byAdding: .hour, value: 5, to: date!)
        
        return date2!
        
    }
    //ממיר דייט לדייט לשליחה לשרת
    func convertNSDateToString(dateTOConvert:NSDate)-> String
    {
        print("date : \(dateTOConvert)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        var myDateString = String(Int64(dateTOConvert.timeIntervalSince1970 * 1000))
        myDateString = "/Date(\(myDateString))/"
        
        return myDateString
    }
    
    func getStringFromDateString(dateString: String) -> String
    {
        if (dateString.contains("Date")){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if dateString != ""
        {
            var fullName = dateString.components(separatedBy: "(")
            
            let lastName: String? = fullName[1]
            var fullNameArr = lastName!.components(separatedBy: "+")
            let lastNam: String? = fullNameArr.count > 0 ? fullNameArr[0] : lastName
            
            let myDouble = Double(lastNam!)
            let date = NSDate(timeIntervalSince1970: myDouble!/1000.0)
            
            return dateFormatter.string(from:date as Date)
            } }
        else {
            return dateString }
        return ""
    }
    //מחזיר הפרשי זמן
    func   getStringDiffBetweenCurrentDate(date :String , time : String)-> String
    {
        let currentDate = Date()
        let calendar = Calendar.current
        _ = calendar.component(.hour, from: currentDate)
        _ = calendar.component(.minute, from: currentDate)
        
        var   dateO = Date()
        dateO =   date.toDate()

//        let dateFormatter = DateFormatter()
//     dateO = dateFormatter.date(from:date)!
//        dateO = getDateFromString(dateString: date) as Date
    return dateO.offsetFrom(date: currentDate)
     
//        var timeInMilliseconds : CLong = 0
//    
//   
//     timeInMilliseconds = getLongDiffBetweenCurrentDate(date);
//    
//    if  time != "" {
//        var hour = 0 : Int
//        var min = 0 :Int
//    try {
//    hour = Integer.parseInt(time.split(":")[0]);
//    min = Integer.parseInt(time.split(":")[1]);
//    } catch (Exception e) {
//    e.printStackTrace();
//    }
//    timeInMilliseconds += (min * 60 * 1000);
//    timeInMilliseconds += (hour * 60 * 60 * 1000);
//    }
//    
//    //        %02d
//    return String.format(Locale.US, "%02d %02d:%02d:%02d",
//    TimeUnit.MILLISECONDS.toDays(timeInMilliseconds),
//    TimeUnit.MILLISECONDS.toHours(timeInMilliseconds) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(timeInMilliseconds)),
//    TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeInMilliseconds)),
//    TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds))
//    );
        return ""
    }
    //מחזיר הפרשי זמן
    func calicuateDaysBetweenTwoDates(start: Date, end: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: start) else {
            return 0
        }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: end) else {
            return 0
        }
        return end - start+1
    }
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    //ממירה תמונה לסטרינג
    func setImageToString(image:UIImage)->String{
//        let data = UIImagePNGRepresentation(image)
//        let formatter = ByteCountFormatter()
//        
//        formatter.allowedUnits = ByteCountFormatter.Units.useBytes
//        formatter.countStyle = ByteCountFormatter.CountStyle.file
//        
//        let formatted = formatter.string(fromByteCount: Int64((data?.count)!))
//        
//        print(formatted)
        
        var imageData:String/*NSString*/
        var dataForJPEGFile:NSData
        
        dataForJPEGFile = UIImageJPEGRepresentation(image, 1)! as NSData
//        dataForJPEGFile = UIImagePNGRepresentation(image)! as NSData
//        dataForJPEGFile = UIImageJPEGRepresentation(image, 1)! as NSData
        let imageSize: Int = dataForJPEGFile.length
//        print("size of image in KB: %f ", imageSize/1024.0)
        if imageSize <= 4000000{//ולא 4 מליון כיוון שמוצא את משקל התמונה אחרי המרה שהוא הרבה יותר כבד
//            image = UIImage(data:dataForJPEGFile as Data)!
//            let newSize:CGSize = CGSize(width: image.size.width/2, height: image.size.height/2)
//            UIGraphicsBeginImageContext(newSize)
//            
//            image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
//            let newImage:UIImage=UIGraphicsGetImageFromCurrentImageContext()!
//            UIGraphicsEndImageContext()
//            //data=UIImagePNGRepresentation(newImage)!
//            dataForJPEGFile = UIImageJPEGRepresentation(newImage, 0.2)! as NSData//picks down
            
            
//            imageData = dataForJPEGFile.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) as NSString
            imageData = dataForJPEGFile.base64EncodedString(options: [])
            
            return imageData/* as String*/
        }
        else{
            let newImage = resizeImage(image: image, newWidth: image.size.width/2)
            dataForJPEGFile = UIImageJPEGRepresentation(newImage, 1)! as NSData
            let imageSize: Int = dataForJPEGFile.length
            if imageSize <= 4000000{
                imageData = dataForJPEGFile.base64EncodedString(options: [])
                
                return imageData/* as String*/
            }
            return ""
        }
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        
        let imageData = UIImagePNGRepresentation(image)
        let dataStr = imageData?.base64EncodedString(options: [])
        // let base64String = imageData.base64EncodedStringWithOptions(.allZeros)
        
        return dataStr!
        
    }
    
    
    
//    func getImageFromUrl(path:String)->UIImage?{
////        var url : NSString = api.sharedInstance.buldUrlFile(fileName: path) as NSString
////   
////        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
////             url.addingPercentEncoding(withAllowedCharacters: String.Encoding.utf8.rawValue)!  as NSString
////        if let url = NSURL(string: urlStr as String) {
////            if let data = NSData(contentsOf: url as URL) {
////            
////                controller?.img = UIImage(data: data as Data)
////                GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.present(controller!, animated: true, completion: nil)
////            }
////        }
//    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    //return numdays between 2 dates
    func YearsBetween2Dates(date1:Date,date2:Date)->Int
    {
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.year], from: date1, to: date2 as Date)
        
        return abs(components.year!)   // This will return the number of day(s) between dates
    }
    
    func AgeOFCurrentYear(date1:Date,date2:Date)->Int{
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
//        let date = Date()
        var components = calendar.dateComponents([.hour, .day, .month, .year], from: date2)
        components.setValue(12, for: .month)
        components.setValue(1, for: .day)
        let startOfMonth = Calendar.current.date(from: components)!
//            
        dateFormatter.dateFormat = "dd-MM-yyyy"
//        print(dateFormatter.string(from: lastMonth))
//        
//        let comp: DateComponents = Calendar.current.dateComponents([.hour, .day, .month, .year], from: lastMonth)
//        let startOfMonth = Calendar.current.date(from: comp)!
        print(dateFormatter.string(from: startOfMonth))
        
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = Calendar.current.date(byAdding: comps2, to: startOfMonth)
        print(dateFormatter.string(from: endOfMonth!))
        
        let componentsYears = calendar.dateComponents([.year], from: date1, to: endOfMonth! as Date)
        return componentsYears.year!
    }
    //מחזיר הפרש זמן מעכשיו עד התאריך שמתקבל
    let timeFormatter = DateFormatter()
    func getHourDifferencesToday(serverDate : String) -> String {
        timeFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        var startHour :String = ""
        startHour =   startHour.convertDateFromServer(dateString: serverDate, dateFormat: "dd/MM/yyyy HH:mm:ss", bAddHours: false)
        let datestring = timeFormatter.string(from: Date())
        var  date1 : Date = timeFormatter.date(from: datestring)!
        let  date2 : Date = timeFormatter.date(from: startHour)!
        return date1.offsetFrom(date: date2)
    }
    //מיון מערך שיהיה ללא כפולים
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    //מוצאת ערך בטבלה לפי מזהה
    func getIndexOfSysTableList( iId : Int, sysTableList : Array<SysTable> ) -> Int
    {
        for i in 0 ..< sysTableList.count {
            
            if sysTableList[i] != nil && iId == sysTableList[i].iId {
                return i
            }
        }
        return -1

    }
    //set animatiom in Image
    func addGifToImage(nameImage : String) ->UIImageView
    {
        let jeremyGif = UIImage.gifImageWithName(nameImage)
        let imageView = UIImageView(image: jeremyGif)
        return imageView
    }
    
//    func BuildUrlForImage(urlfromServ:String) -> String
//    {
//        return "http://qa.webit-track.com/FineWorkWSDev/Files/\(urlfromServ)"
//    }
    
    func editPhon(phon:String)->String{
        var numbersString = ""
        for tempChar in (phon.characters){
            if "0"..."9" ~= tempChar {
                numbersString += String(tempChar)
            }
        }
        let startIndex = numbersString.startIndex
        if numbersString.characters.count < 10{
            if numbersString.characters.count >= 3{
                numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 2))
                if numbersString.characters.count >= 7{
                    numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 6))
                }
            }
        }else{
            if numbersString.characters.count >= 4{
                numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 3))
                if numbersString.characters.count >= 8{
                    numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 7))
                }
            }
        }

        return numbersString
    }
    
    // get string from html
    func getStringFromHtml(string: String) -> String? {
        do {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try /*NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)*/
                NSAttributedString(data: d, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
                return str.string
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
    
    //MARK: - error to text fields
    func setErrorToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.red.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setValidToTextField(sender : UITextField) {
        sender.layer.borderColor = UIColor.clear.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setErrorToLabel(sender : UILabel) {
        sender.layer.borderColor = UIColor.red.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
    func setValidToLabel(sender : UILabel) {
        sender.layer.borderColor = UIColor.clear.cgColor
        sender.layer.borderWidth = 0.5
        sender.layer.cornerRadius = 1
    }
    
        func setButtonCircle(sender : UIButton, isBorder : Bool) {
        if isBorder {
            sender.layer.borderColor = sender.titleLabel?.textColor.cgColor
            sender.layer.borderWidth = 1
        }
        sender.layer.cornerRadius = sender.frame.height / 2
    }

  /*  func setLableCircle(sender : UILabel, isBorder : Bool) {
        if isBorder {
            let color = sender.textColor
            sender.layer.borderColor = color as! CGColor?
            sender.layer.borderWidth = 1
        }
        sender.layer.cornerRadius = sender.frame.height / 2
    }*/
    //MARK: -- global dictionary
    let familyStatusArr : Array<String> = ["רווק/ה", "נשוי/אה", "גרוש/ה", "אלמן/ה", "פרוד/ה"]

    func openUrl(url: String){
        let url = URL(string: url)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
}
enum FamilyStateKey : Int {
    case Single = 1
    case Married = 2
    case Divorcee = 3
    case Widow = 4
    case Separated = 21
    
    func count() -> Int {
        return 5
    }
}

//רמת שפה
enum LanguageLevelType : Int {
    case MotherTongue = 46
    case Good = 47
    case Medium = 48
    case Low = 49
}

//אמצעי תשלום
enum PaymentMethodType : Int {
    case BankTransfer = 50
    case CashWithdrawal = 51
    
    // for employer
    case DirectDebit = 149
    case CreditCard = 150
}

enum FamilyStateValue : String {
    case Single = "רווק/ה"
    case Married = "נשוי/אה"
    case Divorcee = "גרוש/ה"
    case Widow = "אלמן/ה"
    case Separated = "פרוד/ה"
    
    func count() -> Int {
        return 5
    }
}

//יום בשבוע
enum DayType : Int {
    case Sunday = 1
    case Monday = 2
    case Tuesday = 3
    case Wednesday = 4
    case Thursday = 5
    case Friday = 6
    case Saturday = 7
}

//זמינות
enum AvailabiltyType : Int {
    case PartialJob = 68
    case FullJob = 69
    case PartialAndFullJob = 167
    case NoChooseDomains = 168
}

//סטטוס פרטי תשלום למעסיק
enum EmployerPaymentMethodStatusType : Int {
    case New = 152
    case Approved = 153
    case NotApproved = 154
    case Inactive = 155
    case Canceled = 156
    case CancellationRequest = 157
}

//סוג דף הבית לעובד
enum WorkerHomePageType : Int {
    case NewWorkerCompleteProfile = 85
    case NewWorkerNotCompleteProfile = 84
    case ExistWorker = 83
}

//enum PaymentMethodType : Int {
//    
//
//}


