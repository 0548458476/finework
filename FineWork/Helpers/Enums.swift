//
//  Enums.swift
//  FineWork
//
//  Created by User on 23/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

//import UIKit
//
//class Enums: NSObject {
//
//}

//סוג משתמש
enum UsetType : Int {
    case Administrator = 16
    case Employer = 17
    case AuthorizedEmployer = 72
    case Employee = 18
}

//סוג אוכלוסיה
enum PopulationType : Int {
    case General = 86
    case Youth = 87
    case Retired = 88
    case ForeignWorkers = 89
}

//תקופת העסקה
enum EmployerJobPeriodType : Int {
    case ConstantWorker = 90
    case TemporaryWorker = 91
}

//סוג זמן המשרה
enum EmployerJobScheduleType : Int {
    case OneTimeJob = 92
    case ConstantHoursJob = 93
    case ChangingHoursJob = 94
}

//סוג מיקום
enum WorkingLocationType : Int {
    case myAddress = 95
    case anotherAddress = 96
}

//סוג עובד למשרה
enum EmployerJobWorkerType : Int {
    case NewWorker = 176
    case KnownWorker = 177
}

//סטטוס משרה מוצעת
enum OfferJobStatusType : Int{
    case new = 97 //חדש
    case interesting = 98 //מעניין אותי
    case apply = 99 //הוגשה מעמדות
    case want_your = 100 //רוצים אותך
    case wait_begin_work = 135 // ממתין לתחילת עבודה
    case active = 136 //נוכחות פעילה
    case cancel = 137 //בוטל
    case occupied = 138 //מאויש
    case whiting_verification_details = 190 //ממתין לאימות פרטים
    case whiting_for_rank = 188 //ממתין לדירוג
    case job_cancel = 186 //משרה מבוטלת
    case job_cancel_by_edit = 191 //משרה בוטלה ע"י עריכה
    case finish_time = 507 //תם הזמן
    case process_extened = 508 //תהליך הגיוס מתארך
    case waiting = 515 //תהליך הגיוס מתארך - המתנה
    case not_answer  = 509 //לא ענית - עבר לעובד אחר
    case staffed_and_not_run = 592 //אויש ולא התמודד
}
//סטטוס משרה - מעסיק
enum EmployerJobStatusType : Int{
    case Canceled = 131
    case New = 132
    case LookingForYou = 133
    case FirstCandidate = 134
    case SecondCandidate = 140
    case WaitingForContinueProcess = 141
    case AwaitingWorkerApproval = 142
    case WaitingToGetStarted = 143
    case WaitingForWorkerIdentification = 144
    case ActivePresence = 145
    case Closed = 146
    case Expired = 148
    case SentYourOrder = 174
    case WaitingForRating = 187
    case EndJob = 197
    case CanceledByUpdate = 191
    case RequestForJobExtension = 230
    case RequestForJobtermination = 195
    case RequestForJobabandonment = 196
    case NotFoundYet=510
    case ModificationOrExtension=511
    case PendingApproval=512
    case LookingForYouAmount = 491
    case FoundCandidateAmount = 493
}

enum NotificationType  : Int{
    case NOT_NOTIFICATION = 100
    case JOB_ABANDONMENT = 1
    case JOB_ABANDONMENT_CLOSE_JOB = 2
    case JOB_TERMINATION = 3
    case JOB_WORKER_STAR = 4
    case JOB_EXTENSION = 5
    case WORKER_JOB_UPDATE = 6
    case EMPLOYER_JOB_UPDATE = 7
    case REFRESH_CHAT = 8
    case REFRESH_SYSTEM_MESSAGE = 9
    case END_TIME_REPORTING = 10
    case DELETE_EMPLOYER_JOB = 11
    case JOB_ABANDONMENT_ANSWER_MISUNDERSTANDING = 13
    case JOB_ABANDONMENT_ANSWER_NOT_WANT_CONTINUE = 25
    case JOB_TERMINATION_WORKER_WANT_CONTINUE = 16
    case JOB_TERMINATION_WORKER_STOP_JOB = 21
    case TURN_TO_NEXT_CANDIDATE = 19
    case MINUTES_BEFORE_END  = 28
}

//סטטוס תצוגה בלוח שנה
enum StatusDateInCalander : Int {
    case regularNoInMonth = 0 //רגיל
    case regularInMonth = 1 //רגיל
    
    case noSelected = 2 //לא בחור
    case yesSelected = 3 //בחור
    
}
//סוג דירוג
enum RankingType : Int{
    case RankByWorker = 180//דרוג עי העובד
    case RankByEmployer = 181//דרוג עי המעסיק
}

//מצב דווח של העובד
enum ReportingCheckStatus : Int{
    case REGULAR = 1
    case REPORTING_ACTIVE = 219
    case BREAK_WITH_PAYMENT = 220
    case BREAK = 223
    case BETWEEN_SHIFTS = 513
    case START_SHIFT = 514
    
    
}
enum StepType : Int {
   case START = 1
   case BREAK = 2
   case CONTINU = 3
   case END = 4
    
    //not server
   case BETWEEN_SHIFTS = 513
  case  START_SHIFT = 514
   
}
// תאריך החל ב:
enum DateType : Int {
    case Regular = 0
    case Holiday = 1
    case HolidayEve = 2
    case Shabbat = 3
    case Friday = 4
}
//הארכת עבודה
enum jobExtension : Int {
    case OK = 228
    case NOT_OK = 229
    case REQUEST = 227
}

enum GlobalParametersType : Int {
    case HolidayEve = 8
    case Holiday = 9
}

//סוג טקסט - האם הוא בפני עצמו או על ויו אחר (לצורך העלאת המסך לנקודה המתאימה כשהמקלדת נפתחת)
enum TextFieldType : Int {
    //    case Simple = 0  //בפני עצמו (default)
    case OnView = 1  //מעל ויו אחר
    case OpenListUnder = 2  //נפתחת רישמה מתחתיו (בחיפושים וסינונים)
}
