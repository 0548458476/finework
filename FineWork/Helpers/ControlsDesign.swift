//
//  ControlsDesign.swift
//  FineWork
//
//  Created by Racheli Kroiz on 6.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
//מחלקה כללית המכילה פונקציות לאתחול פקדים
class ControlsDesign: NSObject {
    static let sharedInstance : ControlsDesign = {
        let instance = ControlsDesign()
        return instance
    }()
    
   // rgb(115, 189, 191)
  //MARK:- Varibals
    //MARK:-- COLORS
    var colorTurquoise:UIColor = UIColor(red: 0.45, green: 0.74, blue: 0.75, alpha: 1.0)
    var colorTurquoiseWithOpacity:UIColor = UIColor(red: 0.45, green: 0.74, blue: 0.75, alpha: 0.6)
    
    var colorPurple:UIColor =  UIColor(red: 0.4, green: 0.2, blue: 0.4, alpha: 1.0)//673366
    
    var colorDarkPurple:UIColor =  UIColor(red:0.27, green:0.11, blue:0.27, alpha:1.0)//441c44
    
    var colorLightDarkPurple:UIColor =  UIColor(red:0.92, green:0.90, blue:0.93, alpha:1.0)
    
    
    var colorFucsia:UIColor =  UIColor(red:198/255, green:36/255, blue:103/250, alpha:1.0)
    var colorWhiteGray:UIColor =  UIColor(red:250/255, green:250/255, blue:252/250, alpha:1.0)
    var colorGray:UIColor = UIColor(red: 64/255, green: 64/255, blue: 65/255, alpha: 1.0)
    var colorGrayText:UIColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)

    //var colorBlue:UIColor = UIColor(red: 48/255, green: 78/255, blue: 168/255, alpha: 1.0)
    var colorRed:UIColor = UIColor(red: 235/255, green: 42/255, blue: 32/255, alpha: 1.0)
     var colorOrang:UIColor = UIColor(red: 243/255, green: 128/255, blue: 48/255, alpha: 1.0)
    var colorOrangLight:UIColor = UIColor(red: 254/255, green: 240/255, blue: 226/255, alpha: 1.0)

      var colorBlue:UIColor = UIColor(red: 0/255, green: 184/255, blue: 222/255, alpha: 1.0)
    var colorBrown : UIColor = UIColor(red: 155/255, green: 77/255, blue: 40/255, alpha: 1.0)
    //MARK:-- Fonts
    var fontAppicationMedium:UIFont = UIFont(name: "RUBIK-MEDIUM", size: 18.0)!
    var fontAppicationLight:UIFont = UIFont(name: "RUBIK-LIGHT", size: 18.0)!
    var fontAppicationRegular:UIFont = UIFont(name: "RUBIK-REGULAR", size: 18.0)!
    
  //MARK: - Border functions
    

    var colorGrayShadow:UIColor = UIColor(red: 225/250, green:  225/250, blue:  224/250, alpha:0.75)
    var fontAppicationIconFont:UIFont = UIFont(name: "FontAwesome", size: 12.0)!

    func addTopAndBottomBorderWithColor(color: UIColor, width: CGFloat,any :AnyObject) {
        self.addTopBorderWithColor(color: color, width: width, any: any as! UIView)
        self.addBottomBorderWithColor(color: color, width: width, any: any as! UIView)
    }
    
    func addTopBottomRightLeftBorderWithColor(color: UIColor, width: CGFloat,any :AnyObject) {
        self.addTopBorderWithColor(color: color, width: width, any: any as! UIView)
        self.addBottomBorderWithColor(color: color, width: width, any: any as! UIView)
        self.addRightBorderWithColor(color: color, width: width, any: any as! UIView)
        self.addLeftBorderWithColor(color: color, width: width, any: any as! UIView)
    }
    
    //design func that get color, width and view and add top border
    func addTopBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: any.frame.size.width, height: width)
        any.layer.addSublayer(border)
    }
    //design func that get color, width and view and add buttom  border
    func addBottomBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {

        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: (any.frame.size.height - width - 3), width: any.frame.size.width, height: width)
        any.layer.addSublayer(border)
    }
    
    //design func that get color, width and view and add Right border
    func addRightBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: any.frame.size.width - width, y: 0, width: width, height: any.frame.size.height)
        any.layer.addSublayer(border)
    }
    
    //design func that get color, width and view and add Left border
    func addLeftBorderWithColor(color: UIColor, width: CGFloat,any :UIView) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: any.frame.size.height)
        any.layer.addSublayer(border)
    }
    
//    static let myPurple2 = UIColor(rgb: 0x703f71)
}

extension UIColor {
    
    
    static let myTurquoise = UIColor(red: 0.45, green: 0.74, blue: 0.75, alpha: 1.0)
    
    static let myTurquoiseWithOpacity = UIColor(red: 0.45, green: 0.74, blue: 0.75, alpha: 0.6)
    
    static let myPurple:UIColor =  UIColor(red: 0.4, green: 0.2, blue: 0.4, alpha: 1.0)//673366
    
    static let myDarkPurple:UIColor =  UIColor(red:0.27, green:0.11, blue:0.27, alpha:1.0)//441c44
    
    static let myLightPurple:UIColor =  UIColor(red:0.92, green:0.90, blue:0.93, alpha:1.0)
//    let _ = UIColor(
    static let myFucsia:UIColor =  UIColor(red:198/255, green:36/255, blue:103/250, alpha:1.0)
    
    static let myWhiteGray:UIColor =  UIColor(red:250/255, green:250/255, blue:252/250, alpha:1.0)
    
    static let myGray:UIColor = UIColor(red: 64/255, green: 64/255, blue: 65/255, alpha: 1.0)
    
    static let myBlue:UIColor = UIColor(red: 48/255, green: 78/255, blue: 168/255, alpha: 1.0)
    
    static let myRed:UIColor = UIColor(red: 235/255, green: 42/255, blue: 32/255, alpha: 1.0)
    
    
    static let myPurple2 = UIColor(red: 0.439, green: 0.247, blue: 0.443, alpha: 1.0)//703f71


    
    ///new-->
    static let lightBlue1 = UIColor(red: 0.38, green: 0.187, blue: 0.223, alpha: 1.0)
    
    static let orange = UIColor(red: 0.243, green: 0.128, blue: 0.48, alpha: 1.0)
    
    static let orange2 = #colorLiteral(red: 0.9759463668, green: 0.5845184922, blue: 0.1595045924, alpha: 1)
    
    static let blue2 = #colorLiteral(red: 0.1479548812, green: 0.7789113522, blue: 0.8998932242, alpha: 1)
    
    static let bluelight = #colorLiteral(red: 0.5569341183, green: 0.8548176885, blue: 0.9220035076, alpha: 1)
    
    static let bluelightlight = #colorLiteral(red: 0.8514958024, green: 0.9521906972, blue: 0.9846723676, alpha: 1)
    
    static let lightBlue = UIColor(red: 0.126, green: 0.210, blue: 0.230, alpha: 1.0)

    static let lightBluetransfer35 = UIColor(red: 0.222, green: 0.242, blue: 0.250, alpha: 1.0)

    static let lightOrange = UIColor(red: 0.254, green: 0.240, blue: 0.226, alpha: 1.0)

    static let blueGreen = UIColor(red: 0.25, green: 0.117, blue: 0.135, alpha: 1.0)

    static let brown = UIColor(red: 0.155, green: 0.77, blue: 0.40, alpha: 1.0)

    
    
//    convenience init(rgb: CGFloat) {
//        self.init(
//            red: (rgb >> 16) & 0xFF,
//            green: (rgb >> 8) & 0xFF,
//            blue: rgb & 0xFF
//        )
//    }
    
//    var colorGray:UIColor = UIColor(red: 64/255, green: 64/255, blue: 65/255, alpha: 1.0)
//    var colorBlue:UIColor = UIColor(red: 48/255, green: 78/255, blue: 168/255, alpha: 1.0)
//    var colorRed:UIColor = UIColor(red: 235/255, green: 42/255, blue: 32/255, alpha: 1.0)
}
