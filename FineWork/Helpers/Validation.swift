//
//  Validation.swift
//  v-check
//
//  Created by User on 24.11.2015.
//  Copyright © 2015 User. All rights reserved.
//

import UIKit
//ולידציות על כל הפרטים:טלפון, מייל וכו׳
class Validation: NSObject {
    static let sharedInstance : Validation = {
        let instance = Validation()
        return instance
        }()
    
    func nameValidation(string:String)->Bool
    {
        if string.characters.count <= 30 && string.characters.count >= 2
        {
            let decimalCharacters = NSCharacterSet.decimalDigits
            
            let decimalRange = string.rangeOfCharacter(from: decimalCharacters)
            //rangeOfCharacterFromSet(decimalCharacters, options: NSString.CompareOptions.allZeros, range: nil)
            
            if decimalRange != nil//Numbers found
            {
                return false
            }
            return true
        }
        return false
    }
    
    func validationOfEmail(string:String) -> Bool
    {
        
        if string.contains("@") == true && string.contains(".") == true
        {
            let rangeOfDot: Range<String.Index> = string.range(of: ".", options: String.CompareOptions.backwards, range: nil, locale: nil)!

            let indexOfDot: Int = string.distance(from: string.startIndex, to: rangeOfDot.lowerBound)
            
            let range: Range<String.Index> = string.range(of: "@")!
                let index: Int = string.distance(from: string.startIndex, to: range.lowerBound)
            
            if indexOfDot > index
            {
                return true
            }
            return false
        }
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z0-9]{1,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr:String)->Bool{
        let passwordRegEx = "^[A-z0-9]+$"
        
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: testStr)
    }
    
    func isValidPasswordFinal(testStr:String) -> Bool {
        return testStr != "" && (testStr.characters.count) >= 6 && (testStr.characters.count) <= 10 && isValidPassword(testStr: testStr)
    }

    //foe cellPone
    func cellPhoneValidation(string:String)->Bool
    {
        return string.characters.count == 10 && (string as NSString).substring(to: 2) == "05"
    }
    //for telephone
    func phoneValidation(string:String)->Bool
    {
        if (string.characters.count == 9 || string.characters.count == 10) && (string as NSString).substring(to: 1) == "0"{
            return true
        }else{
            return false
        }
    }
    
    func isTzValid(string:String)->Bool
    {
        if string.characters.count != 9
        {
            return false
        }
        var sum:Int = 0
        var devidedNumber:Int = 0
        var id:Int = (Int)(string)!
        let checkDigit:Int = id%10
        id = id/10
        for i:Int in 0  ..< 8
        {
            devidedNumber = id%10
            if i%2 == 0
            {
                let sumOfDevided = devidedNumber*2
                sum += sumOfDevided%10+sumOfDevided/10
            }
            else
            {
                sum += devidedNumber
            }
            id = id/10
        }
        if (sum+checkDigit)%10 == 0
        {
            return true
        }
        return false
    }
    
    func isPassportValid(string:String)->Bool
    {
        let regexNumbersAndletters = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: [])
        return (regexNumbersAndletters.firstMatch(in: string, options: [], range: NSMakeRange(0, string.characters.count)) == nil)/* && capitalresult && numberresult */&& string.characters.count >= 2 && string.characters.count <= 20
    }

    
    /*func passwordValidation(string:String)->Bool
    {
        let regexNumbersAndletters = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: [])
        
        let LetterRegEx  = ".*[A-Za-z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", LetterRegEx)
        let capitalresult = texttest.evaluate(with: string)
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: string)
        return (regexNumbersAndletters.firstMatch(in: string, options: [], range: NSMakeRange(0, string.characters.count)) == nil) && capitalresult && numberresult && string.characters.count >= 8 && string.characters.count <= 20
    }
    
    func creditCardValidation(cardNum:String)->Bool
    {
        return cardNum.characters.count == 19
    }
    func cardCvvValidation(cardNum:String)->Bool
    {
        return cardNum.characters.count == 3
    }*/
    func isContainsEmojy(string:String) -> Bool {
        
        //var containsEmoji: Bool
        
        for scalar in string.unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F:   // Variation Selectors
                return false
            default:
                continue
            }
        }
        return true
    }
    
    func isStringContainsOnlyNumbers(string: String) -> Bool {
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return (Set(string.characters).isSubset(of: nums))
    }
}

