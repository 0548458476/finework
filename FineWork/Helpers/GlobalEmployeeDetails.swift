//
//  GlobalEmployeeDetails.swift
//  FineWork
//
//  Created by Racheli Kroiz on 7.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class GlobalEmployeeDetails: NSObject {
    
    static let sharedInstance : GlobalEmployeeDetails = {
        let instance = GlobalEmployeeDetails()
        return instance
    }()
    
    //MARK: - Variables
    
    var num:Float = 0.0
    var howManyToScroll:CGFloat = 0.0//height of opened reasons cells
    var howManyToScroll2:CGFloat = 0.0//height of opened other cells
    var employeeDetailsViewController:EmployeeDetailsViewController?
    var moreSalaryTableViewCell:MoreSalaryTableViewCell?
    var worker101Form:Worker101Form = Worker101Form()//keep the 101 details fillled by the user
    var dicReasonsCell:Dictionary<Int,UITableViewCell> = [:]//contains the resonCells which open other cell
    var numSectionsInTbl = 4
    var selectRowsForSection:Array<Bool> = [false,false,false,false,false,false]//save the status to all sections(open or close)
    var numRowsForSection:Array<Int> = [2,1,1,1,1,1]//save to each section number of rows to open(maximum sections in tbl=6, 4 always shown and 2 depend if "moreEarn" selected)
    //first item of array not used : 0=false,1=true,2=child
    var arrReasonsBool:Array<Int> = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]//save for each reason if its parent and select(1) or child(2)
    var arrReasonsKodsSelected:Array<Int> = []//save the reasons kods selected for server
    var familyStatus = ""//for family status in PersonaldetailsTableViewCell,contain the string of family status selected
    var incomePartnerSelected = false//for PartnerDeatilsTableViewCell,true = income button selected
    var isButtonsChanged:Array<Bool> = []//to know which setData to invoke in ChildrenDetails
    var isExemptionTaxCredit_Selected = false//if ״אני מבקש פטור״ selected
    var otherIncome:String = ""//if has other income
    
    //strings for in cells
    var arrReasonsForExemption_TaxCredit:Array<String> = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
    var arrReasonsForScroll:Dictionary<String,CGFloat> = [:]//
    var arrReasonsPossision:Dictionary<Int,CGFloat> = [:]
    var arrReasonsForExemption_Kods:Array<Int> = []//מערך קודים לסלים של הסיבות בהתאם לשרת
    var arrHeightForRowBase:Array<Float> = []//גבהים לכל סל של סיבה
    var dicHeightForRowOpened:Dictionary<Int,Float> = [:]
    var heightEmployeeDetailsFrame:Float = 0.0
    
    var dicHeightOfCellsOpened:[String:Float]  = [:]
    var reloadFromSelectReason:Bool = false//מציין האם לחצו על בחירת או ביטול סל של סיבה,טרו=בחרו(לא משנה אם לכבות או להדליק)
    //var isFromLogin:Bool?// מציין האם הגיעו מלוגין כדי לדעת האם להציג את ה״נמצא בחזקתי״ שבילדים לפי מה שנבחר(שמור באוביקט) או לפי המצב המשפחתי
    var isFromChangeFamiliStatus:Bool?
    
    var toCheckReason101SingleParent = false
    
    
    //MARK: - Functions
    func set_dicHeightOfCellsOpened(){
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (120/750/*101/750(47/750) * 2*/)
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (138/667)/*(146/750/*(90/750) * 2*/)*/
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((75/750) * 2)
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * /*((47/750) * 2)*/(54/667)
        
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((120/750) * 2)
        
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101SingleParentTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * ((110/750) * 2)
        
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101ChildrenNotHoldTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (60/667)/*((47/750) * 2)*/
        
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (60/667)/*((47/750) * 2)*/
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101SoldierTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (155/750/*(75/750) * 2*/)
        GlobalEmployeeDetails.sharedInstance.dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"] = GlobalEmployeeDetails.sharedInstance.heightEmployeeDetailsFrame * (60/667)/*((47/750) * 2)*/
    }

    func CheckForMarried()
    {
        toCheckReason101SingleParent = false
        //set the in reasons cell
        let reason101HandicappedTableViewCell: Reason101HandicappedTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101HandicappedTableViewCell") as? Reason101HandicappedTableViewCell
        reason101HandicappedTableViewCell.setDisplayData()
        
        let reason101ResidentPlaceTableViewCell: Reason101ResidentPlaceTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101ResidentPlaceTableViewCell") as? Reason101ResidentPlaceTableViewCell
        reason101ResidentPlaceTableViewCell.setDisplayData()
        
        let reason101NewImmegrantTableViewCell: Reason101NewImmegrantTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101NewImmegrantTableViewCell") as? Reason101NewImmegrantTableViewCell
        reason101NewImmegrantTableViewCell.setDisplayData()
        
        let reason101AddFileDisabledBlindTableViewCell: Reason101AddFileDisabledBlindTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101AddFileDisabledBlindTableViewCell") as? Reason101AddFileDisabledBlindTableViewCell
        reason101AddFileDisabledBlindTableViewCell.setDisplayData()
        
        let reason101ManSingleParentTableViewCell: reason101ManSingleParentTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "reason101ManSingleParentTableViewCell") as? reason101ManSingleParentTableViewCell
        reason101ManSingleParentTableViewCell.setDisplayData()
        
        let reason101SingleParentTableViewCell: Reason101SingleParentTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101SingleParentTableViewCell") as? Reason101SingleParentTableViewCell
        reason101SingleParentTableViewCell.setDisplayData()
        
        let reason101FoodToPartnerTableViewCell: Reason101FoodToPartnerTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101FoodToPartnerTableViewCell") as? Reason101FoodToPartnerTableViewCell
        reason101FoodToPartnerTableViewCell.setDisplayData()
        
        let reason101SoldierTableViewCell: Reason101SoldierTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101SoldierTableViewCell") as? Reason101SoldierTableViewCell
        reason101SoldierTableViewCell.setDisplayData()
        
        let reason101EndLearnTableViewCell: Reason101EndLearnTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101EndLearnTableViewCell") as? Reason101EndLearnTableViewCell
        reason101EndLearnTableViewCell.setDisplayData()
        
        let defaultHeight:Float = heightEmployeeDetailsFrame * ((47/750) * 2)
        var numYears = 0
        if GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate != nil
        {
            numYears = Global.sharedInstance.YearsBetween2Dates(date1: GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate as! Date, date2: NSDate() as Date)
        }
        
        //if partner details not null
        //if GlobalEmployeeDetails.sharedInstance.worker101Form.workerSpouse.isRequiredFieldsNull() == false
        //{
        //------------------------------------------------------
        //check if male and has child with him and get Allowance
        if GlobalEmployeeDetails.sharedInstance.worker101Form.iGenderType == 5//זכר
        {
            //גבר שיש לו ילדים בחזקתו שמקבלים קצבה
            var bHoldAllowance = false
            for child in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
            {
                if child.bWithMe == true && child.bGetChildAllowance == true
                {
                    bHoldAllowance = true
                }
            }
            if numYears >= 16 && numYears < 18
            {
                if bHoldAllowance == true////אני בין 16 ל-18 ולפחות ילד אחד בחזקתי ומקבל בגינו קצבת ילדים
                {
                    //show all reasons for married cells
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("TURNED_16_NOT_18", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    arrReasonsForExemption_Kods = [0,2,3,4,5,8,9,11,12,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""):0,NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""):0,NSLocalizedString("TURNED_16_NOT_18", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        4:reason101AddFileDisabledBlindTableViewCell,
                        5:reason101SingleParentTableViewCell,
                        6:reason101FoodToPartnerTableViewCell,
                        9:reason101SoldierTableViewCell,
                        10:reason101EndLearnTableViewCell]
                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,5:dicHeightOfCellsOpened["Reason101SingleParentTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,9:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!, 10:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    ]

                    
//                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
//                        1:reason101HandicappedTableViewCell,
//                        2:reason101ResidentPlaceTableViewCell,
//                        3:reason101NewImmegrantTableViewCell,
//                        4:reason101AddFileDisabledBlindTableViewCell,
//                        5:reason101SingleParentTableViewCell,
//                        6:reason101FoodToPartnerTableViewCell,
//                        8:reason101SoldierTableViewCell,
//                        9:reason101EndLearnTableViewCell]
//                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,5:dicHeightOfCellsOpened["Reason101SingleParentTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!, 9:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
//                    ]
                }
                else//אני בין 16 ל-18 ואין ילדים בחזקתי
                {
                    //show all reasons for married cells
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("TURNED_16_NOT_18", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    arrReasonsForExemption_Kods = [0,2,3,4,5,9,11,12,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""):0,NSLocalizedString("TURNED_16_NOT_18", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    
                    
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        4:reason101AddFileDisabledBlindTableViewCell,
                        6:reason101FoodToPartnerTableViewCell,
                        8:reason101SoldierTableViewCell,
                        9:reason101EndLearnTableViewCell]
                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!, 9:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    ]
                }
            }
            else//לא בין 16 ל-18
            {
                if bHoldAllowance == true//לא בין 16 ל-18 ולפחות ילד אחד בחזקתו ומקבל בגינו קצבת ילדים
                {
                    //show all reasons for married cells
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    
                    arrReasonsForExemption_Kods = [0,2,3,4,5,8,9,11,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""):0,NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    
                    //נסיון לתקן באג
                    //                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    //                        1:reason101HandicappedTableViewCell,
                    //                        2:reason101ResidentPlaceTableViewCell,
                    //                        3:reason101NewImmegrantTableViewCell,
                    //                        4:reason101AddFileDisabledBlindTableViewCell,
                    //                        5:reason101SingleParentTableViewCell,
                    //                        6:reason101FoodToPartnerTableViewCell,
                    //                        7:reason101SoldierTableViewCell,
                    //                        8:reason101EndLearnTableViewCell]
                    //                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,5:dicHeightOfCellsOpened["Reason101SingleParentTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!, 8:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    //                    ]
                    
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        4:reason101AddFileDisabledBlindTableViewCell,
                        5:reason101SingleParentTableViewCell,
                        7:reason101FoodToPartnerTableViewCell,
                        8:reason101SoldierTableViewCell,
                        9:reason101EndLearnTableViewCell]
                    
                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,5:dicHeightOfCellsOpened["Reason101SingleParentTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!, 9:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    ]
                }
                else//לא בין 16 ל-18 ואין ילדים בחזקתו
                {
                    //show all reasons for married cells
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    
                    arrReasonsForExemption_Kods = [0,2,3,4,5,9,11,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    //נסיון לתקן באג
                    //                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    //                        1:reason101HandicappedTableViewCell,
                    //                        2:reason101ResidentPlaceTableViewCell,
                    //                        3:reason101NewImmegrantTableViewCell,
                    //                        4:reason101AddFileDisabledBlindTableViewCell,
                    //                        5:reason101FoodToPartnerTableViewCell,
                    //                        6:reason101SoldierTableViewCell,
                    //                        7:reason101EndLearnTableViewCell]
                    //                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,5:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!, 7:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    //                    ]
                    //                    Reason101ChildrenNotHoldTableViewCell
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        4:reason101AddFileDisabledBlindTableViewCell,
                        6:reason101FoodToPartnerTableViewCell,
                        7:reason101SoldierTableViewCell,
                        8:reason101EndLearnTableViewCell]
                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!, 8:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    ]
                    
                }
            }
        }
            
            //---------------------------------------------------------------
        else//נקבה
        {
            if numYears >= 16 && numYears < 18
            {
                //show all reasons without :"בגין ילדי הפעוטים ימולא עי גבר (שלא סימן בפסקה הקודמת), אישה החיה בנפרד וילדיה אינם בחזקתה, וכן הורה יחיד"
                arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("TURNED_16_NOT_18", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                arrReasonsForExemption_Kods = [0,2,3,4,5,9,11,12,13,14]
                arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""):0,NSLocalizedString("TURNED_16_NOT_18", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                
                numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                
                arrHeightForRowBase = [0.0,
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight]
                
                GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    1:reason101HandicappedTableViewCell,
                    2:reason101ResidentPlaceTableViewCell,
                    3:reason101NewImmegrantTableViewCell,
                    4:reason101AddFileDisabledBlindTableViewCell,
                    6:reason101FoodToPartnerTableViewCell,
                    8:reason101SoldierTableViewCell,
                    9:reason101EndLearnTableViewCell]
                
                
                dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,9:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                ]
            }
            else
            {//אישה מעל גיל 18
                arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                
                arrReasonsForExemption_Kods = [0,2,3,4,5,9,11,13,14]
                arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                //                0,2,3,4,5,9,11,13,14
                arrHeightForRowBase = [0.0,
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight]
                
                GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    1:reason101HandicappedTableViewCell,
                    2:reason101ResidentPlaceTableViewCell,
                    3:reason101NewImmegrantTableViewCell,
                    4:reason101AddFileDisabledBlindTableViewCell,
                    6:reason101FoodToPartnerTableViewCell,
                    7:reason101SoldierTableViewCell,
                    8:reason101EndLearnTableViewCell]
                
                
                dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,4:dicHeightOfCellsOpened["Reason101AddFileDisabledBlindTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101FoodToPartnerTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                ]
            }
        }
        numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
        num = 0
        for height in arrHeightForRowBase {
            num += height
        }
        howManyToScroll = CGFloat(num)
        
        var numOpenCells = 0//מספר הסלים הפתוחים
        
        if arrReasonsKodsSelected.count > 0
        {
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[2] = true
        }
        GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.sort()//שורה זו קריטית וחשובה!!!
        //save reasons state for open and close
        for var i in 0..<arrReasonsKodsSelected.count
        {
            for var j in 0..<arrReasonsForExemption_Kods.count
            {
                if arrReasonsForExemption_Kods[j] == arrReasonsKodsSelected[i]
                {
                    //סיבה 7 תהא אב שפותח רק אם סיבה 6 מסומנת
                    if arrReasonsKodsSelected[i] == 6{
                        if toCheckReason101SingleParent == true{
                            dicHeightForRowOpened[5] = dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"]!
                            dicReasonsCell[5] = reason101ManSingleParentTableViewCell
                        }
                    }
                    setDataForReasons(tagParent: j, tag: j + numOpenCells)
                    if GlobalEmployeeDetails.sharedInstance.dicReasonsCell[j] != nil//אם נפתח ילדים
                    {
                        numOpenCells += 1
                    }
                    break
                }
            }
        }
    }
    
    func CheckForNotMarried()
    {
        toCheckReason101SingleParent = false
        //set in reasons cell
        let reason101HandicappedTableViewCell: Reason101HandicappedTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101HandicappedTableViewCell") as? Reason101HandicappedTableViewCell
        reason101HandicappedTableViewCell.setDisplayData()
        
        let reason101ResidentPlaceTableViewCell: Reason101ResidentPlaceTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101ResidentPlaceTableViewCell") as? Reason101ResidentPlaceTableViewCell
        reason101ResidentPlaceTableViewCell.setDisplayData()
        
        let reason101NewImmegrantTableViewCell: Reason101NewImmegrantTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101NewImmegrantTableViewCell") as? Reason101NewImmegrantTableViewCell
        reason101NewImmegrantTableViewCell.setDisplayData()
        
        let reason101ManSingleParentTableViewCell: reason101ManSingleParentTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "reason101ManSingleParentTableViewCell") as? reason101ManSingleParentTableViewCell
        reason101ManSingleParentTableViewCell.setDisplayData()
        
        let reason101ChildrenNotHoldTableViewCell: Reason101ChildrenNotHoldTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101ChildrenNotHoldTableViewCell") as? Reason101ChildrenNotHoldTableViewCell
        reason101ChildrenNotHoldTableViewCell.setDisplayData()
        
        let reason101SoldierTableViewCell: Reason101SoldierTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101SoldierTableViewCell") as? Reason101SoldierTableViewCell
        reason101SoldierTableViewCell.setDisplayData()
        
        let reason101EndLearnTableViewCell: Reason101EndLearnTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101EndLearnTableViewCell") as? Reason101EndLearnTableViewCell
        reason101EndLearnTableViewCell.setDisplayData()
        
        let reason101SingleParentTableViewCell: Reason101SingleParentTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "Reason101SingleParentTableViewCell") as? Reason101SingleParentTableViewCell
        reason101SingleParentTableViewCell.setDisplayData()
        
        let defaultHeight:Float = heightEmployeeDetailsFrame * ((47/750) * 2)
        var numYears = 0
        if GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate != nil
        {
            numYears = Global.sharedInstance.YearsBetween2Dates(date1: GlobalEmployeeDetails.sharedInstance.worker101Form.dBirthDate as! Date, date2: NSDate() as Date)
        }
        
        var bWithMe = false
        var bGetChildAllowance = false
        for child in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
        {
            if child.bWithMe == true
            {
                bWithMe = true
            }
            if child.bGetChildAllowance == true
            {
                bGetChildAllowance = true
            }
        }
        
        if bWithMe == false && GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds.count > 0//יש לו ילדים והילדים לא בחזקתו
        {
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iGenderType == 6//אישה
            {
                if numYears >= 16 && numYears < 18
                {
                    //return reasons for not married cells except ,"בגין ילדי שבחזקתי המפורטים בחלק ג ימולא רק עי גבר חד הורי שמקבל את קצבת הילדים בגינם החי בנפרד או עי אישה או עי הורה יחיד"
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("TURNED_16_NOT_18", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    arrReasonsForExemption_Kods = [0,2,3,4,6,8,9,10,12,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""):0,NSLocalizedString("TURNED_16_NOT_18", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    
                    
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        /*5:reason101SingleParentTableViewCell,*/
                        7:reason101ChildrenNotHoldTableViewCell,
                        9:reason101SoldierTableViewCell,
                        10:reason101EndLearnTableViewCell]
                    
                    
                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,/*5:dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"]!,*/7:dicHeightOfCellsOpened["Reason101ChildrenNotHoldTableViewCell"]!,9:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,10:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!]
                    toCheckReason101SingleParent = true
                }
                else
                {
                    //return reasons for not married cells except ,"בגין ילדי שבחזקתי המפורטים בחלק ג ימולא רק עי גבר חד הורי שמקבל את קצבת הילדים בגינם החי בנפרד או עי אישה או עי הורה יחיד"
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    arrReasonsForExemption_Kods = [0,2,3,4,6,8,9,10,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    //try...
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        5:reason101SingleParentTableViewCell,
                        7:reason101ChildrenNotHoldTableViewCell,
                        8:reason101SoldierTableViewCell,
                        9:reason101EndLearnTableViewCell]
                    
                    dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,5:dicHeightOfCellsOpened["Reason101SingleParentTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101ChildrenNotHoldTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,9:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!]
//                    toCheckReason101SingleParent = true
                }
                num = 0
                for height in arrHeightForRowBase {
                    num += height
                }
                howManyToScroll = CGFloat(num)
            }
            else
            {
                if numYears >= 16 && numYears < 18
                {
                    //return reasons for not married cells except "בגין ילדי הפעוטים ימולא ע״י גבר (שלא סימן בפסקה הקודמת), אישה החיה בנפרד  וילדיה אינם בחזקתה,וכן הורה יחיד"
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("TURNED_16_NOT_18", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    arrReasonsForExemption_Kods = [0,2,3,4,6,9,10,12,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""):0,NSLocalizedString("TURNED_16_NOT_18", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        6:reason101ChildrenNotHoldTableViewCell,
                        8:reason101SoldierTableViewCell,
                        9:reason101EndLearnTableViewCell]
                    
                    dicHeightForRowOpened = [
                        1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101ChildrenNotHoldTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,9:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    ]
                }
                else
                {
                    //return reasons for not married cells except "בגין ילדי הפעוטים ימולא ע״י גבר (שלא סימן בפסקה הקודמת), אישה החיה בנפרד  וילדיה אינם בחזקתה,וכן הורה יחיד"
                    arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                    arrReasonsForExemption_Kods = [0,2,3,4,6,9,10,13,14]
                    
                    arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                    
                    numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                    
                    arrHeightForRowBase = [0.0,
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           heightEmployeeDetailsFrame * ((65/750) * 2),
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight,
                                           defaultHeight]
                    
                    GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                        1:reason101HandicappedTableViewCell,
                        2:reason101ResidentPlaceTableViewCell,
                        3:reason101NewImmegrantTableViewCell,
                        6:reason101ChildrenNotHoldTableViewCell,
                        7:reason101SoldierTableViewCell,
                        8:reason101EndLearnTableViewCell]
                    
                    dicHeightForRowOpened = [
                        1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!,2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101ChildrenNotHoldTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!
                    ]
                }
                num = 0
                for height in arrHeightForRowBase {
                    num += height
                }
                howManyToScroll = CGFloat(num)
            }
        }
        else if bWithMe == true && bGetChildAllowance == true//יש לפחות ילד בחזקתו ומקבלת קצבת ילדים
        {
            if numYears >= 16 && numYears < 18
            {
                //return 1 cell
                arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE",comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("TURNED_16_NOT_18", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                arrReasonsForExemption_Kods = [0,2,3,4,6,7,9,12,13,14]
                
                arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE",comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("TURNED_16_NOT_18", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                
                arrHeightForRowBase = [0.0,
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       heightEmployeeDetailsFrame * ((90/750) * 2),
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight]
                
                GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    1:reason101HandicappedTableViewCell,
                    2:reason101ResidentPlaceTableViewCell,
                    3:reason101NewImmegrantTableViewCell,
                    /*5:reason101ManSingleParentTableViewCell,*/
                    8:reason101SoldierTableViewCell,
                    9:reason101EndLearnTableViewCell]
                
                dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,/*5:dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"]!,*/8:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,9:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!]
                toCheckReason101SingleParent = true
            }
            else
            {
                //return 1 cell
                arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE",comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                arrReasonsForExemption_Kods = [0,2,3,4,6,7,9,13,14]
                
                arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE",comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                
                arrHeightForRowBase = [0.0,
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       heightEmployeeDetailsFrame * ((90/750) * 2),
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight]
                
                GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    1:reason101HandicappedTableViewCell,
                    2:reason101ResidentPlaceTableViewCell,
                    3:reason101NewImmegrantTableViewCell,
                    /*5:reason101ManSingleParentTableViewCell,*/
                    7:reason101SoldierTableViewCell,
                    8:reason101EndLearnTableViewCell]
                
                dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,/*5:dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"]!,*/7:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!]
                toCheckReason101SingleParent = true
            }
            num = 0
            for height in arrHeightForRowBase {
                num += height
            }
            howManyToScroll = CGFloat(num)
        }
        else//אין לו ילדים, או שיש לו ילדים אבל הם בחזקתו ולא מקבל בגינם קצבת ילדים
        {
            if numYears >= 16 && numYears < 18
            {
                //return 1 cell
                arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("TURNED_16_NOT_18", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                arrReasonsForExemption_Kods = [0,2,3,4,6,9,12,13,14]
                
                arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("TURNED_16_NOT_18", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                
                arrHeightForRowBase = [0.0,
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight]
                
                GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    1:reason101HandicappedTableViewCell,
                    2:reason101ResidentPlaceTableViewCell,
                    3:reason101NewImmegrantTableViewCell,
                    7:reason101SoldierTableViewCell,
                    8:reason101EndLearnTableViewCell]
                
                dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,8:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!]
            }
            else
            {
                //return 1 cell
                arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
                arrReasonsForExemption_Kods = [0,2,3,4,6,9,13,14]
                
                arrReasonsForScroll = ["":0,NSLocalizedString("DISABLE_BLIND", comment: ""):0,NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""):0,NSLocalizedString("NEW_IMMIGRANT", comment: ""):0,NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""):0,NSLocalizedString("SINGLE_PARENT", comment: ""):0,NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""):0,NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: ""):0]
                numRowsForSection[2] = arrReasonsForExemption_TaxCredit.count
                
                arrHeightForRowBase = [0.0,
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       heightEmployeeDetailsFrame * ((65/750) * 2),
                                       defaultHeight,
                                       defaultHeight,
                                       defaultHeight]
                
                GlobalEmployeeDetails.sharedInstance.dicReasonsCell = [
                    1:reason101HandicappedTableViewCell,
                    2:reason101ResidentPlaceTableViewCell,
                    3:reason101NewImmegrantTableViewCell,
                    6:reason101SoldierTableViewCell,
                    7:reason101EndLearnTableViewCell]
                
                dicHeightForRowOpened = [1:dicHeightOfCellsOpened["Reason101HandicappedTableViewCell"]!, 2: dicHeightOfCellsOpened["Reason101ResidentPlaceTableViewCell"]!,3:dicHeightOfCellsOpened["Reason101NewImmegrantTableViewCell"]!,6:dicHeightOfCellsOpened["Reason101SoldierTableViewCell"]!,7:dicHeightOfCellsOpened["Reason101EndLearnTableViewCell"]!]
            }
            num = 0
            for height in arrHeightForRowBase {
                num += height
            }
            howManyToScroll = CGFloat(num)
        }
        
        //-----------------------------------------------
        
        GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.sort()//שורה זו קריטית וחשובה!!!
        //save reasons state for open and close
        var numOpenCells = 0
        for i in 0..<arrReasonsKodsSelected.count
        {
            for j in 0..<arrReasonsForExemption_Kods.count
            {
                if arrReasonsForExemption_Kods[j] == arrReasonsKodsSelected[i]
                {
                    //סיבה 7 תהא אב שפותח רק אם סיבה 6 מסומנת
                    if arrReasonsKodsSelected[i] == 6{
                        if toCheckReason101SingleParent == true{
                            dicHeightForRowOpened[5] = dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"]!
                            dicReasonsCell[5] = reason101ManSingleParentTableViewCell
                        }
                    }
                    setDataForReasons(tagParent: j, tag: j + numOpenCells)
                    if GlobalEmployeeDetails.sharedInstance.dicReasonsCell[j] != nil
                    {
                        numOpenCells += 1
                    }
                    break
                }
            }
        }
    }
    
    //בודק אם מצב כפתור הסיבה מסומן או לא
    //tagParent = אינדקס של האבא ביחס לאבות בלבד, tag = אינדקס הבן ביחס לכל השורו
    func setDataForReasons(tagParent:Int,tag:Int)
    {
        if GlobalEmployeeDetails.sharedInstance.dicReasonsCell[tagParent] != nil//if this cell open other cell
        {
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[2] += 1
            GlobalEmployeeDetails.sharedInstance.arrReasonsBool.insert(2, at: tag + 1)
            GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit.insert("", at: tag + 1)
            GlobalEmployeeDetails.sharedInstance.howManyToScroll += CGFloat(GlobalEmployeeDetails.sharedInstance.dicHeightForRowOpened[tagParent]!)
        }
        
        GlobalEmployeeDetails.sharedInstance.arrReasonsBool[tag] = 1
        
        GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.append(GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_Kods[tagParent])
        GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected = Global.sharedInstance.uniq(source: GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected)
    }
    
    //reload reasons cells when change status
    //calls when change: Gender,family status,hold child selected,added new child and remove child
    func ReloadReasons()
    {
        // - 14-05-2017
//        if GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected == true
//        {  // - 14-05-2017
            //            //set properties to default value
            //            var arrReasonsBool_Help = GlobalEmployeeDetails.sharedInstance.arrReasonsBool
            //            var arrReasonsKodsSelected_Help = GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected
            //            var dicHeightForRowOpened_Help = GlobalEmployeeDetails.sharedInstance.dicHeightForRowOpened
            //            var dicReasonsCell_Help = GlobalEmployeeDetails.sharedInstance.dicReasonsCell
            GlobalEmployeeDetails.sharedInstance.arrReasonsBool = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            //            GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected = []
            
            if GlobalEmployeeDetails.sharedInstance.worker101Form.iFamilyStatusType == 2//נשוי
            {
                GlobalEmployeeDetails.sharedInstance.CheckForMarried()
            }
            else//לא נשוי
            {
                GlobalEmployeeDetails.sharedInstance.CheckForNotMarried()
            }
            
            var arrReasonsKodsSelected_Help = GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected
            GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected = []

            //למחוק סיבות שלא מופיעות ממערך הסיבות שנבחרו
            for lastReason in arrReasonsKodsSelected_Help{
                for reason in arrReasonsForExemption_Kods{
                    if lastReason == reason{
                        GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.append(lastReason)
                    }
                }
            }
        
        // - 15-04-2017
        if !GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected {
            GlobalEmployeeDetails.sharedInstance.howManyToScroll = 0
            GlobalEmployeeDetails.sharedInstance.arrReasonsBool = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
            GlobalEmployeeDetails.sharedInstance.numRowsForSection[2] = 1
            
            //                        GlobalEmployeeDetails.sharedInstance.isExemptionTaxCredit_Selected = false
            GlobalEmployeeDetails.sharedInstance.selectRowsForSection[2] = false
            GlobalEmployeeDetails.sharedInstance.worker101Form.bAskTaxCredit = false
        }  // - 15-04-2017

        
            GlobalEmployeeDetails.sharedInstance.employeeDetailsViewController?.extTbl.reloadData()
//        }
    }
    
    //פונקציה זו מוסיפה איברים ריקים למערך של הסיבות לפטור ממס, כדי שיהיה מלא באיברים כמספר הסיבות (אחרי שצמצמו את המערך)
    func setArrReasonFull()
    {
        var arrAllReasons:Array<WorkerTaxRelifeReason> = []
        var didFind = false
        for var i in 0 ..< 16
        {
            didFind = false
            for item in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons
            {
                if item.iTaxRelifeReasonId == i
                {
                    arrAllReasons.append(item)
                    didFind = true
                    break
                }
            }
            if didFind == false
            {
                arrAllReasons.append(WorkerTaxRelifeReason())
            }
        }
        
        GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons = arrAllReasons
    }
    
    func setReason7father(isFather:Bool,tag:Int,tagParent:Int,section:Int){
        
        if toCheckReason101SingleParent == true{
            if isFather{
                let reason101ManSingleParentTableViewCell: reason101ManSingleParentTableViewCell! = employeeDetailsViewController?.extTbl.dequeueReusableCell(withIdentifier: "reason101ManSingleParentTableViewCell") as? reason101ManSingleParentTableViewCell
                reason101ManSingleParentTableViewCell.setDisplayData()
                dicHeightForRowOpened[5] = dicHeightOfCellsOpened["reason101ManSingleParentTableViewCell"]!
                dicReasonsCell[5] = reason101ManSingleParentTableViewCell
                
                if arrReasonsBool[tag+1] == 1{//סיבה 7 בחורה ולכן צריך לפתוח לו את הבן
                    numRowsForSection[section] += 1
                    arrReasonsBool.insert(2, at: tag + 2)
                    arrReasonsForExemption_TaxCredit.insert("", at: tag + 2)
                    howManyToScroll += CGFloat(dicHeightForRowOpened[tagParent+1]!)
                }
                
                //                if GlobalEmployeeDetails.sharedInstance.dicReasonsCell[tagParent] != nil//if this cell open other cell
                //                {
                //                    GlobalEmployeeDetails.sharedInstance.numRowsForSection[section] += 1
                //                    GlobalEmployeeDetails.sharedInstance.arrReasonsBool.insert(2, at: tag + 1)
                //                    GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_TaxCredit.insert("", at: tag + 1)
                //                    GlobalEmployeeDetails.sharedInstance.howManyToScroll += CGFloat(GlobalEmployeeDetails.sharedInstance.dicHeightForRowOpened[tagParent]!)
                //                }
                
                //                GlobalEmployeeDetails.sharedInstance.arrReasonsBool[self.tag] = 1
                //                GlobalEmployeeDetails.sharedInstance.arrReasonsKodsSelected.append(GlobalEmployeeDetails.sharedInstance.arrReasonsForExemption_Kods[self.tagParent])
            }else{
                //                arrReasonsBool[tag+1] = 1
                //                worker101Form.lWorkerTaxRelifeReasons[tagParent+1].iTaxRelifeReasonId = 0
                
                if GlobalEmployeeDetails.sharedInstance.arrReasonsBool[tag + 2] == 2//his child opened
                {
                    numRowsForSection[section] -= 1
                    arrReasonsBool.remove(at: tag + 2)
                    arrReasonsForExemption_TaxCredit.remove(at: tag + 2)
                    howManyToScroll -= CGFloat(dicHeightForRowOpened[tagParent+1]!)
                }
                
                //remove from array
                arrReasonsKodsSelected = arrReasonsKodsSelected.filter() {$0 != arrReasonsForExemption_Kods[tagParent+1]
                }
                dicHeightForRowOpened.removeValue(forKey: 5)
                dicReasonsCell.removeValue(forKey: 5)
            }
        }
    }
    
    func replaceUser(){
        num = 0.0
        howManyToScroll = 0.0//height of opened reasons cells
        howManyToScroll2 = 0.0//height of opened other cells
        //         employeeDetailsViewController = EmployeeDetailsViewController()
        //         moreSalaryTableViewCell = MoreSalaryTableViewCell()
        worker101Form = Worker101Form()//keep the 101 details fillled by the user
        dicReasonsCell = [:]//contains the resonCells which open other cell
        numSectionsInTbl = 4
        selectRowsForSection = [false,false,false,false,false,false]//save the status to all sections(open or close)
        numRowsForSection = [2,1,1,1,1,1]//save to each section number of rows to open(maximum sections in tbl=6, 4 always shown and 2 depend if "moreEarn" selected)
        //first item of array not used : 0=false,1=true,2=child
        arrReasonsBool = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]//save for each reason if its parent and select(1) or child(2)
        arrReasonsKodsSelected = []//save the reasons kods selected for server
        familyStatus = ""//for family status in PersonaldetailsTableViewCell,contain the string of family status
        incomePartnerSelected = false//for PartnerDeatilsTableViewCell,true = income button selected
        isButtonsChanged = []//to know which setData to invoke in ChildrenDetails
        isExemptionTaxCredit_Selected = false//if ״אני מבקש פטור״ selected
        otherIncome = ""//if has other income
        
        //strings for in cells
        arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
        arrReasonsForScroll = [:]//
        arrReasonsPossision = [:]
        arrReasonsForExemption_Kods = []//מערך קודים לסלים של הסיבות בהתאם לשרת
        arrHeightForRowBase = []//גבהים לכל סל של סיבה
//        dicHeightForRowOpened = [:]
        heightEmployeeDetailsFrame = 0.0
        
//        dicHeightOfCellsOpened  = [:]
        reloadFromSelectReason = false//מציין האם לחצו על בחירת או ביטול סל של סיבה,טרו=בחרו(לא משנה אם לכבות או להדליק)
        //var isFromLogin:Bool?// מציין האם הגיעו מלוגין כדי לדעת האם להציג את ה״נמצא בחזקתי״ שבילדים לפי מה שנבחר(שמור באוביקט) או לפי המצב המשפחתי
        isFromChangeFamiliStatus = false
        
        toCheckReason101SingleParent = false
        
    }
    
    func reload101Form(){
        num = 0.0
        howManyToScroll = 0.0//height of opened reasons cells
        howManyToScroll2 = 0.0//height of opened other cells
        //         employeeDetailsViewController = EmployeeDetailsViewController()
        //         moreSalaryTableViewCell = MoreSalaryTableViewCell()
        worker101Form = Worker101Form()//keep the 101 details fillled by the user
        dicReasonsCell = [:]//contains the resonCells which open other cell
        numSectionsInTbl = 4
        selectRowsForSection = [false,false,false,false,false,false]//save the status to all sections(open or close)
        numRowsForSection = [2,1,1,1,1,1]//save to each section number of rows to open(maximum sections in tbl=6, 4 always shown and 2 depend if "moreEarn" selected)
        //first item of array not used : 0=false,1=true,2=child
        arrReasonsBool = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]//save for each reason if its parent and select(1) or child(2)
        arrReasonsKodsSelected = []//save the reasons kods selected for server
        familyStatus = ""//for family status in PersonaldetailsTableViewCell,contain the string of family status
        incomePartnerSelected = false//for PartnerDeatilsTableViewCell,true = income button selected
        isButtonsChanged = []//to know which setData to invoke in ChildrenDetails
        isExemptionTaxCredit_Selected = false//if ״אני מבקש פטור״ selected
        otherIncome = ""//if has other income
        
        //strings for in cells
        arrReasonsForExemption_TaxCredit = ["",NSLocalizedString("DISABLE_BLIND", comment: ""),NSLocalizedString("SPECIAL_PERMANENT_RESIDER", comment: ""),NSLocalizedString("NEW_IMMIGRANT", comment: ""),NSLocalizedString("MY_PARTNER_NO_INCOME", comment: ""),NSLocalizedString("ASK_POINT_FOR_CHILDREN", comment: ""),NSLocalizedString("SINGLE_PARENT_RECEIVING_ALLOWANCE", comment: ""),NSLocalizedString("MAN_ALONE_WOMAN_SINGE_PARENT", comment: ""),NSLocalizedString("SINGLE_PARENT", comment: ""),NSLocalizedString("PARTICIPATE_OF_ECONOMY", comment: ""),NSLocalizedString("FORMER_SPOUSE_FOODS", comment: ""),NSLocalizedString("DISCHARGED_SOLDIER_NATIONAL_SERVICE", comment: ""),NSLocalizedString("TERMINATION_OF_DEGREE_PROGRAMS", comment: "")]
        arrReasonsForScroll = [:]//
        arrReasonsPossision = [:]
        arrReasonsForExemption_Kods = []//מערך קודים לסלים של הסיבות בהתאם לשרת
        arrHeightForRowBase = []//גבהים לכל סל של סיבה
        dicHeightForRowOpened = [:]
//        heightEmployeeDetailsFrame = 0.0
        
//        dicHeightOfCellsOpened  = [:]
        reloadFromSelectReason = false//מציין האם לחצו על בחירת או ביטול סל של סיבה,טרו=בחרו(לא משנה אם לכבות או להדליק)
//        isFromLogin = false// מציין האם הגיעו מלוגין כדי לדעת האם להציג את ה״נמצא בחזקתי״ שבילדים לפי מה שנבחר(שמור באוביקט) או לפי המצב המשפחתי
        isFromChangeFamiliStatus = false
        
        toCheckReason101SingleParent = false
        
//        var num:Float = 0.0
//        var howManyToScroll:CGFloat = 0.0//height of opened reasons cells
//        var howManyToScroll2:CGFloat = 0.0//height of opened other cells
//        var employeeDetailsViewController:EmployeeDetailsViewController?
//        var moreSalaryTableViewCell:MoreSalaryTableViewCell?
//        var worker101Form:Worker101Form = Worker101Form()//keep the 101 details fillled by the user
//        var dicReasonsCell:Dictionary<Int,UITableViewCell> = [:]//contains the resonCells which open other cell
//        var numSectionsInTbl = 4
//        var selectRowsForSection:Array<Bool> = [false,false,false,false,false,false]//save the status to all sections(open or close)
//        var numRowsForSection:Array<Int> = [2,1,1,1,1,1]//save to each section number of rows to open(maximum sections in tbl=6, 4 always shown and 2 depend if "moreEarn" selected)
//        //first item of array not used : 0=false,1=true,2=child
//        var arrReasonsBool:Array<Int> = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]//save for each reason if its parent and select(1) or child(2)
//        var arrReasonsKodsSelected:Array<Int> = []//save the reasons kods selected for server
//        var familyStatus = ""//for family status in PersonaldetailsTableViewCell,contain the string of family status selected
//        var incomePartnerSelected = false//for PartnerDeatilsTableViewCell,true = income button selected
    }
    
    func show101ViewConGeneric(){
        Generic.sharedInstance.showNativeActivityIndicator(cont: employeeDetailsViewController!)
    }
    
    func hide101ViewConGeneric() {
        Generic.sharedInstance.hideNativeActivityIndicator(cont: employeeDetailsViewController!)
    }
    
}
