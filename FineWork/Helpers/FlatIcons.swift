//
//  FlatIcons.swift
//  FineWork
//
//  Created by Lior Ronen on 12/12/2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class FlatIcons: NSObject {
    static let sharedInstance : FlatIcons = {
        let instance = FlatIcons()
        return instance
    }()
    //LoginVc
    var LOGIN_MAIL:String = "\u{F12D}" //
    var LOGIN_PASSWORD:String = "\u{F136}" //
 //RegisterVc
    var REGISTER_USER_NAME:String = "\u{F122}" //
    var REGISTER_MAIL:String = "\u{F12D}" //
    var REGISTER_MOBILE:String = "\u{f116}" //
    var REGISTER_PASSWORD:String = "\u{F136}" //
    var REGISTER_RECCOMEND_PHONE:String = "\u{F118}" //
//PERSONAL_INFORAMTION_VC
    var PERSONAL_PARENT_VC_DetaisCard : String = "\u{f128}"//facebook
    var PERSONAL_PARENT_VC_SalaryDetails : String = "\u{f11f}"//interface-6
    var PERSONAL_PARENT_VC_VisibleProfile : String = "\u{f122}"//-profile
    var PERSONAL_PARENT_VC_Contract : String = "\u{f109}"//people-2
    var PERSONAL_PARENT_VC_RegisterDetails : String = "\u{f136}"//interface-5
    
    var CALANDER : String = "ֿֿֿ\u{F134}"
    
    var emptyStar = "\u{f006}"
    var fullStar = "\u{f005}"
    
    var location = "\u{F12C}"
    var x = "\u{F101}"
    var arrow_down = "\u{F13c}"



}
