//
//  UserDefaultManagment.swift
//  ShiftTours
//
//  Created by User on 12/06/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class UserDefaultManagment: NSObject {

    static let sharedInstance : UserDefaultManagment = {
        let instance = UserDefaultManagment()
        return instance
    }()
    
    let userName = "userName"
    let password = "password"
    
    func setUserName(value : String) {
        UserDefaults.standard.setValue(value, forKey: userName)
    }

    func getUserName() -> String {
        if let name = UserDefaults.standard.value(forKey: userName) {
            return name as! String
        }
        return ""
    }
    
    func setPassword(value : String) {
        UserDefaults.standard.setValue(value, forKey: password)
    }
    
    func getPassword() -> String {
        if let password = UserDefaults.standard.value(forKey: password) {
            return password as! String
        }
        return ""
    }
    
  /*  func setIsSaveDetails(value : Bool) {
        UserDefaults.standard.setValue(value, forKey: isSaveDetailsKey)
    }
    
    func getIsSaveDetails() -> Bool {
        if let isSaveDetails = UserDefaults.standard.value(forKey: isSaveDetailsKey) as? Bool {
            return isSaveDetails
        }
        return true
    }
    */

}
