//
//  FontAwesome.swift
//  FineWork
//
//  Created by User on 27.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class FontAwesome: NSObject {

    static let sharedInstance : FontAwesome = {
        let instance = FontAwesome()
        return instance
    }()
    
    // icon font : awesome
    var arrow = "\u{f107}"
    var openEye = "\u{f06e}"
    var closedEye = "\u{f070}"
    var close_x = "\u{f00d}"
    var arrow_bottom = "\u{f107}"
    var arrow_up = "\u{f106}"


}
