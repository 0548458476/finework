//
//  PhonTextField.swift
//  FineWork
//
//  Created by Tamy wexelbom on 13.2.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class PhonTextField: UITextField {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }

    fileprivate func initialize() {
        self.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
    }
    

    func textFieldDidChange(textField: UITextField) {
        var numbersString = ""
        for tempChar in (textField.text?.characters)!{
            if "0"..."9" ~= tempChar {
                numbersString += String(tempChar)
            }
        }
        let startIndex = numbersString.startIndex
        if numbersString.characters.count < 10{
            if numbersString.characters.count >= 3{
                numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 2))
                if numbersString.characters.count >= 7{
                    numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 6))
                }
            }
        }else{
            if numbersString.characters.count >= 4{
                numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 3))
                if numbersString.characters.count >= 8{
                    numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 7))
                }
            }
        }
        textField.text = numbersString
    }
    
    func editText(){
        var numbersString = ""
        for tempChar in (self.text?.characters)!{
            if "0"..."9" ~= tempChar {
                numbersString += String(tempChar)
            }
        }
        let startIndex = numbersString.startIndex
        if numbersString.characters.count < 10{
            if numbersString.characters.count >= 3{
                numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 2))
                if numbersString.characters.count >= 7{
                    numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 6))
                }
            }
        }else{
            if numbersString.characters.count >= 4{
                numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 3))
                if numbersString.characters.count >= 8{
                    numbersString.insert("-", at: numbersString.index(startIndex, offsetBy: 7))
                }
            }
        }
        
        self.text = numbersString
    }
    
    func getOriginalText()->String?{
         return self.text?.replacingOccurrences(of: "-", with: "")
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
