//
//  CheckBox.swift
//  FineWork
//
//  Created by Racheli Kroiz on 5.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class CheckBox: UIButton {

    var isChecked:Bool = false{
        didSet{
            if isChecked == true
            {
                self.setTitle("\u{f046}", for: .normal)
            }
            else
            {
                 self.setTitle("\u{f096}", for: .normal)
            }
        }
    }
    override func awakeFromNib() {
        
        self.isChecked = false
    }

}
