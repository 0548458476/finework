//
//  CheckBoxCalander.swift
//  FineWork
//
//  Created by User on 12/09/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class CheckBoxCalander: UIButton {

    
        // Images
       // let checkedImage = UIImage(named: "ic_check_box")! as UIImage
      //  let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")! as UIImage
        
        // Bool property
        var isChecked: Bool = false {
            didSet{
                if isChecked == true {
                    self.backgroundColor = ControlsDesign.sharedInstance.colorOrang
                   
                } else {
                    self.backgroundColor = UIColor.white
                }
            }
        }
        
        override func awakeFromNib() {
            self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
            self.isChecked = false
            
            self.layer.borderColor = ControlsDesign.sharedInstance.colorOrang.cgColor
            self.layer.borderWidth = 1
            self.layer.cornerRadius = self.frame.height / 2
        }
        
        func buttonClicked(sender: UIButton) {
            if sender == self {
                isChecked = !isChecked
            }
        }
    
}
