//
//  extensions.swift
//  FineWork
//
//  Created by User on 22/08/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class Extensions: NSObject { }
    // extensions
    extension UIImage {
        // rotate image
        public func imageRotatedByDegrees(degrees: CGFloat, flip: Bool) -> UIImage {
            let radiansToDegrees: (CGFloat) -> CGFloat = {
                return $0 * (180.0 / CGFloat(M_PI))
            }
            let degreesToRadians: (CGFloat) -> CGFloat = {
                return $0 / 180.0 * CGFloat(M_PI)
            }
            
            // calculate the size of the rotated view's containing box for our drawing space
            let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
            let t = CGAffineTransform(rotationAngle: degreesToRadians(degrees));
            rotatedViewBox.transform = t
            let rotatedSize = rotatedViewBox.frame.size
            
            // Create the bitmap context
            UIGraphicsBeginImageContext(rotatedSize)
            let bitmap = UIGraphicsGetCurrentContext()
            
            // Move the origin to the middle of the image so we will rotate and scale around the center.
            bitmap!.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0);
            
            //   // Rotate the image context
            bitmap!.rotate(by: degreesToRadians(degrees))
            //rotateCGContextRotateCTM(bitmap!,by: degreesToRadians(degrees));
            
            // Now, draw the rotated/scaled image into the context
            var yFlip: CGFloat
            
            if(flip){
                yFlip = CGFloat(-1.0)
            } else {
                yFlip = CGFloat(1.0)
            }
            
            bitmap!.scaleBy(x: yFlip, y: -1.0)
            
            draw(in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return newImage!
        }
           }
    
    extension UIImageView {
        // download image by url
        func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
            contentMode = mode
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                DispatchQueue.main.async() { () -> Void in
                    self.image = image
                }
                }.resume()
        }
        func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
            guard let url = URL(string: link) else { return }
            downloadedFrom(url: url, contentMode: mode)
        }
        
        
        
        func openUrl(urlString:String!) {
            let url = URL(string: urlString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    extension String {
        
        subscript (i: Int) -> Character {
            return self[index(startIndex, offsetBy: i)]
        }
        
        subscript (i: Int) -> String {
            return String(self[i] as Character)
        }
        
        subscript (r: Range<Int>) -> String {
            let start = index(startIndex, offsetBy: r.lowerBound)
            let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
            return self[Range(start ..< end)]
        }
        func toDate() -> Date
        {
            //Create Date Formatter
            let dateFormatter = DateFormatter()
            
            //Specify Format of String to Parse
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            //Parse into NSDate
            let dateFromString : Date = dateFormatter.date(from: self)!
            
            //Return Parsed Date
            return dateFromString
        }

         func convertDateFromServer(dateString : String, dateFormat: String, bAddHours : Bool) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormat
            
            if dateString != ""
            {
                var fullName = dateString.components(separatedBy: "(")
                
                let lastName: String? = fullName[1]
                var fullNameArr = lastName!.components(separatedBy: "+")
                var sumHour = 0
                if fullNameArr.count == 1 {
                    fullNameArr = lastName!.components(separatedBy: ")")
                } else if bAddHours {
                    var arr = fullNameArr[1].components(separatedBy: ")")
                    if arr.count > 0 {
                        if let num = Int(arr[0]) {
                            
                            sumHour = (num/100) * 60 * 60 * 1000
                        }
                    }
                }
                let lastNam: String? = fullNameArr.count > 0 ? fullNameArr[0] : lastName
                
                var myDouble = Double(lastNam!)
                myDouble = myDouble! + Double(sumHour)
                let date = Date(timeIntervalSince1970: myDouble!/1000.0)
                
                return dateFormatter.string(from:date as Date)
            }
            
            return ""
        }
    }
    extension Date {
        // מחזיר הפרשי זמן בין שתי תאריכים
        mutating func offsetFrom(date: Date) -> String {
    
            let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
                    let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: self, to: date);
            
           
                    let seconds = "\(difference.second ?? 0)"
            let minutes = "\(difference.minute ?? 0)" + ":" + seconds
            let hours = "\(difference.hour ?? 0)" + ":" + minutes
            let days = "\(difference.day ?? 00)" + " " + hours
            
            if days.characters.contains("-"){
                return "00:00:00"
            }
                 return " \(days)"
           
           
//var s = ""
           // return " \(days)  \(hours):\(minutes):\(seconds)"
//            if let day = difference.day, day          > 0 { return days }
//            if let hour = difference.hour, hour       > 0 { return hours }
//            if let minute = difference.minute, minute > 0 { return minutes }
//            if let second = difference.second, second > 0 { return seconds }
//            return ""
        }
        func getStringFromDate(today:Date)->String
        {
            let formatter  = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            let todayDate = formatter.string(from: today)
            return todayDate
            
        }
        
        var weekdayOrdinal: Int {
            return Calendar.current.component(.weekday, from: self)
        }
}
