//
//  LocalizableStringStatic.swift
//  FineWork
//
//  Created by Lior Ronen on 12/12/2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class LocalizableStringStatic: NSObject {
    static let sharedInstance : LocalizableStringStatic = {
        let instance = LocalizableStringStatic()
        return instance
    }()
    //MARK:Images
    var LOGIN_IMAGE_LOGO:String = "LOGIN_IMAGE_LOGO";
    //MARK:General
    var FIELDS_NOT_FILLED:String = "FIELDS_NOT_FILLED";
    var OK_ALERT: String = "OK_ALERT"
    var CANCEL_ALERT: String = "CANCEL_ALERT"
    var SAVE:String = "SAVE"
    var FAILURE_SERVER:String = "FAILURE_SERVER"
    var NO_INTERNET:String = "NO_INTERNET"
    var BAD_IMAGE:String = "BAD_IMAGE"
    //MARK:LoginViewController
    var LOGIN_TITLE:String = "LOGIN_TITLE";
    var LOGIN_MAIL:String = "LOGIN_MAIL";//*general
    var LOGIN_PASSWORD:String = "LOGIN_PASSWORD";//*general
    var LOGIN_FORGOT_PASSWORD:String = "LOGIN_FORGOT_PASSWORD";
    var LOGIN:String = "LOGIN";
    var LOGIN_WITH_GOOGLE:String = "LOGIN_WITH_GOOGLE";
    var LOGIN_WITH_FACEBOOK:String = "LOGIN_WITH_FACEBOOK";
    var LOGIN_IS_NOT_YET_REGISTER:String = "LOGIN_IS_NOT_YET_REGISTER";
    var LOGIN_REGISTER:String = "LOGIN_REGISTER";
    //MARK:RegisterViewController
    var REGISTER_TITLE:String = "REGISTER_TITLE"
    var REGISTER_WITH_FACEBOOK:String = "REGISTER_WITH_FACEBOOK"
    var REGISTER_WITH_GOOGLE:String = "REGISTER_WITH_GOOGLE"
    var REGISTER_AS_EMPLOYER:String = "REGISTER_AS_EMPLOYER"
    var REGISTER_AS_EMPLOYEE:String = "REGISTER_AS_EMPLOYEE"
    var REGISTER_USER_NAME:String = "REGISTER_USER_NAME"
    var REGISTER_MOBILE:String = "REGISTER_MOBILE"
    var REGISTER_MAIL:String = "REGISTER_MAIL"
    var REGISTER_PASSWORD:String = "REGISTER_PASSWORD"
    var REGISTER_RECOMEND_PHONE:String = "REGISTER_RECOMEND_PHONE"
    var SAVE_SUCCESSFULLY:String = "SAVE_SUCCESSFULLY"
    var TEMPORAY_SAVING:String = "TEMPORAY_SAVING"
    var TEMPORAY_SAVING_SMALL_TEXT:String = "TEMPORAY_SAVING_SMALL_TEXT"
    var VIEW_FILE:String = "VIEW_FILE"
    var CONFIRMATION_OfCHILD_ALLOWANCE:String = "CONFIRMATION_OfCHILD_ALLOWANCE"
    var UPLOAD_TZ:String = "UPLOAD_TZ"
    var UPLOAD_CONFIRMATION_OF_SEPERATE:String = "UPLOAD_CONFIRMATION_OF_SEPERATE"
    var UPLOAD_DISABLED_BLIND:String = "UPLOAD_DISABLED_BLIND"
    var UPLOAD_VERDICT:String = "UPLOAD_VERDICT"//צרף צילום פסק דין
    var UPLOAD_119FORM:String = "UPLOAD_119FORM"
    var UPLOAD_FOR_HANDICAPPED:String = "UPLOAD_FOR_HANDICAPPED"
    var UPLOAD_IMMEGRANT_FILE:String = "UPLOAD_IMMEGRANT_FILE"
    var UPLOAD_FOR_RESIDENT_PLACE:String = "UPLOAD_FOR_RESIDENT_PLACE"
    var UPLOAD_SOLDIER_FILE:String = "UPLOAD_SOLDIER_FILE"
    var UPLAOD_EMPLOYER_FILE:String = "UPLAOD_EMPLOYER_FILE"
    var ADD_FILE:String = "ADD_FILE"
    var ERRORFILES:String = "ERRORFILES"
    var EXIST_TZ:String = "EXIST_TZ"
    var SAVESUCCESS_FILLREQUIRED:String = "SAVESUCCESS_FILLREQUIRED"
    var OK_READING_REGULATION:String = "OK_READING_REGULATION"
    //MARK: - VerficationCodePopUpViewController
    var ENTER_VERIFICATION:String = "ENTER_VERIFICATION"
    var IILEGAL_CODE:String = "IILEGAL_CODE"
    
    //MARK: - Alert
    var SURE_DELETE_IMAGR_FILE:String = "SURE_DELETE_IMAGR/FILE"
    
    //MARK: - EmploeeDetalis
    var REMOVE:String = "REMOVE"
}
