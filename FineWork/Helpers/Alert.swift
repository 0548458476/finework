//
//  Alert.swift
//  v-check
//
//  Created by User on 24.11.2015.
//  Copyright © 2015 User. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
var selfController:UIViewController = UIViewController()


class Alert: NSObject {
    static let sharedInstance : Alert = {
        let instance = Alert()
        return instance
    }()
    
    func AlertopenPictures(controller:UIViewController,tag:Int)
    {
        // Your action
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        
        
        settingsActionSheet.addAction(UIAlertAction(title: "פתח מצלמה"/*NSLocalizedString("OPEN_CAMERA", comment: "")*/, style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePicker = UIImagePickerController()
                
                if tag == 1//personalInformationModel
                {
                    if let _ = controller as? PersonalInformationModelViewController
                    {
                        imagePicker.delegate = controller as! PersonalInformationModelViewController
                    }
                }
                else if tag == 2//employeeDetails
                {
                    if let _ = controller as? EmployeeDetailsViewController
                    {
                        imagePicker.delegate = controller as! EmployeeDetailsViewController
                    }
                }
//                else
//                {
//                    imagePicker.delegate = controller as! Update_details_Update_details_UpdateDetailsViewController
//                }
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        settingsActionSheet.addAction(UIAlertAction(title: "פתח תמונות"/*NSLocalizedString("OPEN_IMAGE", comment: "")*/, style:UIAlertActionStyle.default, handler:{ (UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                
                if tag == 1//personalInformationModel
                {
                    if let _ = controller as? PersonalInformationModelViewController
                    {
                        imagePicker.delegate = controller as! PersonalInformationModelViewController
                    }
                }
                else if tag == 2//employeeDetails
                {
                    if let _ = controller as? EmployeeDetailsViewController
                    {
                        imagePicker.delegate = controller as! EmployeeDetailsViewController
                    }
                }
//                else
//                {
//                    imagePicker.delegate = controller as! Update_details_Update_details_UpdateDetailsViewController
//                }
                
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                
                imagePicker.allowsEditing = true
                
                controller.modalPresentationStyle = UIModalPresentationStyle.custom
                controller.present(imagePicker, animated: true, completion: nil)
            }
            }
            
            // { action in self.presentRandomJoke()
            //}
            ))
        
        settingsActionSheet.addAction(UIAlertAction(title: "ביטול"/*NSLocalizedString("CANCEL", comment: "")*/, style:UIAlertActionStyle.cancel, handler:nil))
        
        controller.present(settingsActionSheet, animated:true, completion:nil)
    }
    
    func showAlertViewController(_message : String,controller:UIViewController)
    {
        selfController = controller
        var message : String = "\n"
        for _ in 0 ..< 2
        {
            message = message + "\n\n"
        }
        
        let alert : UIAlertController = UIAlertController(title:  "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let text : UILabel = UILabel()
        //let y = alert.view.frame.height / 40
        
    
        //text.frame = CGRectMake(alert.view.frame.origin.x/2 , (CGFloat)((1) * 25), alert.view.frame.width * 2/3, 25)
        text.textColor = UIColor(red: 18/255, green: 96/255, blue: 135/255, alpha: 1.0)
        //text.textColor = UIColor(red: 18, green: 96, blue: 135, alpha: 0.1)
        text.textAlignment = NSTextAlignment.center
        text.font = UIFont(name: "FbSpacer-Regular", size: 14)
        
        text.text = _message
        
        alert.view.addSubview(text)
        
        let button : UIButton = UIButton()
       
        text.frame.size.height = alert.view.frame.height * 1/15
        text.frame.size.width = alert.view.frame.width * 4/5
        
        button.setBackgroundImage(UIImage(named: "butten.png"), for: UIControlState.normal)
        button.setTitle("אישור", for: UIControlState.normal)
       
        alert.view.addSubview(button)
        controller.present(alert, animated: true, completion: nil)
    }
    func showAlertViewControllerForLongMessage(sendMessage: Array<String>,controller:UIViewController)//_message : String,controller:UIViewController)
    {
        selfController=controller
        var message : String = "\n"
        for _ in 0 ..< sendMessage.count {
            message = message + "\n\n\n"
        }
        
        let alert : UIAlertController = UIAlertController(title:  "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        for i in 0 ..< sendMessage.count {
            let text : UILabel = UILabel()
            //var x=alert.view.window?.frame.origin.x
            var y = alert.view.frame.height / 15
            let ii=Double(i)*25.0
            y = y + CGFloat(ii)
           // text.center=CGPointMake(alert.view.frame.width/40, y)
            //text.frame.width=alert.view.frame.width
            text.textColor = UIColor(red: 18/255, green: 96/255, blue: 135/255, alpha: 1.0)
            text.frame.size.height = alert.view.frame.height * 1/20
            text.frame.size.width = alert.view.frame.width * 4/5
            // CGRectMake(x, (CGFloat)((i + 1) * 25), alert.view.frame.width , 25)
         //   text.textAlignment = NSTextAlignment.Center
            text.text = sendMessage[i]
            alert.view.addSubview(text)
        }
        

    }
    

    
    func alertNo()
    {
        selfController.dismiss(animated: true, completion: nil)
    }
    func alertYes()
    {
        selfController.dismiss(animated: true, completion: nil)
        
    }
    
    
    //    func showAlertViewController(_message : String,controller:UIViewController)
    //    {
    //        var message : String = "\n"
    //        for(var i = 0; i < 10; i++) {
    //            message = message + "\n\n"
    //        }
    //
    //        let alert:UIAlertController = UIAlertController(title: "", message: _message, preferredStyle: UIAlertControllerStyle.Alert)
    //
    //         //alert.setFrame = CGRectMake(10, 100, 300, 320)
    //            let text : UILabel = UILabel()
    //            text.frame = CGRectMake(0, (CGFloat)(200), alert.view.frame.width * 2/3, 25)
    //            text.textAlignment = NSTextAlignment.Center
    //            text.text = _message
    //            alert.view.addSubview(text)
    //
    //
    //        let button : UIButton = UIButton()
    //        button.frame = CGRectMake(alert.view.frame.origin.x/2 + 50, alert.view.frame.origin.y/2 + (CGFloat)((1) * 35) + 25, 170, 45)
    //        button.setBackgroundImage(UIImage(named: "ok.png"), forState: UIControlState.Normal)
    //        button.addTarget(self, action: "dismissAlert", forControlEvents: UIControlEvents.TouchDown)
    //        alert.view.addSubview(button)
    //        controller.presentViewController(alert, animated: true, completion: nil)
    //    }
    
    
    //    func showAlertViewController(controller:UIViewController)
    //    {
    //        let message : String = " פרטיך התעדכנו בהצלחה"
    //
    //        let alert : UIAlertController = UIAlertController(title:  "", message: message, preferredStyle: UIAlertControllerStyle.Alert)
    //
    //        let button : UIButton = UIButton()
    //        button.frame = CGRectMake(alert.view.frame.origin.x/2 + 50, alert.view.frame.origin.y/2 + 25, 170, 45)
    //        button.setBackgroundImage(UIImage(named: "ok.png"), forState: UIControlState.Normal)
    //        button.addTarget(self, action: "dismissAlert", forControlEvents: UIControlEvents.TouchDown)
    //        alert.view.addSubview(button)
    //        controller.presentViewController(alert, animated: true, completion: nil)
    //    }
    //
    func showAlert(mess:String)
    {
        let text: UILabel = UILabel()
        text.font = UIFont(name: text.font.fontName, size: 14)
        text.text = mess
        text.textAlignment = NSTextAlignment.center
        var frame:CGRect = CGRect()
        frame.size = CGSize(width: 100, height: 100)
        frame.origin = CGPoint(x: 30,y: 30)
        text.frame = frame
        text.backgroundColor = UIColor.red
        let alert = UIAlertView()
        //alert.setValue(text, forKey: "accessoryView")
        alert.addButton(withTitle: "אישור")
        alert.title = ""
        alert.message = mess
        alert.addSubview(text)
        alert.show()
        
        //        let alert : UIAlertController = UIAlertController(title: "", message: mess, preferredStyle: UIAlertControllerStyle.Alert)
        //          let okAction:UIAlertAction = UIAlertAction(title: "אישור", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) in
        //            self.dismissAlert()
        //        })
        //
        //        alert.addAction(okAction)
        
        
    }
    
    func showAlertWithTitle(mess:String,title:String){
        let alert = UIAlertView()
        alert.addButton(withTitle: "אישור")
        alert.title = title
        alert.message = mess
        alert.show()
//                let alert : UIAlertController = UIAlertController(title: title, message: mess, preferredStyle: UIAlertControllerStyle.alert)
//                  let okAction:UIAlertAction = UIAlertAction(title: "אישור", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
////                    self.dismissAlert()
//                    alert.dismiss(animated: true, completion: nil)
//                })
//                alert.addAction(okAction)
    }
    
    func showAlertWithCellDelegate(mess:String,delegate:UITableViewCell){
        let alert = UIAlertView()
        alert.addButton(withTitle: "אישור")
        alert.message = mess
        alert.delegate = delegate
        alert.show()
    }
    
    func showAlertWithCellDelegateTag(mess:String,delegate:UITableViewCell,tag:Int){
        let alert = UIAlertView()
        alert.addButton(withTitle: "אישור")
        alert.message = mess
        alert.delegate = delegate
        alert.tag = tag
        alert.show()
    }
    
    func showAlertWithDelegate(mess:String,delegate:UIViewController){
        let alert = UIAlertView()
        alert.addButton(withTitle: "אישור")
        alert.message = mess
        alert.delegate = delegate
        alert.show()
    }
    
    func showAskAlertWithCellDelegate(mess:String,delegate:UITableViewCell){
        let alert = UIAlertView()
        alert.addButton(withTitle: "אישור")
        alert.addButton(withTitle: "ביטול")
        alert.message = mess
        alert.delegate = delegate
        alert.show()
    }
    
    func showAskAlertWithCellDelegateTag(mess:String,delegate:UITableViewCell,tag:Int){
        let alert = UIAlertView()
        alert.addButton(withTitle: "אישור")
        alert.addButton(withTitle: "ביטול")
        alert.message = mess
        alert.delegate = delegate
        alert.tag = tag
        alert.show()
    }
    
    func showAskAlertWithDelegate(mess:String,delegate:UIViewController){
        let alert = UIAlertView()
        alert.addButton(withTitle: "אישור")
        alert.addButton(withTitle: "ביטול")
        alert.message = mess
        alert.delegate = delegate
        alert.show()
    }
    
    
    
    }
