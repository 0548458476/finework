//
//  EmployerLocation.swift
//  FineWork
//
//  Created by Lior Ronen on 27/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmployerLocation: NSObject {

    var nvLogo : String = "" //לוגו מעסיק
    var nvLng : String = "" //נקודת אורך
    var nvLat : String = "" //נקודת רוחב
    
    func getEmployerLocationAsDic() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["nvLogo"] = nvLogo as AnyObject
        dic["nvLng"] = nvLng as AnyObject
        dic["nvLat"] = nvLat as AnyObject
        
        return dic
    }
    
    func getWorkerLanguageFromDic(dic : Dictionary<String, AnyObject>) -> EmployerLocation {
        let employerLocation = EmployerLocation()
        
        if dic["nvLogo"] != nil {
            employerLocation.nvLogo = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLogo"]!)
        }
        if dic["nvLng"] != nil {
            employerLocation.nvLng = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLng"]!)
        }
        if dic["nvLat"] != nil {
            employerLocation.nvLat = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLat"]!)
        }
        return employerLocation
    }

}
