//
//  WorkerTaxRelifeReason.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerTaxRelifeReason: NSObject {
    
    //MARK: - Variables

    var iTaxRelifeReasonId:Int //מזהה סיבה
    var nvValueContent1:String //ערך 1
    var nvValueContent2:String //ערך 2
    var nvValueContent3:String //ערך 3
    var nvValueContent4:String //ערך 4
    var fileObj:FileObj// קובץ מצורף
    
    //MARK: - Initial
    
    override init() {
        iTaxRelifeReasonId = 0
        nvValueContent1 = ""
        nvValueContent2 = ""
        nvValueContent3 = ""
        nvValueContent4 = ""
        fileObj = FileObj()
    }
    
    init(_iTaxRelifeReasonId:Int,_nvValueContent1:String,_nvValueContent2:String,_nvValueContent3:String,_nvValueContent4:String,_fileObj:FileObj) {
        iTaxRelifeReasonId = _iTaxRelifeReasonId
        nvValueContent1 = _nvValueContent1
        nvValueContent2 = _nvValueContent2
        nvValueContent3 = _nvValueContent3
        nvValueContent4 = _nvValueContent4
        fileObj = _fileObj
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        editDateToFromServer()
        dic["iTaxRelifeReasonId"] = iTaxRelifeReasonId as AnyObject?
        if nvValueContent1 != ""
        {
            dic["nvValueContent1"] = nvValueContent1 as AnyObject?
        }
        if nvValueContent2 != ""
        {
            dic["nvValueContent2"] = nvValueContent2 as AnyObject?
        }
        if nvValueContent3 != ""
        {
            dic["nvValueContent3"] = nvValueContent3 as AnyObject?
        }
        if nvValueContent4 != ""
        {
            dic["nvValueContent4"] = nvValueContent4 as AnyObject?
        }
        //תוספת כדי שלא ידרוס קובץ שכבר נשמר בשרת 
        //|| fileObj.nvFileURL != ""
        if fileObj.nvFile != "" || fileObj.nvFileURL != ""
        {
            dic["fileObj"] = fileObj.getDic() as AnyObject?
        }
        
        return dic
    }
    
    func dicToWorkerTaxRelifeReason(dic:Dictionary<String,AnyObject>)->WorkerTaxRelifeReason
    {
        var workerTaxRelifeReason:WorkerTaxRelifeReason = WorkerTaxRelifeReason()
        workerTaxRelifeReason.iTaxRelifeReasonId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iTaxRelifeReasonId"]!)
        workerTaxRelifeReason.nvValueContent1 = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvValueContent1"]!)
        workerTaxRelifeReason.nvValueContent2 = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvValueContent2"]!)
        workerTaxRelifeReason.nvValueContent3 = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvValueContent3"]!)
        workerTaxRelifeReason.nvValueContent4 = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvValueContent4"]!)
        
        if dic["fileObj"] is NSNull
        {
            workerTaxRelifeReason.fileObj = fileObj
        }
        else
        {
            workerTaxRelifeReason.fileObj = fileObj.dicToFileObj(dic: dic["fileObj"] as! Dictionary<String, AnyObject>)
        }
        workerTaxRelifeReason = editDateToFromServer(workerTaxRelifeReason:workerTaxRelifeReason)!
        return workerTaxRelifeReason
    }
    
    func editDateToFromServer(workerTaxRelifeReason:WorkerTaxRelifeReason? = nil)->WorkerTaxRelifeReason?{//הפורמט של תאריך הסיבות בשרת הפוך
        if let taxRelifeREason = workerTaxRelifeReason{
            if taxRelifeREason.iTaxRelifeReasonId == 3{//תושב קבוע
                taxRelifeREason.nvValueContent1 = String(taxRelifeREason.nvValueContent1.characters.reversed())
            }else if taxRelifeREason.iTaxRelifeReasonId == 4{//עולה חדש
                taxRelifeREason.nvValueContent2 = String(taxRelifeREason.nvValueContent2.characters.reversed())
            }else if taxRelifeREason.iTaxRelifeReasonId == 13{//חייל משוחרר
                taxRelifeREason.nvValueContent1 = String(taxRelifeREason.nvValueContent1.characters.reversed())
               taxRelifeREason.nvValueContent2 = String(taxRelifeREason.nvValueContent2.characters.reversed())
            }
            return taxRelifeREason
        }else{
            if iTaxRelifeReasonId == 3{//תושב קבוע
                nvValueContent1 = String(nvValueContent1.characters.reversed())
            }else if iTaxRelifeReasonId == 4{//עולה חדש
                nvValueContent2 = String(nvValueContent2.characters.reversed())
            }else if iTaxRelifeReasonId == 13{//חייל משוחרר
                nvValueContent1 = String(nvValueContent1.characters.reversed())
                nvValueContent2 = String(nvValueContent2.characters.reversed())
            }
            return nil
        }
    }
    
    
    func workerTaxRelifeReasonToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<WorkerTaxRelifeReason>{
        
        var arrWorkerTaxRelifeReason:Array<WorkerTaxRelifeReason> = Array<WorkerTaxRelifeReason>()
        var workerTaxRelifeReason:WorkerTaxRelifeReason = WorkerTaxRelifeReason()
        
        for i in 0  ..< arrDic.count
        {
            workerTaxRelifeReason = dicToWorkerTaxRelifeReason(dic: arrDic[i])
            arrWorkerTaxRelifeReason.append(workerTaxRelifeReason)
        }
        return arrWorkerTaxRelifeReason
    }
}
