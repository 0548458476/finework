//
//  WorkerTaxCoordinationFileDet.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerTaxCoordinationFileDet: NSObject {
    
    //MARK: - Variables

    var nTillAmount:Float //עד סכום
    var fDeductedPercent:Float //אחוזי ניכוי
    var nBeyondTaxDeducted:Float //מעבר לסך זה שיעור המס
    var fileObj:FileObj// תיאום מס קיים

    //MARK: - Initial
    
    override init() {
        nTillAmount = 0.0
        fDeductedPercent = 0.0
        nBeyondTaxDeducted = 0.0
        fileObj = FileObj()
    }
    
    init(_nTillAmount:Float,_fDeductedPercent:Float,_nBeyondTaxDeducted:Float,_fileObj:FileObj) {
        nTillAmount = _nTillAmount
        fDeductedPercent = _fDeductedPercent
        nBeyondTaxDeducted = _nBeyondTaxDeducted
        fileObj = _fileObj
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        if nTillAmount != 0.0
        {
            dic["mTillAmount"] = nTillAmount as AnyObject?
        }
        if fDeductedPercent != 0.0
        {
            dic["fDeductedPercent"] = fDeductedPercent as AnyObject?
        }
        if nBeyondTaxDeducted != 0.0
        {
            dic["fBeyondTaxDeducted"] = nBeyondTaxDeducted as AnyObject?
        }
        dic["fileObj"] = fileObj.getDic() as AnyObject?
        
        return dic
    }
    
    func dicToWorkerTaxCoordinationFileDet(dic:Dictionary<String,AnyObject>)->WorkerTaxCoordinationFileDet
    {
        let workerTaxCoordinationFileDet:WorkerTaxCoordinationFileDet = WorkerTaxCoordinationFileDet()
        
        if dic["mTillAmount"] != nil
        {
            workerTaxCoordinationFileDet.nTillAmount = Global.sharedInstance.parseJsonToFloat(floatToParse:dic["mTillAmount"]!)
        }
        else
        {
            workerTaxCoordinationFileDet.nTillAmount = 0.0
        }
        
        if dic["fDeductedPercent"] != nil
        {
            workerTaxCoordinationFileDet.fDeductedPercent = Global.sharedInstance.parseJsonToFloat(floatToParse: dic["fDeductedPercent"]!)
        }
        else
        {
            workerTaxCoordinationFileDet.fDeductedPercent = 0.0
        }
        
        if dic["fBeyondTaxDeducted"] != nil
        {
            workerTaxCoordinationFileDet.nBeyondTaxDeducted = Global.sharedInstance.parseJsonToFloat(floatToParse: dic["fBeyondTaxDeducted"]!)
        }
        else{
            workerTaxCoordinationFileDet.nBeyondTaxDeducted = 0.0
        }
        if dic["fileObj"] is NSNull
        {
            workerTaxCoordinationFileDet.fileObj = fileObj
        }
        else
        {
            workerTaxCoordinationFileDet.fileObj = fileObj.dicToFileObj(dic: dic["fileObj"] as! Dictionary<String, AnyObject>)
        }
        
        return workerTaxCoordinationFileDet
    }
}
