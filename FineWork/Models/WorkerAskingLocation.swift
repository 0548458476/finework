//
//  WorkerAskingLocation.swift
//  FineWork
//
//  Created by Lior Ronen on 27/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerAskingLocation: NSObject {

    var iWorkerAskingLocationId : Int = 0 //מזהה מיקום לעובד
    var iWorkerUserId : Int = 0 //מזהה העובד
    var nvAddress : String = "" //כתובת
    var nvLng : String = "" //נקודת אורך
    var nvLat : String = "" //נקודת רוחב
    var nRadius : Float? = nil //רדיוס
    var bDeleted : Bool = false //למחיקה?
    
    func getWorkerAskingLocationAsDic() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerAskingLocationId"] = iWorkerAskingLocationId as AnyObject
        dic["iWorkerUserId"] = iWorkerUserId as AnyObject
        dic["nvAddress"] = nvAddress as AnyObject
        dic["nvLng"] = nvLng as AnyObject
        dic["nvLat"] = nvLat as AnyObject
        
        if nRadius != nil {
            dic["nRadius"] = nRadius as AnyObject
        }
        
        dic["bDeleted"] = bDeleted as AnyObject
        
        return dic
    }
    
    func getWorkerAskingLocationFromDic(dic : Dictionary<String, AnyObject>) -> WorkerAskingLocation {
        let workerAskingLocation = WorkerAskingLocation()
        
        if dic["iWorkerAskingLocationId"] != nil {
            workerAskingLocation.iWorkerAskingLocationId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerAskingLocationId"]!)
        }
        if dic["iWorkerUserId"] != nil {
            workerAskingLocation.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!)
        }
        if dic["nvAddress"] != nil {
            workerAskingLocation.nvAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!)
        }
        if dic["nvLng"] != nil {
            workerAskingLocation.nvLng = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLng"]!)
        }
        if dic["nvLat"] != nil {
            workerAskingLocation.nvLat = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLat"]!)
        }
        if dic["nRadius"] != nil {
            workerAskingLocation.nRadius = Global.sharedInstance.parseJsonToFloatOptionNil(floatToParse: dic["nRadius"]!)
        }
        if dic["bDeleted"] != nil {
            workerAskingLocation.bDeleted = dic["bDeleted"] as! Bool
        }
        
        return workerAskingLocation
    }
}
