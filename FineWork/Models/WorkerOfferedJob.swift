//
//  WorkerOfferedJob.swift
//  FineWork
//
//  Created by User on 25.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerOfferedJob: NSObject {

    //משרה מוצעת
    var iWorkerOfferedJobId : Int = 0 //מזהה משרה מוצעת לעובד
    var iJobId : Int = 0 //מזהה משרה לעובד
    var iOfferJobStatusType : Int = 0 //סטטוס משרה מוצעת
    var nvOfferJobStatusName : String = "" //שם סטטוס משרה מוצעת
    var bFavorite : Bool = false //כוכב-העדפות
    var dtWorkerCandidacyExpiration  : String = "" //תאריך סיום הגשת מועמדות
    var lWorkerOfferedJobTimeAvailablity : Array<WorkerOfferedJobTimeAvailablity> = []//זמינות

    
    var tTimeLeft : String = "" //זמן שנותר עד לזמן סיום הגשת המועמדות
    var iMessagesCount : Int = 0 //מס’ הודעות
    var mAskingHourlyWage : Float? = nil //סכום לשעה
   // List<WorkerOfferedJobTimeAvailablity> lWorkerOfferedJobTimeAvailablity  //זמינות
    var bLoginFromApp : Bool = true //האם נכנס פעם מהאפליקציה
    var iBaloonOccur : Int = 0
    //משרה
    var nvAddress : String = ""
    var nvLng :String =  ""
    var nvLat : String = ""
    var nvDomain : String = ""
    var nvRole : String = ""
    var iEmployerAskedJobStatusType : Int = 0
    var nvJobDescription : String = ""
    var bRisk : Bool = false
    var nvRiskDetails : String = ""
    var bSafetyRequirement : Bool = false
    var nvSafetyRequirementDetails : String = ""
    var iPhysicalDifficultyRank : Int = 0
    var nvWorkSpaceDetails :String = ""
    var iEmployerJobScheduleType : Int = 0
    var employerJobNewWorker : EmployerJobNewWorker = EmployerJobNewWorker()
    var nvEmployerJobPopulations : String = ""
    var iEmployerJobWorkerType : Int = 0
    var nvEmployerJobPeriodType :String = ""
    var nvMatchDescription : String  = ""
    //מעסיק
    var iEmployerUserId : Int = 0 //מזהה משתמש מעסיק
    var iEmployerJobId : Int = 0 //מזהה משרה
    var nvEmployerName : String = "" //שם עסק
    var nvLogoImageFilePath : String = "" //לוגו
    var nvBusinessDescription : String = "" //תאור עסק
    var nvEmployeeNumberType : String = "" //כמות עובדים בחברה
    var nvMainActivityFieldType : String = "" //תחום פעילות מרכזי
    var nvCentralAreaActivity : String = "" //אזור מרכזי בו פועלים
    var nvSiteLink : String = "" //לינק לאתר החברה
    var ranking : Ranking?  //דרוג המעסיק
    var mExpectationsCostJob :Float?//צפיות עלות המשרה

    //עובד
    var nvWorkerName : String = ""
    var iWorkerUserId : Int = 0
    var bTimeReportingFlexibility : Bool = false //גמיש/קשיח
    var iTimeReportingRadius : Int? //רדיוס

    
    
    // logo image 
    var logoImage : UIImage?
    
    func getDicFromWorkerOfferedJob() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        dic["iWorkerOfferedJobId"] = self.iWorkerOfferedJobId as AnyObject?
        dic["iJobId"] = self.iJobId as AnyObject?
        dic["iOfferJobStatusType"] = self.iOfferJobStatusType as AnyObject?
        dic["nvOfferJobStatusName"] = self.nvOfferJobStatusName as AnyObject?
        dic["bFavorite"] = self.bFavorite as AnyObject?
       // dic["dtWorkerCandidacyExpiration"] = self.dtWorkerCandidacyExpiration as AnyObject?
        dic["lWorkerOfferedJobTimeAvailablity"] = getlWorkerOfferedJobTimeAvailablityDic() as AnyObject
        dic["tTimeLeft"] = self.tTimeLeft as AnyObject?
        dic["iMessagesCount"] = self.iMessagesCount as AnyObject?
        dic["mAskingHourlyWage"] = self.mAskingHourlyWage as AnyObject?
        dic["bLoginFromApp"] = self.bLoginFromApp as AnyObject?
        dic["iBaloonOccur"] = self.iBaloonOccur as AnyObject?

        
        dic["nvAddress"] = self.nvAddress as AnyObject?
        dic["nvLng"] = self.nvLng as AnyObject?
        dic["nvLat"] = self.nvLat as AnyObject?
        dic["nvDomain"] = self.nvDomain as AnyObject?
        dic["nvRole"] = self.nvRole as AnyObject?
        dic["iEmployerAskedJobStatusType"] = self.iEmployerAskedJobStatusType as AnyObject?
        dic["nvJobDescription"] = self.nvJobDescription as AnyObject?
        dic["bRisk"] = self.bRisk as AnyObject?
        dic["nvRiskDetails"] = self.nvRiskDetails as AnyObject?
        dic["bSafetyRequirement"] = self.bSafetyRequirement as AnyObject?
        dic["nvSafetyRequirementDetails"] = self.nvSafetyRequirementDetails as AnyObject?
        dic["iPhysicalDifficultyRank"] = self.iPhysicalDifficultyRank as AnyObject?
        dic["nvWorkSpaceDetails"] = self.nvWorkSpaceDetails as AnyObject?
        dic["iEmployerJobScheduleType"] = self.iEmployerJobScheduleType as AnyObject?
        dic["nvEmployerJobPopulations"] = self.nvEmployerJobPopulations as AnyObject?
        dic["nvMatchDescription"] = self.nvMatchDescription as AnyObject?
        dic["iEmployerJobWorkerType"] = self.iEmployerJobWorkerType as AnyObject?
        dic["nvEmployerJobPeriodType"] = self.nvEmployerJobPeriodType as AnyObject?
        dic["iEmployerUserId"] = self.iEmployerUserId as AnyObject?
        dic["iEmployerJobId"] = self.iEmployerJobId as AnyObject?
        dic["nvEmployerName"] = self.nvEmployerName as AnyObject?
        dic["nvLogoImageFilePath"] = self.nvLogoImageFilePath as AnyObject?
        dic["nvBusinessDescription"] = self.nvBusinessDescription as AnyObject?
        dic["nvEmployeeNumberType"] = self.nvEmployeeNumberType as AnyObject?
        dic["nvMainActivityFieldType"] = self.nvMainActivityFieldType as AnyObject?
        dic["nvCentralAreaActivity"] = self.nvCentralAreaActivity as AnyObject?
        dic["nvSiteLink"] = self.nvSiteLink as AnyObject?
       // dic["ranking"] = self.ranking?.getRankingDic() as AnyObject?
          dic["mExpectationsCostJob"] = self.mExpectationsCostJob as AnyObject?
        dic["nvWorkerName"] = self.nvWorkerName as AnyObject?
        dic["iWorkerUserId"] = self.iWorkerUserId as AnyObject?
        dic["bTimeReportingFlexibility"] = self.bTimeReportingFlexibility as AnyObject?
        dic["iTimeReportingRadius"] = self.iTimeReportingRadius as AnyObject?

        return dic
    }
    func getlWorkerOfferedJobTimeAvailablityDic()->Array<AnyObject>
    {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for workerOffer in lWorkerOfferedJobTimeAvailablity
        {
            dic = workerOffer.getDicFromWorkerOfferedJobTimeAvailablity()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    

    func getWorkerOfferedJobFromDic(dic : Dictionary<String, AnyObject>) -> WorkerOfferedJob {
        let workerOfferedJob = WorkerOfferedJob()
        
        if dic["iWorkerOfferedJobId"] != nil {
            workerOfferedJob.iWorkerOfferedJobId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerOfferedJobId"]!)
        }
        if dic["iJobId"] != nil {
            workerOfferedJob.iJobId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iJobId"]!)
        }
        if dic["iOfferJobStatusType"] != nil {
            workerOfferedJob.iOfferJobStatusType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iOfferJobStatusType"]!)
        }
        if dic["nvOfferJobStatusName"] != nil {
            workerOfferedJob.nvOfferJobStatusName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvOfferJobStatusName"]!)
        }
        if dic["bFavorite"] != nil {
            workerOfferedJob.bFavorite = dic["bFavorite"] as! Bool
        }
        if dic["dtWorkerCandidacyExpiration"] != nil {
            workerOfferedJob.dtWorkerCandidacyExpiration = Global.sharedInstance.parseJsonToString(stringToParse: dic["dtWorkerCandidacyExpiration"]!)
        }
        if dic["tTimeLeft"] != nil {
            workerOfferedJob.tTimeLeft = Global.sharedInstance.parseJsonToString(stringToParse: dic["tTimeLeft"]!)
        }
        if dic["lWorkerOfferedJobTimeAvailablity"] != nil {
            if let array =  dic["lWorkerOfferedJobTimeAvailablity"] as? Array<Dictionary<String, AnyObject>>{
                   workerOfferedJob.lWorkerOfferedJobTimeAvailablity = getWorkerOfferedJobTimeAvailablityFromDic(dic: array)
            }
         
        }
        if dic["iMessagesCount"] != nil {
            workerOfferedJob.iMessagesCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iMessagesCount"]!)
        }
        if dic["mAskingHourlyWage"] != nil {
            workerOfferedJob.mAskingHourlyWage = Global.sharedInstance.parseJsonToFloatOptionNil(floatToParse: dic["mAskingHourlyWage"]!)
        }
        if dic["nvLogoImageFilePath"] != nil {
            workerOfferedJob.nvLogoImageFilePath = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLogoImageFilePath"]!)
        }
        if dic["iBaloonOccur"] != nil {
            workerOfferedJob.iBaloonOccur = Global.sharedInstance.parseJsonToInt(intToParse: dic["iBaloonOccur"]!)

        }

        
        //
        if dic["nvAddress"] != nil {
            workerOfferedJob.nvAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!)
        }
        
        if dic["nvLng"] != nil {
            workerOfferedJob.nvLng = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLng"]!)
        }

        
        if dic["nvLat"] != nil {
            workerOfferedJob.nvLat = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLat"]!)
        }
        if dic["nvDomain"] != nil {
            workerOfferedJob.nvDomain = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvDomain"]!)
        }
        if dic["nvRole"] != nil {
            workerOfferedJob.nvRole = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRole"]!)
        }
        if dic["iEmployerAskedJobStatusType"] != nil {
            workerOfferedJob.iEmployerAskedJobStatusType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerAskedJobStatusType"]!)
        }
        if dic["nvJobDescription"] != nil {
            workerOfferedJob.nvJobDescription = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvJobDescription"]!)
        }
        if dic["nvRiskDetails"] != nil {
            workerOfferedJob.nvRiskDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRiskDetails"]!)
        }
        if dic["bSafetyRequirement"] != nil {
            workerOfferedJob.bSafetyRequirement = dic["bSafetyRequirement"] as! Bool
        }
        if dic["nvSafetyRequirementDetails"] != nil {
            workerOfferedJob.nvSafetyRequirementDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSafetyRequirementDetails"]!)
        }
        if dic["iPhysicalDifficultyRank"] != nil {
            workerOfferedJob.iPhysicalDifficultyRank = Global.sharedInstance.parseJsonToInt(intToParse: dic["iPhysicalDifficultyRank"]!)
        }
        if dic["nvWorkSpaceDetails"] != nil {
            workerOfferedJob.nvWorkSpaceDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkSpaceDetails"]!)
        }
        if dic["nvWorkSpaceDetails"] != nil {
            workerOfferedJob.nvWorkSpaceDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkSpaceDetails"]!)
        }
        
        if dic["iEmployerJobScheduleType"] != nil {
            workerOfferedJob.iEmployerJobScheduleType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobScheduleType"]!)
        }
    
            if dic["employerJobNewWorker"] != nil {
                let employer : EmployerJobNewWorker = EmployerJobNewWorker()
                if ((dic["employerJobNewWorker"] as? Dictionary<String, AnyObject>) != nil) {
                workerOfferedJob.employerJobNewWorker = employer.getEmployerJobNewWorkerFromDic(dic: dic["employerJobNewWorker"] as! Dictionary<String, AnyObject>)
                }
            }
      
        if dic["nvEmployerJobPopulations"] != nil {
            workerOfferedJob.nvEmployerJobPopulations = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerJobPopulations"]!)
        }
        if dic["iEmployerJobWorkerType"] != nil {
            workerOfferedJob.iEmployerJobWorkerType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobWorkerType"]!)
        }
        
        if dic["nvEmployerJobPeriodType"] != nil {
            workerOfferedJob.nvEmployerJobPeriodType = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerJobPeriodType"]!)
        }
        if dic["nvMatchDescription"] != nil {
            workerOfferedJob.nvMatchDescription = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMatchDescription"]!)
        }

    // מעסיק
        if dic["iEmployerUserId"] != nil {
            workerOfferedJob.iEmployerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerUserId"]!)
        }
        if dic["iEmployerJobId"] != nil {
            workerOfferedJob.iEmployerJobId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobId"]!)
        }
        if dic["nvEmployerName"] != nil {
            workerOfferedJob.nvEmployerName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerName"]!)
        }
        
        if dic["nvBusinessDescription"] != nil {
            workerOfferedJob.nvBusinessDescription = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvBusinessDescription"]!)
        }
        
        if dic["nvEmployeeNumberType"] != nil {
            workerOfferedJob.nvEmployeeNumberType = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployeeNumberType"]!)
        }
        
        if dic["nvMainActivityFieldType"] != nil {
            workerOfferedJob.nvMainActivityFieldType = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMainActivityFieldType"]!)
        }
        
        if dic["nvCentralAreaActivity"] != nil {
            workerOfferedJob.nvCentralAreaActivity = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvCentralAreaActivity"]!)
        }
        
        if dic["nvSiteLink"] != nil {
            workerOfferedJob.nvSiteLink = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSiteLink"]!)
        }
        if dic["nvWorkerName"] != nil {
            workerOfferedJob.nvWorkerName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkerName"]!)
        }
        if dic["bTimeReportingFlexibility"] != nil {
            workerOfferedJob.bTimeReportingFlexibility = dic["bTimeReportingFlexibility"] as! Bool
        }
        if dic["iWorkerUserId"] != nil {
            workerOfferedJob.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!)
        }
        if dic["mExpectationsCostJob"] != nil {
            workerOfferedJob.mExpectationsCostJob = Global.sharedInstance.parseJsonToFloatOptionNil(floatToParse: dic["mExpectationsCostJob"]!)
        }

        if dic["ranking"] != nil {

        let ranking : Ranking = Ranking()
             if ((dic["ranking"] as? Dictionary<String, AnyObject>) != nil) {
        workerOfferedJob.ranking = ranking.getRankingFromDic(dic: dic["ranking"] as! Dictionary<String, AnyObject>)
            }
        }
        
           
            if dic["iTimeReportingRadius"] != nil {
                workerOfferedJob.iTimeReportingRadius = Global.sharedInstance.parseJsonToInt(intToParse: dic["iTimeReportingRadius"]!)
            }
         

        
        return workerOfferedJob
    }
    
    
    func getWorkerOfferedJobTimeAvailablityFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerOfferedJobTimeAvailablity> {
        var timeAvailablity : Array<WorkerOfferedJobTimeAvailablity> = Array<WorkerOfferedJobTimeAvailablity>()
        
        var workerOfferedJobTime : WorkerOfferedJobTimeAvailablity = WorkerOfferedJobTimeAvailablity()
        
        for language in dic {
            workerOfferedJobTime = workerOfferedJobTime.getSWorkerOfferedJobTimeAvailablityFromDic(dic: language)
            timeAvailablity.append(workerOfferedJobTime)
        }
        
        return timeAvailablity
    }
    
}
