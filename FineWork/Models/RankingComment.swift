//
//  RankingComment.swift
//  FineWork
//
//  Created by User on 14.6.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class RankingComment: NSObject {
    var nvImage : String = "" //תמונה
    var nvRankingComment : String = "" //המלצה
    var nRankingSum : CGFloat = 0.0 //שקלול הדירוג
    
    var imageView : UIImage?
    
    //MARK: - Initial
    
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["nvImage"] = nvImage as AnyObject?
        dic["nvRankingComment"] = nvRankingComment as AnyObject?
        dic["nRankingSum"] = nRankingSum as AnyObject?
        
        return dic
    }
    
    /* func getImageDic()->Dictionary<String,AnyObject>
     {
     var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
     
     dic["nvFile"] = nvFile as AnyObject?
     dic["nvFileName"] = nvFileName as AnyObject?
     dic["nvFileURL"] = "" as AnyObject?
     dic["iFileId"] = 0 as AnyObject?
     
     return dic
     }
     
     */
    
    func getRankingCommentObj(dic:Dictionary<String,AnyObject>)->RankingComment
    {
        let rankingComment :RankingComment = RankingComment()
        
         if dic["nvImage"] != nil {
        rankingComment.nvImage = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImage"]!)
        }
         if dic["nvRankingComment"] != nil {
        rankingComment.nvRankingComment = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRankingComment"]!)
        }
        
        if dic["nRankingSum"] != nil {
            rankingComment.nRankingSum = CGFloat(Global.sharedInstance.parseJsonToFloat(floatToParse: dic["nRankingSum"]!))
        }
        
        return rankingComment
    }
    
    
    
}
