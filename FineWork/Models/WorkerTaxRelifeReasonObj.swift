//
//  WorkerTaxRelifeReasonObj.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerTaxRelifeReasonObj: NSObject {

    //MARK: - Variables
    
    var iWorker101FormId:Int
    var iWorkerUserId:Int
    var lWorkerTaxRelifeReasons:Array<WorkerTaxRelifeReason> = []

    //MARK: - Initial
    
    override init() {
        iWorker101FormId = 0
        iWorkerUserId = 0
        lWorkerTaxRelifeReasons = [WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason()]
    }
    
    init(_iWorker101FormId:Int,_iWorkerUserId:Int,_lWorkerTaxRelifeReasons:Array<WorkerTaxRelifeReason>) {
        iWorker101FormId = _iWorker101FormId
        iWorkerUserId = _iWorkerUserId
        lWorkerTaxRelifeReasons = _lWorkerTaxRelifeReasons
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iWorker101FormId"] = iWorker101FormId as AnyObject?
        dic["iWorkerUserId"] = iWorkerUserId as AnyObject?
        //2do add func to array as in BThere
        dic["lWorkerTaxRelifeReasons"] = lWorkerTaxRelifeReasons as AnyObject?
        
        return dic
    }
    
    func dicToWorkerTaxRelifeReasonObj(dic:Dictionary<String,AnyObject>)->WorkerTaxRelifeReasonObj
    {
        let workerTaxRelifeReasonObj:WorkerTaxRelifeReasonObj = WorkerTaxRelifeReasonObj()
        
        workerTaxRelifeReasonObj.iWorker101FormId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorker101FormId"]!)
        workerTaxRelifeReasonObj.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!)
        //2do add func to array as in BThere
//        workerTaxRelifeReasonObj.lWorkerTaxRelifeReasons = lWorkerTaxRelifeReasons
        
        return workerTaxRelifeReasonObj
    }
}
