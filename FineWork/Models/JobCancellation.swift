//
//  JobCancellation.swift
//  FineWork
//
//  Created by User on 10/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class JobCancellation: NSObject {
    
    var iEmployerJobId : Int = 0 //מזהה משרה
    var iJobId : Int = 0  //מזהה משרה לעובד
    var iCancellationReasonType : Int = 0 //סיבת ביטול
    var bCancellationCostApproved : Bool = false //עלות הביטול
    var bOutOfSystemEmplymentPaymentApproved : Bool = false//
    var iEmployerJobWorkerType : Int = 0 //סוג עובד

   
    
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iEmployerJobId"] = iEmployerJobId as AnyObject?
        dic["iJobId"] = iJobId as AnyObject?
        dic["iCancellationReasonType"] = iCancellationReasonType as AnyObject?
        dic["bCancellationCostApproved"] = bCancellationCostApproved as AnyObject?
        dic["bOutOfSystemEmplymentPaymentApproved"] = bOutOfSystemEmplymentPaymentApproved as AnyObject?
        dic["iEmployerJobWorkerType"] = iEmployerJobWorkerType as AnyObject?

        return dic
    }
    
   
    
    func getRankingCommentObj(dic:Dictionary<String,AnyObject>)->JobCancellation
    {
        let jobCancellation :JobCancellation = JobCancellation()
        
        if dic["iEmployerJobId"] != nil {
         jobCancellation.iEmployerJobId =  Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobId"]!)
        }
        if dic["iJobId"] != nil {
            jobCancellation.iJobId =  Global.sharedInstance.parseJsonToInt(intToParse: dic["iJobId"]!)
        }
        if dic["iCancellationReasonType"] != nil {
            jobCancellation.iCancellationReasonType =  Global.sharedInstance.parseJsonToInt(intToParse: dic["iCancellationReasonType"]!)
        }
        
        if dic["bCancellationCostApproved"] != nil {
            jobCancellation.bCancellationCostApproved = dic["bCancellationCostApproved"] as! Bool
        }
        if dic["bOutOfSystemEmplymentPaymentApproved"] != nil {
            jobCancellation.bOutOfSystemEmplymentPaymentApproved = dic["bOutOfSystemEmplymentPaymentApproved"] as! Bool
        }
        if dic["iEmployerJobWorkerType"] != nil {
            jobCancellation.iEmployerJobWorkerType =  Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobWorkerType"]!)
        }
        return jobCancellation
    }
    
}


