//
//  WorkerLocation.swift
//  FineWork
//
//  Created by Lior Ronen on 27/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerLocation: NSObject {

    var lWorkerAskingLocations : Array<WorkerAskingLocation> = [] //רשימת מיקומים לעובד
    var lEmployerLocations : Array<EmployerLocation> = [] //רשימת מיקומים של מעסיקים להצגה על המפה

    func getDictionaryFromWorkerLocation() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["lWorkerAskingLocations"] = getlWorkerAskingLocationsAsDic() as AnyObject
        
        dic["lEmployerLocations"] = getlEmployerLocationsAsDic() as AnyObject
        return dic
    }
    
    func getlWorkerAskingLocationsAsDic() -> Array<AnyObject> {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for workerAskingLocation in lWorkerAskingLocations
        {
            dic = workerAskingLocation.getWorkerAskingLocationAsDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
 
    func getlEmployerLocationsAsDic() -> Array<AnyObject> {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for employerLocation in lEmployerLocations
        {
            dic = employerLocation.getEmployerLocationAsDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    

    func getWorkerLocationFromDic(dic : Dictionary<String, AnyObject>) -> WorkerLocation {
        let workerLocation = WorkerLocation()
        
        if dic["lWorkerAskingLocations"] != nil {
            workerLocation.lWorkerAskingLocations = getlWorkerAskingLocationsFromDic(dic: dic["lWorkerAskingLocations"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        if dic["lEmployerLocations"] != nil {
            workerLocation.lEmployerLocations = getlEmployerLocationsFromDic(dic: dic["lEmployerLocations"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        return workerLocation
    }
    
    func getlWorkerAskingLocationsFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerAskingLocation> {
        var lWorkerAskingLocations : Array<WorkerAskingLocation> = Array<WorkerAskingLocation>()
        
        var workerAskingLocation : WorkerAskingLocation = WorkerAskingLocation()
        
        for location in dic {
            workerAskingLocation = workerAskingLocation.getWorkerAskingLocationFromDic(dic: location)
            lWorkerAskingLocations.append(workerAskingLocation)
        }
        
        return lWorkerAskingLocations
    }
    
    func getlEmployerLocationsFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<EmployerLocation> {
        var lEmployerLocations : Array<EmployerLocation> = Array<EmployerLocation>()
        
        var employerLocation : EmployerLocation = EmployerLocation()
        
        for location in dic {
            employerLocation = employerLocation.getWorkerLanguageFromDic(dic: location)
            lEmployerLocations.append(employerLocation)
        }
        
        return lEmployerLocations
    }
}
