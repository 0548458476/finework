//
//  WorkerRoleSysTables.swift
//  FineWork
//
//  Created by Lior Ronen on 22/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerRoleSysTables: NSObject {

    var lEmploymentDomainType : Array<SysTable> = []
    var lEmploymentDomainRole : Array<EmploymentDomainRole> = []
    
    func getWorkerRoleSysTablesFromDictionary(dic : Dictionary<String, AnyObject>) -> WorkerRoleSysTables {
        let workerRoleSysTables = WorkerRoleSysTables()
        
        if dic["lEmploymentDomainType"] != nil {
            workerRoleSysTables.lEmploymentDomainType = getlEmploymentDomainTypeFromDic(dic: dic["lEmploymentDomainType"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        if dic["lEmploymentDomainRole"] != nil {
            workerRoleSysTables.lEmploymentDomainRole = getlEmploymentDomainRoleFromDic(dic: dic["lEmploymentDomainRole"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        return workerRoleSysTables
    }
    
    private func getlEmploymentDomainTypeFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<SysTable> {
        var lEmploymentDomainType : Array<SysTable> = Array<SysTable>()
        
        var employmentDomainType : SysTable = SysTable()
        
        for type in dic {
            employmentDomainType = employmentDomainType.getSysTableFromDic(dic: type)
            lEmploymentDomainType.append(employmentDomainType)
        }
        
        return lEmploymentDomainType
    }
    
    private func getlEmploymentDomainRoleFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<EmploymentDomainRole> {
        var lEmploymentDomainRole : Array<EmploymentDomainRole> = Array<EmploymentDomainRole>()
        
        var employmentDomainRole : EmploymentDomainRole = EmploymentDomainRole()
        
        for role in dic {
            employmentDomainRole = employmentDomainRole.getEmploymentDomainRoleFromDictionary(dic: role)
            lEmploymentDomainRole.append(employmentDomainRole)
        }
        
        return lEmploymentDomainRole
    }
}
