//
//  BankAccountGuide.swift
//  FineWork
//
//  Created by User on 4.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class BankAccountGuide: NSObject {
    //DataMember
    var iBankId = 0 //מזהה בנק
    var lImages:Array<String> = []//מדריך להוראת קבע
    var nvLink:String = ""//לינק לאתר
    func getBankAccountGuideFromDic(dic : Dictionary<String, AnyObject>) -> BankAccountGuide {
        let bankAccountGuide = BankAccountGuide()
        
        if dic["iBankId"] != nil {
            bankAccountGuide.iBankId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iBankId"]!)
        }
        
        if dic["nvLink"] != nil {
            bankAccountGuide.nvLink = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLink"]!)
        }
        if dic["lImages"] != nil {
    bankAccountGuide.lImages =  dic["lImages"]! as! Array<String>
        }
        
        return bankAccountGuide
    }
}
