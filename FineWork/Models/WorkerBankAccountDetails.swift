//
//  WorkerBankAccountDetails.swift
//  FineWork
//
//  Created by Lior Ronen on 06/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerBankAccountDetails: NSObject {

    //Members
    var iBankId : Int = 0 //מס’ בנק
    var iBranchNumber : Int? = nil //מס’ סניף
    var iAccountNumber : Int? = nil //מס’ חשבון
    var nvAccountName : String = "" //שם בעל החשבון
    var bBankAccountDetailsUpdated : Bool = false //עודכנו פרטי חשבון הבנק?
    
    override init() {}
    
    func getWorkerBankAccountDetailsAsDic() -> Dictionary<String, AnyObject> {
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        
        dic["iBankId"] = iBankId as AnyObject
        dic["iBranchNumber"] = iBranchNumber as AnyObject
        dic["iAccountNumber"] = iAccountNumber as AnyObject
        dic["nvAccountName"] = nvAccountName as AnyObject
        dic["bBankAccountDetailsUpdated"] = bBankAccountDetailsUpdated as AnyObject
        
        return dic
    }
    
    func getWorkerBankAccountDetailsFromDic(dic : Dictionary<String, AnyObject>) -> WorkerBankAccountDetails {
        let workerBankAccount : WorkerBankAccountDetails = WorkerBankAccountDetails()
        
        if dic["iBankId"] != nil {
            workerBankAccount.iBankId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iBankId"]!)
        }
        if dic["iBranchNumber"] != nil {
            workerBankAccount.iBranchNumber = Global.sharedInstance.parseJsonToIntOptionNil(intToParse: dic["iBranchNumber"]!)
        }
        if dic["iAccountNumber"] != nil {
            workerBankAccount.iAccountNumber = Global.sharedInstance.parseJsonToIntOptionNil(intToParse: dic["iAccountNumber"]!)
        }
        if dic["nvAccountName"] != nil {
            workerBankAccount.nvAccountName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAccountName"]!)
        }
        if dic["bBankAccountDetailsUpdated"] != nil {
            workerBankAccount.bBankAccountDetailsUpdated = dic["bBankAccountDetailsUpdated"] as! Bool
        }
        
        return workerBankAccount
    }

}
