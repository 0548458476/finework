//
//  CandidateFound.swift
//  FineWork
//
//  Created by User on 18/09/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class CandidateFound: NSObject {
    var iEmployerJobId : Int = 0//מזהה משרה
   var  iWorkerOfferedJobId : Int = 0//מזהה משרה מוצעת
    var  iWorkerUserId : Int = 0//מזהה עובד
    var  nvEmployerAskedJobStatusType : String = "" //סטטוס משרה מוצעת
   var  nvWorkerName : String = "" //שם המועמד
   var  nvRole : String = "" //תפקיד
   var  Domain : String = "" //תחום
   var  nvImageFilePath : String = "" //תמונת מועמד
   var  iMatchPercentage : Int = 0//% התאמה
   var  iRank : Int = 0//רמת מועמדות
   var  nvVideoLink : String = "" //וידאו
    //דירוג (%)
   var  nvJobType : String = "" //משרה מלאה/חלקית
   var  nPercentAvailability : Double = 0//אחוזי זמינות המשרה החלקית
   var  mAskingHourlyWage : Double? = 0//X ₪ לשעה
   var  mRecommandedHourlyWage : Double? = 0//עלות לשעה ברוטו
   var  nYearsOfExperience : Double? = 0//שנות ניסיון
   var  nvMobile : String = "" //טלפון נייד
   var  nvAddress : String = "" //כתובת
   var  fAge : Double? = 0//גיל
   var  nvFamilyStatusType : String = "" //מצב משפחתי
   var  nvMilitaryService : String = "" //שרות צבאי/לאומי
   var  nvLanguages : String = "" //שפות
   var  nvResumeCharacteristics : String = "" //תכונות
   var  nvResumeStudies : String = "" //לימודים
   var  nvResumeWorkExperience : String = "" //ניסיון
   var  nvResumeSuitableWork : String = "" //עבודות שאני יכול להתאים להם
   var  nvResumeFilePath : String = "" //קובץ קו”ח
    var  ranking : Ranking?  //מה מעסיקים אומרים על המועמד
    var iWaitingForReplyTime : Int = 0 //זמן שממתין המועמד לתשובה
    var  lWorkerOfferedJobTimeAvailablity : Array<WorkerOfferedJobTimeAvailablity> = [] //זמינות
    var iEmployerJobScheduleType : Int = 0
    
    
    
    func getCandidateFoundDic() -> Dictionary<String, AnyObject> {
        
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        
        dic["iEmployerJobId"] = iEmployerJobId as AnyObject
        dic["iWorkerOfferedJobId"] = iWorkerOfferedJobId as AnyObject
        dic["iWorkerUserId"] = iWorkerUserId as AnyObject
        dic["nvEmployerAskedJobStatusType"] = nvEmployerAskedJobStatusType as AnyObject
        dic["nvWorkerName"] = nvWorkerName as AnyObject
        dic["nvRole"] = nvRole as AnyObject
        dic["Domain"] = Domain as AnyObject
        dic["nvImageFilePath"] = nvImageFilePath as AnyObject
        dic["iMatchPercentage"] = iMatchPercentage as AnyObject
        dic["iRank"] = iRank as AnyObject
        dic["nvVideoLink"] = nvVideoLink as AnyObject
        dic["nvJobType"] = nvJobType as AnyObject
        dic["nPercentAvailability"] = nPercentAvailability as AnyObject
        dic["nvWorkerName"] = nvWorkerName as AnyObject
        if mAskingHourlyWage != nil {
            dic["mAskingHourlyWage"] = mAskingHourlyWage as AnyObject
        }
        if mRecommandedHourlyWage != nil {
            dic["mRecommandedHourlyWage"] = mRecommandedHourlyWage as AnyObject
        }
        if nYearsOfExperience != nil {
            dic["nYearsOfExperience"] = nYearsOfExperience as AnyObject
        }
        dic["nvMobile"] = nvMobile as AnyObject
        dic["nvAddress"] = nvAddress as AnyObject
        if fAge != nil {
            dic["fAge"] = fAge as AnyObject
        }
        dic["nvFamilyStatusType"] = nvFamilyStatusType as AnyObject
        dic["nvMilitaryService"] = nvMilitaryService as AnyObject
        dic["nvLanguages"] = nvLanguages as AnyObject
        dic["nvResumeCharacteristics"] = nvResumeCharacteristics as AnyObject
        dic["nvResumeStudies"] = nvResumeStudies as AnyObject
        dic["nvResumeWorkExperience"] = nvResumeWorkExperience as AnyObject
        dic["nvResumeSuitableWork"] = nvResumeSuitableWork as AnyObject
        dic["nvResumeFilePath"] = nvResumeFilePath as AnyObject
        dic["ranking"] = self.ranking?.getRankingDic() as AnyObject?
        dic["iWaitingForReplyTime"] = iWaitingForReplyTime as AnyObject
        dic["lWorkerOfferedJobTimeAvailablity"] = getlWorkerOfferedJobTimeAvailablityDic() as AnyObject
        dic["iEmployerJobScheduleType"] = iEmployerJobScheduleType as AnyObject


        
        return dic
    }
    
    func getlWorkerOfferedJobTimeAvailablityDic()->Array<AnyObject>
    {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for workerOffer in lWorkerOfferedJobTimeAvailablity
        {
            dic = workerOffer.getDicFromWorkerOfferedJobTimeAvailablity()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getCandidateFoundFromDic(dic : Dictionary<String, AnyObject>) -> CandidateFound {
        let candidateFound = CandidateFound()
        
        if dic["iEmployerJobId"] != nil {
            candidateFound.iEmployerJobId = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobId"]!))
        }
        if dic["iWorkerOfferedJobId"] != nil {
            candidateFound.iWorkerOfferedJobId = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerOfferedJobId"]!))
        }
        if dic["iWorkerUserId"] != nil {
            candidateFound.iWorkerUserId = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!))
        }
        if dic["nvEmployerAskedJobStatusType"] != nil {
            candidateFound.nvEmployerAskedJobStatusType = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerAskedJobStatusType"]!))
        }
        if dic["nvWorkerName"] != nil {
            candidateFound.nvWorkerName = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkerName"]!))
        }
        if dic["nvRole"] != nil {
            candidateFound.nvRole = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRole"]!))
        }
        if dic["Domain"] != nil {
            candidateFound.Domain = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["Domain"]!))
        }
        if dic["nvImageFilePath"] != nil {
            candidateFound.nvImageFilePath = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImageFilePath"]!))
        }
        if dic["iMatchPercentage"] != nil {
            candidateFound.iMatchPercentage = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iMatchPercentage"]!))
        }
        if dic["iRank"] != nil {
            candidateFound.iRank = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iRank"]!))
        }
        if dic["nvVideoLink"] != nil {
            candidateFound.nvVideoLink = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvVideoLink"]!))
        }
        if dic["nvJobType"] != nil {
            candidateFound.nvJobType = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvJobType"]!))
        }
        if dic["nPercentAvailability"] != nil {
            candidateFound.nPercentAvailability = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["nPercentAvailability"]!))
        }
        if dic["mAskingHourlyWage"] != nil {
            candidateFound.mAskingHourlyWage = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["mAskingHourlyWage"]!))
        }
        if dic["mRecommandedHourlyWage"] != nil {
            candidateFound.mRecommandedHourlyWage = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["mRecommandedHourlyWage"]!))
        }
        if dic["nYearsOfExperience"] != nil {
            candidateFound.nYearsOfExperience = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["nYearsOfExperience"]!))
        }
        if dic["nvMobile"] != nil {
            candidateFound.nvMobile = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMobile"]!))
        }
        if dic["nvAddress"] != nil {
            candidateFound.nvAddress = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!))
        }
        if dic["fAge"] != nil {
            candidateFound.fAge = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["fAge"]!))
        }
        if dic["nvFamilyStatusType"] != nil {
            candidateFound.nvFamilyStatusType = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvFamilyStatusType"]!))
        }
        if dic["nvMilitaryService"] != nil {
            candidateFound.nvMilitaryService = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMilitaryService"]!))
        }
        if dic["nvLanguages"] != nil {
            candidateFound.nvLanguages = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLanguages"]!))
        }
        if dic["nvResumeCharacteristics"] != nil {
            candidateFound.nvResumeCharacteristics = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeCharacteristics"]!))
        }
        if dic["nvResumeStudies"] != nil {
            candidateFound.nvResumeStudies = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeStudies"]!))
        }
        if dic["nvResumeWorkExperience"] != nil {
            candidateFound.nvResumeWorkExperience = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeWorkExperience"]!))
        }
        if dic["nvResumeSuitableWork"] != nil {
            candidateFound.nvResumeSuitableWork = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeSuitableWork"]!))
        }
        if dic["nvResumeFilePath"] != nil {
            candidateFound.nvResumeFilePath = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeFilePath"]!))
        }
        if dic["ranking"] != nil {
            let ranking : Ranking = Ranking()
          //  candidateFound.ranking = ranking.getRankingFromDic(dic: dic["ranking"] as! Dictionary<String, AnyObject>)
    }
        if dic["iWaitingForReplyTime"] != nil {
            candidateFound.iWaitingForReplyTime = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWaitingForReplyTime"]!)
        }
        if dic["lWorkerOfferedJobTimeAvailablity"] != nil {
            if let p = dic["lWorkerOfferedJobTimeAvailablity"] as? Array<Dictionary<String, AnyObject>>{
                 candidateFound.lWorkerOfferedJobTimeAvailablity = getTimeAvailablityFromDic(dic: p)
           }
//            candidateFound.lWorkerOfferedJobTimeAvailablity = getTimeAvailablityFromDic(dic: dic["lWorkerOfferedJobTimeAvailablity"] as! Array<Dictionary<String, AnyObject>>)
        }
        if dic["iEmployerJobScheduleType"] != nil {
            candidateFound.iEmployerJobScheduleType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobScheduleType"]!)
        }
        
        return candidateFound

    }
    func getTimeAvailablityFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerOfferedJobTimeAvailablity> {
        var timeAvailablity : Array<WorkerOfferedJobTimeAvailablity> = Array<WorkerOfferedJobTimeAvailablity>()
        
        var workerOfferedJobTime : WorkerOfferedJobTimeAvailablity = WorkerOfferedJobTimeAvailablity()
        
        for language in dic {
            workerOfferedJobTime = workerOfferedJobTime.getSWorkerOfferedJobTimeAvailablityFromDic(dic: language)
            timeAvailablity.append(workerOfferedJobTime)
        }
        
        return timeAvailablity
    }
}
