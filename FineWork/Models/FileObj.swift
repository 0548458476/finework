//
//  FileObj.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class FileObj: NSObject {
    
    //MARK: - Variables

    var nvFile:String//הקובץ בעצמו-בייס64
    var nvFileName:String//שם הקובץ
    var nvFileURL:String
    var iFileId:Int//(שולחים 1-)שולחים לשרת רק אם משנים קובץ שהגיע מהשרת
    
    //MARK: - Initial
    
    override init() {
        nvFile = ""
        nvFileName = ""
        nvFileURL = ""
        iFileId = 0
    }
    
    init(_nvFile:String,_nvFileName:String,_nvFileURL:String,_iFileId:Int) {
        nvFile = _nvFile
        nvFileName = _nvFileName
        nvFileURL = _nvFileURL
        iFileId = _iFileId
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["nvFile"] = nvFile as AnyObject?
        dic["nvFileName"] = nvFileName as AnyObject?
        dic["nvFileURL"] = nvFileURL as AnyObject?
        dic["iFileId"] = iFileId as AnyObject?
        
        return dic
    }
    
    func getImageDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["nvFile"] = nvFile as AnyObject?
        dic["nvFileName"] = nvFileName as AnyObject?
        dic["nvFileURL"] = "" as AnyObject?
        dic["iFileId"] = 0 as AnyObject?
        
        return dic
    }
    
    func dicToFileObj(dic:Dictionary<String,AnyObject>)->FileObj
    {
        let fileObj:FileObj = FileObj()
        fileObj.nvFile = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvFile"]!)
        fileObj.nvFileName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvFileName"]!)
        fileObj.nvFileURL
            = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvFileURL"]!)
        fileObj.iFileId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iFileId"]!)
        
        return fileObj
    }
    
}
