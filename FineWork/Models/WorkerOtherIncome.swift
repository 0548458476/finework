//
//  WorkerOtherIncome.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerOtherIncome: NSObject {
    
    //MARK: - Variables

    var iIncomeType:Int //סוג הכנסה
    var nvOtherIncomeSource:String //”פרוט לסוג הכנסה “אחר
    
    //MARK: - Initial
    
    override init() {
        iIncomeType = 0
        nvOtherIncomeSource = ""
    }
    
    init(_iIncomeType:Int,_nvOtherIncomeSource:String) {
        iIncomeType = _iIncomeType
        nvOtherIncomeSource = _nvOtherIncomeSource
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iIncomeType"] = iIncomeType as AnyObject?
        dic["nvOtherIncomeSource"] = nvOtherIncomeSource as AnyObject?
        
        return dic
    }
    
    func dicToWorkerOtherIncome(dic:Dictionary<String,AnyObject>)->WorkerOtherIncome
    {
        let workerOtherIncome:WorkerOtherIncome = WorkerOtherIncome()
        if dic["iIncomeType"] != nil
        {
            workerOtherIncome.iIncomeType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iIncomeType"]!)
        }
        if dic["nvOtherIncomeSource"] != nil
        {
            workerOtherIncome.nvOtherIncomeSource = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvOtherIncomeSource"]!)
        }
        
        return workerOtherIncome
    }
    
    func workerOtherIncomeToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<WorkerOtherIncome>{
        
        var arrWorkerOtherIncome:Array<WorkerOtherIncome> = Array<WorkerOtherIncome>()
        var workerOtherIncome:WorkerOtherIncome = WorkerOtherIncome()
        
        for i in 0  ..< arrDic.count
        {
            workerOtherIncome = dicToWorkerOtherIncome(dic: arrDic[i] )
            arrWorkerOtherIncome.append(workerOtherIncome)
        }
        return arrWorkerOtherIncome
    }
}
