//
//  LookingForYouWorkers.swift
//  FineWork
//
//  Created by User on 18/09/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class LookingForYouWorkers: NSObject {
    
    var iEmployerJobId : Int = 0//מזהה משרה
    var iWorkerOfferedJobId : Int = 0//מזהה משרה מוצעת של מועמד ראשון
    var  iJobId : Int = 0//מזהה משרה לעובד
    var  nvRole : String = ""//תפקיד
    var  nvDomain : String = ""
    var  nvImageFilePath : String = ""//תמונת מועמד ראשון
    var  dtJobStartDate : String? = nil//תאריך תחילת המשרה
    var tFromHoure : String = "" //זמן תחילת המשרה במשמרת הראשונה
    var tToHoure : String = "" //זמן סיום המשרה במשמרת הראשונה
    var dtWorkerCandidacyExpiration : String? = nil//תאריך פג תוקף למעסיק לבחירת מועמד
    var dtEmployerJobProcessExpiration :String? = nil //תאריך פג תוקף שהמשרה תוצג למשתמשים שלא השלימו טופס 101
    var  iEmployerAskedJobStatusType : Int = 0//סטטוס משרה
    var  nvEmployerAskedJobStatusType : String = ""//שם סטטוס משרה
    var  dtWorkerApprovalExpiration : String? = nil//זמן תגובה שנותר למועמד הראשון
    var  iWorkerOfferJobCount : Int = 0//מספר מועמדים למשרה
    var   iEmployerJobScheduleType : Int = 0//סוג זמן המשרה
    var  nvWorkerName : String = ""//שם עובד (לזיהוי)
    var  nvWorkerIdentityNumber : String = ""//ת.ז. עובד (לזיהוי)
    var  iEmployerJobWorkerType : Int = 0//סוג עובד
    var  mRecommandedHourlyWage : Double = 0//עלות לשעה ברוטו
    var  iWorkerUserId : Int = 0//מזהה עובד
    var  bTimeReportingFlexibility :Bool? = nil //קשיחות/גמישות
    var  candidateFound : CandidateFound? = nil //פרוט המועמד
    var  iBaloonOccur : Int = 0//בועת התרחשות למעסיק
    var bShowContinueProcess : Bool = false//שייך למשרה כמותית - האם להציג את הכפתור פנה למועמד המתאים ביותר
    var bDeleted: Bool = false //שייך למשרה כמותית - האם להציג את הכפתור מחיקה
    var bEmployerQaunitativeJob : Bool = false //האם המשרה היא כמותית

    
    // logo image
    var logoImage : UIImage?
    
    
    func getLookingForYouWorkersDic() -> Dictionary<String, AnyObject> {
        
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        dic["iEmployerJobId"] = iEmployerJobId as AnyObject
        dic["iWorkerOfferedJobId"] = iWorkerOfferedJobId as AnyObject
        dic["iJobId"] = iJobId as AnyObject
        dic["nvRole"] = nvRole as AnyObject
        dic["nvDomain"] = nvDomain as AnyObject
        dic["nvImageFilePath"] = nvImageFilePath as AnyObject
        if dtJobStartDate != nil {
            dic["dtJobStartDate"] = dtJobStartDate as AnyObject
        }
        dic["tFromHoure"] = tFromHoure as AnyObject
        dic["tToHoure"] = tToHoure as AnyObject
        if dtEmployerJobProcessExpiration != nil {
            dic["dtEmployerJobProcessExpiration"] = dtEmployerJobProcessExpiration as AnyObject
        }
        if dtWorkerCandidacyExpiration != nil {
            dic["dtWorkerCandidacyExpiration"] = dtWorkerCandidacyExpiration as AnyObject
        }
        
        dic["iEmployerAskedJobStatusType"] = iEmployerAskedJobStatusType as AnyObject
        dic["nvEmployerAskedJobStatusType"] = nvEmployerAskedJobStatusType as AnyObject
        if dtWorkerApprovalExpiration != nil {
            dic["dtWorkerApprovalExpiration"] = dtWorkerApprovalExpiration as AnyObject
        }
        dic["iWorkerOfferJobCount"] = iWorkerOfferJobCount as AnyObject
        dic["iEmployerJobScheduleType"] = iEmployerJobScheduleType as AnyObject
        dic["nvWorkerName"] = nvWorkerName as AnyObject
        dic["nvWorkerIdentityNumber"] = nvWorkerIdentityNumber as AnyObject
        dic["iEmployerJobWorkerType"] = iEmployerJobWorkerType as AnyObject
        dic["mRecommandedHourlyWage"] = mRecommandedHourlyWage as AnyObject
        dic["iWorkerUserId"] = iWorkerUserId as AnyObject
        if bTimeReportingFlexibility != nil {
            dic["bTimeReportingFlexibility"] = bTimeReportingFlexibility as AnyObject
        }
        dic["candidateFound"] = self.candidateFound?.getCandidateFoundDic() as AnyObject
        dic["iBaloonOccur"] = iBaloonOccur as AnyObject
        dic["bShowContinueProcess"] = bShowContinueProcess as AnyObject
        dic["bDeleted"] = bDeleted as AnyObject
        dic["bEmployerQaunitativeJob"] = bEmployerQaunitativeJob as AnyObject


       
        return dic
    }
    func getLookingForYouWorkersFromDic(dic : Dictionary<String, AnyObject>) -> LookingForYouWorkers {
        let lookingForYouWorkers = LookingForYouWorkers()
        
        if dic["iEmployerJobId"] != nil {
            lookingForYouWorkers.iEmployerJobId = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobId"]!))
        }
        if dic["iWorkerOfferedJobId"] != nil {
            lookingForYouWorkers.iWorkerOfferedJobId = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerOfferedJobId"]!))
        }
        if dic["iJobId"] != nil {
            lookingForYouWorkers.iJobId = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iJobId"]!))
        }
        if dic["nvRole"] != nil {
            lookingForYouWorkers.nvRole = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRole"]!))
        }
        if dic["nvDomain"] != nil {
            lookingForYouWorkers.nvDomain = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvDomain"]!))
        }
        
        if dic["nvImageFilePath"] != nil {
            lookingForYouWorkers.nvImageFilePath = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImageFilePath"]!))
        }
        if dic["dtJobStartDate"] != nil {
            lookingForYouWorkers.dtJobStartDate = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dtJobStartDate"]!)
        }
        if dic["tFromHoure"] != nil {
       lookingForYouWorkers.tFromHoure =  Global.sharedInstance.parseJsonToString(stringToParse: dic["tFromHoure"]!)
        }
        if dic["tToHoure"] != nil {
            lookingForYouWorkers.tToHoure =  Global.sharedInstance.parseJsonToString(stringToParse: dic["tToHoure"]!)
        }
        
        
        if dic["dtEmployerJobProcessExpiration"] != nil {
            lookingForYouWorkers.dtEmployerJobProcessExpiration = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dtEmployerJobProcessExpiration"]!)
        }
        if dic["dtWorkerCandidacyExpiration"] != nil {
            lookingForYouWorkers.dtWorkerCandidacyExpiration = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dtWorkerCandidacyExpiration"]!)
        }
        
        if dic["iEmployerAskedJobStatusType"] != nil {
            lookingForYouWorkers.iEmployerAskedJobStatusType = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerAskedJobStatusType"]!))
        }
        if dic["nvEmployerAskedJobStatusType"] != nil {
            lookingForYouWorkers.nvEmployerAskedJobStatusType = String(Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerAskedJobStatusType"]!))
        }
        if dic["dtWorkerApprovalExpiration"] != nil {
            lookingForYouWorkers.dtWorkerApprovalExpiration = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dtWorkerApprovalExpiration"]!)
        }
        if dic["iWorkerOfferJobCount"] != nil {
            lookingForYouWorkers.iWorkerOfferJobCount = Int(Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerOfferJobCount"]!))
        }
        if dic["iEmployerJobScheduleType"] != nil {
            lookingForYouWorkers.iEmployerJobScheduleType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobScheduleType"]!)
        }
        if dic["nvWorkerName"] != nil {
            lookingForYouWorkers.nvWorkerName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkerName"]!)
        }
        if dic["nvWorkerIdentityNumber"] != nil {
            lookingForYouWorkers.nvWorkerIdentityNumber = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkerIdentityNumber"]!)
        }
        if dic["iEmployerJobWorkerType"] != nil {
            lookingForYouWorkers.iEmployerJobWorkerType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobWorkerType"]!)
        }
        
        if dic["mRecommandedHourlyWage"] != nil {
            lookingForYouWorkers.mRecommandedHourlyWage = Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["mRecommandedHourlyWage"]!)
        }
        if dic["iWorkerUserId"] != nil {
            lookingForYouWorkers.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!)
        }
        if dic["bTimeReportingFlexibility"] != nil {
            lookingForYouWorkers.bTimeReportingFlexibility = dic["bTimeReportingFlexibility"] as? Bool
        }
        
        if dic["candidateFound"] != nil {
            if ((dic["candidateFound"] as? Dictionary<String, AnyObject>) != nil){
                let candidateFound = CandidateFound()

            lookingForYouWorkers.candidateFound = candidateFound.getCandidateFoundFromDic(dic: (dic["candidateFound"] as! Dictionary<String, AnyObject>))
            }}
        if dic["iBaloonOccur"] != nil {
            lookingForYouWorkers.iBaloonOccur = Global.sharedInstance.parseJsonToInt(intToParse: dic["iBaloonOccur"]!)
        }
        if dic["bShowContinueProcess"] != nil {
            lookingForYouWorkers.bShowContinueProcess = dic["bShowContinueProcess"] as! Bool
        }
        if dic["bDeleted"] != nil {
            lookingForYouWorkers.bDeleted = dic["bDeleted"] as! Bool
        }
        if dic["bEmployerQaunitativeJob"] != nil {
            lookingForYouWorkers.bEmployerQaunitativeJob = dic["bEmployerQaunitativeJob"] as! Bool
        }
    

        return lookingForYouWorkers
        
    }

 
}
