//
//  WorkerByEmploymentDomainRole.swift
//  FineWork
//
//  Created by User on 23/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerByEmploymentDomainRole: NSObject {

    var iWorkersCount : Int = 0 //מס’ עובדים
    var iWorkersByEmploymentDomainRoleCount : Int = 0 //מס’ עובדים שבחרו בתחום ותפקיד מסוים

    func getWorkerByEmploymentDomainRoleFromDic(dic : Dictionary<String, AnyObject>) -> WorkerByEmploymentDomainRole {
        let workerByEmploymentDomainRole = WorkerByEmploymentDomainRole()
        
        if dic["iWorkersCount"] != nil {
            workerByEmploymentDomainRole.iWorkersCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkersCount"]!)
        }
        
        if dic["iWorkersByEmploymentDomainRoleCount"] != nil {
            workerByEmploymentDomainRole.iWorkersByEmploymentDomainRoleCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkersByEmploymentDomainRoleCount"]!)
        }
        
        return workerByEmploymentDomainRole
    }
}
