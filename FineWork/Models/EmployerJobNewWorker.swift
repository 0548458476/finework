//
//  EmployerJobNewWorker.swift
//  FineWork
//
//  Created by User on 23/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmployerJobNewWorker: NSObject {

    
    var nvExperienceDetails : String = "" //פרטי ניסיון
    var nvLanguageDetails : String = "" //פרטי שפה
    var bDiploma : Bool = false //דיפלומה?
    var nvTimeDescription : String = "" //פרטי ותק
    var nvProfessionalTimeDetails : String = "" //פרטי ותק מקצועי
    var bPhoneInterview : Bool = false //נדרש ראיון טלפוני?
    var bWorkedWorkerPreference : Bool = false //מעדיף עובדים שכבר עבדו אצלי בעבר?

    
    func getDicFromEmployerJobNewWorker() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["nvExperienceDetails"] = nvExperienceDetails as AnyObject
        dic["nvLanguageDetails"] = nvLanguageDetails as AnyObject
        dic["bDiploma"] = bDiploma as AnyObject
        dic["nvTimeDescription"] = nvTimeDescription as AnyObject
        dic["nvProfessionalTimeDetails"] = nvProfessionalTimeDetails as AnyObject
        dic["bPhoneInterview"] = bPhoneInterview as AnyObject
        dic["bWorkedWorkerPreference"] = bWorkedWorkerPreference as AnyObject
        
        return dic
    }
    
    func getEmployerJobNewWorkerFromDic(dic: Dictionary<String, AnyObject>) -> EmployerJobNewWorker {
        let employerJobNewWorker = EmployerJobNewWorker()
        
        if dic["nvExperienceDetails"] != nil {
            employerJobNewWorker.nvExperienceDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvExperienceDetails"]!)
        }
        if dic["nvLanguageDetails"] != nil {
            employerJobNewWorker.nvLanguageDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLanguageDetails"]!)
        }
        if dic["bDiploma"] != nil {
            employerJobNewWorker.bDiploma = dic["bDiploma"] as! Bool
        }
        if dic["nvTimeDescription"] != nil {
            employerJobNewWorker.nvTimeDescription = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvTimeDescription"]!)
        }
        if dic["nvProfessionalTimeDetails"] != nil {
            employerJobNewWorker.nvProfessionalTimeDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvProfessionalTimeDetails"]!)
        }
        if dic["bPhoneInterview"] != nil {
            employerJobNewWorker.bPhoneInterview = dic["bPhoneInterview"] as! Bool
        }
        if dic["bWorkedWorkerPreference"] != nil {
            employerJobNewWorker.bWorkedWorkerPreference = dic["bWorkedWorkerPreference"] as! Bool
        }
        
        return employerJobNewWorker
    }
}
