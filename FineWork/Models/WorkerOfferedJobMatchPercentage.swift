//
//  WorkerOfferedJobMatchPercentage.swift
//  FineWork
//
//  Created by User on 20/12/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerOfferedJobMatchPercentage: NSObject {
    var iMatchPercentageCount : Int = 0//מספר מועמדים שקדמו לו
    var bHourlyWageMatchPercentage : Bool = false//בסכום
    var bRankingAvgMatchPercentage : Bool = false//בדרוג מעסיקים
    var bTimeMatchPercentage : Bool = false//בזמינות
    
    
    func getDicFromWorkerOfferedJobMatchPercentage() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        dic["iMatchPercentageCount"] = self.iMatchPercentageCount as AnyObject?
        dic["bHourlyWageMatchPercentage"] = self.bHourlyWageMatchPercentage as AnyObject?
        dic["bRankingAvgMatchPercentage"] = self.bRankingAvgMatchPercentage as AnyObject?
        dic["bTimeMatchPercentage"] = self.bTimeMatchPercentage as AnyObject?
       
        return dic
        
    }
    
    func getWorkerOfferedJobMatchPercentageFromDic(dic : Dictionary<String, AnyObject>) -> WorkerOfferedJobMatchPercentage {
        let workerOfferedJobMatchPercentage = WorkerOfferedJobMatchPercentage()
        
        if dic["iMatchPercentageCount"] != nil {
            workerOfferedJobMatchPercentage.iMatchPercentageCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iMatchPercentageCount"]!)
        }
        if dic["bHourlyWageMatchPercentage"] != nil {
            workerOfferedJobMatchPercentage.bHourlyWageMatchPercentage = dic["bHourlyWageMatchPercentage"] as! Bool
        }
        if dic["bRankingAvgMatchPercentage"] != nil {
            workerOfferedJobMatchPercentage.bRankingAvgMatchPercentage = dic["bRankingAvgMatchPercentage"] as! Bool
        }
        if dic["bTimeMatchPercentage"] != nil {
            workerOfferedJobMatchPercentage.bTimeMatchPercentage = dic["bTimeMatchPercentage"] as! Bool
        }
        return workerOfferedJobMatchPercentage
    }
}
