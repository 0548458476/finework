//
//  WorkerHomePage.swift
//  FineWork
//
//  Created by User on 25.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerHomePage: NSObject {

    var iWorkerHomePageType : Int = 0 //סוג דף הבית
    var bWorkerOfferedJob : Bool = false //יש משרות מוצעות לעובד?
    var iTotalEarned : Int = 0 //ש”ח שהרווחת דרכנו
    var iEmployersFoundYouCount : Int = 0 //מעסיקים שמצאו אותך
    var iReportedHoursCount : Int = 0 //שעות שדיווחת במערכת
    var lWorkerHomePageJobs : Array<WorkerHomePageJob> = [] //משרות רלונטיות

    func getDicFromWorkerHomePage() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerHomePageType"] = iWorkerHomePageType as AnyObject
        dic["bWorkerOfferedJob"] = bWorkerOfferedJob as AnyObject
        dic["iTotalEarned"] = iTotalEarned as AnyObject
        dic["iEmployersFoundYouCount"] = iEmployersFoundYouCount as AnyObject
        dic["iReportedHoursCount"] = iReportedHoursCount as AnyObject
        dic["lWorkerHomePageJobs"] = lWorkerHomePageJobs as AnyObject
        
        return dic
    }
    
    func getWorkerHomePageFromDic(dic : Dictionary<String, AnyObject>) -> WorkerHomePage {
        let workerHomePage = WorkerHomePage()
        
        if dic["iWorkerHomePageType"] != nil {
            workerHomePage.iWorkerHomePageType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerHomePageType"]!)
        }
        
        if dic["bWorkerOfferedJob"] != nil {
            workerHomePage.bWorkerOfferedJob = dic["bWorkerOfferedJob"] as! Bool
        }
        
        if dic["iTotalEarned"] != nil {
            workerHomePage.iTotalEarned = Global.sharedInstance.parseJsonToInt(intToParse: dic["iTotalEarned"]!)
        }
        
        if dic["iEmployersFoundYouCount"] != nil {
            workerHomePage.iEmployersFoundYouCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployersFoundYouCount"]!)
        }
        
        if dic["iReportedHoursCount"] != nil {
            workerHomePage.iReportedHoursCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iReportedHoursCount"]!)
        }
        
        if dic["lWorkerHomePageJobs"] != nil {
            workerHomePage.lWorkerHomePageJobs = getlWorkerHomePageJobs(dic: dic["lWorkerHomePageJobs"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        return workerHomePage
    }
    
    func getlWorkerHomePageJobs(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerHomePageJob> {
        var workerHomePageJobsArr = Array<WorkerHomePageJob>()
        let workerHomePageJob = WorkerHomePageJob()
        
        for job in dic {
            workerHomePageJobsArr.insert(workerHomePageJob.getWorkerHomePageJobFromDic(dic: job), at: workerHomePageJobsArr.count)
        }
        
        return workerHomePageJobsArr
    }
}
