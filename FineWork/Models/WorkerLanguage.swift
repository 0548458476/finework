//
//  WorkerLanguage.swift
//  FineWork
//
//  Created by Lior Ronen on 06/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerLanguage: NSObject {

    //Members
    var iLanguageType : Int = 0 //שפה
    var iLanguageLevelType : Int = 0//רמה
    
    override init() {}
    
    func getWorkerLanguageAsDic() -> Dictionary<String, AnyObject> {
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        
        dic["iLanguageType"] = iLanguageType as AnyObject
        dic["iLanguageLevelType"] = iLanguageLevelType as AnyObject
        
        return dic
    }
    
    func getWorkerLanguageFromDic(dic : Dictionary<String, AnyObject>) -> WorkerLanguage {
        let workerLanguage : WorkerLanguage = WorkerLanguage()
        
        if dic["iLanguageType"] != nil {
            workerLanguage.iLanguageType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iLanguageType"]!)
        }
        if dic["iLanguageLevelType"] != nil {
            workerLanguage.iLanguageLevelType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iLanguageLevelType"]!)
        }
        
        return workerLanguage
    }

}
