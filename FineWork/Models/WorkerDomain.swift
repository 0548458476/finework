//
//  WorkerDomain.swift
//  FineWork
//
//  Created by Lior Ronen on 22/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerDomain: NSObject {

    var iEmploymentDomainType : Int = 0 //תחום, מתקבל מהשרת
    var lWorkerRoles : Array<WorkerRole> = [] //תפקידים
    var nYearsOfExperience : Float? = nil//שנות ניסיון
    var mHourlyWage : Double? = nil //שכר שעתי רצוי
    var bIsForFullTimeJob : Bool = false //משרה מלאה?
    var bIsForPartTimeJob : Bool = false //משרה חלקית?

    override init() {}
    
    func getDictionaryFromWorkerDomain() -> Dictionary<String, AnyObject> {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iEmploymentDomainType"] = iEmploymentDomainType as AnyObject
        dic["lWorkerRoles"] = getlWorkerRolesAsDic() as AnyObject
        if nYearsOfExperience != nil {
            dic["nYearsOfExperience"] = nYearsOfExperience as AnyObject
        }
        
        if mHourlyWage != nil {
            dic["mHourlyWage"] = mHourlyWage as AnyObject
        }
        
        dic["bIsForFullTimeJob"] = bIsForFullTimeJob as AnyObject
        dic["bIsForPartTimeJob"] = bIsForPartTimeJob as AnyObject
        
        return dic
    }
    
    func getlWorkerRolesAsDic()->Array<AnyObject>
    {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for workerRole in lWorkerRoles
        {
            dic = workerRole.getDictionaryFromWorkerRole()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getWorkerDomainFromDictionary(dic : Dictionary<String, AnyObject>) -> WorkerDomain {
        let workerDomain = WorkerDomain()
        
        if dic["iEmploymentDomainType"] != nil {
            workerDomain.iEmploymentDomainType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentDomainType"]!)
        }
        // get lWorkerRoles - excepted
        if dic["lWorkerRoles"] != nil {
            workerDomain.lWorkerRoles = getlWorkerRolesFromDic(dic: dic["lWorkerRoles"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        if dic["nYearsOfExperience"] != nil {
            workerDomain.nYearsOfExperience = Global.sharedInstance.parseJsonToFloatOptionNil(floatToParse: dic["nYearsOfExperience"]!)
        }
        if dic["mHourlyWage"] != nil {
            workerDomain.mHourlyWage = Global.sharedInstance.parseJsonToDoubleOptionNil(doubleToParse: dic["mHourlyWage"]!)
        }
        
        if dic["bIsForFullTimeJob"] != nil {
            workerDomain.bIsForFullTimeJob = dic["bIsForFullTimeJob"] as! Bool
        }
        
        if dic["bIsForPartTimeJob"] != nil {
            workerDomain.bIsForPartTimeJob = dic["bIsForPartTimeJob"] as! Bool
        }
        
        return workerDomain
    }
    
    func getlWorkerRolesFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerRole> {
        var lWorkerRoles : Array<WorkerRole> = Array<WorkerRole>()
        
        var workerRole : WorkerRole = WorkerRole()
        
        for role in dic {
            workerRole = workerRole.getWorkerRoleFromDictionary(dic: role)
            lWorkerRoles.append(workerRole)
        }
        
        return lWorkerRoles
    }
}
