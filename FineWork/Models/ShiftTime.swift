//
//  ShiftTime.swift
//  FineWork
//
//  Created by User on 02/01/2018.
//  Copyright © 2018 webit. All rights reserved.
//

import UIKit

class ShiftTime: NSObject {

    var dFromDate :  String = ""
    var tFromHoure : String = ""
    var tToHoure : String = ""
    var  nSumHour : Int = 0
    
    
   
    
    func getDicFromShiftTime() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["dFromDate"] = dFromDate as AnyObject
        dic["tFromHoure"] = tFromHoure as AnyObject
        dic["tToHoure"] = tToHoure as AnyObject
        dic["nSumHour"] = nSumHour as AnyObject
        
        
        return dic
    }
    
    func getShiftTimeFromDic(dic: Dictionary<String, AnyObject>) -> ShiftTime {
        let shiftTime = ShiftTime()
        
       
        if dic["dFromDate"] != nil {
            shiftTime.dFromDate = Global.sharedInstance.parseJsonToString(stringToParse: dic["dFromDate"]!)
        }
        
        
        if dic["tFromHoure"] != nil {
            shiftTime.tFromHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tFromHoure"]!)
        }
        if dic["tToHoure"] != nil {
            shiftTime.tToHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tToHoure"]!)
        }
        
        if dic["nSumHour"] != nil {
            shiftTime.nSumHour = Global.sharedInstance.parseJsonToInt(intToParse: dic["nSumHour"]!)
        }
        return shiftTime
    }
    
}
