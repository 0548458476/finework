//
//  WorkerResumeDetails.swift
//  FineWork
//
//  Created by Lior Ronen on 06/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerResumeDetails: NSObject {

    //Members
    var nvResumeCharacteristics : String = "" //תכונות
    var nvResumeStudies : String = "" //לימודים
    var nvResumeWorkExperience : String = "" //נסיון תעסוקתי
    var nvResumeSuitableWork : String = "" //עבודות שאני יכול להתאים להן
    var resume :FileObj = FileObj() //קו”ח
    
    override init() {}
    
    func getWorkerResumeDetailsAsDic() -> Dictionary<String, AnyObject> {
        
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        
        dic["nvResumeCharacteristics"] = nvResumeCharacteristics as AnyObject
        dic["nvResumeStudies"] = nvResumeStudies as AnyObject
        dic["nvResumeWorkExperience"] = nvResumeWorkExperience as AnyObject
        dic["nvResumeSuitableWork"] = nvResumeSuitableWork as AnyObject
        dic["resume"] = resume.getDic() as AnyObject
        
        return dic
    }
    
    func getWorkerResumeDetailsFromDic(dic : Dictionary<String, AnyObject>) -> WorkerResumeDetails {
        let workerResumeDetails : WorkerResumeDetails = WorkerResumeDetails()
        
        if dic["nvResumeCharacteristics"] != nil {
            workerResumeDetails.nvResumeCharacteristics = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeCharacteristics"]!)
        }
        if dic["nvResumeStudies"] != nil {
            workerResumeDetails.nvResumeStudies = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeStudies"]!)
        }
        if dic["nvResumeWorkExperience"] != nil {
            workerResumeDetails.nvResumeWorkExperience = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeWorkExperience"]!)
        }
        if dic["nvResumeSuitableWork"] != nil {
            workerResumeDetails.nvResumeSuitableWork = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvResumeSuitableWork"]!)
        }
        if dic["resume"] != nil {
            let fileObj : FileObj = FileObj()
            workerResumeDetails.resume = fileObj.dicToFileObj(dic: dic["resume"] as! Dictionary<String, AnyObject>)
        }
        
        return workerResumeDetails
    }
}
