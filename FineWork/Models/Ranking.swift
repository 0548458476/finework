//
//  Ranking.swift
//  FineWork
//
//  Created by User on 14.6.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class Ranking: NSObject {
    var nRanking1Avg : Double = 0.0 //ממוצע הדירוג למדד 1
    var nRanking2Avg : Double = 0.0//ממוצע הדירוג למדד 2
    var   nRanking3Avg : Double = 0.0//ממוצע הדירוג למדד 3
    var nRanking4Avg : Double = 0.0//ממוצע הדירוג למדד 4
    var nRankingAvg : Double = 0.0//ממוצע הדירוג
    var lRankingComment : Array<RankingComment>?
    
    func getRankingDic() -> Dictionary<String, AnyObject> {
        
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        
        dic["nRanking1Avg"] = nRanking1Avg as AnyObject
        dic["nRanking2Avg"] = nRanking2Avg as AnyObject
        dic["nRanking3Avg"] = nRanking3Avg as AnyObject
        dic["nRanking4Avg"] = nRanking4Avg as AnyObject
        dic["nRankingAvg"] = nRankingAvg as AnyObject
        if lRankingComment != nil {
        dic["lRankingComment"] = lRankingComment as AnyObject
        }
        return dic
    }
    
    func getRankingFromDic(dic : Dictionary<String, AnyObject>) -> Ranking {
        let ranking = Ranking()
        
        if dic["nRanking1Avg"] != nil {
            ranking.nRanking1Avg = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["nRanking1Avg"]!))
        }
        if dic["nRanking2Avg"] != nil {
            ranking.nRanking2Avg = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["nRanking2Avg"]!))
        }
        if dic["nRanking3Avg"] != nil {
            ranking.nRanking3Avg = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["nRanking3Avg"]!))
        }
        if dic["nRanking4Avg"] != nil {
            ranking.nRanking4Avg = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["nRanking4Avg"]!))
        }
        if dic["nRankingAvg"] != nil {
            ranking.nRankingAvg = Double(Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["nRankingAvg"]!))
        }

        if dic["lRankingComment"] != nil {
            ranking.lRankingComment = getRankingListDic(dic: dic["lRankingComment"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        return ranking
    }
    
    func getRankingListDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<RankingComment> {
        var rankingComments = Array<RankingComment>()
        
        var item = RankingComment()
        
        for commtent in dic {
            item = item.getRankingCommentObj(dic: commtent)
            rankingComments.append(item)
        }
        
        return rankingComments
    }

}
