//
//  Worker101Form.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.

import UIKit

class Worker101Form: NSObject {
    
    //MARK: - Variables
    
    var iWorker101FormId:Int //מזהה טופס 101
    var iWorkerUserId:Int //מזהה עובד
    var nvFirstName:String //שם פרטי
    var nvLastName:String //שם משפחה
    var iFamilyStatusType:Int //מצב משפחתי
    var iGenderType:Int //מגדר -  זכר=5, נקבה=6
    var bResident:Bool //תושב ישראל?
    var nvIdentityNumber:String //מס’ זהות/דרכון
    var dBirthDate:NSDate? //תאריך לידה
    var dImmigrationDate:NSDate? //תאריך עליה
//    var nvCityName:String// עיר
    var iCityId:Int
    var nvStreet:String //רחוב
    var nvHouseNumber:String //מס’ בית
    var nvPhone:String //טלפון
    var iIncomeType:Int //סוג הכנסה
    var bHaveOtherIncome:Bool //יש הכנסה נוספת?
    var bAskTaxCredit:Bool //מבקש נקודות זיכוי למס?
    var bAskingTaxCoodination:Bool //מבקש תאום מס?
    var bAskingTaxCoodinationByAssessor:Bool //מבקש תאום מס ע”י פקיד שומה?
    var fCreditPoints:Float //נקודות זכות
    var workerSpouse:WorkerSpouse //פרטי בן זוג
    var lWorkerChilds:Array<WorkerChild> //פרטי ילדים
    var lWorkerOtherIncomes:Array<WorkerOtherIncome> = [] //פרטי הכנסות נוספות
    var lWorkerTaxRelifeReasons:Array<WorkerTaxRelifeReason> //פרטי תאומי מס - מתאתחל בבונה במספר הסיבות האפשריות סה״כ,הגישה למערך היא למיקום של הקוד
    var lWorkerTaxCoordinations:Array<WorkerTaxCoordination> //פרטי נקודות זיכוי למס - מעסיקים
    var workerTaxCoordinationFileDet:WorkerTaxCoordinationFileDet //פרוט תאום מס ע”י פקיד
    var photoID:FileObj// צילום ת”ז
    var confirmationOfChildAllowance:FileObj//אישור זכאות  לקבלת קצבת ילדים
    var confirmationOfSeparate:FileObj // אישור פקיד שומה לפרוד
    
    //MARK: - Initial
    
    override init() {
        iWorker101FormId = 0
        iWorkerUserId = 0
        nvFirstName = ""
        nvLastName = ""
        iFamilyStatusType = 0
        iGenderType = 5
        bResident = true
        nvIdentityNumber = ""
        //dBirthDate = NSDate()
        //dImmigrationDate = NSDate()
        iCityId = -1
        nvStreet = ""
        nvHouseNumber = ""
        nvPhone = ""
        iIncomeType = 12
        bHaveOtherIncome = Bool()
        bAskTaxCredit = Bool()
        bAskingTaxCoodination = Bool()
        bAskingTaxCoodinationByAssessor = Bool()
        fCreditPoints = 0.0
        workerSpouse = WorkerSpouse()
        lWorkerChilds = []
        //lWorkerOtherIncomes = [WorkerOtherIncome(),WorkerOtherIncome(),WorkerOtherIncome(),WorkerOtherIncome(),WorkerOtherIncome(),WorkerOtherIncome(),WorkerOtherIncome()]
        lWorkerOtherIncomes = []
        lWorkerTaxRelifeReasons = [WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason(),WorkerTaxRelifeReason()]
        lWorkerTaxCoordinations = []
        workerTaxCoordinationFileDet = WorkerTaxCoordinationFileDet()
        photoID = FileObj()
        confirmationOfChildAllowance = FileObj()
        confirmationOfSeparate = FileObj()
    }
    
    init(_iWorker101FormId:Int,_iWorkerUserId:Int,_nvFirstName:String,_nvLastName:String,_iFamilyStatusType:Int,_iGenderType:Int,_bResident:Bool,_nvIdentityNumber:String,_dBirthDate:NSDate,_dImmigrationDate:NSDate,_iCityId:Int,_nvStreet:String,_nvHouseNumber:String,_nvPhone:String,_iIncomeType:Int,_bHaveOtherIncome:Bool,_bAskTaxCredit:Bool,_bAskingTaxCoodination:Bool,_bAskingTaxCoodinationByAssessor:Bool,_fCreditPoints:Float,_workerSpouse:WorkerSpouse,_lWorkerChilds:Array<WorkerChild>,_lWorkerOtherIncomes:Array<WorkerOtherIncome>,_lWorkerTaxRelifeReasons:Array<WorkerTaxRelifeReason>,_lWorkerTaxCoordinations:Array<WorkerTaxCoordination>,_workerTaxCoordinationFileDet:WorkerTaxCoordinationFileDet,_photoID:FileObj,_confirmationOfChildAllowance:FileObj,_confirmationOfSeparate:FileObj) {
        
        iWorker101FormId = _iWorker101FormId
        iWorkerUserId = _iWorkerUserId
        nvFirstName = _nvFirstName
        nvLastName = _nvLastName
        iFamilyStatusType = _iFamilyStatusType
        iGenderType = _iGenderType
        bResident = _bResident
        nvIdentityNumber = _nvIdentityNumber
        dBirthDate = _dBirthDate
        dImmigrationDate = _dImmigrationDate
        iCityId = _iCityId
        nvStreet = _nvStreet
        nvHouseNumber = _nvHouseNumber
        nvPhone = _nvPhone
        iIncomeType = _iIncomeType
        bHaveOtherIncome = _bHaveOtherIncome
        bAskTaxCredit = _bAskTaxCredit
        bAskingTaxCoodination = _bAskingTaxCoodination
        bAskingTaxCoodinationByAssessor = _bAskingTaxCoodinationByAssessor
        fCreditPoints = _fCreditPoints
        workerSpouse = _workerSpouse
        lWorkerChilds = _lWorkerChilds
        lWorkerOtherIncomes = _lWorkerOtherIncomes
        lWorkerTaxRelifeReasons = _lWorkerTaxRelifeReasons
        lWorkerTaxCoordinations = _lWorkerTaxCoordinations
        workerTaxCoordinationFileDet = _workerTaxCoordinationFileDet
        photoID = _photoID
        confirmationOfChildAllowance = _confirmationOfChildAllowance
        confirmationOfSeparate = _confirmationOfSeparate
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        var dicBig:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dicBig["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["iWorker101FormId"] = iWorker101FormId as AnyObject?
        dic["iWorkerUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["nvFirstName"] = nvFirstName as AnyObject?
        dic["nvLastName"] = nvLastName as AnyObject?
        dic["iFamilyStatusType"] = iFamilyStatusType as AnyObject?
        dic["iGenderType"] = iGenderType as AnyObject?
        dic["bResident"] = bResident as AnyObject?
        dic["nvIdentityNumber"] = nvIdentityNumber as AnyObject?
        
        if dBirthDate != nil
        {
            dic["dBirthDate"] = Global.sharedInstance.convertNSDateToString(dateTOConvert: dBirthDate!) as AnyObject?
        }
        if dImmigrationDate != nil
        {
            dic["dImmigrationDate"] = Global.sharedInstance.convertNSDateToString(dateTOConvert: dImmigrationDate!) as AnyObject?
        }
        dic["iCityId"] = iCityId as AnyObject?
        dic["nvStreet"] = nvStreet as AnyObject?
        dic["nvHouseNumber"] = nvHouseNumber as AnyObject?
        dic["nvPhone"] = nvPhone as AnyObject?
        dic["iIncomeType"] = iIncomeType as AnyObject?
        dic["bHaveOtherIncome"] = bHaveOtherIncome as AnyObject?
        dic["bAskTaxCredit"] = bAskTaxCredit as AnyObject?
        dic["bAskingTaxCoodination"] = bAskingTaxCoodination as AnyObject?
        dic["bAskingTaxCoodinationByAssessor"] = bAskingTaxCoodinationByAssessor as AnyObject?
        if fCreditPoints != 0.0
        {
            dic["fCreditPoints"] = fCreditPoints as AnyObject?
        }
        dic["workerSpouse"] = workerSpouse.getDic() as AnyObject?
        dic["lWorkerChilds"] = getWorkerChildsAsDic() as AnyObject?
        dic["lWorkerOtherIncomes"] = getOtherIncomesAsDic() as AnyObject?
        //dic["lWorkerTaxRelifeReasons"] = lWorkerTaxRelifeReasons as AnyObject?
        //dic["lWorkerTaxCoordinations"] = lWorkerTaxCoordinations as AnyObject?
        dic["workerTaxCoordinationFileDet"] = workerTaxCoordinationFileDet.getDic() as AnyObject?
        dic["photoID"] = photoID.getDic() as AnyObject?
        dic["confirmationOfChildAllowance"] = confirmationOfChildAllowance.getDic() as AnyObject?
        dic["confirmationOfSeparate"] = confirmationOfSeparate.getDic() as AnyObject?
        
        dicBig["worker101Form"] = dic as AnyObject?
        
        return dicBig
    }
    
    func getDicForTaxRelifeReasons()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        var dicBig:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dicBig["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["lWorkerTaxRelifeReasons"] = getTaxRelifeReasonsAsDic() as AnyObject?
        dic["iWorker101FormId"] = GlobalEmployeeDetails.sharedInstance.worker101Form.iWorker101FormId as AnyObject?
        dic["iWorkerUserId"] = GlobalEmployeeDetails.sharedInstance.worker101Form.iWorkerUserId as AnyObject?
        
        dicBig["workerTaxRelifeReasonObj"] = dic as AnyObject?
        
        return dicBig
    }
    
    func getDicForTaxCoordinations()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        var dicBig:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dicBig["iUserId"] = Global.sharedInstance.user.iUserId as AnyObject?
        dic["lWorkerTaxCoordinations"] = getTaxCoordinationsAsDic() as AnyObject?
        dic["iWorker101FormId"] = GlobalEmployeeDetails.sharedInstance.worker101Form.iWorker101FormId as AnyObject?
        dic["iWorkerUserId"] = GlobalEmployeeDetails.sharedInstance.worker101Form.iWorkerUserId as AnyObject?
        
        dicBig["workerTaxCoordinationObj"] = dic as AnyObject?
        
        return dicBig
    }
    
    func getTaxCoordinationsAsDic()->Array<AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for it in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxCoordinations
        {
            dic = it.getDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getTaxRelifeReasonsAsDic()->Array<AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for it in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerTaxRelifeReasons
        {
            dic = it.getDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getWorkerChildsAsDic()->Array<AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for it in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerChilds
        {
            dic = it.getDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getOtherIncomesAsDic()->Array<AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr:Array<AnyObject> = []
        
        for it in GlobalEmployeeDetails.sharedInstance.worker101Form.lWorkerOtherIncomes
        {
            dic = it.getDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func dicToWorker101Form(dic:Dictionary<String,AnyObject>)->Worker101Form
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let worker101Form:Worker101Form = Worker101Form()
        
        if dic["iWorker101FormId"] != nil
        {
            worker101Form.iWorker101FormId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorker101FormId"]!)
        }
        else
        {
            worker101Form.iWorker101FormId = 0
        }
        
        if dic["iWorkerUserId"] != nil
        {
            worker101Form.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!)
        }
        else
        {
            worker101Form.iWorkerUserId = 0
        }
        
        worker101Form.nvFirstName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvFirstName"]!)
        
        worker101Form.nvLastName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLastName"]!)
        
        if dic["iFamilyStatusType"] != nil
        {
            worker101Form.iFamilyStatusType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iFamilyStatusType"]!)
        }
        else
        {
            worker101Form.iFamilyStatusType = 0
        }
        
        if dic["iGenderType"] != nil
        {
            worker101Form.iGenderType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iGenderType"]!)
        }
        else{
            worker101Form.iGenderType = 5
        }
        
        worker101Form.bResident = dic["bResident"] as! Bool
        
        worker101Form.nvIdentityNumber = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvIdentityNumber"]!)
        
        worker101Form.dBirthDate = dateFormatter.date(from: Global.sharedInstance.getStringFromDateString(dateString: Global.sharedInstance.parseJsonToString(stringToParse: dic["dBirthDate"]!))) as NSDate?
        
        
        worker101Form.dImmigrationDate = dateFormatter.date(from: Global.sharedInstance.getStringFromDateString(dateString: Global.sharedInstance.parseJsonToString(stringToParse: dic["dImmigrationDate"]!))) as NSDate?
        if dic["iCityId"] != nil
        {
            worker101Form.iCityId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iCityId"]!)
        }
        if dic["nvStreet"] != nil
        {
            worker101Form.nvStreet = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvStreet"]!)
        }
        if dic["nvHouseNumber"] != nil
        {
            worker101Form.nvHouseNumber = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvHouseNumber"]!)
        }
        if dic["nvPhone"] != nil
        {
            worker101Form.nvPhone = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvPhone"]!)
        }
        if dic["iIncomeType"] != nil
        {
            worker101Form.iIncomeType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iIncomeType"]!)
        }
        else{
            worker101Form.iIncomeType = 12
        }
        
        if let b = dic["bHaveOtherIncome"] as? Bool {
            worker101Form.bHaveOtherIncome = b
        } else {
            worker101Form.bHaveOtherIncome = false
        }
        
        
        worker101Form.bAskTaxCredit = dic["bAskTaxCredit"] as! Bool
        
        worker101Form.bAskingTaxCoodination = dic["bAskingTaxCoodination"] as! Bool
        
        worker101Form.bAskingTaxCoodinationByAssessor = dic["bAskingTaxCoodinationByAssessor"] as! Bool
        
        worker101Form.fCreditPoints = Global.sharedInstance.parseJsonToFloat(floatToParse: dic["fCreditPoints"]!)
        
        worker101Form.workerSpouse = workerSpouse.dicToWorkerSpouse(dic: dic["workerSpouse"] as! Dictionary<String, AnyObject>)
        
        //list of children
        let workerChild:WorkerChild = WorkerChild()
        worker101Form.lWorkerChilds = workerChild.workerChildToArray(arrDic: dic["lWorkerChilds"]! as! Array<Dictionary<String,AnyObject>>)
        
        
        //list of workerOtherIncome
        let workerOtherIncome:WorkerOtherIncome = WorkerOtherIncome()
        worker101Form.lWorkerOtherIncomes = workerOtherIncome.workerOtherIncomeToArray(arrDic: dic["lWorkerOtherIncomes"]! as! Array<Dictionary<String,AnyObject>>)
        
        //list of workerTaxRelifeReason
        let workerTaxRelifeReason:WorkerTaxRelifeReason = WorkerTaxRelifeReason()
        worker101Form.lWorkerTaxRelifeReasons = workerTaxRelifeReason.workerTaxRelifeReasonToArray(arrDic: dic["lWorkerTaxRelifeReasons"]! as! Array<Dictionary<String,AnyObject>>)
        
        //list of workerTaxCoordination
        let workerTaxCoordination:WorkerTaxCoordination = WorkerTaxCoordination()
        worker101Form.lWorkerTaxCoordinations = workerTaxCoordination.workerTaxCoordinationToArray(arrDic: dic["lWorkerTaxCoordinations"]! as! Array<Dictionary<String,AnyObject>>)
        
        worker101Form.workerTaxCoordinationFileDet = workerTaxCoordinationFileDet.dicToWorkerTaxCoordinationFileDet(dic: dic["workerTaxCoordinationFileDet"]! as! Dictionary<String, AnyObject>)
        
        if dic["photoID"] is NSNull
        {
            worker101Form.photoID = photoID
        }
        else
        {
            worker101Form.photoID = photoID.dicToFileObj(dic: dic["photoID"] as! Dictionary<String, AnyObject>)
        }
        if dic["confirmationOfChildAllowance"] is NSNull
        {
            worker101Form.confirmationOfChildAllowance = confirmationOfChildAllowance
        }
        else
        {
            worker101Form.confirmationOfChildAllowance = confirmationOfChildAllowance.dicToFileObj(dic: dic["confirmationOfChildAllowance"] as! Dictionary<String, AnyObject>)
        }
        
        if dic["confirmationOfSeparate"] is NSNull
        {
            worker101Form.confirmationOfSeparate = confirmationOfSeparate
        }
        else
        {
            worker101Form.confirmationOfSeparate = confirmationOfSeparate.dicToFileObj(dic: dic["confirmationOfSeparate"] as! Dictionary<String, AnyObject>)
        }
        
        return worker101Form
    }
    
}
