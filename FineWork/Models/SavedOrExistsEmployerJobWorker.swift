//
//  SavedOrExistsEmployerJobWorker.swift
//  FineWork
//
//  Created by Racheli Kroiz on 30/07/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SavedOrExistsEmployerJobWorker: NSObject {
    
    var nvUserName : String = "" //שם עובד
    var nvImageFilePath : String = "" //תמונה
    var dtDate : String = ""//תאריך
    var  nvMobile : String = ""//מס’ נייד

    var image : UIImage? = nil //מקום לשמירת התמונה
  
    func getDicFromSavedOrExistsEmployerJobWorker() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["nvUserName"] = nvUserName as AnyObject
        dic["nvImageFilePath"] = nvImageFilePath as AnyObject
        dic["dtDate"] = dtDate as AnyObject
        dic["nvMobile"] = nvMobile as AnyObject
        return dic
    }
    
    func getSavedOrExistsEmployerJobWorkerFromDic(dic: Dictionary<String, AnyObject>) -> SavedOrExistsEmployerJobWorker {
        let savedOrExistsEmployerJobWorker = SavedOrExistsEmployerJobWorker()
        
        if dic["nvUserName"] != nil {
            savedOrExistsEmployerJobWorker.nvUserName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvUserName"]!)
        }
        if dic["nvImageFilePath"] != nil {
            savedOrExistsEmployerJobWorker.nvImageFilePath = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImageFilePath"]!)
        }
        if dic["dtDate"] != nil {
            savedOrExistsEmployerJobWorker.dtDate = Global.sharedInstance.parseJsonToString(stringToParse: dic["dtDate"]!)
        }
        if dic["nvMobile"] != nil {
            savedOrExistsEmployerJobWorker.nvMobile = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMobile"]!)
        }
               return savedOrExistsEmployerJobWorker
    }


}
