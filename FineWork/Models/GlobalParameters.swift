//
//  GlobalParameters.swift
//  FineWork
//
//  Created by User on 29/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class GlobalParameters: NSObject {
    
    var iGlobalCodeId : Int = 0  // קוד פרמטר
    var nvGlobalParamName : String = ""  // שם פרמטר - תיאור
    var nvGlobalParamValue : String = ""  // ערך הפרמטר
    
    
    func getGlobalParametersFromDic(dic : Dictionary<String, AnyObject>) -> GlobalParameters {
        let globalParams = GlobalParameters()
        
        if dic["iGlobalCodeId"] != nil {
            globalParams.iGlobalCodeId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iGlobalCodeId"]!)
        }
        
        if dic["nvGlobalParamName"] != nil {
            globalParams.nvGlobalParamName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvGlobalParamName"]!)
        }
        
        if dic["nvGlobalParamValue"] != nil {
            globalParams.nvGlobalParamValue = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvGlobalParamValue"]!)
        }
        
        return globalParams
    }
    
}
