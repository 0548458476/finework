//
//  SimpleObj.swift
//  FineWork
//
//  Created by User on 14/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SimpleObj<T>: NSObject {
    
    var simpleProp : T?
    
    func getDicFromSimpleObj() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        dic["simpleProp"] = simpleProp as AnyObject
        return dic
    }
    
    func getSimpleObjFromDic(dic : Dictionary<String, AnyObject>) -> SimpleObj {
        let simpleObj = SimpleObj()
        if dic["simpleProp"] != nil {
            simpleObj.simpleProp = Global.sharedInstance.parseJsonToAnyObject(anyToParse: dic["simpleProp"]!) as? T
        }
        return simpleObj
    }

}
