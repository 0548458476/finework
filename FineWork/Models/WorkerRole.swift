//
//  WorkerRole.swift
//  FineWork
//
//  Created by Lior Ronen on 22/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerRole: NSObject {

    
    var iWorkerRoleId : Int = 0 //מזהה תפקיד לעובד
    var iEmploymentDomainRoleId : Int = 0 //(מזהה של תחום ותפקיד נשלח לשרת (וגם מתקבל
    var iEmploymentRoleType : Int = 0 //תפקיד, מתקבל מהשרת
    
    func getDictionaryFromWorkerRole() -> Dictionary<String, AnyObject> {
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        
        dic["iWorkerRoleId"] = iWorkerRoleId as AnyObject
        dic["iEmploymentDomainRoleId"] = iEmploymentDomainRoleId as AnyObject
        dic["iEmploymentRoleType"] = iEmploymentRoleType as AnyObject
        
        return dic
    }
    
    func getWorkerRoleFromDictionary(dic : Dictionary<String, AnyObject>) -> WorkerRole {
        let workerRole = WorkerRole()
        
        if dic["iWorkerRoleId"] != nil {
            workerRole.iWorkerRoleId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerRoleId"]!)
        }
        if dic["iEmploymentDomainRoleId"] != nil {
            workerRole.iEmploymentDomainRoleId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentDomainRoleId"]!)
        }
        if dic["iEmploymentRoleType"] != nil {
            workerRole.iEmploymentRoleType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentRoleType"]!)
        }
        
        return workerRole
    }

}
