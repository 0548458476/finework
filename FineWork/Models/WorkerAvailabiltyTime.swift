//
//  WorkerAvailabiltyTime.swift
//  FineWork
//
//  Created by Tamy wexelbom on 30.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerAvailabiltyTime: NSObject {

    var iWorkerAvailabilityId : Int = 0 //מזהה זמינות לעובד
    var iDayType : Int = 0 //-יום בשבוע, במקרה משרה מלאה יכיל 1
    var tFromHoure : String = "" //משעה
    var tToHoure : String = "" //עד שעה
    
    override init() {
        
    }
    
    func getWorkerAvailabiltyTimeAsDic() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerAvailabilityId"] = iWorkerAvailabilityId as AnyObject
        dic["iDayType"] = iDayType as AnyObject
        dic["tFromHoure"] = tFromHoure as AnyObject
        dic["tToHoure"] = tToHoure as AnyObject
        
        return dic
    }
    
    func getWorkerAvailabiltyTimeFromDic(dic : Dictionary<String, AnyObject>) -> WorkerAvailabiltyTime {
        let workerAvailabiltyTime : WorkerAvailabiltyTime = WorkerAvailabiltyTime()
        
        if dic["iWorkerAvailabilityId"] != nil {
            workerAvailabiltyTime.iWorkerAvailabilityId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerAvailabilityId"]!)
        }
        
        if dic["iDayType"] != nil {
            workerAvailabiltyTime.iDayType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iDayType"]!)
        }
        
        if dic["tFromHoure"] != nil {
            workerAvailabiltyTime.tFromHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tFromHoure"]!)
        }
        
        if dic["tToHoure"] != nil {
            workerAvailabiltyTime.tToHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tToHoure"]!)
        }
        
        return workerAvailabiltyTime
    }
}
