//
//  WorkerOfferedJobTimeAvailablity.swift
//  FineWork
//
//  Created by User on 21/08/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerOfferedJobTimeAvailablity: NSObject {
    var iEmployerJobTimeId : Int = 0
    var dFromDate :  String = ""
    var dToDate: String? = ""
    var tFromHoure : String = ""
    var tToHoure : String = ""
    var bWorkerOfferedJobTimeAvailablity : Bool = false
    var  nSumHour : Int = 1044
    
    
    var date :  Date? = nil
    
    func getDicFromWorkerOfferedJobTimeAvailablity() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerJobTimeId"] = iEmployerJobTimeId as AnyObject
        dic["dFromDate"] = dFromDate as AnyObject
        if dToDate == "" {
         dic["dToDate"] = nil
        }else{
            dic["dToDate"] = dToDate as AnyObject}
        dic["tFromHoure"] = tFromHoure as AnyObject
        dic["tToHoure"] = tToHoure as AnyObject
        dic["bWorkerOfferedJobTimeAvailablity"] = bWorkerOfferedJobTimeAvailablity as AnyObject
        dic["nSumHour"] = nSumHour as AnyObject


        return dic
    }
    
    func getSWorkerOfferedJobTimeAvailablityFromDic(dic: Dictionary<String, AnyObject>) -> WorkerOfferedJobTimeAvailablity {
        let workerOfferedJobTimeAvailablity = WorkerOfferedJobTimeAvailablity()
        
        if dic["iEmployerJobTimeId"] != nil {
            workerOfferedJobTimeAvailablity.iEmployerJobTimeId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobTimeId"]!)
        }
        if dic["dFromDate"] != nil {
            workerOfferedJobTimeAvailablity.dFromDate = Global.sharedInstance.parseJsonToString(stringToParse: dic["dFromDate"]!)
        }
        
        if dic["dToDate"] != nil {
            workerOfferedJobTimeAvailablity.dToDate = Global.sharedInstance.parseJsonToString(stringToParse: dic["dToDate"]!)
        }
        if dic["tFromHoure"] != nil {
            workerOfferedJobTimeAvailablity.tFromHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tFromHoure"]!)
        }
        if dic["tToHoure"] != nil {
            workerOfferedJobTimeAvailablity.tToHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tToHoure"]!)
        }
        if dic["bWorkerOfferedJobTimeAvailablity"] != nil {
            workerOfferedJobTimeAvailablity.bWorkerOfferedJobTimeAvailablity = dic["bWorkerOfferedJobTimeAvailablity"] as! Bool
        }
        if dic["nSumHour"] != nil {
            workerOfferedJobTimeAvailablity.nSumHour = Global.sharedInstance.parseJsonToInt(intToParse: dic["nSumHour"]!)
        }
              return workerOfferedJobTimeAvailablity
    }

}
