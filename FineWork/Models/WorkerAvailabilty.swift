//
//  WorkerAvailabilty.swift
//  FineWork
//
//  Created by Tamy wexelbom on 30.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerAvailabilty: NSObject {

    var iWorkerUserId : Int = 0 //מזהה משתמש
    var iWorkerAvailabilityType : Int = 0 //זמינות
    var dFullTimeAvailabilityFromDate : String? = nil //מתאריך משרה מלאה
    var dPartTimeAvailabilityFromDate : String? = nil //מתאריך משרה חלקית
    var dPartlTimeAvailabilityToDate : String? = nil //עד תאריך משרה חלקית
    var lWorkerAvailabiltyDays : Array<WorkerAvailabiltyDay> = [] //זמני העובד
    
    override init() {}
    
    func getDistionaryFromWorkerAvailability() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iWorkerUserId"]  = iWorkerUserId as AnyObject
        dic["iWorkerAvailabilityType"] = iWorkerAvailabilityType as AnyObject
        if dFullTimeAvailabilityFromDate != nil {
            dic["dFullTimeAvailabilityFromDate"] = dFullTimeAvailabilityFromDate as AnyObject
        }
        
        if dPartTimeAvailabilityFromDate != nil {
            dic["dPartTimeAvailabilityFromDate"] = dPartTimeAvailabilityFromDate as AnyObject
        }
        
        if dPartlTimeAvailabilityToDate != nil {
           dic["dPartlTimeAvailabilityToDate"] = dPartlTimeAvailabilityToDate as AnyObject
        }
        
        // get lWorkerAvailabiltyDays
        dic["lWorkerAvailabiltyDays"] = getlWorkerAvailabiltyDays() as AnyObject
        
        return dic
    }
    
    func getlWorkerAvailabiltyDays() -> Array<AnyObject> {
      
            var dic = Dictionary<String,AnyObject>()
            
            var arr : Array<AnyObject> = []
            
            for workerAvailabiltyDay in lWorkerAvailabiltyDays
            {
                dic = workerAvailabiltyDay.getWorkerAvailabiltyDayAsDic()
                arr.append(dic as AnyObject)
            }
            return arr
    }
    
    func getWorkerAvailabiltyFromDic(dic : Dictionary<String, AnyObject>) -> WorkerAvailabilty {
        let workerAvailabilty = WorkerAvailabilty()
        
        if dic["iWorkerUserId"] != nil {
            workerAvailabilty.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!)
        }
        
        if dic["iWorkerAvailabilityType"] != nil {
            workerAvailabilty.iWorkerAvailabilityType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerAvailabilityType"]!)
        }
        
        if dic["dFullTimeAvailabilityFromDate"] != nil {
            workerAvailabilty.dFullTimeAvailabilityFromDate = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dFullTimeAvailabilityFromDate"]!)
        }
        
        if dic["dPartTimeAvailabilityFromDate"] != nil {
            workerAvailabilty.dPartTimeAvailabilityFromDate = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dPartTimeAvailabilityFromDate"]!)
        }
        
        if dic["dPartlTimeAvailabilityToDate"] != nil {
            workerAvailabilty.dPartlTimeAvailabilityToDate = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dPartlTimeAvailabilityToDate"]!)
        }
        
        if dic["lWorkerAvailabiltyDays"] != nil {
            workerAvailabilty.lWorkerAvailabiltyDays = getlWorkerAvailabiltyDaysFromDic(dic: dic["lWorkerAvailabiltyDays"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        return workerAvailabilty
    }
    
    func getlWorkerAvailabiltyDaysFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerAvailabiltyDay> {
        var lWorkerAvailabiltyDays = Array<WorkerAvailabiltyDay>()
        
        var workerAvailabiltyDay = WorkerAvailabiltyDay()
        
        for day in dic {
            workerAvailabiltyDay = workerAvailabiltyDay.getWorkerAvailabiltyDayFromDic(dic: day)
            lWorkerAvailabiltyDays.append(workerAvailabiltyDay)
        }
        
        return lWorkerAvailabiltyDays
    }

    

}
