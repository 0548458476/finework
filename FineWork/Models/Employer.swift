//
//  Employer.swift
//  FineWork
//
//  Created by User on 18.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class Employer: NSObject {
    
    var iEmployerId : Int = 0 //מזהה עסק
    var nvCompanyName : String = "" //שם עסק/חברה
    var nvBusinessIdentity : String = "" //מס’ עוסק מורשה ח.פ./ע.מ.
    var nvAddress : String = "" //כתובת
    var nvLng : String = "" //נקודת אורך
    var nvLat : String = "" //נקודת רוחב
    var nvPhone : String = "" //טלפון
    var nvMail : String = "" //מייל
    var iUserId : Int = 0 //מזהה משתמש
    var iMainActivityFieldType : Int = 0 //תחום הפעילות המרכזי
    var iEmployeeNumberType : Int = 0 //כמות עובדים בחברה
    var nvEmployerName : String = "" //שם עסק/מותג
    var nvBusinessDescription : String = "" //תאור
    var nvCentralAreaActivity : String = "" //אזור מרכזי בו אתם פועלים
    var nvSiteLink : String = "" //קישור לאתר
    var logoImage : FileObj = FileObj() //לוגו
    var banner : FileObj = FileObj() //באנר
    var employerPaymentMethod : EmployerPaymentMethod = EmployerPaymentMethod() //פרטי תשלום
    
    func getDictionaryFromEmployer() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerId"] = iEmployerId as AnyObject
        dic["nvCompanyName"] = nvCompanyName as AnyObject
        dic["nvBusinessIdentity"] = nvBusinessIdentity as AnyObject
        dic["nvAddress"] = nvAddress as AnyObject
        dic["nvLng"] = nvLng as AnyObject
        dic["nvLat"] = nvLat as AnyObject
        dic["nvPhone"] = nvPhone as AnyObject
        dic["nvMail"] = nvMail as AnyObject
        dic["iUserId"] = iUserId as AnyObject
        dic["iMainActivityFieldType"] = iMainActivityFieldType as AnyObject
        dic["iEmployeeNumberType"] = iEmployeeNumberType as AnyObject
        dic["nvEmployerName"] = nvEmployerName as AnyObject
        dic["nvBusinessDescription"] = nvBusinessDescription as AnyObject
        dic["nvCentralAreaActivity"] = nvCentralAreaActivity as AnyObject
        dic["nvSiteLink"] = nvSiteLink as AnyObject
        dic["logoImage"] = logoImage.getDic() as AnyObject
        dic["banner"] = banner.getDic() as AnyObject
        dic["employerPaymentMethod"] = employerPaymentMethod.getDicFromEmployerPaymentMethod() as AnyObject

        return dic
    }
    
    func getEmployerFromDic(dic : Dictionary<String, AnyObject>) -> Employer {
        let employer = Employer()
        
        if dic["iEmployerId"] != nil {
            employer.iEmployerId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerId"]!)
        }
        
        if dic["nvCompanyName"] != nil {
            employer.nvCompanyName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvCompanyName"]!)
        }
        
        if dic["nvBusinessIdentity"] != nil {
            employer.nvBusinessIdentity = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvBusinessIdentity"]!)
        }
        
        if dic["nvAddress"] != nil {
            employer.nvAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!)
        }
        
        if dic["nvLng"] != nil {
            employer.nvLng = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLng"]!)
        }
        
        if dic["nvLat"] != nil {
            employer.nvLat = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLat"]!)
        }
        
        if dic["nvPhone"] != nil {
            employer.nvPhone = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvPhone"]!)
        }
        
        if dic["nvMail"] != nil {
            employer.nvMail = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMail"]!)
        }
        
        
        if dic["iUserId"] != nil {
            employer.iUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iUserId"]!)
        }
        
        
        if dic["iMainActivityFieldType"] != nil {
            employer.iMainActivityFieldType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iMainActivityFieldType"]!)
        }
        
        if dic["iEmployeeNumberType"] != nil {
            employer.iEmployeeNumberType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployeeNumberType"]!)
        }
        if dic["nvEmployerName"] != nil {
            employer.nvEmployerName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerName"]!)
        }
        
        if dic["nvBusinessDescription"] != nil {
            employer.nvBusinessDescription = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvBusinessDescription"]!)
        }
        
        
        if dic["nvCentralAreaActivity"] != nil {
            employer.nvCentralAreaActivity = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvCentralAreaActivity"]!)
        }
        
        
        if dic["nvSiteLink"] != nil {
            employer.nvSiteLink = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSiteLink"]!)
        }
        
        if dic["logoImage"] != nil {
            let fileObj : FileObj = FileObj()
            employer.logoImage = fileObj.dicToFileObj(dic: dic["logoImage"] as! Dictionary<String, AnyObject>)
        }
        
        if dic["banner"] != nil {
            let fileObj : FileObj = FileObj()
            employer.banner = fileObj.dicToFileObj(dic: dic["banner"] as! Dictionary<String, AnyObject>)
        }
        
        if dic["employerPaymentMethod"] != nil {
            let employerPaymentMethod1 : EmployerPaymentMethod = EmployerPaymentMethod()
            employer.employerPaymentMethod = employerPaymentMethod1.getEmployerPaymentMethodFromDic(dic: dic["employerPaymentMethod"] as! Dictionary<String, AnyObject>)
        }

        
        return employer
    }
    
    

}
