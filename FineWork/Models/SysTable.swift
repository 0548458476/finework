//
//  SysTable.swift
//  FineWork
//
//  Created by Lior Ronen on 09/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SysTable: NSObject {

    //DataMember
    var iId : Int = 0
    var nvName : String = ""
    var iWorkersByEmploymentDomainCount : Int = 0
    //--
    var isSelected = false
    
    func getSysTableFromDic(dic : Dictionary<String, AnyObject>) -> SysTable {
        let sysTable = SysTable()
        
        if dic["iId"] != nil {
            sysTable.iId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iId"]!)
        }
        
        if dic["nvName"] != nil {
            sysTable.nvName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvName"]!)
        }
        if dic["iWorkersByEmploymentDomainCount"] != nil {
            sysTable.iWorkersByEmploymentDomainCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkersByEmploymentDomainCount"]!)
        }
        
        return sysTable
    }
}
