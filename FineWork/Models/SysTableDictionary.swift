//
//  SysTableDictionary.swift
//  FineWork
//
//  Created by Lior Ronen on 08/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SysTableDictionary: NSObject {

    var Key : String = ""
    var Value : Array<SysTable> = []
    
    
    func getSysTableDictionaryFromDic(dic : Dictionary<String, AnyObject>) -> SysTableDictionary {
        let sysTableDictionary = SysTableDictionary()
    
        if dic["Key"] != nil {
            sysTableDictionary.Key = Global.sharedInstance.parseJsonToString(stringToParse: dic["Key"]!)
        }
        
        if dic["Value"] != nil {
            sysTableDictionary.Value = getSysTableArrayFromDic(dic: dic["Value"] as! Array<Dictionary<String, AnyObject>>)
        }
    
        return sysTableDictionary
    }
    
    func getSysTableArrayFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<SysTable> {
        var sysTableArray : Array<SysTable> = Array<SysTable>()
        
        var sysTable : SysTable = SysTable()
        
        for sysTableItem in dic {
            sysTable = sysTable.getSysTableFromDic(dic: sysTableItem)
            sysTableArray.append(sysTable)
        }
        
        return sysTableArray
    }
}
