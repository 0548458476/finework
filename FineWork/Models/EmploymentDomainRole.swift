//
//  EmploymentDomainRole.swift
//  FineWork
//
//  Created by Lior Ronen on 22/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmploymentDomainRole: NSObject {

    var iEmploymentDomainRoleId : Int = 0 //מזהה תחום תפקיד
    var iEmploymentDomainType : Int = 0 //מזהה תחום
    var iEmploymentRoleType : Int = 0 //מזהה תפקיד
    var nvEmploymentRoleTypeName : String = "" //שם תפקיד
    var iWorkersByEmploymentDomainRoleCount : Int = 0 //תפקיד
    //---
    var isSelected = false
    
    func getDictionaryFromEmploymentDomainRole() -> Dictionary<String, AnyObject> {
        var dic : Dictionary<String, AnyObject> = Dictionary<String, AnyObject>()
        
        dic["iEmploymentDomainRoleId"] = iEmploymentDomainRoleId as AnyObject
        dic["iEmploymentDomainType"] = iEmploymentDomainType as AnyObject
        dic["iEmploymentRoleType"] = iEmploymentRoleType as AnyObject
        dic["nvEmploymentRoleTypeName"] = nvEmploymentRoleTypeName as AnyObject
        dic["iWorkersByEmploymentDomainRoleCount"] = iWorkersByEmploymentDomainRoleCount as AnyObject

        
        return dic
    }
    
    func getEmploymentDomainRoleFromDictionary(dic : Dictionary<String, AnyObject>) -> EmploymentDomainRole {
        let employmentDomainRole = EmploymentDomainRole()
        
        if dic["iEmploymentDomainRoleId"] != nil {
            employmentDomainRole.iEmploymentDomainRoleId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentDomainRoleId"]!)
        }
        if dic["iEmploymentDomainType"] != nil {
            employmentDomainRole.iEmploymentDomainType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentDomainType"]!)
        }
        if dic["iEmploymentRoleType"] != nil {
            employmentDomainRole.iEmploymentRoleType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentRoleType"]!)
        }
        if dic["nvEmploymentRoleTypeName"] != nil {
            employmentDomainRole.nvEmploymentRoleTypeName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmploymentRoleTypeName"]!)
        }
        if dic["iWorkersByEmploymentDomainRoleCount"] != nil {
            employmentDomainRole.iWorkersByEmploymentDomainRoleCount = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkersByEmploymentDomainRoleCount"]!)
        }
        
        return employmentDomainRole
    }

}
