//
//  SumWageEmployerJob.swift
//  FineWork
//
//  Created by Racheli Kroiz on 17/07/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import Foundation

class SumWageEmployerJob: NSObject {
    var iSumWage : Int = 0 //עלות כוללת של המשרה למעסיק
    var fSumWageAvg : Float? = 0.0 ////ממוצע חודשי של המשרה (במקרה שהמשרה מתפרסת על פני יותר מחודש)
   
    override init() {}
    
    
    func getSumWageEmployerJobFromDic(dic : Dictionary<String, AnyObject>) -> SumWageEmployerJob {
        let sumWageEmployerJob = SumWageEmployerJob()
        
        if dic["iSumWage"] != nil {
            sumWageEmployerJob.iSumWage = Global.sharedInstance.parseJsonToInt(intToParse: dic["iSumWage"]!)
        }
        
        if dic["fSumWageAvg"] != nil {
            sumWageEmployerJob.fSumWageAvg = Global.sharedInstance.parseJsonToFloatOptionNil(floatToParse: dic["fSumWageAvg"]!)
        }
        
        return sumWageEmployerJob
    }
    

   }
