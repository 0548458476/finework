//
//  WorkerAvailabiltyDay.swift
//  FineWork
//
//  Created by Tamy wexelbom on 30.3.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerAvailabiltyDay: NSObject {

    
    var iDayType : Int = 0 //יום בשבוע
    var lWorkerAvailabiltyTimes : Array<WorkerAvailabiltyTime> = [] //טווחי שעות

    override init() {}
    
    func getWorkerAvailabiltyDayAsDic() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iDayType"] = iDayType as AnyObject
        
        dic["lWorkerAvailabiltyTimes"] = getlWorkerAvailabiltyTimes() as AnyObject
        
        return dic
    }
    
    func getlWorkerAvailabiltyTimes() -> Array<AnyObject> {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for workerAvailabiltyTime in lWorkerAvailabiltyTimes
        {
            dic = workerAvailabiltyTime.getWorkerAvailabiltyTimeAsDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getWorkerAvailabiltyDayFromDic(dic : Dictionary<String,AnyObject>) -> WorkerAvailabiltyDay {
        let workerAvailabiltyDay = WorkerAvailabiltyDay()
        
        if dic["iDayType"] != nil {
            workerAvailabiltyDay.iDayType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iDayType"]!)
        }
        
        if dic["lWorkerAvailabiltyTimes"] != nil {
            workerAvailabiltyDay.lWorkerAvailabiltyTimes = getlWorkerAvailabiltyTimesFromDic(dic: dic["lWorkerAvailabiltyTimes"] as! Array<Dictionary<String, AnyObject>>)
        }
        
        return workerAvailabiltyDay
    }
    
    func getlWorkerAvailabiltyTimesFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerAvailabiltyTime> {
        var lWorkerAvailabiltyTimes = Array<WorkerAvailabiltyTime>()
        
        var workerAvailabiltyTime = WorkerAvailabiltyTime()
        
        for time in dic {
            workerAvailabiltyTime = workerAvailabiltyTime.getWorkerAvailabiltyTimeFromDic(dic: time)
            lWorkerAvailabiltyTimes.append(workerAvailabiltyTime)
        }
        
        return lWorkerAvailabiltyTimes
    }
}
