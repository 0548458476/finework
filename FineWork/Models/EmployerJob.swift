//
//  EmployerJob.swift
//  FineWork
//
//  Created by User on 14/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmployerJob: NSObject {

    
    var iEmployerJobId : Int = 0 //מזהה משרה
    var iJobQuantity : Int = 0 //מס’ עובדים
    var iEmploymentDomainRoleId : Int = 0 // (מזהה של תחום ותפקיד נשלח לשרת (וגם מתקבל
    var iEmploymentDomainType : Int = 0 //תחום, מתקבל מהשרת
    var iEmploymentRoleType : Int = 0 //תפקיד, מתקבל מהשרת
    var iEmployerId : Int = 0 //מזהה מעסיק
    var iEmployerJobWorkerType : Int = 0 //סוג עובד למשרה
    var nvKnownWorkerPhone : String = "" //טלפון עובד מוכר
    var lPopulationType : Array<SimpleObj<Int>> = [] //העבודה מתאימה ל
    var iEmployerJobPeriodType : EmployerJobPeriodType = EmployerJobPeriodType.ConstantWorker //תקופת העסקה
    var iEmployerJobScheduleType : EmployerJobScheduleType = EmployerJobScheduleType.OneTimeJob //סוג זמן המשרה
    var lEmployerJobTimes : Array<EmployerJobTime> = [] //זמני המשרה
    var mHourlyWage : Double = 0 //עלות לשעה
    var bBonusOption : Bool = false //אפשרות לבונוסים
    var bShowHourlyWageToWorker : Bool = false //האם להציג את צפיות עלות המשרה
    var iWorkingLocationType : WorkingLocationType = WorkingLocationType.myAddress //סוג מיקום
    var nvGoogleAddress : String = "" //כתובת גוגל
    var nvLng : String = "" //נקודת אורך
    var nvLat : String = "" //נקודת רוחב
    var nvJobDescription : String = "" //תאור המשרה
    var bRisk : Bool = false //סיכונים?
    var nvRiskDetails : String = "" //פירוט סיכונים
    var bSafetyRequirement : Bool = false //דרישות בטיחות?
    var nvSafetyRequirementDetails : String = "" //פירוט דרישות בטיחות
    var iPhysicalDifficultyRank : Int? = nil //קושי פיזי
    var nvWorkSpaceDetails : String = "" //סביבת עבודה
    var employerJobNewWorker = EmployerJobNewWorker() //תנאי סף
    var  nvEmployerJobPeriodType : String = ""
    var nvEmployerJobPopulations : String = ""
    
    func getDicFromEmployerJob() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerJobId"] = iEmployerJobId as AnyObject
        dic["iJobQuantity"] = iJobQuantity as AnyObject
        dic["iEmploymentDomainRoleId"] = iEmploymentDomainRoleId as AnyObject
        dic["iEmploymentDomainType"] = iEmploymentDomainType as AnyObject
        dic["iEmploymentRoleType"] = iEmploymentRoleType as AnyObject
        dic["iEmployerId"] = iEmployerId as AnyObject
        dic["iEmployerJobWorkerType"] = iEmployerJobWorkerType as AnyObject
        dic["nvKnownWorkerPhone"] = nvKnownWorkerPhone as AnyObject
        dic["lPopulationType"] = getlPopulationTypeAsDic() as AnyObject
        dic["iEmployerJobPeriodType"] = iEmployerJobPeriodType.rawValue as AnyObject
        dic["iEmployerJobScheduleType"] = iEmployerJobScheduleType.rawValue as AnyObject
        dic["lEmployerJobTimes"] = getlEmployerJobTimesAsDic() as AnyObject
        dic["mHourlyWage"] = mHourlyWage as AnyObject
        dic["bBonusOption"] = bBonusOption as AnyObject
        dic["bShowHourlyWageToWorker"] = bShowHourlyWageToWorker as AnyObject
        dic["iWorkingLocationType"] = iWorkingLocationType.rawValue as AnyObject
        dic["nvGoogleAddress"] = nvGoogleAddress as AnyObject
        dic["nvLng"] = nvLng as AnyObject
        dic["nvLat"] = nvLat as AnyObject
        dic["nvJobDescription"] = nvJobDescription as AnyObject
        dic["bRisk"] = bRisk as AnyObject
        dic["nvRiskDetails"] = nvRiskDetails as AnyObject
        dic["bSafetyRequirement"] = bSafetyRequirement as AnyObject
        dic["nvSafetyRequirementDetails"] = nvSafetyRequirementDetails as AnyObject
        if iPhysicalDifficultyRank != nil {
            dic["iPhysicalDifficultyRank"] = iPhysicalDifficultyRank as AnyObject
        }
        dic["nvWorkSpaceDetails"] = nvWorkSpaceDetails as AnyObject
        dic["employerJobNewWorker"] = employerJobNewWorker.getDicFromEmployerJobNewWorker() as AnyObject
        dic["nvEmployerJobPeriodType"] = nvEmployerJobPeriodType as AnyObject
        dic["nvEmployerJobPopulations"] = nvEmployerJobPopulations as AnyObject

        return dic
    }
    
    func getlPopulationTypeAsDic()->Array<AnyObject>
    {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for item in lPopulationType
        {
            dic = item.getDicFromSimpleObj()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getlEmployerJobTimesAsDic()->Array<AnyObject>
    {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for item in lEmployerJobTimes
        {
            dic = item.getDicFromEmployerJobTime()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    func getEmployerJobFromDic(dic : Dictionary<String, AnyObject>) -> EmployerJob {
        let employerJob = EmployerJob()
        
        if dic["iEmployerJobId"] != nil {
            employerJob.iEmployerJobId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobId"]!)
        }
        if dic["iJobQuantity"] != nil {
            employerJob.iJobQuantity = Global.sharedInstance.parseJsonToInt(intToParse: dic["iJobQuantity"]!)
        }
        if dic["iEmploymentDomainRoleId"] != nil {
            employerJob.iEmploymentDomainRoleId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentDomainRoleId"]!)
        }
        if dic["iEmploymentDomainType"] != nil {
            employerJob.iEmploymentDomainType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentDomainType"]!)
        }
        if dic["iEmploymentRoleType"] != nil {
            employerJob.iEmploymentRoleType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmploymentRoleType"]!)
        }
        if dic["iEmployerId"] != nil {
            employerJob.iEmployerId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerId"]!)
        }
       
        if dic["iEmployerJobWorkerType"] != nil {
            employerJob.iEmployerJobWorkerType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobWorkerType"]!)
        }
        if dic["nvKnownWorkerPhone"] != nil {
            employerJob.nvKnownWorkerPhone = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvKnownWorkerPhone"]!)
        }
        if dic["lPopulationType"] != nil {
            employerJob.lPopulationType = getlPopulationType(dic: dic["lPopulationType"] as! Array<Dictionary<String, AnyObject>>)
        }
        if dic["iEmployerJobPeriodType"] != nil {
            employerJob.iEmployerJobPeriodType = EmployerJobPeriodType(rawValue: Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobPeriodType"]!))!
        }
        if dic["iEmployerJobScheduleType"] != nil {
            employerJob.iEmployerJobScheduleType = EmployerJobScheduleType(rawValue: Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobScheduleType"]!))!
        }
        if dic["lEmployerJobTimes"] != nil {
            employerJob.lEmployerJobTimes = getlEmployerJobTimes(dic: dic["lEmployerJobTimes"] as! Array<Dictionary<String, AnyObject>>)
        }
        if dic["mHourlyWage"] != nil {
            employerJob.mHourlyWage = Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["mHourlyWage"]!)
        }
        if dic["bBonusOption"] != nil {
            employerJob.bBonusOption = dic["bBonusOption"] as! Bool
        }
        if dic["bShowHourlyWageToWorker"] != nil {
            employerJob.bShowHourlyWageToWorker = dic["bShowHourlyWageToWorker"] as! Bool
        }
        
        if dic["iWorkingLocationType"] != nil {
            employerJob.iWorkingLocationType = WorkingLocationType(rawValue: Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkingLocationType"]!))!
        }
        if dic["nvGoogleAddress"] != nil {
            employerJob.nvGoogleAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvGoogleAddress"]!)
        }
        if dic["nvLng"] != nil {
            employerJob.nvLng = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLng"]!)
        }
        if dic["nvLat"] != nil {
            employerJob.nvLat = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLat"]!)
        }
        if dic["nvJobDescription"] != nil {
            employerJob.nvJobDescription = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvJobDescription"]!)
        }
        if dic["bRisk"] != nil {
            employerJob.bRisk = dic["bRisk"] as! Bool
        }
        if dic["nvRiskDetails"] != nil {
            employerJob.nvRiskDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRiskDetails"]!)
        }
        if dic["bSafetyRequirement"] != nil {
            employerJob.bSafetyRequirement = dic["bSafetyRequirement"] as! Bool
        }
        if dic["nvSafetyRequirementDetails"] != nil {
            employerJob.nvSafetyRequirementDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSafetyRequirementDetails"]!)
        }
        if dic["iPhysicalDifficultyRank"] != nil {
            employerJob.iPhysicalDifficultyRank = Global.sharedInstance.parseJsonToIntOptionNil(intToParse: dic["iPhysicalDifficultyRank"]!)
        }
        
        if dic["nvWorkSpaceDetails"] != nil {
            employerJob.nvWorkSpaceDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkSpaceDetails"]!)
        }
        //        employerJobNewWorker
        if dic["employerJobNewWorker"] != nil {
            let employerJobNewWorker1 = EmployerJobNewWorker()
            employerJob.employerJobNewWorker = employerJobNewWorker1.getEmployerJobNewWorkerFromDic(dic: dic["employerJobNewWorker"] as! Dictionary<String, AnyObject>)
        }
        if dic["nvEmployerJobPeriodType"] != nil {
            employerJob.nvEmployerJobPeriodType = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerJobPeriodType"]!)
        }
        if dic["nvEmployerJobPopulations"] != nil {
            employerJob.nvEmployerJobPopulations = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerJobPopulations"]!)
        }

        return employerJob
    }
    
    func getlPopulationType(dic : Array<Dictionary<String, AnyObject>>) -> Array<SimpleObj<Int>> {
        var arr = Array<SimpleObj<Int>>()
        let item = SimpleObj<Int>()
        
        for itemDic in dic {
            arr.insert(item.getSimpleObjFromDic(dic: itemDic), at: arr.count)
        }
        
        return arr
    }
    
    func getlEmployerJobTimes(dic : Array<Dictionary<String, AnyObject>>) -> Array<EmployerJobTime> {
        var arr = Array<EmployerJobTime>()
        let item = EmployerJobTime()
        
        for itemDic in dic {
            arr.insert(item.getEmployerJobTimeFromDic(dic: itemDic), at: arr.count)
        }
        
        return arr
    }

}
