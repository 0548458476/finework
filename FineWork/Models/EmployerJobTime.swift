//
//  EmployerJobTime.swift
//  FineWork
//
//  Created by User on 14/05/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmployerJobTime: NSObject {

    var iEmployerJobTimeId : Int = 0 //מזהה זמן משרה
    var dFromDate : String? = nil //מתאריך
    var dToDate : String? = nil //עד תאריך
    var tFromHoure : String = "" //משעה
    var tToHoure : String = "" //עד שעה

    func getDicFromEmployerJobTime() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        dic["iEmployerJobTimeId"] = iEmployerJobTimeId as AnyObject
        if dFromDate != nil {
            if !dFromDate!.contains("Date"){
            dic["dFromDate"] = Global.sharedInstance.convertNSDateToString(dateTOConvert: dateFormatter.date(from: dFromDate!)! as NSDate) as AnyObject
            }
            else {
                  dic["dFromDate"] = dFromDate as AnyObject
            }
        }
        if dToDate != nil {
            if !dToDate!.contains("Date"){
                dic["dToDate"] = Global.sharedInstance.convertNSDateToString(dateTOConvert: dateFormatter.date(from: dToDate!)! as NSDate) as AnyObject
            }
            else {
                dic["dToDate"] = dToDate as AnyObject
            }

        
        }
        dic["tFromHoure"] = tFromHoure as AnyObject
        dic["tToHoure"] = tToHoure as AnyObject
        
        return dic
    }
    
    func getEmployerJobTimeFromDic(dic : Dictionary<String, AnyObject>) -> EmployerJobTime {
        let employerJobTime = EmployerJobTime()
        
        if dic["iEmployerJobTimeId"] != nil {
            employerJobTime.iEmployerJobTimeId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobTimeId"]!)
        }
        if dic["dFromDate"] != nil {
            employerJobTime.dFromDate = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dFromDate"]!)
        }
        if dic["dToDate"] != nil {
            employerJobTime.dToDate = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["dToDate"]!)
        }
        if dic["tFromHoure"] != nil {
            employerJobTime.tFromHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tFromHoure"]!)
        }
        if dic["tToHoure"] != nil {
            employerJobTime.tToHoure = Global.sharedInstance.parseJsonToString(stringToParse: dic["tToHoure"]!)
        }
        
        return employerJobTime
    }
}
