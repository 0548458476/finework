//
//  RankingMeasure.swift
//  FineWork
//
//  Created by User on 14/09/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class RankingMeasure: NSObject {
    var      iRankingType  : Int = 0//סוג דירוג
    var   iJobId : Int = 0//מזהה משרה מוצעת
    var iMeasureValue1 : Int = 0 //ערך ראשון
   var iMeasureValue2 : Int = 0//ערך שני
   var iMeasureValue3 : Int = 0//ערך שלישי
   var iMeasureValue4 : Int = 0 //ערך רביעי
    var nvRankingComment : String = "" //מלל חופשי
    var bAgreeToWorkAgain : Bool?  =  nil //האם אתה מעונין…

    override init() {
       
    }
    init(_iRankingType : Int , _iJobId: Int , _iMeasureValue1 : Int , _iMeasureValue2 : Int , _iMeasureValue3 : Int , _iMeasureValue4 :Int , _nvRankingComment :String , _bAgreeToWorkAgain :Bool?) {
        iRankingType = _iRankingType
        iJobId = _iJobId
        iMeasureValue1 = _iMeasureValue1
        iMeasureValue2 = _iMeasureValue2
        iMeasureValue3 = _iMeasureValue3
        iMeasureValue4 = _iMeasureValue4
        nvRankingComment = _nvRankingComment
        if _bAgreeToWorkAgain != nil{
        bAgreeToWorkAgain = _bAgreeToWorkAgain!
        }

    }
    func getDicFromRankingMeasure() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iRankingType"] = iRankingType as AnyObject
        dic["iJobId"] = iJobId as AnyObject
        dic["iMeasureValue1"] = iMeasureValue1 as AnyObject
        dic["iMeasureValue2"] = iMeasureValue2 as AnyObject
        dic["iMeasureValue3"] = iMeasureValue3 as AnyObject
        dic["iMeasureValue4"] = iMeasureValue4 as AnyObject
        dic["nvRankingComment"] = nvRankingComment as AnyObject
        if bAgreeToWorkAgain != nil{
        dic["bAgreeToWorkAgain"] = bAgreeToWorkAgain as AnyObject
        }
        return dic
    }
    
    func getRankingMeasureFromDic(dic: Dictionary<String, AnyObject>) -> RankingMeasure {
        let rankingMeasure = RankingMeasure()
        
        if dic["iRankingType"] != nil {
            rankingMeasure.iRankingType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iRankingType"]!)
        }
        if dic["iJobId"] != nil {
            rankingMeasure.iJobId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iJobId"]!)
        }
        if dic["iMeasureValue1"] != nil {
            rankingMeasure.iMeasureValue1 = Global.sharedInstance.parseJsonToInt(intToParse: dic["iMeasureValue1"]!)
        }
        if dic["iMeasureValue2"] != nil {
            rankingMeasure.iMeasureValue2 = Global.sharedInstance.parseJsonToInt(intToParse: dic["iMeasureValue2"]!)
        }
        if dic["iMeasureValue3"] != nil {
            rankingMeasure.iMeasureValue3 = Global.sharedInstance.parseJsonToInt(intToParse: dic["iMeasureValue3"]!)
        }
        if dic["iMeasureValue4"] != nil {
            rankingMeasure.iMeasureValue4 = Global.sharedInstance.parseJsonToInt(intToParse: dic["iMeasureValue4"]!)
        }
        if dic["nvRankingComment"] != nil {
            rankingMeasure.nvRankingComment = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRankingComment"]!)
        }
        if dic["bAgreeToWorkAgain"] != nil {
            rankingMeasure.bAgreeToWorkAgain = dic["nvRankingComment"]! as! Bool
            
           
        }

               return rankingMeasure
    }

}
