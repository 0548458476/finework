//
//  WorkerHomePageJob.swift
//  FineWork
//
//  Created by User on 25.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class WorkerHomePageJob: NSObject {

    var nvEmployerName : String = "" //שם העסק
    var nvLogoImageFilePath : String = "" //תמונת לוגו
    var nvAddress : String = "" //כתובת
    var tTimeLeft : String = "" //זמן עד תאריך אחרון להגשת מועמדות
    
    
    
    // logo image
    var logoImage : UIImage?

    
    func getDicFromWorkerHomePageJob() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["nvEmployerName"] = nvEmployerName as AnyObject
        dic["nvLogoImageFilePath"] = nvLogoImageFilePath as AnyObject
        dic["nvAddress"] = nvAddress as AnyObject
        dic["tTimeLeft"] = tTimeLeft as AnyObject
        
        return dic
    }
    
    func getWorkerHomePageJobFromDic(dic : Dictionary<String, AnyObject>) -> WorkerHomePageJob {
        let workerHomePageJob = WorkerHomePageJob()
        if dic["nvEmployerName"] != nil {
            workerHomePageJob.nvEmployerName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerName"]!)
        }
        
        if dic["nvLogoImageFilePath"] != nil {
            workerHomePageJob.nvLogoImageFilePath = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLogoImageFilePath"]!)
        }
        
        if dic["nvAddress"] != nil {
            workerHomePageJob.nvAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!)
        }
        
        if dic["tTimeLeft"] != nil {
            workerHomePageJob.tTimeLeft = Global.sharedInstance.parseJsonToString(stringToParse: dic["tTimeLeft"]!)
        }
        
        return workerHomePageJob
    }
}
