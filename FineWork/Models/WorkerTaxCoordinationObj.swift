//
//  WorkerTaxCoordinationObj.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerTaxCoordinationObj: NSObject {
    
    //MARK: - Variables

    var iWorker101FormId:Int
    var iWorkerUserId:Int
    var lWorkerTaxCoordinations:Array<WorkerTaxCoordination> = []

    //MARK: - Initial
    
    override init() {
        iWorker101FormId = 0
        iWorkerUserId = 0
        lWorkerTaxCoordinations = []
    }
    
    init(_iWorker101FormId:Int,_iWorkerUserId:Int,_lWorkerTaxCoordinations:Array<WorkerTaxCoordination>) {
        iWorker101FormId = _iWorker101FormId
        iWorkerUserId = _iWorkerUserId
        lWorkerTaxCoordinations = _lWorkerTaxCoordinations
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iWorker101FormId"] = iWorker101FormId as AnyObject?
        dic["iWorkerUserId"] = iWorkerUserId as AnyObject?
        //2do add func to array as in BThere
        dic["lWorkerTaxCoordinations"] = lWorkerTaxCoordinations as AnyObject?
        
        return dic
    }
    
    func dicToWorkerTaxCoordinationObj(dic:Dictionary<String,AnyObject>)->WorkerTaxCoordinationObj
    {
        let workerTaxCoordinationObj:WorkerTaxCoordinationObj = WorkerTaxCoordinationObj()
        
        workerTaxCoordinationObj.iWorker101FormId = Global.sharedInstance.parseJsonToInt(intToParse:dic["iWorker101FormId"]!)
        workerTaxCoordinationObj.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse:dic["iWorkerUserId"]!)
        //2do add func to array as in BThere
//        workerTaxCoordinationObj.lWorkerTaxCoordinations =
        
        return workerTaxCoordinationObj
    }
}
