//
//  ResetRanking.swift
//  FineWork
//
//  Created by Racheli Kroiz on 25/07/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class ResetRanking: NSObject {

    var isReset : Bool = false
    var NextResetFromDate :  String? = nil
    
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["isReset"] = isReset as AnyObject?
        dic["NextResetFromDate"] = NextResetFromDate as AnyObject?
        
        return dic
    }
    
   
    
    func getRResetRankingObj(dic:Dictionary<String,AnyObject>)->ResetRanking
    {
        let resetRanking :ResetRanking = ResetRanking()
        
        if dic["isReset"] != nil {
            resetRanking.isReset = dic["isReset"] as! Bool
        }
        if dic["NextResetFromDate"] != nil {
            resetRanking.NextResetFromDate = Global.sharedInstance.parseJsonToStringOptionNil(stringToParse: dic["NextResetFromDate"]!)
        }
       
        return resetRanking
    }
}
