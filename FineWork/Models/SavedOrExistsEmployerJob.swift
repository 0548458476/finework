//
//  SavedOrExistsEmployerJob.swift
//  FineWork
//
//  Created by Racheli Kroiz on 30/07/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class SavedOrExistsEmployerJob: NSObject {

    var iEmployerJobId : Int = 0//מזהה משרה
    var nvRole : String = "" //תחום
    var nvDomain : String = ""//תפקיד
    var dtJobStartDate : String = "" //תאריך התחלה
    var dtJobEndDate : String = ""//תאריך סיום
    var mHourlyWage : Double = 0//עלות לשעה
    var mSumWage :Double = 0//סך הכל עלות המשרה
    var nvEmployerJobPopulation : String = ""//אוכלוסיה
    var iEmployerJobPeriodType : Int = 0//עובד לטווח
    var iEmployerJobScheduleType : Int = 0//סוג משרה
    var nvEmployerJobPeriodType : String = ""//עובד לטווח
    var nvEmployerJobScheduleType : String = ""//סוג משרה
    var bBonusOption :Bool = false//אפשרות לבונוסים
    var nvAddress : String = ""//כתובת
    var nvJobDescription : String = ""//תאור
    var bRisk : Bool = false//סיכונים
    var bSafetyRequirement : Bool = false//דרישות
    var iPhysicalDifficultyRank : Int = 0 //קושי פיזי
    var nvWorkSpaceDetails : String = ""//סביבת עבודה
    var   employerJobNewWorker : EmployerJobNewWorker = EmployerJobNewWorker()//תנאי סף
    var lSavedOrExistsEmployerJobWorker = Array<SavedOrExistsEmployerJobWorker>() //עובדים שעבדו במשרה

    var isShowDetails : Bool = false
    
    func getDicFromSSavedOrExistsEmployerJob() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerJobId"] = iEmployerJobId as AnyObject
        dic["nvRole"] = nvRole as AnyObject
        dic["nvDomain"] = nvDomain as AnyObject
        dic["dtJobStartDate"] = dtJobStartDate as AnyObject
        dic["dtJobEndDate"] = dtJobEndDate as AnyObject
        dic["mHourlyWage"] = mHourlyWage as AnyObject
        dic["mSumWage"] = mSumWage as AnyObject
        dic["nvEmployerJobPopulation"] = nvEmployerJobPopulation as AnyObject
        dic["iEmployerJobPeriodType"] = iEmployerJobPeriodType as AnyObject
        dic["iEmployerJobScheduleType"] = iEmployerJobScheduleType as AnyObject
        dic["nvEmployerJobPeriodType"] = nvEmployerJobPeriodType as AnyObject
        dic["nvEmployerJobScheduleType"] = nvEmployerJobScheduleType as AnyObject
        dic["bBonusOption"] = bBonusOption as AnyObject
        dic["nvAddress"] = nvAddress as AnyObject
        dic["nvJobDescription"] = nvJobDescription as AnyObject
        dic["bRisk"] = bRisk as AnyObject
        dic["bSafetyRequirement"] = bSafetyRequirement as AnyObject
        dic["iPhysicalDifficultyRank"] = iPhysicalDifficultyRank as AnyObject
        dic["nvWorkSpaceDetails"] = nvWorkSpaceDetails as AnyObject
        dic["employerJobNewWorker"] = employerJobNewWorker as AnyObject
        dic["lSavedOrExistsEmployerJobWorker"] = lSavedOrExistsEmployerJobWorker as AnyObject

        return dic
    }
    
    func getSavedOrExistsEmployerJobFromDic(dic: Dictionary<String, AnyObject>) -> SavedOrExistsEmployerJob {
        let savedOrExistsEmployerJob = SavedOrExistsEmployerJob()
        
        if dic["iEmployerJobId"] != nil {
            savedOrExistsEmployerJob.iEmployerJobId = Global.sharedInstance.parseJsonToInt(intToParse:  dic["iEmployerJobId"]!)
        }
        if dic["nvRole"] != nil {
            savedOrExistsEmployerJob.nvRole = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvRole"]!)
        }
        if dic["nvDomain"] != nil {
            savedOrExistsEmployerJob.nvDomain = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvDomain"]!)
        }
        if dic["dtJobStartDate"] != nil {
            savedOrExistsEmployerJob.dtJobStartDate = Global.sharedInstance.parseJsonToString(stringToParse: dic["dtJobStartDate"]!)
        }
        if dic["dtJobEndDate"] != nil {
            savedOrExistsEmployerJob.dtJobEndDate = Global.sharedInstance.parseJsonToString(stringToParse: dic["dtJobEndDate"]!)
        }
        if dic["mHourlyWage"] != nil {
            savedOrExistsEmployerJob.mHourlyWage = Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["mHourlyWage"]!)
        }
        if dic["mSumWage"] != nil {
            savedOrExistsEmployerJob.mSumWage = Global.sharedInstance.parseJsonToDouble(doubleToParse: dic["mSumWage"]!)
        }
        if dic["nvEmployerJobPopulation"] != nil {
            savedOrExistsEmployerJob.nvEmployerJobPopulation = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerJobPopulation"]!)
        }
        if dic["iEmployerJobPeriodType"] != nil {
            savedOrExistsEmployerJob.iEmployerJobPeriodType = Global.sharedInstance.parseJsonToInt(intToParse:  dic["iEmployerJobPeriodType"]!)
        }
        if dic["iEmployerJobScheduleType"] != nil {
            savedOrExistsEmployerJob.iEmployerJobScheduleType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerJobScheduleType"]!)
        }
        if dic["nvEmployerJobPeriodType"] != nil {
            savedOrExistsEmployerJob.nvEmployerJobPeriodType = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerJobPeriodType"]!)
        }
        if dic["nvEmployerJobScheduleType"] != nil {
            savedOrExistsEmployerJob.nvEmployerJobScheduleType = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvEmployerJobScheduleType"]!)
        }
        if dic["bBonusOption"] != nil {
            savedOrExistsEmployerJob.bBonusOption = dic["bBonusOption"] as! Bool
        }
        if dic["nvAddress"] != nil {
            savedOrExistsEmployerJob.nvAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!)
        }
        if dic["nvJobDescription"] != nil {
            savedOrExistsEmployerJob.nvJobDescription = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvJobDescription"]!)
        }
        if dic["bRisk"] != nil {
            savedOrExistsEmployerJob.bRisk = dic["bRisk"] as! Bool
        }
        if dic["bSafetyRequirement"] != nil {
            savedOrExistsEmployerJob.bSafetyRequirement = dic["bSafetyRequirement"] as! Bool

        }
        if dic["iPhysicalDifficultyRank"] != nil {
            savedOrExistsEmployerJob.iPhysicalDifficultyRank = Global.sharedInstance.parseJsonToInt(intToParse: dic["iPhysicalDifficultyRank"]!)
        }
        if dic["nvWorkSpaceDetails"] != nil {
            savedOrExistsEmployerJob.nvWorkSpaceDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvWorkSpaceDetails"]!)
        }
        if dic["employerJobNewWorker"] != nil {
            let employer : EmployerJobNewWorker = EmployerJobNewWorker()
            savedOrExistsEmployerJob.employerJobNewWorker = employer.getEmployerJobNewWorkerFromDic(dic: dic["employerJobNewWorker"] as! Dictionary<String, AnyObject>)

        }
        if dic["lSavedOrExistsEmployerJobWorker"] != nil {
            savedOrExistsEmployerJob.lSavedOrExistsEmployerJobWorker = getSavedOrExistsEmployerJobWorker(dic: dic["lSavedOrExistsEmployerJobWorker"] as! Array<Dictionary<String, AnyObject>>)
        }

        
        return savedOrExistsEmployerJob
    }

    func getSavedOrExistsEmployerJobWorker(dic : Array<Dictionary<String, AnyObject>>) -> Array<SavedOrExistsEmployerJobWorker> {
        var savedOrExistsEmployerJobWorkers = Array<SavedOrExistsEmployerJobWorker>()
        
        var item = SavedOrExistsEmployerJobWorker()
        
        for saved in dic {
            item = item.getSavedOrExistsEmployerJobWorkerFromDic(dic: saved)
            savedOrExistsEmployerJobWorkers.append(item)
        }
        
        return savedOrExistsEmployerJobWorkers
    }

    
}
