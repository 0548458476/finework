//
//  WorkerSpouse.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerSpouse: NSObject {
    
    //MARK: - Variables

    var nvSpouseIdentityNumber:String //מס’ זהות/דרכון
    var nvSpouseFirstName:String //שם פרטי
    var nvSpouseLastName:String //שם משפחה
    var dSpouseBirthDate:NSDate? //תאריך לידה
    var dSpouseImmigrationDate:NSDate? //תאריך עליה
    var iSpouseIncomSourceType:Int //מקור הכנסה
    
    //MARK: - Initial
    
    override init() {
        nvSpouseIdentityNumber = ""
        nvSpouseFirstName = ""
        nvSpouseLastName = ""
        //dSpouseBirthDate = NSDate()
        //dSpouseImmigrationDate = NSDate()
        iSpouseIncomSourceType = 0
    }
    
    init(_nvSpouseIdentityNumber:String,_nvSpouseFirstName:String,_nvSpouseLastName:String,_dSpouseBirthDate:NSDate,_dSpouseImmigrationDate:NSDate,_iSpouseIncomSourceType:Int) {
        
        nvSpouseIdentityNumber = _nvSpouseIdentityNumber
        nvSpouseFirstName = _nvSpouseFirstName
        nvSpouseLastName = _nvSpouseLastName
        dSpouseBirthDate = _dSpouseBirthDate
        dSpouseImmigrationDate = _dSpouseImmigrationDate
        iSpouseIncomSourceType = _iSpouseIncomSourceType
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        if nvSpouseIdentityNumber != ""
        {
            dic["nvSpouseIdentityNumber"] = nvSpouseIdentityNumber as AnyObject?
        }
        if nvSpouseFirstName != ""
        {
            dic["nvSpouseFirstName"] = nvSpouseFirstName as AnyObject?
        }
        if nvSpouseLastName != ""
        {
            dic["nvSpouseLastName"] = nvSpouseLastName as AnyObject?
        }
        if dSpouseBirthDate != nil
        {
            dic["dSpouseBirthDate"] = Global.sharedInstance.convertNSDateToString(dateTOConvert: dSpouseBirthDate!) as AnyObject?
        }
        if dSpouseImmigrationDate != nil
        {
            dic["dSpouseImmigrationDate"] = Global.sharedInstance.convertNSDateToString(dateTOConvert: dSpouseImmigrationDate!) as AnyObject?
        }
        dic["iSpouseIncomSourceType"] = iSpouseIncomSourceType as AnyObject?
        
        return dic
    }
    
    func dicToWorkerSpouse(dic:Dictionary<String,AnyObject>)->WorkerSpouse
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let workerSpouse:WorkerSpouse = WorkerSpouse()
        
        workerSpouse.nvSpouseIdentityNumber = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSpouseIdentityNumber"]!)
        workerSpouse.nvSpouseFirstName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSpouseFirstName"]!)
        workerSpouse.nvSpouseLastName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSpouseLastName"]!)
        workerSpouse.dSpouseBirthDate = dateFormatter.date(from: Global.sharedInstance.getStringFromDateString(dateString: Global.sharedInstance.parseJsonToString(stringToParse: dic["dSpouseBirthDate"]!))) as NSDate?
        workerSpouse.dSpouseImmigrationDate = dateFormatter.date(from: Global.sharedInstance.getStringFromDateString(dateString: Global.sharedInstance.parseJsonToString(stringToParse: dic["dSpouseImmigrationDate"]!))) as NSDate?
        //workerSpouse.iSpouseIncomSourceType = Global.sharedInstance.parseJsonToInt(intToParse: iSpouseIncomSourceType as AnyObject)
        workerSpouse.iSpouseIncomSourceType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iSpouseIncomSourceType"] as AnyObject)
        
        
        return workerSpouse
    }
    
    //check if all required fields filled
    //return: true = required fields not filled, false = all required fields filled
    func isRequiredFieldsNull()->Bool
    {
        if nvSpouseIdentityNumber != "" && nvSpouseFirstName != "" && nvSpouseLastName != "" && dSpouseBirthDate != NSDate()
        {
            return false
        }
        return true
    }
}
