//
//  AuthorizedEmploy.swift
//  FineWork
//
//  Created by User on 23.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class AuthorizedEmploy: NSObject {

    var user : User = User() //פרטי משתמש
    var iEmployerFatherId : Int = 0 //מזהה מעסיק האבא
    var mAmount : Int? = 0 //סכום שהוקצב
    var bOneTime : Bool = false //חד פעמי
    var bDeleted : Bool = false //למחיקה?
    var nvOneTimeName : String = "" //חד פעמי/חודשי
    var  iAmountSpent : Int = 0//נוצל
    var  iBalance :Int = 0//יתרה
    
    
    
    func getDictionaryFromAuthorizedEmploy() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["user"] = user.getUserDictionaryForServer() as AnyObject
        dic["iEmployerFatherId"] = iEmployerFatherId as AnyObject
        if mAmount != nil {
            dic["mAmount"] = mAmount as AnyObject
        }
        dic["bOneTime"] = bOneTime as AnyObject
        dic["bDeleted"] = bDeleted as AnyObject
        dic["nvOneTimeName"] = nvOneTimeName as AnyObject
        dic["iAmountSpent"] = iAmountSpent as AnyObject
        dic["iBalance"] = iBalance as AnyObject

        return dic
    }
    
    func getAuthorizedEmployFromDic(dic : Dictionary<String, AnyObject>) -> AuthorizedEmploy {
        let authorizedEmploy = AuthorizedEmploy()
        
        if dic["user"] != nil && dic["user"]?.description != "<null>" {
            let user1 = User()
            authorizedEmploy.user = user1.dicToUser(dicUser: dic["user"] as! NSDictionary)
        }
        
        if dic["iEmployerFatherId"] != nil {
            authorizedEmploy.iEmployerFatherId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerFatherId"]!)
        }
        
        if dic["mAmount"] != nil {
            authorizedEmploy.mAmount = Global.sharedInstance.parseJsonToIntOptionNil(intToParse: dic["mAmount"]!)
        }
        
        if dic["bOneTime"] != nil {
            authorizedEmploy.bOneTime = dic["bOneTime"] as! Bool
        }
        
        if dic["bDeleted"] != nil {
            authorizedEmploy.bDeleted = dic["bDeleted"] as! Bool
        }
        
        if dic["nvOneTimeName"] != nil {
            authorizedEmploy.nvOneTimeName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvOneTimeName"]!)
        }
        if dic["iAmountSpent"] != nil {
            authorizedEmploy.iAmountSpent = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAmountSpent"]!)
        }

        if dic["iBalance"] != nil {
            authorizedEmploy.iBalance = Global.sharedInstance.parseJsonToInt(intToParse: dic["iBalance"]!)
        }

        return authorizedEmploy
    }

}
