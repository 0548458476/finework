//
//  Worker.swift
//  FineWork
//
//  Created by Lior Ronen on 06/03/2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class Worker: NSObject {

    //Members
    var iWorkerUserId : Int = 0 //מזהה עובד
    var nvUserName : String = "" //שם משתמש
    var nvNickName : String = "" //כינוי
    var nvMobile : String = "" //טלפון נייד
    var nvPhone : String = "" //טלפון
    var iFamilyStatusType : Int = 0 //מצב משפחתי
    var fAge :  Float? = nil //גיל
    var nvAddress : String = "" //כתובת
    var nvLng : String = "" //נקודת אורך
    var nvLat : String = "" //נקודת רוחב
    var lWorkerLanguages : Array<WorkerLanguage> = [] //שפות
    var bMilitaryService : Bool = false//שרות צבאי/לאומי?
    var iMilitaryServiceYears : Int? = nil //שרות צבאי - שנים
    var nvMilitaryServiceRole : String = "" //שרות צבאי - תפקיד
    var iPaymentMethodType : Int = 0 //צורת תשלום
    var bContractApproved : Bool = false //חתם על החוזה?
    var nvContractDetails : String = "" //תוכן חוזה
    var workerResumeDetails : WorkerResumeDetails = WorkerResumeDetails() //פרטי קו”ח
    var workerBankAccountDetails : WorkerBankAccountDetails = WorkerBankAccountDetails() //פרטי חשבון בנק
    var nvVideoLink : String = ""
    
    override init() {}
    
    //MARK: --send to server
    func getDictionaryFromWorker() -> Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic["iWorkerUserId"] = iWorkerUserId as AnyObject
        dic["nvUserName"] = nvUserName as AnyObject
        dic["nvNickName"] = nvNickName as AnyObject
        dic["nvMobile"] = nvMobile as AnyObject
        dic["nvPhone"] = nvPhone as AnyObject
        dic["iFamilyStatusType"] = iFamilyStatusType as AnyObject
        if fAge != nil {
            dic["fAge"] = fAge as AnyObject
        }
        dic["nvAddress"] = nvAddress as AnyObject
        dic["nvLng"] = nvLng as AnyObject
        dic["nvLat"] = nvLat as AnyObject
        dic["lWorkerLanguages"] = getlWorkerLanguagesAsDic() as AnyObject
        dic["bMilitaryService"] = bMilitaryService as AnyObject
        if iMilitaryServiceYears != nil {
            dic["iMilitaryServiceYears"] = iMilitaryServiceYears as AnyObject
        }
        dic["nvMilitaryServiceRole"] = nvMilitaryServiceRole as AnyObject
        dic["iPaymentMethodType"] = iPaymentMethodType as AnyObject
        dic["bContractApproved"] = bContractApproved as AnyObject
        dic["nvContractDetails"] = nvContractDetails as AnyObject
        dic["workerResumeDetails"] = workerResumeDetails.getWorkerResumeDetailsAsDic() as AnyObject
        dic["workerBankAccountDetails"] = workerBankAccountDetails.getWorkerBankAccountDetailsAsDic() as AnyObject
        
        dic["nvVideoLink"] = nvVideoLink as AnyObject
        
        return dic
    }
    
    func getlWorkerLanguagesAsDic()->Array<AnyObject>
    {
        var dic : Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        var arr : Array<AnyObject> = []
        
        for workerLanguage in lWorkerLanguages
        {
            dic = workerLanguage.getWorkerLanguageAsDic()
            arr.append(dic as AnyObject)
        }
        return arr
    }
    
    //MARK: --get from server
    func getWorkerFromDictionary(dic : Dictionary<String, AnyObject>) -> Worker {
        
        let worker : Worker = Worker()
        
        if dic["iWorkerUserId"] != nil {
            worker.iWorkerUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iWorkerUserId"]!)
        }
        if dic["nvUserName"] != nil {
            worker.nvUserName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvUserName"]!)
        }
        if dic["nvNickName"] != nil {
            worker.nvNickName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvNickName"]!)
        }
        if dic["nvMobile"] != nil {
            worker.nvMobile = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMobile"]!)
        }
        if dic["nvPhone"] != nil {
            worker.nvPhone = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvPhone"]!)
        }
        if dic["iFamilyStatusType"] != nil {
            worker.iFamilyStatusType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iFamilyStatusType"]!)
        }
        if dic["fAge"] != nil {

            worker.fAge = Global.sharedInstance.parseJsonToFloatOptionNil(floatToParse: dic["fAge"]!)
     //   worker.fAge =(CGFloat)a!
        }
        if dic["nvAddress"] != nil {
            worker.nvAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!)
        }
        if dic["nvLng"] != nil {
            worker.nvLng = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLng"]!)
        }
        if dic["nvLat"] != nil {
            worker.nvLat = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvLat"]!)
        }
        if dic["lWorkerLanguages"] != nil {
            worker.lWorkerLanguages = getlWorkerLanguagesFromDic(dic: dic["lWorkerLanguages"] as! Array<Dictionary<String, AnyObject>>)
        }
        if dic["bMilitaryService"] != nil {
            worker.bMilitaryService = dic["bMilitaryService"] as! Bool
        }
        if dic["iMilitaryServiceYears"] != nil {
            worker.iMilitaryServiceYears = Global.sharedInstance.parseJsonToIntOptionNil(intToParse: dic["iMilitaryServiceYears"]!)
        }
        if dic["nvMilitaryServiceRole"] != nil {
            worker.nvMilitaryServiceRole = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMilitaryServiceRole"]!)
        }
        if dic["iPaymentMethodType"] != nil {
            worker.iPaymentMethodType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iPaymentMethodType"]!)
        }
        if dic["bContractApproved"] != nil {
            worker.bContractApproved = dic["bContractApproved"] as! Bool
        }
        if dic["nvContractDetails"] != nil {
            worker.nvContractDetails = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvContractDetails"]!)
        }
        if dic["workerResumeDetails"] != nil {
            let workerResumeDetails : WorkerResumeDetails = WorkerResumeDetails()
            worker.workerResumeDetails = workerResumeDetails.getWorkerResumeDetailsFromDic(dic: dic["workerResumeDetails"] as! Dictionary<String, AnyObject>)
        }
        if dic["workerBankAccountDetails"] != nil {
            let workerBankAccountDetails : WorkerBankAccountDetails = WorkerBankAccountDetails()
            worker.workerBankAccountDetails = workerBankAccountDetails.getWorkerBankAccountDetailsFromDic(dic: dic["workerBankAccountDetails"]! as! Dictionary<String, AnyObject>)
        }
        
        if dic["nvVideoLink"] != nil {
            worker.nvVideoLink = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvVideoLink"]!)
        }
    
        
        return worker
        
    }
    
    func getlWorkerLanguagesFromDic(dic : Array<Dictionary<String, AnyObject>>) -> Array<WorkerLanguage> {
        var lWorkerLanguages : Array<WorkerLanguage> = Array<WorkerLanguage>()
        
        var workerLanguage : WorkerLanguage = WorkerLanguage()
        
        for language in dic {
            workerLanguage = workerLanguage.getWorkerLanguageFromDic(dic: language)
            lWorkerLanguages.append(workerLanguage)
        }
        
        return lWorkerLanguages
    }
    

}
