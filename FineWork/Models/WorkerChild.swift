//
//  WorkerChild.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerChild: NSObject {
    
    //MARK: - Variables
    var dBirthDate:NSDate? //תאריך לידה
    var nvName:String //שם
    var nvIdentityNumber:String //מס’ זהות/דרכון
    var bWithMe:Bool //בחזקתי?
    var bGetChildAllowance:Bool //מקבל קיצבה בגינו?
    
    //MARK: - Initial
    
    override init() {
        //dBirthDate = NSDate()
        nvName = ""
        nvIdentityNumber = ""
        bWithMe = Bool()
        bGetChildAllowance = Bool()
    }
    
    init(iFamilyStatusType:Int){
        nvName = ""
        nvIdentityNumber = ""
        bWithMe = Bool()
        bGetChildAllowance = Bool()
        if iFamilyStatusType == 2{
            bWithMe = true
            bGetChildAllowance = true
        }
    }
    
    init(_dBirthDate:NSDate,_nvName:String,_nvIdentityNumber:String,_bWithMe:Bool,_bGetChildAllowance:Bool) {
        dBirthDate = _dBirthDate
        nvName = _nvName
        nvIdentityNumber = _nvIdentityNumber
        bWithMe = _bWithMe
        bGetChildAllowance = _bGetChildAllowance
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        if dBirthDate != nil
        {
            dic["dBirthDate"] = Global.sharedInstance.convertNSDateToString(dateTOConvert: dBirthDate!) as AnyObject?
        }
        dic["nvName"] = nvName as AnyObject?
        dic["nvIdentityNumber"] = nvIdentityNumber as AnyObject?
        dic["bWithMe"] = bWithMe as AnyObject?
        dic["bGetChildAllowance"] = bGetChildAllowance as AnyObject?
        
        return dic
    }
    
    func dicToWorkerChild(dic:Dictionary<String,AnyObject>)->WorkerChild
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let workerChild:WorkerChild = WorkerChild()
        
        workerChild.dBirthDate = dateFormatter.date(from: Global.sharedInstance.getStringFromDateString(dateString: Global.sharedInstance.parseJsonToString(stringToParse: dic["dBirthDate"]!))) as NSDate?
        workerChild.nvName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvName"]!)
        workerChild.nvIdentityNumber = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvIdentityNumber"]!)
        workerChild.bWithMe = dic["bWithMe"] as! Bool
        workerChild.bGetChildAllowance = dic["bGetChildAllowance"] as! Bool
        
        return workerChild
    }
    
    //בדיקה האם ערכו את הכפתורים נמצא בחזקתי או מקבל קצבת ילדים בגינו
    //למקרה בו נשנה סטטוס (לדוג׳ מרווקה לנשואה) ואז: אם לחצו על הכפתורים
    //check if the object null
    //return true = null,false = not null
    func isObjectNull(btnChanged:Bool)->Bool
    {
        if btnChanged == true
        {
            return false
        }
        let calendar = NSCalendar.current
        
        let componentsBirthDate = calendar.dateComponents([.day, .month, .year], from: dBirthDate as! Date)
        let componentsNSDate = calendar.dateComponents([.day, .month, .year], from: NSDate() as Date)
        
        if componentsBirthDate.day == componentsNSDate.day && componentsBirthDate.month == componentsNSDate.month && componentsBirthDate.year == componentsNSDate.year && nvName == "" && nvIdentityNumber == "" &&
        nvName == "" && bWithMe == Bool() && bGetChildAllowance == Bool()
        {
            return true
        }
        return false
    }
    
    func workerChildToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<WorkerChild>{
        
        var arrWorkerChild:Array<WorkerChild> = Array<WorkerChild>()
        var workerChild:WorkerChild = WorkerChild()
        
        for i in 0  ..< arrDic.count
        {
            workerChild = dicToWorkerChild(dic: arrDic[i] )
            arrWorkerChild.append(workerChild)
        }
        return arrWorkerChild
    }
}
