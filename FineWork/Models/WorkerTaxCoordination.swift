//
//  WorkerTaxCoordination.swift
//  FineWork
//
//  Created by Racheli Kroiz on 18.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit

class WorkerTaxCoordination: NSObject {
    
    //MARK: - Variables

    var nvTikNikuim:String //תיק ניכויים
    var nvIncomeSourceName:String //שם מעסיק
    var nvAddress:String //כתובת
    var iIncomeType:Int //סוג הכנסה
    var mMonthlyIncomeAmount:Float //הכנסה חודשית
    var mTaxDeducted:Float //המס שנוכה
    var fileObj:FileObj// תלוש מצורף

    //MARK: - Initial
    
    override init() {
        nvTikNikuim = ""
        nvIncomeSourceName = ""
        nvAddress = ""
        iIncomeType = 0
        mMonthlyIncomeAmount = 0.0
        mTaxDeducted = 0.0
        fileObj = FileObj()
    }
    
    init(_nvTikNikuim:String,_nvIncomeSourceName:String,_nvAddress:String,_iIncomeType:Int,_mMonthlyIncomeAmount:Float, _mTaxDeducted:Float,_fileObj:FileObj) {
        nvTikNikuim = _nvTikNikuim
        nvIncomeSourceName = _nvIncomeSourceName
        nvAddress = _nvAddress
        iIncomeType = _iIncomeType
        mMonthlyIncomeAmount = _mMonthlyIncomeAmount
        mTaxDeducted = _mTaxDeducted
        fileObj = _fileObj
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        if nvTikNikuim != ""
        {
            dic["nvTikNikuim"] = nvTikNikuim as AnyObject?
        }
        if nvIncomeSourceName != ""
        {
            dic["nvIncomeSourceName"] = nvIncomeSourceName as AnyObject?
        }
        if nvAddress != ""
        {
            dic["nvAddress"] = nvAddress as AnyObject?
        }
        if iIncomeType != 0
        {
            dic["iIncomeType"] = iIncomeType as AnyObject?
        }
        if mMonthlyIncomeAmount != 0.0
        {
            dic["mMonthlyIncomeAmount"] = mMonthlyIncomeAmount as AnyObject?
        }
        if mTaxDeducted != 0.0
        {
            dic["mTaxDeducted"] = mTaxDeducted as AnyObject?
        }
        //תוספת כדי שלא ידרוס קובץ שכבר נשמר בשרת
        //|| fileObj.nvFileURL != ""
        if fileObj.nvFile != "" || fileObj.nvFileURL != ""
        {
            dic["fileObj"] = fileObj.getDic() as AnyObject?
        }
        
        return dic
    }
    
    func dicToWorkerTaxCoordination(dic:Dictionary<String,AnyObject>)->WorkerTaxCoordination
    {
        let workerTaxCoordination:WorkerTaxCoordination = WorkerTaxCoordination()
        
        workerTaxCoordination.nvTikNikuim = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvTikNikuim"]!)
        
        workerTaxCoordination.nvIncomeSourceName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvIncomeSourceName"]!)
        
        workerTaxCoordination.nvAddress = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAddress"]!)
        
        if dic["iIncomeType"] != nil
        {
        workerTaxCoordination.iIncomeType = Global.sharedInstance.parseJsonToInt(intToParse:  dic["iIncomeType"]!)
        }
        
        if dic["mMonthlyIncomeAmount"] != nil
        {
        workerTaxCoordination.mMonthlyIncomeAmount = Global.sharedInstance.parseJsonToFloat(floatToParse: dic["mMonthlyIncomeAmount"]!)
        }
        
        if dic["mTaxDeducted"] != nil
        {
            workerTaxCoordination.mTaxDeducted = Global.sharedInstance.parseJsonToFloat(floatToParse: dic["mTaxDeducted"]!)
        }
        
        if dic["fileObj"] is NSNull
        {
            workerTaxCoordination.fileObj = fileObj
        }
        else
        {
            workerTaxCoordination.fileObj = fileObj.dicToFileObj(dic: dic["fileObj"] as! Dictionary<String, AnyObject>)
        }
        
        return workerTaxCoordination
    }
    
    func workerTaxCoordinationToArray(arrDic:Array<Dictionary<String,AnyObject>>)->Array<WorkerTaxCoordination>{
        
        var arrWorkerTaxCoordination:Array<WorkerTaxCoordination> = Array<WorkerTaxCoordination>()
        var workerTaxCoordination:WorkerTaxCoordination = WorkerTaxCoordination()
        
        for i in 0  ..< arrDic.count
        {
            workerTaxCoordination = dicToWorkerTaxCoordination(dic: arrDic[i] )
            arrWorkerTaxCoordination.append(workerTaxCoordination)
        }
        return arrWorkerTaxCoordination
    }
}
