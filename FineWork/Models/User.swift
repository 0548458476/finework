//
//  User.swift
//  FineWork
//
//  Created by Lior Ronen on 06/12/2016.
//  Copyright © 2016 webit. All rights reserved.
//
import UIKit

enum UserType:Int {
    case manager = 16
    case employer = 17
    case worker = 18
    case authorized = 72
}

//משתמש
class User: NSObject {
    
  //MARK:-  Members
    
    var iUserId:Int = 0 //מזהה משתמש
    var nvUserName:String = "" //שם משתמש
    var nvPassword:String = "" //סיסמה
    var nvMail:String = "" //מייל
    var nvMobile:String = "" //נייד
    var nvRecommendPhone:String = "" //טלפון ממליץ
    var iUserType:UserType = UserType.worker //סוג משתמש
    var nvGoogleId:String = "" //מזהה גוגל
    var nvFacebookId:String = "" //מזהה פייסבוק
    var iEmployerId:Int = 0 //מזהה מעסיק אם הסוג משתמש הוא מעסיק
    var picture:FileObj = FileObj()
    //var iAuthorizedEmployId:Int = 0 //מזהה משתמש מורשה אם הסוג משתמש הוא משתמש מורשה
    let bLoginFromApp = true
    var bPaymentMethod : Bool = false
    var bAuthorizedEmployer : Bool = false
    
    
    // שמירת תמונת הפרופיל לצורך הצגת התמונה ללא שמוש בurl
    var profileImage : UIImage!
    //MARK: - Initial
    
    override init() {}
    init(iUserId_:Int,nvUserName_:String,nvPassword_:String,nvMail_:String,nvMobile_:String,nvRecommendPhone_:String,iUserType_:Int,nvGoogleId_:String,nvFacebookId_:String,iEmployerId_:Int) {
       
        iUserId = iUserId_
        nvUserName = nvUserName_
        nvPassword = nvPassword_
        nvMail = nvMail_
        nvMobile = nvMobile_
        nvRecommendPhone = nvRecommendPhone_
        iUserType = UserType(rawValue: iUserType_)!
        nvGoogleId = nvGoogleId_
        nvFacebookId = nvFacebookId_
        iEmployerId = iEmployerId_
    }
    
    
    func getUserDictionaryForServer() -> Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic["iUserId"] = self.iUserId as AnyObject?
        dic["nvUserName"] = self.nvUserName as AnyObject?
        dic["nvPassword"] = self.nvPassword as AnyObject?
        dic["nvMail"] = self.nvMail as AnyObject?
        dic["nvMobile"] = self.nvMobile as AnyObject?
        dic["nvRecommendPhone"] = self.nvRecommendPhone as AnyObject?
        dic["iUserType"] = self.iUserType.rawValue as AnyObject?
        dic["nvGoogleId"] = self.nvGoogleId as AnyObject?
        dic["nvFacebookId"] = self.nvFacebookId as AnyObject?
        dic["iEmployerId"] = self.iEmployerId as AnyObject?
        dic["bLoginFromApp"] = self.bLoginFromApp as AnyObject?
        dic["bAuthorizedEmployer"] = self.bAuthorizedEmployer as AnyObject?

        return dic
    }
    //MARK: - getUserDictionaryForRegister set user in dictionary to register
    func getUserDictionaryForRegister() ->  Dictionary<String,AnyObject>
    {
        var dict = [String: AnyObject]()
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        dic["nvUserName"] = self.nvUserName as AnyObject?
        dic["nvPassword"] = self.nvPassword as AnyObject?
        dic["nvMail"] = self.nvMail as AnyObject?
        dic["nvMobile"] = self.nvMobile as AnyObject?
        dic["nvRecommendPhone"] = self.nvRecommendPhone as AnyObject?
        
        dic["nvGoogleId"] = self.nvGoogleId as AnyObject?
        dic["nvFacebookId"] = self.nvFacebookId as AnyObject?
        
        dic["iUserType"] = self.iUserType.rawValue as AnyObject?
        dic["bLoginFromApp"] = self.bLoginFromApp as AnyObject?
        //dic["iAuthorizedEmployId"] = self.iAuthorizedEmployId as AnyObject?
        dic["nvTokenId"] = Global.sharedInstance.token as AnyObject?
        dic["iDeviceType"] = 193 as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        dict["user"] = dic as AnyObject?
        return dict   
    }
    func getUserDictionaryForLogin() ->  Dictionary<String,AnyObject>
    {
        
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        if !self.nvGoogleId.isEmpty{
            dic["nvGoogleId"] = self.nvGoogleId as AnyObject?
        }else
            if !self.nvFacebookId.isEmpty{
                dic["nvFacebookId"] = self.nvFacebookId as AnyObject?
            }else{
             dic["nvPassword"] = self.nvPassword as AnyObject?
        }
        dic["nvMobile"] = self.nvMobile as AnyObject?
        dic["nvTokenId"] = Global.sharedInstance.token as AnyObject?
        dic["iDeviceType"] = 193 as AnyObject?
        dic["iLanguageId"] = Global.sharedInstance.iLanguageId as AnyObject?
        dic["bLoginFromApp"] = self.bLoginFromApp as AnyObject?

        return dic
    }

    
    func dicToUser(dicUser:NSDictionary)-> User
    {
        let user:User = User()
        
        user.iUserId = Global.sharedInstance.parseJsonToInt(intToParse: dicUser["iUserId"]! as AnyObject)
        if dicUser["nvUserName"] != nil
        {
            user.nvUserName = Global.sharedInstance.parseJsonToString(stringToParse: dicUser["nvUserName"]! as AnyObject)
        }
        if dicUser["nvPassword"] != nil{
            user.nvPassword = Global.sharedInstance.parseJsonToString(stringToParse: dicUser["nvPassword"]! as AnyObject)
        }
        if dicUser["nvMail"] != nil
        {
            user.nvMail = Global.sharedInstance.parseJsonToString(stringToParse: dicUser["nvMail"]! as AnyObject)
        }
        if dicUser["nvMobile"] != nil
        {
            user.nvMobile = Global.sharedInstance.parseJsonToString(stringToParse: dicUser["nvMobile"]! as AnyObject)
        }
        if dicUser["nvRecommendPhone"] != nil
        {
            user.nvRecommendPhone = Global.sharedInstance.parseJsonToString(stringToParse: dicUser["nvRecommendPhone"]! as AnyObject)
        }
        if dicUser["iUserType"] != nil
        {
            user.iUserType = UserType(rawValue: Global.sharedInstance.parseJsonToInt(intToParse: dicUser["iUserType"]! as AnyObject))!
        }
        if dicUser["nvGoogleId"] != nil
        {
            user.nvGoogleId = Global.sharedInstance.parseJsonToString(stringToParse: dicUser["nvGoogleId"]! as AnyObject)
        }
        if dicUser["nvFacebookId"] != nil
        {
            user.nvFacebookId = Global.sharedInstance.parseJsonToString(stringToParse: dicUser["nvFacebookId"]! as AnyObject)
        }
        if dicUser["iEmployerId"] != nil
        {
            user.iEmployerId = Global.sharedInstance.parseJsonToInt(intToParse: dicUser["iEmployerId"]! as AnyObject)
        }
        
        if dicUser["bPaymentMethod"] != nil
        {
            user.bPaymentMethod = dicUser["bPaymentMethod"] as! Bool
        }
        if dicUser["bAuthorizedEmployer"] != nil
        {
            user.bAuthorizedEmployer = dicUser["bAuthorizedEmployer"] as! Bool
        }

        return user
    }
}
