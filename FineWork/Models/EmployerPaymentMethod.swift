//
//  EmployerPaymentMethod.swift
//  FineWork
//
//  Created by User on 18.4.2017.
//  Copyright © 2017 webit. All rights reserved.
//

import UIKit

class EmployerPaymentMethod: NSObject {

    var iEmployerIPaymentMethodId : Int = 0 //מזהה פרטי תשלום למעסיק
    var iPaymentMethodStatusType : Int = 0 //סטטוס פרטי התשלום
    var nvPaymentMethodStatusTypeName : String = "" //שם סטטוס פרטי התשלום
    var iPaymentMethodType : Int = 0 //סוג פרטי התשלום
    
    var nvTokenNumber : String = "" //טוקן
    var nvCardLastFourDigits : String = "" //4 ספרות אחרונות של כרטיס אשראי
    
    var nvBank : String = "" //בנק
    var iBranchNumber : Int? = nil //מס’ סניף
    var iAccountNumber : Int? = nil //מס’ חשבון
    var nvAccountName : String = "" //שם בעל החשבון
    
    var directDebit : FileObj = FileObj() //קובץ של הוראת קבע
    var bBankAccountDetailsUpdated : Bool = false //פרטי תשלום עודכנו?
    
    
    func getDicFromEmployerPaymentMethod() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iEmployerIPaymentMethodId"] = iEmployerIPaymentMethodId as AnyObject
        dic["iPaymentMethodStatusType"] = iPaymentMethodStatusType as AnyObject
        dic["nvPaymentMethodStatusTypeName"] = nvPaymentMethodStatusTypeName as AnyObject
        dic["iPaymentMethodType"] = iPaymentMethodType as AnyObject
        dic["nvTokenNumber"] = nvTokenNumber as AnyObject
        dic["nvCardLastFourDigits"] = nvCardLastFourDigits as AnyObject
        dic["nvBank"] = nvBank as AnyObject
        if iBranchNumber != nil {
            dic["iBranchNumber"] = iBranchNumber as AnyObject
        }
        if iAccountNumber != nil {
            dic["iAccountNumber"] = iAccountNumber as AnyObject
        }
        dic["nvAccountName"] = nvAccountName as AnyObject
        dic["directDebit"] = directDebit.getDic() as AnyObject
        dic["bBankAccountDetailsUpdated"] = bBankAccountDetailsUpdated as AnyObject
        
        return dic
    }
    
    func getEmployerPaymentMethodFromDic(dic : Dictionary<String, AnyObject>) -> EmployerPaymentMethod {
        var employerPaymentMethod = EmployerPaymentMethod()
        
        if dic["iEmployerIPaymentMethodId"] != nil {
            employerPaymentMethod.iEmployerIPaymentMethodId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iEmployerIPaymentMethodId"]!)
        }
        if dic["iPaymentMethodStatusType"] != nil {
            employerPaymentMethod.iPaymentMethodStatusType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iPaymentMethodStatusType"]!)
        }
        if dic["nvPaymentMethodStatusTypeName"] != nil {
            employerPaymentMethod.nvPaymentMethodStatusTypeName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvPaymentMethodStatusTypeName"]!)
        }
        if dic["iPaymentMethodType"] != nil {
            employerPaymentMethod.iPaymentMethodType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iPaymentMethodType"]!)
        }
        if dic["nvTokenNumber"] != nil {
            employerPaymentMethod.nvTokenNumber = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvTokenNumber"]!)
        }
        if dic["nvCardLastFourDigits"] != nil {
            employerPaymentMethod.nvCardLastFourDigits = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvCardLastFourDigits"]!)
        }
        if dic["nvBank"] != nil {
            employerPaymentMethod.nvBank = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvBank"]!)
        }
        if dic["iBranchNumber"] != nil {
            employerPaymentMethod.iBranchNumber = Global.sharedInstance.parseJsonToIntOptionNil(intToParse: dic["iBranchNumber"]!)
        }
        if dic["iAccountNumber"] != nil {
            employerPaymentMethod.iAccountNumber = Global.sharedInstance.parseJsonToIntOptionNil(intToParse: dic["iAccountNumber"]!)
        }
        if dic["nvAccountName"] != nil {
            employerPaymentMethod.nvAccountName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAccountName"]!)
        }
        if dic["directDebit"] != nil {
            let fileObj = FileObj()
            employerPaymentMethod.directDebit = fileObj.dicToFileObj(dic: dic["directDebit"]! as! Dictionary<String, AnyObject>)
        }
        if dic["bBankAccountDetailsUpdated"] != nil {
            employerPaymentMethod.bBankAccountDetailsUpdated = dic["bBankAccountDetailsUpdated"] as! Bool
        }
        
        return employerPaymentMethod
    }
}
